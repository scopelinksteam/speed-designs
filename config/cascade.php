<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Multi Tenancy Configurations
    |--------------------------------------------------------------------------
    |
    | This is where you can toggle between Available Components Availability
    | note that developer mode when Enabled it will override many components
    | and make them available by default.
    |
    */

    'multi_tenancy' => env('MULTI_TENANCY', false),

    'central_domain' => env('CENTRAL_DOMAIN', ''),

    /*
    |--------------------------------------------------------------------------
    | Application Components
    |--------------------------------------------------------------------------
    |
    | This is where you can toggle between Available Components Availability
    | note that developer mode when Enabled it will override many components
    | and make them available by default.
    |
    */

    'developer_mode' => env('DEVELOPER_MODE', false),

    /*
    |--------------------------------------------------------------------------
    | Modules Manager
    |--------------------------------------------------------------------------
    |
    | Setting this to 'true' will add a Modules Manager to the Dashboard,
    | By disable it will not only remove from dashboard but also remove
    | the routes and configurations
    |
    */

    'modules_manager' => env('MODULES_MANAGER', true),

    /*
    |--------------------------------------------------------------------------
    | Plugins Manager
    |--------------------------------------------------------------------------
    |
    | Setting this to 'true' will add a Plugins Manager to the Dashboard,
    | By disable it will not only remove from dashboard but also remove
    | the routes and configurations
    |
    */

    'plugins_manager' => env('PLUGINS_MANAGER', true),

    /*
    |--------------------------------------------------------------------------
    | Themes Manager
    |--------------------------------------------------------------------------
    |
    | Setting this to 'true' will add a Site Themes Manager to the Dashboard,
    | By disable it will not only remove from dashboard but also remove
    | the routes and configurations
    |
    */

    'themes_manager' => env('THEMES_MANAGER', true),

    /*
    |--------------------------------------------------------------------------
    | Translation Manager
    |--------------------------------------------------------------------------
    |
    | Setting this to 'true' will add a Translation and Languages Manager to the
    | Dashboard, By disable it will not only remove from dashboard but also remove
    | the routes and configurations
    |
    */

    'translation_manager' => env('TRANSLATION_MANAGER', true),

    /*
    |--------------------------------------------------------------------------
    | Site Manager
    |--------------------------------------------------------------------------
    |
    | Setting this to 'true' will add a Site Themes Manager to the Dashboard,
    | By disable it will not only remove from dashboard but also remove
    | the routes and configurations
    |
    */

    'site_manager' => env('SITE_MANAGER', true),

    /*
    |--------------------------------------------------------------------------
    | Sliders Manager
    |--------------------------------------------------------------------------
    |
    | Setting this to 'true' will add a Site Sliders Manager to the Dashboard,
    | By disable it will not only remove from dashboard but also remove
    | the routes and configurations
    |
    */

    'sliders_manager' => env('SLIDERS_MANAGER', true),


];
