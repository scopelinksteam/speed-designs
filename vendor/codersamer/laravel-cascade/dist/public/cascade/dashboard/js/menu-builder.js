document.addEventListener('DOMContentLoaded', function(){

    Livewire.on('ItemsUpdated', () => {
            new NestedSort(
                {
                    el: '#menu-builder-items',
                    actions : {
                        onDrop : function(data) {
                            Livewire.emit('SetMenuOrder', data);
                            console.log('Menu Changed');
                            console.log(data);
                        }
                    }
                }
            );
    })

    Array.from(document.getElementsByClassName('add-menu-item-button')).forEach(function(el){
        el.addEventListener('click', function(e) {
            var card = e.target.closest('.card');
            if(!card) { return; }
            var form  = card.querySelector('form');
            if(!form) { return;}
            var data = {};
            for (var i = 0; i < form.elements.length; i++) {
                data[form.elements[i].name] = form.elements[i].value;
            }
            Livewire.emit('AddMenuItem', card.dataset.builderId, data);
        });
    });

    document.addEventListener('click',function(e){
        if(e.target && e.target.classList.contains('update-menu-item-button')){
            var card = e.target.closest('.card');
            if(!card) { return; }
            var form  = card.querySelector('form');
            if(!form) { return;}
            var data = {};
            for (var i = 0; i < form.elements.length; i++) {
                data[form.elements[i].name] = form.elements[i].value;
            }
            Livewire.emit('UpdateMenuItem', card.dataset.builderId, e.target.dataset.itemId, data);
         }
     });

    new NestedSort({
        el: '#menu-builder-items',
        actions : {
            onDrop : function(data) {
                Livewire.emit('SetMenuOrder', data);
                console.log('Menu Changed');
                console.log(data);
            }
        }
    });
});
