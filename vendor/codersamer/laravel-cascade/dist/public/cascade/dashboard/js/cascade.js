
document.addEventListener('DOMContentLoaded', function(){

    //Clear Title
    document.title = document.title.replace(/<\/?[^>]+(>|$)/g, "");

    Array.from(document.querySelectorAll('[data-toggle=form]')).forEach(function(triggerElement){
        triggerElement.addEventListener('click', function(e){
            e.preventDefault();
            e.stopPropagation();
            var formItem = document.forms[e.target.dataset.target];
            if(formItem) { formItem.submit(); }
        });
    });

    Array.from(document.querySelectorAll('.phone-input')).forEach(function(phoneElement){
        window.intlTelInput(phoneElement, {
            //initialisation options
            autoPlaceholder: 'aggressive',
            separateDialCode: true,
            hiddenInput: 'full',
            utilsScript : "/cascade/plugins/intl-tel-input/js/utils.js"
          });
    });

    const DashboardStatistics = document.querySelectorAll('.dashboard-statistics');

    Array.from(DashboardStatistics).forEach(function(sliderContainer){
        new Swiper(sliderContainer, {
            slidesPerView : 1,
            spaceBetween : 20,
            breakpoints: {
                // when window width is >= 320px
                320: {
                  slidesPerView: 1,
                  spaceBetween: 20
                },
                // when window width is >= 480px
                480: {
                  slidesPerView: 2,
                  spaceBetween: 20
                },
                // when window width is >= 640px
                640: {
                  slidesPerView: 3,
                  spaceBetween: 20
                },
                // when window width is >= 640px
                760: {
                    slidesPerView: 4,
                    spaceBetween: 20
                }
            },
            navigation: {
                nextEl: '#dashboard-statistics-next',
                prevEl: '#dashboard-statistics-prev'
            }
        });
    })

    if(document.querySelector('.selectr') != null)
    {
        Array.from(document.querySelectorAll('.selectr')).forEach(function(selectElement){
            if(selectElement.dataset.taggable)
            {
                new Selectr(selectElement, { searchable: true, taggable: true });
                return;
            }
            new Selectr(selectElement, { searchable: true });
        })
    }

    window.RebuildSelectr = () =>
    {
        Array.from(document.querySelectorAll('.selectr')).forEach(function(selectElement){
            if(selectElement.classList.contains('selectr-hidden')) { return; }
            if(selectElement.dataset.taggable)
            {
                new Selectr(selectElement, { searchable: true, taggable: true });
                return;
            }
            new Selectr(selectElement, { searchable: true });
        })
    }

    Livewire.hook('message.processed', (message, component) => { RebuildSelectr(); });
    //Rebuild Dropify Images
    window.RebuildDropify = () =>
    {
        Array.from(document.querySelectorAll('.dropify-image')).forEach(function(imageElement){
            if(imageElement.parentElement.classList.contains('dropify-wrapper')) { return; }
            $(imageElement).dropify({});
        })
    }
    Livewire.hook('message.processed', (message, component) => { RebuildDropify(); });

    window.RebuildIcons = () => 
    {
        const iconInputs = Array.from(document.querySelectorAll('.icon-select'));
        if(iconInputs.length > 0)
        {
            iconInputs.forEach((selector) => {
                if(selector.classList.contains('initialized')) { return; }
                selector.classList.add('initialized');
                const ButtonId = selector.querySelector('.icon-select-toggle').id;
                const iconPicker = new UniversalIconPicker('#'+ButtonId, {
                    iconLibraries: ICON_LIBRARIES.split(','),
                    iconLibrariesCss: [

                    ],
                    //resetSelector: '#uip-reset-btn',
                    onSelect: function(jsonIconData) {
                        selector.querySelector('.icon-display').innerHTML = jsonIconData.iconHtml;
                        selector.querySelector('.icon-select-value').value = jsonIconData.iconClass;
                    },
                    onReset: function() {
                        selector.querySelector('.icon-display').innerHTML.innerHTML = '';
                        selector.querySelector('.icon-select-value').value = '';
                    }
                });
            });
        }
    }
    Livewire.hook('message.processed', (message, component) => { RebuildIcons(); });

    if(document.querySelector('.fontawesome-select') != null)
    {
        new Selectr('.fontawesome-select', { searchable: true, customClass: 'fontawesome-selectr' });
    }
    $('.dropify-image').dropify({

    });
    $('.dropify-archive').dropify({
        allowedExtensions : ['png']
    });
    $('.dropify-file').dropify({

    });
    $('.dropify-sheet').dropify({

    });
    $('.select2').select2();




    var customPlugins = [];
    customPlugins.push('advlist autolink lists link image charmap print preview anchor');
            customPlugins.push('searchreplace visualblocks code fullscreen');
            customPlugins.push('insertdatetime media table paste imagetools wordcount');
    document.dispatchEvent(new CustomEvent('tinymce.plugins', { detail : {
        plugins: customPlugins
    }}));
    var customToolbar = { buttons : 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image'};
    document.dispatchEvent(new CustomEvent('tinymce.toolbar', { detail : {
        toolbar: customToolbar
    }}));

    tinymce.init({
        selector: '.editor',
        height : '380',
        setup: function(ed) {

            if ($('#'+ed.id).prop('readonly')) {
                ed.settings.readonly = true;
            }
            console.log(customPlugins);
            console.log(customToolbar);
            ed.ui.registry.addMenuButton('shortcodes', {
                text : "Shorcodes",
                fetch : function(callback){
                    var items = [
                        {
                            type: "menuitem",
                            text: "Bold",
                            onAction: function(){
                                ed.insertContent('[b]Bold[/b]');
                            }
                        }
                    ];
                    callback(items);
                }
            });
        },
        relative_urls : false,
        remove_script_host : false,
        convert_urls : true,
        images_upload_handler: function(blobInfo, success,failure, progress){
            var url = '/dashboard/tinymce/image-upload';
            var formData = new FormData();
            formData.append('file', blobInfo.blob(), blobInfo.filename());
            fetch(url, {
                method : 'POST',
                body : formData
            })
            .then((response) => {
                if(response.status == 200)
                {
                    response.json().then(json => {
                        success(json.location);
                    });
                }
                else
                {
                    response.json().then(json => {
                        failure(json.message);
                    });
                }
            })
            .catch((error) => {

            });
        },
        plugins: customPlugins,
        toolbar:customToolbar.buttons,
    });
    const iconInputs = Array.from(document.querySelectorAll('.icon-select'));
    if(iconInputs.length > 0)
    {
        iconInputs.forEach((selector) => {
            if(selector.classList.contains('initialized')) { return; }
            selector.classList.add('initialized');
            const ButtonId = selector.querySelector('.icon-select-toggle').id;
            const iconPicker = new UniversalIconPicker('#'+ButtonId, {
                iconLibraries: ICON_LIBRARIES.split(','),
                iconLibrariesCss: [
                ],
                onSelect: function(jsonIconData) {
                    selector.querySelector('.icon-display').innerHTML = jsonIconData.iconHtml;
                    selector.querySelector('.icon-select-value').value = jsonIconData.iconClass;
                },
                onReset: function() {
                    selector.querySelector('.icon-display').innerHTML.innerHTML = '';
                    selector.querySelector('.icon-select-value').value = '';
                }
            });
        });
    }

});
