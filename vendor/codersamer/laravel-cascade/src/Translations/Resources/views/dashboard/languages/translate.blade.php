@extends('admin::layouts.master')
@section('page-title', __('Translate Language').' : '. $language->name)
@section('page-actions')
<a href="{{ route('dashboard.translations.languages.translate', ['language' => $language, ]) }}" class="btn btn-primary">@lang('All')</a>
<a href="{{ route('dashboard.translations.languages.translate', ['language' => $language, 'mode' => 'not-translated']) }}" class="btn btn-warning">@lang('Not Translated')</a>
<a href="{{ route('dashboard.translations.languages.translate', ['language' => $language, 'mode' => 'translated']) }}" class="btn btn-success">@lang('Translated')</a>
<a href="{{ route('dashboard.translations.languages.index') }}" class="btn btn-dark">@lang('Languages')</a>
@endsection
@section('content')

<div class="row">

    <div class="col-12">
        <form action="{{ route('dashboard.translations.languages.translate.store') }}" method="POST">
            @csrf
            <input type="hidden" name="language_id" value="{{ $language->id }}">
            <div class="card">
                <div class="card-header"></div>
                <div class="card-body">
                    <table class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>@lang('Original Text')</th>
                                <th style="width:50%">@lang('Translation')</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($translations as $translation)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $translation->original_string }}</td>
                                <td><input type="text" name="translations[{{ $translation->raw_id }}]" id="" class="form-control" value="{{ $translation->translated_string }}"></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="card-footer">
                    {{ $translations->appends(request()->query())->links() }}
                    <div class="div float-right">
                        <input type="submit" value="@lang('Save Translations')" class="btn btn-primary">
                    </div>
                </div>
            </div>
        </form>
    </div>

</div>
@endsection
