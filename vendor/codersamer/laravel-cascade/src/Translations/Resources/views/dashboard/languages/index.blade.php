@extends('admin::layouts.master')
@section('page-title', __('Languages Management'))
@section('page-actions')
@can('scan languages')
<a href="{{ route('dashboard.translations.scan') }}" class="btn btn-warning"> @lang('Scan Translations') </a>
@endcan
@endsection
@section('content')
<div class="row">
    <div class="col-4">
        <form action="{{ route('dashboard.translations.languages.store') }}" method="POST" enctype="multipart/form-data">
            @csrf
            @isset($language)
            <input type="hidden" name="entity_id" value="{{ $language->id }}">
            @endisset
            <div class="card">
                <div class="card-header">@lang('Language Details')</div>
                <div class="card-body">
                    <div class="form-group">
                        <label for="name">@lang('Name')</label>
                        <input type="text" name="name" id="name" value="@isset($language) {{ $language->name }} @endisset" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="locale">@lang('Locale')</label>
                        <input type="text" name="locale" id="locale" value="@isset($language) {{ $language->locale }} @endisset" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="flag">@lang('Flag')</label>
                        <input type="file" name="flag" id="flag" @isset($language) data-default-file="{{ $language->flag != null ? url('uploads/'.$language->flag) : ''}}" @endisset class="dropify-image">
                    </div>
                    <div class="form-group">
                        <label for="direction">@lang('Direction')</label>
                        <select name="direction" id="direction" class="select2 form-control">
                            <option value="0" @if(isset($language) && !$language->is_rtl) selected @endif>@lang('Left to Right')</option>
                            <option value="1" @if(isset($language) && $language->is_rtl) selected @endif>@lang('Right to Left')</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="status">@lang('Status')</label>
                        <select name="status" id="status" class="select2 form-control">
                            <option value="1" @if(isset($language) && $language->enabled) selected @endif>@lang('Enabled')</option>
                            <option value="0" @if(isset($language) && !$language->enabled) selected @endif>@lang('Disabled')</option>
                        </select>
                    </div>
                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">@lang('Save Language')</button>
                </div>
            </div>
        </form>
    </div>
    <div class="col-8">
        <div class="card">
            <div class="card-header">
                @lang('Available Languages')
            </div>
            <div class="card-body">
                <livewire:datatable :table="$table" />
            </div>
        </div>
    </div>
</div>
@endsection
