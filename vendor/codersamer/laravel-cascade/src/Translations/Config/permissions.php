<?php

return [
    'create languages' => 'Allow User to Create Languages',
    'edit languages' => 'Edit Existing Languages Details',
    'delete languages' => 'Delete Created Languages',
    'translate languages' => 'Translate Language Contents',
    'build languages' => 'Build and Apply Translations',
    'scan translations' => 'Scan System for Translatable Content'
];
