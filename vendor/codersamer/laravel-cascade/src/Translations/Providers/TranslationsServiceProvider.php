<?php

namespace Cascade\Translations\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;
use Cascade\Dashboard\Entities\MenuItem;
use Cascade\Dashboard\Services\DashboardBuilder;
use Cascade\Dashboard\Services\DashboardMenu;
use Cascade\Translations\Console\ScanTranslationsCommand;
use Cascade\Translations\Http\Middleware\ApplyLocaleMiddleware;
use Illuminate\Support\Facades\Artisan;
use SebastianBergmann\CodeCoverage\Report\Html\Dashboard;

class TranslationsServiceProvider extends ServiceProvider
{
    /**
     * @var string $moduleName
     */
    protected $moduleName = 'Translations';

    /**
     * @var string $moduleNameLower
     */
    protected $moduleNameLower = 'translations';

    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerTranslations();
        $this->registerConfig();
        $this->registerViews();
        $this->loadMigrationsFrom(module_path($this->moduleName, 'Database/Migrations'));

        $this->registerMiddlewares();
        $this->registerMenus();
    }

    /**
     * Register Module Menus
     *
     * @return void
     */
    protected function registerMenus()
    {
        if(translation_manager_enabled())
        {
            DashboardBuilder::Tenant(function(){
                DashboardBuilder::Menu('system-overview')->Call(function(DashboardMenu $menu){
                    $menu->AddItem(
                        MenuItem::make(__('Languages'))->description(__('Translations and Languages'))->icon('globe')->route('dashboard.translations.languages.index')->icon('language')->permission('view languages')
                    );
                });
            });
            DashboardBuilder::Central(function(){
                DashboardBuilder::Menu('system-overview')->Call(function(DashboardMenu $menu){
                    $menu->AddItem(
                        MenuItem::make(__('Languages'))->description(__('Translations and Languages'))->icon('globe')->route('dashboard.translations.languages.index')->icon('language')->permission('view languages')
                    );
                });
            });
        }
    }

    /**
     * Register Module Middlewares
     *
     * @return void
     */
    public function registerMiddlewares()
    {
        $this->app['router']->pushMiddlewareToGroup('web', ApplyLocaleMiddleware::class);
    }

    /**
     * Register Module Commands
     *
     * @return void
     */
    protected function registerCommands()
    {
        $this->commands([
            ScanTranslationsCommand::class
        ]);
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->registerCommands();

        $this->app->register(RouteServiceProvider::class);
    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->publishes([
            module_path($this->moduleName, 'Config/config.php') => config_path($this->moduleNameLower . '.php'),
        ], 'config');
        $this->mergeConfigFrom(
            module_path($this->moduleName, 'Config/config.php'), $this->moduleNameLower
        );
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $viewPath = resource_path('views/modules/' . $this->moduleNameLower);

        $sourcePath = module_path($this->moduleName, 'Resources/views');

        $this->publishes([
            $sourcePath => $viewPath
        ], ['views', $this->moduleNameLower . '-module-views']);

        $this->loadViewsFrom(array_merge($this->getPublishableViewPaths(), [$sourcePath]), $this->moduleNameLower);
    }

    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $langPath = resource_path('lang/modules/' . $this->moduleNameLower);

        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, $this->moduleNameLower);
        } else {
            $this->loadTranslationsFrom(module_path($this->moduleName, 'Resources/lang'), $this->moduleNameLower);
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }

    private function getPublishableViewPaths(): array
    {
        $paths = [];
        foreach (\Config::get('view.paths') as $path) {
            if (is_dir($path . '/modules/' . $this->moduleNameLower)) {
                $paths[] = $path . '/modules/' . $this->moduleNameLower;
            }
        }
        return $paths;
    }
}
