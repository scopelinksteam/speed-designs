<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;
use Cascade\Translations\Http\Controllers\Dashboard\LanguagesController;
use Cascade\Translations\Http\Controllers\Dashboard\TranslationsController;

Route::get('/localize/{locale}', function($locale){
    session(['locale' => $locale]);
    if(auth()->check())
    {
        $user = auth()->user();
        $user->locale = $locale;
        $user->save();
    }
    return back();
})->name('translations.set_locale');


Route::hybrid(function(){

    if(translation_manager_enabled())
    {
        //Languages
        Route::prefix('languages')->as('languages.')->group(function(){
            Route::get('/', [LanguagesController::class, 'index'])->name('index')->middleware('can:view languages');
            Route::get('/edit/{language}', [LanguagesController::class, 'edit'])->name('edit')->middleware('can:edit languages');
            Route::get('/translate/{language}', [TranslationsController::class, 'translate'])->name('translate')->middleware('can:translate languages');
            Route::get('/build/{language}', [TranslationsController::class, 'build'])->name('build')->middleware('can:build languages');
            Route::post('/translate', [TranslationsController::class, 'store'])->name('translate.store')->middleware('can:translate languages');
            Route::post('/', [LanguagesController::class, 'store'])->name('store');
            Route::get('/delete/{language}', [LanguagesController::class, 'destroy'])->name('destroy');
        });
        Route::get('/scan', [TranslationsController::class, 'scan'])->name('scan')->middleware('can:scan languages');
    }

}, 'translations');
