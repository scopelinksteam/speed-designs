<?php

use Cascade\System\Entities\Currency;
use Illuminate\Support\Arr;
use Cascade\Translations\Entities\Language;
use Illuminate\Contracts\Container\BindingResolutionException;

function get_active_languages()
{
    $languages = Language::active()->get();
    if(count($languages) == 0)
    {
        $defaultLanguage = new Language();
        $defaultLanguage->name = __('English');
        $defaultLanguage->locale = 'en';
        return collect([
            $defaultLanguage
        ]);
    }
    return $languages;
}

if(!function_exists('get_active_currency'))
{
    function get_active_currency()
    {
        $currencyId = session('currency', 0);
        $currency = Currency::find($currencyId);
        if($currency == null)
        {
            $currency = Currency::first();
        }
        if($currency == null)
        {
            $currency = new Currency();
            $currency->name = fill_locales([], 'USD');
            $currency->code = 'USD';
            $currency->symbol = '$';
            $currency->rate = 1;
            $currency->save();
            session(['currency' => $currency->id]);
        }
        return $currency;
    }
}

if(!function_exists('currency'))
{
    function currency() 
    { 
        try
        {
            $cachedCurrency = app('active_currency');
            if($cachedCurrency) { return $cachedCurrency; }
        }
        catch(BindingResolutionException $ex)
        {
            $activeCurrency = get_active_currency(); 
            app()->singleton('active_currency', function() use($activeCurrency){
                return $activeCurrency;
            });
            return $activeCurrency;
        }
    }
}

if(!function_exists('currencies'))
{
    function currencies() { return Currency::all(); }
}

if(!function_exists('currency_exchange'))
{
    function currency_exchange($amount, $currency = null)
    {
        if($currency == null) { $currency = currency(); }
        if($currency == null) { return $amount; }
        return $amount * $currency->rate;
    }
}

function fill_locales($arr, $emptyValue = null)
{
    if(is_string($arr)) { $arr = [app()->getLocale() => $arr]; }
    $arr = $arr == null ? [] : Arr::where($arr, function($val, $key){
        if(is_array($val)) { return count($val) > 0; }
        return $val != null && trim($val) != '';
    });
    $vals = array_values($arr);
    $defaultValue = $emptyValue !== null ? $emptyValue : (count($arr) > 0 ? array_shift($vals) : '');

    $languages = get_active_languages();
    foreach($languages as $language)
    {
        if(!isset($arr[$language->locale]))
        {
            $arr[$language->locale] = $defaultValue;
        }
    }
    return $arr;
}

function active_language()
{
    $locale = app()->getLocale();
    $language = Language::where('locale', $locale)->first();
    if($language == null)
    {
        $language = new Language();
        $language->name = __('English');
        $language->locale = 'en';
        $language->enabled = true;
        $language->is_rtl = false;
    }
    return $language;
}


function translation_manager_enabled() { return env('TRANSLATION_MANAGER', true) || env('DEVELOPER_MODE', false); }
