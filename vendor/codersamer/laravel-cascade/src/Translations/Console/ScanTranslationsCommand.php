<?php

namespace Cascade\Translations\Console;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Cascade\Translations\Entities\Translation;
use Symfony\Component\Finder\Finder;

class ScanTranslationsCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'translations:scan';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Scan Translatable PHP and Javascript Strings and Build them Into a JSON Formatted File.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $path = base_path();
        $keys = array();
        $functions = array(
            'trans', 'trans_choice', 'Lang::get', 'Lang::choice', 'Lang::trans', 'Lang::transChoice', '@lang',
            '@choice', 'transEditable', '__'
        );
        $pattern =                              // See http://regexr.com/392hu
            //"[^\w|>]" .                          // Must not have an alphanum or _ or > before real method
            "(" . implode('|', $functions) . ")" .  // Must start with one of the functions
            "\(" .                               // Match opening parenthese
            "[\'\"]" .                           // Match " or '
            "(" .                                // Start a new group to match:
            "([^\1)]+)+" .                // Be followed by one or more items/keys
            ")" .                                // Close group
            "[\'\"]" .                           // Closing quote
            "[\),]";                            // Close parentheses or new parameter
        // Find all PHP + Twig files in the app folder, except for storage
        $finder = new Finder();
        $this->info('Finding PHP, Twig and Vue Files to Scan...');
        //$exclusions = ['storage','vendor','public','database', 'Modules', 'Plugins', 'Themes'];
        $exclusions = ['storage','vendor','public','database'];
        $systemPath = base_path('/vendor/codersamer/laravel-cascade');
        $finder->in([$path, $systemPath])->exclude($exclusions)->name('*.php')->name('*.blade.php')->name('*.twig')->name('*.vue')->files();
        $this->info("Scanning ( ".$finder->count()." ) Files ...");
        foreach ($finder as $file) {
            // Search the current file for the pattern
            if (preg_match_all("/$pattern/siU", $file->getContents(), $matches))
            { foreach ($matches[2] as $key) { $keys[] = $key; } }
        }
        // Remove duplicates
        $keys = array_unique($keys);
        $this->info('Scan completed, found ( ' . count($keys) . ' ) localized Strings!');
        // Add the translations to the database, if not existing.
        $all_string = Translation::select("string")->where("locale", "raw")->get()->pluck('string')->toArray();

        //Building Raw Translations
        $data = []; $now = now();
        if(count($keys) > 0)
        {
            $this->info('Building Raw Translations ...');
            foreach ($keys as $key) {
                // Split the group and item
                if (!in_array($key, $all_string)) {
                    $data[] = ['locale' => 'raw', 'string' => $key, 'created_at' => $now, 'updated_at' => $now];
                }
            }
        }

        //Saving Raw Strings if Any
        if(count($data) > 0)
        {
            $this->info('Saving ( ' . count($data) . ' ) Raw Translations to Database....');
            DB::table('translations')->insert($data);
            $this->info('( ' . count($data) . ' ) Raw Translations has been Saved to Database!');
            $this->GenerateDefaultJSON();
        }
    }

    /**
     * Generate Default Translations JSON
     */
    public function GenerateDefaultJSON()
    {
        $this->info('Generating Default Translations JSON');
        $path = module_path('translations', 'Resources/lang');
        $file = module_path('translations', 'Resources/lang/default.json');
        //Check if Writable Path
        if (!is_writable($path))  { $this->error('Directory Resources/lang is not Writable'); return; }
        if (file_exists($file) and !is_writable($file)) {
            $this->error('default.json File is not found or not Writable');
            return;
        }
        $this->info('Loading Raw Translations from Database');
        $query = Translation::select(['*'])->where('locale', 'raw');
        $json = [];
        $rows = $query->get();
        $this->info('Found ( '.count($rows).' ) Raw Translations, Building Formatted Json Translations...');
        if (!empty($rows)) {
            foreach ($rows as $row) { $json[$row['string']] = ''; }
        }
        $this->info('Writing JSON Translations ...');
        $stream = fopen($file, "w");
        fwrite($stream, json_encode($json,JSON_PRETTY_PRINT));
        fclose($stream);
        $this->info("Raw Translations is available at : ".$file);
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            //['example', InputArgument::REQUIRED, 'An example argument.'],
        ];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
            //['example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null],
        ];
    }
}
