<?php

namespace Cascade\Translations\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Cascade\Dashboard\Services\DashboardBuilder;
use Cascade\System\Services\System;
use Cascade\Translations\Entities\Language;

class ApplyLocaleMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if(auth()->check() && auth()->user()->locale != null)
        {
            session(['locale' => auth()->user()->locale]);
            app()->setLocale(auth()->user()->locale);
        }
        else if(session('locale', null) != null)
        {
            app()->setLocale(session('locale'));
        }
        else
        {
            $firstLanguage = Language::first();
            $overrideLocale = $firstLanguage == null ? app()->getLocale() : $firstLanguage->locale;
            $defaultLocale = $request->is('dashboard/*')
                ? get_setting('system::localization.languages.default', $overrideLocale)
                : get_setting('site::general.languages.default', $overrideLocale);
            app()->setLocale($defaultLocale);
        }

        if(app()->getLocale() == 'ar')
        { DashboardBuilder::SetRTL(); }
        System::DispatchAction('locale_set', active_language());
        return $next($request);
    }
}
