<?php

namespace Cascade\Translations\Http\Controllers\Dashboard;

use Cascade\System\Services\Feedback;
use Cascade\Translations\Entities\Language;
use Cascade\Translations\Entities\Translation;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;

class TranslationsController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function translate(Request $request, Language $language)
    {
        $translations = [];

        $query = DB::table('translations', 'RAW')->leftJoin('translations', 'RAW.id', '=', 'translations.parent_id');

        $query = $query->whereRaw("RAW.locale = 'raw' AND (COALESCE(translations.locale,'".$language->locale."') = '".$language->locale."')");
        if($request->has('mode'))
        {
            $mode = $request->mode;
            if($mode == 'translated')
            {
                $query = $query->whereRaw("COALESCE(translations.string, '') != ''");
            }
            else
            {
                $query = $query->whereRaw("COALESCE(translations.string, '') = ''");
            }
        }


        $query = $query->selectRaw('RAW.id AS raw_id,
        RAW.string AS original_string,
        COALESCE(translations.id, 0) AS translated_id,
        translations.string AS translated_string,
        translations.locale AS locale');

        //dd($query->toSql());
        $translations = $query->paginate(30);
        return view('translations::dashboard.languages.translate', [
            'language' => $language,
            'translations' => $translations
        ]);
    }

    public function build(Request $request, Language $language)
    {

        $query = DB::table('translations', 'RAW')->leftJoin('translations', 'RAW.id', '=', 'translations.parent_id');

        $query = $query->whereRaw("RAW.locale = 'raw' AND (COALESCE(translations.locale,'".$language->locale."') = '".$language->locale."')");

        $translations = $query = $query->selectRaw('RAW.id AS raw_id,
        RAW.string AS original_string,
        COALESCE(translations.id, 0) AS translated_id,
        translations.string AS translated_string,
        translations.locale AS locale')->get();
        $filtered = [];
        foreach($translations as $translation)
        {
            $filtered[$translation->original_string] = empty($translation->translated_string) ? $translation->original_string : $translation->translated_string;
        }
        $filtered = json_encode($filtered, JSON_PRETTY_PRINT);
        file_put_contents(base_path('resources/lang/'.$language->locale.'.json'), $filtered, FILE_USE_INCLUDE_PATH );
        Feedback::getInstance()->addSuccess(__('Language File Built Successfully'));
        Feedback::flash();
        return back();
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function scan()
    {
        Artisan::call('translations:scan');
        Feedback::getInstance()->addSuccess(__('System Scanned for Possible Translatable Content'));
        Feedback::flash();
        return back();
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        if($request->has('translations') && is_array($request->translations))
        {
            $language = Language::find($request->input('language_id', 0));
            foreach($request->translations as $rawId => $translation)
            {

                $item = Translation::where('parent_id', $rawId)->where('locale', $language->locale)->first();
                if($item == null && !empty($translation)) { $item = new Translation(); }
                if($item == null) { continue; }
                $item->parent_id = $rawId;
                $item->locale = $language->locale;
                $item->string = $translation ?? '';
                $item->save();
            }
            Feedback::getInstance()->addSuccess(__('Translations Saved Successfully, You can build Language to Apply New Translations'));
        }
        else
        {
            Feedback::getInstance()->addAlert(__('Not Translations Found, Skipped'));
        }
        Feedback::flash();
        return back();
    }
}
