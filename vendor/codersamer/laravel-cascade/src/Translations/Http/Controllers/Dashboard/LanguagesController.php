<?php

namespace Cascade\Translations\Http\Controllers\Dashboard;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Gate;
use Cascade\System\Services\Feedback;
use Cascade\Translations\Entities\Language;
use Cascade\Translations\Tables\LanguagesTable;

class LanguagesController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index(LanguagesTable $table)
    {
        return view('translations::dashboard.languages.index', [
            'table' => $table
        ]);
    }

    /**
     * Edit Existing Language.
     * @return Renderable
     */
    public function edit(Language $language, LanguagesTable $table)
    {
        return $this->index($table)->with([
            'language' => $language
        ]);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        $languageId = intval($request->input('entity_id', 0));
        if($languageId == 0) { Gate::authorize('create languages'); }
        else { Gate::authorize('edit languages'); }

        $valid = true;
        if(!$request->filled('name'))
        {
            Feedback::getInstance()->addError(__('Language Name Cannot be Empty'));
            $valid = false;
        }

        if(!$request->filled('locale'))
        {
            Feedback::getInstance()->addError(__('Language Locale Cannot be Empty'));
            $valid = false;
        }

        if(!$valid)
        {
            Feedback::flash();
            return back();
        }

        $language = $languageId > 0 ? Language::find($languageId) : new Language();

        //Set Required Attributes
        $language->name = $request->name;
        $language->locale = strtolower($request->locale);
        $language->is_rtl = $request->direction;
        $language->enabled = $request->status;

        //Check for Flag
        if($request->file('flag'))
        {
            $language->flag = $request->file('flag')->store('languages', ['disk' => 'uploads']);
        }
        $language->save();

        Feedback::getInstance()->addSuccess(__('Language Saved Successfully'));
        Feedback::flash();
        return redirect()->route('dashboard.translations.languages.index');
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy(Language $language)
    {
        Gate::authorize('delete languages');
        $language->delete();
        Feedback::success(__('Language Deleted Successfully'));
        Feedback::flush();
        return to_route('dashboard.translations.languages.index');
    }
}
