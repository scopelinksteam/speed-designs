<?php

namespace Cascade\Translations\Tables;

use Cascade\Dashboard\Services\Datatables\Action;
use Cascade\Dashboard\Services\Datatables\Column;
use Cascade\Dashboard\Services\Datatables\DataTable;
use Cascade\Dashboard\Services\Datatables\Filter;
use Cascade\Translations\Entities\Language;
use Illuminate\Support\Facades\Gate;

class LanguagesTable extends DataTable
{


    public function __construct($params = null)
    {
        $this->editColumn('name', function($language){
            return $language->name.' ( '.$language->locale.' ) ';
        })
        ->editColumn('progress', function($language){
            return $this->progress($language->progress, 0, 100, $language->translations()->count() .' / '.$language->master_count);
        })
        ->editColumn('direction', function($language){
            return $language->is_rtl ? __('RTL') : __('LTR');
        })
        ->editColumn('status', function($language){
            return $language->enabled ? __('Active') : __('Disabled');
        })
        ->editColumn('flag', function($language){
            if($language->flag == null) { return __('NA') ; }
            return '<img src="'.uploads_url($language->flag).'" style="max-width:32px;" />';
        })
        ;
    }

    public function query($search = null, $filters = null)
    {
        $query = Language::query();
        if(!empty($search))
        {
            $query = $query->where(function($query) use($search) {
                return $query->where('name', 'LIKE', '%'.trim($search).'%')
                ->orWhere('locale', 'LIKE', '%'.trim($search).'%');
            });
        }
        if(isset($filters['direction']) && $filters['direction'] != '')
        {
            $query = $query->where('is_rtl', intval($filters['direction']));
        }
        if(isset($filters['status']) && $filters['status'] != '')
        {
            $query = $query->where('enabled', intval($filters['status']));
        }
        return $query;
    }

    public function columns()
    {
        return [
            //Column::make('id')->text(__('ID'))->visible(false),
            Column::make('name')->text(__('Name')),
            Column::make('flag')->text(__('Flag')),
            Column::make('progress')->text(__('Progress')),
            Column::make('direction')->text(__('Direction')),
            Column::make('status')->text(__('Status')),
        ];
    }

    public function actions()
    {
        return [
            Action::make('translate', __('Translate'))->route('dashboard.translations.languages.translate', ['language' => 'id'])->permission('translate languages'),
            Action::make('build', __('Build'))->route('dashboard.translations.languages.build', ['language' => 'id'])->permission('build languages'),
            Action::make('edit', __('Edit'))->route('dashboard.translations.languages.edit', ['language' => 'id'])->permission('edit languages'),
            Action::make('delete', __('Delete'))->route('dashboard.translations.languages.destroy', ['language' => 'id'])->permission('delete languages'),
        ];
    }

    public function filters()
    {
        return [
            Filter::make(Filter::FILTER_TYPE_SELECT, 'direction')
            ->title(__('Filter By Direction'))
            ->values(function(){
                return ['0' => __('LTR'), '1' => __('RTL')];
            }),
            Filter::make(Filter::FILTER_TYPE_SELECT, 'status')
            ->title(__('Filter By Status'))
            ->values(function(){
                return ['1' => __('Enabled'), '0' => __('Disabled')];
            })
        ];
    }
}
