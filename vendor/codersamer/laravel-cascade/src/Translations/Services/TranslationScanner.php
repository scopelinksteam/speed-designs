<?php

namespace Cascade\Translations\Services;

use Cascade\Translations\Entities\Translation;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Symfony\Component\Finder\Finder;

class TranslationScanner
{

    protected $finder = null;

    protected $path = '';

    protected $excludes = ['storage','vendor','public','database', 'node_modules'];

    protected $extensions = ['*.php', '*.twig', '*.blade.php', '*.vue'];

    protected $translatable = [];


    public function in($path)
    {
        $this->path = $path;
        return $this;
    }

    public function exclude($path)
    {
        if(is_array($path)) { $this->excludes = array_merge($this->excludes, $path); }
        else { $this->excludes[] = $path;}
        return $this;
    }

    public function extension($extension)
    {
        if(is_array($extension)) { $this->extensions = array_merge($this->extensions, $extension); }
        else { $this->extensions[] = $extension;}
        return $this;
    }



    protected static function GetFunctions()
    {
        return ['trans', 'trans_choice', 'Lang::get', 'Lang::choice', 'Lang::trans', 'Lang::transChoice', '@lang', '@choice', 'transEditable', '__'];
    }

    protected static function GetPattern()
    {
        return //"[^\w|>]" .                          // Must not have an alphanum or _ or > before real method
                "(" . implode('|', self::GetFunctions()) . ")" .  // Must start with one of the functions
                "\(" .                               // Match opening parenthese
                "[\'\"]" .                           // Match " or '
                "(" .                                // Start a new group to match:
                "([^\1)]+)+" .                // Be followed by one or more items/keys
                ")" .                                // Close group
                "[\'\"]" .                           // Closing quote
                "[\),]";
    }

    public  function Scan()
    {
        $pattern = self::GetPattern();
        // Find all PHP + Twig files in the app folder, except for storage
        $finder = new Finder();
        //Finding PHP, Twig and Vue Files to Scan...
        $finder = $finder->in($this->path)->exclude($this->excludes);
        foreach($this->extensions as $extension) { $finder = $finder->name($extension); }
        //Scanning X Files
        foreach ($finder->files() as $file) {
            // Search the current file for the pattern
            if (preg_match_all("/$pattern/siU", $file->getContents(), $matches))
            { foreach ($matches[2] as $key) { $this->translatable[] = $key; } }
        }
        // Remove duplicates
        $this->translatable = array_unique($this->translatable);

        return $this;
    }

    public function CommitToDatabase()
    {
        // Add the translations to the database, if not existing.
        $existItems = Translation::select("string")->where("locale", "raw")->get()->pluck('string')->toArray();
        //Building Raw Translations
        $data = [];
        $now = now();
        if(count(self::$translatable) > 0)
        {
            foreach (self::$translatable as $key) {
                // Split the group and item
                if (!in_array($key, $existItems)) {
                    $data[] = ['locale' => 'raw', 'string' => $key, 'created_at' => $now, 'updated_at' => $now];
                }
            }
        }
        //Saving Raw Strings if Any
        if(count($data) > 0)
        {
            //Insert into Database
            DB::table('translations')->insert($data);
        }

        return $this;
    }

    public function SaveDefaultJson($destination, $forceCreate = true)
    {
        $destinationPath = rtrim($destination, '/');

        if(!is_dir($destinationPath))
        {
            if($forceCreate) { File::ensureDirectoryExists($destinationPath); }
            else { return false; }
        }

        $filePath = rtrim($destination,'/').'/default.json';

        if (!is_writable($destinationPath))  { return false; }

        if (file_exists($filePath) and !is_writable($filePath)) { return false; }

        $json = [];
        foreach($this->translatable as $item) { $json[$item] = ''; }

        $stream = fopen($filePath, "w");
        fwrite($stream, json_encode($json,JSON_PRETTY_PRINT));
        fclose($stream);
        return true;
    }

    public function SaveLocalizedJson($destination, $forceCreate = true)
    {
        $destinationPath = rtrim($destination, '/');

        if(!is_dir($destinationPath))
        {
            if($forceCreate) { File::ensureDirectoryExists($destinationPath); }
            else { return false; }
        }

        foreach(get_active_languages() as $language)
        {
            $filePath = rtrim($destination,'/').'/'.$language->locale.'.json';

            if (!is_writable($destinationPath))  { return false; }

            if (file_exists($filePath) and !is_writable($filePath)) { return false; }

            $existTranslations = [];

            if(file_exists($filePath)) { $existTranslations = json_decode(file_get_contents($filePath), true);}

            $json = [];
            foreach($this->translatable as $item) { $json[$item] = isset($existTranslations[$item]) ? $existTranslations[$item] : ''; }

            $stream = fopen($filePath, "w");
            fwrite($stream, json_encode($json,JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT));
            fclose($stream);
        }
    }


}
