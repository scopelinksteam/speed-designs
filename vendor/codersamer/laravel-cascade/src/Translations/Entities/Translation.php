<?php

namespace Cascade\Translations\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Translation extends Model
{
    public function scopeRaw($query)
    {
        return $query->where('locale', 'raw');
    }

    public function parent()
    {
        return $this->belongsTo(Translation::class, 'parent_id', 'id');
    }

    public function getOriginalAttribute()
    {
        return ($this->parent) ? $this->parent->string : $this->string;
    }

    public function getTranslationAttribute()
    {
        return $this->locale == 'raw' ? '' : $this->string;
    }

    public function getIsRawAttribute()
    {
        return $this->locale == 'raw' && !$this->translate_locale;
    }
}
