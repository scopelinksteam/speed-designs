<?php

namespace Cascade\Translations\Entities;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Language extends Model
{
    protected $casts = [
        'enabled'   => 'boolean',
        'is_rtl'    => 'boolean'
    ];

    public function translations()
    {
        return $this->hasMany(Translation::class, 'locale', 'locale');
    }

    public function getMasterCountAttribute()
    {
        return Translation::where('locale', 'raw')->count();
    }

    public function getProgressAttribute()
    {
        $langTranslationsCount = $this->translations()->count();
        $progress = ($langTranslationsCount == 0) ? 0 : (int)($langTranslationsCount / $this->master_count * 100);
        return $progress;
    }

    public function scopeActive(Builder $query)
    {
        return $query->where('enabled', true);
    }
}
