<?php

namespace Cascade\Admin\Components;

use Illuminate\View\Component;

class AccordionItemComponent extends Component
{
    public $active;

    public function __construct($active = false)
    {
        $this->active = $active;
    }

    public function render()
    {
        return view('admin::components.accordion_item');
    }
}