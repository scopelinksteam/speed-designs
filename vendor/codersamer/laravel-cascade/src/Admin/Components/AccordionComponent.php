<?php

namespace Cascade\Admin\Components;

use Illuminate\View\Component;

class AccordionComponent extends Component
{
    public function render()
    {
        return view('admin::components.accordion');
    }
}