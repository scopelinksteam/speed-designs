<?php

namespace Cascade\Admin\Components;

use Illuminate\View\Component;

class InfoBoxComponent extends Component
{

    public $title;

    public $subtitle = '';

    public $value = 0;

    public $icon = '';

    public $theme = 'info';

    public function __construct($title = '', $value = 0, $subtitle = '', $icon = '', $theme = 'info')
    {
        $this->title = $title;
        $this->subtitle = $subtitle;
        $this->value = $value;
        $this->icon = $icon;
        $this->theme = $theme;
    }

    public function render()
    {
        return view('admin::components.infobox');
    }
}
