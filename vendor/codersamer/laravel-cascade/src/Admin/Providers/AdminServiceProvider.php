<?php

namespace Cascade\Admin\Providers;

use Cascade\Admin\Components\AccordionComponent;
use Cascade\Admin\Components\AccordionItemComponent;
use Cascade\Dashboard\Entities\MenuItem;
use Cascade\Dashboard\Services\DashboardBuilder;
use Cascade\Dashboard\Services\DashboardMenu;
use Cascade\System\Services\System;
use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Blade;

class AdminServiceProvider extends ServiceProvider
{
    /**
     * @var string $moduleName
     */
    protected $moduleName = 'Admin';

    /**
     * @var string $moduleNameLower
     */
    protected $moduleNameLower = 'admin';

    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerTranslations();
        $this->registerConfig();
        $this->registerViews();
        Paginator::useBootstrapFive();
        $this->loadMigrationsFrom(module_path($this->moduleName, 'Database/Migrations'));
        //System::OverrideView('adminlte::layouts.master', 'admin::layouts.master');
        //System::OverrideView('adminlte::layouts.base', 'admin::layouts.base');

        if(!$this->app->runningInConsole())
        {
            DashboardBuilder::Menu('quick-access')->view('admin::partials.quick-access');
            DashboardBuilder::MainMenu()->view('admin::partials.sidebar');
            DashboardBuilder::Central(function(){
                DashboardBuilder::MainMenu()->view('admin::partials.sidebar');
            });
            DashboardBuilder::Menu('navbar')->view('admin::partials.navbar-menu');
            DashboardBuilder::Menu('footer')->view('admin::partials.footer-menu');
            $supportLink = current_tenant() == null || current_tenant()->GetOption('support_link', null) == null ? null : current_tenant()->GetOption('support_link');
            if($supportLink == null)
            {
                $supportLink = config('cascade.multi_tenancy', true) ? 'https://bluewaves.website' : null;
            }
            if($supportLink != null)
            { DashboardBuilder::Menu('footer')->AddItem(MenuItem::make(__('Support'))->link($supportLink)); }
        }
        Blade::component(InfoBoxComponent::class, 'adminlte-info-box');
        Blade::component(InfoBoxComponent::class, 'admin-info-box');
        Blade::component(AccordionComponent::class, 'admin-accordion');
        Blade::component(AccordionItemComponent::class, 'admin-accordion-item');
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->publishes([
            module_path($this->moduleName, 'Config/config.php') => config_path($this->moduleNameLower . '.php'),
        ], 'config');
        $this->mergeConfigFrom(
            module_path($this->moduleName, 'Config/config.php'), $this->moduleNameLower
        );
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $viewPath = resource_path('views/modules/' . $this->moduleNameLower);

        $sourcePath = module_path($this->moduleName, 'Resources/views');

        $this->publishes([
            $sourcePath => $viewPath
        ], ['views', $this->moduleNameLower . '-module-views']);

        $this->loadViewsFrom(array_merge($this->getPublishableViewPaths(), [$sourcePath]), $this->moduleNameLower);
    }

    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $langPath = resource_path('lang/modules/' . $this->moduleNameLower);

        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, $this->moduleNameLower);
        } else {
            $this->loadTranslationsFrom(module_path($this->moduleName, 'Resources/lang'), $this->moduleNameLower);
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }

    private function getPublishableViewPaths(): array
    {
        $paths = [];
        foreach (\Config::get('view.paths') as $path) {
            if (is_dir($path . '/modules/' . $this->moduleNameLower)) {
                $paths[] = $path . '/modules/' . $this->moduleNameLower;
            }
        }
        return $paths;
    }
}
