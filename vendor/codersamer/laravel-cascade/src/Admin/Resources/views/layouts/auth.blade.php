<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>@lang('Authentication')</title>

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ cascade_asset('plugins/fontawesome/css/all.min.css') }}">
  <!-- Bootstrap -->

  <link rel="stylesheet" href="{{ cascade_asset('plugins/bootstrap/css/bootstrap'.(app()->getLocale() == 'ar' ? '.rtl' : '').'.min.css') }}">
  <link rel="stylesheet" href="{{ url('admin') }}/css/auth.css">
  @livewireStyles()
  {{dashboard_render_styles()}}
  @stack('styles')
</head>
<body>
<div class="container-fluid h-full">
    <div class="row h-full">
        <div class="col-lg-8 col-md-4 d-md-block d-sm-none px-0" id="image-holder" style="background-image: url('{{ get_setting('system::login_page.background', null) != null ? uploads_url(get_setting('system::login_page.background')) : asset('cascade/images/auth_image.jpg') }}'); background-">
            <div id="splash" class="bg-dark">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-2">
                            <img id="logo" src="{{ get_setting('system::general.logo', null) == null ?  url('cascade/images/logo_small.png') : uploads_url(get_setting('system::general.logo', null))}}" alt="">
                        </div>
                        <div class="col-8 p-3">
                            <p class="mb-0" style="text-transform: uppercase;"><b>@lang('Authentication Area')</b></p>
                            <h1 class="mt-1">{{ get_setting('system::general.app_name', 'Cascade') }}</h1>
                            <p>{{ get_setting('system::general.app_description', __('Powerful Site Building System Built on Laravel 9 and PHP 8')) }}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-8 p-4 bg-light d-flex flex-column align-items-center justify-content-center">
           <p class="auth-icon"><i class="fa fa-lock text-dark"></i></p>
            <div class="card w-100 p-4">
                <div class="card-body">
                    @yield('content')
                </div>
            </div>

        </div>
    </div>
</div>
@livewireScripts()
@livewire('livewiremodal')
{{dashboard_render_scripts()}}
@stack('scripts')
</body>
</html>
