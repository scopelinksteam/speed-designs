<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}" @if(app()->getLocale() == 'ar') dir="rtl" @endif>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Responsive Admin &amp; Dashboard Template based on Bootstrap 5">
    <meta name="author" content="AdminKit">
    <meta name="keywords"
        content="adminkit, bootstrap, bootstrap 5, admin, dashboard, template, responsive, css, sass, html, theme, front-end, ui kit, web">

    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link rel="shortcut icon" href="{{ url('admin') }}/img/icons/icon-48x48.png" />

    <link rel="canonical" href="{{url('/')}}" />

    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@lang('Dashboard') @hasSection('page-title') | @endif @yield('page-title')</title>

    @if(active_language()->is_rtl)
    <link href="{{ url('admin') }}/css/app.rtl.css" rel="stylesheet">
    @else
    <link href="{{ url('admin') }}/css/app.css" rel="stylesheet">
    @endif
    <link href="{{ url('admin') }}/css/custom.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;600&display=swap" rel="stylesheet">
    @livewireStyles()
    {{dashboard_render_styles()}}
    @stack('styles')
</head>

<body>
    <div class="wrapper">
        <x-dashboard-menu name="main" />


        <div class="main">

            @include('admin::partials.navbar')

            <main class="content">
                @yield('header')
                <div class="container-fluid p-0">
                    <div class="row">
                        <div class="col-12">
                          {!! \Cascade\System\Services\Feedback::display() !!}
                          @php \Cascade\System\Services\Feedback::flush() @endphp
                        </div>
                    </div>
                </div>
                @yield('content')
            </main>

            @include('admin::partials.footer')
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.6/dist/umd/popper.min.js"></script>
    <script src="{{ url('admin') }}/js/app.js"></script>
    <script src="{{ url('admin') }}/js/custom.js"></script>
    <script type="text/javascript">
        document.addEventListener('DOMContentLoaded', function(){
            const popoverTriggerList = document.querySelectorAll('[data-bs-toggle="popover"]')
            const popoverList = [...popoverTriggerList].map(popoverTriggerEl => new bootstrap.Popover(popoverTriggerEl))
        });
    </script>
    @livewireScripts()
    @livewire('livewiremodal')
    {{dashboard_render_scripts()}}
    @stack('scripts')
</body>

</html>
