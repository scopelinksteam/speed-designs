<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}" @if(app()->getLocale() == 'ar') dir="rtl" @endif>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>@lang('Dashboard') @hasSection('page-title') | @endif @yield('page-title')</title>

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ url('adminlte') }}/dist/css/adminlte.min{{ app()->getLocale() == 'ar' ? '.rtl' : '' }}.css">
  <!-- Custom Styles -->
  <link rel="stylesheet" href="{{ url('adminlte') }}/dist/css/custom.css">
  @livewireStyles()
  {{dashboard_render_styles()}}
  @stack('styles')
</head>
<body class="hold-transition sidebar-mini sidebar-collapse" style="height: 100vh !important;">


    @yield('content')


<!-- REQUIRED SCRIPTS -->

<!-- jQuery -->
<script src="{{ url('adminlte') }}/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="{{ url('adminlte') }}/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- AdminLTE App -->
<script src="{{ url('adminlte') }}/dist/js/adminlte.min.js"></script>
@livewireScripts()
{{dashboard_render_scripts()}}
@stack('scripts')
</body>
</html>
