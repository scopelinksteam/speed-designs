@extends('admin::layouts.base')
@section('header')
<div class="container-fluid p-0">
    <div class="row justify-content-between">
        <div class="col-auto">
            <h1 class="h3 mb-3">@yield('page-title')</h1>
            <p>@yield('page-description')</p>
        </div>
        <div class="col-auto">
            @yield('page-actions')
        </div>
    </div>
</div>
@endsection
