<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}" @if(app()->getLocale() == 'ar') dir="rtl" @endif>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>@lang('Dashboard') @hasSection('page-title') | @endif @yield('page-title')</title>

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ url('adminlte') }}/dist/css/adminlte.min{{ app()->getLocale() == 'ar' ? '.rtl' : '' }}.css">
  <!-- Custom Styles -->
  <link rel="stylesheet" href="{{ url('adminlte') }}/dist/css/custom.css">

  @livewireStyles()
  {{dashboard_render_styles()}}
  @stack('styles')
</head>
<body class="hold-transition sidebar-mini sidebar-collapse">
<div class="wrapper">
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <x-dashboard-menu name="navbar" />

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
        <x-dashboard-navbar-actions />
        @if(count(dashboard_widgets('quick_access')) > 0)
        <li class="nav-item">
            <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#" role="button">
            <i class="fas fa-th-large"></i>
            </a>
        </li>
        @endif
    </ul>
  </nav>
  <!-- /.navbar -->
  <!-- Main Sidebar Container -->
  <aside class="main-sidebar elevation-1 sidebar-{{ get_setting('system::adminlte.sidebar_theme', 'dark') }}-{{ get_setting('system::adminlte.sidebar_color', 'primary') }} {{-- elevation-4 --}}">
    <!-- Brand Logo -->
    <a href="{{ is_central() ? route('central.index') : route('dashboard.index') }}" class="brand-link">
      <img src="{{ get_setting('system::general.logo', null) == null ?  url('cascade/images/logo_small.png') : url('uploads/'.get_setting('system::general.logo', null))}}" alt="" class="brand-image img-circle {{ config('adminlte.logo_elevation', true) ? 'elevation-3' : '' }}" style="opacity: .8">
      <span class="brand-text font-weight-light">{{ get_setting('system::general.app_name', __('Cascade')) }}</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="{{ url(auth()->user()->avatar) }}" class="img-circle {{-- elevation-2 --}}" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block">{{ auth()->user()->name }}</a>
        </div>
      </div>

      <!-- SidebarSearch Form -->
      <div class="form-inline">
        <div class="input-group" data-widget="sidebar-search">
          <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
          <div class="input-group-append">
            <button class="btn btn-sidebar">
              <i class="fas fa-search fa-fw"></i>
            </button>
          </div>
        </div>
      </div>

      <x-dashboard-menu name="main" />
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    @yield('header')
    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
          <div class="row">
              <div class="col-12">
                {!! \Cascade\System\Services\Feedback::display() !!}
                @php \Cascade\System\Services\Feedback::flush() @endphp
              </div>
          </div>
        @yield('content')
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  @if(count(dashboard_widgets('quick_access')) > 0)
  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark" style="overflow-y:scroll">
    <!-- Control sidebar content goes here -->
    <div class="p-3">
      <x-dashboard-widgets location="quick_access" :allow-columns="false" />
    </div>
  </aside>
  <!-- /.control-sidebar -->
  @endif
  <!-- Main Footer -->
  <footer class="main-footer">
    <!-- To the right -->
    <div class="float-right d-none d-sm-inline">
      Cascade Version 1.5
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; 2020-{{ now()->format('Y') }} <a href="https://cascade.io">Cascade</a>.</strong> All rights reserved.
  </footer>
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->

<!-- jQuery -->
<script src="{{ url('adminlte') }}/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="{{ url('adminlte') }}/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- AdminLTE App -->
<script src="{{ url('adminlte') }}/dist/js/adminlte.min.js"></script>


@livewireScripts()
{{dashboard_render_scripts()}}
@livewire('livewiremodal')
@stack('scripts')
</body>
</html>
