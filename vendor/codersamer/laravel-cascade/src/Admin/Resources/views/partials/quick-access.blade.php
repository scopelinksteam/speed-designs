@isset($menu)

@foreach ($menu->GetRenderItems() as $section)
<div class="row">
@foreach ($section->GetItems() as $item)
@if(!$item->CanShow() || !$item->HasContents()) @continue @endif
<div class="col-md-4">
    <div class="card">
        <div class="card-body">
            <a href="{{ $item->GetLink() }}" class="text-dark text-decoration-none">
                <i class="fs-1 fas fa-{{ $item->GetIcon() }} w-100 text-center" ></i>
                <br />
                <p class="mb-0 mt-2 fw-bold text-center">{{ $item->GetTitle() }}</p>
            </a>
        </div>
    </div>
</div>
@endforeach
</div>
@endforeach
@endisset