<footer class="footer">
    <div class="container-fluid">
        <div class="row text-muted">
            <div class="col-6 text-start">
                <p class="mb-0">
                    {!! get_setting('system::admin.copyrights', '<a class="text-muted" href="https://bluewaves.site/"
                    target="_blank"><strong>Cascade</strong></a> - Developed By<a class="text-muted"
                    href="https://bluewaves.site/" target="_blank"><strong> BlueWaves</strong></a>') !!}
                </p>
            </div>
            <div class="col-6 text-end">
                <x-dashboard-menu name="footer" />

            </div>
        </div>
    </div>
</footer>
