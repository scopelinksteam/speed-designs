<nav id="sidebar" class="sidebar js-sidebar">
    <div class="sidebar-content js-simplebar">
        <a class="sidebar-brand d-flex align-items-center" href="{{ is_central() ? route('central.index') : route('dashboard.index')}}">
            {{--
            @if(!empty(get_setting('system::general.logo', '')))
            <div class="col-auto me-3">
                <img src="{{uploads_url(get_setting('system::general.logo', ''))}}" class="rounded" alt="" style="width: 40px;">
            </div>
            @endif
            --}}
            <div class="col">
                <span class="align-middle">{{get_setting('system::general.app_name')}}</span>
            </div>
        </a>
        <div class="sidebar-user px-4">
            <div class="d-flex justify-content-center">
                <div class="flex-shrink-0">
                    <img src="{{auth()->user()->avatar}}" class="avatar img-fluid rounded me-1" alt="{{auth()->user()->name}}">
                </div>
                <div class="flex-grow-1 ps-2">
                    <a class="sidebar-user-title text-white dropdown-toggle" href="#" data-bs-toggle="dropdown">
                        {{auth()->user()->name}}
                    </a>
                    <div class="dropdown-menu dropdown-menu-start">
                        <a class="dropdown-item" href="{{route('dashboard.users.profile')}}">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-user align-middle me-1">
                                <path d="M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2"></path>
                                <circle cx="12" cy="7" r="4"></circle>
                            </svg>
                            @lang('Profile')
                        </a>
                        <!--
                        <a class="dropdown-item" href="#"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-pie-chart align-middle me-1"><path d="M21.21 15.89A10 10 0 1 1 8 2.83"></path><path d="M22 12A10 10 0 0 0 12 2v10z"></path></svg> Analytics</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="pages-settings.html"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-settings align-middle me-1"><circle cx="12" cy="12" r="3"></circle><path d="M19.4 15a1.65 1.65 0 0 0 .33 1.82l.06.06a2 2 0 0 1 0 2.83 2 2 0 0 1-2.83 0l-.06-.06a1.65 1.65 0 0 0-1.82-.33 1.65 1.65 0 0 0-1 1.51V21a2 2 0 0 1-2 2 2 2 0 0 1-2-2v-.09A1.65 1.65 0 0 0 9 19.4a1.65 1.65 0 0 0-1.82.33l-.06.06a2 2 0 0 1-2.83 0 2 2 0 0 1 0-2.83l.06-.06a1.65 1.65 0 0 0 .33-1.82 1.65 1.65 0 0 0-1.51-1H3a2 2 0 0 1-2-2 2 2 0 0 1 2-2h.09A1.65 1.65 0 0 0 4.6 9a1.65 1.65 0 0 0-.33-1.82l-.06-.06a2 2 0 0 1 0-2.83 2 2 0 0 1 2.83 0l.06.06a1.65 1.65 0 0 0 1.82.33H9a1.65 1.65 0 0 0 1-1.51V3a2 2 0 0 1 2-2 2 2 0 0 1 2 2v.09a1.65 1.65 0 0 0 1 1.51 1.65 1.65 0 0 0 1.82-.33l.06-.06a2 2 0 0 1 2.83 0 2 2 0 0 1 0 2.83l-.06.06a1.65 1.65 0 0 0-.33 1.82V9a1.65 1.65 0 0 0 1.51 1H21a2 2 0 0 1 2 2 2 2 0 0 1-2 2h-.09a1.65 1.65 0 0 0-1.51 1z"></path></svg> Settings &amp;
                            Privacy</a>
                        <a class="dropdown-item" href="#"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-help-circle align-middle me-1"><circle cx="12" cy="12" r="10"></circle><path d="M9.09 9a3 3 0 0 1 5.83 1c0 2-3 3-3 3"></path><line x1="12" y1="17" x2="12.01" y2="17"></line></svg> Help Center</a>
                        -->
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="{{route('logout')}}">@lang('Log out')</a>
                    </div>

                    <div class="sidebar-user-subtitle text-light small">
                        @if(auth()->user()->is_super_admin)
                        @lang('Super Admin')
                        @else
                        {{implode(', ', auth()->user()->roles()->pluck('name')->toArray())}}
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <ul class="sidebar-nav">
            @if(isset($menu) && $menu != null)
            @foreach ($menu->GetRenderItems() as $section)
            @if(!$section->HasItems()) @continue @endif
            <li class="sidebar-header">{{$section->GetTitle()}}</li>
            @foreach ($section->GetItems() as $item)
            @if(!$item->CanShow() || !$item->HasContents()) @continue @endif
            <li class="sidebar-item @if($item->IsActive()) active @endif" id="{{$item->GetId()}}">
                <a class="sidebar-link @if(!$item->IsActive()) collapsed @endif" @if(!$item->HasChildren()) href="{{ $item->GetLink() }}" @endif @if($item->HasChildren()) data-bs-toggle="collapse" data-bs-target="#{{$item->GetId()}}-items" @endif>
                    <i class="align-middle fas fa-{{ $item->GetIcon() }}" ></i> <span
                    class="align-middle">{{ $item->GetTitle() }}</span>
                    @if($item->HasBadge())
                    <small class="badge bg-{{$item->GetBadgeType()}} float-end @if($item->HasChildren()) me-3 @endif mt-2 small " style="font-size: 65%;">{{$item->GetBadge()}}
                    </small>
                    @endif
                </a>
                @if($item->HasChildren())
                <ul id="{{$item->GetId()}}-items" class="sidebar-dropdown list-unstyled collapse @if($item->IsActive()) show @endif" data-bs-parent="#sidebar" style="">
                    @foreach ($item->GetChildren() as $child)
                    @if(!$child->CanShow()) @continue @endif
                    <li class="sidebar-item @if($child->IsActive()) active @endif" id="{{$child->GetId()}}">
                        <a class="sidebar-link collapsed" @if(!$child->HasChildren()) href="{{ $child->GetLink() }}" @endif @if($child->HasChildren()) data-bs-toggle="collapse" data-bs-target="{{$child->GetLink()}}" @endif>
                            <i class="fas fa-{{ $child->GetIcon() }}"></i>
                            {{ $child->GetTitle() }}
                        </a>
                        @if($child->HasChildren())
                        <ul id="{{str_replace('#','',$child->GetLink())}}" class="sidebar-dropdown list-unstyled collapse @if($child->IsActive()) show @endif" data-bs-parent="{{$child->GetLink()}}" style="">
                            @foreach ($child->GetChildren() as $subChild)
                            <li class="sidebar-item @if($subChild->IsActive()) active @endif" id="{{$subChild->GetId()}}">
                                <a class="sidebar-link collapsed" href="{{ $subChild->GetLink() }}">
                                    <i class="fas fa-{{ $subChild->GetIcon() }}"></i>
                                    {{ $subChild->GetTitle() }}
                                </a>
                            </li>
                            @endforeach
                        </ul>
                        @endif
                    </li>
                    @endforeach
                </ul>
                @endif
            </li>
            @endforeach
            @endforeach
            @endif
        </ul>
    </div>
</nav>
