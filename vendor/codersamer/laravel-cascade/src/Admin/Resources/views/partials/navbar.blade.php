<nav class="navbar navbar-expand navbar-light navbar-bg">

    <a class="sidebar-toggle js-sidebar-toggle">
        <i class="hamburger align-self-center"></i>
    </a>
    @if(!empty(get_setting('system::general.logo', '')))
    <div class="col-auto me-3">
        <img src="{{uploads_url(get_setting('system::general.logo', ''))}}" class="rounded" alt="" style="width: 40px;">
    </div>
    @endif
    <x-dashboard-menu name="navbar" />
    <div class="navbar-collapse collapse">
        <ul class="navbar-nav navbar-align">
            <li class="nav-item dropdown">
                <a class="nav-icon dropdown-toggle" href="#" id="alertsDropdown"
                    data-bs-toggle="dropdown">
                    <div class="position-relative">
                        <i class="align-middle" data-feather="bell"></i>
                        <span class="indicator"> {{auth()->user()->unreadNotifications()->count() }}</span>
                    </div>
                </a>
                <div class="dropdown-menu dropdown-menu-lg dropdown-menu-end py-0"
                    aria-labelledby="alertsDropdown">
                    <div class="dropdown-menu-header">
                        {{auth()->user()->unreadNotifications()->count() }} @lang('New Notifications')
                    </div>
                    <div class="list-group">
                        @foreach (auth()->user()->unreadNotifications as $notification)
                        @if(!$notification->builder) @continue @endif
                        <a href="{{$notification->builder->link()}}" class="list-group-item">
                            <div class="row g-0 align-items-center">
                                <div class="col-2">
                                    {!! $notification->builder->icon() !!}
                                </div>
                                <div class="col-10">
                                    <div class="text-dark">{{$notification->builder->title()}}</div>
                                    <div class="text-muted small mt-1">{!!$notification->builder->render()!!}</div>
                                    <div class="text-muted small mt-1">{{$notification->created_at->diffForHumans()}}</div>
                                </div>
                            </div>
                        </a>
                        @endforeach
                    </div>
                    <div class="dropdown-menu-footer">
                        <a href="#" class="text-muted">@lang('Show all notifications')</a>
                    </div>
                </div>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-icon dropdown-toggle" href="#" id="messagesDropdown"
                    data-bs-toggle="dropdown">
                    <div class="position-relative">
                        <i class="align-middle" data-feather="message-square"></i>
                    </div>
                </a>
                <div class="dropdown-menu dropdown-menu-lg dropdown-menu-end py-0"
                    aria-labelledby="messagesDropdown">
                    <div class="dropdown-menu-header">
                        <div class="position-relative">
                            0 New Messages
                        </div>
                    </div>
                    <div class="list-group">
                        <!--
                        <a href="#" class="list-group-item">
                            <div class="row g-0 align-items-center">
                                <div class="col-2">
                                    <img src="{{ url('admin') }}/img/avatars/avatar-5.jpg"
                                        class="avatar img-fluid rounded-circle" alt="Vanessa Tucker">
                                </div>
                                <div class="col-10 ps-2">
                                    <div class="text-dark">Vanessa Tucker</div>
                                    <div class="text-muted small mt-1">Nam pretium turpis et arcu. Duis
                                        arcu tortor.</div>
                                    <div class="text-muted small mt-1">15m ago</div>
                                </div>
                            </div>
                        </a>
                        <a href="#" class="list-group-item">
                            <div class="row g-0 align-items-center">
                                <div class="col-2">
                                    <img src="{{ url('admin') }}/img/avatars/avatar-2.jpg"
                                        class="avatar img-fluid rounded-circle" alt="William Harris">
                                </div>
                                <div class="col-10 ps-2">
                                    <div class="text-dark">William Harris</div>
                                    <div class="text-muted small mt-1">Curabitur ligula sapien euismod
                                        vitae.</div>
                                    <div class="text-muted small mt-1">2h ago</div>
                                </div>
                            </div>
                        </a>
                        <a href="#" class="list-group-item">
                            <div class="row g-0 align-items-center">
                                <div class="col-2">
                                    <img src="{{ url('admin') }}/img/avatars/avatar-4.jpg"
                                        class="avatar img-fluid rounded-circle" alt="Christina Mason">
                                </div>
                                <div class="col-10 ps-2">
                                    <div class="text-dark">Christina Mason</div>
                                    <div class="text-muted small mt-1">Pellentesque auctor neque nec urna.
                                    </div>
                                    <div class="text-muted small mt-1">4h ago</div>
                                </div>
                            </div>
                        </a>
                        <a href="#" class="list-group-item">
                            <div class="row g-0 align-items-center">
                                <div class="col-2">
                                    <img src="{{ url('admin') }}/img/avatars/avatar-3.jpg"
                                        class="avatar img-fluid rounded-circle" alt="Sharon Lessman">
                                </div>
                                <div class="col-10 ps-2">
                                    <div class="text-dark">Sharon Lessman</div>
                                    <div class="text-muted small mt-1">Aenean tellus metus, bibendum sed,
                                        posuere ac, mattis non.</div>
                                    <div class="text-muted small mt-1">5h ago</div>
                                </div>
                            </div>
                        </a>
                    -->
                    </div>
                    <div class="dropdown-menu-footer">
                        <!--<a href="#" class="text-muted">Show all messages</a>-->
                    </div>
                </div>
            </li>
            <li class="nav-item">
                <a class="nav-icon" href="#" id="quick-access-menu" data-bs-toggle="offcanvas" data-bs-target="#quick-access-canvas">
                    <div class="position-relative">
                        <i class="align-middle" data-feather="command"></i>
                    </div>
                </a>
            </li>
            @if(count(get_active_languages()) > 0)
            <li class="nav-item dropdown">
                <a class="nav-flag dropdown-toggle" href="#" id="languageDropdown" data-bs-toggle="dropdown">
                    @if(!empty(active_language()->flag) && \Storage::disk('uploads')->exists(active_language()->flag))
                    <img src="{{uploads_url(active_language()->flag)}}" alt="{{active_language()->flag}}">
                    @endif
                    <span class="fs-6">{{active_language()->name}}</span>
                </a>
                <div class="dropdown-menu dropdown-menu-end" aria-labelledby="languageDropdown">
                    @foreach (get_active_languages() as $language)
                    <a class="dropdown-item" href="{{route('translations.set_locale', ['locale' => $language->locale])}}">
                        @if(!empty($language->flag) && \Storage::disk('uploads')->exists($language->flag))
                        <img src="{{uploads_url($language->flag)}}" alt="{{$language->name}}" width="20" class="align-middle me-1">
                        @endif
                        <span class="align-middle">{{$language->name}}</span>
                    </a>
                    @endforeach
                </div>
            </li>
            @endif
            @if(count(currencies()) > 0)
            <li class="nav-item dropdown">
                <a class="nav-flag dropdown-toggle" href="#" id="currenciesDropdown" data-bs-toggle="dropdown">
                    <span class="fs-6">{{currency()->name}}</span>
                </a>
                <div class="dropdown-menu dropdown-menu-end" aria-labelledby="currenciesDropdown">
                    @foreach (currencies() as $currency)
                    <a class="dropdown-item" href="{{route('system.set_currency', ['currency' => $currency])}}">
                        <span class="align-middle">{{$currency->name}} ({{$currency->symbol}})</span>
                    </a>
                    @endforeach
                </div>
            </li>
            @endif
            <li class="nav-item dropdown">
                <a class="nav-icon dropdown-toggle d-inline-block d-sm-none" href="#"
                    data-bs-toggle="dropdown">
                    <i class="align-middle" data-feather="settings"></i>
                </a>

                <a class="nav-link dropdown-toggle d-none d-sm-inline-block" href="#"
                    data-bs-toggle="dropdown">
                    <img src="{{auth()->user()->avatar}}" class="avatar img-fluid rounded me-1"
                        alt="{{auth()->user()->name}}" /> <span class="text-dark">{{auth()->user()->name}}</span>
                </a>
                <div class="dropdown-menu dropdown-menu-end">
                    <a class="dropdown-item" href="{{route('dashboard.users.profile')}}"><i class="align-middle me-1"
                            data-feather="user"></i> @lang('Profile')</a>
                    <!--
                    <a class="dropdown-item" href="#"><i class="align-middle me-1"
                            data-feather="pie-chart"></i> Analytics</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="index.html"><i class="align-middle me-1"
                            data-feather="settings"></i> Settings & Privacy</a>
                    <a class="dropdown-item" href="#"><i class="align-middle me-1"
                            data-feather="help-circle"></i> Help Center</a>
                    -->
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="{{route('logout')}}">@lang('Log out')</a>
                </div>
            </li>
        </ul>
    </div>
</nav>

<div class="offcanvas offcanvas-end" id="quick-access-canvas" style="width:500px;">
    <div class="offcanvas-header">
        <h5 class="offcanvas-title">@lang('Quick Access')</h5>
        <button class="btn-close" data-bs-dismiss="offcanvas"></button>
    </div>
    <div class="offcanvas-body">
        <x-dashboard-menu name="quick-access" />
    </div>
</div>