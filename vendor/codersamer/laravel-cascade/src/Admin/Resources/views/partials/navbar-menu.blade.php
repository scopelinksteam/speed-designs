<ul class="list-inline navbar-nav ms-2">
    @foreach ($menu->GetRenderItems() as $section)
    {{-- Skip Sections --}}
    @foreach ($section->GetItems() as $item)
    <li class="list-inline-item">
        <a class="text-muted" href="{{ $item->GetLink() }}" target="_blank">{{ $item->GetTitle() }}</a>
    </li>
    @endforeach
    @endforeach
</ul>
