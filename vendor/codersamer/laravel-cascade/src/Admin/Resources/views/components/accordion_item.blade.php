<div class="accordion-item">
    <h3 class="accordion-header">
        <button type="button" class="accordion-button @if(!$active) collapsed @endif" id="{{$attributes->get('parent')}}-{{$attributes->get('id')}}-button" data-bs-toggle="collapse" data-bs-target="#{{$attributes->get('parent')}}-{{$attributes->get('id')}}">
            {{$title}}
        </button>
    </h3>
    <div class="accordion-collapse collapse @if($active) show @endif" id="{{$attributes->get('parent')}}-{{$attributes->get('id')}}" data-bs-parent="#{{$attributes->get('parent')}}">
        <div class="accordion-body">
            {{$slot}}
        </div>
    </div>
</div>