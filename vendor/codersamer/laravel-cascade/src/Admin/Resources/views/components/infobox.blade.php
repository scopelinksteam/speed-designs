<div class="info-box">
    <span class="info-box-icon bg-{{ $theme }}"><i class="fas fa-{{ $icon }}"></i></span>
    <div class="info-box-content">
      <span class="info-box-text">{{ $slot }}</span>
      <span class="info-box-number">{{ $value }}</span>
    </div>
    <!-- /.info-box-content -->
</div>
