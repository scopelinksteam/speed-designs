<?php

namespace Cascade\Dashboard\Components;

use Illuminate\View\Component;
use Cascade\Dashboard\Services\DashboardBuilder;

class WidgetsComponent extends Component
{

    public $allow_columns = true;

    public $location = 0;

    public function __construct($location, $allow_columns = true)
    {
        $this->location = $location;
        $this->allow_columns = $allow_columns;
    }

    public function render()
    {
        return view('dashboard::components.widgets', [
            'widgets' => DashboardBuilder::Widgets($this->location)
        ]);
    }
}
