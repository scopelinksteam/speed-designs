<?php

namespace Cascade\Dashboard\Components;

use Cascade\Dashboard\Services\Datatables\DataTable;
use Cascade\System\Services\System;
use Closure;
use Exception;
use Illuminate\Support\Arr;
use Livewire\Component;
use Livewire\WithPagination;

class DataTableComponent extends Component
{

    use WithPagination;

    //Table Identifiers
    protected $table = null;
    public $tableClass = null;
    public $originalClass = '';
    protected $tableInstance = null;
    public $params = null;
    protected $entries = [];
    protected $columns = [];
    protected $paginationTheme = 'bootstrap';
    public $per_page = 10;
    public $filters = [];
    public $search_term = null;

    public $serializedParams = '';

    protected $listeners = [
        'reload-table' => 'ReloadTable'
    ];

    protected function Setup()
    {
        if(empty($this->params)) { $this->params = []; }
        $this->BuildParams();
        //Allow Override
        $activeTable = is_object($this->table) ? get_class($this->table) : $this->table;
        $this->originalClass = $activeTable;
        $activeInstance = class_exists($activeTable) ? new $activeTable(...$this->params) : null;
        while($activeInstance != null && $activeInstance instanceof DataTable && $activeInstance->Extender()->hasReplacement())
        {
            $replacement = $activeInstance->Extender()->getReplacement();
            if($replacement instanceof Closure)
            {
                $replacement = call_user_func($replacement, ...$this->params);
                if(!is_string($replacement)) { break; }
            }
            $activeTable = $replacement;
            $activeInstance = class_exists($activeTable) ? new $activeTable(...$this->params) : null;
        }


        $this->table = $activeInstance;

        if(is_object($this->table))
        {
            $this->tableInstance = $this->table;
            $this->tableClass = get_class($this->tableInstance);
        }
        else
        {
            if(class_exists($this->table))
            {
                $this->tableClass = $this->table;
                $this->tableInstance = new $this->tableClass(...$this->params);
            }
            else if(class_exists($this->tableClass))
            {
                $this->tableInstance = new $this->tableClass(...$this->params);

            }
            else { dd(__('Invalid Datatable'));}
        }



        if(!is_object($this->tableInstance)) { return; }
        $this->columns = System::DispatchFilter("datatables.{$this->tableClass}.columns", $this->tableInstance->GetColumns()) ;
        $query = $this->tableInstance->GetQuery($this->search_term, $this->filters);
        $this->entries = System::DispatchFilter("datatables.{$this->tableClass}.query", $query)->paginate($this->per_page);
    }

    public function ApplyFilters()
    {

        $this->Setup();
        $query = $this->tableInstance->GetQuery($this->search_term, $this->filters);
        $this->entries = System::DispatchFilter("datatables.{$this->tableClass}.query", $query)->paginate($this->per_page);
    }

    public function mount($table)
    {
        $this->table = $table;
        if(empty($this->serializedParams))
        {
            $temp = [];
            foreach(Arr::wrap($this->params ?? []) as $key => $value)
            {
                $temp[$key] = $value instanceof Closure ? $value : serialize($value);
            }
            $this->serializedParams = $temp;
        }
    }

    public function BuildParams()
    {
        if(!empty($this->serializedParams))
        {
            $serialized = $this->serializedParams;
            $tempParams = [];
            foreach(Arr::wrap($serialized) as $key => $data)
            {
                $tempParams[$key] = is_callable($data) || is_array($data) ? $data : ( unserialize($data));
            }
            $this->params = $tempParams;
        }
    }

    protected function BuildEntries()
    {
        $items = [];
        foreach($this->entries as $entry)
        {
            $items[] = $this->tableInstance->build($entry);
        }
        return $items;
    }

    public function ExecuteAction($actionName, $entry, $entryClass)
    {
        $this->Setup();
        if(!($entry instanceof $entryClass))
        {
            try { $entry = call_user_func([$entryClass, 'find'], $entry ?? '0');}
            catch(Exception $ex) {}
        }
        $actions = $this->tableInstance->Extender()->parseActions($this->tableInstance->actions($entry), $entry) ;
        foreach($actions as $action)
        {
            if(strtolower($action->GetName()) == strtolower($actionName))
            {
                $returnValue = $action->ProcessExecute($entry, $this);
                break;
            }
        }
    }

    public function ReloadTable($payload = [])
    {
        if(isset($payload['table']) && ($this->tableClass == $payload['table'] || $this->originalClass == $payload['table']))
        {
            $this->render();
        }
    }

    public function render()
    {
        $this->Setup();

        return view('dashboard::components.datatable',[
            'rows' => $this->BuildEntries(),
            //'entries' => $this->entries
        ]);
    }
}
