<?php

namespace Cascade\Dashboard\Components;

use Illuminate\View\Component;
use Cascade\Dashboard\Services\DashboardBuilder;
use Cascade\System\Services\System;

class MenuComponent extends Component
{

    public $menu;

    public $name;

    public function __construct($name)
    {
        $this->menu = DashboardBuilder::FindMenu($name);

    }

    public function render()
    {
        if($this->menu != null)
        { $this->menu->AssociateItems(); }
        return System::FindView($this->menu != null ? ($this->menu->GetView() ?? 'dashboard::components.menu') : 'dashboard::components.menu');
    }
}
