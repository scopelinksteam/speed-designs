<?php

namespace Cascade\Dashboard\Components;

use Cascade\System\Services\System;
use Illuminate\View\Component;
use ReflectionClass;

class TabsComponent extends Component
{

    public function __construct(public array|String $tabs, public array $data = []) { }

    public function GetTabs() : array
    {
        $tabs = is_string($this->tabs) ? call_user_func([$this->tabs, 'GetTabs']) : $this->tabs;
        $builtTabs = [];
        foreach($tabs as $tab)
        {
            $tempData = [];
            if(is_string($tab) && !class_exists($tab)) { continue; }
            $tabObject = is_string($tab) ? new $tab() : $tab;
            $tabObject->data = $this->data;
            if(!$tabObject->show()) { continue; }
            $builtTabs[] = $tabObject;

        }
        usort($builtTabs, function($a, $b){
            return $a->order() > $b->order();
        });
        return $builtTabs;
    }

    public function render()
    {
        return System::FindView('dashboard::components.tabs', ['pages' => $this->GetTabs()]);
    }
}
