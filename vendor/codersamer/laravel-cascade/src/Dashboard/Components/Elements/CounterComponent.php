<?php

namespace Cascade\Dashboard\Components\Elements;

use Illuminate\View\Component;

class CounterComponent extends Component
{

    public $value;
    public $icon = 'activity';

    public function __construct($value, $icon = 'activity')
    {
        $this->value = $value;
        $this->icon = $icon;
    }
    public function render()
    {
        return view('dashboard::components.elements.counter');
    }
}
