<?php

namespace Cascade\Dashboard\Components;

use Illuminate\View\Component;
use Cascade\Dashboard\Services\DashboardBuilder;

class  NavbarActionsComponent extends Component
{

    public function render()
    {
        return view('dashboard::components.navbar-actions', [
            'actions' => DashboardBuilder::NavbarActions()
        ]);
    }

}
