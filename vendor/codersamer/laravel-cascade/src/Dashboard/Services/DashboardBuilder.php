<?php

namespace Cascade\Dashboard\Services;

use Cascade\Dashboard\Abstraction\DashboardWidget;
use Cascade\Dashboard\Abstraction\NavbarAction;
use Cascade\Dashboard\Entities\Statistic;
use Cascade\Dashboard\Enums\DashboardWidgetLocation;
use Cascade\Dashboard\Enums\ScriptLocation;
use Cascade\Tenancy\Enums\TenantMode;
use Cascade\Tenancy\Services\TenancyService;
use Closure;
use League\CommonMark\Environment\Environment;
use stdClass;

class DashboardBuilder
{

    const WIDGET_LOCATION_DASHBOARD = 'dashboard';
    const WIDGET_LOCATION_QUICK_ACCESS = 'quick_access';

    protected static $mode = TenantMode::Tenant;

    protected static $isDark = false;

    protected static $isRTL = false;

    protected static $custom_scripts = [];

    protected static $custom_styles = [];

    protected static $top_loading_scripts = [];

    protected static $injected_scripts = [];

    protected static $injected_styles = [];

    protected static $javascript_vars = [];

    protected static $javascript_functions = [];

    protected static $central_main_menu = null;

    protected static $main_menu = null;

    protected static $menus = [];

    protected static $widgets = [];

    protected static $navbarActions = [];

    protected static array $statistics = [];

    public static function SetDark() { self::$isDark = true; }
    public static function SetLight() { self::$isDark = false; }

    public static function IsDark() { return self::$isDark; }
    public static function IsLight() { return !self::$isDark; }

    public static function SetRTL() { self::$isRTL = true; }
    public static function SetLTR() { self::$isRTL = false; }

    public static function IsRTL() { return self::$isRTL; }
    public static function IsLTR() { return !self::$isRTL; }

    public static function RegisterScript($scriptPath)
    {
        self::$custom_scripts[] = $scriptPath;
    }

    public static function Central(Closure $callback)
    {
        $oldMode = self::$mode;
        self::$mode = TenantMode::Central;
        $callback();
        self::$mode = $oldMode;
    }

    public static function Tenant(Closure $callback)
    {
        $oldMode = self::$mode;
        self::$mode = TenantMode::Tenant;
        $callback();
        self::$mode = $oldMode;
    }

    public static function Hybrid(Closure $callback)
    {
        static::Tenant($callback);
        static::Central($callback);
    }

    public static function RegisterWidget(DashboardWidget $widget, DashboardWidgetLocation $location)
    {
        if(!isset(self::$widgets[self::$mode->value])){ self::$widgets[self::$mode->value] = []; }
        if(!isset(self::$widgets[self::$mode->value][$location->value])) { self::$widgets[self::$mode->value][$location->value] = []; }
        self::$widgets[self::$mode->value][$location->value][] = $widget;
    }

    public static function Widgets(DashboardWidgetLocation|String $location)
    {
        $location = is_string($location) ? $location : $location->value;
        return isset(self::$widgets[self::$mode->value][$location]) ? collect(self::$widgets[self::$mode->value][$location])->sortBy('order') : [];
    }

    public static function RegisterNavbarAction(NavbarAction $action)
    {
        self::$navbarActions[] = $action;
    }

    public static function NavbarActions()
    {
        return self::$navbarActions;
    }

    public static function AddJavascriptConstant(String $key, String $value)
    {
        static::$javascript_vars[$key] = $value;
    }

    public static function  AddJavascriptFunction(String $name, String $code)
    {
        static::$javascript_functions[$name] = $code;
    }

    public static function Scripts()
    {
        $systemScript = '<script type="text/javascript">'."\n";
        foreach(static::$javascript_vars as $key => $value)
        { $systemScript .= "\t".'const '.strtoupper($key).' = "'.$value.'";'."\n"; }
        foreach(static::$javascript_functions as $name => $code)
        {
            $systemScript .= 'function '.$name.'(){'."\n";
            $systemScript .= $code."\n";
            $systemScript .= '}'."\n";
            $systemScript .= "\t".'window.'.$name.' = '.$name.';'."\n";
        }
        $systemScript.= '</script>'."\n";
        echo $systemScript;
        $cascade = '/cascade/dashboard';
        $cascadePlugins = '/cascade/plugins';
        return array_merge(self::$top_loading_scripts,[
            $cascadePlugins.'/jquery/jquery.min.js',
            $cascade.'/js/select2.min.js',
            $cascadePlugins.'/selectr/selectr.min.js',
            $cascade.'/js/dropify.js',
            $cascadePlugins.'/tinymce/tinymce.min.js',
            $cascadePlugins.'/alpinejs/alpinejs.min.js',
            $cascadePlugins.'/swiperjs/swiper-bundle.min.js',
            $cascadePlugins.'/intl-tel-input/js/intlTelInput.min.js',
            $cascade.'/js/cascade.js'
        ], self::$custom_scripts);
    }

    public static function GetScripts(ScriptLocation $location)
    {
        return isset(self::$injected_scripts[$location->value]) ? self::$injected_scripts[$location->value] : [];
    }
    public static function RegisterStyle($stylePath)
    {
        if(is_array($stylePath))
        {
            self::$custom_styles = array_merge(self::$custom_styles, $stylePath);
            return;
        }
        self::$custom_styles[] = $stylePath;
    }

    public static function InjectStyle($styleUrl)
    {
        self::$injected_styles[] = $styleUrl;
    }

    public static function InjectScript($scriptURL, $location = ScriptLocation::AfterLibraries)
    {
        self::$injected_scripts[$location->value] = isset(self::$injected_scripts[$location->value]) ? self::$injected_scripts[$location->value] : [];
        $script = new stdClass;
        $script->url = $scriptURL;
        self::$injected_scripts[$location->value][] = $script;
    }


    public static function InjectedScripts() { return self::$injected_scripts; }

    public static function InjectedStyles() { return self::$injected_styles; }

    public static function Styles()
    {
        $cascade = '/cascade/dashboard';
        $cascadePlugins = '/cascade/plugins';
        return array_merge([
            $cascadePlugins.'/bootstrap/css/bootstrap-grid'.(app()->getLocale() == 'ar' ? '.rtl' : '').'.min.css',
            $cascadePlugins.'/fontawesome/css/all.min.css',
            $cascade.'/css/select2.css',
            $cascadePlugins.'/selectr/selectr.min.css',
            $cascade.'/css/dropify.css',
            $cascadePlugins.'/swiperjs/swiper-bundle.min.css',
            $cascadePlugins.'/intl-tel-input/css/intlTelInput.min.css',
            $cascade.'/css/cascade.css',
        ], self::$custom_styles);
    }

    public static function MainMenu() : DashboardMenu
    {
        if(self::$mode == TenantMode::Central)
        {
            if(self::$central_main_menu == null) { self::$central_main_menu = new DashboardMenu(); }
            return self::$central_main_menu;
        }
        else
        {
            if(self::$main_menu == null) { self::$main_menu = new DashboardMenu(); }
            return self::$main_menu;
        }


    }

    public static function GetMainMenu() : DashboardMenu
    {
        $oldMode = self::$mode;
        self::$mode = TenancyService::GetUIMode();
        if(self::$mode == TenantMode::Central)
        {
            if(self::$central_main_menu == null) { self::$central_main_menu = new DashboardMenu(); }
            self::$mode = $oldMode;
            return self::$central_main_menu;
        }
        else
        {
            if(self::$main_menu == null) { self::$main_menu = new DashboardMenu(); }
            self::$mode = $oldMode;
            return self::$main_menu;
        }

    }

    public static function Menu($name, $autoCreate = true) : DashboardMenu
    {
        if(strtolower($name) == 'main') { return self::MainMenu(); }
        if(isset(self::$menus[self::$mode->value][$name])) { return self::$menus[self::$mode->value][$name]; }
        if($autoCreate) { self::$menus[self::$mode->value][$name] = new DashboardMenu(); return self::$menus[self::$mode->value][$name]; }
        return null;
    }

    public static function FindMenu($name) : ?DashboardMenu
    {
        if(strtolower($name) == 'main') { return self::GetMainMenu(); }
        if(isset(self::$menus[TenancyService::GetUIMode()->value][$name])) { return self::$menus[TenancyService::GetUIMode()->value][$name]; }
        return null;
    }

    public static function AddMenu(DashboardMenu $menu)
    {
        self::$menus[self::$mode->value][$menu->GetName()] = $menu;
    }

    public static function AddStatistic(Statistic $statistic)
    {
        if(!isset(static::$statistics[self::$mode->value]))
        { static::$statistics[self::$mode->value] = []; }
        static::$statistics[self::$mode->value][] = $statistic;
    }

    public static function Statistics()
    {
        return collect(static::$statistics[TenancyService::GetUIMode()->value])->sort(function(Statistic $a, Statistic $b){
            if($a->order() == $b->order()) { return 0; }
            return $a->order() < $b->order() ? 1 : -1;
        });
    }
}
