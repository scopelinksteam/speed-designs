<?php

namespace Cascade\Dashboard\Services;

use Closure;
use Illuminate\View\Component;
use Cascade\Dashboard\Entities\MenuItem;
use Cascade\Dashboard\Entities\MenuSection;
use Cascade\System\Services\System;

class DashboardMenu
{
    public $items = [];

    protected $sections = [];

    protected $menu_items = [];

    protected $callbacks = [];

    protected $name = 'main';

    protected $view = null;

    public static function make($name)
    {
        $menu = new DashboardMenu();
        $menu->name($name);
        return $menu;
    }

    public function __construct()
    {
        $this->sections['default'] = MenuSection::make('default', null, 99999);
    }

    /**
     * Accessors
     */
    public function name($name) { $this->name = $name; return $this; }
    public function GetName() { return $this->name; }
    public function view($view) { $this->view = $view; return $this; }
    public function GetView() { return $this->view; }

    public function GetRenderItems()
    {
        $sectionsCollection = collect($this->sections);
        $sectionsCollection = $sectionsCollection->sortBy(function($item){
            return $item->GetOrder();
        });
        return $sectionsCollection;
    }

    public function Call(Closure $callback)
    {
        $this->callbacks[] = $callback;
    }

    public function AddSection(MenuSection $section)
    {
        $this->sections[$section->GetId()] = $section;
    }

    public function AddItem(MenuItem $item)
    {
        $this->menu_items[$item->GetId()] = $item;
    }

    public function GetItem(String $id) : ?MenuItem
    {
        return $this->menu_items[$id] ?? null;
    }

    public function AddItems($items)
    {
        foreach($items as $item)
        { $this->menu_items[] = $item; }
    }

    public function AssociateItems()
    {
        foreach($this->callbacks as $callback)
        {
            call_user_func($callback, $this);
        }
        foreach($this->sections as $id => $section)
        {
            $this->sections[$id]->items(collect($this->menu_items)->filter(function($item) use($id){
                return $item->GetSection() == $id;
            })->toArray());
        }
    }
}
