<?php

namespace Cascade\Dashboard\Services\Datatables;

use Cascade\Dashboard\Services\Datatables\Actions\BaseAction;
use Exception;

class Action extends BaseAction
{
    const ACTION_LINK = 0;
    const ACTION_ROUTE = 1;
    const ACTION_CALLBACK = 2;

    protected $hasLink = true;
    protected $link = null;
    protected $route = null;
    protected $callback = null;
    protected $route_params = [];


    public function link($link) { $this->link = $link; return $this; }
    public function route($route, $params = [])
    { $this->route = $route; $this->route_params = $params; return $this; }
    public function callback(callable $callback) {  $this->callback = $callback; return $this; }


    public function GetLink() { return $this->link; }
    public function GetRoute() { return $this->route; }
    public function GetRouteParams() { return $this->route_params; }

    public function GetCallback() { return $this->callback; }



    public function HasCallback()
    { return $this->callback != null && is_callable($this->callback); }

    public function GenerateLink($entry)
    {
        if(!$this->HasLink()) { return '#'; }
        if($this->link != null) { return $this->link; }
        if($this->route != null)
        {
            $params = [];
            foreach($this->route_params as $key => $input)
            {
                try
                {
                    $params[$key] = $entry->$input;

                } catch(Exception $ex)
                {
                    $params[$key] = $input;
                }
                if($params[$key] == null)
                {
                    $params[$key] = $input;
                }
            }
            return route($this->route, $params);
        }
        //Fallback
        return '#';
    }


}
