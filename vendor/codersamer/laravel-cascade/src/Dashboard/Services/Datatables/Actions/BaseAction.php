<?php

namespace Cascade\Dashboard\Services\Datatables\Actions;

use Cascade\Dashboard\Components\DataTableComponent;
use Illuminate\Support\Facades\Gate;

abstract class BaseAction
{
    protected $onRenderCallbacks = [];

    protected $onExecuteCallbacks = [];

    protected $name = '';
    protected $text = '';
    protected $icon = null;
    protected $permission = null;
    protected $hasLink = false;


    protected function __construct($name, $text = '')
    {
        $this->name($name);
        $this->text($text);
    }

    public static function make($name, $text = '')
    {
        return new static($name, $text);
    }

    public function name($name) { $this->name = $name; return $this; }
    public function text($text) { $this->text = $text; return $this; }
    public function icon($iconClass) { $this->icon = $iconClass; return $this; }
    public function permission($permission) { $this->permission = $permission; return $this; }

    public function GetName() { return $this->name; }
    public function GetText() { return $this->text; }
    public function GetIcon() { return $this->icon; }

    public function OnExecute(callable ...$callbacks) { $this->onExecuteCallbacks = array_merge($this->onExecuteCallbacks, $callbacks); return $this;}

    public function CanShow()
    {
        return empty($this->permission) ? true : Gate::allows($this->permission);
    }

    public function HasLink()
    { return $this->hasLink; }

    public function ProcessExecute($entry, DataTableComponent $component)
    {
        foreach($this->onExecuteCallbacks as $callback)
        { call_user_func($callback, $entry, $component); }
        if(method_exists($this, 'Execute'))
        { call_user_func([$this, 'Execute'], $entry, $component); }
    }
}
