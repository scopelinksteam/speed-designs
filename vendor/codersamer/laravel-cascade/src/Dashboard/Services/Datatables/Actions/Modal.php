<?php

namespace Cascade\Dashboard\Services\Datatables\Actions;

use Cascade\Dashboard\Components\DataTableComponent;
use Closure;
use Livewire\Livewire;

class Modal extends BaseAction
{

    protected array|Closure $params = [];
    protected String $modal = '';

    public function params(array|Closure $params)
    {
        $this->params = $params;
        return $this;
    }

    public function modal(String $modalName)
    {
        $this->modal = $modalName;
        return $this;
    }

    public function Execute($entry, DataTableComponent $component)
    {
        $params = is_array($this->params) ? $this->params : call_user_func($this->params, $entry, $component);
        $component->emit('openModal', $this->modal, $params);
    }
}
