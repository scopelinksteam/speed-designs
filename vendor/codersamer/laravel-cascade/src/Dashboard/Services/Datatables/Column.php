<?php

namespace Cascade\Dashboard\Services\Datatables;

use Illuminate\Support\Facades\Gate;

class Column
{
    protected $name = '';
    protected $text = '';
    protected $visible = true;
    protected $permission = null;

    protected function __construct($name)
    {
        $this->name($name);
    }

    public static function make($name)
    {
        $instance = new Column($name);
        return $instance;
    }

    public function name($name) { $this->name = $name; return $this; }
    public function text($text) { $this->text = $text; return $this; }
    public function visible($visibility) { $this->visible = $visibility; return $this; }

    public function GetName() { return $this->name; }
    public function GetText() { return $this->text; }
    public function IsVisible() { return $this->visible; }

    public function permission($permission)
    {
        $this->permission = $permission;
        return $this;
    }

    public function GetPermission()
    {
        return $this->permission;
    }

    public function CanShow()
    {
        return $this->permission == null ? true : Gate::allows($this->permission);
    }

}
