<?php

namespace Cascade\Dashboard\Services\Datatables;

use Carbon\Carbon;
use Cascade\People\Enums\Gender;
use Cascade\System\Services\System;
use Closure;
use Illuminate\Database\Eloquent\Model;
use stdClass;
use UnitEnum;

abstract class DataTable
{

    private $columns_callbacks = [];

    protected $rowClassCallback = false;

    abstract function query();

    abstract function columns();


    /***********************************/
    // Presentation
    /***********************************/
    public function GetColumns()
    {
        return $this->Extender()->parseColumns($this->columns());
    }

    public function GetQuery($searchTerm, $filters)
    {
        return static::Extender()->parseQuery($this->query($searchTerm, $filters));
    }

    public function GetFilters()
    {
        return  static::Extender()->parseFilters($this->filters());
    }
    /****************************/

    protected function editColumn($columnName, $callback)
    {
        $this->columns_callbacks[$columnName] = $callback;
        return $this;
    }

    public function GetColumnValue(Column $column, $entry)
    {
        $class = get_class($this);
        if(isset($this->columns_callbacks[$column->GetName()]))
        {
            $callback = $this->columns_callbacks[$column->GetName()];
            $value = $callback($entry);
            if($value instanceof UnitEnum){ return __($value->name); }
            $value = System::DispatchFilter("datatables.{$class}.column.{$column->GetName()}", $value, $entry);
            return $this->Extender()->parseColumnValue($column->GetName(), $entry, $value);
        }
        $columnValue = $entry->{$column->GetName()};

        $columnValue = System::DispatchFilter("datatables.{$class}.column.{$column->GetName()}", $columnValue, $entry);
        if($columnValue instanceof UnitEnum) {
            return  __($columnValue->name);
        }
        return $this->Extender()->parseColumnValue($column->GetName(), $entry, $columnValue);
    }

    protected function rowClass(Closure $callback)
    {
        $this->rowClassCallback = $callback;
        return $this;
    }

    public function HasActions()
    {
        return method_exists($this, 'actions');
        //$actions = $this->actions();
        //return is_array($actions) && count($actions) > 0;
    }

    public function build($row)
    {
        $item = new stdClass;
        $item->identifier = $row->id;
        $item->row_class = $this->rowClassCallback ? ($this->rowClassCallback)($row) : '';
        foreach($this->GetColumns() as $column)
        {
            if(isset($this->columns_callbacks[$column->GetName()]))
            {
                $item->{$column->GetName()} =  $this->columns_callbacks[$column->GetName()]($row);
                continue;
            }
            if(\Str::contains($column->GetName(), '.'))
            {
                $segments = explode('.', $column->GetName());
            }
            $item->{$column->GetName()} = $row->{$column->GetName()};
        }
        return $item;
    }



    public function tel($telephone, $display = null)
    {
        return '<a href="tel:' . $telephone . '">' . ($display == null ? $telephone : $display). '</a>';
    }

    public function email($email, $display = null)
    {
        return '<a href="mailto:' . $email . '">' . ($display == null ? $email : $display). '</a>';
    }

    public function thumbnail($url)
    {
        return '<img src="' . $url . '" class="rounded thumbnail" width="100" />';
    }

    public function subtitle($subtitle)
    {
        return '<small class="text-muted">'.$subtitle.'</small>';
    }

    public function avatar($url = null, $id = -1)
    {
        if($id == -1) { $id = rand(1,100);}
        if($url == null || empty($url))
        {
            $url = url('dashboard/images/avatars/'.(($id%10)+1).'.png');
        }
        return $this->thumbnail($url);
    }

    public function progress($value, $min = 0, $max = 100, $extraInfo = '')
    {
        $max = $max == 0 ? 1 : $max;
        $average = round(($value/$max) * 100);
        return <<<html
        <div class="progress">
            <div class="progress-bar bg-primary progress-bar-striped" style="width:$average%"></div>
        </div>
        <center>$extraInfo</center>
        html;
    }

    public function link($text, $link, $target = '_self')
    {
        return '<a href="'.$link.'" target="'.$target.'">'.$text.'</a>';
    }

    public function modal($text, $modalName, $params = [])
    {
        return '<button onclick="Livewire.emit(\'openModal\', \''.$modalName.'\', '.json_encode($params).')">'.$text.'</button>';
    }

    public function badge($value, $theme = 'primary')
    {
        if(str($theme)->startsWith('#'))
        {
            return '<span class="badge" style="background-color:'.$theme.';">'.$value.'</span>';
        }
        return '<span class="badge bg-'.$theme.'">'.$value.'</span>';
    }

    public function error()
    {
        return '<i class="fa fa-times-circle" style="color:red;"></i>';
    }

    public function success()
    {
        return '<i class="fa fa-check-circle" style="color:green;"></i>';
    }

    public function gender(Gender $gender) : String
    {
        $icon = $gender == Gender::Female ? 'female' : 'male';
        $color = $gender == Gender::Female ? 'danger' : 'info';
        return '<i class="fa fa-'.$icon.' me-2 text-'.$color.'"></i>';
    }

    public function color(String $color) : String
    {
        return '<span class="rounded-circle d-inline-block" style="width:24px; height:24px;background-color:'.$color.'"></span>';
    }
    public function date(String|Carbon $date) : String
    {
        return format_date($date);
    }

    public function datetime(String|Carbon $date) : String
    {
        return format_datetime($date);
    }

    public function time(String|Carbon $date) : String
    {
        return format_time($date);
    }

    public function dateDiffForHumans($date)
    {
        if (is_string($date)) {
            $date = Carbon::parse($date);
        }
        return $date->diffForHumans();
    }

    public function filters()
    {
        return [];
    }

    public static function Extender() : DataTableExtender { return DataTableExtender::Get(static::class); }
}
