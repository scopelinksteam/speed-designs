<?php

namespace Cascade\Dashboard\Services\Datatables;

use Closure;

class DataTableExtender
{
    protected static $instances = [];

    protected $columnsEditors = [];

    protected $hiddenColumns = [];

    protected $shownColumns = [];

    protected $columns = [];

    protected $removedColumns = [];

    protected $onQueryEditors = [];

    protected $onColumnsEditors = [];

    protected $actions = [];

    protected $removedActions = [];

    protected $onActionsEditors = [];

    protected $filters = [];

    protected $removedFilters = [];

    protected $onFiltersEditors = [];

    protected $replaceTable = null;


    public static function Get(String $tableClass)
    {
        if(!isset(static::$instances[$tableClass]))
        { static::$instances[$tableClass] = new static($tableClass); }
        return static::$instances[$tableClass];
    }

    public static function Register(String $class, String $tableClass)
    {
        if(class_exists($class))
        {
            static::$instances[$tableClass] = new $class;
        }
        return static::Get($tableClass);
    }

    public function override(Closure|String $tableClass)
    {
        $this->replaceTable = $tableClass;
    }

    public function hasReplacement()
    {
        return $this->replaceTable != null;
    }

    public function getReplacement()
    {
        return $this->replaceTable;
    }


    /*******************************/
    // Columns Editor
    /*******************************/

    public function editColumn(String $column, Closure $callback)
    {
        if(!isset($this->columnsEditors[$column]))
        { $this->columnsEditors[$column] = []; }
        $this->columnsEditors[$column][] = $callback;
    }

    public function hideColumn(String $column) { $this->hiddenColumns[] = $column; }

    public function showColumn(String $column) { $this->shownColumns[] = $column; }

    public function addColumn(Column $column) { $this->columns[] = $column; }

    public function removeColumn(String $column) { $this->removedColumns[] = $column; }

    public function onColumns(Closure $callback) { $this->onColumnsEditors[] = $callback; }

    /*******************************/
    // Query Editor
    /*******************************/

    public function onQuery(Closure $callback) { $this->onQueryEditors[] = $callback; }


    /*******************************/
    // Actions Editor
    /*******************************/

    public function addAction(Closure $action) { $this->actions[] = $action; }

    public function removeAction(String $action) { $this->removedActions[] = $action; }

    public function onActions(Closure $callback) { $this->onActionsEditors[] = $callback; }

    public function getActions($entry)
    {
        $temp = [];
        foreach($this->actions as $action)
        {
            if(is_callable($action)) { $action = call_user_func($action, $entry); }
            if(is_array($action))
            { $temp = array_merge_recursive($temp, $action); }
            elseif (empty($action)) { continue; }
            else { $temp[] = $action; }
        }
        return $temp;
    }

    /*******************************/
    // Filters Editor
    /*******************************/

    public function addFilter(Filter $filter) { $this->filters[] = $filter; }

    public function removeFilter(String $filter) { $this->removedFilters[] = $filter; }

    public function onFilters(Closure $callback) { $this->onFiltersEditors[] = $callback; }

    /*******************************/
    // Parsers
    /*******************************/

    public function parseFilters($filters) : array
    {
        if(!is_array($filters)) { return []; }
        $tempFilters = [];

        //Remove Filters
        foreach($filters as $filter)
        {
            if(in_array($filter->GetName(), $this->removedFilters)) { continue; }
            $tempFilters[] = $filter;
        }

        $filters = array_merge_recursive($tempFilters, $this->filters);
        foreach($this->onFiltersEditors as $callback)
        {
            $callbackReturn = call_user_func($callback, $filters);
            if(is_array($callbackReturn)) { $filters = $callbackReturn; }
        }
        return $filters;
    }

    public function parseActions($actions, $entry = null) : array
    {
        if(!is_array($actions)) { return []; }
        $tempActions = [];

        //Remove Actions
        foreach($actions as $action)
        {
            if(in_array($action->GetName(), $this->removedActions)) { continue; }
            $tempActions[] = $action;
        }
        $actions = array_merge_recursive($tempActions, $this->getActions($entry));
        foreach($this->onActionsEditors as $callback)
        {
            $callbackReturn = call_user_func($callback, $actions);
            if(is_array($callbackReturn)) { $actions = $callbackReturn; }
        }
        return $actions;
    }

    

    public function parseColumns($columns) : array
    {
        if(!is_array($columns)) { return []; }
        $tempColumns = [];

        //Filter Columns
        foreach($columns as $column)
        {
            if(in_array($column->GetName(), $this->removedColumns)) { continue; }
            if(in_array($column->GetName(), $this->hiddenColumns)) { $column->visible(false); }
            if(in_array($column->GetName(), $this->shownColumns)) { $column->visible(true); }
            $tempColumns[] = $column;
        }

        $columns = array_merge_recursive($tempColumns, $this->columns);
        foreach($this->onColumnsEditors as $callback)
        {
            $callbackReturn = call_user_func($callback, $columns);
            if(is_array($callbackReturn)) { $columns = $callbackReturn; }
        }
        return $columns;
    }

    public function parseColumnValue($columnName, $param, $defaultValue = null)
    {
        $value = $defaultValue;
        if(isset($this->columnsEditors[$columnName]))
        {
            foreach($this->columnsEditors[$columnName] as $callback)
            {
                $callbackReturn = call_user_func($callback, $param, $value);
                $value = $callbackReturn;
            }
        }
        return $value;
    }

    public function parseQuery($query)
    {
        foreach($this->onQueryEditors as $callback)
        {
            $query = call_user_func($callback, $query);
        }

        return $query;
    }
}
