<?php

namespace Cascade\Dashboard\Services\Datatables\Filters;

use Cascade\System\Services\System;
use Illuminate\View\View;

class DateFilter extends BaseFilter
{
    public static function make($name)
    {
        $object = new static;
        $object->name($name);
        return $object;
    }

    public function view(): View|string
    {
        return System::FindView('dashboard::datatables.filters.date');
    }
}