<?php

namespace Cascade\Dashboard\Services\Datatables\Filters;

use Cascade\System\Services\System;
use Illuminate\View\View;

class SelectFilter extends BaseFilter
{

    protected $values = [];

    protected $multiple = false;

    public static function make($name)
    {
        $object = new static;
        $object->name($name);
        return $object;
    }

    public function values($values)
    {
        $this->values = $values;
        return $this;
    }

    public function multiple($isMultiple = true)
    {
        $this->multiple = $isMultiple;
        return $this;
    }

    public function GetValues()
    {
        //Check if Callback
        if(is_callable($this->values))
        {
            return call_user_func($this->values);
        }
        return $this->values;
    }

    public function IsMultiple()
    {
        return $this->multiple;
    }

    public function view(): View|string
    {
        return System::FindView('dashboard::datatables.filters.select');
    }
}