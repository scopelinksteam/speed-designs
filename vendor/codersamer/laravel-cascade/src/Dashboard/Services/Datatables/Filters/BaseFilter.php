<?php

namespace Cascade\Dashboard\Services\Datatables\Filters;

use Illuminate\View\View;

abstract class BaseFilter
{
    protected $title = false;

    protected $name = '';

    protected $placeholder = '';

    public function name($name)
    {
        $this->name = $name;
        return $this;
    }

    public function GetName()
    {
        return $this->name;
    }

    public function title($title)
    {
        $this->title = $title;
        return $this;
    }

    public function GetTitle()
    {
        return $this->title;
    }

    public function placeholder($placeholder)
    {
        $this->placeholder = $placeholder;
        return $this;
    }

    public function GetPlaceholder()
    {
        return $this->placeholder;
    }

    public function Is($type)
    {
        return static::class == $type;
    }

    public function GetType()
    {
        return static::class;
    }

    public abstract function view() : View|String;

    public function render($value = null) 
    {
        $content = $this->view();
        if(is_string($content)) { return $content; }
        return $content->with([
            'filter' => $this,
            'value' => $value,
        ])->render();
    }
}