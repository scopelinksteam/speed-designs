<?php

namespace Cascade\Dashboard\Services\Datatables;

use Cascade\Dashboard\Services\Datatables\Filters\BaseFilter;
use Cascade\System\Services\System;
use Illuminate\Support\Facades\View as FacadesView;
use Illuminate\View\View;

class Filter extends BaseFilter
{

    const FILTER_TYPE_TEXT = 0;
    const FILTER_TYPE_SELECT = 1;
    const FILTER_TYPE_CHECKBOX = 2;
    const FILTER_TYPE_NUMBER = 3;

    protected $type = '';

    protected $values = [];
    
    protected function __construct($type, $name)
    {
        $this->type = $type;
        $this->name = $name;
    }

    public static function make($type, $name)
    {
        return new Filter($type, $name);
    }

    public function values($values)
    {
        $this->values = $values;
        return $this;
    }

    public function GetValues()
    {
        //Check if Callback
        if(is_callable($this->values))
        {
            return call_user_func($this->values);
        }
        return $this->values;
    }

    public function view(): View|string
    {
        $viewName = '';
        switch($this->type)
        {
            case Filter::FILTER_TYPE_TEXT : $viewName = 'dashboard::datatables.filters.filter-text'; break;
            case Filter::FILTER_TYPE_SELECT : $viewName = 'dashboard::datatables.filters.filter-select'; break;
            case Filter::FILTER_TYPE_CHECKBOX : $viewName = 'dashboard::datatables.filters.filter-checkbox'; break;
            case Filter::FILTER_TYPE_NUMBER : $viewName = 'dashboard::datatables.filters.filter-number'; break;
        }

        if(FacadesView::exists($viewName))
        {
            return System::FindView($viewName);
        }
        return '';
    }

    

}
