<?php


namespace Cascade\Dashboard\Traits;

use Cascade\Dashboard\Abstraction\DashboardTab;
use stdClass;

trait HasTabs
{
    protected static $tabs = [];

    protected static $removedTabs = [];

    public static function AddTab(String|DashboardTab $tabClass, $order = -1)
    {
        static::RegisterSelf();
        static::$tabs[static::class][is_string($tabClass) ? $tabClass : get_class($tabClass)] = $tabClass;
    }


    protected static function RegisterSelf()
    {
        if(!isset(static::$tabs[static::class]))
        { static::$tabs[static::class] = []; }
        if(!isset(static::$removedTabs[static::class]))
        { static::$removedTabs[static::class] = []; }
    }

    public static function RemoveTab(String $tabClass)
    {
        static::RegisterSelf();
        static::$removedTabs[static::class][] = $tabClass;
    }

    public static function GetTabs()
    {
        static::RegisterSelf();
        $tempTabs = [];
        foreach(static::$tabs[static::class] as $key => $tab)
        {
            if(in_array($key, static::$removedTabs[static::class])) { continue; }
            $tempTabs[$key] = $tab;
        }
        $tempTabs = collect($tempTabs);
        return $tempTabs->sortBy(function($tab){
            $tabObject = is_string($tab) ? new $tab : $tab;
            return $tabObject->order();
        })->all();
    }
}
