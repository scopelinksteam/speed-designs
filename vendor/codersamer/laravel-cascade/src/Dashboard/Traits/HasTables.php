<?php

namespace Cascade\Dashboard\Traits;

use Cascade\Dashboard\Services\Datatables\DataTable;

trait HasTables
{
    protected static $datatables = [];

    protected static function EnsureDatatablesInitialized()
    {
        if(!isset(static::$datatables[static::class]))
        { static::$datatables[static::class] = []; }
    }

    public static function SetDataTable(String $identifier, String|DataTable $dataTable)
    {
        static::EnsureDatatablesInitialized();
        static::$datatables[static::class][$identifier] = $dataTable;
    }

    public static function GetDataTable(String $identifier, $defaultTable = null)
    {
        static::EnsureDatatablesInitialized();
        if(isset(static::$datatables[static::class][$identifier]))
        {
            return static::$datatables[static::class][$identifier] ?? $defaultTable;
        }
        return $defaultTable;
    }

    public static function SetPrimaryTable(String $tableClass)
    {
        static::SetDataTable('primary', $tableClass);
    }

    public static function GetPrimaryTable($defaultTable = null)
    {
        return static::GetDataTable('primary', $defaultTable);
    }
}
