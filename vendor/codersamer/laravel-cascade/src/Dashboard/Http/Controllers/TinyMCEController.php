<?php

namespace Cascade\Dashboard\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class TinyMCEController extends Controller
{
    public function postImage(Request $request)
    {
        if(!$request->file('file')) {
            return response(json_encode([
                'message' => __('File Missing')
            ]), 400);
        }

        $file = $request->file('file');
        $filename = $file->getClientOriginalName();
        $fileExtension = $file->getClientOriginalExtension();
        $validExtensions = ['jpg', 'jpeg', 'png', 'bmp', 'gif', 'webp'];
        if(!in_array(strtolower($fileExtension), $validExtensions))
        {
            return response(json_encode([
                'message' => __('Invalid File Extension')
            ]), 400);
        }

        $path = $file->store('editor', ['disk' => 'uploads']);
        $location = uploads_url($path);
        return response(json_encode([
            'location' => $location
        ]), 200);

    }
}
