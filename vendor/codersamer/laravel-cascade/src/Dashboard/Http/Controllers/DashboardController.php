<?php

namespace Cascade\Dashboard\Http\Controllers;

use Cascade\Dashboard\Enums\DashboardWidgetLocation;
use Cascade\Dashboard\Services\DashboardBuilder;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Cascade\System\Services\System;
use Illuminate\Support\Facades\Gate;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        Gate::authorize('view dashboard');
        return System::FindView('dashboard::index', [
            'statistics' => DashboardBuilder::Statistics(),
            'widgets' => DashboardBuilder::Widgets(DashboardWidgetLocation::Dashboard)
        ]);
    }
}
