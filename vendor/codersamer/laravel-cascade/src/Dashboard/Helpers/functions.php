<?php

use Cascade\Dashboard\Enums\ScriptLocation;
use Cascade\Dashboard\Services\DashboardBuilder;

function dashboard_is_dark()
{ return DashboardBuilder::IsDark(); }


function dashboard_is_light()
{ return DashboardBuilder::IsLight(); }


function dashboard_is_rtl()
{ return DashboardBuilder::IsRTL(); }

function dashboard_is_ltr()
{ return DashboardBuilder::IsLTR(); }

function dashboard_scripts()
{ return DashboardBuilder::Scripts(); }

function dashboard_styles()
{ return DashboardBuilder::Styles(); }


function renderScriptObject(stdClass $script)
{
    echo '<script '.((property_exists($script, 'defer') && $script->defer) ? 'defer' : '').' src="'.url($script->url).'"></script>'."\n";
}
function dashboard_render_scripts()
{
    //Before Libraries
    foreach(DashboardBuilder::GetScripts(ScriptLocation::BeforeLibraries) as $script) { renderScriptObject($script); }
    //System Libraries
    foreach(dashboard_scripts() as $script)
    { echo '<script defer src="'.url($script).'"></script>'."\n"; }
    //After Libraries
    foreach(DashboardBuilder::GetScripts(ScriptLocation::AfterLibraries) as $script) { renderScriptObject($script); }
    //Before Custom Scripts
    foreach(DashboardBuilder::GetScripts(ScriptLocation::BeforeCustom) as $script) { renderScriptObject($script); }
    //Custom Scripts
    foreach(DashboardBuilder::GetScripts(ScriptLocation::Custom) as $script) { renderScriptObject($script); }
    //After Custom Scripts
    foreach(DashboardBuilder::GetScripts(ScriptLocation::AfterCustom) as $script) { renderScriptObject($script); }
}

function dashboard_render_styles()
{
    foreach(dashboard_styles() as $style)
    { echo '<link rel="stylesheet" href="'.url($style).'">'."\n"; }
    foreach(DashboardBuilder::InjectedStyles() as $style)
    { echo '<link rel="stylesheet" href="'.$style.'">'."\n"; }
}

function dashboard_widgets($location)
{
    return DashboardBuilder::Widgets($location);
}

function dashboard_navbar_actions()
{
    return DashboardBuilder::NavbarActions();
}
