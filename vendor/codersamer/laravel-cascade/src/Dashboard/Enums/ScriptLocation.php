<?php

namespace Cascade\Dashboard\Enums;

enum ScriptLocation : int {
    case End = 0;
    case BeforeLibraries = 1;
    case AfterLibraries = 2;
    case BeforeCustom = 3;
    case Custom = 4;
    case AfterCustom = 5;
}
