<?php

namespace Cascade\Dashboard\Enums;

enum DashboardWidgetLocation : String
{
    case Dashboard = 'dashboard';
    case QuickAccess = 'quick_access';
}
