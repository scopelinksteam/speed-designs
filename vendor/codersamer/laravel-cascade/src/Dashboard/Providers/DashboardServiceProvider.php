<?php

namespace Cascade\Dashboard\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Blade;
use Livewire\Livewire;
use Cascade\Dashboard\Components\DataTableComponent;
use Cascade\Dashboard\Components\Elements\CounterComponent;
use Cascade\Dashboard\Components\MenuComponent;
use Cascade\Dashboard\Components\NavbarActionsComponent;
use Cascade\Dashboard\Components\TabsComponent;
use Cascade\Dashboard\Components\WidgetsComponent;
use Cascade\Dashboard\Entities\MenuItem;
use Cascade\Dashboard\Entities\MenuSection;
use Cascade\Dashboard\Services\DashboardBuilder;
use Cascade\Dashboard\Services\DashboardMenu;
use Cascade\Settings\Services\Settings;
use LivewireUI\Modal\Modal;

class DashboardServiceProvider extends ServiceProvider
{
    /**
     * @var string $moduleName
     */
    protected $moduleName = 'Dashboard';

    /**
     * @var string $moduleNameLower
     */
    protected $moduleNameLower = 'dashboard';

    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerTranslations();
        $this->registerConfig();
        $this->registerViews();
        $this->loadMigrationsFrom(module_path($this->moduleName, 'Database/Migrations'));
        $this->registerComponents();
        $this->registerSettings();
        DashboardBuilder::Menu('navbar')->AddItem(MenuItem::make('Dashboard')->route('dashboard.index'));
    }


    /**
     * Register Module Settings
     *
     * @return void
     */
    public function registerSettings()
    {
        //die(config('filesystems.disks.uploads.root'));
        Settings::on('system', function(){
            Settings::page(__('Dashboard Access'), 'dashboard', function(){
                Settings::group(__('Link Customization'), function(){
                    Settings::text(__('URL Prefix'), 'url_prefix');
                });
            });
            Settings::page('Login Page', 'login_page', function(){
                Settings::group(__('Design and Layout'), function(){
                    Settings::image(__('Background Image'), 'background');
                });
            });

            Settings::page(__('Dashboard Customization'), 'admin', function(){
                Settings::group(__('General'), function(){
                    Settings::text(__('Footer Copyrights'), 'copyrights');
                });
            });
        });
    }

     /**
     * Register Macros found in ../Macros Directory
     *
     * @return void
     */
    public function registerMacros()
    {
        $macros = array_diff(scandir(__DIR__ . '/../Macros/'), ['.', '..']);
        foreach ($macros as $macro) {
            require_once(__DIR__ . '/../Macros/' . $macro);
        }
    }

    /**
     * Register Blade Components
     *
     * @return void
     */
    public function registerComponents()
    {

        Blade::component(MenuComponent::class, 'dashboard-menu');

        Blade::component(CounterComponent::class, 'dashboard-widget-counter');

        Blade::component(WidgetsComponent::class, 'dashboard-widgets');

        Blade::component(NavbarActionsComponent::class, 'dashboard-navbar-actions');

        Blade::component(TabsComponent::class, 'dashboard-tabs');

        Livewire::component('datatable', DataTableComponent::class);
        Livewire::component('livewiremodal', Modal::class);

        //DashboardBuilder::RegisterScript('js/dashboard.js');
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->registerMacros();
        $this->app->register(RouteServiceProvider::class);
    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->publishes([
            module_path($this->moduleName, 'Config/config.php') => config_path($this->moduleNameLower . '.php'),
        ], 'config');
        $this->mergeConfigFrom(
            module_path($this->moduleName, 'Config/config.php'), $this->moduleNameLower
        );
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $viewPath = resource_path('views/modules/' . $this->moduleNameLower);

        $sourcePath = module_path($this->moduleName, 'Resources/views');

        $this->publishes([
            $sourcePath => $viewPath
        ], ['views', $this->moduleNameLower . '-module-views']);

        $this->loadViewsFrom(array_merge($this->getPublishableViewPaths(), [$sourcePath]), $this->moduleNameLower);

        $this->loadViewsFrom(array_merge($this->getPublishableViewPaths(), [base_path('vendor\wire-elements\modal\resources\views')]), 'livewire-ui-modal');
    }

    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $langPath = resource_path('lang/modules/' . $this->moduleNameLower);

        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, $this->moduleNameLower);
        } else {
            $this->loadTranslationsFrom(module_path($this->moduleName, 'Resources/lang'), $this->moduleNameLower);
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }

    private function getPublishableViewPaths(): array
    {
        $paths = [];
        foreach (\Config::get('view.paths') as $path) {
            if (is_dir($path . '/modules/' . $this->moduleNameLower)) {
                $paths[] = $path . '/modules/' . $this->moduleNameLower;
            }
        }
        return $paths;
    }
}
