<?php

use Cascade\Tenancy\Http\Middleware\EnsureTenantRequest;
use Illuminate\Routing\Router;
use Illuminate\Support\Facades\Route;

Router::macro('dashboard', function(Closure $callback, $slug = null, $name = null){
    $prefix = 'dashboard';
    try { $prefix = get_setting('system::dashboard.url_prefix', 'dashboard'); }
    catch(Exception $ex) {}
    
    if($slug == null)
    {
        Route::prefix($prefix)->as('dashboard.')->middleware( [EnsureTenantRequest::class, 'auth', 'can:view dashboard'])->namespace('Dashboard')->group($callback);
    }
    else
    {
        $name = ($name == null) ? str_replace('-', '_',$slug) : $name;
        Route::prefix($prefix.'/'.$slug)->as('dashboard.'.$name.'.')->middleware([EnsureTenantRequest::class, 'auth', 'can:view dashboard'])->namespace('Dashboard')->group($callback);
    }
});
