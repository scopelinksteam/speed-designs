<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Cascade\Dashboard\Http\Controllers\CentralDashboardController;
use Illuminate\Support\Facades\Route;
use Cascade\Dashboard\Http\Controllers\DashboardController;
use Cascade\Dashboard\Http\Controllers\TinyMCEController;
use Cascade\Tenancy\Enums\TenantMode;
use Cascade\Tenancy\Services\TenancyService;

//Mixed Routes
Route::hybrid(function(){
    Route::prefix('tinymce')->as('tinymce.')->group(function(){
        Route::post('image-upload', [TinyMCEController::class, 'postImage'])->name('image-upload');
    });
}, null);

TenancyService::Initialize();
if(TenancyService::ActiveMode() == TenantMode::Central)
{
    //Central Dashboard
    Route::central(function(){
        Route::get('/', [CentralDashboardController::class, 'index'])->name('index')->middleware('can:view dashboard');
    }, null);
}
else
{
    //Tenant Dashboard
    Route::dashboard(function(){
        Route::get('/', [DashboardController::class, 'index'])->name('index')->middleware('can:view dashboard');
    }, null);
}




