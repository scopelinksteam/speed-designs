<?php

namespace Cascade\Dashboard\Entities;

use Cascade\System\Enums\BootstrapColors;
use Closure;

class Statistic
{

    protected String $title = '';

    protected int|Closure $count = 0;

    protected ?String $icon = null;

    protected ?String $link = null;

    protected String $color = '#000000';

    protected int $progress = -1;

    protected int $order = 100;

    protected function __construct(String $title = '', int|Closure $count = 0)
    {
        $this->title($title);
        $this->count($count);
    }

    public static function make(String $title = '', int $count = 0) : static
    {
        return new static($title, $count);
    }

    public function icon(?String $icon = null) : static|String|null
    {
        if($icon === null) { return $this->icon; }
        $this->icon = $icon;
        return $this;
    }

    public function link(?String $link = null) : static|String|null
    {
        if($link === null) { return $this->link; }
        $this->link = $link;
        return $this;
    }

    public function color(String|BootstrapColors|null $color = null) : static|String
    {
        if($color === null) { return $this->color; }
        $this->color = is_string($color) ? $color : $color->value;
        return $this;
    }

    public function title(?String $title = null) : static|String
    {
        if($title === null) { return $this->title; }
        $this->title = $title;
        return $this;
    }

    public function count(null|int|Closure $count = null) : static|int
    {
        if($count === null) { 
            return $this->count instanceof Closure ? call_user_func($this->count) : $this->count; 
        }
        $this->count = $count;
        return $this;
    }

    public function progress(?int $progress = null) : static|int
    {
        if($progress === null) { return $this->progress; }
        $this->progress = $progress;
        return $this;
    }

    public function order(?int $order = null) : static|int
    {
        if($order === null) { return $this->order; }
        $this->order = $order;
        return $this;
    }


}
