<?php

namespace Cascade\Dashboard\Entities;

class MenuItem
{
    protected static $currentId = 1;
    protected $id = null;
    protected $title;
    protected $description = '';
    protected $icon = null;
    protected $section;
    protected $order = 1;
    protected $items = [];
    protected $badge_callback = null;
    protected $badge_type = 'primary';
    protected $visible = true;
    protected $route = null;
    protected $route_params = [];
    protected $link = '#';

    protected $permissions = [];

    protected function __construct($title, $section = 'default')
    {
        $this->section($section);
        $this->title($title);
        $this->GenerateId();
    }

    protected function GenerateId()
    {
        $this->id = 'menu-item-'.self::$currentId;
        self::$currentId++;
    }

    public static function make( $title, $section = 'default')
    {
        return new MenuItem($title ,$section  );
    }

    public function section($section) { $this->section = $section; return $this; }
    public function title($title) { $this->title = $title; return $this; }
    public function description($description) { $this->description = $description; return $this; }
    public function icon($icon) { $this->icon = $icon; return $this; }
    public function order($order) { $this->order = $order; return $this; }
    public function child(MenuItem $item) { $this->items[] = $item; return $this;  }
    public function route($route, $params = []) { $this->route = $route; $this->route_params = $params; return $this; }
    public function link($link) { $this->link = $link; return $this; }
    public function permission($permission) { $this->permissions[] = $permission; return $this; }
    public function id($id) { $this->id = $id; return $this; }
    public function visible($isVisible) { $this->visible = $isVisible; return $this; }
    public function badge($callback, $type = 'primary')
    {
        $this->badge_callback = $callback;
        $this->badge_type = $type;
        return $this;
    }
    public function children($items)
    {
        foreach($items as $item) { $this->child($item); }
        return $this;
    }

    public function GetId() { return $this->id; }
    public function GetSection() { return $this->section; }
    public function GetTitle() { return $this->title; }
    public function GetDescription() { return $this->description; }
    public function GetIcon() { return $this->icon == null ? 'grid' : $this->icon; }
    public function GetOrder() { return $this->order; }
    public function GetPermissions() { return $this->permissions; }
    public function GetChildren()
    {
        return collect($this->items)->sortBy(function($item){
            return $item->GetOrder();
        })->toArray();
    }

    public function GetBadge()
    {
        if(!$this->HasBadge()) { return null; }
        return call_user_func($this->badge_callback, $this);
    }

    public function GetLink()
    {
        if($this->route != null) { return route($this->route, $this->route_params); }
        if($this->link != null && $this->link != '#') { return $this->link;}
        return $this->HasChildren() ? '#'.$this->id : '#';
    }

    public function GetBadgeType() { return $this->badge_type; }

    public function HasChildren()
    {
        return count($this->items) > 0;
    }

    public function HasContents()
    {
        $hasLink = $this->route != null || (!empty($this->link) && $this->link != '#');
        if($hasLink) { return true; }
        if(!$this->HasChildren()) { return false; }
        $showableChilds = 0;
        foreach($this->items as $childItem)
        {
            if($childItem->CanShow()) { $showableChilds++; }
        }
        return $showableChilds > 0;
    }

    public function HasBadge()
    {
        return $this->badge_callback != null && is_callable($this->badge_callback);
    }

    public function CanShow()
    {
        if(!$this->visible) { return false; }
        if(count($this->permissions) == 0) { return true; }
        $valid = true;
        foreach($this->permissions as $permission)
        {
            if(!\can($permission)) { $valid = false; break;}
        }
        return $valid;
    }

    public function IsActive()
    {
        $strippedLink = trim(str_replace(url('/'), '', $this->GetLink()), '/');
        if(request()->is($strippedLink))
        {
            return true;
        }
        if($this->HasChildren())
        {
            foreach($this->GetChildren() as $child)
            {
                if($child->IsActive()) {return true;}
            }
        }
        return false;
    }
    public function HasActiveChild()
    {
        if($this->HasChildren())
        {
            foreach($this->GetChildren() as $child)
            {
                if($child->IsActive()) {return true;}
            }
        }
        return false;
    }

}
