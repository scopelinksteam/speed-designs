<?php

namespace Cascade\Dashboard\Entities;

class MenuSection
{
    protected $title = "";
    protected $id = "";
    protected $order = 1;
    protected $items = [];

    protected function __construct($id, $title, $order = 1)
    {
        $this->id($id);
        $this->title($title);
        $this->order($order);
    }

    public static function make($id, $title, $order = 1)
    {
        return new MenuSection($id, $title, $order);
    }

    public function id($id) { $this->id = $id; return $this; }
    public function title($title) { $this->title = $title; return $this; }
    public function order($order) { $this->order = intval($order); return $this; }

    public function GetId() { return $this->id; }
    public function GetTitle() { return $this->title; }
    public function GetOrder() { return $this->order; }

    public function GetItems()
    {
        return collect($this->items)->sortBy(function($item){
            return $item->GetOrder();
        })->toArray();
    }

    public function items($items)
    {
        if($items instanceof MenuItem) { $this->items[] = $items; return $this; }
        if(!is_array($items)) { return $this; }
        $this->items = array_merge($this->items, $items);
    }

    public function HasItems()
    {
        if(count($this->items) == 0) { return false;}
        $showableItems = 0;
        foreach($this->items as $item)
        {
            if($item->HasContents() && $item->CanShow()) { $showableItems++; }
        }
        return $showableItems > 0;
    }

}
