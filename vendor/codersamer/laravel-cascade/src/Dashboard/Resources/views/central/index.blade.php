@extends('admin::layouts.master')
@section('page-title', __('System Dashboard'))
@section('page-description', __('Overview for the Running system with Statistics and Analytics'))
@section('content')
<section class="dashboard-statistics">
    <div class="row">
        <div class="col">
            <p class="mb-4 text-bold">@lang('Dashboard Statistics')</p>
        </div>
        <div class="col d-flex justify-content-end gap-x-2">
            <button class="btn btn-light" id="dashboard-statistics-prev" role="button" type="button"><i class="fa fa-arrow-left"></i></button>
            <button class="btn btn-light" id="dashboard-statistics-next" role="button" type="button"><i class="fa fa-arrow-right"></i></button>
        </div>
    </div>
    <div class="swiper-wrapper">

        @foreach ($statistics as $statistic)
        <div class="swiper-slide card" >
            <div class="card-body">
                <div class="row align-items-center mb-3">
                    <div class="col-auto">
                        <i class="display-5 fa fa-{{ $statistic->icon() }}"></i>
                    </div>
                    <div class="col">
                        <h3 style="{{--color: {{ $statistic->color() }}--}}" class="display-6 mb-0 text-end">{{ number_format($statistic->count()) }}</h3>
                    </div>
                </div>

                <p class="mb-0">{{ $statistic->title() }}</p>
                @if($statistic->link())
                <a href="{{ $statistic->link() }}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                @endif
            </div>
        </div>
        @endforeach
    </div>
</section>
<div class="row">
    @foreach ($widgets as $widget)
    <div class="col-{{ $widget->columns() }}" class="dashboard-widget" id="{{ $widget->id() }}">
        <div class="card">
            <div class="card-header">
                <div class="card-title">{{ $widget->title() }}</div>
                <div class="card-tools">
                    <button class="btn btn-tool" data-card-widget="collapse">
                        <i class="fa fa-minus"></i>
                    </button>
                </div>
            </div>
            <div class="card-body">
                {!! $widget->render()->toHtml() !!}
            </div>
        </div>
    </div>
    @endforeach
</div>
@yield('widgets')
@endsection
