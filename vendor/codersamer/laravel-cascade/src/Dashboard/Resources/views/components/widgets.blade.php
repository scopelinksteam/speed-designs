<div class="row">
    @foreach ($widgets as $widget)
    <div class="@if($allow_columns) 'col-'.{{$widget->GetColumns()}} @endif">
        @include($widget->view(), $widget->GetValues())
    </div>
    @endforeach
</div>
