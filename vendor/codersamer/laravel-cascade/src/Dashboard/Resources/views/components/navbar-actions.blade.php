@foreach ($actions as $action)
<li class="nav-item {{ $action->GetClasses() }}">
    @include($action->view(), $action->GetValues())
</li>
@endforeach
