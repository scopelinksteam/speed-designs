{{-- Tabs Navigation --}}
@if(count($pages) > 0)
<div class="pt-3 mb-3 pb-0 bg-white rounded-3">
<ul class="nav nav-tabs px-3">
    @foreach ($pages as $tab)
    <li class="nav-item">
        <button class="nav-link {{$loop->iteration == 1 ? 'active' : ''}}" type="button" data-bs-toggle="tab" data-bs-target="#{{$tab->slug()}}-tab-content">{{$tab->name()}}</button>
    </li>
    @endforeach
</ul>
</div>
<div class="tab-content">
    @foreach($pages as $tab)
    <div class="tab-pane {{$loop->iteration == 1 ? 'active' : ''}}" id="{{$tab->slug()}}-tab-content">
        {!! $tab->render() !!}
    </div>
    @endforeach
</div>
@endif
