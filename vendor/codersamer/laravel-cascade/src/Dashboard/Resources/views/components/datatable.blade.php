<div>
    <div class="toolbar">
        <form class="form">
          <div class="row justify-content-between mb-2">
            <div class="form-group col-auto mr-auto">
              <label class="my-1 mr-2 sr-only" for="inlineFormCustomSelectPref1">Show</label>
              <select wire:model="per_page" class="form-select" id="inlineFormCustomSelectPref1">
                <option value="10">10</option>
                <option value="15">15</option>
                <option value="25">25</option>
                <option value="50">50</option>
                <option value="100">100</option>
              </select>
            </div>
            <div class="form-group col">

              <label for="search" class="sr-only">Search</label>
              <input type="text" class="form-control" id="search1" value="" wire:model="search_term" placeholder="Search">
            </div>
            @if(count($this->tableInstance->GetFilters()) > 0)
            <div class="col-auto">
                <a data-bs-target="#datatable-filters" class="btn" data-bs-toggle="collapse"><span><i class="fas fa-filter"></i></span></a>
            </div>
            @endif
          </div>
        </form>
    </div>
      <!-- table -->
    @if(count($this->tableInstance->GetFilters()) > 0)
    {{-- Filters Card --}}
    <div class="card collapse" id="datatable-filters" wire:ignore.self>
        <div class="card-body">
            <div class="row">

                @foreach ($this->tableInstance->GetFilters() as $filter)
                <div class="col-3">
                    <div class="form-group">
                        <label for="filter-{{ $filter->GetName() }}" class="form-label">{{ $filter->GetTitle() }}</label>
                        {!! $filter->render() !!}
                    </div>
                </div>
                @endforeach

            </div>
            {{-- End: Filters Card --}}

        </div>
    </div>
    @endif
    <div class="table-responsive">

        <table class="table table-bordered table-striped" id="{{\Str::slug(get_class($this->tableInstance))}}">
            <thead>
                <tr>
                <th>
                    <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" id="all2">
                    <label class="custom-control-label" for="all2"></label>
                    </div>
                </th>
                @foreach ($this->columns as $column)
                <th>{{ $column->GetText() }}</th>
                @endforeach
                @if($this->tableInstance->HasActions()) <th><i class="fas fa-ellipsis-h"></i></th> @endif
                </tr>
            </thead>
        <tbody>
            @foreach ($this->entries as $entry)
            @php $columnsCount = 0; @endphp
            <tr class="{{ $entry->row_class }}">
            <td>
                <div class="custom-control custom-checkbox">
                <input type="checkbox" class="custom-control-input" id="{{ $entry->id }}">
                <label class="custom-control-label" for="{{ $entry->id }}"></label>
                </div>
            </td>
            @foreach ($this->columns as $column)
            <td>
                @php $columnsCount++; @endphp
                {!! $this->tableInstance->GetColumnValue($column, $entry) !!}
            </td>
            @endforeach
            @if($this->tableInstance->HasActions())
            <td>
                <div class="btn-group btn-group-sm">
                    @foreach($this->tableInstance->Extender()->parseActions($this->tableInstance->actions($entry), $entry) as $action)
                    @if(!$action->CanShow()) @continue @endif
                    @if($action->HasLink())
                    <a class="btn btn-outline-secondary" href="{{ $action->GenerateLink($entry) }}">{{ $action->GetText() }}</a>
                    @else
                    <a class="btn btn-outline-secondary" href="#" wire:click.prevent="ExecuteAction('{{$action->GetName()}}', {{$entry->id}}, '{{addslashes(get_class($entry))}}')">{{ $action->GetText() }}</a>
                    @endif
                    @endforeach
                </div>
            </td>
            @endif
            </tr>
            @endforeach
        </tbody>
        </table>
    </div>
    <nav aria-label="Table Paging" class="mb-0 text-muted justify-content-center">
        {!! $this->entries->links() !!}
    </nav>
</div>
