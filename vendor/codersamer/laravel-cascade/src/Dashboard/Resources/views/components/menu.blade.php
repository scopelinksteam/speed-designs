@if($menu == null)

@else
    @foreach ($menu->GetRenderItems() as $section)
    @if(!$section->HasItems()) @continue @endif
    <p class="text-muted nav-heading mt-4 mb-1" id="menu-section-{{ $section->GetId() }}">
        <span>{{ $section->GetTitle() }}</span>
    </p>
    <ul class="navbar-nav flex-fill w-100 mb-2">
        @foreach ($section->GetItems() as $item)
        @if(!$item->CanShow()) @continue @endif
        <li class="nav-item {{ $item->HasChildren() ? 'dropdown' : '' }}">
            <a href="{{ $item->GetLink() }}" @if($item->HasChildren()) data-toggle="collapse"  aria-expanded="false" @endif class="{{ $item->HasChildren() ? 'dropdown-toggle' : '' }} nav-link">
            <i class="fe fe-{{ $item->GetIcon() }} fe-16"></i>
            <span class="ml-3 item-text">{{ $item->GetTitle() }}</span><span class="sr-only">(current)</span>
            @if($item->HasBadge())
            <span class="badge badge-pill badge-{{ $item->GetBadgeType() }}">{{ $item->GetBadge() }}</span>
            @endif
            </a>
            @if($item->HasChildren())
            <ul class="collapse list-unstyled pl-4 w-100" id="{{ $item->GetId() }}">

                @foreach ($item->GetChildren() as $child)

                @if(!$child->CanShow())  @continue @endif
                <li class="nav-item"> {{-- .active --}}
                <a class="nav-link pl-3" href="{{ $child->GetLink() }}"><span class="ml-1 item-text">{{ $child->GetTitle() }}</span></a>
                </li>
                @endforeach
            </ul>
            @endif
        </li>
        @endforeach
    </ul>
    @endforeach
@endif
