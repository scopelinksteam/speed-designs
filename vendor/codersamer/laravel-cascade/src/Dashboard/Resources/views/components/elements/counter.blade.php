<div class="col-md-6 col-xl-3 mb-4">
    <div class="card shadow">
      <div class="card-body">
        <div class="row align-items-center">
          <div class="col-3 text-center">
            <span class="circle circle-sm bg-primary">
              <i class="fe fe-16 fe-{{ $icon }} text-white mb-0"></i>
            </span>
          </div>
          <div class="col">
            <p class="small text-muted mb-0">{{ $slot }}</p>
            <span class="h3 mb-0">{{ $value }}</span>
          </div>
        </div>
      </div>
    </div>
</div>
