<div wire:ignore>
    <select name="filters-{{ $filter->GetName() }}" id="filters-{{ $filter->GetName() }}" class="form-control selectr" wire:model="filters.{{ $filter->GetName() }}" @if($filter->IsMultiple()) multiple @endif>
        @if(!$filter->IsMultiple())
        <option value="">@lang('Any')</option>
        @endif
        @foreach ($filter->GetValues() as $key => $val)
        <option value="{{ $key }}">{{ $val }}</option>
        @endforeach
    </select>
</div>