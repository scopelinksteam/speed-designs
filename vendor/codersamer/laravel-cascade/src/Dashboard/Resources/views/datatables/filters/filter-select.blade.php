<div wire:ignore>
    <select name="filters-{{ $filter->GetName() }}" id="filters-{{ $filter->GetName() }}" class="form-control selectr" wire:model="filters.{{ $filter->GetName() }}">
        <option value="">@lang('Any')</option>
        @foreach ($filter->GetValues() as $key => $val)
        <option value="{{ $key }}">{{ $val }}</option>
        @endforeach
    </select>
</div>