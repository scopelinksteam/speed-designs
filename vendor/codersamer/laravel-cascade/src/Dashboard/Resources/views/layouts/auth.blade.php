<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="favicon.ico">
    <title>@yield('title', __('Authentication'))</title>
    <!-- Simple bar CSS -->
    {{ dashboard_render_styles(); }}
    @yield('styles')
  </head>
  <body class="" style="overflow: hidden">
    <div class="wrapper vh-100">
      <div class="row align-items-center h-100">
        @if (Route::is('register'))
        <div class="col-lg-6 col-md-8 col-10 mx-auto">
        @else
        <div class="col-lg-3 col-md-4 col-10 mx-auto text-center">
        @endif
          <a class="navbar-brand mx-auto mt-2 flex-fill text-center mb-4" href="{{ url('/') }}">
            <img class="navbar-brand-img brand-sm" id="logo" src="{{ get_setting('system::general.logo', null) == null ? url('cascade-logo.png') : url('uploads/'.get_setting('system::general.logo', null))}}" alt="">
          </a>
          @yield('content')
          <p class="mt-5 mb-3 text-muted">© {{ now()->format('Y') }}</p>
        </div>
      </div>
    </div>
    {{ dashboard_render_scripts() }}
    @yield('scripts')
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-56159088-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];

      function gtag()
      {
        dataLayer.push(arguments);
      }
      gtag('js', new Date());
      gtag('config', 'UA-56159088-1');
    </script>
  </body>
</html>
</body>
</html>
