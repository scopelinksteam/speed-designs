<?php

namespace Cascade\Dashboard\Abstraction;

use Cascade\Dashboard\Traits\HasTables;
use Cascade\Dashboard\Traits\HasTabs;

abstract class DashboardPage
{
    use HasTabs, HasTables;
}
