<?php

namespace Cascade\Dashboard\Abstraction;

use Closure;

abstract class NavbarAction
{

    public $order = 0;

    public $classes = '';

    protected $values = null;

    public function order($order) { $this->order = $order; return $this; }
    public function GetOrder() { return $this->order; }

    public function classes($classes) { $this->classes = $classes; return $this; }
    public function GetClasses() { return $this->classes; }

    public function values(Closure $valuesCallback) { $this->values = $valuesCallback; return $this;}
    public function GetValues() { return is_callable($this->values) ? call_user_func($this->values) : []; }

    abstract function view();
}
