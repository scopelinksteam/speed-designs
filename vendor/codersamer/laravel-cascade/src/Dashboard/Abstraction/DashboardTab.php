<?php

namespace Cascade\Dashboard\Abstraction;

use Closure;
use Illuminate\View\View;

abstract class DashboardTab
{
    public array $data = [];

    public abstract function name() : String;
    public abstract function slug() : String;
    public abstract function view() : View|String;

    public function order() : int { return 1; }
    public function show() : bool { return true; }
    public function render()
    {
        $view = $this->view();
        return $view instanceof View ? $view->with($this->data)->render() : $view;
    }
}
