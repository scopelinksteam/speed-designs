<?php

namespace Cascade\Dashboard\Abstraction;

use Closure;
use Illuminate\Support\Facades\Gate;

abstract class DashboardWidget
{
    public $order = 0;

    protected $title = '';

    protected $id = null;

    protected $permission = null;

    protected $columns = 4;

    protected $values = null;

    public function order($order = null) { if($order === null) { return $this->order; } $this->order = $order; return $this; }

    public function id($id = null)
    {
        if($id === null)
        {
            $this->id = $this->id == null ? str(static::class)->slug() : $this->id;
            return $this->id;
        }
        $this->id = $id; return $this;
    }

    public function title($title = null) { if($title === null) { return $this->title; } $this->title = $title; return $this; }

    public function permission($permission) { $this->permission = $permission; return $this; }
    public function GetPermission() { return $this->permission; }
    public function CanShow() { return $this->permission == null ? true : Gate::allows($this->permission); }

    public function columns($columns = null) { if($columns === null) { return $this->columns; } $this->columns = $columns; return $this; }

    abstract function render();
}
