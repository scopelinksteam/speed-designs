<?php

namespace Cascade\Dashboard\Abstraction;

use LivewireUI\Modal\Contracts\ModalComponent as ContractsModalComponent;
use LivewireUI\Modal\ModalComponent;

abstract class DashboardModal extends ModalComponent implements ContractsModalComponent
{
    protected static array $maxWidths = [
        'sm'  => 'modal-sm',
        'md'  => '',
        'lg'  => 'modal-lg',
        'xl'  => 'modal-xl',
        'fs' => 'modal-fullscreen'
    ];

    public function title() : String { return ''; }

    public static function modalMaxWidth(): string
    {
        return 'md';
    }
}
