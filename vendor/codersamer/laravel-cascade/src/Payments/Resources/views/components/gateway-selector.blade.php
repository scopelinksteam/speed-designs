<div>
    @foreach ($this->gateways as $gateway)
    <div class="form-group">
        <input id="gateway-{{$gateway->GetAlias()}}" style="height: 20px;width:20px;float:left; margin-right:20px;" type="radio" name="gateway" id="gateway" class="form-control" value="{{$gateway->GetAlias()}}" wire:model="selectedGateway" required>
        <label for="gateway-{{$gateway->GetAlias()}}">{{$gateway->GetName()}}</label>
        <br>
        <span>{{implode(', ', $gateway->GetMethods())}}</span>
    </div>
    @if($selectedGateway == $gateway->GetAlias() && $gateway->HasFields())
    {!! $gateway->RenderFields()->render() !!}
    @endif
    @endforeach
</div>
