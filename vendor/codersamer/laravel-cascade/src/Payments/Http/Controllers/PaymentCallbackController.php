<?php

namespace Cascade\Payments\Http\Controllers;

use Cascade\System\Services\Action;
use Exception;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Cascade\Payments\Actions\PaymentCallbackHandled;
use Cascade\Payments\Services\PaymentService;

class PaymentCallbackController extends Controller
{
    public function index($gateway, Request $request)
    {
        $gateway = PaymentService::Resolve($gateway);
        if($gateway)
        {
            $transaction = $gateway->HandleCallback($request);
            Action::Apply(PaymentCallbackHandled::class, $transaction);
        }
        return redirect()->to('/');
    }
}
