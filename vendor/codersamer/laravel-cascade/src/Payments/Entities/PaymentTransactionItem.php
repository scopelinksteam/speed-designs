<?php

namespace Cascade\Payments\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class PaymentTransactionItem extends Model
{
    protected $table = 'payments_transactions_items';

    public function transaction()
    {
        return $this->belongsTo(PaymentTransaction::class, 'transaction_id');
    }

    public function bindable()
    {
        return $this->morphTo('bindable');
    }


}
