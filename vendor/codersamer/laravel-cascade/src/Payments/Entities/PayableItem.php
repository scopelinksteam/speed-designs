<?php

namespace Cascade\Payments\Entities;

use Illuminate\Database\Eloquent\Model;
use Cascade\Payments\Contracts\IPayableItem;

class PayableItem implements IPayableItem
{
    protected Model $item;
    protected String $name = '';
    protected float $unitPrice = 0;
    protected float $quantity = 0;
    protected float $totalPrice = 0;

    public function __construct(Model $item) { $this->item = $item; }

    public function name(String $name = null) { $this->name = $name; return $this; }
    public function unitPrice(float $unitPrice) { $this->unitPrice = $unitPrice; return $this; }
    public function quantity(float $quantity) { $this->quantity = $quantity; return $this; }
    public function totalPrice(float $total) { $this->totalPrice = $total; return $this; }

    public function GetName(): string { return $this->name; }

    public function GetQuantity(): float { return $this->quantity; }

    public function GetUnitPrice(): float { return $this->unitPrice; }

    public function GetTotalPrice(): float
    {
        if($this->totalPrice == 0)
        { $this->totalPrice = $this->GetQuantity() * $this->GetUnitPrice(); }
        return $this->totalPrice;
    }

    public function GetReference(): Model { return $this->item; }
}
