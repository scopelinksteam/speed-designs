<?php

namespace Cascade\Payments\Entities;

use Cascade\System\Entities\Currency;
use Cascade\System\Traits\HasAttributes;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Cascade\Payments\Contracts\IPayable;
use Cascade\Payments\Contracts\IPayer;
use Cascade\Payments\Enums\TransactionStatus;
use Cascade\Payments\Services\PaymentService;

class PaymentTransaction extends Model
{
    use HasAttributes;

    protected $table = 'payments_transactions';

    protected $casts = [
        'payload' => 'json',
        'data' => 'json',
        'status' => TransactionStatus::class,
    ];

    public function payer()
    {
        return $this->morphTo('payer');
    }

    public function payable()
    {
        return $this->morphTo('payable');
    }

    public function currency()
    {
        return $this->belongsTo(Currency::class, 'currency_id');
    }

    public function getGatewayAttribute()
    {
        return PaymentService::Resolve($this->gateway_type);
    }

    public function items(){
        return $this->hasMany(PaymentTransactionItem::class, 'transaction_id');
    }

    public function scopeOf(Builder $query, IPayer $payer)
    {
        return $query->where('payer_type', get_class($payer))->where('payer_id', $payer->id);
    }

    public function scopeFor(Builder $query, IPayable $payable)
    {
        return $query->where('payable_type', get_class($payable))->where('payable_id', $payable->id);
    }
}
