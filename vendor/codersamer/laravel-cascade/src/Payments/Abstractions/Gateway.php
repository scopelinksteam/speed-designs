<?php

namespace Cascade\Payments\Abstractions;

use Illuminate\Http\Client\PendingRequest;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\View\View;
use Cascade\Payments\Contracts\IPayable;
use Cascade\Payments\Contracts\IPayableItem;
use Cascade\Payments\Contracts\IPaymentGateway;
use Cascade\Payments\Entities\PaymentTransaction;
use Cascade\Payments\Entities\PaymentTransactionItem;
use Cascade\Payments\Enums\TransactionStatus;

abstract class Gateway implements IPaymentGateway
{


    public function CreateTransaction(IPayable $payable, array $payload = []) : PaymentTransaction
    {
        $transaction = PaymentTransaction::where('payable_type', get_class($payable))->where('payable_id', $payable->id)->first();
        $transaction = $transaction == null ? new PaymentTransaction() : $transaction;
        $transaction->gateway_type = get_class($this);
        $transaction->status = TransactionStatus::Created;
        $transaction->total = $payable->GetTotal();
        $transaction->currency_id = $payable->GetCurrency()->id;
        $transaction->payable_id = $payable->id;
        $transaction->payable_type = get_class($payable);
        $transaction->payer_id = $payable->GetPayer()->id;
        $transaction->payer_type = get_class($payable->GetPayer());
        $transaction->payload = $payload;
        $transaction->save();

        $transaction->items()->delete();

        foreach($payable->GetItems() as $item)
        {
            if(! ($item instanceof IPayableItem)) { continue; }
            $transactionItem = new PaymentTransactionItem();
            $transactionItem->transaction_id = $transaction->id;
            $transactionItem->name = $item->GetName();
            $transactionItem->bindable_id = $item->GetReference()->id;
            $transactionItem->bindable_type = get_class($item->GetReference());
            $transactionItem->item_price = $item->GetUnitPrice();
            $transactionItem->quantity = $item->GetQuantity();
            $transactionItem->total_price = $item->GetTotalPrice();
            $transactionItem->save();
        }
        return $transaction;
    }


    public function GetReturnUrl() : String
    {
        return route('api.payments.return', ['gateway' => $this->GetAlias()]);
    }

    public function GetCallbackUrl() : String
    {
        return route('api.payments.callback', ['gateway' => $this->GetAlias()]);
    }


    public function Pay(IPayable $payable, array $payload = [])
    {
        $transaction = $this->CreateTransaction($payable, $payload);
        $response = $this->ProcessPayment($transaction);
        if($response instanceof View)
        {
            return $response;
        }
        if($response instanceof RedirectResponse)
        {
            $response->send();
            exit;
        }

    }
    /*
    public function HasFields() : bool
    {
        return $this->RenderFields() != null;
    }

    public function RenderFields(): ?View
    {
        return null;
    }

    public function ValidateFields(Request $request): bool
    {
        return true;
    }
    */
    public function Settings() { }
}
