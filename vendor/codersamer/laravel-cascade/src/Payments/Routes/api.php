<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Cascade\Payments\Http\Controllers\PaymentCallbackController;
use Cascade\Payments\Http\Controllers\PaymentReturnController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::ApiGuest(function(){
    Route::any('callback/{gateway}', [PaymentCallbackController::class, 'index'])->name('callback');
    Route::any('return/{gateway}', [PaymentReturnController::class, 'index'])->name('return');    
}, 'payments');