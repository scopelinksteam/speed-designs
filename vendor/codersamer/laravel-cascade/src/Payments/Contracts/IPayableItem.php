<?php

namespace Cascade\Payments\Contracts;

use Illuminate\Database\Eloquent\Model;

interface IPayableItem
{
    function GetName() : String;
    function GetQuantity() : float;
    function GetUnitPrice() : float;
    function GetTotalPrice() : float;
    function GetReference() : Model;
}
