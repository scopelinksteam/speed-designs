<?php

namespace Cascade\Payments\Contracts;

interface IPayer
{
    function GetName() : String;
    function GetAddress();
}
