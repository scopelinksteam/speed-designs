<?php

namespace Cascade\Payments\Contracts;

use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;
use Cascade\Payments\Data\AuthenticationResponse;
use Cascade\Payments\Entities\PaymentTransaction;

/**
 * Payment Gateway Implementation Contract
 *
 * All Gateways should Implement this Contract
 */
interface IPaymentGateway
{
    function GetAlias() : String;
    function GetLogo() : String;
    function GetName() : String;
    //function GetMethods() : array;
    function GetReturnUrl() : String;
    function GetCallbackUrl() : String;
    //function RequiresCard() : bool;
    //function HasFields() : bool;
    //function RenderFields() : ?View;
    //function ValidateFields(Request $request) : bool;
    //function CreateTransaction(IPayable $payable, array $payload = []) : PaymentTransaction;
    function ProcessPayment(PaymentTransaction $transaction) : View|RedirectResponse;
    function HandleCallback(Request $request) : ?PaymentTransaction;
    function HandleReturn(Request $request) : ?PaymentTransaction;

}
