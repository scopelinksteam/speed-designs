<?php

namespace Cascade\Payments\Contracts;

use Cascade\System\Entities\Currency;
use Illuminate\Database\Eloquent\Collection;
use Cascade\Payments\Entities\PaymentTransaction;

interface IPayable
{
    function GetPayer() : IPayer;
    function GetTotal();
    function GetCurrency() : Currency;
    /**
     * Get Payable Items
     *
     * @return IPayableItem[]
     */
    function GetItems() : array|Collection;
    function HandlePayment(PaymentTransaction $transaction);
}
