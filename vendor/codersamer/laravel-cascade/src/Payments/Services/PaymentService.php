<?php

namespace Cascade\Payments\Services;

use Cascade\System\Entities\Currency;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Cascade\Payments\Abstractions\Gateway;
use Cascade\Payments\Contracts\IPayable;
use Cascade\Payments\Contracts\IPaymentGateway;
use Cascade\Payments\Data\AuthenticationResponse;
use Cascade\Payments\Data\Payable;
use Cascade\Payments\Entities\PaymentTransaction;
use Cascade\Payments\Enums\PaymentGateway;
use Cascade\Payments\Gateways\Paypal;

class PaymentService
{

    protected static $gateways = [];

    public static function RegisterGateway(Gateway $gateway)
    {
        static::$gateways[strtolower($gateway->GetAlias())] = $gateway;
    }

    public static function Gateways() { return static::$gateways; }

    public static function Resolve(String $alias) : ?Gateway
    {
        //Find by Alias
        if(isset(static::$gateways[strtolower($alias)]))
        { return static::$gateways[strtolower($alias)]; }

        //Otherwise Find by Class Name
        foreach(static::$gateways as $key => $object)
        { if(strtolower($alias) == strtolower(get_class($object)))  { return $object; }}
        return null;
    }


}
