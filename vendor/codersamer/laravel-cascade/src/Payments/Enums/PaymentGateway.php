<?php

namespace Cascade\Payments\Enums;

enum PaymentGateway : String
{
    case Paypal = 'paypal';
}
