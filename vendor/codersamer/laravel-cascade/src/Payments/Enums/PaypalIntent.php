<?php

namespace Cascade\Payments\Enums;

enum PaypalIntent : String
{
    case Capture = 'CAPTURE';
    case Approve = 'APPROVE';
    case Update = 'UPDATE';
    case Get = 'SELF';
    case Authorize = 'AUTHORIZE';
}
