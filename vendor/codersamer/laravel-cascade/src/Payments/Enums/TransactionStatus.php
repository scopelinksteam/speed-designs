<?php

namespace Cascade\Payments\Enums;

enum TransactionStatus : String
{
    case Preparing  = 'PREPARING';
    case Created = 'CREATED';
    case Pending = 'PENDING';
    case Confirming = 'CONFIRMING';
    case Canceled = 'CANCELED';
    case Failed = 'FAILED';
    case Completed = 'COMPLETED';
}
