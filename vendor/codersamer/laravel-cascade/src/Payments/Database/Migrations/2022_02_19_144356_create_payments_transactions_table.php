<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Cascade\Payments\Enums\TransactionStatus;

class CreatePaymentsTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments_transactions', function (Blueprint $table) {
            $table->id();
            $table->string('gateway_type');
            $table->string('code')->nullable();
            $table->string('gateway_code')->nullable();
            $table->string('status')->default(TransactionStatus::Created->value);
            $table->unsignedDouble('total')->default(0);
            $table->unsignedBigInteger('currency_id')->default(0);
            $table->json('payload')->nullable();
            $table->json('data')->nullable();
            $table->nullableMorphs('payable');
            $table->nullableMorphs('payer');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments_transactions');
    }
}
