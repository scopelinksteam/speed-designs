<?php

return [
    'name' => 'Payments',

    'paypal' => [
        'production' => env('PAYPAL_PRODUCTION', false),
        'client_id' => env('PAYPAL_CLIENT_ID', null),
        'secret' => env('PAYPAL_SECRET', null)
    ]
];
