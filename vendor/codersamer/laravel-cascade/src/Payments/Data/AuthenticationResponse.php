<?php

namespace Cascade\Payments\Data;

class AuthenticationResponse
{
    public ?String $token = null;

    public bool $success = false;

    public array $data = [];

    public ?String $error = null;
}
