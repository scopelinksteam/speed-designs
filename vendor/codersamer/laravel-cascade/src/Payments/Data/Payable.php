<?php

namespace Cascade\Payments\Data;

use Cascade\System\Entities\Currency;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Cascade\Payments\Contracts\IPaymentGateway;
use Cascade\Payments\Entities\PaymentTransaction;
use Cascade\Payments\Entities\PaymentTransactionItem;
use Cascade\Payments\Enums\PaymentGateway;
use Cascade\Payments\Enums\TransactionStatus;
use Ramsey\Uuid\Guid\Guid;

class Payable
{

    protected array $items = [];

    protected Model|Authenticatable|null $payer = null;

    protected ?Model $payable = null;

    protected array $payloads = [];

    protected array $data = [];

    protected function __construct(protected Currency $currency)
    {

    }

    public static function make(Currency $currency)
    {
        return new static($currency);
    }

    public function withItem(String $name, float $price, float $quantity = 1, ?Model $reference = null) : self
    {
        $item = new PaymentTransactionItem();
        $item->name = $name;
        $item->item_price = $price;
        $item->quantity = $quantity;
        $item->total_price = $price * $quantity;
        if($reference != null)
        {
            $item->bindable_id = $reference->id;
            $item->bindable_type = get_class($reference);
        }
        $this->items[] = $item;
        return $this;
    }

    public function withPayer(Model|Authenticatable|null $payer = null) : self
    {
        $this->payer = $payer ?? auth()->user();
        return $this;
    }

    public function withPayload(String $key, mixed $value) : self
    {
        $this->payloads[$key] = $value;
        return $this;
    }

    public function withPayable(Model $model) : self
    {
        $this->payable = $model;
        return $this;
    }

    public function toTransaction(PaymentGateway $gateway) : PaymentTransaction
    {
        $items = collect($this->items);
        $transaction = new PaymentTransaction();
        $transaction->gateway_type = $gateway->value;
        $transaction->code = \Str::orderedUuid();
        $transaction->status = TransactionStatus::Preparing->value;
        $transaction->total = $items->sum('total_price');
        $transaction->currency_id = $this->currency?->id ?? 0;
        $transaction->payload = json_encode($this->payloads);
        $transaction->data = json_encode([]);
        if($this->payable != null)
        {
            $transaction->payable_id = $this->payable->id;
            $transaction->payable_type = get_class($this->payable);
        }

        if($this->payer != null)
        {
            $transaction->payer_id = $this->payer->id;
            $transaction->payer_type = get_class($this->payer);
        }

        $transaction->save();

        foreach($items as $item)
        {
            $item->transaction_id = $transaction->id;
            $item->save();
        }

        return $transaction;
    }
}
