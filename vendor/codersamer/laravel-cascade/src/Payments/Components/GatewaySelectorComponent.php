<?php

namespace Cascade\Payments\Components;

use Cascade\System\Services\System;
use Livewire\Component;
use Cascade\Payments\Services\PaymentService;

class GatewaySelectorComponent extends Component
{

    public $selectedGateway = '';

    public function mount()
    {
        //$this->gateways = PaymentService::Gateways();
    }

    public function getGatewaysProperty()
    {
        return PaymentService::Gateways();
    }

    public function updatedSelectedGateway($value)
    {
        $gateway = PaymentService::Resolve($value);

    }

    public function render()
    {
        return System::FindView('payments::components.gateway-selector');
    }
}
