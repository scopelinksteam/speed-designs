<?php

namespace Cascade\API\Http\Requests;

use Cascade\API\Enums\ResponseMessageType;
use Cascade\API\Services\ApiResponse;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class ApiRequest extends FormRequest
{

    function failedValidation(Validator $validator)
    {
        $response = ApiResponse::make();
        $messages = $validator->getMessageBag();
        foreach($messages->getMessages() as $key => $message)
        {
            $response->message(ResponseMessageType::Error, implode(', ',$message));
        }
        $response->code(400);
        $response->setStatusText('Invalid Request');
        throw new ValidationException($validator, $response);
    }
}
