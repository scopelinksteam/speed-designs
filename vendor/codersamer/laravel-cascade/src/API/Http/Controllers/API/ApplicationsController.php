<?php

namespace Cascade\API\Http\Controllers\API;

use Cascade\API\Entities\IntegrationApplication;
use Cascade\API\Http\Controllers\ApiController;
use Cascade\API\Services\ApiResponse;

class ApplicationsController extends ApiController
{

    public function list()
    {
        return ApiResponse::SendSuccess(__('Valid Request'), IntegrationApplication::all());
    }

    public function get(IntegrationApplication $application)
    {

        return ApiResponse::SendSuccess(__('Valid Request'), $application);
    }

}
