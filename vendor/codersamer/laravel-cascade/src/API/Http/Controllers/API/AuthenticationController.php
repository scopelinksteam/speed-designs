<?php

namespace Cascade\API\Http\Controllers\API;

use Cascade\API\Entities\IntegrationApplication;
use Cascade\API\Enums\HttpStatusCode;
use Cascade\API\Http\Controllers\ApiController;
use Cascade\API\Services\ApiResponse;
use Illuminate\Http\Request;

class AuthenticationController extends ApiController
{

    public function token(Request $request)
    {

        $response = ApiResponse::make();
        if(!$request->filled('app_key'))
        { $response->error(__('App Key is Required and it\'s Missing')); }
        if(!$request->filled('app_secret'))
        { $response->error(__('App Secret is Required and it\'s Missing')); }

        if(!$response->isValid()) { return $response->code(HttpStatusCode::Invalid); }

        $application = IntegrationApplication::where('key', $request->app_key)->first();

        if($application == null || $application->secret != $request->app_secret)
        {
            $response->error(__('Invalid Credentials'));

            return $response->code(HttpStatusCode::Unauthorized);
        }

        $token = $application->createToken('generic');
        return $response->data([
            'token' => $token->plainTextToken,
            'app' => $application
        ]);

    }

    public function me(Request $request)
    {
        return ApiResponse::SendSuccess(__('Valid'), ['user' => auth()->user()]);
    }
}
