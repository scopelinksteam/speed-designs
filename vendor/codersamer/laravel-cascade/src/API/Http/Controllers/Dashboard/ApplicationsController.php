<?php

namespace Cascade\API\Http\Controllers\Dashboard;

use Cascade\API\Entities\IntegrationApplication;
use Cascade\API\Enums\ApplicationStatus;
use Cascade\API\Tables\ApplicationsTable;
use Cascade\System\Services\Feedback;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Gate;

class ApplicationsController extends Controller
{
    public function index()
    {
        return view('api::dashboard.applications.index', [
            'table' => ApplicationsTable::class
        ]);
    }

    public function store(Request $request)
    {
        $valid = true;

        if(!$request->filled('name'))
        {
            Feedback::error(__('Application name is required'));
            $valid = false;
        }

        if(!$valid)
        {
            Feedback::flash();
            return back();
        }

        $entityId = intval($request->input('entity_id', 0));
        $entity = $entityId > 0 ? IntegrationApplication::find($entityId) : new IntegrationApplication();

        $entity->name = $request->name;
        $entity->status = ApplicationStatus::from($request->status);
        if($entityId == 0)
        {
            $entity->generateKey();
            $entity->generateSecret();
        }

        $entity->save();

        Feedback::info(__('Application Key'). ' : '.'<span class="badge badge-light">'.$entity->key.'</span><br />'.__('Application Secret'). ' : '.'<span class="badge badge-light">'.$entity->secret.'</span>');
        Feedback::success(__('Application Saved Successfully'));
        $info = '';
        Feedback::flash();
        return to_route('dashboard.integration.applications.index');
    }

    public function edit(IntegrationApplication $application)
    {
        Gate::authorize('edit integration applications');
        return $this->index()->with(['application' => $application]);
    }

    public function delete(IntegrationApplication $application)
    {
        Gate::authorize('delete integration applications');
        $application->delete();
        Feedback::success(__('Application Deleted Successfully'));
        Feedback::flash();
        return to_route('dashboard.integration.applications.index');
    }
}

