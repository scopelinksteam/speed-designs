<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIntegerationApplicationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('integration_applications', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('key');
            $table->string('secret');
            $table->smallInteger('status');
            $table->timestamp('expire_at')->nullable();
            $table->nullableMorphs('owner');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('integration_applications');
    }
}
