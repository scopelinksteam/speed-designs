<?php

use Illuminate\Routing\Router;
use Illuminate\Support\Facades\Route;

Router::macro('ApiGuest', function(Closure $callback, $slug = null, $name = null){
    if($slug == null)
    {
        Route::as('api.')->middleware(['api', 'guest:sanctum'])->namespace('API')->group($callback);
    }
    else
    {
        $name = ($name == null) ? str_replace('-', '_',$slug) : $name;
        Route::prefix($slug.'/')->as('api.'.$name.'.')->middleware(['api', 'guest:sanctum'])->namespace('API')->group($callback);
    }
});


Router::macro('Api', function(Closure $callback, $slug = null, $name = null){
    if($slug == null)
    {
        Route::as('api.')->middleware(['api','auth:sanctum'])->namespace('API')->group($callback);
    }
    else
    {
        $name = ($name == null) ? str_replace('-', '_',$slug) : $name;
        Route::prefix($slug.'/')->as('api.'.$name.'.')->middleware(['api','auth:sanctum'])->namespace('API')->group($callback);
    }
});
