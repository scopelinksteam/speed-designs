<?php

namespace Cascade\API\Entities;

use Cascade\API\Enums\ApplicationStatus;
use Faker\Factory as Faker;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Sanctum\HasApiTokens;

class IntegrationApplication extends Authenticatable
{
    use HasApiTokens;

    protected $table = 'integration_applications';

    protected $casts = [
        'status' => ApplicationStatus::class
    ];

    public function owner()
    {
        return $this->morphTo('owner');
    }

    public function generateKey()
    {
        do { $this->key = (string)str(Faker::create()->bothify('*****-*****-*****-*****-*****'))->upper(); }
        while(static::where('key', $this->key)->count() > 0);
        return $this->key;
    }

    public function generateSecret()
    {
        do { $this->secret = (string)str(Faker::create()->bothify('**************************************************')); }
        while(static::where('secret', $this->secret)->count() > 0);
        return $this->secret;
    }
}
