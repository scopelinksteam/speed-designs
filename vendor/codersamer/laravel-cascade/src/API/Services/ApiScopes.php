<?php

namespace Cascade\API\Services;

use Cascade\System\Services\System;
use Closure;
use Symfony\Component\Finder\Finder;

class ApiScopes
{
    protected static array $modules = [];

    protected static array $plugins = [];

    protected static array $themes = [];

    protected static array $cascade = [];

    public static function Initialize()
    {
        static::$cascade = static::GetScopesFromPath(base_path('Cascade'));
        static::$modules = static::GetScopesFromPath(base_path('Modules'));
        static::$plugins = static::GetScopesFromPath(base_path('Plugins'));
        static::$themes = static::GetScopesFromPath(base_path('Themes'));
    }

    public static function CascadeScopes() : array { return static::$cascade; }
    public static function ModuleScopes() : array { return static::$modules; }
    public static function PluginsScopes() : array { return static::$plugins; }
    public static function ThemesScopes() : array { return static::$themes; }

    protected static function GetScopesFromPath(String $path, Closure $validateModule = null)
    {
        if(!is_dir($path)) { return []; }
        $directories = (new Finder())->in($path)->depth(0)->directories();
        $foundScopes = [];
        foreach($directories as $directory)
        {
            $directoryPath = (string)$directory;
            $directoryName = basename($directoryPath);
            if($validateModule != null)
            { if(call_user_func($validateModule, $directoryName) === false) { continue; } }
            if(file_exists($directoryPath.'/Config/scopes.php'))
            {
                $scopes = require_once($directoryPath.'/Config/scopes.php');
                if(is_array($scopes) && count($scopes) > 0)
                {
                    $formattedScopes = [];
                    foreach($scopes as $key => $description)
                    {
                        $formattedScopes[(string)str($directoryName)->slug().':'.$key] = $description;
                    }
                    $foundScopes[$directoryName] = $formattedScopes;
                }
            }
            continue;
        }
        return $foundScopes;
    }
}
