<?php

namespace Cascade\API\Services;

use Carbon\Carbon;
use Cascade\API\Enums\HttpStatusCode;
use Cascade\API\Enums\ResponseMessageType;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class ApiResponse extends JsonResponse
{

    protected array $messages = [];

    public static function make() : static
    {
        $object = new static;

        return $object;
    }

    public static function SendError(String|array $message, $code = 400, $status = 'Invalid Request') : static
    {
        $object = new static;
        $object->code($code);
        $object->setStatusText($status);
        $message = is_array($message) ? $message : [$message];
        foreach($message as $entry) { $object->error($entry); }
        return $object;
    }

    public static function SendSuccess(String|array $message, $data = [],  $code = 200, $status = 'OK') : static
    {
        $object = new static;
        $object->code($code);
        $object->setStatusText($status);
        $message = is_array($message) ? $message : [$message];
        foreach($message as $entry) { $object->success($entry); }
        $object->data($data);
        return $object;
    }


    function setContent(?string $content): static
    {
        $valid = $this->isValid();
        $this->content = json_encode([
            'valid' => $valid,
            'issued_at' => Carbon::now(),
            'data' => $this->getData(),
            'messages' => $this->messages
        ]);
        return $this;
    }

    function isValid()
    {
        return (!isset($this->messages[ResponseMessageType::Error->value]) || count($this->messages[ResponseMessageType::Error->value]) == 0);
    }

    public function code(int|HttpStatusCode $code) : self
    {
        $this->statusCode = $code instanceof HttpStatusCode ? $code->value : $code;
        return $this;
    }

    public function message(ResponseMessageType $type, String $message) : self
    {
        if(!isset($this->messages[$type->value])) { $this->messages[$type->value] = []; }
        $this->messages[$type->value][] = $message;
        $this->update();
        return $this;
    }

    public function error(String $message) : self
    {
        return $this->message(ResponseMessageType::Error, $message);
    }

    public function success(String $message) : self
    {
        return $this->message(ResponseMessageType::Success, $message);
    }

    public function data(mixed $data) : self
    {
        $this->setData($data);
        $this->update();
        return $this;
    }

    public function setStatusText(String $text)
    {
        $this->statusText = $text;
    }
}
