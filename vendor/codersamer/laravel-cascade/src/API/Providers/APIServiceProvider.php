<?php

namespace Cascade\API\Providers;

use Cascade\API\Entities\AccessToken;
use Cascade\API\Services\ApiScopes;
use Cascade\Dashboard\Entities\MenuItem;
use Cascade\Dashboard\Services\DashboardBuilder;
use Cascade\Dashboard\Services\DashboardMenu;
use Illuminate\Support\ServiceProvider;
use Laravel\Sanctum\Sanctum;
use Modules\API\Entities\IntegrationApplication;

class APIServiceProvider extends ServiceProvider
{
    /**
     * @var string $moduleName
     */
    protected $moduleName = 'API';

    /**
     * @var string $moduleNameLower
     */
    protected $moduleNameLower = 'api';

    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerTranslations();
        $this->registerConfig();
        $this->registerViews();
        $this->loadMigrationsFrom(module_path($this->moduleName, 'Database/Migrations'));
        $this->overrideModels();
        $this->registerMenus();
        //Register Javascript Variables
        DashboardBuilder::AddJavascriptConstant('BASE_URL', url(''));
        DashboardBuilder::AddJavascriptConstant('API_BASE_URL', url('api'));
    }

    public function registerMenus()
    {
        DashboardBuilder::Menu('system-overview')->Call(function(DashboardMenu $menu){
            $menu->AddItem(
                MenuItem::make(__('Integrations'))->icon('puzzle-piece')->description(__('Manage Integration Applications'))->route('dashboard.integration.applications.index')
            );
        });
    }

    /**
     * Register Auth Guards
     *
     * @return void
     */
    protected function registerGuards()
    {
        $currentGuards = config('auth.providers', []);
        $currentGuards['apps'] = [
            'driver' => 'eloquent',
            'model' => IntegrationApplication::class,
        ];
        config(['auth.providers' => $currentGuards]);
    }
    /**
     * Override System Models
     *
     * @return void
     */
    protected function overrideModels()
    {
        Sanctum::usePersonalAccessTokenModel(AccessToken::class);
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->registerMacros();
        $this->registerGuards();
        ApiScopes::Initialize();
        $this->app->register(RouteServiceProvider::class);
    }

    /**
     * Register Macros found in ../Macros Directory
     *
     * @return void
     */
    public function registerMacros()
    {
        $macros = array_diff(scandir(__DIR__ . '/../Macros/'), ['.', '..']);
        foreach ($macros as $macro) {
            require_once(__DIR__ . '/../Macros/' . $macro);
        }
    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->publishes([
            module_path($this->moduleName, 'Config/config.php') => config_path($this->moduleNameLower . '.php'),
        ], 'config');
        $this->mergeConfigFrom(
            module_path($this->moduleName, 'Config/config.php'), $this->moduleNameLower
        );
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $viewPath = resource_path('views/modules/' . $this->moduleNameLower);

        $sourcePath = module_path($this->moduleName, 'Resources/views');

        $this->publishes([
            $sourcePath => $viewPath
        ], ['views', $this->moduleNameLower . '-module-views']);

        $this->loadViewsFrom(array_merge($this->getPublishableViewPaths(), [$sourcePath]), $this->moduleNameLower);
    }

    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $langPath = resource_path('lang/modules/' . $this->moduleNameLower);

        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, $this->moduleNameLower);
        } else {
            $this->loadTranslationsFrom(module_path($this->moduleName, 'Resources/lang'), $this->moduleNameLower);
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }

    private function getPublishableViewPaths(): array
    {
        $paths = [];
        foreach (\Config::get('view.paths') as $path) {
            if (is_dir($path . '/modules/' . $this->moduleNameLower)) {
                $paths[] = $path . '/modules/' . $this->moduleNameLower;
            }
        }
        return $paths;
    }
}
