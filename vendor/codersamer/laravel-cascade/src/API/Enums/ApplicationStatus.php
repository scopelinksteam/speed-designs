<?php

namespace Cascade\API\Enums;


enum ApplicationStatus : int
{
    case Pending = 0;
    case Active = 1;
    case Disabled = 2;
    case Expired = 3;
}
