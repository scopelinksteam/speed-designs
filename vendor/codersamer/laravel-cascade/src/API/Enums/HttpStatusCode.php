<?php

namespace Cascade\API\Enums;

enum HttpStatusCode : int
{
    case Ok = 200;
    case Invalid = 406;
    case NotFound = 404;
    case Unauthorized = 401;
    case Forbidden = 403;
}
