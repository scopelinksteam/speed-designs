<?php

namespace Cascade\API\Enums;

enum ResponseMessageType : String
{
    case Error = 'error';
    case Success = 'success';
    case Note = 'note';
    case Feedback = 'feedback';
    case Warning = 'warning';
    case Message = 'message';
}
