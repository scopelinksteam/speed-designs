<?php

use Cascade\API\Http\Controllers\Dashboard\ApplicationsController;
use Illuminate\Support\Facades\Route;

Route::dashboard(function(){
    Route::prefix('applications')->as('applications.')->group(function(){
        Route::get('/', [ApplicationsController::class, 'index'])->name('index');
        Route::post('/', [ApplicationsController::class, 'store'])->name('store');
        Route::get('/edit/{application}', [ApplicationsController::class, 'edit'])->name('edit');
        Route::get('/delete/{application}', [ApplicationsController::class, 'delete'])->name('delete');
    });
}, 'integration');
