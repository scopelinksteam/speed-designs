<?php

use Cascade\API\Http\Controllers\API\ApplicationsController;
use Cascade\API\Http\Controllers\API\AuthenticationController;
use Illuminate\Support\Facades\Route;

Route::ApiGuest(function(){
    Route::prefix('auth')->as('auth.')->group(function(){
        Route::post('/token', [AuthenticationController::class, 'token'])->name('token');
    });
}, 'apps');

Route::Api(function(){
    Route::prefix('auth')->as('auth.')->group(function(){
        Route::get('/me', [AuthenticationController::class, 'me'])->name('me');
    });
    Route::get('/list', [ApplicationsController::class, 'list'])->name('list');
    Route::get('/get/{application}', [ApplicationsController::class, 'get'])->name('get');
}, 'apps');
