@extends('admin::layouts.master')
@section('page-title', __('Integration Applications'))
@section('page-description', __('Manage and Create New Integration Applications'))
@section('page-actions')
@endsection
@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <form action="{{ route('dashboard.integration.applications.store') }}" method="POST">
                <div class="card-header">
                    <div class="card-title">
                        <i class="fa fa-cube mr-2"></i>
                        @lang('Application Details')
                    </div>
                </div>
                <div class="card-body">
                    <div class="form-group">
                        <label for="name">@lang('Name')</label>
                        <input type="text" name="name" id="name" class="form-control" value="{{ isset($application) ? $application->name : '' }}">
                    </div>
                    <div class="form-group">
                        <label for="status">@lang('Status')</label>
                        <select name="status" id="status" class="selectr">
                            @foreach(\Cascade\API\Enums\ApplicationStatus::cases() as $case)
                            <option value="{{ $case->value }}" @selected(isset($application) && $application->status == $case)>{{ $case->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="card-footer">
                    @csrf
                    @isset($application)
                    <input type="hidden" name="entity_id" value="{{ $application->id }}">
                    @endisset
                    <button type="submit" class="btn btn-primary">@lang('Save Application')</button>
                </div>
            </form>
        </div>
    </div>
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <div class="card-title">
                    <i class="fa fa-cubes mr-2"></i>
                    @lang('Applications List')
                </div>
            </div>
            <div class="card-body">
                @livewire('datatable', ['table' => $table])
            </div>
        </div>

    </div>
</div>
@endsection
