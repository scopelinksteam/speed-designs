<?php

namespace Cascade\API\Tables;

use Cascade\API\Entities\IntegrationApplication;
use Cascade\Dashboard\Services\Datatables\Action;
use Cascade\Dashboard\Services\Datatables\Column;
use Cascade\Dashboard\Services\Datatables\DataTable;

class ApplicationsTable extends DataTable
{
    public function __construct()
    {

    }

    public function query($searchTerm = null, $filters = [])
    {
        return IntegrationApplication::query();
    }

    public function columns()
    {
        return [
            Column::make('name')->text(__('Name')),
            Column::make('key')->text(__('Key')),
            Column::make('secret')->text(__('Secret')),
            Column::make('status')->text(__('Status')),
            Column::make('expire_at')->text(__('Valid Until'))
        ];
    }

    public function actions()
    {
        $actions = [];

        $actions[] = Action::make('edit', __('Edit'))->route('dashboard.integration.applications.edit', ['application' => 'id'])->permission('edit integration applications');
        $actions[] = Action::make('delete', __('Delete'))->route('dashboard.integration.applications.delete', ['application' => 'id'])->permission('delete integration applications');

        return $actions;
    }
}
