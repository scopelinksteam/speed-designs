<?php

namespace Cascade\Tenancy\Enums;

enum SubscriptionCycle : int
{
    case None = 0;
    case Monthly = 1;
    case Annually = 2;
    case Binually = 3;
    case Daily = 4;
    case Free = 5;
}