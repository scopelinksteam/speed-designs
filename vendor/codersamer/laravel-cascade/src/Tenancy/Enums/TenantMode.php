<?php

namespace Cascade\Tenancy\Enums;

enum TenantMode : int
{
    case Central = 0;
    case Tenant = 1;
    case None = 2;
}
