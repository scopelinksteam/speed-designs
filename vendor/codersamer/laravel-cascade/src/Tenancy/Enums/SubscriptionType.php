<?php

namespace Cascade\Tenancy\Enums;

enum SubscriptionType : int
{
    case None = 0;
    case New = 1;
    case Renew = 2;
    case Upgrade = 3;
    case Trial = 4;
}