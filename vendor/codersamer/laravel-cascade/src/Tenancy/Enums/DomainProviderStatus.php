<?php

namespace Cascade\Tenancy\Enums;

enum DomainProviderStatus : int
{
    case Draft = 0;
    case Pending = 1;
    case Active = 2;
    case Disabled = 3;
    case InsufficientCredit = 4;
}