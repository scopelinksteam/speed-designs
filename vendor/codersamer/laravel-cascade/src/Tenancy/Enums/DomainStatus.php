<?php

namespace Cascade\Tenancy\Enums;

enum DomainStatus : int
{
    case Draft = 0;
    case Pending = 1;
    case Propagating = 2;
    case Active = 3;
    case Disabled = 4;
    case Suspended = 5;
    case Expired = 6;
    case Abandoned = 7;
}
