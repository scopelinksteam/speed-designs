<?php

namespace Cascade\Tenancy\Site\PricingTables;

use Cascade\Site\Abstraction\PricingTable;
use Cascade\Tenancy\Entities\TenantPackage;

class PackagesPricingTable extends PricingTable
{
    public function name(): string
    {
        return __('Tenants Packages');
    }

    public function slug(): string
    {
        return 'tenants-packages';
    }

    public function items(): array
    {
        return TenantPackage::where('is_enabled', '1')->get()->map(function($entry){
            return new PackagesPricingItem($entry);
        })->toArray();
    }
}