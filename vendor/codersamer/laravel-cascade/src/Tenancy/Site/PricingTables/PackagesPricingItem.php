<?php

namespace Cascade\Tenancy\Site\PricingTables;

use Cascade\Site\Abstraction\PricingTableItem;
use Cascade\Tenancy\Entities\TenantPackage;
use Cascade\Tenancy\Enums\TenantMode;
use Cascade\Tenancy\Services\TenancyService;

class PackagesPricingItem extends PricingTableItem
{

    public function __construct(protected TenantPackage $package) { }

    public function name(): string
    {
        return $this->package->name;
    }

    public function id(): int
    {
        return $this->package->id;
    }

    public function price(): float
    {
        return floatval($this->package->monthly_price);
    }

    public function image()
    {
        if(!empty($this->package->image_path))
        {
            $centralTenant = TenancyService::Enabled() ? TenancyService::GetCentralTenant() : null;
            if($centralTenant != null)
            {
                $imagePath = $this->package->image_path;
                $url = TenancyService::ActAs($centralTenant, function() use($imagePath){
                    return uploads_url($imagePath);
                }, TenantMode::Central);
                if(!empty($url))
                { return $url; }
            }
        }
    }

    public function lines(): array
    {
        return $this->package->lines_list;
    }
}