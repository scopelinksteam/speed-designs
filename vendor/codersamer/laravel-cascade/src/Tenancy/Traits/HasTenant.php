<?php

namespace Cascade\Tenancy\Traits;

use Cascade\Tenancy\Entities\Tenant;

trait HasTenant
{

    public static function bootHasTenant()
    {
        //Todo: Add Listeners to Model Events
    }

    public function tenant()
    {
        return $this->belongsTo(Tenant::class, 'tenant_id');
    }
}
