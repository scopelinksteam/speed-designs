<?php

namespace Cascade\Tenancy\Tabs\Packages;

use Cascade\Dashboard\Abstraction\DashboardTab;
use Cascade\System\Services\System;
use Illuminate\View\View;

class BasicPackageTab extends DashboardTab
{
    public function name(): string
    {
        return __('Basic Details');
    }

    public function slug(): string
    {
        return 'package-basic';
    }

    public function view(): View|string
    {
        return System::FindView('tenancy::central.dashboard.packages.tabs.basic');
    }
}