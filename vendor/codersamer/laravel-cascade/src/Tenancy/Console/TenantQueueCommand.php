<?php

namespace Cascade\Tenancy\Console;

use Cascade\Tenancy\Entities\Tenant;
use Cascade\Tenancy\Services\TenancyService;
use Exception;
use Illuminate\Console\Command;
use Illuminate\Database\QueryException;
use Illuminate\Queue\DatabaseQueue;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Queue;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class TenantQueueCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $name = 'tenancy:queue';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        TenancyService::ForEach(function(Tenant $tenant){
            try
            {
                $database = config('database.connections.mysql.database');
                if($database == 'cascade_evolution_license') { return; }
                $this->info(get_setting('system::general.app_name'));
                $this->info("Database : " .$database);
                $this->info("Queue Size : ".count(DB::select('SELECT * FROM jobs')));
                Artisan::call('queue:work --once --daemon --force');
            }
            catch(QueryException $queryException)
            {
                $this->error($queryException->getMessage());
            }
            catch(Exception $ex)
            {
                $this->error($ex->getMessage());
            }
            $this->info(Artisan::output());
        });
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            //['example', InputArgument::REQUIRED, 'An example argument.'],
        ];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null],
        ];
    }
}
