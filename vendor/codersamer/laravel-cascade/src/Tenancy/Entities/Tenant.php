<?php

namespace Cascade\Tenancy\Entities;

use Cascade\Site\Services\Themes;
use Cascade\System\Services\Action;
use Cascade\Tenancy\Actions\TenantCreatedAction;
use Cascade\Tenancy\Actions\TenantCreatingAction;
use Cascade\Tenancy\Enums\TenantMode;
use Cascade\Tenancy\Enums\TenantStatus;
use Cascade\Tenancy\Services\TenancyService;
use Cascade\Tenancy\Services\TenantManager;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Cascade\Tenancy\Entities\TenantEmail;

class Tenant extends Model
{
    protected $table = 'tenants';

    protected $connection = 'central';

    protected $casts = [
        'status' => TenantStatus::class,
        'migrated_at' => 'date',
        'data' => 'array',
        'expire_at' => 'datetime'
    ];

    protected static function boot()
    {
        static::created(function($entity){
            Action::Apply(TenantCreatedAction::class, $entity);
        });

        static::creating(function($entity){
            Action::Apply(TenantCreatingAction::class, $entity);
        });
        parent::boot();
    }

    public function owner()
    {
        return $this->morphTo('owner');
    }

    public function package()
    {
        return $this->morphTo('package');
    }

    public function currency()
    {
        return $this->belongsTo(CentralCurrency::class, 'currency_id');
    }
    
    public function domains()
    {
        return $this->hasMany(TenantDomain::class, 'tenant_id');
    }

    public function emails()
    {
        return $this->hasMany(TenantEmail::class, 'tenant_id');
    }

    public function getManagerAttribute() { return TenantManager::For($this); }

    public function getModulesAttribute()
    {
        $modules = $this->data['modules'] ?? [];
        return $modules;
    }

    public function getPluginsAttribute()
    {
        $plugins = $this->data['plugins'] ?? [];
        return $plugins;
    }

    public function getThemesAttribute()
    {
        $themes = $this->data['themes'] ?? [];
        return $themes;
    }

    public function getDatabaseConfigAttribute()
    {
        $databaseHash = $this->database;
        $database = [];
        $valid = true;
        try
        {
            $databaseString = Crypt::decryptString($databaseHash);
            $database = json_decode($databaseString, true);
        } catch(Exception $ex) { $valid = false; }
        return $database;
    }

    public function getDatabaseUsernameAttribute()
    {
        return $this->database_config['username'] ?? '';
    }

    public function getDatabasePasswordAttribute()
    {
        return $this->database_config['password'] ?? '';
    }

    public function getDatabaseNameAttribute()
    {
        return $this->database_config['database'] ?? '';
    }

    public function getDatabaseHostAttribute()
    {
        return $this->database_config['host'] ?? '';
    }

    public function HasModule($moduleName)
    {return isset($this->modules[$moduleName]); }
    public function ModuleStatus($moduleName)
    { return $this->HasModule($moduleName) ? $this->modules[$moduleName] : false; }
    public function HasModuleEnabled($moduleName)
    { return $this->ModuleStatus($moduleName); }
    public function SetModuleStatus($moduleName, $status, $force = false)
    {
        $modules = $this->modules;
        if(isset($modules[$moduleName]) || TenancyService::ActiveMode() == TenantMode::Central || !config('cascade.multi_tenancy') || $force)
        {
            $data = $this->data;
            $modules[$moduleName] = $status;
            $data['modules'] = $modules;
            $this->data = $data;
            $this->save();
        }
    }

    public function HasPlugin($pluginName)
    {return isset($this->plugins[$pluginName]); }
    public function PluginStatus($pluginName)
    { return $this->HasPlugin($pluginName) ? $this->plugins[$pluginName] : false; }
    public function HasPluginEnabled($pluginName)
    { return $this->PluginStatus($pluginName); }
    public function SetPluginStatus($pluginName, $status, $force = false)
    {
        $plugins = $this->plugins;
        if(isset($plugins[$pluginName])  || TenancyService::ActiveMode() == TenantMode::Central || $force)
        {
            $data = $this->data;
            $plugins[$pluginName] = $status;
            $data['plugins'] = $plugins;
            $this->data = $data;
            $this->save();
        }
    }

    public function SetTheme($name)
    {
        $data = $this->data;
        $data['theme'] = $name;
        $this->data = $data;
        $this->save();
    }

    public function CurrentTheme()
    {
        $name = isset($this->data['theme']) ? $this->data['theme'] : null;
        if($name == null){ return null; }
        return Themes::GetTheme($name);
    }

    public function CurrentThemeName()
    {
        return isset($this->data['theme']) ? $this->data['theme'] : null;
    }

    public function SetFeature($feature, $status = true)
    {
        $data = $this->data;
        $data['features'] = isset($data['features']) ? $data['features'] : [];
        $data['features'][strtoupper(str_replace(' ', '_', $feature))] = (bool)$status;
        $this->data = $data;
        $this->save();
    }

    public function SetOption($option, $value)
    {
        $data = $this->data;
        $data['options'] = isset($data['options']) ? $data['options'] : [];
        $data['options'][strtoupper(str_replace(' ', '_', $option))] = serialize($value);
        $this->data = $data;
        $this->save();
    }

    public function GetOption($option, $default = null)
    {
        $data = $this->data;
        if(!isset($data['options'])) { return $default; }
        if(!isset($data['options'][strtoupper(str_replace(' ', '_', $option))])) { return $default; }
        return unserialize($data['options'][strtoupper(str_replace(' ', '_', $option))]);
    }

    public function HasFeature($feature)
    {
        $data = $this->data;
        if(!isset($data['features'])) { return false; }
        if(!isset($data['features'][strtoupper(str_replace(' ', '_', $feature))])) { return false; }
        return $data['features'][strtoupper(str_replace(' ', '_', $feature))];
    }

    public function GetStorageSize()
    {
        $file_size = 0;
        try
        {
            foreach( File::allFiles(public_path('/uploads/'.$this->slug)) as $file)
            {
                $file_size += $file->getSize();
            }
        } catch(Exception $ex) {}
        return number_format($file_size / 1024 / 1024,2);
    }
}
