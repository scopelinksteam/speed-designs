<?php

namespace Cascade\Tenancy\Entities;

use Cascade\System\Entities\Currency;

class CentralCurrency extends Currency {
    protected $connection = 'central';
}