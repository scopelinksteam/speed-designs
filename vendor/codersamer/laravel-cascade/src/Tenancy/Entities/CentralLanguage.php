<?php

namespace Cascade\Tenancy\Entities;

use Cascade\Translations\Entities\Language;

class CentralLanguage extends Language {
    protected $connection = 'central';
}