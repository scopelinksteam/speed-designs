<?php

namespace Cascade\Tenancy\Entities;

use Cascade\Tenancy\Enums\DomainStatus;
use Illuminate\Database\Eloquent\Model;

class TenantDomain extends Model
{

    protected $table = 'domains';

    protected $connection = 'central';

    protected $casts = [
        'status' => DomainStatus::class
    ];

    public function tenant()
    {
        return $this->belongsTo(Tenant::class, 'tenant_id');
    }

    public function currency()
    {
        return $this->belongsTo(CentralCurrency::class, 'currency_id');
    }

}
