<?php

namespace Cascade\Tenancy\Entities;

use Cascade\System\Traits\HasAttributes;
use Cascade\System\Traits\HasPrices;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Spatie\Translatable\HasTranslations;

class TenantPackage extends Model
{

    use HasTranslations, HasAttributes, HasPrices;

    protected $table = 'tenants_packages';

    protected $connection = 'central';

    public $translatable = [
        'name', 'description', 'lines', 'label'
    ];

    protected $casts = [
        'monthly_price' => 'double',
        'is_popular' => 'boolean',
        'is_enabled' => 'boolean',
    ];

    public function getMonthlyPriceAttribute()
    {
        return $this->GetPriceValue('monthly_price');
    }
    public function getLinesListAttribute()
    {
        $lines = $this->lines;
        return explode("\n", $lines);
    }

    public function currency()
    {
        return $this->belongsTo(CentralCurrency::class, 'currency_id');
    }

    public function GetOption($optionName, $default = null)
    {
        return $this->Attribute('options_'.$optionName, $default);
    }

    public function SetOption($optionName, $value)
    {
        return $this->storeAttribute('options_'.$optionName, $value);
    }
}
