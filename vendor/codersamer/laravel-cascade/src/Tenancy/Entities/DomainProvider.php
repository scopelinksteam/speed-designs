<?php

namespace Cascade\Tenancy\Entities;

use Cascade\System\Traits\HasAttributes;
use Cascade\Tenancy\Enums\DomainProviderStatus;
use Illuminate\Database\Eloquent\Model;

class DomainProvider extends Model
{

    use HasAttributes;
    
    protected $table = 'domains_providers';

    protected $connection = 'central';

    protected $casts = [
        'status' => DomainProviderStatus::class,
    ];
}