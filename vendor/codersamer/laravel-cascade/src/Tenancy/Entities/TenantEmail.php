<?php

namespace Cascade\Tenancy\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class TenantEmail extends Model
{
    protected $table = 'tenants_emails';

    protected $connection = 'central';

    public function tenant()
    {
        return $this->belongsTo(Tenant::class, 'tenant_id');
    }
}
