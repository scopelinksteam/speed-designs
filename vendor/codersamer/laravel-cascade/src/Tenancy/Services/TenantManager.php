<?php

namespace Cascade\Tenancy\Services;

use Cascade\Tenancy\Entities\Tenant;
use Exception;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class TenantManager
{

    protected static $tenants = [];

    public function __construct(protected Tenant $tenant)
    {
        static::$tenants[$tenant->id] = $this;
    }

    public static function For(Tenant $tenant)
    {
        if(!isset(static::$tenants[$tenant->id])) { new TenantManager($tenant); }
        return static::$tenants[$tenant->id];
    }


    public function SetDatabase()
    {
        $databaseHash = $this->tenant->database;
        $database = [];
        $valid = true;
        try
        {
            $databaseString = Crypt::decryptString($databaseHash);
            $database = json_decode($databaseString, true);
        } catch(Exception $ex) { $valid = false; }
        if($valid)
        {
            $tenantConfiguration = config('database.connections.mysql', []);
            $tenantConfiguration['driver'] = $database['driver'] ?? $tenantConfiguration['driver'];
            $tenantConfiguration['port'] = $database['port'] ?? $tenantConfiguration['port'];
            $tenantConfiguration['database'] = $database['database'] ?? $tenantConfiguration['database'];
            $tenantConfiguration['username'] = $database['username'] ?? $tenantConfiguration['username'];
            $tenantConfiguration['password'] = $database['password'] ?? $tenantConfiguration['password'];
            $tenantConfiguration['host'] = $database['host'] ?? $tenantConfiguration['host'];
            config(['database.connections.mysql' => $tenantConfiguration]);
            DB::purge('mysql');
            DB::connection('mysql')->reconnect();
        }


    }

    public static function SwitchDatabase(Tenant $tenant)
    {
        static::For($tenant)->SetDatabase();
    }

    public static function ResetDatabase()
    {
        static::For(TenancyService::CurrentTenant())->SetDatabase();
    }

    public function UpdateDatabase()
    {
        $this->SetDatabase();
        //Set Current Tenant for Modules Activation
        TenancyService::ActAs($this->tenant, function(){
            DB::purge('mysql');
            Schema::connection('mysql')->defaultStringLength(191);
            Artisan::call('migrate');
            Artisan::call('module:migrate');
            DB::purge('mysql');
        });
        $this->tenant->migrated_at = now();
        $this->tenant->save();
    }
}
