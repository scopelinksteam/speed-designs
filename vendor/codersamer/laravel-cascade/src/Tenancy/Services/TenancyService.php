<?php

namespace Cascade\Tenancy\Services;

use Cascade\System\Services\System;
use Cascade\Tenancy\Entities\Tenant;
use Cascade\Tenancy\Entities\TenantDomain;
use Cascade\Tenancy\Enums\DomainStatus;
use Cascade\Tenancy\Enums\TenantMode;
use Cascade\Tenancy\Enums\TenantStatus;
use Cascade\Users\Entities\User;
use Closure;
use Illuminate\Support\Facades\Artisan;

class TenancyService
{

    protected static TenantMode $activeMode = TenantMode::None;

    protected static ?Tenant $activeTenant = null;

    protected static ?TenantDomain $activeDomain = null;

    protected static bool $initialized = false;

    protected static TenantMode $currentUI = TenantMode::None;

    public static function Enabled()
    {
        return config('cascade.multi_tenancy', false) == true;
    }

    public static function Initialize()
    {
        if(static::$initialized || app()->runningInConsole()) { return; }
        static::$initialized = true;
        $request = request();
        $host = $request->getHost();

        //Tenancy Desabled
        if(!config('cascade.multi_tenancy', false))
        {
            static::$initialized = true;
            static::$activeMode = TenantMode::None;
            static::$currentUI = TenantMode::Tenant;
            return;
        }

        $centralDomain = config('cascade.central_domain');

        //Matching Central
        if($centralDomain == $host || 'www.'.$centralDomain == $host)
        {

            static::$activeMode = TenantMode::Central;

            $centralDomain = TenantDomain::where('domain', $host)->first();
            if($centralDomain == null)
            {
                $centralTenant = new Tenant();
                $centralTenant->name = config('app.name');
                $centralTenant->owner_id = 0;
                $centralTenant->package_id = 77;
                $centralTenant->status = TenantStatus::Active;

                $centralTenant->save();
                $centralDomain = new TenantDomain();
                $centralDomain->domain = $host;
                $centralDomain->tenant_id = $centralTenant->id;
                $centralDomain->status = DomainStatus::Active;
                $centralDomain->save();
                static::$activeTenant = $centralTenant;
                static::$activeDomain = $centralDomain;
            }
            else
            {
                static::$activeTenant = $centralDomain->tenant;
                static::$activeDomain = $centralDomain;
            }
        }
        //Regular Tenant ?
        else
        {
            $tenantDomain = TenantDomain::where('domain', $host)->get()->first();
            if($tenantDomain != null)
            {
                $tenant = $tenantDomain->tenant;
                if($tenant)
                {
                    static::$activeMode = TenantMode::Tenant;
                    static::$activeTenant = $tenant;
                    TenantManager::For(static::$activeTenant)->SetDatabase();
                }
                else
                {
                    static::$activeMode = TenantMode::None;
                }
            }
            else
            {
                static::$activeMode = TenantMode::None;
            }
        }

        //Apply Configurations
        if(static::$activeMode == TenantMode::Tenant)
        {
            config([
                'cascade.developer_mode' => static::$activeTenant->HasFeature('developer_mode'),
                'cascade.modules_manager' => static::$activeTenant->HasFeature('modules_manager'),
                'cascade.plugins_manager'  => static::$activeTenant->HasFeature('plugins_manager'),
                'cascade.themes_manager' => static::$activeTenant->HasFeature('themes_manager'),
                'cascade.translation_manager' => static::$activeTenant->HasFeature('translation_manager'),
                'cascade.site_manager' => static::$activeTenant->HasFeature('site_manager'),
                'cascade.sliders_manager' => static::$activeTenant->HasFeature('sliders_manager'),
            ]);
        }

    }

    public static function ActAs(Tenant $tenant, Closure $callback, TenantMode $mode = TenantMode::Tenant)
    {
        $currentTenant = static::CurrentTenant();
        $currentMode = static::$activeMode;
        static::$activeTenant = $tenant;
        static::$activeMode = $mode;
        $tenant->manager->SetDatabase();
        $value = $callback($tenant);
        static::$activeTenant = $currentTenant;
        static::$activeMode = $currentMode;
        static::$activeTenant->manager->SetDatabase();
        return $value;
    }

    public static function GetCentralTenant()
    {
        $centralDomain = config('cascade.central_domain');
        $centralDomain = TenantDomain::where('domain', $centralDomain)->get()->first();
        $tenant = null;
        if($centralDomain != null)
        { $tenant = $centralDomain->tenant; }
        else
        {
            $centralTenant = new Tenant();
            $centralTenant->name = 'Central';
            $centralTenant->slug = config('cascade.central_domain');
            $centralTenant->database = null;
            $centralTenant->owner_id = 0;
            $centralTenant->package_id = 0;
            $centralTenant->status = TenantStatus::Active;
            $centralTenant->data = [];
            $centralTenant->save();
            $centralDomain = new TenantDomain();
            $centralDomain->domain = config('cascade.central_domain');
            $centralDomain->tenant_id = $centralTenant->id;
            $centralDomain->status = DomainStatus::Active;
            $centralDomain->save();
            $tenant = $centralTenant;
        }
        return $tenant;
    }

    public static function ActiveMode() : TenantMode
    {
        return static::$activeMode;
    }

    public static function IsCentral() : bool
    {
        return static::$activeMode == TenantMode::Central;
    }

    public static function CurrentTenant() : ?Tenant
    {
        if(static::$activeMode == TenantMode::None) { return null; }
        if(static::$activeTenant == null)  { return static::GetCentralTenant(); }
        return static::$activeTenant;
    }

    public static function SetUIMode(TenantMode $mode)
    {
        static::$currentUI = $mode;
    }

    public static function GetUIMode() : TenantMode
    {
        return static::$currentUI;
    }

    public static function ForEach(Closure $callback)
    {
        $tenants = Tenant::all();
        foreach($tenants as $tenant)
        {
            static::ActAs($tenant, $callback);
        }
    }

}
