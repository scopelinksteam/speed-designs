<?php

use Cascade\Tenancy\Http\Middleware\EnsureCentralRequest;
use Illuminate\Routing\Router;
use Illuminate\Support\Facades\Route;

Router::macro('hybrid', function(Closure $callback, $slug = null, $name = null){
    $prefix = is_central() ? 'dashboard' : get_setting('system::dashboard.url_prefix', 'dashboard');
    if($slug == null)
    {
        Route::prefix($prefix)->as('dashboard.')->middleware(['auth', 'can:view dashboard'])->namespace('Dashboard')->group($callback);
    }
    else
    {
        $name = ($name == null) ? str_replace('-', '_',$slug) : $name;
        Route::prefix($prefix.'/'.$slug)->as('dashboard.'.$name.'.')->middleware(['auth', 'can:view dashboard'])->namespace('Dashboard')->group($callback);
    }
});
