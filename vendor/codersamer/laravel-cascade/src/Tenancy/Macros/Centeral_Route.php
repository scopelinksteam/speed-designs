<?php

use Cascade\Tenancy\Http\Middleware\EnsureCentralRequest;
use Illuminate\Routing\Router;
use Illuminate\Support\Facades\Route;

Router::macro('central', function(Closure $callback, $slug = null, $name = null){
    $prefix = 'dashboard'; //get_setting('system::central.url_prefix', 'central');
    if($slug == null)
    {
        Route::prefix($prefix)->as('central.')->middleware([EnsureCentralRequest::class, 'auth', 'can:view central'])->namespace('Central')->group($callback);
    }
    else
    {
        $name = ($name == null) ? str_replace('-', '_',$slug) : $name;
        Route::prefix($prefix.'/'.$slug)->as('central.'.$name.'.')->middleware([EnsureCentralRequest::class, 'auth', 'can:view central'])->namespace('Central')->group($callback);
    }
});
