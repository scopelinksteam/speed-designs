<?php

namespace Cascade\Tenancy\Providers;

use Cascade\Dashboard\Entities\MenuItem;
use Cascade\Dashboard\Entities\MenuSection;
use Cascade\Dashboard\Entities\Statistic;
use Cascade\Dashboard\Services\DashboardBuilder;
use Cascade\Dashboard\Services\DashboardMenu;
use Cascade\System\Actions\InitializeDisksAction;
use Cascade\System\Enums\BootstrapColors;
use Cascade\System\Services\Action;
use Cascade\System\Services\Filter;
use Cascade\Tenancy\Console\TenantCommand;
use Cascade\Tenancy\Entities\Tenant;
use Cascade\Tenancy\Entities\TenantDomain;
use Cascade\Tenancy\Http\Middleware\TenantMiddleware;
use Cascade\Tenancy\Services\TenancyService;
use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;
use Illuminate\Support\Facades\Artisan;
use Cascade\Tenancy\Console\TenantQueueCommand;
use Cascade\Tenancy\Entities\TenantPackage;
use Cascade\Tenancy\Enums\TenantMode;
use Cascade\Tenancy\Handlers\Actions\ActivateThemeHandler;
use Cascade\Tenancy\Pages\PackageUpsertPage;
use Cascade\Tenancy\Subscribers\ModulesSubscriber;
use Cascade\Tenancy\Subscribers\ThemesSubscriber;
use Cascade\Tenancy\Tabs\Packages\BasicPackageTab;
use Exception;
use SebastianBergmann\CodeCoverage\Report\Html\Dashboard;

class TenancyServiceProvider extends ServiceProvider
{
    /**
     * @var string $moduleName
     */
    protected $moduleName = 'Tenancy';

    /**
     * @var string $moduleNameLower
     */
    protected $moduleNameLower = 'tenancy';

    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerTranslations();
        $this->registerConfig();
        $this->registerViews();
        $this->loadMigrationsFrom(module_path($this->moduleName, 'Database/Migrations'));
        if(!$this->app->runningInConsole())
        {
            $this->registerMenu();
            $this->registerMiddlewares();
            $this->registerStatistics();
            PackageUpsertPage::AddTab(BasicPackageTab::class);
        }
    }

    protected function registerStatistics()
    {
        DashboardBuilder::Central(function(){
            try
            {
                DashboardBuilder::AddStatistic(Statistic::make(__('Tenants'), Tenant::count())->icon('cubes')->color(BootstrapColors::Cyan));
                DashboardBuilder::AddStatistic(Statistic::make(__('Domains'), TenantDomain::count())->icon('cubes')->color(BootstrapColors::Red));
                DashboardBuilder::AddStatistic(Statistic::make(__('Packages'), TenantPackage::count())->icon('list')->color(BootstrapColors::Red));                
            }
            catch(Exception $ex)
            {
                
            }
        });
    }

    protected function registerMenu()
    {
        DashboardBuilder::Central(function(){
            DashboardBuilder::MainMenu()->Call(function(DashboardMenu $menu){
                $menu->AddItem(
                    MenuItem::make(__('Tenants'))->route('central.tenants.index')->icon('cubes')
                );
                
                $menu->AddItem(
                    MenuItem::make(__('Packages'))->route('central.tenants.packages.index')->icon('cube')
                );
                /*
                $menu->AddItem(
                    MenuItem::make(__('Domains'))->route('central.tenants.index')->icon('at')
                );
                $menu->AddItem(
                    MenuItem::make(__('Settings'))->route('central.tenants.index')->icon('cogs')
                );
                */
            });
        });
    }

    /**
     * Register Macros found in ../Macros Directory
     *
     * @return void
     */
    public function registerMacros()
    {
        $macros = array_diff(scandir(__DIR__ . '/../Macros/'), ['.', '..']);
        foreach ($macros as $macro) {
            require_once(__DIR__ . '/../Macros/' . $macro);
        }
    }

    /**
     * Register Middlewares
     *
     * @return void
     */
    public function registerMiddlewares()
    {
        $router = $this->app['router'];

        $router->pushMiddlewareToGroup('web', TenantMiddleware::class);
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        TenancyService::Initialize();
        Action::Subscribe(ActivateThemeHandler::class);
        Action::On(InitializeDisksAction::class, function(){
            //Uploads Disks
        if(TenancyService::Enabled())
        {
            $tenant = TenancyService::CurrentTenant();
            if($tenant != null)
            {
                $tenantSlug = TenancyService::ActiveMode() == TenantMode::Central
                        ? 'central'
                        : $tenant->slug;
                $tenantSlug = empty($tenantSlug) ? str($tenant->name)->slug() : $tenantSlug;
                $uploadsDisk = config('filesystems.disks.uploads');
                $uploadsDisk['root'] = public_path('uploads/'.$tenantSlug);
                config(['filesystems.disks.uploads' => $uploadsDisk]);
            }
        }
        });
        $this->registerMacros();
        if($this->app->runningInConsole())
        {
            $this->commands([
                TenantCommand::class,
                TenantQueueCommand::class
            ]);
        }
        $this->app->register(RouteServiceProvider::class);
    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->publishes([
            module_path($this->moduleName, 'Config/config.php') => config_path($this->moduleNameLower . '.php'),
        ], 'config');
        $this->mergeConfigFrom(
            module_path($this->moduleName, 'Config/config.php'), $this->moduleNameLower
        );
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $viewPath = resource_path('views/modules/' . $this->moduleNameLower);

        $sourcePath = module_path($this->moduleName, 'Resources/views');

        $this->publishes([
            $sourcePath => $viewPath
        ], ['views', $this->moduleNameLower . '-module-views']);

        $this->loadViewsFrom(array_merge($this->getPublishableViewPaths(), [$sourcePath]), $this->moduleNameLower);
    }

    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $langPath = resource_path('lang/modules/' . $this->moduleNameLower);

        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, $this->moduleNameLower);
        } else {
            $this->loadTranslationsFrom(module_path($this->moduleName, 'Resources/lang'), $this->moduleNameLower);
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }

    private function getPublishableViewPaths(): array
    {
        $paths = [];
        foreach (\Config::get('view.paths') as $path) {
            if (is_dir($path . '/modules/' . $this->moduleNameLower)) {
                $paths[] = $path . '/modules/' . $this->moduleNameLower;
            }
        }
        return $paths;
    }
}
