<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('domains_subscriptions');
        Schema::dropIfExists('tenants_subscriptions');
        Schema::table('tenants', function(Blueprint $table){
            $table->unsignedDouble('monthly_price')->default(0)->after('status');
            $table->unsignedDouble('annually_discount')->default(0)->after('status');
            $table->unsignedInteger('billing_cycle')->default(0)->after('status');
            $table->unsignedBigInteger('currency_id')->default(0)->after('status');
        });

        Schema::table('domains', function(Blueprint $table){
            $table->unsignedDouble('annually_discount')->default(0)->after('status');
            $table->unsignedInteger('billing_cycle')->default(0)->after('status');
            $table->unsignedBigInteger('currency_id')->default(0)->after('status');
            $table->string('provider_reference')->default(0)->after('provider_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //TODO: implementing roll back
    }
};
