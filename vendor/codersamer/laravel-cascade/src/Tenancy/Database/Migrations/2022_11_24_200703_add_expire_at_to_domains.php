<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('domains', function (Blueprint $table) {
            $table->timestamp('expire_at')->nullable()->after('status');
            $table->unsignedBigInteger('owner_id')->nullable()->after('status');
            $table->string('owner_type')->nullable()->after('status');
            $table->unsignedBigInteger('provider_id')->default(0)->after('status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('domains', function (Blueprint $table) {
            $table->drop('expire_at');
            $table->drop('owner_id');
            $table->drop('owner_type');
            $table->drop('provider_id');
        });
    }
};
