<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('domains_subscriptions', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('tenant_id')->default(0);
            $table->string('domain')->nullable();
            $table->string('name')->nullable();
            $table->string('tld')->nullable();
            $table->unsignedBigInteger('provider_id')->default(0);
            $table->string('provider_reference')->nullable();
            $table->unsignedInteger('type')->default(0);
            $table->unsignedInteger('status')->default(0);
            $table->unsignedDouble('annually_price')->default(0);
            $table->unsignedInteger('billing_cycle')->default(0);
            $table->timestamp('paid_at')->nullable();
            $table->unsignedBigInteger('currency_id')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('domains_subscriptions');
    }
};
