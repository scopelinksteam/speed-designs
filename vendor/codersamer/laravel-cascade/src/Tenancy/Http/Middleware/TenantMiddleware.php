<?php

namespace Cascade\Tenancy\Http\Middleware;

use Cascade\System\Services\System;
use Cascade\Tenancy\Enums\TenantMode;
use Cascade\Tenancy\Enums\TenantStatus;
use Cascade\Tenancy\Services\TenancyService;
use Closure;
use Illuminate\Http\Request;

class TenantMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        TenancyService::Initialize();
        if(TenancyService::ActiveMode() == TenantMode::None && System::IsMultiTenant()) {
            abort(404); 
        }

        if(TenancyService::ActiveMode() == TenantMode::None)
        {
            return $next($request);
        }

        $mode = TenancyService::ActiveMode();
        if($mode == TenantMode::Central)
        {
            TenancyService::SetUIMode(TenantMode::Central);
        }
        else 
        { 
            $currentStatus = TenancyService::CurrentTenant()->status;
            $abort = 0;
            $message = '';
            if($currentStatus == TenantStatus::Expired)
            {
                $abort = 402;
                $message = __('Requested System is Expired, Please Contact your Provider to Renew');
            }
            if($currentStatus == TenantStatus::Disabled)
            {
                $abort = 406;
                $message = __('Requested System has been disabled by System Administrator');
            }
            if($currentStatus == TenantStatus::Pending)
            {
                $abort = 503;
                $message = __('Pending Verifying and Setup');
            }
            if($currentStatus == TenantStatus::Propagating)
            {
                $abort = 425;
                $message = __('Network Propagation under Processing, Will be Available Soon');
            }
            if($currentStatus == TenantStatus::Suspended)
            {
                $abort = 403;
                $message = __('Account Suspended');
            }

            if($abort > 0)
            {
                abort($abort, $message);
            }
            
            TenancyService::SetUIMode(TenantMode::Tenant); 
        }

        return $next($request);
    }
}
