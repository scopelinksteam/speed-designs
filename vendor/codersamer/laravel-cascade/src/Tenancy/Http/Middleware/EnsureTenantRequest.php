<?php

namespace Cascade\Tenancy\Http\Middleware;

use Cascade\System\Services\System;
use Cascade\Tenancy\Enums\TenantMode;
use Cascade\Tenancy\Services\TenancyService;
use Closure;
use Illuminate\Http\Request;

class EnsureTenantRequest
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        TenancyService::Initialize();

        if(TenancyService::ActiveMode() != TenantMode::Tenant && System::IsMultiTenant()) { abort(404); }

        return $next($request);
    }
}
