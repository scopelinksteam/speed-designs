<?php

namespace Cascade\Tenancy\Http\Controllers\Central;

use Cascade\System\Services\Feedback;
use Cascade\System\Services\System;
use Cascade\Tenancy\Entities\TenantPackage;
use Cascade\Tenancy\Pages\PackageUpsertPage;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Cascade\Tenancy\Tables\PackagesTable;

class PackagesController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index(PackagesTable $table)
    {
        return System::FindView('tenancy::central.dashboard.packages.index', [
            'table' => $table
        ]);
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return System::FindView('tenancy::central.dashboard.packages.upsert', [
            'tabs' => PackageUpsertPage::GetTabs()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        $entityId = intval($request->input('entity_id', 0));
        $entity = $entityId > 0 ? TenantPackage::find($entityId) : new TenantPackage();

        $names = only_filled($request->input('name', []));
        if(count($names) == 0)
        {
            Feedback::error(__('Name Cannot be Empty'));
            Feedback::flash();
            return back();
        }

        $entity->name = fill_locales($names);
        $entity->description = fill_locales(only_filled($request->input('description', [])));
        $entity->lines = fill_locales(only_filled($request->input('lines', [])));
        $entity->label = fill_locales(only_filled($request->input('label', [])));

        //Image
        if($request->hasFile('image'))
        { $entity->image_path = $request->file('image')->store('packages', ['disk' => 'uploads']); }
        
        $entity->monthly_price = floatval($request->input('monthly_price', 0));
        $entity->annually_discount = floatval($request->input('annually_discount', 0));
        $entity->is_popular = boolval(intval($request->input('is_popular', '0')));
        $entity->is_enabled = boolval(intval($request->input('is_enabled', '1')));

        
        $entity->save();
        foreach($request->input('attributes', []) as $key => $value)
        {
            $entity->storeAttribute($key, $value);
        }

        foreach($request->input('options', []) as $key => $value)
        {
            $entity->storeAttribute('options_'.$key, $value);
        }
        Feedback::success(__('Package Saved Successfully'));
        Feedback::flash();
        return to_route('central.tenants.packages.index');

    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit(TenantPackage $package)
    {
        return $this->create()->with([
            'package' => $package
        ]);
    }


    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy(TenantPackage $package)
    {

    }
}
