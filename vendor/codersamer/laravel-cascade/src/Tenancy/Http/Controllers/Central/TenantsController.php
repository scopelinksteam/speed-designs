<?php

namespace Cascade\Tenancy\Http\Controllers\Central;

use Carbon\Carbon;
use Cascade\System\Services\Feedback;
use Cascade\Tenancy\Entities\Tenant;
use Cascade\Tenancy\Entities\TenantDomain;
use Cascade\Tenancy\Enums\DomainStatus;
use Cascade\Tenancy\Enums\TenantStatus;
use Cascade\Tenancy\Tables\TenantDomainsTable;
use Cascade\Tenancy\Tables\TenantEmailsTable;
use Cascade\Tenancy\Tables\TenantsTable;
use Cascade\Users\Datatables\Central\UsersDatatable;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Crypt;
use Cascade\Tenancy\Entities\TenantEmail;

class TenantsController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index(TenantsTable $table)
    {
        return view('tenancy::central.dashboard.tenants.index', [
            'table' => $table
        ]);
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('tenancy::central.dashboard.tenants.upsert');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        $entityId = intval($request->input('entity_id', 0));
        $entity = $entityId > 0 ? Tenant::find($entityId) : new Tenant();

        $errors = [];

        if(!$request->filled('name')) { $errors[] = __('Name is Required'); }
        if(!$request->filled('slug')) { $errors[] = __('Slug is Required'); }

        if(count($errors) > 0)
        {
            $message = __('There are several errors occurred') . ' : <br />';
            foreach($errors as $error) { $message .= '- '.$error.'<br />'; }
            Feedback::error($message);
            Feedback::flash();
            return back();
        }

        $entity->name = $request->input('name', '');
        $entity->slug = $request->input('slug', '');

        $expireAt = $request->input('expire_at', null);
        if($expireAt != null) { $expireAt = Carbon::parse($expireAt); }
        $entity->expire_at = $expireAt;

        $database = [];
        $database['host'] = $request->input('host', '127.0.0.1');
        $database['port'] = $request->input('database_port', '3306');
        $database['username'] = $request->input('database_username', 'root');
        $database['password'] = $request->input('database_password', '');
        $database['driver'] = $request->input('database_driver', 'mysql');
        $database['database'] = $request->input('database_name', 'forge');

        $database = json_encode($database);
        $database = Crypt::encryptString($database);

        $entity->database = $database;

        $entity->status = TenantStatus::from(intval($request->input('status', 0)));


         //Data
         $data = $entity->data ?? [];
         //Modules
         $modules = [];
         $selectedModules = $request->input('modules', []);

         foreach($selectedModules as $moduleName => $value)
         {
             if(intval($value) == 1)
             { $modules[$moduleName] = intval($request->input('modules_status.'.$moduleName, '0')) == 1;; }
         }

         $data['modules'] = $modules;

         //Plugins
         $plugins = [];
         $selectedPlugins = $request->input('plugins', []);

         foreach($selectedPlugins as $pluginName => $value)
         {
             if(intval($value) == 1)
             { $plugins[$pluginName] = intval($request->input('plugins_status.'.$pluginName, '0')) == 1;; }
         }
         $data['plugins'] = $plugins;

         //Themes
         $themes = [];
         $selectedThemes = $request->input('themes', []);

         foreach($selectedThemes as $themeName => $value)
         {
             if(intval($value) == 1)
             { $themes[$themeName] = $themeName; }
         }
         $data['themes'] = $themes;

         $entity->data = $data;


        $entity->save();

        $entity->SetFeature('developer_mode', $request->input('features.developer_mode', false));
        $entity->SetFeature('modules_manager', $request->input('features.modules_manager', false));
        $entity->SetFeature('plugins_manager', $request->input('features.plugins_manager', false));
        $entity->SetFeature('themes_manager', $request->input('features.themes_manager', false));
        $entity->SetFeature('translation_manager', $request->input('features.translation_manager', false));
        $entity->SetFeature('site_manager', $request->input('features.site_manager', false));
        $entity->SetFeature('sliders_manager', $request->input('features.sliders_manager', false));

        //Options
        foreach($request->input('options', []) as $key => $value)
        {
            $entity->SetOption($key, $value);
        }
        //Storage Limit

        //Mail Sending Limit
        //Email Accounts
        //

        Feedback::success(__('Tenant Saved Successfully'));
        Feedback::flash();
        return to_route('central.tenants.index');
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function migrate(Tenant $tenant)
    {
        $tenant->manager->UpdateDatabase();
        Feedback::success(__('Database Updated Successfully'));
        Feedback::flash();
        return to_route('central.tenants.index');
    }



    /**
     * Manage Tenant Domains
     *
     * @param Tenant $tenant
     * @return Renderable
     */
    public function domains(Tenant $tenant)
    {
        return view('tenancy::central.dashboard.tenants.domains', [
            'tenant' => $tenant,
            'table' => TenantDomainsTable::class
        ]);
    }

    /**
     * Manage Tenant Domains
     *
     * @param Tenant $tenant
     * @return Renderable
     */
    public function editDomains(Tenant $tenant, TenantDomain $domain)
    {
        return view('tenancy::central.dashboard.tenants.domains', [
            'tenant' => $tenant,
            'domain' => $domain,
            'table' => TenantDomainsTable::class
        ]);
    }

    /**
     * Manage Tenant Domains
     *
     * @param Tenant $tenant
     * @return Renderable
     */
    public function storeDomains(Request $request)
    {
        $tenantId = intval($request->input('tenant_id', 0));
        $entityId = intval($request->input('entity_id', 0));
        $entity = $entityId > 0 ? TenantDomain::find($entityId) : new TenantDomain();

        $errors = [];

        if(!$request->filled('domain'))
        {
            $errors[] = __('Domain Name is Required');
        }
        $domain = $request->input('domain', '');
        if($request->filled('domain'))
        {
            $existDomain = TenantDomain::where('domain', strtolower($domain))->first();
            if($existDomain != null && $existDomain->id != $entityId)
            {
                $errors[] = __('Domain Already Associated with a Tenant');
            }
        }

        if(count($errors) > 0)
        {
            $message = __('There are several errors occurred') . ' : <br />';
            foreach($errors as $error) { $message .= '- '.$error.'<br />'; }
            Feedback::error($message);
            Feedback::flash();
            return back();
        }

        $entity->domain = $domain;
        $entity->tenant_id = $tenantId;
        $entity->status = DomainStatus::from(intval($request->input('status', 0)));


        $entity->save();

        Feedback::success(__('Domain Saved Successfully'));
        Feedback::flash();

        return to_route('central.tenants.domains', ['tenant' => $tenantId]);
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroyDomains(Tenant $tenant, TenantDomain $domain)
    {
        $domain->delete();
        Feedback::success(__('Domain Deleted Successfully'));
        Feedback::flash();
        return to_route('central.tenants.domains', ['tenant' => $tenant->id]);
    }


     /**
     * Manage Tenant Domains
     *
     * @param Tenant $tenant
     * @return Renderable
     */
    public function emails(Tenant $tenant)
    {
        return view('tenancy::central.dashboard.tenants.emails', [
            'tenant' => $tenant,
            'table' => TenantEmailsTable::class
        ]);
    }

    /**
     * Manage Tenant Domains
     *
     * @param Tenant $tenant
     * @return Renderable
     */
    public function editEmails(Tenant $tenant, TenantEmail $email)
    {
        return view('tenancy::central.dashboard.tenants.emails', [
            'tenant' => $tenant,
            'email' => $email,
            'table' => TenantEmailsTable::class
        ]);
    }

    /**
     * Manage Tenant Domains
     *
     * @param Tenant $tenant
     * @return Renderable
     */
    public function storeEmails(Request $request)
    {
        $tenantId = intval($request->input('tenant_id', 0));
        $entityId = intval($request->input('entity_id', 0));
        $entity = $entityId > 0 ? TenantEmail::find($entityId) : new TenantEmail();

        $errors = [];

        if(!$request->filled('email'))
        {
            $errors[] = __('Email Address is Required');
        }
        $email = $request->input('email', '');
        if($request->filled('email'))
        {
            $existEmail = TenantEmail::where('email', strtolower($email))->first();
            if($existEmail != null && $existEmail->id != $entityId)
            {
                $errors[] = __('Email Already Associated with a Tenant');
            }
        }

        if(count($errors) > 0)
        {
            $message = __('There are several errors occurred') . ' : <br />';
            foreach($errors as $error) { $message .= '- '.$error.'<br />'; }
            Feedback::error($message);
            Feedback::flash();
            return back();
        }

        $entity->email = $email;
        $entity->tenant_id = $tenantId;
        $entity->size = intval($request->input('size', 0));


        $entity->save();

        Feedback::success(__('Email Saved Successfully'));
        Feedback::flash();

        return to_route('central.tenants.emails', ['tenant' => $tenantId]);
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroyEmails(Tenant $tenant, TenantEmail $email)
    {
        $email->delete();
        Feedback::success(__('Email Deleted Successfully'));
        Feedback::flash();
        return to_route('central.tenants.emails', ['tenant' => $tenant->id]);
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit(Tenant $tenant)
    {
        return $this->create()->with([
            'tenant' => $tenant
        ]);
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy(Tenant $tenant)
    {
        $tenant->domains()->delete();
        $tenant->delete();
        Feedback::success(__('Tenant Deleted Successfully'));
        Feedback::flash();
        return to_route('central.tenants.index');
    }
}
