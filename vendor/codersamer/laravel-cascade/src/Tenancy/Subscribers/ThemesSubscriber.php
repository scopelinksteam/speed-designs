<?php

namespace Cascade\Tenancy\Subscribers;

use Cascade\Site\Filters\ActiveThemeNameFilter;
use Cascade\Site\Filters\AvailableThemesFilter;
use Cascade\System\Services\Filter;
use Cascade\System\Services\System;
use Cascade\Tenancy\Handlers\Filters\ActiveThemeNameFilterHandler;
use Cascade\Tenancy\Services\TenancyService;

class ThemesSubscriber
{
    public function filters()
    {
        Filter::On(ActiveThemeNameFilter::class, [$this, 'themeName']);
        Filter::On(AvailableThemesFilter::class, [$this, 'availableThemes']);
    }

    public function actions()
    {

    }

    public function themeName($themeName)
    {
        if(System::IsMultiTenant())
        {
            TenancyService::Initialize();
            $currentTenant = TenancyService::CurrentTenant();
            if($currentTenant == null) { return null; }
            return $currentTenant->CurrentThemeName();
        }
        return $themeName;
    }

    public function availableThemes($themes)
    {
        if(System::IsMultiTenant() && !TenancyService::IsCentral())
        {
            $tenantThemes = TenancyService::CurrentTenant()->themes;
            return $themes->filter(function($theme) use ($tenantThemes) {
                return isset($tenantThemes[$theme->GetName()]);
            });
        }
        return $themes;
    }
}