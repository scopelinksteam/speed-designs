<?php

namespace Cascade\Tenancy\Subscribers;

use Cascade\System\Actions\ModuleDisableAction;
use Cascade\System\Actions\ModuleEnableAction;
use Cascade\System\Filters\AvailableModulesFilter;
use Cascade\System\Filters\ModuleStatusFilter;
use Cascade\System\Services\Action;
use Cascade\System\Services\Filter;
use Cascade\Tenancy\Enums\TenantMode;
use Cascade\Tenancy\Services\TenancyService;
use Nwidart\Modules\Module;

class ModulesSubscriber
{
    public function filters()
    {
        Filter::On(AvailableModulesFilter::class, [$this, 'availableModules']);
        Filter::On(ModuleStatusFilter::class, [$this, 'moduleStatus']);
    }

    public function actions()
    {
        Action::On(ModuleDisableAction::class, [$this, 'disableModule']);
        Action::On(ModuleEnableAction::class, [$this, 'enableModule']);
    }

    public function availableModules($modules)
    {
        if(TenancyService::Enabled() && !app()->runningInConsole())
        {
            TenancyService::Initialize();
            if(TenancyService::ActiveMode() != TenantMode::None)
            {
                $tenant = TenancyService::CurrentTenant();
                if($tenant != null)
                {
                    $currentModules = $tenant->modules;
                    $list = [];
                    foreach($modules as $module)
                    {
                        if(in_array($module->getName(), array_keys($currentModules)))
                        { $list[] = $module; }
                    }
                    return $currentModules;
                }
            }
        }
    }

    public function moduleStatus($currentStatus, Module $module)
    {
        if(TenancyService::Enabled() && !app()->runningInConsole())
        {
            TenancyService::Initialize();
            $currentTenant = TenancyService::CurrentTenant();
            if($currentTenant != null)
            {
                return $currentTenant->ModuleStatus($module->getName());
            }
        }
        return $currentStatus;
    }

    public function enableModule(Module $module)
    {
        if(TenancyService::Enabled() && !app()->runningInConsole())
        {
            TenancyService::Initialize();
            $currentTenant = TenancyService::CurrentTenant();
            if($currentTenant != null)
            {
                $currentTenant->SetModuleStatus($module->getName(), true);
                return false;
            }
        }
    }

    public function disableModule(Module $module)
    {
        if(TenancyService::Enabled() && !app()->runningInConsole())
        {
            TenancyService::Initialize();
            $currentTenant = TenancyService::CurrentTenant();
            if($currentTenant != null)
            {
                $currentTenant->SetModuleStatus($module->getName(), false);
                return false;
            }
        }
    }

    
}