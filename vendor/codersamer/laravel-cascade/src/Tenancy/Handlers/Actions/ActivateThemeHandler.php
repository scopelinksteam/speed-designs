<?php

namespace Cascade\Tenancy\Handlers\Actions;

use Cascade\Site\Actions\ActivateThemeReadyAction;
use Cascade\System\Services\Action;
use Cascade\Tenancy\Services\TenancyService;

class ActivateThemeHandler
{
    public function actions()
    {
        if(TenancyService::Enabled() && TenancyService::CurrentTenant() != null)
        { 
            Action::On(ActivateThemeReadyAction::class, [$this, 'SetActiveTheme']);
        }
    }

    public function SetActiveTheme($theme)
    {
        TenancyService::CurrentTenant()->SetTheme($theme->GetName());
    }
}