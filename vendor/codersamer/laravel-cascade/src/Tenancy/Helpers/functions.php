<?php

use Cascade\Tenancy\Entities\Tenant;
use Cascade\Tenancy\Enums\TenantMode;
use Cascade\Tenancy\Services\TenancyService;

if(!function_exists('is_central'))
{ function is_central() { return TenancyService::ActiveMode() == TenantMode::Central; } }

if(!function_exists('is_tenant'))
{ function is_tenant() { return TenancyService::ActiveMode() == TenantMode::Tenant; } }

if(!function_exists('get_tenants'))
{ function get_tenants() { return Tenant::all(); } }

if(!function_exists('current_tenant'))
{ function current_tenant() { return TenancyService::CurrentTenant(); } }
