<?php

namespace Cascade\Tenancy\Tables;

use Cascade\Dashboard\Services\Datatables\Action;
use Cascade\Dashboard\Services\Datatables\Column;
use Cascade\Dashboard\Services\Datatables\DataTable;
use Cascade\Tenancy\Entities\Tenant;
use Cascade\Tenancy\Entities\TenantPackage;

class PackagesTable extends DataTable
{

    public function __construct($params = [])
    {

    }

    public function query()
    {
        return TenantPackage::query();
    }

    public function columns()
    {
        $columns = [];
        $columns[] = Column::make('name')->text(__('Name'));
        $columns[] = Column::make('monthly_price')->text(__('Price'));
        return $columns;
    }

    public function actions()
    {
        $actions = [];
        $actions[] = Action::make('edit', __('Edit'))->route('central.tenants.packages.edit', ['package' => 'id']);
        $actions[] = Action::make('delete', __('Delete'))->route('central.tenants.packages.delete', ['package' => 'id']);
        return $actions;
    }
}
