<?php

namespace Cascade\Tenancy\Tables;

use Cascade\Dashboard\Services\Datatables\Action;
use Cascade\Dashboard\Services\Datatables\Column;
use Cascade\Dashboard\Services\Datatables\DataTable;
use Cascade\Tenancy\Entities\Tenant;

class TenantEmailsTable extends DataTable
{

    protected $tenantId = 0;

    protected Tenant $tenant;

    public function __construct($tenantId)
    {
        $this->tenantId = intval($tenantId);
        $this->tenant = Tenant::find($this->tenantId);
    }

    public function query()
    {
        return $this->tenant->emails();
    }

    public function columns()
    {
        $columns = [];
        $columns[] = Column::make('email')->text(__('Address'));
        $columns[] = Column::make('size')->text(__('Size'));
        return $columns;
    }

    public function actions()
    {
        $actions = [];
        $actions[] = Action::make('edit', __('Edit'))->route('central.tenants.emails.edit', ['tenant' => 'tenant_id', 'email' => 'id']);
        $actions[] = Action::make('delete', __('Delete'))->route('central.tenants.emails.delete', ['tenant' => 'tenant_id', 'email' => 'id']);
        return $actions;
    }
}
