<?php

namespace Cascade\Tenancy\Tables;

use Cascade\Dashboard\Services\Datatables\Action;
use Cascade\Dashboard\Services\Datatables\Column;
use Cascade\Dashboard\Services\Datatables\DataTable;
use Cascade\Tenancy\Entities\Tenant;

class TenantsTable extends DataTable
{

    public function __construct($params = [])
    {
        $this->editColumn('domains', function(Tenant $tenant){
            $domains = $tenant->domains;
            $text = [];
            foreach($domains as $domain)
            {
                $text[] = $this->subtitle($domain->domain);
            }
            return implode('<br />', $text);
        });
        $this->editColumn('storage_size', function(Tenant $tenant){
            return $tenant->GetStorageSize() . ' MB';
        });
    }

    public function query()
    {
        return Tenant::query();
    }

    public function columns()
    {
        $columns = [];
        $columns[] = Column::make('name')->text(__('Name'));
        $columns[] = Column::make('domains')->text(__('Domains'));
        $columns[] = Column::make('status')->text(__('Status'));
        $columns[] = Column::make('storage_size')->text(__('Storage'));
        $columns[] = Column::make('migrated_at')->text(__('Last Database Update'));
        return $columns;
    }

    public function actions()
    {
        $actions = [];
        $actions[] = Action::make('edit', __('Edit'))->route('central.tenants.edit', ['tenant' => 'id']);
        $actions[] = Action::make('accounts', __('Accounts'))->route('central.users.tenant.index', ['tenant' => 'id']);

        $actions[] = Action::make('domains', __('Domains'))->route('central.tenants.domains', ['tenant' => 'id']);
        $actions[] = Action::make('emails', __('Emails'))->route('central.tenants.emails', ['tenant' => 'id']);
        $actions[] = Action::make('migrate', __('Update Database'))->route('central.tenants.migrate', ['tenant' => 'id']);
        $actions[] = Action::make('delete', __('Delete'))->route('central.tenants.delete', ['tenant' => 'id']);
        return $actions;
    }
}
