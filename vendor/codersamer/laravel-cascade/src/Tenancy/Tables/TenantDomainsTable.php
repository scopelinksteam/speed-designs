<?php

namespace Cascade\Tenancy\Tables;

use Cascade\Dashboard\Services\Datatables\Action;
use Cascade\Dashboard\Services\Datatables\Column;
use Cascade\Dashboard\Services\Datatables\DataTable;
use Cascade\Tenancy\Entities\Tenant;

class TenantDomainsTable extends DataTable
{

    protected $tenantId = 0;

    protected Tenant $tenant;

    public function __construct($tenantId)
    {
        $this->tenantId = intval($tenantId);
        $this->tenant = Tenant::find($this->tenantId);
    }

    public function query()
    {
        return $this->tenant->domains();
    }

    public function columns()
    {
        $columns = [];
        $columns[] = Column::make('domain')->text(__('Domain'));
        $columns[] = Column::make('status')->text(__('Status'));
        return $columns;
    }

    public function actions()
    {
        $actions = [];
        $actions[] = Action::make('edit', __('Edit'))->route('central.tenants.domains.edit', ['domain' => 'id', 'tenant' => 'tenant_id']);
        $actions[] = Action::make('delete', __('Delete'))->route('central.tenants.domains.delete', ['tenant' => 'tenant_id', 'domain' => 'id']);
        return $actions;
    }
}
