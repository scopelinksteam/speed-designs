@extends('admin::layouts.master')
@section('page-title', __('Package Details'))
@section('page-description', __('Manage Websites, Tenants Packages Available and hosted by this System'))
@section('page-actions')
<a href="{{ route('central.tenants.packages.index') }}" class="btn btn-dark"><i class="fa fa-plus me-2"></i>@lang('Back to List')</a>
@endsection
@section('content')
@stack('content::start')
<form action="{{route('central.tenants.packages.store')}}" method="POST" enctype="multipart/form-data">
    @stack('form::start')
    @csrf
    @isset($package)
    <input type="hidden" name="entity_id" value="{{ $package->id }}">
    @endisset
    <x-dashboard-tabs :tabs="$tabs" :data="$__data" />
    <div class="p-3 rounded-3 bg-white">
        <input  type="submit" class="btn btn-primary " value="@lang('Save')" />
        <a href="#" class="ms-2 btn btn-dark">@lang('Back to List')</a>
    </div>
@stack('form::end')
</form>
@stack('content::end')
@endsection

