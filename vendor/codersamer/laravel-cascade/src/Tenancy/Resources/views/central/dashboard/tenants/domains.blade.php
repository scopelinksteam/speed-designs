@extends('admin::layouts.master')
@section('page-title', __('Tenant Domains').' : '.$tenant->name)
@section('page-description', __('Manage Domains Owned by Selected Tenant'))
@section('page-actions')
<a href="{{ route('central.tenants.create') }}" class="btn btn-dark"><i class="fa fa-plus me-2"></i>@lang('New Tenant')</a>
@endsection
@section('content')
<div class="row">
    <div class="col-md-4">
        <form action="{{ route('central.tenants.domains.store') }}" method="POST">
            <div class="card">
                <div class="card-header">
                    <div class="card-title">
                        <i class="fa fa-info me-2"></i>
                        @lang('Domain Details')
                    </div>
                </div>
                <div class="card-body">
                    <div class="form-group">
                        <label for="domain">@lang('Domain')</label>
                        <input type="text" name="domain" id="domain" class="form-control" value="{{ isset($domain) ? $domain->domain : '' }}">
                    </div>
                    <div class="form-group">
                        <label for="status">@lang('Status')</label>
                        <select name="status" id="status" class="selectr">
                            <option value="0" @selected(isset($domain) && $domain->status->value == 0)>@lang('Draft')</option>
                            <option value="1" @selected(isset($domain) && $domain->status->value == 1)>@lang('Pending')</option>
                            <option value="2" @selected(isset($domain) && $domain->status->value == 2)>@lang('Propagating')</option>
                            <option value="3" @selected(isset($domain) && $domain->status->value == 3)>@lang('Active')</option>
                            <option value="4" @selected(isset($domain) && $domain->status->value == 4)>@lang('Disabled')</option>
                            <option value="5" @selected(isset($domain) && $domain->status->value == 5)>@lang('Suspended')</option>
                            <option value="6" @selected(isset($domain) && $domain->status->value == 6)>@lang('Expired')</option>
                            <option value="7" @selected(isset($domain) && $domain->status->value == 7)>@lang('Abandoned')</option>
                        </select>
                    </div>
                </div>
                <div class="card-footer">
                    @csrf
                    <input type="hidden" name="tenant_id" value="{{ $tenant->id }}">
                    @isset($domain)
                    <input type="hidden" name="entity_id" value="{{ $domain->id }}">
                    @endisset
                    <button class="btn btn-primary" type="submit">@lang('Save Domain')</button>
                </div>
            </div>
        </form>
    </div>
    <div class="col-md-8">
        <div class="card">
            <div class="card-header">
                <div class="card-title">
                    <i class="fa fa-cubes me-2"></i>
                    @lang('Domains List')
                </div>
            </div>
            <div class="card-body">
                @livewire('datatable', ['table' => $table, 'params' => $tenant->id])
            </div>
        </div>
    </div>
</div>
@endsection
