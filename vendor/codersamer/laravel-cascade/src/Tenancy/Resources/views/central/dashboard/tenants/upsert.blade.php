@extends('admin::layouts.master')
@section('page-title', __('Tenants Management'))
@section('page-description', __('Manage Websites, Tenants and Packages Available and hosted by this System'))
@section('page-actions')
<a href="{{ route('central.tenants.index') }}" class="btn btn-dark"><i class="fa fa-plus me-2"></i>@lang('Back to List')</a>
@endsection
@section('content')
<div class="row">
    <div class="col-12">
        <form action="{{ route('central.tenants.store') }}" method="POST">

            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-auto align-self-center">
                            <div class="card-title">
                                <i class="fa fa-cubes me-2"></i>
                                @lang('Tenants Details')
                            </div>
                        </div>
                        <div class="col-auto">
                            <ul class="nav nav-tabs">
                                <li class="nav-item">
                                    <button class="nav-link active" type="button" data-bs-toggle="tab" data-bs-target="#basic-tab">@lang('Basic Details')</button>
                                </li>
                                <li class="nav-item">
                                    <button class="nav-link" type="button" data-bs-toggle="tab" data-bs-target="#features-tab">@lang('Features')</button>
                                </li>
                                <li class="nav-item">
                                    <button class="nav-link" type="button" data-bs-toggle="tab" data-bs-target="#modules-tab">@lang('Modules')</button>
                                </li>
                                <li class="nav-item">
                                    <button class="nav-link" type="button" data-bs-toggle="tab" data-bs-target="#plugins-tab">@lang('Plugins')</button>
                                </li>
                                <li class="nav-item">
                                    <button class="nav-link" type="button" data-bs-toggle="tab" data-bs-target="#themes-tab">@lang('Themes')</button>
                                </li>
                                <li class="nav-item">
                                    <button class="nav-link" type="button" data-bs-toggle="tab" data-bs-target="#options-tab">@lang('Options')</button>
                                </li>
                            </ul>
                        </div>
                    </div>


                </div>
                <div class="card-body">
                    <div class="tab-content">
                        <div class="tab-pane active" role="tabpanel" id="basic-tab">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="form-label" for="name">@lang('Name')</label>
                                        <input type="text" name="name" id="name" class="form-control" value="{{ isset($tenant) ? $tenant->name : '' }}">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="form-label" for="slug">@lang('Slug')</label>
                                        <input type="text" name="slug" id="slug" class="form-control" value="{{ isset($tenant) ? $tenant->slug : '' }}">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="form-label" for="status">@lang('Status')</label>
                                        <select name="status" id="status" class="selectr">
                                            <option value="0" @selected(isset($tenant) && $tenant->status->value == 0)>@lang('Draft')</option>
                                            <option value="1" @selected(isset($tenant) && $tenant->status->value == 1)>@lang('Pending')</option>
                                            <option value="2" @selected(isset($tenant) && $tenant->status->value == 2)>@lang('Propagating')</option>
                                            <option value="3" @selected(isset($tenant) && $tenant->status->value == 3)>@lang('Active')</option>
                                            <option value="4" @selected(isset($tenant) && $tenant->status->value == 4)>@lang('Disabled')</option>
                                            <option value="5" @selected(isset($tenant) && $tenant->status->value == 5)>@lang('Suspended')</option>
                                            <option value="6" @selected(isset($tenant) && $tenant->status->value == 6)>@lang('Expired')</option>
                                            <option value="7" @selected(isset($tenant) && $tenant->status->value == 7)>@lang('Abandoned')</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="form-label" for="expire_at">@lang('Expire At')</label>
                                        <input type="date" name="expire_at" id="expire_at" class="form-control" value="{{ isset($tenant) ? timestamp_to_date_input($tenant->expire_at) : '' }}">
                                    </div>
                                </div>
                                <div class="col-md-12 mt-2">
                                    <h5 class="fw-bold">@lang('Database Configuration')</h5>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="form-label" for="host">@lang('Host / Server')</label>
                                        <input type="text" name="host" id="host" class="form-control" value="{{ isset($tenant) ? $tenant->database_host : '' }}">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="form-label" for="database_name">@lang('Database')</label>
                                        <input type="text" name="database_name" id="database_name" class="form-control" value="{{ isset($tenant) ? $tenant->database_name : '' }}">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="form-label" for="database_username">@lang('Username')</label>
                                        <input type="text" name="database_username" id="database_username" class="form-control" value="{{ isset($tenant) ? $tenant->database_username : '' }}">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="form-label" for="database_password">@lang('Password')</label>
                                        <input type="password" name="database_password" id="database_password" class="form-control" value="{{ isset($tenant) ? $tenant->database_password : '' }}">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="features-tab" role="tabpanel">
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="form-group mb-2">
                                        <label class="form-label" for="feature-developer_mode">@lang('Developer Mode')</label>
                                        <br />
                                        <select name="features[developer_mode]" id="feature-developer_mode" class="selectr">
                                            <option value="0" @selected(isset($tenant) && !$tenant->HasFeature('developer_mode'))>@lang('Disabled')</option>
                                            <option value="1" @selected(isset($tenant) && $tenant->HasFeature('developer_mode'))>@lang('Enabled')</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group mb-2">
                                        <label class="form-label" for="feature-modules_manager">@lang('Modules Manager')</label>
                                        <br />
                                        <select name="features[modules_manager]" id="feature-modules_manager" class="selectr">
                                            <option value="0" @selected(isset($tenant) && !$tenant->HasFeature('modules_manager'))>@lang('Disabled')</option>
                                            <option value="1" @selected(isset($tenant) && $tenant->HasFeature('modules_manager'))>@lang('Enabled')</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group mb-2">
                                        <label class="form-label" for="feature-plugins_manager">@lang('Plugins Manager')</label>
                                        <br />
                                        <select name="features[plugins_manager]" id="feature-plugins_manager" class="selectr">
                                            <option value="0" @selected(isset($tenant) && !$tenant->HasFeature('plugins_manager'))>@lang('Disabled')</option>
                                            <option value="1" @selected(isset($tenant) && $tenant->HasFeature('plugins_manager'))>@lang('Enabled')</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group mb-2">
                                        <label class="form-label" for="feature-themes_manager">@lang('Themes Manager')</label>
                                        <br />
                                        <select name="features[themes_manager]" id="feature-themes_manager" class="selectr">
                                            <option value="0" @selected(isset($tenant) && !$tenant->HasFeature('themes_manager'))>@lang('Disabled')</option>
                                            <option value="1" @selected(isset($tenant) && $tenant->HasFeature('themes_manager'))>@lang('Enabled')</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group mb-2">
                                        <label class="form-label" for="feature-translation_manager">@lang('Translations Manager')</label>
                                        <br />
                                        <select name="features[translation_manager]" id="feature-translation_manager" class="selectr">
                                            <option value="0" @selected(isset($tenant) && !$tenant->HasFeature('translation_manager'))>@lang('Disabled')</option>
                                            <option value="1" @selected(isset($tenant) && $tenant->HasFeature('translation_manager'))>@lang('Enabled')</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group mb-2">
                                        <label class="form-label" for="feature-site_manager">@lang('Site Manager')</label>
                                        <br />
                                        <select name="features[site_manager]" id="feature-site_manager" class="selectr">
                                            <option value="0" @selected(isset($tenant) && !$tenant->HasFeature('site_manager'))>@lang('Disabled')</option>
                                            <option value="1" @selected(isset($tenant) && $tenant->HasFeature('site_manager'))>@lang('Enabled')</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group mb-2">
                                        <label class="form-label" for="feature-sliders_manager">@lang('Sliders Manager')</label>
                                        <br />
                                        <select name="features[sliders_manager]" id="feature-sliders_manager" class="selectr">
                                            <option value="0" @selected(isset($tenant) && !$tenant->HasFeature('sliders_manager'))>@lang('Disabled')</option>
                                            <option value="1" @selected(isset($tenant) && $tenant->HasFeature('sliders_manager'))>@lang('Enabled')</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="modules-tab" role="tabpanel">
                            <div class="row">
                                <table class="table table-striped">
                                    @foreach(get_modules() as $module)
                                    <tr>
                                        <td>
                                            <input type="checkbox" name="modules[{{ $module->GetName() }}]" value="1" id="module-{{ $module->GetName() }}" @checked(isset($tenant) && isset($tenant->modules[$module->GetName()]))>
                                        </td>
                                        <td>
                                            <label class="form-label" for="module-{{ $module->GetName() }}">{{ $module->GetName() }}</label>
                                        </td>
                                        <td>
                                            {{ $module->GetDescription() }}
                                        </td>
                                        <td width="200px">
                                            <select name="modules_status[{{ $module->GetName() }}]" id="" class="selectr">
                                                <option value="0" @selected(isset($tenant) && !$tenant->ModuleStatus($module->GetName()))>@lang('Disabled')</option>
                                                <option value="1" @selected(isset($tenant) && $tenant->ModuleStatus($module->GetName()))>@lang('Enabled')</option>
                                            </select>
                                        </td>
                                    </tr>
                                    @endforeach
                                </table>
                            </div>
                        </div>
                        <div class="tab-pane" id="plugins-tab" role="tabpanel">
                            <div class="row">
                                @foreach(get_plugins() as $plugin)
                                <div class="col-md-2">
                                    <div class="form-group mb-2">
                                        <input type="checkbox" name="plugins[{{ $plugin->GetName() }}]" value="1" id="plugin-{{ $plugin->GetName() }}" @checked(isset($tenant) && isset($tenant->plugins[$plugin->GetName()]))>
                                        <label class="form-label" for="plugin-{{ $plugin->GetName() }}">{{ $plugin->GetName() }}</label>
                                        <br />
                                        <select name="plugins_status[{{ $plugin->GetName() }}]" id="" class="selectr">
                                            <option value="0" @selected(isset($tenant) && !$tenant->PluginStatus($plugin->GetName()))>@lang('Disabled')</option>
                                            <option value="1" @selected(isset($tenant) && $tenant->PluginStatus($plugin->GetName()))>@lang('Enabled')</option>
                                        </select>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div>
                        <div class="tab-pane" id="themes-tab" role="tabpanel">
                            <div class="row">
                                @foreach(get_themes() as $theme)
                                <div class="col-md-2">
                                    <div class="form-control mb-2">
                                        <input type="checkbox" name="themes[{{ $theme->GetName() }}]" value="1" id="theme-{{ $theme->GetName() }}" @checked(isset($tenant) && isset($tenant->themes[$theme->GetName()]))>
                                        <label for="theme-{{ $theme->GetName() }}">{{ $theme->GetName() }}</label>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div>
                        <div class="tab-pane" id="options-tab" role="tabpanel">
                            <div class="row">
                                <div class="col-md-3">
                                    <label for="option_storage_limit" class="form-label">@lang('Storage Limit') MB</label>
                                    <input type="number" class="form-control" name="options[storage_limit]" id="option_storage_limit" value="{{isset($tenant) ? $tenant->GetOption('storage_limit') : ''}}">
                                </div>
                                <div class="col-md-3">
                                    <label for="option_emails_limit" class="form-label">@lang('Email Accounts')</label>
                                    <input type="number" class="form-control" name="options[email_accounts_limit]" id="option_emails_limit" value="{{isset($tenant) ? $tenant->GetOption('email_accounts_limit') : ''}}">
                                </div>
                                <div class="col-md-3">
                                    <label for="option_support_link" class="form-label">@lang('Support Link')</label>
                                    <input type="text" class="form-control" name="options[support_link]" id="option_support_link" value="{{isset($tenant) ? $tenant->GetOption('support_link') : ''}}">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    @csrf
                    @isset($tenant)
                    <input type="hidden" name="entity_id" value="{{ $tenant->id }}">
                    @endisset
                    <button type="submit" class="btn btn-primary">@lang('Save Tenant')</button>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection
