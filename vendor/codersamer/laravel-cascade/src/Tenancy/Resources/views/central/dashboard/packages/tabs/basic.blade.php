<div class="row">
    <div class="col-md-8">
        <div class="card">
            <div class="card-header">
                <div class="card-title">
                    <i class="fa fa-cubes me-2"></i>
                    @lang('Basic Details')
                </div>
            </div>
            <div class="card-body">
                <ul class="nav nav-tabs">
                @foreach(get_active_languages() as $language)
                    <li class="nav-item">
                        <a href="#" data-bs-toggle="tab" data-bs-target="#content-tab-{{$language->locale}}" class="nav-link {{$loop->iteration == 1 ? 'active' : ''}}">{{$language->name}}</a>
                    </li>
                @endforeach
                </ul>
                <div class="tab-content mt-3">
                    @foreach(get_active_languages() as $language)
                    <div class="tab-pane fade {{$loop->iteration == 1 ? 'active show' : ''}}" id="content-tab-{{$language->locale}}">
                        <div class="form-group">
                            <label for="name_{{$language->locale}}" class="form-label">@lang('Name')</label>
                            <input type="text" name="name[{{$language->locale}}]" id="name_{{$language->locale}}" class="form-control" @isset($package) value="{{$package->translate('name', $language->locale)}}" @endisset>
                        </div>
                        <div class="form-group">
                            <label for="label_{{$language->locale}}" class="form-label">@lang('Label')</label>
                            <input type="text" name="label[{{$language->locale}}]" id="label_{{$language->locale}}" class="form-control" @isset($package) value="{{$package->translate('label', $language->locale)}}" @endisset>
                        </div>
                        <div class="form-group">
                            <label for="lines_{{$language->locale}}" class="form-label">@lang('Lines')</label>
                            <textarea name="lines[{{$language->locale}}]" id="lines_{{$language->locale}}" rows="10" class="form-control">@isset($package){{$package->translate('lines', $language->locale)}}@endisset</textarea>
                        </div>
                        <div class="form-group">
                            <label for="description_{{$language->locale}}" class="form-label">@lang('Extra Description')</label>
                            <textarea name="description[{{$language->locale}}]" id="description_{{$language->locale}}" rows="10" class="form-control">@isset($package){{$package->translate('description', $language->locale)}}@endisset</textarea>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        @stack('card::media.before')
        <div class="card" id="media">
            <div class="card-header">
                <div class="card-title">@lang('Media')</div>
            </div>
            <div class="card-body">
                @stack('card::media.start')
                <input type="file" name="image" id="image" class="dropify-image" @isset($package) data-default-file="{{!empty($package->image_path) ? uploads_url($package->image_path) : ''}}" @endisset>
                @stack('card::media.end')
            </div>
        </div>
        @stack('card::media.after')
        @stack('card::price.before')
        <div class="card mt-3" id="price">
            <div class="card-header">
                <div class="card-title">@lang('Price Options')</div>
            </div>
            <div class="card-body">
                @stack('card::price.start')
                <div class="form-group">
                    <label for="monthly_price" class="form-label">@lang('Monthly Price')</label>
                    <input type="number" name="monthly_price" id="monthly_price" class="form-control" @isset($package) value="{{$package->monthly_price}}" @endisset>
                </div>
                <div class="form-group">
                    <label for="annually_discount" class="form-label">@lang('Annually Discount') <span class="badge bg-dark">%</span></label>
                    <input type="number" name="annually_discount" id="annually_discount" class="form-control" @isset($package) value="{{$package->annually_discount}}" @endisset>
                </div>
                @stack('card::price.end')
            </div>
        </div>
        @stack('card::price.after')
        @stack('card::attributes.before')
        <div class="card" id="attributes">
            <div class="card-header">
                <div class="card-title">@lang('Attributes')</div>
            </div>
            <div class="card-body">
                @stack('card::attributes.start')
                <label for="is_popular" class="form-label">@lang('Popular')</label>
                <select name="is_popular" id="is_popular" class="form-select">
                    <option value="0" @selected(isset($package) && !$package->is_popular)>@lang('No')</option>
                    <option value="1" @selected(isset($package) && $package->is_popular)>@lang('Yes')</option>
                </select>
                <label for="is_enabled" class="form-label">@lang('Enabled')</label>
                <select name="is_enabled" id="is_enabled" class="form-select">
                    <option value="1" @selected(isset($package) && $package->is_enabled)>@lang('Yes')</option>
                    <option value="0" @selected(isset($package) && !$package->is_enabled)>@lang('No')</option>
                </select>
                @stack('card::attributes.end')
            </div>
        </div>
        @stack('card::attributes.after')
    </div>
</div>