@extends('admin::layouts.master')
@section('page-title', __('Tenant Emails').' : '.$tenant->name)
@section('page-description', __('Manage Emails Owned by Selected Tenant'))
@section('page-actions')
<a href="{{ route('central.tenants.create') }}" class="btn btn-dark"><i class="fa fa-plus me-2"></i>@lang('New Tenant')</a>
@endsection
@section('content')
<div class="row">
    <div class="col-md-4">
        <form action="{{ route('central.tenants.emails.store') }}" method="POST">
            <div class="card">
                <div class="card-header">
                    <div class="card-title">
                        <i class="fa fa-info me-2"></i>
                        @lang('Email Details')
                    </div>
                </div>
                <div class="card-body">
                    <div class="form-group">
                        <label for="email">@lang('Email')</label>
                        <input type="text" name="email" id="email" class="form-control" value="{{ isset($email) ? $email->email : '' }}">
                    </div>
                    <div class="form-group">
                        <label for="size">@lang('Size')</label>
                        <input type="number" name="size" id="size" class="form-control" value="{{ isset($email) ? $email->size : '' }}">
                    </div>
                </div>
                <div class="card-footer">
                    @csrf
                    <input type="hidden" name="tenant_id" value="{{ $tenant->id }}">
                    @isset($email)
                    <input type="hidden" name="entity_id" value="{{ $email->id }}">
                    @endisset
                    <button class="btn btn-primary" type="submit">@lang('Save Email Account')</button>
                </div>
            </div>
        </form>
    </div>
    <div class="col-md-8">
        <div class="card">
            <div class="card-header">
                <div class="card-title">
                    <i class="fa fa-cubes me-2"></i>
                    @lang('Email Accounts')
                </div>
            </div>
            <div class="card-body">
                @livewire('datatable', ['table' => $table, 'params' => $tenant->id])
            </div>
        </div>
    </div>
</div>
@endsection
