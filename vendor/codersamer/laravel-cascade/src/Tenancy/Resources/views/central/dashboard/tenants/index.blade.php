@extends('admin::layouts.master')
@section('page-title', __('Tenants Management'))
@section('page-description', __('Manage Websites, Tenants and Packages Available and hosted by this System'))
@section('page-actions')
<a href="{{ route('central.tenants.create') }}" class="btn btn-dark"><i class="fa fa-plus me-2"></i>@lang('New Tenant')</a>
@endsection
@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <div class="card-title">
                    <i class="fa fa-cubes me-2"></i>
                    @lang('Tenants List')
                </div>
            </div>
            <div class="card-body">
                @livewire('datatable', ['table' => $table])
            </div>
        </div>
    </div>
</div>
@endsection
