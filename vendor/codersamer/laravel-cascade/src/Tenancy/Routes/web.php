<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Cascade\Tenancy\Http\Controllers\Central\PackagesController;
use Cascade\Tenancy\Http\Controllers\Central\TenantsController;
use Illuminate\Support\Facades\Route;

Route::central(function(){
        Route::get('/', [TenantsController::class, 'index'])->name('index');
        Route::post('/', [TenantsController::class, 'store'])->name('store');
        Route::get('/create', [TenantsController::class, 'create'])->name('create');
        Route::get('/migrate/{tenant}', [TenantsController::class, 'migrate'])->name('migrate');
        Route::get('/edit/{tenant}', [TenantsController::class, 'edit'])->name('edit');
        Route::get('/delete/{tenant}', [TenantsController::class, 'destroy'])->name('delete');

        Route::get('/domains/{tenant}', [TenantsController::class, 'domains'])->name('domains');
        Route::get('/domains/{tenant}/edit/{domain}', [TenantsController::class, 'editDomains'])->name('domains.edit');
        Route::get('/domains/{tenant}/delete/{domain}', [TenantsController::class, 'destroyDomains'])->name('domains.delete');
        Route::post('/domains', [TenantsController::class, 'storeDomains'])->name('domains.store');

        Route::get('/emails/{tenant}', [TenantsController::class, 'emails'])->name('emails');
        Route::post('/emails', [TenantsController::class, 'storeEmails'])->name('emails.store');
        Route::get('/emails/{tenant}/edit/{email}', [TenantsController::class, 'editEmails'])->name('emails.edit');
        Route::get('/emails/{tenant}/delete/{email}', [TenantsController::class, 'destroyEmails'])->name('emails.delete');

        Route::prefix('packages')->name('packages.')->group(function(){
			Route::get('/', [PackagesController::class, 'index'])->name('index');
			Route::post('/', [PackagesController::class, 'store'])->name('store');
			Route::get('/create', [PackagesController::class, 'create'])->name('create');
			Route::get('/edit/{package}', [PackagesController::class, 'edit'])->name('edit');
			Route::get('/delete/{package}', [PackagesController::class, 'destroy'])->name('delete');
        });

}, 'tenants');
