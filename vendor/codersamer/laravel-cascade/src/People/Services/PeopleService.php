<?php

namespace Cascade\People\Services;

use Cascade\System\Entities\Category;
use Cascade\System\Entities\Tag;
use Cascade\People\Enums\PersonFeature;
use Cascade\People\Http\Controllers\Dashboard\PersonController;
use Cascade\Users\Entities\Role;
use Exception;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Route;

class PeopleService
{
    protected static $types = [];

    protected $type = null;

    protected String $slug = '';

    protected String $title = '';

    protected ?String $menu_label = null;

    protected String $singular = '';

    protected String $plural = '';

    protected String $description = '';

    protected bool $showInMenu = false;

    protected String $icon = 'user';

    protected bool $hasCrud = true;

    protected array $features = [PersonFeature::All];

    protected array $requires = [];

    protected ?String $previewRoute = null;

    protected ?String $routesParam = null;

    protected array $defaultRoles = [];


    /******************************************
    * Static Interface
    ******************************************/
    //Get Collection of People Types
    public static function types() : Collection
    { return collect(static::$types); }

    //Generate New Type
    public static function new(String $className, ?String $slug = null) : self
    {
        $builder = new static($className, $slug);
        static::$types[$className] = $builder;
        return $builder;
    }

    //Get Type for Class
    public static function for(String $className) : self|null
    {
        $item = static::types()->first(function(PeopleService $personType) use($className){
            return $personType->type() == $className || $personType->slug() == $className;
        });
        return $item instanceof PeopleService ? $item : null;
    }

    //Alias For
    public static function of(String $className) : self|null
    {
        return static::for($className);
    }

    public static function registerRoutes() : void
    {
        foreach(static::types() as $personType)
        {
            if(!$personType->hasCrud()) { continue; }
            $personType->routes();
        }
    }


    /******************************************
    * Object Interface
    ******************************************/

    public function __construct(String $className, ?String $slug = null)
    {
        $this->type = $className;
        $this->slug = $slug == null ?  \str(class_basename($className))->slug() : $slug;
    }

    public function routes()
    {
        if(!$this->hasCrud()) { return; }
        $personType = $this;
        Route::prefix($personType->slug())->as($this->slug().'.')->group(function() use($personType)
        {
            Route::get('/', [PersonController::class, 'index'])->name('index')->defaults('type', $personType);
            Route::post('/', [PersonController::class, 'store'])->name('store')->defaults('type', $personType);
            Route::get('/create', [PersonController::class, 'create'])->name('create')->defaults('type', $personType);
            Route::get('/edit/{person}', [PersonController::class, 'edit'])->name('edit')->defaults('type', $personType);
            Route::get('/delete/{person}', [PersonController::class, 'destroy'])->name('destroy')->defaults('type', $personType);

            if($personType->supports(PersonFeature::Category))
            {
                Route::prefix('categories')->name('categories.')->group(function() use($personType){
                    Route::categories($personType->type());
                });
            }

            if($personType->supports(PersonFeature::Tags))
            {
                Route::prefix('tags')->name('tags.')->group(function() use($personType){
                    Route::tags($personType->type());
                });
            }

        });
    }

    /**
     * Get or Set Default Roles
     *
     * @param String|int|Role|array|null $role
     * @return array|self
     */
    public function defaultRoles(String|int|Role|array|null $roles = null)
    {
        if($roles === null) { return $this->defaultRoles; }
        $roles = Arr::wrap($roles);
        foreach($roles as $roleIdentifier)
        {
            $roleObject = null;
            if(is_integer($roleIdentifier) || intval($roleIdentifier) == $roleIdentifier)
            {
                $roleIdentifier = intval($roleIdentifier);
                try { $roleObject = Role::findById($roleIdentifier); } catch(Exception $ex) { }
            }
            else if(is_string($roleIdentifier))
            {
                try { $roleObject = Role::findByName($roleIdentifier); } catch(Exception $ex) { }
            }
            else if($roleIdentifier instanceof Role)
            {
                $roleObject = $roleIdentifier;
            }
            if($roleObject !== null)
            {
                $this->defaultRoles[] = $roleObject;
            }
        }
        return $this;
    }

    /**
     * Set or Get Slug
     *
     * @param String|null $slug
     * @return String|self
     */
    public function slug(?String $slug = null) : String|self
    {
        if($slug != null)
        {
            $this->slug = $slug;
            return $this;
        }
        return $this->slug;
    }

    /**
     * Set or Get Preview Route
     *
     * @param ?String $route
     * @return null|String|self
     */
    public function previewRoute(?String $route = null, ?String $routeParamName = null) : null|String|self
    {
        if($route != null) {
            $this->previewRoute = $route;
            $routeParamName = $routeParamName != null ? $routeParamName : strtolower(class_basename($this->type));
            $this->routesParam = $routeParamName;
            return $this;
        }
        return $this->previewRoute;
    }


    /**
     * Set or Get Type
     *
     * @param String|null $className
     * @return String|self
     */
    public function type(?String $className = null) : String|self
    {
        if($className != null)
        {
            $this->type = $className;
            return $this;
        }
        return $this->type;
    }


    /**
     * Get or Set title
     *
     * @param String|null $title
     * @return String|self
     */
    public function title(?String $title = null) : String|self
    {
        if($title != null)
        {
            $this->title = $title;
            return $this;
        }
        return $this->title;
    }

    /**
     * Get Or Set Singular Name
     *
     * @param String|null $name
     * return String|self
     */
    public function singular(?String $name = null) : String|self
    {
        if($name != null)
        {
            $this->singular = $name;
            return $this;
        }
        return $this->singular;
    }

    /**
     * Get Or Set Plural Name
     *
     * @param String|null $name
     * return String|self
     */
    public function plural(?String $name = null) : String|self
    {
        if($name != null)
        {
            $this->plural = $name;
            return $this;
        }
        return $this->plural;
    }

    /**
     * Get Or Set Menu Label
     *
     * @param String|null $label
     * return String|self
     */
    public function menuLabel(?String $label = null) : String|self
    {
        if($label != null)
        {
            $this->menu_label = $label;
            return $this;
        }
        return $this->menu_label ?? $this->plural();
    }


    /**
     * Get or Set Description
     *
     * @param String|null $description
     * @return String|self
     */
    public function description(?String $description = null) : String|self
    {
        if($description != null)
        {
            $this->description = $description;
            return $this;
        }
        return $this->description;
    }


    /**
     * Get or Set Icon
     *
     * @param String|null $icon
     * @return String|self
     */
    public function icon(?String $icon = null) : String|self
    {
        if($icon != null)
        {
            $this->icon = $icon;
            return $this;
        }
        return $this->icon;
    }


    /**
     * Set Post Type Visible in Menu or Not
     *
     * @param boolean|null $showInMenu
     * @return boolean|self
     */
    public function showInMenu(?bool $showInMenu = null) : bool|self
    {
        if($showInMenu !== null)
        {
            $this->showInMenu = $showInMenu;
            return $this;
        }
        return $this->showInMenu;
    }

    /**
     * Set Post Type Has Crud or Not
     *
     * @param boolean|null $hasCrud
     * @return boolean|self
     */
    public function hasCrud(?bool $hasCrud = null) : bool|self
    {
        if($hasCrud !== null)
        {
            $this->hasCrud = $hasCrud;
            return $this;
        }
        return $this->hasCrud;
    }

    /**
     * Set Post Features
     *
     * @return self|array
     */
    public function features(PersonFeature ...$features) : self|array
    {
        if($features == null || count($features) == 0) { return $this->features; }
        if(count($this->features) == 1 && $this->features[0] == PersonFeature::All)
        { $this->features = []; }

        foreach($features as $feature)
        {
            if($feature instanceof PersonFeature) { $this->features[] = $feature; }

        }

        if($this->supports(PersonFeature::Category))
        {
            Category::register($this->type());
        }

        if($this->supports(PersonFeature::Tags))
        {
            Tag::register($this->type());
        }

        return $this;
    }

    /**
     * Set Post Features
     *
     * @return self|array
     */
    public function requires(PersonFeature ...$features) : self|array
    {
        if($features == null || count($features) == 0) { return $this->requires; }
        if(count($this->requires) == 1 && $this->features[0] == PersonFeature::All)
        { $this->features = []; }

        foreach($features as $feature)
        {
            if($feature instanceof PersonFeature) { $this->requires[] = $feature; }

        }
        return $this;
    }

    /**
     * Check if Feature is Required
     *
     * @param PostFeature|String $feature
     *
     * @return void
     */
    public function require(PersonFeature|String $feature)
    {
        if(is_string($feature)) { $feature = PersonFeature::from($feature); }
        return in_array($feature, $this->requires) || in_array(PersonFeature::All, $this->requires);
    }

    /**
     * Check if Post Type Supports Selected Feature
     *
     * @param PostFeature $feature
     *
     * @return boolean
     */
    public function supports(PersonFeature|String $feature)
    {
        if(is_string($feature)) { $feature = PersonFeature::from($feature); }
        return in_array($feature, $this->features) || in_array(PersonFeature::All, $this->features);
    }

    /**
     * Check if Post Type Supports Selected Feature
     *
     * @param PostFeature $feature
     *
     * @return boolean
     */
    public function supportsAny(PersonFeature|String ...$features)
    {
        foreach($features as $feature)
        {
            if(is_string($feature)) { $feature = PersonFeature::from($feature); }
            if(in_array($feature, $this->features) || in_array(PersonFeature::All, $this->features))
            { return true; }
        }

        return false;
    }

    /**
     * Get Route for Selected Actions
     *
     * @param String $action
     *
     * @return String
     */
    public function route(String $action, $params = [])
    {
        return route('dashboard.people.'.$this->slug.'.'.$action, $params);
    }

    /**
     * Get Route for Selected Actions
     *
     * @param String $action
     *
     * @return String
     */
    public function routeName(String $action)
    {
        return 'dashboard.people.'.$this->slug.'.'.$action;
    }

}
