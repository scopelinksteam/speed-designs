<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPrimaryContactDetailsToPeopleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('people', function (Blueprint $table) {

            $table->string('primary_postal_code')->nullable()->after('city_id');
            $table->string('primary_address')->nullable()->after('city_id');

            $table->string('primary_website')->nullable()->after('primary_email');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('people', function (Blueprint $table) {
            $table->dropColumn('primary_website');
            $table->dropColumn('primary_address');
            $table->dropColumn('primary_postal_code');

        });
    }
}
