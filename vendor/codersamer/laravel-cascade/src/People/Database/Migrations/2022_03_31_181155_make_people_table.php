<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MakePeopleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('people', function (Blueprint $table) {
            $table->id();

            //Name
            $table->string('name')->nullable();
            $table->string('business_name')->nullable();
            $table->string('title')->nullable();
            $table->text('notes')->nullable();

            //Identifiers
            $table->string('code')->nullable();
            $table->unsignedInteger('entry_status')->default(0);
            $table->unsignedBigInteger('status_id')->default(0);

            //Personal Information
            $table->timestamp('birthdate')->nullable();
            $table->string('avatar_file')->nullable();
            $table->unsignedInteger('gender')->default(0);

            //Legal Information
            $table->string("identity_no")->nullable();
            $table->string("tax_no")->nullable();
            $table->string("commercial_no")->nullable();
            $table->string("passport_no")->nullable();

            //Business Information
            $table->string("employer_name")->nullable();
            $table->string("job")->nullable();
            $table->unsignedDouble('salary')->default(0);
            $table->unsignedDouble('monthly_income')->default(0);

            //Social Media
            $table->string('facebook')->nullable();
            $table->string('twitter')->nullable();
            $table->string('instagram')->nullable();
            $table->string('linkedin')->nullable();
            $table->string('whatsapp')->nullable();
            $table->string('youtube')->nullable();

            //Location
            $table->unsignedBigInteger('country_id')->default(0);
            $table->unsignedBigInteger('city_id')->default(0);

            //Polymorphism
            $table->string('type')->nullable();
            $table->nullableMorphs('owner');
            $table->nullableMorphs('parent');
            $table->nullableMorphs('referer');
            $table->nullableMorphs('supervisor');
            $table->nullableMorphs('operator');
            $table->nullableMorphs('source');



            //Dates
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('people');
    }
}
