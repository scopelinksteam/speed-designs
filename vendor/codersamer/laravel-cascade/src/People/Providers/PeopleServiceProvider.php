<?php

namespace Cascade\People\Providers;

use Cascade\Dashboard\Entities\MenuItem;
use Cascade\Dashboard\Entities\MenuSection;
use Cascade\Dashboard\Services\DashboardBuilder;
use Cascade\People\Services\PeopleService;
use Cascade\People\Tabs\Person\PersonBasicTab;
use Cascade\People\Tabs\Person\PersonContactTab;
use Cascade\People\Tabs\Person\PersonEmploymentTab;
use Cascade\People\Tabs\Person\PersonLegalTab;
use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;

class PeopleServiceProvider extends ServiceProvider
{
    /**
     * @var string $moduleName
     */
    protected $moduleName = 'People';

    /**
     * @var string $moduleNameLower
     */
    protected $moduleNameLower = 'people';

    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerTranslations();
        $this->registerConfig();
        $this->registerViews();
        $this->loadMigrationsFrom(module_path($this->moduleName, 'Database/Migrations'));
        $this->dashboard();
        $this->app->booted(function(){
            foreach(PeopleService::types() as $peopleType)
            {
                if($peopleType->supportsAny('code', 'name', 'business_name', 'title', 'notes', 'thumbnail', 'birthdate', 'gender'))
                { call_user_func([$peopleType->type(), 'AddTab'], PersonBasicTab::class); }
                if($peopleType->supportsAny('employer_name', 'job', 'salary', 'monthly_income'))
                { call_user_func([$peopleType->type(), 'AddTab'], PersonEmploymentTab::class); }
                if($peopleType->supportsAny('identity_no', 'tax_no', 'commercial_no', 'passport_no'))
                { call_user_func([$peopleType->type(), 'AddTab'], PersonLegalTab::class); }
                if($peopleType->supportsAny('facebook', 'twitter', 'instagram', 'linkedin', 'whatsapp', 'youtube', 'city', 'country', 'address', 'email','phone'))
                { call_user_func([$peopleType->type(), 'AddTab'], PersonContactTab::class); }
            }
        });
    }

    public function dashboard()
    {
        $peopleItems = [];
        DashboardBuilder::MainMenu()->AddSection(MenuSection::make('hr', __('Human Resources')));
        foreach(PeopleService::types() as $builder)
        {
            if(!$builder->showInMenu()) { continue; }
            $peopleItems[] = MenuItem::make($builder->menuLabel(), 'hr')
                                ->route('dashboard.people.'.$builder->slug().'.index')
                                ->icon($builder->icon());
        }

        if(count($peopleItems) > 0)
        { DashboardBuilder::MainMenu()->AddItems($peopleItems); }

    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->publishes([
            module_path($this->moduleName, 'Config/config.php') => config_path($this->moduleNameLower . '.php'),
        ], 'config');
        $this->mergeConfigFrom(
            module_path($this->moduleName, 'Config/config.php'), $this->moduleNameLower
        );
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $viewPath = resource_path('views/modules/' . $this->moduleNameLower);

        $sourcePath = module_path($this->moduleName, 'Resources/views');

        $this->publishes([
            $sourcePath => $viewPath
        ], ['views', $this->moduleNameLower . '-module-views']);

        $this->loadViewsFrom(array_merge($this->getPublishableViewPaths(), [$sourcePath]), $this->moduleNameLower);
    }

    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $langPath = resource_path('lang/modules/' . $this->moduleNameLower);

        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, $this->moduleNameLower);
        } else {
            $this->loadTranslationsFrom(module_path($this->moduleName, 'Resources/lang'), $this->moduleNameLower);
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }

    private function getPublishableViewPaths(): array
    {
        $paths = [];
        foreach (\Config::get('view.paths') as $path) {
            if (is_dir($path . '/modules/' . $this->moduleNameLower)) {
                $paths[] = $path . '/modules/' . $this->moduleNameLower;
            }
        }
        return $paths;
    }
}
