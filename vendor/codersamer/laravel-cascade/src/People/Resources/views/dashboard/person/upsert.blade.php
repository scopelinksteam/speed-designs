@extends('admin::layouts.master')
@section('page-title',(isset($person) ? __('Update')  : __('Create new')).' '.$type->singular())
@section('page-description', $type->description())
@section('page-actions')

<a href="{{ $type->route('index') }}" class="btn btn-dark">@lang('Back to List')</a>
@endsection
@section('content')
<form action="{{ $type->route('store') }}" method="POST" name="person-form" enctype="multipart/form-data">
    @csrf
    @isset($person)
    <input type="hidden" name="entity_id" value="{{ $person->id }}" />
    @endisset
    <x-dashboard-tabs :tabs="$tabs" :data="$__data" />
    <div class="p-3 rounded-3 bg-white">
        <button type="submit" class="btn btn-primary">@lang('Save')</button>
    </div>
    {{--
    <div class="row">
        <div class="col-12">
            <div class="card card-tabs">
                <div class="card-header px-0">
                    <ul class="nav nav-tabs px-4">
                        <li class="pt-2 me-3">
                            <h5><i class="fa fa-{{ $type->icon() }} mr-2"></i>{{ $type->singular() }} @lang('Details')</h5>
                        </li>
                        @
                        <li class="nav-item">
                            <a data-bs-target="#person-main-general-tab" data-bs-toggle="tab" class="nav-link active"><i class="fa fa-{{ $type->icon() }} mr-2"></i>General</a>
                        </li>
                        @endif
                    </ul>
                </div>

            </div>

        </div>
    </div>
    --}}
</form>
@endsection
