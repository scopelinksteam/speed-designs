@stack('card::legal.before')
<div class="card" id="legal">
    <div class="card-header">
        <div class="card-title">
            <i></i>
            @lang('Legal Information')
        </div>
    </div>
    <div class="card-body">
        @stack('card::legal.start')
        <div class="row">
            @if($type->supports('identity_no'))
            <div class="col-6">
                <div class="form-group">
                    <label for="identity_no">@lang('Identity No.')</label>
                    <input type="text" name="identity_no" id="identity_no" class="form-control" value="{{ isset($person) ? $person->Attribute('identity_no', '') : '' }}">
                </div>
            </div>
            @endif
            @if($type->supports('tax_no'))
            <div class="col-6">
                <div class="form-group">
                    <label for="tax_no">@lang('Tax No.')</label>
                    <input type="text" name="tax_no" id="tax_no" class="form-control" value="{{ isset($person) ? $person->Attribute('tax_no', '') : '' }}">
                </div>
            </div>
            @endif
            @if($type->supports('commercial_no'))
            <div class="col-6">
                <div class="form-group">
                    <label for="commercial_no">@lang('Commercial Registeration No.')</label>
                    <input type="text" name="commercial_no" id="commercial_no" class="form-control" value="{{ isset($person) ? $person->Attribute('commercial_no', '') : '' }}">
                </div>
            </div>
            @endif
            @if($type->supports('passport_no'))
            <div class="col-6">
                <div class="form-group">
                    <label for="passport_no">@lang('Passport No.')</label>
                    <input type="text" name="passport_no" id="passport_no" class="form-control" value="{{ isset($person) ? $person->Attribute('passport_no', '') : '' }}">
                </div>
            </div>
            @endif
        </div>
        @stack('card::legal.end')
    </div>
</div>
@stack('card::legal.after')