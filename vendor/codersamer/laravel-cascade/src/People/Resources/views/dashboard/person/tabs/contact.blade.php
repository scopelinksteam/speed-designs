<div class="card">
    <div class="card-header">
        <div class="card-title">@lang('Contact Details')</div>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-8">
                <div class="row">
                    @if($type->supports('phone'))
                    <div class="col-6">
                        <div class="form-group">
                            <label for="primary_phone" class="form-label">@lang('Primary Phone')</label>
                            <input type="tel" name="phone" id="primary_phone" class="form-control" value="{{ isset($person) ? $person->phone : '' }}">
                        </div>
                    </div>
                    @endif
                    @if($type->supports('email'))
                    <div class="col-6">
                        <div class="form-group">
                            <label for="primary_email" class="form-label">@lang('Primary Email')</label>
                            <input type="email" name="email" id="primary_email" class="form-control" value="{{ isset($person) ? $person->email : '' }}">
                        </div>
                    </div>
                    @endif
                    @if($type->supports('address'))
                    <div class="col-12">
                        <div class="form-group">
                            <label for="primary_address" class="form-label">@lang('Primary Address')</label>
                            <input type="text" name="primary_address" id="primary_address" class="form-control" value="{{ isset($person) ? $person->Attribute('primary_address') : '' }}">
                        </div>
                    </div>
                    @endif
                    @if($type->supports('country'))
                    <div class="col-6">
                        <div class="form-group">
                            <label for="country_id" class="form-label">@lang('Country')</label>
                            <select name="country_id" id="country_id" class="selectr">
                                @foreach (get_countries() as $item)
                                <option value="{{ $item->id }}" @selected(isset($person) && $person->Attribute('country_id', 0) == $item->id)>{{ $item->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    @endif
                    @if($type->supports('city'))
                    <div class="col-6">
                        <div class="form-group">
                            <label for="city_id" class="form-label">@lang('City')</label>
                            <select name="city_id" id="city_id" class="selectr">
                                @foreach (get_cities() as $item)
                                <option value="{{ $item->id }}" @selected(isset($person) && $person->Attribute('city_id', 0) == $item->id)>{{ $item->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    @endif
                </div>

            </div>
            <div class="col-4">
                @if($type->supportsAny('facebook', 'twitter', 'instagram', 'linkedin', 'whatsapp', 'youtube'))
                <div class="card-title mb-2 w-100">
                    <i class="fa fa-dddd me-2"></i>@lang('Social Media')
                    <hr>
                </div>
                @endif
                @if($type->supports('facebook'))
                <div class="form-group">
                    <label for="facebook" class="form-label">@lang('Facebook')</label>
                    <input type="text" name="facebook" id="facebook" class="form-control" value="{{ isset($person) ? $person->Attribute('facebook', '') : '' }}">
                </div>
                @endif
                @if($type->supports('twitter'))
                <div class="form-group">
                    <label for="twitter" class="form-label">@lang('Twitter')</label>
                    <input type="text" name="twitter" id="twitter" class="form-control" value="{{ isset($person) ? $person->Attribute('twitter', '') : '' }}">
                </div>
                @endif
                @if($type->supports('instagram'))
                <div class="form-group">
                    <label for="instagram" class="form-label">@lang('Instagram')</label>
                    <input type="text" name="instagram" id="instagram" class="form-control" value="{{ isset($person) ? $person->Attribute('instagram', '') : '' }}">
                </div>
                @endif
                @if($type->supports('linkedin'))
                <div class="form-group">
                    <label for="linkedin" class="form-label">@lang('Linkedin')</label>
                    <input type="text" name="linkedin" id="linkedin" class="form-control" value="{{ isset($person) ? $person->Attribute('linkedin', '') : '' }}">
                </div>
                @endif
                @if($type->supports('whatsapp'))
                <div class="form-group">
                    <label for="whatsapp" class="form-label">@lang('Whatsapp')</label>
                    <input type="text" name="whatsapp" id="whatsapp" class="form-control" value="{{ isset($person) ? $person->Attribute('whatsapp', '') : '' }}">
                </div>
                @endif
                @if($type->supports('youtube'))
                <div class="form-group" class="form-label">
                    <label for="youtube">@lang('Youtube')</label>
                    <input type="text" name="youtube" id="youtube" class="form-control" value="{{ isset($person) ? $person->Attribute('youtube', '') : '' }}">
                </div>
                @endif
            </div>
        </div>
    </div>
</div>
