<div class="card">
    <div class="card-header">
        <div class="card-title">@lang('Employment Details')</div>
    </div>
    <div class="card-body">
        <div class="row">
            @if($type->supports('employer_name'))
            <div class="col-6">
                <div class="form-group">
                    <label for="employer_name">@lang('Employer Name')</label>
                    <input type="text" name="employer_name" id="employer_name" class="form-control" value="{{ isset($person) ? $person->Attribute('employer_name', '') : '' }}">
                </div>
            </div>
            @endif
            @if($type->supports('job'))
            <div class="col-6">
                <div class="form-group">
                    <label for="job">@lang('Job')</label>
                    <input type="text" name="job" id="job" class="form-control" value="{{ isset($person) ? $person->Attribute('job', '') : '' }}">
                </div>
            </div>
            @endif
            @if($type->supports('salary'))
            <div class="col-6">
                <div class="form-group">
                    <label for="salary">@lang('Salary')</label>
                    <input type="number" name="salary" id="salary" class="form-control" value="{{ isset($person) ? $person->Attribute('salary', '0') : '' }}">
                </div>
            </div>
            @endif
            @if($type->supports('monthly_income'))
            <div class="col-6">
                <div class="form-group">
                    <label for="monthly_income">@lang('Monthly Income')</label>
                    <input type="number" name="monthly_income" id="monthly_income" class="form-control" value="{{ isset($person) ? $person->Attribute('monthly_income', 0) : '' }}">
                </div>
            </div>
            @endif
        </div>
    </div>
</div>
