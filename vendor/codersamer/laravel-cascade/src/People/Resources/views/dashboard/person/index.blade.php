@extends('admin::layouts.master')
@section('page-title', $type->title())
@section('page-description', $type->description())
@section('page-actions')
<a href="{{ $type->route('create') }}" class="btn btn-success">@lang('Create New')</a>
@endsection
@section('content')
<div class="card">
    <div class="card-header">
        <div class="card-title">
            <i class="fa fa-{{ $type->icon() }} mr-2"></i>
            {{ $type->plural() }}
        </div>
    </div>
    <div class="card-body">
        @livewire('datatable', ['table' => $table, 'params' => $type->type()])
    </div>
</div>
@endsection
