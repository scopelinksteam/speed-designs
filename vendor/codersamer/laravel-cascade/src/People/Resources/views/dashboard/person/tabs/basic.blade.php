<div class="row">
    <div class="col-md-8">
        @stack('primary::start')
        @stack('card::content.before')
        <div class="card" id="content">
            @csrf
            <div class="card-header">
                {{ __('Basic Details') }}
            </div>
            <div class="card-body">
                @stack('card::content.start')
                <div class="row">
                    @if($type->supports('code'))
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="code" class="form-label">@lang('Code')</label>
                            <input type="text" name="code" id="code" class="form-control" value="{{ isset($person) ? $person->Attribute('code', '') : '' }}">
                        </div>
                    </div>
                    @endif
                    @if($type->supports('title'))
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="title" class="form-label">@lang('Title')</label>
                            <input type="text" name="title" id="title" class="form-control" value="{{ isset($person) ? $person->Attribute('title', '') : '' }}">
                        </div>
                    </div>
                    @endif
                    @if($type->supports('business_name'))
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="business_name" class="form-label">@lang('Business Name')</label>
                            <input type="text" name="business_name" id="business_name" class="form-control" value="{{ isset($person) ? $person->Attribute('business_name', '') : '' }}">
                        </div>
                    </div>
                    @endif
                    @if($type->supports('name'))
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="name" class="form-label">@lang('Name')</label>
                            <input type="text" name="name" id="name" class="form-control" value="{{ isset($person) ? $person->name : old('name') }}">
                        </div>
                    </div>
                    @endif
                    @if($type->supports('password'))
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="password" class="form-label">@lang('Password')</label>
                            <input type="password" name="password" id="password" class="form-control" value="">
                        </div>
                    </div>
                    @endif
                    @if($type->supports('notes'))
                    <div class="col-12">
                        <div class="form-group">
                            <label for="notes" class="form-label">@lang('Notes')</label>
                            <textarea name="notes" id="notes" rows="10" class="editor">{!! isset($person) ? $person->Attribute('notes', '') : '' !!}</textarea>
                        </div>
                    </div>
                    @endif
                </div>
                @stack('card::content.end')
            </div>
        </div>
        @stack('card::content.after')
        @stack('primary::end')
    </div>
    <div class="col-md-4">
        @stack('secondary::start')
        @if($type->supports('thumbnail'))
        @stack('card::avatar.before')
        <div class="card" id="avatar">
            <div class="card-header">
                {{ __('Profile Image') }}
            </div>
            <div class="card-body">
                @stack('card::avatar.start')
                <div class="form-group">
                    <input type="file" name="avatar" class="dropify-image" id="">
                </div>
                @stack('card::avatar.end')
            </div>
        </div>
        @stack('card::avatar.after')
        @endif

        @if($type->supportsAny('gender'))
        @stack('card::additional.before')
        <div class="card" id="additional">
            <div class="card-header">
                <div class="card-title">@lang('Additional Info')</div>
            </div>
            <div class="card-body">
                @if($type->supports('gender'))
                <div class="form-group">
                    <label for="gender" class="form-label">@lang('Gender')</label>
                    <select name="gender" id="gender" class="selectr">
                        <option value="0" @selected(isset($person) && $person->Attribute('gender', 0) == 0)>@lang('Unknown')</option>
                        <option value="1" @selected(isset($person) && $person->Attribute('gender', 0) == 1)>@lang('Male')</option>
                        <option value="2" @selected(isset($person) && $person->Attribute('gender', 0) == 2)>@lang('Female')</option>
                    </select>
                </div>
                @endif
                @if($type->supports('birthdate'))
                <div class="form-group">
                    <label for="birthdate" class="form-label">@lang('Birthdate')</label>
                    <input type="date" name="birthdate" id="birthdate" class="form-control" value="{{ isset($person) ? timestamp_to_date_input($person->Attribute('birthdate', '')) : '' }}">
                </div>
                @endif
            </div>
        </div>
        @stack('card::additional.after')
        @endif

        @stack('secondary::end')
    </div>
</div>
