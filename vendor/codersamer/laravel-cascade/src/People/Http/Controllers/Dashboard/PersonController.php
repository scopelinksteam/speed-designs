<?php

namespace Cascade\People\Http\Controllers\Dashboard;

use Cascade\People\Actions\PersonSavedAction;
use Cascade\People\Actions\PersonSavingAction;
use Cascade\People\Entities\Person;
use Cascade\People\Enums\Gender;
use Cascade\People\Enums\PersonFeature;
use Cascade\People\Services\PeopleService;
use Cascade\People\Tables\PeopleTable;
use Cascade\System\Enums\EntityAction;
use Cascade\System\Services\Action;
use Cascade\System\Services\Feedback;
use Cascade\Users\Entities\User;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Gate;

class PersonController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index(PeopleService $personType)
    {
        return view('people::dashboard.person.index', [
            'type' => $personType,
            'table' => new PeopleTable($personType->type())
        ]);
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create(PeopleService $personType)
    {
        return view('people::dashboard.person.upsert', [
            'type' => $personType,
            'tabs' => call_user_func([$personType->type(), 'GetTabs'])
        ]);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(PeopleService $personType, Request $request)
    {
        $entityId = $request->input('entity_id', 0);
        $entityClass = $personType->type();
        $entity = call_user_func([$entityClass, 'withoutGlobalScope'], 'type_constrain')->find($entityId);
        $action = $entity != null ? EntityAction::Update : EntityAction::Create;
        $entity = $entity == null ? new $entityClass : $entity;
        $errors = [];

        $entity->type = $personType->type();


        //Code - Attribute
        if($personType->supports(PersonFeature::Code))
        {
            $code = $request->input('code', null);
            if($personType->require(PersonFeature::Code) && $code == null)
            {
                $errors[] = __('Code is Required and cannot be empty');
            }
        }

        //Name
        if($personType->supports(PersonFeature::Name))
        {
            $name = $request->input('name', null);
            if($personType->require(PersonFeature::Name) && $name == null)
            { $errors[] = __('Name is Required and cannot be empty'); }
            $entity->name = $name ?? '';
        }

        //Title - Attribute
        if($personType->supports(PersonFeature::Title))
        {
            $title = $request->input('title', null);
            if($personType->require(PersonFeature::Title) && $title == null)
            { $errors[] = __('Title is Required and cannot be empty'); }
        }
        //Business Name - Attribute
        if($personType->supports(PersonFeature::BusinessName))
        {
            $business_name = $request->input('attributes.business_name', null);
            if($personType->require(PersonFeature::BusinessName) && $business_name == null)
            { $errors[] = __('Business Name is Required and cannot be empty'); }
        }

        //Notes - Attribute
        if($personType->supports(PersonFeature::Notes))
        {
            $notes = $request->input('notes', null);
            if($personType->require(PersonFeature::Notes) && $notes == null)
            { $errors[] = __('Notes is Required and cannot be empty'); }
        }

        //Gender
        if($personType->supports(PersonFeature::Gender))
        {
            $gender = $request->input('gender', -1);
            if($personType->require(PersonFeature::Gender) && $gender == -1)
            { $errors[] = __('Gender is Required and cannot be empty'); }

        }


        //Birthdate - Attribute
        if($personType->supports(PersonFeature::Birthdate))
        {
            $birthdate = $request->input('birthdate', null);
            if($personType->require(PersonFeature::Birthdate) && $birthdate == null)
            { $errors[] = __('Birthdate is Required and cannot be empty'); }
        }


        //Thumbnail
        if($personType->supports(PersonFeature::Thumbnail))
        {
            if($personType->require(PersonFeature::Thumbnail) &&  $request->file('avatar', null) == null && empty($entity->avatar_url))
            { $errors[] = __('Media is Required and cannot be empty'); }
        }

        //Employer Name - Attribute
        if($personType->supports(PersonFeature::EmployerName))
        {
            $employer_name = $request->input('employer_name', null);
            if($personType->require(PersonFeature::EmployerName) && $employer_name  == null)
            { $errors[] = __('Employer Name is Required and cannot be empty'); }

        }

        //Job - Attribute
        if($personType->supports(PersonFeature::Job))
        {
            $job = $request->input('job', null);
            if($personType->require(PersonFeature::Job) && $job  == null)
            { $errors[] = __('Job is Required and cannot be empty'); }

        }

        //Salary - Attribute
        if($personType->supports(PersonFeature::Salary))
        {
            $salary = $request->input('salary', null);
            if($personType->require(PersonFeature::Salary) && $salary  == null)
            { $errors[] = __('Salary is Required and cannot be empty'); }
        }

        //Monthly Income - Attribute
        if($personType->supports(PersonFeature::MonthlyIncome))
        {
            $monthly_income = $request->input('monthly_income', null);
            if($personType->require(PersonFeature::MonthlyIncome) && $monthly_income  == null)
            { $errors[] = __('Monthly Income is Required and cannot be empty'); }
        }

        //IdentityNo - Attribute
        if($personType->supports(PersonFeature::IdentityNo))
        {
            $identity_no = $request->input('identity_no', null);
            if($personType->require(PersonFeature::IdentityNo) && $identity_no  == null)
            { $errors[] = __('Identity No. is Required and cannot be empty'); }

        }

        //TaxNo - Attribute
        if($personType->supports(PersonFeature::TaxNo))
        {
            $tax_no = $request->input('tax_no', null);
            if($personType->require(PersonFeature::TaxNo) && $tax_no  == null)
            { $errors[] = __('Tax No. is Required and cannot be empty'); }

        }

        //CommercialNo
        if($personType->supports(PersonFeature::CommercialNo))
        {
            $commercial_no = $request->input('commercial_no', null);
            if($personType->require(PersonFeature::CommercialNo) && $commercial_no  == null)
            { $errors[] = __('Commercial No. is Required and cannot be empty'); }
        }

        //PassportNo
        if($personType->supports(PersonFeature::PassportNo))
        {
            $passport_no = $request->input('passport_no', null);
            if($personType->require(PersonFeature::PassportNo) && $passport_no  == null)
            { $errors[] = __('Commercial No. is Required and cannot be empty'); }
        }



        //-- Contact Information

        //Facebook
        if($personType->supports(PersonFeature::Facebook))
        {
            $facebook = $request->input('facebook', null);
            if($personType->require(PersonFeature::Facebook) && $facebook  == null)
            { $errors[] = __('Facebook is Required and cannot be empty'); }
        }

        //Twitter
        if($personType->supports(PersonFeature::Twitter))
        {
            $twitter = $request->input('twitter', null);
            if($personType->require(PersonFeature::Twitter) && $twitter  == null)
            { $errors[] = __('Twitter is Required and cannot be empty'); }
        }

        //Instagram
        if($personType->supports(PersonFeature::Instagram))
        {
            $instagram = $request->input('instagram', null);
            if($personType->require(PersonFeature::Instagram) && $instagram  == null)
            { $errors[] = __('Instagram is Required and cannot be empty'); }
        }

        //Linkedin
        if($personType->supports(PersonFeature::Linkedin))
        {
            $linkedin = $request->input('linkedin', null);
            if($personType->require(PersonFeature::Linkedin) && $linkedin  == null)
            { $errors[] = __('Linkedin is Required and cannot be empty'); }
        }

        //Whatsapp
        if($personType->supports(PersonFeature::Whatsapp))
        {
            $whatsapp = $request->input('linkedin', null);
            if($personType->require(PersonFeature::Whatsapp) && $whatsapp  == null)
            { $errors[] = __('Whatsapp is Required and cannot be empty'); }
        }

        //Youtube
        if($personType->supports(PersonFeature::Youtube))
        {
            $youtube = $request->input('youtube', null);
            if($personType->require(PersonFeature::Youtube) && $youtube  == null)
            { $errors[] = __('Youtube is Required and cannot be empty'); }
        }

        //Phone
        if($personType->supports(PersonFeature::Phone))
        {
            $primary_phone = $request->input('phone', null);
            if($personType->require(PersonFeature::Phone) && $primary_phone  == null)
            { $errors[] = __('Primary Phone is Required and cannot be empty'); }
            $entity->phone = $primary_phone ?? '';
        }

        //Email
        if($personType->supports(PersonFeature::Email))
        {
            $primary_email = $request->input('email', null);
            if($personType->require(PersonFeature::Email) && $primary_email  == null)
            { $errors[] = __('Primary Email is Required and cannot be empty'); }
            $entity->email = $primary_email ?? \Str::random(18).'@'.parse_url(request()->root())['host'];
            $existUser = User::where('email', $entity->email)->first();
            if($existUser != null && $existUser->id != $entity->id)
            {
                $errors[] = __('Email Specified is Already used before in our System');
            }
        }

        //Password
        if($personType->supports(PersonFeature::Password) && $request->filled('password'))
        {
            $entity->password = bcrypt($request->password);
        }

        if($entity->password == null)
        {
            $entity->password = bcrypt(\Str::random(24));
        }

        /*
        //Address
        if($personType->supports(PersonFeature::Address))
        {
            $primary_address = $request->input('primary_address', null);
            if($personType->require(PersonFeature::Address) && $primary_address  == null)
            { $errors[] = __('Primary Address is Required and cannot be empty'); }
            $entity->primary_address = $primary_address ?? '';
        }
        /*
        //City
        if($personType->supports(PersonFeature::City))
        {
            $city_id = $request->input('city_id', null);
            if($personType->require(PersonFeature::City) && $city_id  == null)
            { $errors[] = __('City is Required and cannot be empty'); }
            $entity->city_id = $city_id ?? 0;
        }

        //Country
        if($personType->supports(PersonFeature::Country))
        {
            $country_id = $request->input('country_id', null);
            if($personType->require(PersonFeature::Country) && $country_id  == null)
            { $errors[] = __('Country is Required and cannot be empty'); }
            $entity->country_id = $country_id ?? 0;
        }
        */
        if(count($errors) > 0)
        {
            $message = '<b>'.__('Cannot be saved due to validation failure').'</b> :';
            foreach($errors as $error) { $message .= '<br />'.$error; }
            Feedback::error($message);
            Feedback::flash();
            return back();
        }
        Action::Apply(PersonSavingAction::class, $entity);
        $entity->save();

        //Set Inputs
        //Business Name
        if($personType->supports(PersonFeature::BusinessName))
        {
            $entity->storeAttribute('business_name', $request->input('business_name', ''));
        }
        //Title
        if($personType->supports(PersonFeature::Title))
        { $entity->storeAttribute('title', $request->input('title', '')); }

        //Notes
        if($personType->supports(PersonFeature::Notes))
        { $entity->storeAttribute('notes', $request->input('notes', '')); }

        //Birthdate
        if($personType->supports(PersonFeature::Birthdate))
        { $entity->storeAttribute('birthdate', $request->input('birthdate', '')); }

        //Employer Name
        if($personType->supports(PersonFeature::EmployerName))
        { $entity->storeAttribute('employer_name', $request->input('employer_name', '')); }

        //Job
        if($personType->supports(PersonFeature::Job))
        { $entity->storeAttribute('job', $request->input('job', '')); }

        //Salary
        if($personType->supports(PersonFeature::Salary))
        { $entity->storeAttribute('salary', $request->input('salary', '0')); }

        //Tax Number
        if($personType->supports(PersonFeature::TaxNo))
        { $entity->storeAttribute('tax_no', $request->input('tax_no', '')); }

        //Identity Number
        if($personType->supports(PersonFeature::IdentityNo))
        { $entity->storeAttribute('identity_no', $request->input('identity_no', '')); }

        //Monthly Income
        if($personType->supports(PersonFeature::MonthlyIncome))
        { $entity->storeAttribute('monthly_income', $request->input('monthly_income', '0')); }

        //Passport Number
        if($personType->supports(PersonFeature::PassportNo))
        { $entity->storeAttribute('passport_no', $request->input('passport_no', '')); }

        //Commercial Number
        if($personType->supports(PersonFeature::CommercialNo))
        { $entity->storeAttribute('commercial_no', $request->input('commercial_no', '')); }

        //Social Media
        //Youtube
        if($personType->supports(PersonFeature::Youtube))
        { $entity->storeAttribute('youtube', $request->input('youtube', '')); }

        //Whatsapp
        if($personType->supports(PersonFeature::Whatsapp))
        { $entity->storeAttribute('whatsapp', $request->input('whatsapp', '')); }

        //Facebook
        if($personType->supports(PersonFeature::Facebook))
        { $entity->storeAttribute('facebook', $request->input('facebook', '')); }

        //Twitter
        if($personType->supports(PersonFeature::Twitter))
        { $entity->storeAttribute('twitter', $request->input('twitter', '')); }

        //Instagram
        if($personType->supports(PersonFeature::Instagram))
        { $entity->storeAttribute('instagram', $request->input('instagram', '')); }

        //Linkedin
        if($personType->supports(PersonFeature::Linkedin))
        { $entity->storeAttribute('linkedin', $request->input('linkedin', '')); }

        //Code
        if($personType->supports(PersonFeature::Code))
        { $entity->storeAttribute('code', $request->input('code', '')); }

        //Gender
        if($personType->supports(PersonFeature::Gender))
        {
            $gender= intval($request->input('gender', '0'));
            $gender = $gender == -1 ? 0 : $gender;
            $entity->storeAttribute('gender', $gender);
        }



        //Image After Save and Checking
        //Thumbnail
        if($personType->supports(PersonFeature::Thumbnail))
        {
            $file = $request->file('avatar', null);
            if($file != null)
            {
                $entity->avatar_url = $file->store('people/'.$personType->slug(), ['disk' => 'uploads']);
            }
        }

        
        $entity->save();

        foreach($request->input('attributes', []) as $key => $value)
        {
            $entity->storeAttribute($key, $value);
        }
        Action::Apply(PersonSavedAction::class, $entity);
        Feedback::success(__('Saved Successfully'));
        Feedback::flash();
        return redirect()->to($action == EntityAction::Update ?  $personType->route('edit', ['person' => $entity]) : $personType->route('index'));
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('system::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($person, PeopleService $personType)
    {
        $person = call_user_func([$personType->type(), 'withoutGlobalScope'], 'type_constrain')->find($person);
        return $this->create($personType)->with([
            'person' => $person
        ]);
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($person, PeopleService $personType)
    {
        $person = call_user_func([$personType->type(), 'withoutGlobalScope'], 'type_constrain')->find(intval($person));
        Gate::authorize('delete person');
        $person->delete();
        Feedback::alert(__('Deleted Successfully'));
        Feedback::flash();
        return redirect()->to($personType->route('index'));
    }
}
