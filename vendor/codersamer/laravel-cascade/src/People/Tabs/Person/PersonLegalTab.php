<?php

namespace Cascade\People\Tabs\Person;

use Cascade\Dashboard\Abstraction\DashboardTab;
use Cascade\System\Services\System;
use Illuminate\View\View;

class PersonLegalTab extends DashboardTab
{
    public function name(): string
    {
        return __('Legals');
    }

    public function slug(): string
    {
        return 'legal';
    }

    public function view(): View|string
    {
        return System::FindView('people::dashboard.person.tabs.legal', $this->data);
    }
}
