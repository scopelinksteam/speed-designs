<?php

namespace Cascade\People\Tabs\Person;

use Cascade\Dashboard\Abstraction\DashboardTab;
use Cascade\System\Services\System;
use Illuminate\View\View;

class PersonEmploymentTab extends DashboardTab
{
    public function name(): string
    {
        return __('Employment');
    }

    public function slug(): string
    {
        return 'employment';
    }

    public function view(): View|string
    {
        return System::FindView('people::dashboard.person.tabs.employment', $this->data);
    }
}
