<?php

namespace Cascade\People\Tabs\Person;

use Cascade\Dashboard\Abstraction\DashboardTab;
use Cascade\System\Services\System;
use Illuminate\View\View;

class PersonContactTab extends DashboardTab
{
    public function name(): string
    {
        return __('Contact');
    }

    public function slug(): string
    {
        return 'contact';
    }

    public function view(): View|string
    {
        return System::FindView('people::dashboard.person.tabs.contact', $this->data);
    }
}
