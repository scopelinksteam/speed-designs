<?php

namespace Cascade\People\Tabs\Person;

use Cascade\Dashboard\Abstraction\DashboardTab;
use Cascade\System\Services\System;
use Illuminate\View\View;

class PersonBasicTab extends DashboardTab
{
    public function name(): string
    {
        return __('Basic Details');
    }

    public function slug(): string
    {
        return 'basic';
    }

    public function view(): View|string
    {
        return System::FindView('people::dashboard.person.tabs.basic', $this->data);
    }
}
