<?php

namespace Cascade\People\Tables;

use Cascade\Dashboard\Services\Datatables\Action;
use Cascade\Dashboard\Services\Datatables\Column;
use Cascade\Dashboard\Services\Datatables\DataTable;
use Cascade\People\Entities\Person;
use Cascade\People\Enums\Gender;
use Cascade\People\Enums\PersonFeature;
use Cascade\People\Services\PeopleService;

class PeopleTable extends DataTable
{

    protected String $model = '';
    protected ?PeopleService $service = null;

    public function __construct($model)
    {
        $this->model = $model;
        $this->service = PeopleService::for($this->model);
        $service = $this->service;
        $this->editColumn('name', function(Person $person) use($service) {
            $appends = $service->supports(PersonFeature::Title) ? $person->title : null;
            $text = '';
            if(empty($appends)) { $text = $person->name; }
            else { $text = $person->name .'<br />'.$this->subtitle($person->title);}

            if($service->supports(PersonFeature::Gender))
            { $text = $this->gender(Gender::from(intval($person->Attribute('gender', '0') ?? '0'))).$text; }

            return $text;
        });
        $this->editColumn('business_name', function(Person $person){
            return $person->Attribute('business_name');
        });
        $this->editColumn('code', function(Person $person){
            return $person->Attribute('code');
        });
    }

    public function query()
    {
        $model = $this->service->type();
        $query = call_user_func([$model, 'query']);

        return $query;
    }

    public function columns()
    {
        $columns = [];
        //Code
        if($this->service->supports(PersonFeature::Code))
        { $columns[] = Column::make('code')->text(__('Code')); }
        //Business Name
        if($this->service->supports(PersonFeature::BusinessName))
        { $columns[] = Column::make('business_name')->text(__('Business Name')); }
        //Name
        if($this->service->supports(PersonFeature::Name))
        { $columns[] = Column::make('name')->text(__('Name')); }
        //Phone
        if($this->service->supports(PersonFeature::Phone))
        { $columns[] = Column::make('phone')->text(__('Phone')); }

        if($this->service->supports(PersonFeature::Email))
        { $columns[] = Column::make('email')->text(__('Email')); }

        return $columns;
    }

    public function actions()
    {
        $actions = [];

        $actions[] = Action::make('edit', __('Edit'))->route($this->service->routeName('edit'), ['person' => 'id']);
        $actions[] = Action::make('delete', __('Delete'))->route($this->service->routeName('destroy'), ['person' => 'id']);

        return $actions;
    }
}
