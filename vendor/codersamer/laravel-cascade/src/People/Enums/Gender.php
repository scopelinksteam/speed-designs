<?php

namespace Cascade\People\Enums;

enum Gender : int
{
    case Unknown = 0;
    case Male = 1;
    case Female = 2;
}
