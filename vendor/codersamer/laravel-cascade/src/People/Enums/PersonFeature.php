<?php

namespace Cascade\People\Enums;

enum PersonFeature : String
{
    case All = 'all';
    case Title = 'title';
    case Name = 'name';
    case BusinessName = 'business_name';
    case Notes = 'notes';
    case Code = 'code';
    case Thumbnail = 'thumbnail';
    case Category = 'category';
    case Tags = 'tags';
    case Gender = 'gender';
    case Birthdate = 'birthdate';

    //Legal Information
    case IdentityNo = 'identity_no';
    case TaxNo = 'tax_no';
    case CommercialNo = 'commercial_no';
    case PassportNo = 'passport_no';

    //Employment Information
    case EmployerName = 'employer_name';
    case Job = 'job';
    case Salary = 'salary';
    case MonthlyIncome = 'monthly_income';

    //Contact Information - Social Media
    case Facebook = 'facebook';
    case Twitter = 'twitter';
    case Instagram = 'instagram';
    case Linkedin = 'linkedin';
    case Whatsapp = 'whatsapp';
    case Youtube = 'youtube';

    case Phone = 'phone';
    case Email = 'email';
    case Website = 'website';
    case Address = 'address';

    case Country = 'country';
    case City = 'city';

    case Password = "password";

    public function translate()
    {
        return __($this->value);
    }
}
