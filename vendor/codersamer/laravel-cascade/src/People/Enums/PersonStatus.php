<?php

namespace Cascade\People\Enums;

enum PersonStatus : int
{
    case Draft = 0;
    case Active = 1;
    case Pending = 2;
}
