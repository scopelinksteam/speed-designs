<?php

namespace Cascade\People\Entities;

use Cascade\People\Enums\Gender;
use Cascade\People\Enums\PersonStatus;
use Cascade\People\Services\PeopleService;
use Cascade\Users\Entities\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Person extends User
{
    protected $guard_name = 'web';
    //protected $table = 'people';

    /*
    protected $casts = [
        'entry_status' => PersonStatus::class,
        'birthdate' => 'datetime',
        'gender' => Gender::class,
        'salary' => 'double',
        'monthly_income' => 'double',
    ];
    */

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('type_constrain', function(Builder $builder){
            return $builder->where('type', static::class);
        });

        static::saving(function($entity){
            $entity->type = static::class;
        });

        static::created(function($entity){
            $roles = PeopleService::of(static::class)->defaultRoles();
            $entity->syncRoles(...$roles);
            $user = User::find($entity->id);
            if($user != null) { $user->syncRoles(...$roles); }
        });
    }

    public static function routes()
    {
        return PeopleService::of(static::class)->registerRoutes();
    }

    /*

    //Relations
    public function country() { return $this->belongsTo(Country::class, 'country_id'); }
    public function city() { return $this->belongsTo(City::class, 'city_id'); }
    public function owner() { return $this->morphTo('owner'); }
    public function parent() { return $this->morphTo('parent'); }
    public function referer() { return $this->morphTo('referer'); }
    public function supervisor() { return $this->morphTo('supervisor'); }
    public function operator() { return $this->morphTo('operator'); }
    public function source() { return $this->morphTo('source'); }
    */
    /*
    //Attributes
    public function getAvatarAttribute()
    {
        return $this->avatar_file == null ? '' : uploads_url($this->avatar_file);
    }
    */
}
