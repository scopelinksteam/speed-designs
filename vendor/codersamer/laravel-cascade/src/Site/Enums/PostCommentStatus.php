<?php

namespace Cascade\Site\Enums;

enum PostCommentStatus : int
{
    case Allowed = 0;
    case Disabled = 1;
    case Authorized = 2;

    public function translate()
    {
        return __($this->name);
    }
}
