<?php

namespace Cascade\Site\Enums;

enum PostStatus : int
{
    case Draft = 0;
    case Pending = 1;
    case Published = 2;
    case Hidden = 3;
    case Protected = 4;

    public function translate()
    {
        return __($this->name);
    }
}
