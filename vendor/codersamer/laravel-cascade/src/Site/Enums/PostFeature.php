<?php

namespace Cascade\Site\Enums;

enum PostFeature : String
{
    case All = 'all';
    case Title = 'title';
    case Content = 'content';
    case Thumbnail = 'thumbnail';
    case Category = 'category';
    case Author = 'author';
    case Status = 'status';
    case Tags = 'tags';
    case Password = 'password';
    case Excerpt = 'excerpt';
    case Comments = 'comments';
    case MetaTitle = 'meta-title';
    case MetaKeywords = 'meta-keywords';
    case MetaDescription = 'meta-description';
    case Templates = 'templates';
    case ShowInMenu = 'show-in-menu';
    case Order = 'order';
    case Icon = 'icon';

    public function translate()
    {
        return __($this->value);
    }
}
