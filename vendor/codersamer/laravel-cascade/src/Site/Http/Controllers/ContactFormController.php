<?php

namespace Cascade\Site\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Cascade\Site\Entities\ContactMessage;
use Cascade\System\Http\Rules\RecaptchaRule;
use Cascade\System\Services\Feedback;
use Illuminate\Support\Facades\Validator;

class ContactFormController extends Controller
{

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        $valid = true;
        if(!$request->filled('name'))
        { Feedback::getInstance()->addError(__('Name is Required')); $valid = false; }
        if(!$request->filled('email'))
        { Feedback::getInstance()->addError(__('Email is Required')); $valid = false; }
        if(!$request->filled('phone'))
        { Feedback::getInstance()->addError(__('Phone is Required')); $valid = false; }
        if(!$request->filled('subject'))
        { Feedback::getInstance()->addError(__('Subject is Required')); $valid = false; }
        if(!$request->filled('message'))
        { Feedback::getInstance()->addError(__('Message is Required')); $valid = false; }

        $validator = Validator::make($request->only('recaptcha'), [
            'recaptcha' => ['required', new RecaptchaRule]
        ]);
        if($validator->fails())
        {
            Feedback::error(__('Valid to Verify Recaptcha'));
            $valid = false;
        }
        
        if(!$valid)
        {
            Feedback::flash();
            return back();
        }

        $message = new ContactMessage();
        $message->name = $request->name;
        $message->email = $request->email;
        $message->phone = $request->phone;
        $message->subject = $request->subject;
        $message->message =$request->message;
        $message->save();
        foreach($request->input('attributes', []) as $key => $value)
        {
            $message->storeAttribute($key, $value);
        }
        Feedback::getInstance()->addSuccess(__('Message Sent Successfully, Thanks for Contacting'));
        Feedback::flash();
        return back();
    }

}
