<?php

namespace Cascade\Site\Http\Controllers\API;

use Cascade\API\Http\Controllers\ApiController;
use Cascade\API\Services\ApiResponse;
use Cascade\Site\Services\PostType;
use Illuminate\Http\Request;

class PostsController extends ApiController
{

    protected $configurations;

    public function list(Request $request, PostType $type)
    {
        $this->configurations = $type;
        $query = call_user_func([$this->configurations->type(), 'query']);
        return ApiResponse::SendSuccess(__('Data Fetched Successfully'), [
            'records' => $query->paginate(10)
        ]);
    }
}