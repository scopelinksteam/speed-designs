<?php

namespace Cascade\Site\Http\Controllers;

use GuzzleHttp\Psr7\MimeType;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Cascade\Site\Services\Themes;
use Cascade\System\Services\Feedback;

class ThemesController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function assets($asset)
    {
        $mimeTypes = new MimeType();
        $file = Themes::GetActiveTheme()->GetPath().'/Resources/assets/'.$asset;
        if(file_exists($file))
        {
            return response()->file($file, [
                'Content-type' => $mimeTypes->fromFilename($file)
            ]);
        }
    }



    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('site::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('site::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('site::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        //
    }
}
