<?php

namespace Cascade\Site\Http\Controllers\Dashboard;

use Cascade\Site\Services\ThemeCustomizer;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Storage;
use Cascade\Site\Services\Themes;
use Cascade\System\Services\Feedback;
use Illuminate\Support\Facades\Gate;

class ThemesController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        Gate::authorize('view themes');
        return view('site::dashboard.themes.index');
    }

    public function customizer()
    {
        Gate::authorize('manage settings');
        return view('settings::dashboard.index', [
            'contexts' => ThemeCustomizer::GetContexts(),
            'title' => __('Theme Customizer'),
            'description' => __('Customize Appearance, Feel and Look of your Active Theme')
        ]);
    }
    public function PostInstall(Request $request)
    {
        Gate::authorize('install themes');
        if(!$request->file('theme'))
        {
            Feedback::getInstance()->addError(__('Theme File Cannot be Empty'));
            Feedback::flash();
            return back();
        }
        if($request->file('theme')->extension() != 'zip')
        {
            Feedback::getInstance()->addError(__('Invalid Theme File'));
            Feedback::flash();
            return back();
        }
        $filename = $request->file('theme')->store('temp', ['disk' => 'uploads']);
        $fullPath = Storage::disk('uploads')->path($filename);
        $installed = Themes::InstallFromFile($fullPath);
        if($installed)
        {
            Feedback::getInstance()->addSuccess(__('Theme Installed Successfully'));
        }
        else{
            Feedback::getInstance()->addError(__('Theme Cannot be Installed, Invalid Archive'));
        }
        Feedback::flash();
        return back();
    }

    /**
     * Show Screenshot for Theme
     *
     */
    public function screenshot($theme)
    {
        $theme = Themes::GetTheme($theme);
        $screenshot = $theme != null ? $theme->GetScreenshotFile() : module_path('Site', 'Resources/assets/images/default-theme-screenshot.png');
        return response()->file($screenshot);
    }

    /**
     * Enable Theme
     *
     * @return Renderable
     */
    public function enable($theme)
    {
        Gate::authorize('enable themes');
        $enabled = Themes::Enable($theme);
        if($enabled)
        { Feedback::getInstance()->addSuccess(__('Theme Activated Successfully')); }
        else { Feedback::getInstance()->addError(__('Theme Cannot be activated')); }
        Feedback::flash();
        return redirect()->back();
    }
}
