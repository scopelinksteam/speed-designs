<?php

namespace Cascade\Site\Http\Controllers\Dashboard;

use Cascade\Site\Entities\SiteMenu;
use Cascade\Site\Services\MenuBuilder;
use Cascade\Site\Tables\MenusTable;
use Cascade\System\Services\Feedback;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Gate;

class MenuBuilderController extends Controller
{

    public function index(MenusTable $table)
    {
        return view('site::dashboard.menus.index', [
            'table' => $table
        ]);
    }

    public function store(Request $request)
    {
        //Authorization
        Gate::authorize('build menus');

        //Validation
        if(!$request->filled('name'))
        {
            Feedback::error(__('Menu name is Required'));
            Feedback::flash();
            return back();
        }

        //Initialization
        $entityId = intval($request->input('entity_id', 0));
        $entity = $entityId > 0 ? SiteMenu::find($entityId) : new SiteMenu();

        $entity->name = $request->input('name');
        $entity->save();

        Feedback::success(__('Menu Saved Successfully'));
        Feedback::flash();
        return to_route('dashboard.site.menus.index');

    }

    public function edit(SiteMenu $menu)
    {
        return $this->index(new MenusTable)->with(['entity' => $menu]);
    }

    public function customize(SiteMenu $menu)
    {
        Gate::authorize('build menus');

        return view('site::dashboard.menus.customize', [
            'menu' => $menu,
            'builders' => MenuBuilder::Builders()
        ]);
    }

    public function destroy(SiteMenu $menu)
    {
        //Authorization
        Gate::authorize('build menus');

        $menu->delete();
        Feedback::success(__('Menu Deleted Successfully'));
        Feedback::flash();

        return to_route('dashboard.site.menus.index');
    }


}
