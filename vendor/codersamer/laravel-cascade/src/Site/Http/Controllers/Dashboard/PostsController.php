<?php

namespace Cascade\Site\Http\Controllers\Dashboard;

use Cascade\Site\Actions\PostAfterStoreAction;
use Cascade\Site\Actions\PostBeforeStoreAction;
use Cascade\Site\Entities\Post;
use Cascade\Site\Enums\PostCommentStatus;
use Cascade\Site\Enums\PostFeature;
use Cascade\Site\Enums\PostStatus;
use Cascade\Site\Services\PostType;
use Cascade\Site\Tables\PostsTable;
use Cascade\System\Entities\Category;
use Cascade\System\Entities\Tag;
use Cascade\System\Enums\EntityAction;
use Cascade\System\Services\Action;
use Cascade\System\Services\EntityBuilder;
use Cascade\System\Services\Feedback;
use Cascade\System\Services\System;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Hash;

class PostsController extends Controller
{
    public function index(PostType $type)
    {
        return System::view('site::dashboard.posts.index', [
            'type' => $type,
            'builder' => entity_builder($type->type()),
            'table' => call_user_func([$type->type(), 'GetPrimaryTable'], PostsTable::class) 
        ]);
    }

    public function create(PostType $type)
    {
        return System::view('site::dashboard.posts.upsert', [
            'type' => $type,
            'tabs' => call_user_func([$type->type(), 'GetTabs']),
        ]);
    }

    public function edit($post, PostType $type)
    {
        $post = call_user_func([$type->type(), 'withoutGlobalScope'], 'type_constrain')->find($post);
        return System::view('site::dashboard.posts.upsert', [
            'type' => $type,
            'post' => $post,
            'tabs' => call_user_func([$type->type(), 'GetTabs']),
        ]);
    }

    public function store(Request $request, PostType $type)
    {
        $entityId = intval($request->input('entity_id', 0));
        $action = $entityId > 0 ? EntityAction::Update : EntityAction::Create;
        $entityClass = $type->type();
        $entity = $action == EntityAction::Create ? new $entityClass : call_user_func([$entityClass, 'find'], $entityId);
        $entity->type = $entityClass;
        Action::Apply(PostBeforeStoreAction::class, $entity);
        $errors = [];

        if($type->require(PostFeature::Title) && empty(only_filled($request->input('title',[]))))
        { $errors[] = __('Title is Required and not Filled'); }

        if($type->require(PostFeature::Content) && empty(only_filled($request->input('content', []))))
        { $errors[] = __('Content is Required and Empty'); }

        if($type->require(PostFeature::MetaTitle) && empty(only_filled($request->input('meta_title', []))))
        { $errors[] = __('Meta Title is Required and not Filled'); }

        if($type->require(PostFeature::MetaDescription) && empty(only_filled($request->input('meta_description', []))))
        { $errors[] = __('Meta Description is Required and not Filled'); }

        if($type->require(PostFeature::MetaKeywords) && empty(only_filled($request->input('meta_keywords', []))))
        { $errors[] = __('Meta Keywords is Required and not Filled'); }

        if($type->require(PostFeature::Excerpt) && empty(only_filled($request->input('excerpt', []))))
        { $errors[] = __('Excerpt is Required and not Filled'); }

        if($type->require(PostFeature::Thumbnail) && !$request->file('thumbnail') && empty($entity->thumbnail_file))
        { $errors[] = __('Thumbnail is Required'); }

        if($type->require(PostFeature::Category) && count($request->input('categories', [])) == 0)
        { $errors[] = __('Please Select Category'); }

        if($type->require(PostFeature::Status) && $request->input('status', 0) == 0)
        { $errors[] = __('Please Select Status'); }

        if($type->require(PostFeature::Icon) && empty($request->input('icon', '')))
        { $errors[] = __('Icon is Required and not Filled'); }


        /******** > Handle Errors */
        if(count($errors) > 0)
        {
            $formattedErrors = [];
            foreach($errors as $error)
            { $formattedErrors[] = '<li>'.$error.'</li>'; }
            $formattedErrors = '<ul>'.implode("\n", $formattedErrors).'</ul>';
            $message = '<b>'.__('Whoops! Something went wrong').'</b><hr />'.$formattedErrors;
            Feedback::error($message);
            Feedback::flash();
            return back();
        }

        /******** > Filling Entity Data */

        //Slug
        if($entity->slug == null || ($request->filled('slug') && $request->slug != $entity->slug))
        {
            $requestedSlug = $request->input('slug', null);
            if($requestedSlug == null)
            {
                $titles = fill_locales(only_filled($request->title), $type->slug());
                $requestedSlug = array_shift($titles);
            }
            $requestedSlug = str($requestedSlug)->slug();
            $queryBuilder = call_user_func([$entityClass, 'query']);
            $generatedSlug = $requestedSlug;
            $counter = 0;
            while($queryBuilder->where('slug', $generatedSlug)->count() > 0)
            {
                $counter++;
                $generatedSlug = $requestedSlug.'-'.$counter;
                $queryBuilder = call_user_func([$entityClass, 'query']);
            }

            $entity->slug = $generatedSlug;
        }


        //Title
        if($type->supports(PostFeature::Title))
        { $entity->title = fill_locales(only_filled($request->input('title', []))); }

        //Content
        if($type->supports(PostFeature::Content))
        { $entity->content = fill_locales(only_filled($request->input('content', []))); }

        //Excerpt
        if($type->supports(PostFeature::Excerpt))
        { $entity->custom_excerpt = fill_locales(only_filled($request->input('excerpt', []))); }

        //Icon
        if($type->supports(PostFeature::Icon))
        { $entity->icon = $request->input('icon', ''); }
        //Meta Title
        if($type->supports(PostFeature::MetaTitle))
        { $entity->meta_title = fill_locales(only_filled($request->input('meta_title', []))); }

        //Meta Keywords
        if($type->supports(PostFeature::MetaKeywords))
        {
            $metaKeywords = fill_locales(only_filled($request->input('meta_keywords', [])), []);
            $tempKeywords = [];
            foreach($metaKeywords as $locale => $values)
            {
                $tempKeywords[$locale] = implode(',', $values);
            }
            $entity->meta_keywords = $tempKeywords;
        }

        //Meta Description
        if($type->supports(PostFeature::MetaDescription))
        { $entity->meta_description = fill_locales(only_filled($request->input('meta_description', []))); }

        //Thumbnail
        if($type->supports(PostFeature::Thumbnail) && ($thumbnailFile = $request->file('thumbnail')) != null)
        { $entity->thumbnail_file = $thumbnailFile->store($type->slug(), ['disk' => 'uploads']); }



        //Status
        if($type->supports(PostFeature::Status))
        { $entity->status = PostStatus::tryFrom($request->input('status', 0)); }

        //Template
        if($type->supports(PostFeature::Templates))
        { $entity->template = $request->input('template', null); }

        //Password
        if($type->supports(PostFeature::Password))
        { $entity->password = empty($request->input('password', null)) ? null : Hash::make($request->password); }

        //Author
        if($type->supports(PostFeature::Author))
        { $entity->author_id = intval($request->input('author_id', 0)); }

        //Comments Status
        if($type->supports(PostFeature::Comments))
        { $entity->comment_status = PostCommentStatus::tryFrom($request->input('comment_status', 0)); }


        $entity->save();


        //Show in Menu
        if($type->supports(PostFeature::ShowInMenu))
        { $entity->storeAttribute('show_in_menu', $request->input('show_in_menu', 0));}

        //Category
        if($type->supports(PostFeature::Category))
        {
            $categoriesIds = $request->input('categories', []);
            $entity->categories()->detach();
            foreach($categoriesIds as $categoryId)
            {
                $entity->categories()->save(Category::find($categoryId));
            }
        }

        //Tags
        if($type->supports(PostFeature::Tags))
        {
            $tagsIds = $request->input('tags', []);
            $tags = Tag::SyncRequired($tagsIds, $type->type());
            //dd($tags);
            $entity->tags()->detach();

            foreach($tags as $tag)
            {
                $entity->tags()->attach($tag);
            }

        }

        foreach($request->input('attributes', []) as $attributeKey => $attributeValue)
        {
            $entity->storeAttribute($attributeKey, $attributeValue);
        }
        Action::Apply(PostAfterStoreAction::class, $entity);
        Feedback::success(__('Record Saved Successfully'));
        Feedback::flash();

        return $action == EntityAction::Create ? to_route("dashboard.site.{$type->slug()}.index") :  back();
    }

    public function destroy($post, PostType $type)
    {
        $post = call_user_func([$type->type(), 'withoutGlobalScope'], 'type_constrain')->find($post);
        if($post != null)
        {
            $post->delete();
            Feedback::alert(__('Record Deleted Successfully'));
            Feedback::flash();
        }
        return back();
    }
}
