<?php

namespace Cascade\Site\Http\Controllers\Dashboard;

use Cascade\Site\DTO\WidgetArea;
use Cascade\Site\Services\Widgets;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Cascade\Site\Entities\WidgetOption;

class WidgetsController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        return view('site::dashboard.widgets.index', [
            'areas' => WidgetArea::all(),
            'widgets' => Widgets::all()
        ]);
    }

    public function designer(Request $request)
    {
        $options = null;
        if($request->input('entity_id', 0) == 0)
        {
            $options = $this->create($request->widget_id, $request->area_id, $request->order ?? 0);
        }
        else { $options = WidgetOption::find($request->entity_id); }
        $widget = Widgets::find($request->widget_id);
        $widgetClass = $widget->class;
        $widgetObject = new $widgetClass;
        $widgetObject->designer($options->options);
        return view('site::dashboard.widgets.designer', ['data' => $widget, 'widget' => $widgetObject, 'options' => $options]);
    }

    public function create(String $widget, String $location, $order)
    {
        $options = new WidgetOption();
        $options->options = [];
        $options->order = $order;
        $options->widget_id = $widget;
        $options->area_id = $location;
        $options->save();
        return $options;
    }

    public function move(Request $request, $instance, $location)
    {
        $options = WidgetOption::find($instance);
        if($options)
        {
            $options->area_id = $location;
            if($request->input('order', -1) > -1)
            {
                $options->order = $request->order;
            }
            $options->save();
        }
        return '';
    }

    public function delete($instance)
    {
        $options = WidgetOption::find($instance);
        if($options)
        {
            $options->delete();
        }
        return ['status' => true];
    }

    public function storeDesigner(Request $request)
    {
        $newOptions = $request->except(['widget', '_token']);
        foreach($request->allFiles() as $key => $file)
        {
            $newOptions[$key] = $file->store('widgets', ['disk' => 'uploads']);
        }
        $options = WidgetOption::find($request->options_id);
        $widget = Widgets::find($request->widget );
        $widgetClass = $widget->class;
        $widgetObject = new $widgetClass;
        $modifiedOptions = $widgetObject->handle($options->options, $newOptions);
        $options->options = $modifiedOptions;
        $options->save();
        return $modifiedOptions;
    }
}
