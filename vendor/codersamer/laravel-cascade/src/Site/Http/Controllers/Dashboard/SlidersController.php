<?php

namespace Cascade\Site\Http\Controllers\Dashboard;

use Cascade\Site\Entities\SlideButton;
use Cascade\Site\Entities\Slider;
use Cascade\Site\Entities\SliderSlide;
use Cascade\Site\Tables\SlidersTable;
use Cascade\System\Services\Feedback;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Storage;

class SlidersController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index(SlidersTable $table)
    {
        if(!Gate::any(['view sliders', 'create sliders']))
        {
            return response(__('You are not authorized'), 403);
        }
        return view('site::dashboard.sliders.index', [
            'table' => $table
        ]);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        $entityId = intval($request->input('entity_id', 0));

        ($entityId > 0) ? Gate::authorize('edit sliders') : Gate::authorize('create sliders');

        $valid = true;

        if(!$request->filled('name'))
        {
            Feedback::getInstance()->addError(__('Name Cannot be Empty'));
            $valid = false;
        }

        if(!$valid)
        {
            Feedback::flash();
            return back();
        }


        $entity = $entityId > 0 ? Slider::find($entityId) : new Slider();

        $entity->name = $request->name;
        $entity->status = $request->status ?? 'draft';
        $entity->save();
        Feedback::getInstance()->addSuccess(__('Slider Saved Successfully, you can now customize it'));
        Feedback::flash();
        return redirect()->route('dashboard.site.sliders.index');
    }


    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit(Slider $slider)
    {
        Gate::authorize('edit sliders');
        return $this->index(new SlidersTable)->with(['slider' => $slider]);
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function customize(Slider $slider)
    {
        Gate::authorize('customize sliders');
        return view('site::dashboard.sliders.customize', [
            'slider' => $slider
        ]);
    }



    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function storeSlides(Request $request)
    {
        Gate::authorize('customize sliders');
        $slider = Slider::find($request->input('entity_id', 0));
        $slider->slides()->delete();

        foreach($request->input('slides', []) as $slideKey => $slide)
        {
            $titles = only_filled(isset($slide['title']) ? $slide['title'] : []);
            $subtitles = only_filled(isset($slide['subtitle']) ? $slide['subtitle'] : []);
            $descriptions = only_filled(isset($slide['description']) ? $slide['description'] : []);
            if(empty($titles) && empty($subtitles) && empty($descriptions)) { continue; }
            $titles = fill_locales($titles);
            $subtitles = fill_locales($subtitles);
            $descriptions = fill_locales($descriptions);
            $slideObject = new SliderSlide();
            $slideObject->slider_id = $slider->id;
            $slideObject->title = $titles;
            $slideObject->subtitle = $subtitles;
            $slideObject->description = $descriptions;
            $images = $request->file('slides.'.$slideKey.'.image');
            $imagesValues = [];
            //Upload Images

            if($images != null && count($images) > 0)
            { foreach($images as $locale => $image) { $imagesValues[$locale] =  $image->store('sliders', ['disk' => 'uploads']); } }
            //Restore Images

            if(isset($slide['default_image']) && is_array($slide['default_image']))
            {
                foreach($slide['default_image'] as $locale => $defaultImage)
                {
                    if(!isset($imagesValues[$locale]) && !empty($defaultImage)) { $imagesValues[$locale] = $defaultImage; }
                }
            }
            $slideObject->image = fill_locales($imagesValues);

            $slideObject->save();
            //Buttons
            if(!isset($slide['buttons'])) { continue; }
            foreach($slide['buttons'] as $buttonKey => $button)
            {
                $titles = only_filled(isset($button['title']) ? $button['title'] : []);
                if(empty($titles)) { continue; }
                $slideButton = new SlideButton();
                $slideButton->slide_id = $slideObject->id;
                $slideButton->title = fill_locales($titles);
                $slideButton->link = isset($button['link']) ? $button['link'] : '#';
                $slideButton->theme = isset($button['theme']) ? $button['theme'] : 'primary';
                $slideButton->style = isset($button['layout']) ? $button['layout'] : 'default';
                $slideButton->target = isset($button['target']) ? $button['target'] : '_self';
                $slideButton->save();
            }
        }
        Feedback::getInstance()->addSuccess(__('Slider Saved Successfully'));
        Feedback::flash();
        return redirect()->route('dashboard.site.sliders.index');

    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy(Slider $slider)
    {
        Gate::authorize('delete sliders');
        $slider->delete();
        Feedback::getInstance()->addSuccess(__('Slider Deleted Successfully'));
        Feedback::flash();
        return back();
    }
}
