<?php

namespace Cascade\Site\Http\Controllers\Dashboard;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Gate;
use Cascade\Site\Entities\ContactMessage;
use Cascade\Site\Tables\ContactMessagesTable;
use Cascade\System\Services\Feedback;

class ContactMessagesController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index(ContactMessagesTable $table)
    {
        Gate::authorize('view contact messages');
        return view('site::dashboard.contact_messages.index', [
            'table' => $table
        ]);
    }


    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show(ContactMessage $message)
    {
        Gate::authorize('view contact messages');
        $message->is_read = true;
        $message->save();
        return view('site::dashboard.contact_messages.show', [
            'message' => $message
        ]);
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy(ContactMessage $message)
    {
        Gate::authorize('delete contact messages');
        $message->delete();
        Feedback::getInstance()->addSuccess(__('Message Deleted Successfully'));
        Feedback::flash();
        return redirect()->route('dashboard.site.messages.index');
    }
}
