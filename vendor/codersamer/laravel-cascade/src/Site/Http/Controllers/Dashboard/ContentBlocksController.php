<?php

namespace Cascade\Site\Http\Controllers\Dashboard;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Gate;
use Cascade\Site\Entities\ContentBlock;
use Cascade\Site\Pages\ContentBlockEntryPage;
use Cascade\Site\Tables\ContentBlocksTable;
use Cascade\Site\Tabs\ContentBlocks\ChildTab;
use Cascade\System\Services\Feedback;

class ContentBlocksController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index($type)
    {
        Gate::authorize('view content blocks');
        $type = ContentBlock::GetType($type);
        
        $table = new ContentBlocksTable($type->GetType());
        return view('site::dashboard.blocks.index', [
            'table' => $table,
            'type' => $type,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create($type)
    {
        Gate::authorize('create content blocks');
        $type = is_object($type) ? $type : ContentBlock::GetType($type);
        foreach($type->GetChildren() as $child)
        { ContentBlockEntryPage::AddTab(new ChildTab($child->GetType())); }
        return view('site::dashboard.blocks.upsert',[
            'types' => ContentBlock::GetTypes(),
            'type' => $type,
            'tabs' => ContentBlockEntryPage::GetTabs(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        $valid = true;
        $blockId = $request->has('entity_id') ? intval($request->entity_id) : 0;

        ($blockId > 0) ? Gate::authorize('edit content blocks') : Gate::authorize('create content blocks');

        $block = $blockId > 0 ? ContentBlock::find($blockId) : new ContentBlock();
        $type = ContentBlock::GetType($request->type);
        $titles = $request->has('title') ?  only_filled($request->title) : null;
        $linkText = $request->has('link_text') ?  only_filled($request->link_text) : [];
        $subtitles = $request->has('subtitle') ?  only_filled($request->subtitle) : null;
        $contents = $request->has('content') ? only_filled($request->content) : null;

        if($type->Supports('title') && $type->GetField('title')->IsRequired() && count($titles) == 0)
        {
            Feedback::getInstance()->addError(__('Title Cannot be Empty'));
            $valid = false;
        }

        if($type->Supports('subtitle') && $type->GetField('subtitle')->IsRequired() && count($subtitles) == 0)
        {
            Feedback::getInstance()->addError(__('Subtitle Cannot be Empty'));
            $valid = false;
        }



        if($type->Supports('content') && $type->GetField('content')->IsRequired() && count($contents) == 0)
        {
            Feedback::getInstance()->addError(__('Content Cannot be Empty'));
            $valid = false;
        }

        if($type->Supports('icon') && $type->GetField('icon')->IsRequired() && !$request->filled('icon'))
        {
            Feedback::getInstance()->addError(__('Icon Cannot be Empty'));
            $valid = false;
        }

        if($type->Supports('link') && $type->GetField('link')->IsRequired() && !$request->filled('link'))
        {
            Feedback::getInstance()->addError(__('Link Cannot be Empty'));
            $valid = false;
        }

        if($type->Supports('image') && $type->GetField('image')->IsRequired() && !$request->file('image') && $blockId == 0)
        {
            Feedback::getInstance()->addError(__('Image Cannot be Empty'));
            $valid = false;
        }

        if($type->Supports('background_color') && $type->GetField('background_color')->IsRequired() && !$request->filled('background_color'))
        {
            Feedback::getInstance()->addError(__('Background Color Cannot be Empty'));
            $valid = false;
        }

        if($type->Supports('foreground_color') && $type->GetField('foreground_color')->IsRequired() && !$request->filled('foreground_color'))
        {
            Feedback::getInstance()->addError(__('Foreground Color Cannot be Empty'));
            $valid = false;
        }

        if($type->Supports('order') && $type->GetField('order')->IsRequired() && !$request->filled('order'))
        {
            Feedback::getInstance()->addError(__('Order Cannot be Empty'));
            $valid = false;
        }

        if($type->Supports('theme') && $type->GetField('theme')->IsRequired() && !$request->filled('theme'))
        {
            Feedback::getInstance()->addError(__('Theme Cannot be Empty'));
            $valid = false;
        }

        if($type->Supports('layout') && $type->GetField('layout')->IsRequired() && !$request->filled('layout'))
        {
            Feedback::getInstance()->addError(__('Layout Cannot be Empty'));
            $valid = false;
        }

        if($type->Supports('css_class') && $type->GetField('css_class')->IsRequired() && !$request->filled('css_class'))
        {
            Feedback::getInstance()->addError(__('CSS Class Cannot be Empty'));
            $valid = false;
        }

        if($type->Supports('css_id') && $type->GetField('css_id')->IsRequired() && !$request->filled('css_id'))
        {
            Feedback::getInstance()->addError(__('CSS ID Cannot be Empty'));
            $valid = false;
        }

        if(!$valid)
        {
            Feedback::flash();
            return back();
        }

        $titles = fill_locales($titles);
        $subtitles = fill_locales($subtitles);
        $contents = fill_locales($contents);

        $block->title = $titles;
        $block->subtitle = $subtitles;
        $block->content = $contents;
        /*
        */

        $block->block_type = $request->type;
        $block->link = $request->filled('link') ? $request->link : null;
        $block->link_text = fill_locales($linkText, '');
        $block->link_target = $request->input('link_target', '_self');
        $block->icon = $request->filled('icon') ? $request->icon : null;

        $block->background_color = $request->filled('background_color') ? $request->background_color : null;
        $block->foreground_color = $request->filled('foreground_color') ? $request->foreground_color : null;
        $block->order = $request->filled('order') ? $request->order : 1;
        $block->theme = $request->filled('theme') ? $request->theme : null;
        $block->layout = $request->filled('layout') ? $request->layout : null;
        $block->css_class = $request->filled('css_class') ? $request->css_class : null;
        $block->css_id = $request->filled('css_id') ? $request->css_id : null;


        $block->status = $request->status;
        if($request->file('image'))
        {
            $block->image_url = $request->file('image')->store('blocks', ['disk' => 'uploads']);
        }
        $block->save();

        if($request->has('children'))
        {
            foreach($request->input('children', []) as $childType => $childInfo)
            {
                $selectedIds = [];
                foreach($childInfo as $childIndex => $childInputs)
                {
                    $id = intval($childInputs['id']);
                    $entry = $id > 0 ? ContentBlock::find($id) : new ContentBlock();
                    $entry->parent_id = $block->id;
                    $entry->block_type = $childType;
                    $entry->title = $childInputs['title'] ?? '';
                    $entry->subtitle = $childInputs['subtitle'] ?? '';
                    $entry->icon = $childInputs['icon'] ?? '';
                    $entry->save();
                    $selectedIds[] = $entry->id;
                }
                //Delete Non Parsed
                $block->children()->where('block_type', $childType)->whereNotIn('id', $selectedIds)->delete();
            }
        }

        Feedback::getInstance()->addSuccess(__('Content Block Saved Successfully'));
        Feedback::flash();
        return back();
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($type, ContentBlock $block)
    {
        Gate::authorize('edit content blocks');
        $type = is_object($type) ? $type : ContentBlock::GetType($type);
        foreach($type->GetChildren() as $child)
        { 
            ContentBlockEntryPage::AddTab(new ChildTab($child->GetType())); 
        }
        return $this->create($type)->with([
            'block' => $block,
            'type' => $type,
            'tabs' => ContentBlockEntryPage::GetTabs(),
        ]);
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($type, ContentBlock $block)
    {
        Gate::authorize('delete content blocks');
        $block->delete();
        Feedback::getInstance()->addSuccess(__('Content Block Deleted Successfully'));
        Feedback::flash();
        return redirect()->route('dashboard.site.content_blocks.index', ['type' => $type]);
    }
}
