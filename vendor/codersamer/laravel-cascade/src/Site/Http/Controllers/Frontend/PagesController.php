<?php

namespace Cascade\Site\Http\Controllers\Frontend;

use Cascade\Site\Entities\Page;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Routing\Controller;
use Cascade\System\Services\System;

class PagesController extends Controller
{

    /**
     * Display Single Page
     *
     * @return Renderable
     */
    public function page(Page $page)
    {
        return System::FindView($page->template ?? 'site::frontend.pages.single', [
            'page' => $page,
        ]);
    }


}
