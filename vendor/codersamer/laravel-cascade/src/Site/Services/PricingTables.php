<?php

namespace Cascade\Site\Services;

use Cascade\Site\Abstraction\PricingTable;

class PricingTables
{
    protected static $builders = [];

    public static function Register(String|PricingTable $builder)
    {
        if(is_string($builder))
        {
            if(class_exists($builder)) { $builder = new $builder; }
        }
        if($builder instanceof PricingTable)
        {
            static::$builders[$builder->slug()] = $builder;
        }
    }

    public static function HasBuilders() : bool
    {
        return count(static::$builders) > 0;
    }

    /**
     * Undocumented function
     *
     * @return PricingTable[]
     */
    public static function Builders() : array
    {
        return static::$builders;
    }

    public static function FindBuilder($slug) : ?PricingTable
    {
        return static::$builders[$slug] ?? null;
    }

}