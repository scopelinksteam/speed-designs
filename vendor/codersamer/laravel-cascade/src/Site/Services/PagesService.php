<?php

namespace Cascade\Site\Services;

use Cascade\Dashboard\Entities\MenuItem;
use Cascade\Dashboard\Entities\Statistic;
use Cascade\Dashboard\Services\DashboardBuilder;
use Cascade\Site\Entities\Page;
use Cascade\Site\Enums\PostFeature;
use Cascade\System\Abstraction\Service;
use Cascade\System\Enums\BootstrapColors;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Support\Facades\Route;

class PagesService extends Service
{
    public function name(): string
    {
        return __('Pages System');
    }

    public function description(): string
    {
        return __('Add Pages Functionality to Website');
    }

    public function identifier(): string
    {
        return 'pages-service';
    }

    public function services(Application $app)
    {
        return [];
    }

    public function __construct()
    {
        PostType::new(Page::class, 'pages')
            ->title(__('Pages'))
            ->description(__('Manage your own Site Pages'))
            ->icon('file')
            ->features( PostFeature::All )
            ->showInMenu(false);
    }

    public function register(Application $app)
    {
        Route::middleware('web')
            ->namespace('Cascade\Site\Http\Controllers')
            ->group(module_path('Site', '/Routes/pages.php'));
    }

    public function boot(Application $app)
    {
        DashboardBuilder::AddStatistic(
            Statistic::make(__('Pages'), Page::count())->icon('file')->color(BootstrapColors::Purple)
        );

        DashboardBuilder::MainMenu()->AddItem(
            MenuItem::make(__('Pages'), 'site')->icon('file')->route('dashboard.site.pages.index')->permission('view pages'),
        );
    }

}
