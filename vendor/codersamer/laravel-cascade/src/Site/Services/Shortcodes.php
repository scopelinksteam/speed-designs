<?php

namespace Cascade\Site\Services;


class Shortcodes
{
    protected static $compiler = null;

    public static function compiler() : ShortcodeCompiler
    { static::$compiler = static::$compiler == null ? new ShortcodeCompiler() : static::$compiler; return static::$compiler; }

    public static function Register(String $tag, String $className)
    { static::compiler()->register($tag, $className); }

    public static function Remove(String $tag)
    { static::compiler()->remove($tag); }

    public static function Compile(String $content)
    { return static::compiler()->compile($content); }

    public static function Strip(String $content)
    { return static::compiler()->strip($content); }

}
