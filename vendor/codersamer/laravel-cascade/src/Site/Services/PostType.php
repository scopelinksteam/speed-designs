<?php

namespace Cascade\Site\Services;

use Cascade\Site\Entities\Post;
use Cascade\Site\Enums\PostFeature;
use Cascade\Site\Http\Controllers\Dashboard\PostsController;
use Cascade\System\Entities\Category;
use Cascade\System\Entities\Tag;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Route;
use Javoscript\MacroableModels\Facades\MacroableModels;
use Cascade\Site\Http\Controllers\API\PostsController as ApiPostsController;

class PostType
{
    protected static $types = [];


    protected $type = null;

    protected String $slug = '';

    protected String $title = '';

    protected String $description = '';

    protected bool $showInMenu = false;

    protected String $icon = 'document';

    protected bool $hasCrud = true;

    protected array $features = [PostFeature::All];

    protected array $requires = [];

    protected array $templates = [];

    protected ?String $previewRoute = null;

    protected ?String $routesParam = null;

    protected static array $routesRegistered = [];

    /******************************************
    * Static Interface
    ******************************************/
    public static function types() : Collection
    { return collect(static::$types); }

    public static function new(String $className, ?String $slug = null) : self
    {
        $builder = new static($className, $slug);
        static::$types[$className] = $builder;
        return $builder;
    }

    public static function for(String $className) : self|null
    {
        $item = static::types()->first(function(PostType $postType) use($className){
            return $postType->type() == $className;
        });
        return $item instanceof PostType ? $item : null;
    }

    public static function of(String $className) : self|null
    {
        return static::for($className);
    }
    
    public static function routes() : void
    {
            foreach(PostType::types() as $postType)
            {
                if(!$postType->hasCrud()) { continue; }
                //Already Registered
                if(in_array($postType->slug(), static::$routesRegistered)) { continue; }
                Route::prefix($postType->slug())->as($postType->slug().'.')->group(function() use($postType)
                {
                    Route::get('/', [PostsController::class, 'index'])->name('index')->defaults('type', $postType);
                    Route::post('/', [PostsController::class, 'store'])->name('store')->defaults('type', $postType);
                    Route::get('/create', [PostsController::class, 'create'])->name('create')->defaults('type', $postType);
                    Route::get('/edit/{post}', [PostsController::class, 'edit'])->name('edit')->defaults('type', $postType);
                    Route::get('/delete/{post}', [PostsController::class, 'destroy'])->name('destroy')->defaults('type', $postType);
                    if($postType->supports(PostFeature::Category))
                    {
                        Route::prefix('categories')->name('categories.')->group(function() use($postType){
                            Route::categories($postType->type());
                        });
                        MacroableModels::addMacro(Category::class, str_replace('-', '_',$postType->slug()), function() use ($postType){
                            return $this->morphedByMany($postType->type(), 'categorizable', 'entities_categories');
                        });
                    }
    
                    if($postType->supports(PostFeature::Tags))
                    {
                        Route::prefix('tags')->name('tags.')->group(function() use($postType){
                            Route::tags($postType->type());
                        });
                        MacroableModels::addMacro(Tag::class, str_replace('-', '_',$postType->slug()), function() use ($postType){
                            return $this->morphedByMany($postType->type(), 'taggable', 'entities_tags');
                        });
                    }
                });
            }
    }
    
    public static function routesAPI() : void
    {
            foreach(PostType::types() as $postType)
            {
                if(!$postType->hasCrud()) { continue; }
                //Already Registered
                if(in_array($postType->slug(), static::$routesRegistered)) { continue; }
                Route::prefix($postType->slug())->as($postType->slug().'.')->group(function() use($postType)
                {
                    Route::get('/', [ApiPostsController::class, 'list'])->name('list')->defaults('type', $postType);
                    Route::post('/', [ApiPostsController::class, 'store'])->name('store')->defaults('type', $postType);
                    Route::get('/create', [ApiPostsController::class, 'create'])->name('create')->defaults('type', $postType);
                    Route::get('/edit/{post}', [ApiPostsController::class, 'edit'])->name('edit')->defaults('type', $postType);
                    Route::get('/delete/{post}', [ApiPostsController::class, 'destroy'])->name('destroy')->defaults('type', $postType);
                    if($postType->supports(PostFeature::Category))
                    {
                        Route::prefix('categories')->name('categories.')->group(function() use($postType){
                            Route::categories($postType->type());
                        });
                        MacroableModels::addMacro(Category::class, str_replace('-', '_',$postType->slug()), function() use ($postType){
                            return $this->morphedByMany($postType->type(), 'categorizable', 'entities_categories');
                        });
                    }
    
                    if($postType->supports(PostFeature::Tags))
                    {
                        Route::prefix('tags')->name('tags.')->group(function() use($postType){
                            Route::tags($postType->type());
                        });
                        MacroableModels::addMacro(Tag::class, str_replace('-', '_',$postType->slug()), function() use ($postType){
                            return $this->morphedByMany($postType->type(), 'taggable', 'entities_tags');
                        });
                    }
                });
            }
    }


    /******************************************
    * Object Interface
    ******************************************/

    public function __construct(String $className, ?String $slug = null)
    {
        $this->type = $className;
        $this->slug = $slug == null ?  \str(class_basename($className))->slug() : $slug;
    }

    public function registerRoutes()
    {
        if(!$this->hasCrud()) { return $this; }
        $postType = $this;
        if(in_array($postType->slug(), static::$routesRegistered)) { return $this; }
        static::$routesRegistered[] = $this->slug();

        Route::dashboard(function() use($postType) {
            
            Route::prefix($postType->slug())->as($this->slug().'.')->group(function() use($postType)
            {
                Route::get('/', function(PostType $type){
                    dd($type->type());
                })->name('index')->defaults('type', $postType);
                //Route::get('/', [PostsController::class, 'index'])->name('index')->defaults('type', $postType);
                Route::post('/', [PostsController::class, 'store'])->name('store')->defaults('type', $postType);
                Route::get('/create', [PostsController::class, 'create'])->name('create')->defaults('type', $postType);
                Route::get('/edit/{post}', [PostsController::class, 'edit'])->name('edit')->defaults('type', $postType);
                Route::get('/delete/{post}', [PostsController::class, 'destroy'])->name('destroy')->defaults('type', $postType);

                //Category
                if($postType->supports(PostFeature::Category))
                {
                    Route::prefix('categories')->name('categories.')->group(function() use($postType){
                        Route::categories($postType->type());
                    });
                    MacroableModels::addMacro(Category::class, str_replace('-', '_',$postType->slug()), function() use ($postType){
                        return $this->morphedByMany($postType->type(), 'categorizable', 'entities_categories');
                    });
                }

                //Tags
                if($postType->supports(PostFeature::Tags))
                {
                    Route::prefix('tags')->name('tags.')->group(function() use($postType){
                        Route::tags($postType->type());
                    });
                    MacroableModels::addMacro(Tag::class, str_replace('-', '_',$postType->slug()), function() use ($postType){
                        return $this->morphedByMany($postType->type(), 'taggable', 'entities_tags');
                    });
                }
            });
        }, 'site');
        return $this;
    }


    public function registerAPIRoutes()
    {
        if(!$this->hasCrud()) { return $this; }
        $postType = $this;
        if(in_array($postType->slug(), static::$routesRegistered)) { return $this; }
        static::$routesRegistered[] = $this->slug();

        Route::Api(function() use($postType) {
            
            Route::prefix($postType->slug())->as($this->slug().'.')->group(function() use($postType)
            {
                Route::get('/', [ApiPostsController::class, 'list'])->name('list')->defaults('type', $postType);
                Route::post('/', [ApiPostsController::class, 'store'])->name('store')->defaults('type', $postType);
                Route::get('/create', [ApiPostsController::class, 'create'])->name('create')->defaults('type', $postType);
                Route::get('/edit/{post}', [ApiPostsController::class, 'edit'])->name('edit')->defaults('type', $postType);
                Route::get('/delete/{post}', [ApiPostsController::class, 'destroy'])->name('destroy')->defaults('type', $postType);

                //Category
                if($postType->supports(PostFeature::Category))
                {
                    Route::prefix('categories')->name('categories.')->group(function() use($postType){
                        Route::categories($postType->type());
                    });
                    MacroableModels::addMacro(Category::class, str_replace('-', '_',$postType->slug()), function() use ($postType){
                        return $this->morphedByMany($postType->type(), 'categorizable', 'entities_categories');
                    });
                }

                //Tags
                if($postType->supports(PostFeature::Tags))
                {
                    Route::prefix('tags')->name('tags.')->group(function() use($postType){
                        Route::tags($postType->type());
                    });
                    MacroableModels::addMacro(Tag::class, str_replace('-', '_',$postType->slug()), function() use ($postType){
                        return $this->morphedByMany($postType->type(), 'taggable', 'entities_tags');
                    });
                }
            });
        }, 'site');
        return $this;
    }
    /**
     * Set or Get Slug
     *
     * @param String|null $slug
     * @return String|self
     */
    public function slug(?String $slug = null) : String|self
    {
        if($slug != null)
        {
            $this->slug = $slug;
            return $this;
        }
        return $this->slug;
    }

    /**
     * Set or Get Templates
     *
     * @param array|null $templates
     * @return array|self
     *
     */
    public function templates(array|null $templates = null)
    {
        if($templates == null) { return $this->templates; }
        $this->templates = $templates;
        return $this;
    }

    /**
     * Register new Post Type Template
     *
     * @param String $view
     * @param String $name
     * @return self
     *
     */
    public function addTemplate(String $view, String $name)
    {
        $this->templates[$view] = $name;
        return $this;
    }

    /**
     * Set or Get Preview Route
     *
     * @param ?String $route
     * @return null|String|self
     */
    public function previewRoute(?String $route = null, ?String $routeParamName = null) : null|String|self
    {
        if($route != null) {
            $this->previewRoute = $route;
            $routeParamName = $routeParamName != null ? $routeParamName : strtolower(class_basename($this->type));
            $this->routesParam = $routeParamName;
            return $this;
        }
        return $this->previewRoute;
    }

    /**
     * Check if Can Preview Post
     *
     * @return bool
     */
    public function canPreview() : bool
    {
        return $this->previewRoute != null;
    }

    /**
     * Get Preview URL
     *
     * @param Post $post
     * @return ?String
     */
    public function previewUrl(Post $post)
    {
        if(!$this->canPreview()) { return null; }
        //dd($post);
        return route($this->previewRoute, [$this->routesParam => $post]);
    }


    /**
     * Set or Get Type
     *
     * @param String|null $className
     * @return String|self
     */
    public function type(?String $className = null) : String|self
    {
        if($className != null)
        {
            $this->type = $className;
            return $this;
        }
        return $this->type;
    }


    /**
     * Get or Set title
     *
     * @param String|null $title
     * @return String|self
     */
    public function title(?String $title = null) : String|self
    {
        if($title != null)
        {
            $this->title = $title;
            return $this;
        }
        return $this->title;
    }


    /**
     * Get or Set Description
     *
     * @param String|null $description
     * @return String|self
     */
    public function description(?String $description = null) : String|self
    {
        if($description != null)
        {
            $this->description = $description;
            return $this;
        }
        return $this->description;
    }


    /**
     * Get or Set Icon
     *
     * @param String|null $icon
     * @return String|self
     */
    public function icon(?String $icon = null) : String|self
    {
        if($icon != null)
        {
            $this->icon = $icon;
            return $this;
        }
        return $this->icon;
    }


    /**
     * Set Post Type Visible in Menu or Not
     *
     * @param boolean|null $showInMenu
     * @return boolean|self
     */
    public function showInMenu(?bool $showInMenu = null) : bool|self
    {
        if($showInMenu !== null)
        {
            $this->showInMenu = $showInMenu;
            return $this;
        }
        return $this->showInMenu;
    }

    /**
     * Set Post Type Has Crud or Not
     *
     * @param boolean|null $hasCrud
     * @return boolean|self
     */
    public function hasCrud(?bool $hasCrud = null) : bool|self
    {
        if($hasCrud !== null)
        {
            $this->hasCrud = $hasCrud;
            return $this;
        }
        return $this->hasCrud;
    }

    /**
     * Set Post Features
     *
     * @return self|array
     */
    public function features(PostFeature ...$features) : self|array
    {
        if($features == null || count($features) == 0) { return $this->features; }
        if(count($this->features) == 1 && $this->features[0] == PostFeature::All)
        { $this->features = []; }

        foreach($features as $feature)
        {
            if($feature instanceof PostFeature) { $this->features[] = $feature; }
        }

        if($this->supports(PostFeature::Category))
        {
            Category::register($this->type());
        }

        if($this->supports(PostFeature::Tags))
        {
            Tag::register($this->type());
        }

        return $this;
    }

    /**
     * Set Post Features
     *
     * @return self|array
     */
    public function addFeature(PostFeature ...$features) : self|array
    {
        if(count($this->features) == 1 && $this->features[0] == PostFeature::All)
        { $this->features = [PostFeature::All]; }

        foreach($features as $feature)
        {
            if($feature instanceof PostFeature) { $this->features[] = $feature; }
        }
        return $this;
    }

    /**
     * Set Post Features
     *
     * @return self|array
     */
    public function removeFeature(PostFeature ...$features) : self|array
    {
        if(count($features) == 1 && $features[0] == PostFeature::All)
        { $this->features = []; }

        $tempFeatures = [];
        foreach($this->features as $feature)
        {
            if(in_array($feature, $features)) { continue; }
            $tempFeatures[] = $feature;
        }
        $this->features = $tempFeatures;
        return $this;
    }
    /**
     * Set Post Features
     *
     * @return self|array
     */
    public function requires(PostFeature ...$features) : self|array
    {
        if($features == null || count($features) == 0) { return $this->requires; }
        if(count($this->requires) == 1 && $this->features[0] == PostFeature::All)
        { $this->features = []; }

        foreach($features as $feature)
        {
            if($feature instanceof PostFeature) { $this->requires[] = $feature; }

        }
        return $this;
    }

    /**
     * Check if Feature is Required
     *
     * @param PostFeature|String $feature
     *
     * @return void
     */
    public function require(PostFeature|String $feature)
    {
        if(is_string($feature)) { $feature = PostFeature::from($feature); }
        return in_array($feature, $this->requires) || in_array(PostFeature::All, $this->requires);
    }

    /**
     * Check if Post Type Supports Selected Feature
     *
     * @param PostFeature $feature
     *
     * @return boolean
     */
    public function supports(PostFeature|String $feature)
    {
        if(is_string($feature)) { $feature = PostFeature::from($feature); }
        return in_array($feature, $this->features) || in_array(PostFeature::All, $this->features);
    }

    /**
     * Check if Post Type Supports Selected Feature
     *
     * @param PostFeature $feature
     *
     * @return boolean
     */
    public function supportsAny(PostFeature|String ...$features)
    {
        foreach($features as $feature)
        {
            if(is_string($feature)) { $feature = PostFeature::from($feature); }
            if(in_array($feature, $this->features) || in_array(PostFeature::All, $this->features))
            { return true; }
        }

        return false;
    }

    /**
     * Get Route for Selected Actions
     *
     * @param String $action
     *
     * @return String
     */
    public function route(String $action)
    {
        return route('dashboard.site.'.$this->slug.'.'.$action);
    }

}
