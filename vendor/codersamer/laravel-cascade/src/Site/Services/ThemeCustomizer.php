<?php

namespace Cascade\Site\Services;

use Cascade\Settings\Services\Settings;
use Cascade\Site\Settings\ThemeContentSettingPage;
use Closure;

class ThemeCustomizer extends Settings
{

    protected static $registeredSettings = [];

    public static $moduleContext = '';

    protected static $pageContext = '';

    protected static $groupContext = '';

    protected static $context = [];

    protected static $callbacks = [];

    protected static $injectableCallbacks = [];

    public static function theme($themeName, $themeTitle, Closure $callback)
    {
        return static::module($themeName.'-theme', $themeTitle, $callback);
    }

    public static function GetContexts()
    {
        static::module('theme', __('Core Options'), function(){
            ThemeCustomizer::page(__('Custom Injections'), 'custom_injection', function(){
                ThemeCustomizer::group(__('CSS & Javascript'), function(){
                    ThemeCustomizer::textarea(__('Custom CSS'), 'css');
                    ThemeCustomizer::textarea(__('Custom Javascript'), 'js');
                });
            });
        });

        static::module('contents', __('Content'), function(){
            static::view(__('Static Blocks'), 'static-blocks', ThemeContentSettingPage::class);
        });

        return parent::GetContexts();
    }
}
