<?php

namespace Cascade\Site\Services;

use Nwidart\Modules\Support\Stub as SupportStub;

class Stub extends SupportStub
{

    /**
     * Get stub path.
     *
     * @return string
     */
    public function getPath()
    {
        $path = static::getBasePath() . $this->path;

        return file_exists($path) ? $path : __DIR__ . '/../' . $this->path;
    }


    public static function getBasePath()
    {
        return module_path('Site', 'Stubs').'/';
    }
}
