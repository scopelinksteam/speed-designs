<?php

namespace Cascade\Site\Services;

use Cascade\Site\Abstraction\MenuItemBuilder;
use stdClass;

class MenuBuilder
{
    protected static $status = true;

    protected static $builders = [];

    public static function Enable() { static::$status = true; }
    public static function Disable() { static::$status = false; }

    public static function Enabled() { return static::$status; }
    public static function Disabled() { return !static::$status; }

    public static function AddBuilder(String $id, String $title, MenuItemBuilder $builder)
    {
        $item = new stdClass;
        $item->id = $id;
        $item->title = $title;
        $item->instance = $builder;
        static::$builders[$id] = $item;
    }

    public static function Builders() { return static::$builders; }

    public static function GetBuilder($id) { return static::$builders[$id] ?? null; }
}
