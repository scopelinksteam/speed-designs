<?php

namespace Cascade\Site\Services;

use Cascade\Site\Abstraction\Widget;
use Illuminate\Support\Collection;
use stdClass;

class Widgets
{

    protected static array $widgets = [];

    /**
     * Register New Widget to System
     *
     * @param String|Widget $class
     * @param String $id
     * @param String $name
     * @return void
     */
    public static function register(String|Widget $class, String $id, String $name, ?String $description = null) : void
    {
        $class = is_string($class) ? $class : get_class($class);
        $widget = new stdClass;
        $widget->class = $class;
        $widget->id = $id;
        $widget->name = $name;
        $widget->description = $description;
        static::$widgets[$id] = $widget;
    }

    /**
     * Get a Registered Widget with ID or Class
     *
     * @param String $idOrClass
     * @return stdClass|null
     */
    public static function find(String $idOrClass)
    {
        return collect(static::$widgets)->first(function($widget) use($idOrClass) {
            return $widget->id == $idOrClass || $widget->class == $idOrClass;
        });
    }

    /**
     * Get All Registered Widgets
     *
     * @return Collection
     */
    public static function all() : Collection
    {
        return collect(static::$widgets);
    }


}
