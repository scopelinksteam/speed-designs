<?php

namespace Cascade\Site\Services;


class Site
{
    protected static array $styles = [];

    protected static array $scripts = [];

    protected static ?String $title = null;

    public static function AddStyle(String|array $stylePath, ?String $key = null) : void
    {
        if(is_array($stylePath))
        {
            foreach($stylePath as $index => $path)
            {
                static::$styles[count(static::$styles)] = $path;
            }
            return;
        }
        static::$styles[$key ?? count(static::$styles)] = $stylePath;
    }

    public static function Styles() : array { return static::$styles; }

    public static function CustomCSS() : ?String { return get_setting('theme::custom_injection.css', null); }

    public static function AddScript(String $scriptPath, ?String $key = null) : void
    {
        static::$scripts[$key ?? count(static::$scripts)] = $scriptPath;
    }

    public static function Scripts() : array { return static::$scripts; }

    public static function CustomJS() : ?String { return get_setting('theme::custom_injection.js', null); }

    public static function Title(?String $title = null) : ?String
    {
        if($title != null) { static::$title = $title; }
        return static::$title;
    }
}
