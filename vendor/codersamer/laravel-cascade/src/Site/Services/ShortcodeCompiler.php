<?php

namespace Cascade\Site\Services;

use Cascade\Site\Abstraction\Shortcode;
use Closure;

class ShortcodeCompiler
{

    protected $shortcodes = [];

    public function register(String $tag, String $className)
    { $this->shortcodes[$tag] = $className; }

    public function remove(String $tag)
    { if(isset($this->shortcodes[$tag])) { unset($this->shortcodes[$tag]); } }

    public function compile(String $content) : String
    {
        $tokens = token_get_all($content);
        $results = '';
        foreach($tokens as $token)
        { $results .= is_array($token) ? $this->compileToken($token) : $token; }
        return $results;
    }

    public function strip(String $content) : String
    {
        if (empty($this->registered)) {
            return $content;
        }
        $pattern = $this->getSearchPattern();

        return preg_replace_callback("/{$pattern}/s", function($m) {
            if ($m[1] == '[' && $m[6] == ']') {
                return substr($m[0], 1, -1);
            }

            return $m[1] . $m[6];
        }, $content);
    }

    protected function compileToken($token)
    {
        list($type, $content) = $token;
        if($type != T_INLINE_HTML) { return $content; }
        $pattern = $this->getSearchPattern();
        return preg_replace_callback("/{$pattern}/s", function($matches){
            $attributes= $this->extractAttributes($matches[3]);
            $response = $this->runShortcode($matches[2], $attributes, $matches[5]);
            if($response instanceof \Illuminate\View\View)
            { $response = $response->with([
                'tag' =>$matches[2],
                'attributes' => $attributes,
                'content' => $matches[5]
            ])->toHtml(); }
            return $response;
        }, $content);
    }

    protected function runShortcode(String $tag, array $attributes, $content)
    {
        if(!isset($this->shortcodes[$tag])) { return $content; }
        $className = $this->shortcodes[$tag];
        if(!class_exists($className)) { return $content; }
        $handler = new $className($tag, $attributes, $content);
        if($handler instanceof Shortcode)
        {
            return $handler->handle();
        }
        return $content;
    }

    protected function extractAttributes($text)
    {
        $text = htmlspecialchars_decode($text, ENT_QUOTES);

        $attributes = [];
        // attributes pattern
        $pattern = '/(\w+)\s*=\s*"([^"]*)"(?:\s|$)|(\w+)\s*=\s*\'([^\']*)\'(?:\s|$)|(\w+)\s*=\s*([^\s\'"]+)(?:\s|$)|"([^"]*)"(?:\s|$)|(\S+)(?:\s|$)/';
        // Match
        if (preg_match_all($pattern, preg_replace('/[\x{00a0}\x{200b}]+/u', " ", $text), $match, PREG_SET_ORDER)) {
            foreach ($match as $m) {
                if (!empty($m[1])) {
                    $attributes[strtolower($m[1])] = stripcslashes($m[2]);
                } elseif (!empty($m[3])) {
                    $attributes[strtolower($m[3])] = stripcslashes($m[4]);
                } elseif (!empty($m[5])) {
                    $attributes[strtolower($m[5])] = stripcslashes($m[6]);
                } elseif (isset($m[7]) && strlen($m[7])) {
                    $attributes[] = stripcslashes($m[7]);
                } elseif (isset($m[8])) {
                    $attributes[] = stripcslashes($m[8]);
                }
            }
        } else {
            $attributes = ltrim($text);
        }

        // return attributes
        return is_array($attributes) ? $attributes : [$attributes];
    }

    protected function getSearchPattern()
    {
        $tags = join('|', array_map('preg_quote', array_keys($this->shortcodes)));
        return "\\[(\\[?)($tags)(?![\\w-])([^\\]\\/]*(?:\\/(?!\\])[^\\]\\/]*)*?)(?:(\\/)\\]|\\](?:([^\\[]*+(?:\\[(?!\\/\\2\\])[^\\[]*+)*+)\\[\\/\\2\\])?)(\\]?)";
    }
}
