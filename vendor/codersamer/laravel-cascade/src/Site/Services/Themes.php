<?php

namespace Cascade\Site\Services;

use Cascade\Site\Actions\ActivateThemeAction;
use Cascade\Site\Actions\ActivateThemeReadyAction;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\File;
use Cascade\Site\Entities\Theme;
use Cascade\Site\Filters\ActiveThemeFilter;
use Cascade\Site\Filters\ActiveThemeNameFilter;
use Cascade\Site\Filters\AvailableThemesFilter;
use Cascade\Site\Filters\IsThemeEnabledFilter;
use Cascade\Site\Filters\ThemesPathFilter;
use Cascade\Site\Providers\ThemeProvider;
use Cascade\System\Services\Action;
use Cascade\System\Services\Feedback;
use Cascade\System\Services\Filter;
use Cascade\System\Services\Modules;
use Cascade\System\Services\System;
use Cascade\System\Services\Unzip;
use Cascade\System\Traits\HasActions;
use Cascade\System\Traits\HasFilters;
use Cascade\Tenancy\Services\TenancyService;
use Directory;
use Nwidart\Modules\Facades\Module;
use stdClass;
use Symfony\Component\Finder\Finder;

class Themes
{

    use  HasActions;

    const CURRENT_THEME_SETTING_KEY = 'active_theme';

    protected static $themes = [];


    public static function GetBasePath()
    {
        return Filter::Apply(ThemesPathFilter::class, base_path('Themes'));
    }

    public static function GetPath($name, $appends = '')
    {
        return rtrim(static::GetBasePath().'/'.$name.'/'.$appends, '/');
    }

    public static function GetActiveThemeName()
    {
        return Filter::Apply(ActiveThemeNameFilter::class,get_setting('site::themes.active'));// System::IsMultiTenant() ? TenancyService::CurrentTenant()->CurrentThemeName() : ;
    }

    public static function IsEnabled($themeName)
    {
        return Filter::Apply(IsThemeEnabledFilter::class, strtolower($themeName) == strtolower(static::GetActiveThemeName()), $themeName);
    }

    /**
     * Get Themes
     *
     * @return Collection
     */
    public static function GetThemes($customOnly = null)
    {
        //Enable Caching
        if(count(self::$themes) > 0) { return self::$themes; }

        $finder = new Finder();
        $modulesFiles = $finder->in(static::GetBasePath())->depth(1)->name('theme.json')->files();
        $modules = [];
        foreach($modulesFiles as $file)
        {
            $object = json_decode($file->getContents());
            $isProtected = isset($object->protected) ? $object->protected : false;
            if($customOnly === null)
            { $modules[] = Theme::LoadFrom($file);}
            if($customOnly === false && $isProtected)
            { $modules[] = Theme::LoadFrom($file);}
            if($customOnly === true && !$isProtected)
            { $modules[] = Theme::LoadFrom($file);}
        }

        self::$themes = collect($modules);
        return collect($modules);
    }

    public static function GetAvailableThemes()
    {
        return Filter::Apply(AvailableThemesFilter::class, static::GetThemes());
    }

    public static function InstallFromFile($file)
    {
        if(!File::exists($file)) { return false;}
        if(File::extension($file) != 'zip') { return false; }
        $tempName = \Str::uuid();
        $tempDirectory = File::ensureDirectoryExists(storage_path($tempName));
        $files = (new Unzip())->extract($file, storage_path($tempName));
        if(count($files) == 0 || !\Str::endsWith($files[0],'/'))
        {
            File::deleteDirectory(storage_path($tempName));
            return false;
        }
        $themeName = trim($files[0], '/');
        if(!in_array($themeName.'/theme.json', $files))
        {
            File::deleteDirectory(storage_path($tempName));
            return false;
        }

        File::copyDirectory(storage_path($tempName), self::GetBasePath());
        File::deleteDirectory(storage_path($tempName));
        return true;
    }

    /**
     * Get Theme By Name
     *
     * @return Theme
     */
    public static function GetTheme($name)
    {
        $finder = new Finder();
        $directories = $finder->in(static::GetBasePath())->depth(0)->name('/'.$name.'/i')->directories();

        if($directories->count() > 0)
        {
            foreach($directories as $directory)
            {
                return Theme::LoadFrom($directory->getPathname().DIRECTORY_SEPARATOR.('theme.json'));
            }
        }
        return null;
    }

    /**
     * Get Active Theme
     *
     * @return Theme
     */
    public static function GetActiveTheme()
    {
        return Filter::Apply(ActiveThemeFilter::class, static::GetTheme(static::GetActiveThemeName()));
    }

    /**
     * Enable Theme
     *
     * @return boolean
     */
    public static function Enable($theme)
    {
        $shouldContinue = Action::Apply(ActivateThemeAction::class, $theme);
        if(!$shouldContinue) { return; }
        $theme = self::GetTheme($theme);
        if($theme == null) { return false; }
        $notFoundModules = [];
        $modules = [];
        foreach($theme->GetRequirements() as $requiredModule)
        {
            $module = Module::find($requiredModule);
            if($module == null) { $notFoundModules[] = $requiredModule; }
        }
        if(count($notFoundModules) > 0)
        {
            Feedback::getInstance()->addError(__('Cannot Find Theme Required Modules').' : '.implode(', ', $notFoundModules));
            return false;
        }


        //Enable Requirements
        foreach($theme->GetRequirements() as $requiredModule)
        {
            Modules::Enable($requiredModule);
        }
        //Friendly Name
        $friendlyName = str($theme->GetName())->slug();
        //Install Public Assets
        if(!File::exists(public_path('themes')))
        {
            mkdir(public_path('themes'));
        }
        if(File::exists(public_path('themes'.DIRECTORY_SEPARATOR.$friendlyName)))
        {
            System::unlink(public_path('themes'.DIRECTORY_SEPARATOR.$friendlyName));
        }
        if(is_dir($theme->GetPath('public')))
        {
            System::Link($theme->GetPath().DIRECTORY_SEPARATOR.'public', public_path('themes'.DIRECTORY_SEPARATOR.$friendlyName));
        }
        //Fire OnDeactivate Event
        $oldTheme = Themes::GetActiveTheme();
        if($oldTheme != null)
        {
            foreach($oldTheme->GetProviders() as $provider)
            {
                $provider = new $provider(app());
                if($provider instanceof ThemeProvider)
                { $provider->OnDeactivate(); }
            }
        }

        $shouldContinue = Action::Apply(ActivateThemeReadyAction::class, $theme);
        
        if(!$shouldContinue) { return; }
        set_setting('site::themes.active', $theme->GetName(), false);
        
        //Fire OnActivate Event
        foreach($theme->GetProviders() as $provider)
        {
            $provider = new $provider(app());
            if($provider instanceof ThemeProvider)
            { $provider->OnActivate(); }
        }
        return true;
    }

    public static function Path($themeName = null, $combine = '')
    {
        return  $themeName == null ? self::GetPath($combine) : self::GetPath(\Str::studly($themeName).'/'.$combine);
    }

    public static function FileExists($themeName, $filename)
    {
        return File::exists(self::Path($themeName, $filename));
    }

    public static function DirectoryExists($themeName, $filename)
    {
        return File::isDirectory(self::Path($themeName, $filename));
    }


    public static function Delete($themeName)
    {
        //self::Disable($pluginName);
        File::deleteDirectory(self::Path($themeName));
        return true;
    }



    public static function PutFile($themeName, $filename, $contents)
    {
        File::ensureDirectoryExists(self::Path($themeName, dirname($filename)));
        $filePath = self::Path($themeName, $filename);
        File::put($filePath, $contents);
        return $filePath;
    }

    public static function PutDirectory($themeName, $path, $keep = false)
    {
        File::ensureDirectoryExists(self::Path($themeName, $path));
        if($keep)
        {
            self::PutFile($themeName, rtrim($path,'/').'/'.'.gitkeep', '');
        }
    }

}
