<?php

namespace Cascade\Site\Directives;

class ThemeDirective
{
    public function handle($expression)
    {
        return "<?php echo theme_asset({$expression})?>";
    }
}
