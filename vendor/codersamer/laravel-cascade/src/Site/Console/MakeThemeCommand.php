<?php

namespace Cascade\Site\Console;

use Cascade\Site\Services\Stub;
use Cascade\Site\Services\Themes;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class MakeThemeCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'theme:make {theme}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description.';

    protected $themeName = '';

    protected $themeSlug = '';

    protected $themePath = '';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->themeName = $this->argument('theme');
        $this->themeSlug = strtolower($this->themeName);
        $themesPath = Themes::GetBasePath();
        //Ensure Themes Folder Exists
        File::ensureDirectoryExists($themesPath);
        //Generate Theme Directory
        $this->themePath = $themesPath.'/'.$this->themeName;
        File::ensureDirectoryExists($this->themePath);
        //Scaffold Theme
        $this->scaffoldTheme();
        $this->info('Theme ['.$this->themeName.'] Generated Successfully');
    }

    protected function scaffoldTheme()
    {
        //Generate Base Provider
        $this->generateProviders();
        //Generate Configs
        $this->generateConfigs();
        //Generate Helpers
        $this->generateHelpers();
        //Generate Resources
        $this->generateResources();
        //Generate Routes
        $this->generateRoutes();
        //Generate Plugin.json
        $this->generateJson();
        //Generate Composer File
        $this->generateComposer();
        //Generate Composer File
        $this->generateNPM();
        $this->generateWebpack();
        //Generate Public Folder
        $this->generatePublic();
        //Generate Default View
        $this->generateIndex();
        //Put Screenshot
        $this->generateScreenshot();
    }

    /**
     * Generate Package.json
     *
     * @return void
     */
    protected function generateNPM()
    {
        $packagesJsonContent = (new Stub('themes/package_json.stub', [

        ]))->render();
        Themes::PutFile($this->themeName, 'package.json', $packagesJsonContent);
    }


    /**
     * Generate Default View
     *
     * @return void
     */
    protected function generateIndex()
    {

        $packagesJsonContent = (new Stub('themes/blank_view.stub', [

        ]))->render();
        Themes::PutFile($this->themeName, 'Resources/views/index.blade.php', $packagesJsonContent);
    }

    /**
     * Generate Package.json
     *
     * @return void
     */
    protected function generateWebpack()
    {
        $webpackContent = (new Stub('themes/webpack_mix_js.stub', [
            'SLUG' => $this->themeSlug
        ]))->render();
        Themes::PutFile($this->themeName, 'webpack.mix.js', $webpackContent);
    }

     /**
     * Generate Base Plugin Providers
     *
     * @return void
     */
    protected function generateProviders()
    {
        //Base Provider
        $baseProviderContent = (new Stub('themes/base_provider.stub', [
            'NAME' => $this->themeName,
            'SLUG' => $this->themeSlug
        ]))->render();
        Themes::PutFile($this->themeName, 'Providers/ThemeServiceProvider.php', $baseProviderContent);
        //Route Provider
        $routeProviderContent = (new Stub('themes/route_provider.stub', [
            'NAME' => $this->themeName,
        ]))->render();
        Themes::PutFile($this->themeName, 'Providers/RouteServiceProvider.php', $routeProviderContent);
    }

    protected function generateConfigs()
    {
        $configContent = (new Stub('themes/config.stub', [
            'NAME' => $this->themeName
        ]))->render();
        Themes::PutFile($this->themeName, 'Config/config.php', $configContent);
    }

    protected function generateHelpers()
    {
        $helpersContent = (new Stub('themes/helpers.stub', []))->render();
        Themes::PutFile($this->themeName, 'Helpers/functions.php', $helpersContent);
    }

    protected function generateResources()
    {
        Themes::PutDirectory($this->themeName, 'Resources/views');
        Themes::PutDirectory($this->themeName, 'Resources/lang');
        Themes::PutDirectory($this->themeName, 'Resources/assets');
    }

    protected function generatePublic()
    {
        Themes::PutDirectory($this->themeName, 'public');
    }


    protected function generateRoutes()
    {
        $webRoutesContent = (new Stub('themes/web_routes.stub', []))->render();
        Themes::PutFile($this->themeName, 'Routes/web.php', $webRoutesContent);
        $apiRoutesContent = (new Stub('themes/api_routes.stub', []))->render();
        Themes::PutFile($this->themeName, 'Routes/api.php', $apiRoutesContent);
    }

    protected function generateJson()
    {
        $jsonContent = (new Stub('themes/theme_json.stub', [
            'NAME' => $this->themeName,
            'SLUG' => $this->themeSlug
        ]))->render();
        Themes::PutFile($this->themeName, 'theme.json', $jsonContent);
    }

    protected function generateComposer()
    {
        $composerContent = (new Stub('themes/composer_json.stub', [
            'NAME' => $this->themeName,
            'SLUG' => $this->themeSlug
        ]))->render();
        Themes::PutFile($this->themeName, 'composer.json', $composerContent);
    }

    /**
     * Generate Plugin Screenshot
     */
    protected function generateScreenshot()
    {
        Themes::PutFile($this->themeName, 'screenshot.png', file_get_contents(module_path('Site', 'Resources/assets/images/default-theme-screenshot.png')));
    }


    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['theme', InputArgument::REQUIRED, 'Theme name to be generated.'],
        ];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null],
        ];
    }
}
