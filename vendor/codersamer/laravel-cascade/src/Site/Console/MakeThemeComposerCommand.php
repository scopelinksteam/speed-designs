<?php

namespace Cascade\Site\Console;

use Cascade\Site\Services\Stub;
use Cascade\Site\Services\Themes;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class MakeThemeComposerCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'theme:make-composer {composer} {theme}';

    protected $themeName = '';

    protected $themePath = '';

    protected $composer = '';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description.';


    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $theme = Themes::GetTheme($this->argument('theme'));
        $this->themeName = $this->argument('theme');
        $this->composer =  $this->argument('composer');
        $this->themePath = $theme->GetPath();

        $composerContent = (new Stub('themes/view_composer.stub', [
            'name' => $this->composer,
            'theme' => $this->themeName
        ]))->render();
        Themes::PutFile($this->themeName, 'View/Composers/'.$this->composer.'Composer.php', $composerContent);
        //Scaffold Theme
        $this->info('View Composer ['.$this->composer.'] for Theme ['.$this->themeName.'] Generated Successfully');
    }


    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['composer', InputArgument::REQUIRED, 'Theme name to be generated.'],
            ['theme', InputArgument::REQUIRED, 'Theme name to be generated.'],
        ];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null],
        ];
    }
}
