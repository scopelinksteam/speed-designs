<?php

namespace Cascade\Site\Console;

use Cascade\Site\Services\Themes;
use Cascade\Translations\Services\TranslationScanner;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class TranslateThemeCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'theme:translate {theme}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $scanner = new TranslationScanner;
        $theme = Themes::GetTheme($this->argument('theme'));
        if($theme == null)
        {
            $this->error('Cannot Find Theme with name : '.$this->argument('theme'));
            return;
        }
        $this->info("Set up Scanner...");
        $scanner->in($theme->GetPath())->exclude(['storage','vendor','public','database']);
        $this->info('Scanning Process Starting');
        $scanner->Scan();
        $this->info('Generate Default Language File');
        $scanner->SaveDefaultJson($theme->GetPath('Resources/lang'));
        $this->info('Generate Languages JSON Files');
        $scanner->SaveLocalizedJson($theme->GetPath('Resources/lang'));
        $this->info('Theme Translations Saved Successfully');
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['plugin', InputArgument::REQUIRED, 'Plugin Name' ]
        ];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null],
        ];
    }
}
