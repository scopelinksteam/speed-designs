<?php

namespace Cascade\Site\Tables;

use Cascade\Dashboard\Services\Datatables\Action;
use Cascade\Dashboard\Services\Datatables\Column;
use Cascade\Dashboard\Services\Datatables\DataTable;
use Cascade\Site\Entities\Post;
use Cascade\Site\Enums\PostFeature;
use Cascade\Site\Services\PostType;

class PostsTable extends DataTable
{

    protected PostType $configurations;

    public function __construct(String $type)
    {
        $this->configurations = PostType::for($type);

        $this->editColumn('title', function(Post $post){
            $content =  $post->title . '<br />' . $this->badge($post->slug, 'dark').'<br />';
            if($this->configurations->supports(PostFeature::Comments))
            {
                $commentsIcon = '<i class="fas fa-comment me-2"></i>';
                $content .= $this->badge($commentsIcon.$post->comments()->count(), 'danger me-2');
            }
            if($this->configurations->supports(PostFeature::Author))
            {
                $userIcon = '<i class="fas fa-user me-2"></i>';
                $content .= $this->badge($userIcon.$post->author->name ?? __('NA'), 'dark me-2');
            }
            if($this->configurations->supports(PostFeature::Category))
            {
                $folderIcon = '<i class="fas fa-folder me-2"></i>';
                $categories = $post->categories;
                $categories = count($categories) == 0 ? __('NA') : implode(', ', $categories->pluck('name')->toArray());
                $content .= $this->badge($folderIcon.$categories, 'primary me-2');
            }
            return $content;
        });

        $this->editColumn('thumbnail', function(Post $post){
            $thumbnail = $post->thumbnail_file;
            return $thumbnail == null ? '' : $this->thumbnail(uploads_url($thumbnail));
        });
    }

    public function query()
    {
        $query = call_user_func([$this->configurations->type(), 'query']);

        return $query;
    }

    public function columns()
    {
        $columns = [];

        $columns[] = Column::make('id')->text(__('ID'));

        if($this->configurations->supports(PostFeature::Thumbnail))
        { $columns[] = Column::make('thumbnail')->text(__('Thumbnail')); }

        if($this->configurations->supports(PostFeature::Title))
        { $columns[] = Column::make('title')->text(__('Title')); }

        if($this->configurations->supports(PostFeature::Status))
        { $columns[] = Column::make('status')->text(__('Status')); }

        return $columns;
    }

    public function actions()
    {
        $actions = [];

        $actions[] = Action::make('edit', __('Edit'))->route("dashboard.site.{$this->configurations->slug()}.edit", ['post' => 'id']);
        $actions[] = Action::make('delete', __('Delete'))->route("dashboard.site.{$this->configurations->slug()}.destroy", ['post' => 'id']);
        return $actions;
    }

}
