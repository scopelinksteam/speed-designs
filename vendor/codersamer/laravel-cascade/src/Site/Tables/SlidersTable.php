<?php

namespace Cascade\Site\Tables;

use Cascade\Dashboard\Services\Datatables\Action;
use Cascade\Dashboard\Services\Datatables\Column;
use Cascade\Dashboard\Services\Datatables\DataTable;
use Cascade\Site\Entities\Slider;
use Illuminate\Support\Facades\Gate;

class SlidersTable extends DataTable
{

    public function __construct($params = null)
    {
        $this->editColumn('slides_count', function(Slider $slider){
            return $this->badge($slider->slides()->count());
        });
    }


    public function query($search = null, $filters = [])
    {
        $query = Slider::query();

        return $query;
    }

    public function columns()
    {
        return [
            Column::make('id')->text(__('ID')),
            Column::make('name')->text(__('Name')),
            Column::make('slides_count')->text(__('Slides')),
            Column::make('status')->text(__('Status'))
        ];
    }

    public function actions()
    {
        $actions = [];
        if(Gate::allows('edit sliders'))
        {
            $actions[] = Action::make('edit', __('Edit'))->route('dashboard.site.sliders.edit', ['slider' => 'id']);
        }
        if(Gate::allows('customize sliders'))
        {
            $actions[] = Action::make('customize', __('Customize'))->route('dashboard.site.sliders.customize', ['slider' => 'id']);
        }

        if(Gate::allows('delete sliders'))
        {
            $actions[] = Action::make('delete', __('Delete'))->route('dashboard.site.sliders.destroy', ['slider' => 'id']);
        }

        return $actions;
    }

}
