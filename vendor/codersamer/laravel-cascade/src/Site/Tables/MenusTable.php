<?php

namespace Cascade\Site\Tables;

use Cascade\Dashboard\Services\Datatables\Action;
use Cascade\Dashboard\Services\Datatables\Column;
use Cascade\Dashboard\Services\Datatables\DataTable;
use Cascade\Site\Entities\SiteMenu;


class MenusTable extends DataTable
{
    public function query()
    {
        return SiteMenu::query();
    }

    public function columns()
    {
        return [
            Column::make('id')->text(__('ID')),
            Column::make('name')->text(__('Name'))
        ];
    }

    public function actions()
    {
        return [
            Action::make('customize', __('Customize'))->route('dashboard.site.menus.customize', ['menu' => 'id']),
            Action::make('edit', __('Edit'))->route('dashboard.site.menus.edit', ['menu' => 'id']),
            Action::make('delete', __('Delete'))->route('dashboard.site.menus.delete', ['menu' => 'id']),
        ];
    }
}
