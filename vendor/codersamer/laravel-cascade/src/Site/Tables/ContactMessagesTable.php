<?php

namespace Cascade\Site\Tables;

use Illuminate\Support\Facades\Gate;
use Cascade\Dashboard\Services\Datatables\Action;
use Cascade\Dashboard\Services\Datatables\Column;
use Cascade\Dashboard\Services\Datatables\DataTable;
use Cascade\Site\Entities\ContactMessage;

class ContactMessagesTable extends DataTable
{

    public function query()
    {
        return ContactMessage::orderBy('is_read');
    }
    public function columns()
    {
        return [
            Column::make('name')->text(__('Name')),
            Column::make('phone')->text(__('Phone')),
            Column::make('subject')->text(__('Subject')),
            Column::make('created_at')->text(__('Sent At'))
        ];
    }

    public function actions()
    {
        $actions = [];
        $actions[] = Action::make('show', __('Show Message'))->route('dashboard.site.messages.show', ['message' => 'id']);
        if(Gate::allows('delete contact messages'))
        {
            $actions[] = Action::make('delete', __('Delete'))->route('dashboard.site.messages.destroy', ['message' => 'id']);
        }
        return $actions;
    }
}
