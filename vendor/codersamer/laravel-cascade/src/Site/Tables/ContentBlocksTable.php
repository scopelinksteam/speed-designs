<?php

namespace Cascade\Site\Tables;

use Illuminate\Support\Facades\Gate;
use Cascade\Dashboard\Services\Datatables\Action;
use Cascade\Dashboard\Services\Datatables\Column;
use Cascade\Dashboard\Services\Datatables\DataTable;
use Cascade\Site\Entities\ContentBlock;

class ContentBlocksTable extends DataTable
{

    protected $type = null;

    public function __construct($type)
    {
        $this->type = $type;
    }

    public function columns()
    {
        return [
            Column::make('id')->text(__('ID')),
            Column::make('title')->text(__('Title')),
            Column::make('block_type')->text(__('Type'))
        ];
    }

    public function query()
    {
        return ContentBlock::type($this->type);
    }

    public function actions()
    {
        $actions = [];
        if(Gate::allows('edit content blocks')) { $actions[] = Action::make('edit', __('Edit Block'))->route('dashboard.site.content_blocks.edit', ['type' => 'block_type', 'block' => 'id']);}
        if(Gate::allows('delete content blocks')) { $actions[] = Action::make('delete', __('Delete Block'))->route('dashboard.site.content_blocks.destroy', ['type' => 'block_type', 'block' => 'id']);}
        return $actions;
    }
}
