<?php

namespace Cascade\Site\Tables;

use Cascade\Dashboard\Services\Datatables\Action;
use Cascade\Dashboard\Services\Datatables\Column;
use Cascade\Dashboard\Services\Datatables\DataTable;
use Cascade\Site\Entities\ContentBlock;

class GeneralContentBlocksTable extends DataTable
{
    public function __construct($params = null)
    {
        $this->editColumn('name', function(ContentBlock $block){
            return ContentBlock::GetType($block->block_type)->GetName();
        });
    }

    public function query($search = null, $filters = [])
    {
        $types = ContentBlock::GetTypes();
        $singleTypes = [];
        foreach($types as $type) {
            if($type->IsSingle()) { $singleTypes[] = $type->GetType(); }
        }

        $query = ContentBlock::query();
        $query = $query->whereIn('block_type', $singleTypes);

        return $query;
    }

    public function columns()
    {
        return [
            Column::make('name')->text(__('Name')),
            Column::make('title')->text(__('Title')),
            Column::make('block_type')->text(__('Type')),
            Column::make('updated_at')->text(__('Last Update'))
        ];
    }

    public function actions()
    {
        return [
            Action::make('edit', __('Edit'))->route('dashboard.site.content_blocks.edit', ['block' => 'id', 'type' => 'block_type'])->permission('edit content blocks')
        ];
    }
}
