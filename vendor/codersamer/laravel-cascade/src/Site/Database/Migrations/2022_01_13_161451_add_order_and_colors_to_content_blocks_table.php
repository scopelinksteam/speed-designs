<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOrderAndColorsToContentBlocksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('content_blocks', function (Blueprint $table) {
            $table->string('background_color')->nullable()->default('transparent');
            $table->string('foreground_color')->nullable()->default('transparent');
            $table->unsignedBigInteger('order')->default(1);
            $table->string('theme')->nullable();
            $table->string('layout')->nullable();
            $table->string('css_class')->nullable();
            $table->string('css_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('content_blocks', function (Blueprint $table) {

        });
    }
}
