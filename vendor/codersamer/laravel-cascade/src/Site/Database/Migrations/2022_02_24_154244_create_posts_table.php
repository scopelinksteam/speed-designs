<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->id();
            $table->string('type')->nullable();
            $table->text('title')->nullable();
            $table->text('content')->nullable();
            $table->text('custom_excerpt')->nullable();
            $table->string('slug')->nullable();
            $table->unsignedBigInteger('author_id')->default(0);
            $table->unsignedBigInteger('category_id')->default(0);
            $table->unsignedBigInteger('parent_id')->default(0);
            $table->unsignedInteger('status')->default(0);
            $table->unsignedInteger('comment_status')->default(0);
            $table->string('thumbnail_file')->nullable();
            $table->string('password')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
