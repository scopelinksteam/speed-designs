<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('content_blocks', function (Blueprint $table) {
            $table->unsignedBigInteger('parent_id')->after('block_type')->default(0);
            $table->string('video_link')->after('link')->nullable();
            $table->string('link_type')->nullable()->after('link');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('content_blocks', function (Blueprint $table) {
            $table->dropColumn('parent_id');
            $table->dropColumn('video_link');
            $table->dropColumn('link_type');
        });
    }
};
