<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSlidersButtonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sliders_buttons', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('slide_id');
            $table->text('title')->nullable();
            $table->string('link')->default('#');
            $table->string('theme')->nullable();
            $table->string('style')->nullable();
            $table->string('target')->default('');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sliders_buttons');
    }
}
