<?php

namespace Cascade\Site\Menus\Builders;

use Cascade\Site\Menus\Renderers\LinkMenuRenderer;
use Cascade\Site\Abstraction\MenuItemBuilder;
use Cascade\Site\Abstraction\MenuItemRenderer;
use Cascade\System\DTO\HTML\Div;
use Cascade\System\DTO\HTML\Input;
use Cascade\System\DTO\HTML\Label;
use Cascade\System\DTO\HTML\View;
use Cascade\System\Enums\InputType;
use Cascade\System\Services\HTMLBuilder;

class LinkMenuBuilder extends MenuItemBuilder
{

    public function Renderer(array $options): MenuItemRenderer
    { return  new LinkMenuRenderer($options); }

    public function Designer() : HTMLBuilder
    {
        return $this->designer->clear()->append(
            Div::make()->class('form-group')->append(
                Label::make()->for('text')->text(__('Text')),
                Input::make(InputType::Text, 'text')->class('form-control')
            ),
            Div::make()->class('form-group')->append(
                Label::make()->for('link')->text(__('Link')),
                Input::make(InputType::Text, 'link')->class('form-control')
            )
        );
    }

    public function Editor(array $options) : HTMLBuilder
    {
        return $this->editor->clear()->append(
            Div::make()->class('form-group')->append(
                Label::make()->for('text')->text(__('Text')),
                Input::make(InputType::Text, 'text')->class('form-control')->value($options['text'] ?? '')
            ),
            Div::make()->class('form-group')->append(
                Label::make()->for('link')->text(__('Link')),
                Input::make(InputType::Text, 'link')->class('form-control')->value($options['link'] ?? '')
            )
        );
    }

    public function Handle(array $oldOptions, $newOptions): array
    {
        return $newOptions;
    }
}
