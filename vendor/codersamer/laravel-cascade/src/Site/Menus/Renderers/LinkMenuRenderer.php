<?php

namespace Cascade\Site\Menus\Renderers;

use Cascade\Site\Abstraction\MenuItemRenderer;

class LinkMenuRenderer extends MenuItemRenderer
{

    public function GetTitle()
    {
        return isset($this->options['text']) ? $this->options['text'] : __('Undefined');
    }

    public function GetLink()
    {
        return isset($this->options['text']) ? $this->options['link'] : '';
    }

    public function GetAttributes()
    {

    }
}
