<?php

namespace Cascade\Site\Components;

use Cascade\Site\Services\Site;
use Illuminate\View\Component;

class TitleComponent extends Component
{
    public function render()
    {
        $pageTitle = Site::Title();
        return strip_tags(site_name()).($pageTitle != null ? ' | '.strip_tags($pageTitle) : '');
    }
}
