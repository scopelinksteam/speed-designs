<?php

namespace Cascade\Site\Components;

use Cascade\Dashboard\Entities\MenuItem;
use Cascade\Site\Entities\SiteMenu;
use Cascade\Site\Services\MenuBuilder;
use Livewire\Component;
use Cascade\Site\Entities\SiteMenuItem;

class MenuBuilderComponent extends Component
{

    public SiteMenu $menu;

    public $items = [];

    protected $listeners = [
        'AddMenuItem' => 'addMenuItem',
        'UpdateMenuItem' => 'updateMenuItem',
        'SetMenuOrder' => 'setMenuOrder'
    ];


    public function addMenuItem($builderId, $data)
    {
        $item = new SiteMenuItem();
        $item->menu_id = $this->menu->id;
        $item->type = $builderId;
        $item->data = serialize($data);
        $maxOrder = $this->menu->children()->max('order');
        $item->order = $maxOrder+1;
        $item->save();
        $this->items = $this->menu->items;
        $this->render();
        $this->emit('ItemsUpdated');
    }

    public function updateMenuItem($builderId, $id, $data)
    {
        $item = SiteMenuItem::find(intval($id));
        if($item == null) { return; }
        $builder = MenuBuilder::GetBuilder($builderId);
        $item->data = serialize($builder->instance->Handle($item->data, $data));
        $item->save();
        $this->items = $this->menu->items;
        $this->render();
        $this->emit('ItemsUpdated');
    }

    public function setMenuOrder($data)
    {
        foreach($data as $itemOrder)
        {
            $item = SiteMenuItem::find(intval($itemOrder['id']));
            if($item != null)
            {
                $item->order = intval($itemOrder['order']);
                $item->parent_id = isset($itemOrder['parent']) ? intval($itemOrder['parent']) : 0;
                $item->save();
            }
        }
        $this->render();
        $this->emit('ItemsUpdated');
    }

    public function mount(SiteMenu $menu)
    {
        $this->menu = $menu;
        $this->items = $menu->items;
    }

    public function DeleteItem($id)
    {
        $item = SiteMenuItem::find($id);
        if($item) { $item->delete(); $this->items = $this->menu->items; $this->render(); $this->emit('ItemsUpdated'); }
    }

    public function render()
    {
        $this->items = $this->menu->items;
        return view('site::dashboard.menus.components.builder', [
            'builders' => MenuBuilder::Builders(),
        ]);
    }
}
