<?php

namespace Cascade\Site\Components;

use Cascade\Site\Services\Site;
use Illuminate\View\Component;

class ScriptsComponent extends Component
{
    public function render()
    {
        $scripts = Site::Scripts();
        $template = "\t".'<script src="%s"></script>';
        $output  = '<!-- Scripts -->'."\n";
        foreach($scripts as $key => $path)
        { $output .= sprintf($template, $path)."\n"; }

        $output .= '<script type="text/javascript">'."\n";
        $output .= Site::CustomJS();
        $output .= '</script>'."\n";

        $output .= "\t<!-- End: Scripts -->\n";
        return $output;
    }
}
