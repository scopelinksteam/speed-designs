<?php

namespace Cascade\Site\Components;

use Cascade\Site\DTO\WidgetArea;
use Cascade\Site\Entities\WidgetOption;
use Illuminate\View\Component;

class WidgetsComponent extends Component
{
    public function __construct(public String $name)
    {

    }

    public function build(WidgetOption $widget) : String
    {
        $title = $widget->options['title'] ?? null;
        $content = $widget->handler->render($widget->options)->toHtml();
        $area = WidgetArea::find($this->name);
        $title = $area->titleVisible() ? $widget->title($title) : '';
        $content = $widget->content($title.$content);

        return $content ?? '';
    }

    public function render()
    {
        $widgets = [];
        $area = WidgetArea::find($this->name);
        if($area != null)
        {
            $widgets = $area->widgets();
        }
        return view('site::frontend.widgets_area', ['widgets' => $widgets]);
    }
}
