<?php

namespace Cascade\Site\Components;

use Illuminate\View\Component;

class GoogleMapComponent extends Component
{

    public function __construct(
        public String $address,
        public ?String $latitude = null,
        public ?String $langitude = null
    )
    {

    }
    public function render()
    {
        return view('site::shared.components.google-map');
    }
}
