<?php

namespace Cascade\Site\Components;

use Cascade\System\Services\System;
use Illuminate\View\Component;

class IconComponent extends Component
{

    public $class;
    public $icon;
    public $id;

    public function __construct($icon, $class = '', $id = '')
    {
        $this->class = $class;
        $this->icon = $icon;
        $this->id = $id;
    }

    public function render()
    {
        return System::FindView('site::shared.components.icon', [
            'class' => $this->class,
            'icon' => $this->icon,
            'id' => $this->id,
        ]);
    }
}
