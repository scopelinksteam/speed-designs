<?php

namespace Cascade\Site\Components;

use Cascade\System\Services\System;
use Illuminate\View\Component;

class ContactFormComponent extends Component
{
    public $showLabels = true;

    public $formClass = '';

    public $formId = '';

    public $columns = 1;

    public $submitClass = '';

    public $inputClass = '';

    public $wrapperClass = '';

    public $messageClass = '';

    public function __construct($showLabels = true, $formClass = '', $formId = '', $columns = 1, $submitClass = '', $inputClass = '', $wrapperClass = '', $messageClass = null)
    {
        $this->showLabels = $showLabels;
        $this->formClass = $formClass;
        $this->formId = $formId;
        $this->columns = $columns;
        $this->submitClass = $submitClass;
        $this->inputClass = $inputClass;
        $this->wrapperClass = $wrapperClass;
        $this->messageClass = $messageClass ?? $inputClass;
    }
    public function render()
    {
        return System::FindView('site::frontend.components.contact_form');
    }
}
