<?php

namespace Cascade\Site\Components;

use Cascade\Site\DTO\WidgetArea;
use Cascade\Site\Entities\WidgetOption;
use Cascade\Site\Services\Widgets;
use Livewire\Component;

class WidgetsBuilderComponent extends Component
{

    protected $listeners = [
        'AreaSwitched' => 'onAreaSwitched',
        'WidgetUpdated' => 'onWidgetUpdated',
        'WidgetsOrderChanged' => 'onWidgetsOrderChanged',
    ];

    public $currentAreaId = '';

    public function mount()
    {
        if($this->currentAreaId == '')
        {
            $areas = WidgetArea::all();
            if($areas->count() > 0){$this->currentAreaId = $areas->first()->id();}
        }

    }

    public function onAreaSwitched($area)
    {
        $this->currentAreaId = $area;
        $this->emit('WidgetsChanged');
    }

    public function onWidgetUpdated($widgetId)
    {
        $this->render();
        $this->emit('WidgetsChanged');
    }

    public function onWidgetsOrderChanged($data)
    {
        foreach($data as $widgetData)
        {
            $widget = WidgetOption::find($widgetData['id']);
            $widget->order = intval($widgetData['order']);
            $widget->save();
        }
        $this->emit('WidgetsChanged');
    }

    public function getCurrentAreaProperty()
    {
        return $this->currentAreaId == '' ? null : $this->currentAreaId;
    }

    public function addWidget($widgetId)
    {
        if($this->currentAreaId == '') { return; }
        $currentOrder = WidgetOption::where('area_id', $this->currentAreaId)->max('order');
        $widget = new WidgetOption();
        $widget->widget_id = $widgetId;
        $widget->area_id = $this->currentAreaId;
        $widget->options = [];
        $widget->order = $currentOrder +1;
        $widget->save();
        $this->render();
        $this->emit('WidgetsChanged');
    }

    public function deleteWidget($widgetId)
    {
        $widget = WidgetOption::find($widgetId);
        if($widget)
        {
            $widget->delete();
            $this->render();
            $this->emit('WidgetsChanged');
        }
    }

    public function render()
    {
        return view('site::dashboard.widgets.components.builder', [
            'widgets' => Widgets::all(),
            'areas' => WidgetArea::all(),
        ]);
    }
}
