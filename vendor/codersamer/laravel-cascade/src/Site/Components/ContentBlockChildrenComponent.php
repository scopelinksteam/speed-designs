<?php

namespace Cascade\Site\Components;

use Cascade\Site\Entities\ContentBlock;
use Livewire\Component;

class ContentBlockChildrenComponent extends Component
{

    public $parent_type = '';

    public $child_type = '';

    public $block;

    public $rowIndex = 0;

    public $rows = [];

    public function mount(String $parent, String $child, $block = null)
    {
        $this->parent_type = $parent; 
        $this->child_type = $child;
        $this->block = $block;
        $this->rowIndex = count($this->getRecordsProperty());
    }

    public function addRow()
    {
        $this->rowIndex++;
        $this->rows[$this->rowIndex] = 0;
    }

    public function getParentProperty()
    {
        return ContentBlock::Of($this->parent_type);
    }

    public function getChildProperty()
    {
        $parent = $this->getParentProperty();
        foreach($parent->GetChildren() as $child)
        {
            if($child->GetType() == $this->child_type)
            {
                return $child;
            }
        }
    }

    public function getRecordsProperty()
    {
        $child = $this->getChildProperty();
        return $this->block == null ? collect() : $this->block->children()->where('block_type', $this->child_type)->get();
    }

    public function render()
    {
        return view('site::dashboard.blocks.tabs.components.child');
    }
}