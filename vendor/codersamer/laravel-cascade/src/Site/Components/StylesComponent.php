<?php

namespace Cascade\Site\Components;

use Cascade\Site\Services\Site;
use Illuminate\View\Component;

class StylesComponent extends Component
{
    public function render()
    {
        $styles = Site::Styles();
        $template = "\t".'<link rel="stylesheet" href="%s">';
        $output  = '<!-- Styles -->';
        foreach($styles as $key => $path)
        { $output .= sprintf($template, $path)."\n"; }

        $output .= '<style>'."\n";
        $output .= Site::CustomCSS();
        $output .= '</style>'."\n";

        $output .= "\t<!-- End: Styles -->\n";
        return $output;
    }
}
