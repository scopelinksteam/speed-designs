<?php

namespace Cascade\Site\DTO;

use Cascade\Site\Entities\WidgetOption;
use Illuminate\Support\Collection;

class WidgetArea
{

    protected static array $areas = [];

    protected String $id = '';

    protected String $name = '';

    protected String $before = '';

    protected String $after = '';

    protected String $beforeTitle = '';

    protected String $afterTitle = '';

    protected bool $showTitle = true;

    /**
     * Create new Area
     *
     * @param String $id
     * @param String $name
     * @return static
     */
    public static function make(String $id, String $name) : static
    {
        return new static($id, $name);
    }

    /**
     * Get All Registered Areas
     *
     * @return Collection
     */
    public static function all() : Collection
    {
        return collect(static::$areas);
    }

    /**
     * Find Area matches the ID
     *
     * @param String $id
     * @return mixed
     */
    public static function find(String $id)
    {
        return static::all()->first(function($area) use($id){
            return $id == $area->id();
        });
    }

    /**
     * Initiate New Area
     *
     * @param String $id
     * @param String $name
     */
    protected function __construct(String $id, String $name)
    {
        $this->id = $id;
        $this->name = $name;
        static::$areas[$id] = $this;
    }

    /**
     * Get or Set Widget Area ID
     *
     * @param String|null $id
     * @return String|static
     */
    public function id(?String $id = null) : String|static
    {
        if($id != null)
        {
            $this->id = $id;
            return $this;
        }
        return $this->id;
    }

    /**
     * Get or Set Widget Area Name
     *
     * @param String|null $name
     * @return String|static
     */
    public function name(?String $name = null) : String|static
    {
        if($name != null)
        {
            $this->name = $name;
            return $this;
        }
        return $this->name;
    }

    public function wrap(String $before, String $after) : static
    {
        $this->before = $before;
        $this->after = $after;
        return $this;
    }

    public function wrapTitle(String $before, String $after) : static
    {
        $this->beforeTitle = $before;
        $this->afterTitle = $after;
        return $this;
    }

    public function beforeTitle() : String
    {
        return $this->beforeTitle;
    }

    public function afterTitle() : String
    {
        return $this->afterTitle;
    }


    public function after() : String
    {
        return $this->after;
    }

    public function before() : String
    {
        return $this->before;
    }

    public function titleVisible() : bool
    {
        return $this->showTitle;
    }

    public function hideTitle()
    {
        $this->showTitle = false;
    }

    public function showTitle()
    {
        $this->showTitle = true;
    }


    public function widgets()
    {
        return WidgetOption::where('area_id', $this->id())->orderBy('order', 'ASC')->get();
    }
}
