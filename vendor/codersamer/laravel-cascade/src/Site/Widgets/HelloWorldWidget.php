<?php

namespace Cascade\Site\Widgets;

use Cascade\Site\Abstraction\Widget;
use Cascade\System\DTO\HTML\Div;
use Cascade\System\DTO\HTML\Input;
use Cascade\System\DTO\HTML\Label;
use Cascade\System\Enums\InputType;

class HelloWorldWidget extends Widget
{
    public function designer(array $options)
    {
        $this->builder()->append(Div::make()->append(
            Div::make()->class('form-group')->append(
                Label::make()->text(__('Title')),
                (new Input(InputType::Text, 'title'))->class('form-control')->value($options['title'] ?? '')
            )
        ));
    }
    public function handle(array $oldOptions, $newOptions) : array
    {
        return $newOptions;
    }

    public function render()
    {

    }
}
