<?php

namespace Cascade\Site\Widgets;

use Cascade\Site\Abstraction\Widget;
use Cascade\System\DTO\HTML\Div;
use Cascade\System\DTO\HTML\Input;
use Cascade\System\DTO\HTML\Label;
use Cascade\System\DTO\HTML\Textarea;
use Cascade\System\Enums\InputType;

class HTMLWidget extends Widget
{
    function designer(array $options)
    {
        $this->builder()->append(
            Div::make()->class('form-group')->append(
                Label::make()->text(__('Title')),
                (new Input(InputType::Text, 'title'))->class('form-control')->value($options['title'] ?? '')
            ),
            Div::make()->class('form-group')->append(
                Label::make()->text(__('Code')),
                (new Textarea('code'))->class('form-control')->value($options['code'] ?? '')
            )
        );
    }

    function handle(array $oldOptions, $newOptions): array
    {
        return $newOptions;
    }

    function render(array $options)
    {
        return view('site::widgets.html', ['options' => $options]);
    }
}
