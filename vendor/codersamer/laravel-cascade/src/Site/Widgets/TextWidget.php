<?php

namespace Cascade\Site\Widgets;

use Cascade\Site\Abstraction\Widget;
use Cascade\System\DTO\HTML\Div;
use Cascade\System\DTO\HTML\Input;
use Cascade\System\DTO\HTML\Label;
use Cascade\System\DTO\HTML\Textarea;
use Cascade\System\Enums\InputType;

class TextWidget extends Widget
{
    public function __construct()
    {

    }

    public function designer(array $options)
    {
        $this->builder()->append(
            (new Div())->class('form-group')->append(
                (new Label())->text(__('Title')),
                (new Input(InputType::Text, 'title'))->class('form-control')->value($options['title'] ?? '')
            ),
            (new Div())->class('form-group')->append(
                (new Label())->text(__('Text')),
                (new Textarea("text"))->class('form-control')->value($options['text'] ?? '')
            )
        );
    }

    public function handle(array $oldOptions, $newOptions): array
    {
        return $newOptions;
    }

    public function render(array $options)
    {
        return view('site::widgets.text', ['options' => $options]);
    }
}
