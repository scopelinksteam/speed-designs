<?php

return [
    'view content blocks' => 'View List of Content Blocks',
    'edit content blocks' => 'Edit Existing Blocks',
    'create content blocks' => 'Create New Content Blocks',
    'delete content blocks' => 'Delete Existing Blocks',
    'view contact messages' => 'Display and Show Contact Messages',
    'delete contact messages' => 'Delete Received Contact Messages',
    'view themes' => 'View Installed Themes',
    'enable themes' => 'Enable Site Themes',
    'disable themes' => 'Disable Site Themes',
    'install themes' => 'Add and Install New Themes',
    'view sliders' => 'View Site Sliders List',
    'edit sliders' => 'Edit existing Site Sliders',
    'customize sliders' => 'Customize Existing Site Sliders and manage Slides',
    'delete sliders' => 'Delete Existing Site Sliders',
    'build menus' => 'Build and Customize Site Menus',
];
