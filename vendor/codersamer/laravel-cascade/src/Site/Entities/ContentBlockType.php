<?php

namespace Cascade\Site\Entities;

class ContentBlockType
{

    protected String $type = '';

    protected String $name = '';

    protected array $fields = [];

    protected String $view = '';

    protected String $passAs = 'block';

    protected $viewValuesCallback = null;

    protected bool $single = false;

    protected bool $showInMenu = true;

    protected array $children = [];

    protected function __construct($type, $name, $single)
    {
        $this->type($type);
        $this->name($name);
        $this->single($single);
    }

    public static function make($type, $name, $single = false)
    {
        return new ContentBlockType($type, $name, $single);
    }

    public function type(String $type) { $this->type = $type; return $this; }
    public function showInMenu(bool $show) { $this->showInMenu = $show; return $this; }
    public function name(String $name) { $this->name = $name; return $this; }
    public function fields(ContentBlockField ...$fields) { $this->fields =  array_merge($this->fields, $fields); return $this; }
    public function single(bool $single) { $this->single = $single; return $this; }
    public function view($view, $passAs = 'block', $viewValuesCallback = null)
    {
        $this->view = $view;
        $this->passAs = $passAs;
        $this->viewValuesCallback = $viewValuesCallback;
        return $this;
    }
    public function addChild(ContentBlockType $child) :self
    {
        $this->children[] = $child;
        return $this;
    }
    public function store() : self
    {
        $exists = ContentBlock::type($this->GetType())->first();

        if($exists) { return $this; }

        $block = new ContentBlock();
        $block->block_type = $this->GetType();
        $block->save();

        return $this;
    }


    public function GetType() { return $this->type; }
    public function GetName() { return $this->name; }
    public function GetFields() { return $this->fields; }
    public function IsSingle() { return $this->single; }
    public function GetChildren() { return $this->children; }
    public function IsMenuVisible() { return $this->showInMenu; }
    /**
     * Return Field Options
     *
     * @param String $fieldName
     * @return ContentBlockField
     */
    public function GetField($fieldName)
    {
        foreach($this->fields as $field)
        {
            if($field->GetType() == $fieldName) { return $field; }
        }
        return null;
    }
    public function GetView() { return $this->view; }
    public function GetPassToViewAs() { return $this->passAs; }
    public function GetViewValuesCallback() { return $this->viewValuesCallback; }
    public function Supports($fieldName)
    {
        foreach($this->fields as $field)
        {
            if($field->GetType() == $fieldName) { return true; }
        }
        return false;
    }
    public function SupportsAny(...$fieldNames)
    {
        foreach($fieldNames as $name)
        {
            if($this->Supports($name)) { return true; }
        }
        return false;
    }







}
