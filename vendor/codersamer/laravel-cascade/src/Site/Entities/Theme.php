<?php

namespace Cascade\Site\Entities;

use Cascade\Site\Services\Themes;
use Cascade\System\Enums\OSFamily;
use Symfony\Component\Finder\Finder;

class Theme
{
    protected $name = '';

    protected $slug = '';

    protected $alias = '';

    protected $description = '';

    protected $file = '';

    protected $path = '';

    protected $protected = false;

    protected $keywords = [];

    protected $priority = 0;

    protected $providers = [];

    protected $aliases = [];

    protected $files = [];

    protected $requires = [];

    protected $enabled = true;

    protected $author = '';

    protected $icon = '';

    protected $screenshot = '';

    public static function LoadFrom($file)
    {
        return (new self())->Load($file);
    }

    protected function Load($file)
    {
        $this->file = (string)$file;
        $this->path = trim(dirname($this->file), DIRECTORY_SEPARATOR);
        if(OSFamily::CurrentOS() != OSFamily::Windows)
        {
            $this->path = ltrim($this->path,'/');
            $this->path = '/'.$this->path;
        }
        $moduleData = json_decode(file_get_contents($this->file));
        $this->name = $moduleData->name;
        $this->slug = \str($this->name)->slug();
        $this->protected = (bool)(isset($moduleData->protected) ? $moduleData->protected : false);
        $this->description = $moduleData->description;
        $this->keywords = $moduleData->keywords;
        $this->alias = $moduleData->alias;
        $this->priority = $moduleData->priority;
        $this->providers = $moduleData->providers;
        $this->aliases = $moduleData->aliases;
        $this->files = $moduleData->files;
        $this->requires = $moduleData->requires;
        $this->enabled = Themes::IsEnabled($this->name);
        $this->author = isset($moduleData->author) ? $moduleData->author : __('Cascade');
        $this->icon = isset($moduleData->icon) ? $moduleData->icon : 'cubes';
        $this->screenshot = file_exists($this->path.'/screenshot.png') ? $this->path.'/screenshot.png' : module_path('Site', 'Resources/assets/images/default-theme-screenshot.png');
        return $this;
    }

    public function GetThemeFile() { return $this->file; }
    public function GetPath($appends = '') { return $this->path.($appends == '' ? '' : DIRECTORY_SEPARATOR.$appends); }
    public function GetScreenshotFile() { return $this->screenshot; }
    public function GetScreenshotURL() { return route('dashboard.site.themes.screenshot', ['theme' => $this->name]); }
    public function GetName() { return $this->name; }
    public function GetSlug() { return $this->slug; }
    public function GetDescription() { return $this->description; }
    public function IsProtected() { return $this->protected; }
    public function GetKeywords() { return $this->keywords; }
    public function GetAlias() { return $this->alias; }
    public function GetPriority() { return $this->priority; }
    public function GetProviders() { return $this->providers; }
    public function GetAliases() { return $this->aliases; }
    public function GetFiles() { return $this->files; }
    public function GetRequirements() { return $this->requires; }
    public function IsEnabled() { return $this->enabled; }
    public function GetAuthor() { return $this->author; }
    public function GetIcon() { return $this->icon; }
    public function GetFriendlyName() { return str($this->GetName())->slug(); }
    public function GetURL($path = '') { return url('themes/'.$this->GetFriendlyName().($path != '' ? '/' : '').ltrim($path, '/'));  }


    public static function All()
    {
        $finder = new Finder();
        $modulesFiles = $finder->in([base_path('/Themes')])->depth(1)->name('theme.json')->files();
        $modules = [];

        foreach($modulesFiles as $file) { $modules[] = Theme::LoadFrom($file); }
        return collect($modules);
    }
}
