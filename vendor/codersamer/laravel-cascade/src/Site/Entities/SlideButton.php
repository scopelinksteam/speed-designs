<?php

namespace Cascade\Site\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Spatie\Translatable\HasTranslations;

class SlideButton extends Model
{
    use HasTranslations;

    protected $table = 'sliders_buttons';

    public $translatable = ['title'];

    public function slide()
    {
        return $this->belongsTo(SliderSlide::class, 'slide_id');
    }
}
