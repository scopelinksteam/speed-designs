<?php

namespace Cascade\Site\Entities;

use Cascade\Dashboard\Traits\HasTables;
use Cascade\Dashboard\Traits\HasTabs;
use Cascade\Site\Enums\PostCommentStatus;
use Cascade\Site\Enums\PostStatus;
use Cascade\Site\Services\PostType;
use Cascade\System\Traits\HasAttributes;
use Cascade\System\Traits\HasCategories;
use Cascade\System\Traits\HasComments;
use Cascade\System\Traits\HasEntityBuilder;
use Cascade\System\Traits\HasFilters;
use Cascade\System\Traits\HasTags;
use Cascade\Users\Entities\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Translatable\HasTranslations;

abstract class Post extends Model
{
    use SoftDeletes, HasTabs, HasTables, HasEntityBuilder,HasFilters, HasTranslations, HasComments, HasAttributes, HasCategories, HasTags;

    protected $table = 'posts';

    protected $casts = [
        'status' => PostStatus::class,
        'comment_status' => PostCommentStatus::class
    ];

    public $translatable = ['title', 'content', 'custom_excerpt', 'meta_title', 'meta_description', 'meta_keywords'];

    public $filterable = ['title', 'content', 'custom_excerpt', 'meta_title', 'meta_description', 'meta_keywords'];

    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope('type_constrain', function(Builder $builder){
            return $builder->where('type', static::class);
        });
    }

    public function getPlainContentAttribute() : String
    {
        return trim(strip_tags($this->content));
    }

    public function getExcerptAttribute() : String
    {
        return $this->custom_excerpt == null ? str($this->plain_content)->words(50) : $this->custom_excerpt;
    }

    public function getThumbnailAttribute()
    {
        if($this->thumbnail_file == null) { return null; }
        return url('uploads/'.$this->thumbnail_file);
    }

    public function getKeywordsAttribute()
    {
        $keywords = $this->meta_keywords ?? '';
        return explode(',', $keywords);
    }

    public static function configurations() : ?PostType
    {
        return PostType::for(static::class);
    }

    public function author() : BelongsTo
    {
        return $this->belongsTo(User::class, 'author_id');
    }

    public function scopePublished(Builder $query)
    {
        return $query->where('status', PostStatus::Published);
    }

    public function scopeLatest(Builder $query)
    {
        return $query->orderBy('id', 'DESC');
    }


}
