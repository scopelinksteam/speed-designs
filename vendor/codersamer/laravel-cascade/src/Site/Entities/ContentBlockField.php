<?php

namespace Cascade\Site\Entities;


class ContentBlockField
{
    /**
    * Supported Fields
    */
    const TITLE_FIELD = 'title';
    const SUBTITLE_FIELD = 'subtitle';
    const CONTENT_FIELD = 'content';
    const ICON_FIELD = 'icon';
    const LINK_FIELD = 'link';
    const IMAGE_FIELD = 'image';
    const BACKGROUND_COLOR_FIELD = 'background_color';
    const FOREGROUND_COLOR_FIELD = 'foreground_color';
    const ORDER_FIELD = 'order';
    const THEME_FIELD = 'theme';
    const LAYOUT_FIELD = 'layout';
    const CSS_CLASS_FIELD = 'css_class';
    const CSS_ID_FIELD = 'css_id';

    protected $values = [];

    protected $options = [];

    protected function __construct($type)
    {
        $this->type($type);
    }

    public static function make($type)
    {
        return new ContentBlockField($type);
    }

    public function type($type) { $this->type = $type; return $this; }
    public function values($values) { $this->values = $values; return $this;  }
    public function options($options) { $this->options = $options; return $this; }
    public function option($optionKey, $optionValue)
    {
        $this->options = is_array($this->options) ? $this->options : [];
        $this->options[$optionKey] = $optionValue;
        return $this;
    }

    public function required($isRequired = true) { return $this->option('required', $isRequired); }
    public function editor($isEditor = true) { return $this->option('editor', $isEditor); }
    public function select2($isSelect2 = true) { return $this->option('select2', $isSelect2); }
    public function dropify($isDropify = true) { return $this->option('dropify', $isDropify); }

    public function GetType() { return $this->type; }

    public function GetValues() { return $this->values; }

    public function GetOptions() { return $this->options; }

    public function GetOption($optionKey, $defaultValue = null)
    {
        if(!is_array($this->GetOptions())) { return $defaultValue; }
        if(!isset($this->GetOptions()[$optionKey])) { return $defaultValue; }
        return $this->GetOptions()[$optionKey];
    }

    /**
     * Check if Field Required
     *
     * @param boolean $defaultValue
     * @return boolean
     */
    public function IsRequired($defaultValue = false)
    {
        return $this->GetOption('required', $defaultValue);
    }

}
