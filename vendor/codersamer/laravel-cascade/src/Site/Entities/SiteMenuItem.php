<?php

namespace Cascade\Site\Entities;

use Cascade\Site\Entities\SiteMenu;
use Cascade\Site\Services\MenuBuilder;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class SiteMenuItem extends Model
{
    protected $table = 'site_menu_items';

    public function menu()
    {
        return $this->belongsTo(SiteMenu::class, 'menu_id');
    }


    public function children()
    {
        return $this->hasMany(SiteMenuItem::class, 'parent_id');
    }

    public function getBuilderAttribute()
    {
        $builders = MenuBuilder::Builders();
        foreach($builders as $single)
        {
            if($single->id == $this->type) { return $single; }
        }
        return null;
    }

    public function getRendererAttribute()
    {
        $builder = $this->builder;
        return $builder ? $builder->instance->Renderer($this->data) : null;
    }
    public function BuildItemAttributes($merge = [])
    {
        $renderer = $this->renderer;
        return $renderer ? $renderer->BuildItemAttributes($merge) : '';
    }

    public function getIsActiveAttribute()
    {
        return trim(url()->current(),'/') == trim($this->link, '/');
    }


    public function BuildLinkAttributes($merge = [])
    {
        $renderer = $this->renderer;
        return $renderer ? $renderer->BuildLinkAttributes($merge) : '';
    }

    public function getLinkAttribute()
    {
        $renderer = $this->renderer;
        return $renderer ? $renderer->GetLink() : '';
    }

    public function getTitleAttribute()
    {
        $renderer = $this->renderer;
        return $renderer ? $renderer->GetTitle() : '';
    }


    public function getDataAttribute()
    {
        return unserialize($this->attributes['data']);
    }


    public function GetData()
    {
        return unserialize($this->data);
    }
}
