<?php

namespace Cascade\Site\Entities;

use Cascade\System\Traits\HasAttributes;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Spatie\Translatable\HasTranslations;

class ContentBlock extends Model
{

    use HasTranslations, HasAttributes;

    protected static $types = [];

    protected $table = 'content_blocks';

    public $translatable = ['title', 'subtitle',  'content', 'link_text'];

    public function children()
    {
        return $this->hasMany(ContentBlock::class, 'parent_id');
    }

    public function scopeType(Builder $query, $type)
    {
        return $query->where('block_type', $type);
    }

    public function scopeSingle(Builder $query)
    {
        $types = ContentBlock::GetTypes();
        $singleTypes = [];
        foreach($types as $type) {
            if($type->IsSingle()) { $singleTypes[] = $type->GetType(); }
        }
        $query = $query->whereIn('block_type', $singleTypes);

        return $query;
    }

    public function scopePublished(Builder $query)
    {
        return $query->where('status', 'published');
    }

    public function scopeDraft(Builder $query)
    {
        return $query->where('status', 'draft');
    }

    public static function RegisterType(ContentBlockType $type)
    {
        self::$types[$type->GetType()] = $type;
    }

    public static function GetTypes()
    {
        return self::$types;
    }

    public static function Of($type)
    {
        return self::$types[$type] ?? null;
    }

    public function getHasImageAttribute()
    {
        return $this->image_url != null;
    }

    public function getHasLinkAttribute()
    {
        return $this->link != null && $this->link != '#';
    }
    /**
     * Undocumented function
     *
     * @param String $type
     * @return ContentBlockType
     */
    public static function GetType($type)
    {
        return isset(self::$types[$type]) ? self::$types[$type] : null;
    }

    /**
     * Render Block
     *
     * @return void
     */
    public function render()
    {
        $type = $this->GetType($this->block_type);
        if(empty($type->GetView())) { return; }
        echo  view($type->GetView(), [$type->GetPassToViewAs() => $this])->render();
    }

}
