<?php

namespace Cascade\Site\Entities;

use Cascade\System\Traits\HasAttributes;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class ContactMessage extends Model
{

    use HasAttributes;
    
    protected $table = 'contact_messages';


}
