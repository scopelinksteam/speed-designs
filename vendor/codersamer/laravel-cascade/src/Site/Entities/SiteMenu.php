<?php

namespace Cascade\Site\Entities;

use Illuminate\Database\Eloquent\Model;
use Cascade\Site\Entities\SiteMenuItem;


class SiteMenu extends Model
{
    protected $table = 'site_menus';

    public function children()
    {
        return $this->hasMany(SiteMenuItem::class, 'menu_id')->where('parent_id', 0)->orderBy('order', 'ASC');
    }

    public function items()
    {
        return $this->hasMany(SiteMenuItem::class, 'menu_id');
    }
}
