<?php

namespace Cascade\Site\Entities;

use Cascade\Site\DTO\WidgetArea;
use Cascade\Site\Services\Widgets;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Support\Facades\Blade;

class WidgetOption extends Model
{
    protected $table = 'widgets_options';

    protected $casts = ['options' => 'array'];

    public function getAreaAttribute()
    {
        return WidgetArea::find($this->area_id);
    }

    public function getDataAttribute()
    {
        return Widgets::find($this->widget_id);
    }
    public function getHandlerAttribute()
    {
        $handler = Widgets::find($this->widget_id);
        if($handler != null)
        {
            $handlerClass = $handler->class;
            return new $handlerClass($this->options);
        }
        return null;
    }

    final public function renderDesigner()
    {
        $widget = Widgets::find($this->widget_id);
        $widgetClass = $widget->class;
        $widgetObject = new $widgetClass;
        $widgetObject->designer($this->options);
        return view('site::dashboard.widgets.designer', ['data' => $widget, 'widget' => $widgetObject, 'options' => $this]);
    }

    public function title(?String $title)
    {
        if($title == null) { return; }
        return $this->area->beforeTitle().$title.$this->area->afterTitle();
    }

    public function content(?String $content)
    {
        if($content == null) { return; }
        return $this->area->before().$content.$this->area->after();
    }

    public function renderTitle(?String $title)
    {
        if($title == null) { return; }
        return $this->beforeTitle.$title.$this->afterTitle;
    }

}
