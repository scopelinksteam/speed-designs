<?php

namespace Cascade\Site\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Spatie\Translatable\HasTranslations;

class Slider extends Model
{

    protected $table = 'sliders';


    public function slides()
    {
        return $this->hasMany(SliderSlide::class, 'slider_id');
    }
}
