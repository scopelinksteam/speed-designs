<?php

namespace Cascade\Site\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Spatie\Translatable\HasTranslations;

class SliderSlide extends Model
{
    use HasTranslations;

    protected $table = 'sliders_slides';

    public $translatable = ['title', 'subtitle', 'description', 'image'];

    public function buttons()
    {
        return $this->hasMany(SlideButton::class, 'slide_id');
    }
}
