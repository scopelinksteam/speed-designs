<?php

namespace Cascade\Site\Settings;

use Cascade\Settings\Abstractions\SettingPage;
use Cascade\Site\Tables\GeneralContentBlocksTable;
use Illuminate\Contracts\View\View;
use Illuminate\Contracts\View\Factory;

class ThemeContentSettingPage extends SettingPage
{

    function header(): string
    {
        return __('Custom Static Content');
    }
    public function view(): View|Factory
    {
        return view('site::dashboard.themes.contents', [
            'table' => GeneralContentBlocksTable::class
        ]);
    }
}
