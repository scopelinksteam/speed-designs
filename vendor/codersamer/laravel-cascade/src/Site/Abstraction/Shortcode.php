<?php

namespace Cascade\Site\Abstraction;

abstract class Shortcode
{

    public function __construct(protected String $tag, protected array $attribute, protected String $content)
    { }

    protected function tag() : String { return $this->tag; }
    protected function attributes() : array { return $this->attribute; }
    protected function content() : String { return $this->content; }

    //Abstractions
    public abstract function handle() : String|\Illuminate\View\View;
}
