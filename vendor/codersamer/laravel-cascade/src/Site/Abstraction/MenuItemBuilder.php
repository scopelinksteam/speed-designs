<?php

namespace Cascade\Site\Abstraction;

use Cascade\System\Services\HTMLBuilder;
use Cascade\System\Traits\HasHTMLBuilder;

abstract class MenuItemBuilder
{
    use HasHTMLBuilder;

    protected HTMLBuilder $designer;

    protected HTMLBuilder $editor;

    public function __construct()
    {
        $this->designer = new HTMLBuilder;
        $this->editor = new HTMLBuilder;
    }

    abstract function Renderer(array $options) : MenuItemRenderer;
    abstract function Designer() : HTMLBuilder;
    abstract function Editor(array $options) : HTMLBuilder;
    abstract function Handle(array $oldOptions, $newOptions) : array;
}
