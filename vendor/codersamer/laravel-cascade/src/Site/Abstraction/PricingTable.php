<?php

namespace Cascade\Site\Abstraction;

abstract class PricingTable
{
    public abstract function name() : String;
    public abstract function slug() : String;

    /**
     * Get Items List
     * @return PricingTableItem[]
     */
    public abstract function items() : array;
}