<?php

namespace Cascade\Site\Abstraction;

use Cascade\System\Services\HTMLBuilder;
use Cascade\System\Traits\HasHTMLBuilder;

abstract class Widget
{

    use HasHTMLBuilder;


    public function builder() : HTMLBuilder
    { return $this->html(); }

    abstract function designer(array $options);


    abstract function handle(array $oldOptions, $newOptions) : array;

    abstract function render(array $options);
}
