<?php

namespace Cascade\Site\Abstraction;

abstract class PricingTableItem
{
    public abstract function name() : String;
    public abstract function id() : int;
    public abstract function price() : float;
    public abstract function lines() : array;
    public function description() { }
    public function image() { }
    public function cycleText() { }
    public function priceDescription() { }
    public function currencyText() {}
    public function link() { }
    public function linkText() { }
}