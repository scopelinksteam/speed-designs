<?php

namespace Cascade\Site\Abstraction;

use Cascade\System\Traits\HasHTMLBuilder;

abstract class MenuItemRenderer
{
    use HasHTMLBuilder;

    public function __construct(protected $options)
    {

    }

    abstract function GetTitle();
    abstract function GetLink();
    abstract function GetAttributes();

    public function GetAttribute($name, $default = '')
    {
        $attributes = $this->GetAttributes();
        if(!is_array($attributes)) { return $default; }
        foreach($attributes as $key => $value)
        {
            if(strtolower($key) == $name)
            { return is_array($value) ? implode(' ', $value) : $value; }
        }
        return $default;
    }
    public function GetClassAttribute() { return $this->GetAttribute('class', ''); }

    public function GetIdAttribute() { return $this->GetAttribute('id', ''); }

    public function BuildLinkAttributes($merge = [])
    {
        $merge = is_array($merge) ? $merge : [];
        $attributes = $this->GetAttributes() ?? [];
        $items = [];
        $mergedKeys = [];
        foreach($attributes as $key => $value)
        {
            //Skip Item Attributes
            if(substr($key, 0, '5') == 'item_') { continue; }
            if(substr($key, 0, '5') == 'link_') { $key = str_replace('link_','', $key); }
            $value = is_array($value) ? implode(' ', $value) : $value;
            $mergeableValue = isset($merge[$key]) ? $merge[$key] : null;
            if($mergeableValue != null)
            {
                $mergedKeys[] = $key;
                $mergeableValue = is_array($mergeableValue) ? implode(' ', $mergeableValue) : $mergeableValue;
                $value .= ' '.$mergeableValue;
            }
            $items[] = $key.'="'.$value.'"';
        }
        foreach($merge as $key => $value)
        {
            if(in_array($key, $mergedKeys)) { continue; }
            $value = is_array($value) ? implode(' ', $value) : $value;
            $items[] = $key.'="'.$value.'"';
        }
        return implode(' ', $items);
    }

    public function BuildItemAttributes($merge = [])
    {
        $merge = is_array($merge) ? $merge : [];
        $attributes = $this->GetAttributes() ?? [];
        $items = [];
        $mergedKeys = [];
        foreach($attributes as $key => $value)
        {
            //Skip Item Attributes
            if(substr($key, 0, '5') == 'link_') { continue; }
            if(substr($key, 0, '5') == 'item_') { $key = str_replace('item_','', $key); }
            $value = is_array($value) ? implode(' ', $value) : $value;
            $mergeableValue = isset($merge[$key]) ? $merge[$key] : null;

            if($mergeableValue != null)
            {
                $mergedKeys[] = $key;
                $mergeableValue = is_array($mergeableValue) ? implode(' ', $mergeableValue) : $mergeableValue;
                $value .= ' '.$mergeableValue;
            }
            $items[] = $key.'="'.$value.'"';
        }

        foreach($merge as $key => $value)
        {
            if(in_array($key, $mergedKeys)) { continue; }
            $value = is_array($value) ? implode(' ', $value) : $value;
            $items[] = $key.'="'.$value.'"';
        }
        return implode(' ', $items);
    }
}
