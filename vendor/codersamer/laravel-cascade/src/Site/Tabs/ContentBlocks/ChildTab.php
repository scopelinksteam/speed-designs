<?php

namespace Cascade\Site\Tabs\ContentBlocks;

use Cascade\Dashboard\Abstraction\DashboardTab;
use Cascade\Site\Entities\ContentBlock;
use Cascade\Site\Entities\ContentBlockType;
use Cascade\System\Services\System;
use Illuminate\View\View;

class ChildTab extends DashboardTab
{

    protected ?ContentBlockType $child  = null;

    protected ?ContentBlockType $parent = null; 

    public function __construct(protected $childName) { }

    protected function setType()
    {
        if($this->child != null) { return; }
        if(isset($this->data['type']))
        {
            $this->parent = $this->data['type'];
            if($this->parent instanceof ContentBlockType)
            {
                foreach($this->parent->GetChildren() as $child)
                {
                    if($child instanceof ContentBlockType)
                    {
                        if($child->GetType() == $this->childName)
                        {
                            $this->child = $child;
                            break;
                        }
                    }
                }
            }
        }
    }
    public function name(): string
    {
        $this->setType();
        return $this->child ? $this->child->GetName() : '';
    }

    public function slug(): string
    {
        $this->setType();
        return $this->child ? $this->child->GetType() : '';
    }

    public function show(): bool
    {
        $this->setType();
        return $this->child != null;
    }

    public function view(): View|string
    {
        $this->setType();
        return System::FindView('site::dashboard.blocks.tabs.child', [
            'child' => $this->child,
            'child_records' => isset($this->data['block']) ? $this->data['block']->children()->where('block_type', $this->childName) : collect()
        ]);
    }
}