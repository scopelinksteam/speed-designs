<?php

namespace Cascade\Site\Tabs\Posts;

use Cascade\Dashboard\Abstraction\DashboardTab;
use Cascade\System\Services\System;
use Illuminate\View\View;

class PostBasicTab extends DashboardTab
{
    public function name(): string
    {
        return __('Basic');
    }

    public function slug(): string
    {
        return 'basic';
    }

    public function view(): View|string
    {
        return System::FindView('site::dashboard.posts.tabs.basic');
    }
}
