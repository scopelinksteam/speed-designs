<?php

namespace Cascade\Site\Shortcodes;

use Cascade\Site\Abstraction\Shortcode;

class BoldShortcode extends Shortcode
{
    public function handle(): string
    {
        return "<b>{$this->content()}</b>";
    }
}
