<?php

use Cascade\Site\Entities\Page;
use Cascade\Site\Http\Controllers\Frontend\PagesController;
use Cascade\Site\Services\PostType;
use Illuminate\Support\Facades\Route;

Route::dashboard(function(){
    PostType::of(Page::class)->registerRoutes();
}, 'site');


Route::Api(function(){
    PostType::of(Page::class)->registerAPIRoutes();
}, 'site');

Route::get('/page/{page:slug}', [PagesController::class, 'page'])->name('page.show');
