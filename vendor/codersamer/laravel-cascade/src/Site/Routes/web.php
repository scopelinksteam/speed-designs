<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;
use Cascade\Site\Http\Controllers\ContactFormController;
use Cascade\Site\Http\Controllers\Dashboard\ContactMessagesController;
use Cascade\Site\Http\Controllers\Dashboard\ContentBlocksController;
use Cascade\Site\Http\Controllers\Dashboard\SlidersController;
use Cascade\Site\Http\Controllers\Dashboard\ThemesController as DashboardThemesController;
use Cascade\Site\Http\Controllers\Dashboard\WidgetsController;
use Cascade\Site\Http\Controllers\ThemesController as FrontendThemesController;
use Cascade\Site\Services\PostType;
use Cascade\Site\Services\Site;
use Cascade\System\Services\System;
use Cascade\Site\Http\Controllers\Dashboard\MenuBuilderController;
use Cascade\Site\Services\Shortcodes;

Route::get('/', function () { Site::Title(__('Home')); return System::FindView('welcome'); })->name('index');
Route::get('/site/themes/assets/{asset?}', [FrontendThemesController::class, 'assets'])
->where('asset', '(.*)')
->name('site.themes.assets');
Route::get('/contact', function () { return System::FindView('contact'); })->name('contact');
Route::post('/contact', [ContactFormController::class, 'store'])->name('contact.store');

Route::dashboard(function(){
    //Content Blocks
    Route::prefix('content_blocks')->as('content_blocks.')->group(function(){
        Route::get('/{type}', [ContentBlocksController::class, 'index'])->name('index')->middleware('can:view content blocks');
        Route::get('/{type}/create', [ContentBlocksController::class, 'create'])->name('create')->middleware('can:create content blocks');
        Route::get('/{type}/edit/{block}', [ContentBlocksController::class, 'edit'])->name('edit')->middleware('can:edit content blocks');
        Route::post('/{type}/', [ContentBlocksController::class, 'store'])->name('store');
        Route::get('/{type}/delete/{block}', [ContentBlocksController::class, 'destroy'])->name('destroy')->middleware('can:delete content blocks');
    });

    //Menu Builder
    Route::prefix('menus')->as('menus.')->group(function(){
        Route::get('/', [MenuBuilderController::class, 'index'])->name('index')->middleware('can:build menus');
        Route::post('/', [MenuBuilderController::class, 'store'])->name('store')->middleware('can:build menus');
        Route::get('/edit/{menu}', [MenuBuilderController::class, 'edit'])->name('edit')->middleware('can:build menus');
        Route::get('/delete/{menu}', [MenuBuilderController::class, 'destroy'])->name('delete')->middleware('can:build menus');
        Route::get('/customize/{menu}', [MenuBuilderController::class, 'customize'])->name('customize')->middleware('can:build menus');
    });

    Route::prefix('messages')->as('messages.')->group(function(){
        Route::get('/', [ContactMessagesController::class, 'index'])->name('index')->middleware('can:view contact messages');
        Route::get('/show/{message}', [ContactMessagesController::class, 'show'])->name('show')->middleware('can:view contact messages');
        Route::get('/delete/{message}', [ContactMessagesController::class, 'destroy'])->name('destroy')->middleware('can:view contact messages');
    });

    Route::prefix('widgets')->as('widgets.')->group(function(){
        Route::get('/', [WidgetsController::class, 'index'])->name('index')->middleware('can:manage widgets');
        Route::post('/designer/{widget}', [WidgetsController::class, 'designer'])->name('designer')->middleware('can:edit widgets');
        Route::post('/designer', [WidgetsController::class, 'storeDesigner'])->name('designer.store')->middleware('can:edit widgets');
        Route::get('/create/{widget}/{location}', [WidgetsController::class, 'create'])->name('create')->middleware('can:add widgets');
        Route::post('/move/{instance}/{location}', [WidgetsController::class, 'move'])->name('move')->middleware('can:add widgets');
        Route::get('/delete/{instance}', [WidgetsController::class, 'delete'])->name('delete')->middleware('can:delete widgets');
    });

    if(themes_manager_enabled())
    {
        //Themes
        Route::prefix('themes')->as('themes.')->group(function(){
            Route::get('/', [DashboardThemesController::class, 'index'])->name('index')->middleware('can:view themes');
            Route::get('/customize', [DashboardThemesController::class, 'customizer'])->name('customizer')->middleware('can:customize themes');
            Route::get('/screenshot/{theme}', [DashboardThemesController::class, 'screenshot'])->name('screenshot');
            Route::get('/enable/{theme}', [DashboardThemesController::class, 'enable'])->name('enable')->middleware('can:enable themes');
            Route::post('/install', [DashboardThemesController::class, 'PostInstall'])->name('install')->middleware('can:install themes');
        });
    }
    if(sliders_manager_enabled())
    {
        //Sliders
        Route::prefix('sliders')->as('sliders.')->group(function(){
            Route::get('/', [SlidersController::class, 'index'])->name('index');
            Route::get('/create', [SlidersController::class, 'create'])->name('create')->middleware('can:create sliders');
            Route::get('/edit/{slider}', [SlidersController::class, 'edit'])->name('edit')->middleware('can:edit sliders');
            Route::get('/customize/{slider}', [SlidersController::class, 'customize'])->name('customize')->middleware('can:customize sliders');
            Route::get('/delete/{slider}', [SlidersController::class, 'destroy'])->name('destroy')->middleware('can:delete sliders');
            Route::post('/', [SlidersController::class, 'store'])->name('store');
            Route::post('/slides', [SlidersController::class, 'storeSlides'])->name('slides.store')->middleware('can:customize sliders');
        });
    }
    PostType::routes();
}, 'site');

