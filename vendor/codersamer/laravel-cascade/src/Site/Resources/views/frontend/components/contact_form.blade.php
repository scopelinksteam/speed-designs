<div class="{{empty($wrapperClass) ? 'contact-form' : $wrapperClass}}">
    <form action="{{route('contact.store')}}" method="POST" id="{{$formId}}" class="{{$formClass}}">
        <div class="row">
            @stack('before::inputs')
            @stack('before::inputs.name')
            <div class="col-md-{{12/$columns}}">
                <div class="form-group">
                    @if($showLabels)
                    <label for="name">@lang('Name')</label>
                    @endif
                    <input type="text" name="name" id="name" class="{{empty($inputClass) ? 'form-control' : $inputClass}}" placeholder="@lang('Name')" required/>
                </div>
            </div>
            @stack('before::inputs.email')
            <div class="col-md-{{12/$columns}}">
                <div class="form-group">
                    @if($showLabels)
                    <label for="email">@lang('Email')</label>
                    @endif
                    <input type="email" name="email" id="email" class="{{empty($inputClass) ? 'form-control' : $inputClass}}" placeholder="@lang('Email')" required/>
                </div>
            </div>
            @stack('before::inputs.phone')
            <div class="col-md-{{12/$columns}}">
                <div class="form-group">
                    @if($showLabels)
                    <label for="phone">@lang('Phone')</label>
                    @endif
                    <input type="tel" name="phone" id="phone" class="{{empty($inputClass) ? 'form-control' : $inputClass}}" placeholder="@lang('Phone')" required/>
                </div>
            </div>
            @stack('before::inputs.subject')
            <div class="col-md-{{12/$columns}}">
                <div class="form-group">
                    @if($showLabels)
                    <label for="subject">@lang('Subject')</label>
                    @endif
                    <input type="text" name="subject" id="subject" class="{{empty($inputClass) ? 'form-control' : $inputClass}}" placeholder="@lang('Subject')" required/>
                </div>
            </div>
            @stack('before::inputs.message')
            <div class="col-md-12">
                <div class="form-group">
                    @if($showLabels)
                    <label for="message">@lang('Message')</label>
                    @endif
                    <textarea class="{{empty($messageClass) ? 'form-control' : $messageClass}}" rows="10" name="message" id="message" placeholder="@lang('Message')"></textarea>
                </div>
            </div>
            @stack('after::inputs')
            <div class="col-md-12">
                @csrf
                <x-recaptcha />
                <button type="submit" class="{{empty($submitClass) ? 'btn btn-primary btn-block' : $submitClass}}">@lang('Send Message')</button>
            </div>
        </div>

    </form>
</div>
