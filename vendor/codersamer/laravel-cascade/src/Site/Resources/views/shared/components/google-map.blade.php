<iframe
    width="100%" height="100%" class="gmap_canvas"
    src="https://maps.google.com/maps?{{ $latitude && $langitude ? 'll='.$latitude.','.$langitude.'&' : ''}}q={{ $address }}&t=&z=18&ie=UTF8&iwloc=&output=embed"
    frameborder="0" scrolling="no" marginheight="0" marginwidth="0"
>
</iframe>
