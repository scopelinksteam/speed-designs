@extends('admin::layouts.master')
@section('page-title', __('Site Menus'))
@section('page-description', __('Manage and Create Menus for your Site'))
@section('content')
<div class="row">
    <div class="col-md-4">
        <div class="card">
            <form action="{{ route('dashboard.site.menus.store') }}" method="POST">
                <div class="card-header">
                    <div class="card-title">
                        <i class="fa fa-list mr-2"></i>
                        @lang('Menu Details')
                    </div>
                </div>
                <div class="card-body">
                    <div class="form-group">
                        <label for="name">@lang('Name')</label>
                        <input type="text" name="name" id="name" class="form-control" @isset($entity) value="{{$entity->name}}" @endisset>
                    </div>
                </div>
                <div class="card-footer">
                    @csrf
                    @isset($entity)
                    <input type="hidden" name="entity_id" value="{{ $entity->id }}">
                    @endisset
                    <input type="submit" value="@lang('Save Menu')" class="btn btn-primary">
                </div>
            </form>
        </div>
    </div>
    <div class="col-md-8">
        <div class="card">
            <div class="card-header">
                <div class="card-title">
                    <i class="fa fa-list mr-2"></i>
                    @lang('Available Menus')
                </div>
            </div>
            <div class="card-body">
                @livewire('datatable', ['table' => $table])
            </div>
        </div>
    </div>
</div>
@endsection
