@extends('admin::layouts.master')
@section('page-title', __('Customize Slider').' : '. $slider->name)
@section('page-description', __('Manage Slider Slides and Customize its Look and Feel'))
@section('content')
<form action="{{ route('dashboard.site.sliders.slides.store') }}" method="POST" enctype="multipart/form-data">
    @csrf
    <input type="hidden" name="entity_id" value="{{ $slider->id }}" />
    <div class="row ui-sortable connectedSortable " x-data="slidesComponent()">
        <div class="col-12 mb-3">
            <div class="row justify-content-between">
                <div class="col-auto">
                    <button x-on:click.prevent="addSlide()" class="btn btn-success">New Slide</button>
                </div>
                <div class="col-auto">
                    <button type="submit" class="btn btn-primary">@lang('Save Slider')</button>
                </div>
            </div>
        </div>
        <template x-for="(slide, i) in slides" :key="slide.id">
            <div class="col-12">
                <div class="card">
                    <div class="card-header ui-sortable-handle" >
                        <div class="row justify-content-between align-items-center">
                            <div class="card-title col-auto">
                                <i class="fa fa-image"></i>
                                <span x-text="'@lang('Slide') #' + (i+1)"></span>
                            </div>
                            <div class="col-auto d-flex-inline">
                                <button type="button" class="btn btn-light" data-bs-toggle="collapse" x-bind:data-bs-target="'#slide-content-' + slide.id">
                                    <i class="fas fa-minus"></i>
                                </button>
                                <button type="button" x-on:click="removeSlide(slide.id)" class="btn btn-light">
                                    <i class="fas fa-backspace"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="card-body collapse" x-bind:id="'slide-content-'+slide.id">
                        <div class="row">
                            <div class="col-md-6">

                                <ul class="nav nav-tabs mb-3">
                                @foreach(get_active_languages() as $language)
                                    <li class="nav-item">
                                        <a x-bind:data-bs-target="'#slide-'+ (i+1) +'-content-{{ $language->locale }}'" data-bs-toggle="tab" class="nav-link @if($loop->iteration == 1) active @endif">{{ $language->name }}</a>
                                    </li>
                                @endforeach
                                </ul>
                                <div class="tab-content">
                                    @foreach(get_active_languages() as $language)
                                    <div class="tab-pane fade @if($loop->iteration == 1) active show @endif" x-bind:id="'slide-'+(i+1)+'-content-{{ $language->locale }}'">
                                        <div class="form-group">
                                            <span x-text="slide.image"></span>
                                            <input type="file" x-bind:name="'slides['+slide.id+'][image][{{ $language->locale }}]'" x-bind:data-default-file="slide.image_url_{{ $language->locale }}" class="dropify-image"/>
                                            <input type="hidden" x-bind:name="'slides['+slide.id+'][default_image][{{ $language->locale }}]'" x-model="slide.image_{{ $language->locale }}"/>
                                        </div>
                                        <div class="form-group">
                                            <label for="">@lang('Title')</label>
                                            <input type="text" x-bind:name="'slides['+(slide.id)+'][title][{{ $language->locale }}]'" x-bind:value="slide.title_{{ $language->locale }}" id="" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label for="">@lang('Subtitle')</label>
                                            <input type="text" x-bind:name="'slides['+(slide.id)+'][subtitle][{{ $language->locale }}]'" x-bind:value="slide.subtitle_{{ $language->locale }}" id="" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label for="">@lang('Description')</label>
                                            <textarea class="form-control" x-bind:name="'slides['+(slide.id)+'][description][{{ $language->locale }}]'" x-html="slide.description_{{ $language->locale }}" id="" cols="30" rows="10"></textarea>
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="row mb-3">
                                    <div class="col-8">
                                        <h5>@lang('Slide Buttons')</h5>
                                    </div>
                                    <div class="col-4">
                                        <button x-on:click.prevent="slide.addButton()" class="btn btn-dark float-right">@lang('Add Button')</button>
                                    </div>
                                    <div class="col-12 mt-2">
                                        <div class="callout callout-info">
                                            <h5>@lang('How it works')</h5>
                                            <p>@lang('Leaving Button Title Empty in all Languages will indicate that it will not be saved')</p>
                                        </div>
                                    </div>
                                </div>
                                <div id="buttons-group">
                                    <template x-for="(button, bI) in slide.buttons" :key="button.id">
                                        <div class="card" >
                                            <div class="card-header">
                                                <div class="row justify-content-between align-items-center">
                                                    <div class="col-auto">
                                                        <div class="card-title">
                                                            <i class="fa fa-mouse"></i>
                                                            <span x-text="'@lang('Button') #'+(bI+1)"></span>
                                                        </div>
                                                    </div>
                                                    <div class="col-auto">
                                                        <div class="d-flex-inline">
                                                            <button type="button" class="btn btn-light" data-bs-toggle="collapse" x-bind:data-bs-target="'#button-'+bI">
                                                                <i class="fas fa-minus"></i>
                                                            </button>
                                                            <button type="button" x-on:click="slide.removeButton(button.id)" class="btn btn-light">
                                                                <i class="fas fa-backspace"></i>
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="card-body collapse" data-parent="#buttons-group" x-bind:id="'button-'+bI">
                                                <ul class="nav nav-tabs mb-2">
                                                @foreach(get_active_languages() as $language)
                                                    <li class="nav-item">
                                                        <a class="nav-link @if($loop->iteration == 1) active @endif" x-bind:data-bs-target="'#slide-'+(i)+'-buttons-'+(bI)+'-{{ $language->locale }}'" data-bs-toggle="tab">{{ $language->name }}</a>
                                                    </li>
                                                @endforeach
                                                </ul>
                                                <div class="tab-content mb-2">
                                                    @foreach(get_active_languages() as $language)
                                                    <div class="tab-pane fade @if($loop->iteration == 1) active show @endif" x-bind:id="'slide-'+(i)+'-buttons-'+(bI)+'-{{ $language->locale }}'">
                                                        <div class="form-group">
                                                            <label for="">@lang('Title')</label>
                                                            <input type="text" x-bind:value="button.title_{{ $language->locale }}" x-bind:name="'slides['+(slide.id)+'][buttons]['+(button.id)+'][title][{{ $language->locale }}]'" id="" class="form-control">
                                                        </div>
                                                    </div>
                                                    @endforeach
                                                </div>
                                                <div class="row">
                                                    <div class="col-8">
                                                        <div class="form-group">
                                                            <label for="">@lang('Link')</label>
                                                            <input type="text" x-bind:value="button.link" x-bind:name="'slides['+slide.id+'][buttons]['+button.id+'][link]'" class="form-control" id="">
                                                        </div>
                                                    </div>
                                                    <div class="col-4">
                                                        <div class="form-group">
                                                            <label for="">@lang('Target')</label>
                                                            <select x-model="button.target" x-bind:name="'slides['+slide.id+'][buttons]['+button.id+'][target]'" class="form-control" id="">
                                                                <option value="_self">@lang('Same Page')</option>
                                                                <option value="_blank">@lang('New Tab')</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-6">
                                                        <label for="">@lang('Theme')</label>
                                                        <select x-model="button.theme" x-bind:name="'slides['+slide.id+'][buttons]['+button.id+'][theme]'" id="" class="form-control">
                                                            <option value="primary" class="bg-primary">Primary</option>
                                                            <option value="default" class="bg-default">Default</option>
                                                            <option value="success" class="bg-success">Success</option>
                                                            <option value="warning" class="bg-warning">Warning</option>
                                                            <option value="danger" class="bg-danger">Danger</option>
                                                            <option value="secondary" class="bg-secondary">Secondary</option>
                                                            <option value="light" class="bg-light">Light</option>
                                                            <option value="dark" class="bg-dark">Dark</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-6">
                                                        <label for="">@lang('Layout')</label>
                                                        <select x-model="button.layout" x-bind:name="'slides['+slide.id+'][buttons]['+button.id+'][layout]'" id="" class="form-control">
                                                            <option value="default">@lang('Default')</option>
                                                            <option value="flat">@lang('Flat')</option>
                                                            <option value="rounded">@lang('Rounded')</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </template>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </template>
    </div> <!-- /.row -->
</form>
@endsection
@push('scripts')
<script type="text/javascript">
    function slidesComponent()
    {
        let counter = 1;
        let buttonsCounter = 1;

        class SlideElement {
            constructor() {
                this.buttons = [];
                this.id = counter;
                counter++;
                @foreach(get_active_languages() as $language)

                this.title_{{ $language->locale }} = '';
                this.subtitle_{{ $language->locale }} = '';
                this.description_{{ $language->locale }} = '';
                this.image_{{ $language->locale }} = '';
                @endforeach


            }
            addButton() {
                this.buttons.push(new SlideButtonElement)
            }
            removeButton(id) {
                this.buttons = this.buttons.filter(item => item.id != id);
            }
        };

        class SlideButtonElement {
            constructor() {
                this.id = buttonsCounter; buttonsCounter++;
                @foreach(get_active_languages() as $language)
                this.title_{{ $language->locale }} = '';
                @endforeach
                this.target = '_self';
                this.link = '#';
                this.layout = 'default';
                this.theme = 'primary';
            }
        }
        var existSlides = [];
        @isset($slider)
        @foreach($slider->slides as $slide)

        var slide{{ $slide->id }} = new SlideElement;

            @foreach(get_active_languages() as $language)
            slide{{ $slide->id }}.title_{{ $language->locale }} = '{{ $slide->translate('title', $language->locale) }}';
            slide{{ $slide->id }}.subtitle_{{ $language->locale }} = '{{ $slide->translate('subtitle', $language->locale) }}';
            slide{{ $slide->id }}.description_{{ $language->locale }} = '{{ $slide->translate('description', $language->locale) }}';
            slide{{ $slide->id }}.image_url_{{ $language->locale }} = '{{ $slide->translate('image', $language->locale) == null ? '' : uploads_url($slide->translate('image', $language->locale))  }}';
            slide{{ $slide->id }}.image_{{ $language->locale }} = '{{ $slide->translate('image', $language->locale) ?? ''}}';
            @endforeach
            @foreach($slide->buttons as $button)
            var slideButton{{ $button->id }} = new SlideButtonElement;
                @foreach(get_active_languages() as $language)
                slideButton{{ $button->id }}.title_{{ $language->locale }} = '{{ $button->translate('title', $language->locale) }}';
                @endforeach
                slideButton{{ $button->id }}.link = '{{ $button->link }}';
                slideButton{{ $button->id }}.target = '{{ $button->target }}';
                slideButton{{ $button->id }}.layout = '{{ $button->layout }}';
                slideButton{{ $button->id }}.theme = '{{ $button->theme }}';

                slide{{ $slide->id }}.buttons.push(slideButton{{ $button->id }});
            @endforeach

        existSlides.push(slide{{ $slide->id }});

        @endforeach
        @endisset

        return {
            slides : existSlides,
            addSlide: function(){
                this.slides.push(new SlideElement);
                setTimeout(() => {
                    $('.dropify-image').dropify({});
                }, 500);
            },
            removeSlide: function(id) {
                this.slides = this.slides.filter(item => item.id != id);
            }
        }
    }
</script>
@endpush
