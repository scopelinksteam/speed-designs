<div class="row">
    <x-entity-cards :type="$type->type()" action="upsert" :placement="['start']" :params="['post' => $post ?? null, 'type' => $type]" />
</div>
<div class="row">
<div class="col-8">
    @stack('primary::start')
    <x-entity-cards :type="$type->type()" action="upsert" :placement="['primary-start']" :params="['post' => $post ?? null, 'type' => $type]" />
    @if($type->supportsAny('content', 'title'))
    @stack('card::content.before')
    <div class="card" id="content-card">
        <div class="card-header">
            <div class="card-title">
                <i class="fa fa-info mr-2"></i>
                @lang('Basic Content')
            </div>
        </div>
        <div class="card-body">
            @stack('card::content.start')
            <ul class="nav nav-tabs mb-4">
                @foreach(get_active_languages() as $language)
                <li class="nav-item">
                    <a data-bs-target="#content-tab-{{ $language->locale }}" data-bs-toggle="tab" class="nav-link @if($loop->iteration == 1) active @endif">{{ $language->name }}</a>
                </li>
                @endforeach
            </ul>
            <div class="tab-content">
                @foreach (get_active_languages() as $language)
                <div class="tab-pane fade @if($loop->iteration == 1) active show @endif" id="content-tab-{{ $language->locale }}">
                    @if($type->supports('title'))
                    <div class="form-group">
                        <label for="title-{{ $language->locale }}">@lang('Title')</label>
                        <input type="text" name="title[{{ $language->locale }}]" id="title-{{ $language->locale }}" value="{{ isset($post) ? $post->translate('title', $language->locale) : '' }}" class="form-control">
                    </div>
                    @endif
                    @if($type->supports('content'))
                    <div class="form-group">
                        <label for="content-{{ $language->locale }}">@lang('Content')</label>
                        <textarea name="content[{{ $language->locale }}]" id="content-{{ $language->locale }}" class="editor">{!! isset($post) ? $post->translate('content', $language->locale) : '' !!}</textarea>
                    </div>
                    @endif
                    @if($type->supports('excerpt'))
                    <div class="form-group">
                        <label for="excerpt-{{ $language->locale }}">@lang('Excerpt')</label>
                        <textarea name="excerpt[{{ $language->locale }}]" id="excerpt-{{ $language->locale }}" class="form-control" rows="5">{{ isset($post) ? $post->translate('custom_excerpt', $language->locale) : '' }}</textarea>
                    </div>
                    @endif
                </div>
                @endforeach
            </div>
            @stack('card::content.end')
        </div>
    </div>
    @stack('card::content.after')
    @endif
    <x-entity-cards :type="$type->type()" action="upsert" :placement="['primary']" :params="['post' => $post ?? null, 'type' => $type]" />

    @if($type->supportsAny('meta-title', 'meta-description', 'meta-keywords'))
    @stack('card::meta.before')
    <div class="card" id="meta-card">
        <div class="card-header">
            <div class="card-title">
                <i class="fa fa-info mr-2"></i>
                @lang('Seo Meta Content')
            </div>
        </div>
        <div class="card-body">
            @stack('card::meta.start')
            <ul class="nav nav-tabs mb-4">
                @foreach(get_active_languages() as $language)
                <li class="nav-item">
                    <a data-bs-target="#seo-tab-{{ $language->locale }}" data-bs-toggle="tab" class="nav-link @if($loop->iteration == 1) active @endif">{{ $language->name }}</a>
                </li>
                @endforeach
            </ul>
            <div class="tab-content">
                @foreach (get_active_languages() as $language)
                <div class="tab-pane fade @if($loop->iteration == 1) active show @endif" id="seo-tab-{{ $language->locale }}">
                    @if($type->supports('meta-title'))
                    <div class="form-group">
                        <label for="meta-title-{{ $language->locale }}">@lang('Meta Title')</label>
                        <input type="text" name="meta_title[{{ $language->locale }}]" id="meta-title-{{ $language->locale }}" value="{{ isset($post) ? $post->translate('meta_title', $language->locale) : '' }}" class="form-control" />
                    </div>
                    @endif
                    @if($type->supports('meta-keywords'))
                    <div class="form-group">
                        <label for="meta-keywords-{{ $language->locale }}">@lang('Meta Keywords')</label>
                        <select name="meta_keywords[{{ $language->locale }}][]" id="meta-keywords-{{ $language->locale }}" class="selectr" data-taggable="true" multiple>
                            @isset($post)
                            @foreach(explode(',', $post->translate('meta_keywords', $language->locale)) as $keyword)
                            <option value="{{ $keyword }}" selected>{{ $keyword }}</option>
                            @endforeach
                            @endisset
                        </select>
                    </div>
                    @endif
                    @if($type->supports('meta-description'))
                    <div class="form-group">
                        <label for="meta-description-{{ $language->locale }}">@lang('Meta Description')</label>
                        <textarea name="meta_description[{{ $language->locale }}]" id="meta-description-{{ $language->locale }}" class="form-control" rows="5">{{ isset($post) ? $post->translate('meta_description', $language->locale) : '' }}</textarea>
                    </div>
                    @endif
                </div>
                @endforeach
            </div>
            @stack('card::meta.end')
        </div>
    </div>
    @stack('card::meta.after')
    @endif
    <x-entity-cards :type="$type->type()" action="upsert" :placement="['primary-end']" :params="['post' => $post ?? null, 'type' => $type]" />
    @stack('primary::end')
</div>
<div class="col-4">
    @stack('secondary::start')
    <x-entity-cards :type="$type->type()" action="upsert" :placement="['secondary-start']" :params="['post' => $post ?? null, 'type' => $type]" />
    @stack('card::actions.before')
    <div class="card"  id="actions-card">
        <div class="card-header">
            <div class="card-title">
                <i class="fa fa-circle mr-2"></i>
                @lang('Actions')
            </div>
        </div>
        <div class="card-body">
            @stack('card::actions.start')
            <div class="form-group">
                <label for="slug">@lang('Slug')</label>
                <div class="input-group">
                    <label for="slug"></label>
                    <input type="text" name="slug" class="form-control" value="{{ isset($post) ? $post->slug : '' }}" id="slug">
                </div>
            </div>
            @stack('card::actions.end')
        </div>
    </div>
    @stack('card::actions.after')
    @if($type->supports('thumbnail') || $type->supports('icon'))
    @stack('card::thumbnail.before')
    <div class="card" id="thumbnail-card">
        <div class="card-header">
            <div class="card-title">
                <i class="fa fa-image mr-2"></i>
                @lang('Media')
            </div>
        </div>
        <div class="card-body">
            @stack('card::media.start')
            @if($type->supports('thumbnail'))
            <input type="file" data-default-file="{{ isset($post) && $post->thumbnail_file ? uploads_url($post->thumbnail_file) : '' }}" name="thumbnail" id="thumbnail" class="dropify-image">
            @endif
            @if($type->supports('icon'))
            <x-icon-select name="icon" value="{{isset($post) ? $post->icon : ''}}" class="mt-4"/>
            @endif
            @stack('card::media.end')
        </div>
    </div>
    @stack('card::thumbnail.after')
    @endif
    <x-entity-cards :type="$type->type()" action="upsert" :placement="['secondary']" :params="['post' => $post ?? null, 'type' => $type]" />
    @if($type->supportsAny('category','tags'))
    @stack('card::taxonomies.before')
    <div class="card" id="taxonomies-card">
        <div class="card-header">
            <div class="card-title">
                <i class="fa fa-folder mr-2"></i>
                @lang('Taxonomies')
            </div>
        </div>
        <div class="card-body">
            @stack('card::taxonomies.start')
            @if($type->supports('category'))
            <div class="form-group">
                <label for="categories">@lang('Category')</label>
                @php $entityCategories = !isset($post) ? [] : $post->categories->pluck('id')->toArray(); @endphp
                <select name="categories[]" id="categories" class="selectr" multiple>
                    @foreach(get_categories($type->type()) as $category)
                    <option value="{{ $category->id }}" @selected(isset($post) && in_array($category->id, $entityCategories))>{{ $category->name }}</option>
                    @endforeach
                </select>
            </div>
            @endif
            @if($type->supports('tags'))
            <div class="form-group">
                <label for="tags">@lang('Tags')</label>
                @php $entityTags = !isset($post) ? [] : $post->tags->pluck('id')->toArray(); @endphp
                <select name="tags[]" id="tags" class="selectr" data-taggable="true" multiple>
                    @foreach(get_tags($type->type()) as $tag)
                    <option value="{{ $tag->id }}" @selected(isset($post) && in_array($tag->id, $entityTags))>{{ $tag->name }}</option>
                    @endforeach
                </select>
            </div>
            @endif
            @stack('card::taxonomies.end')
        </div>
    </div>
    @stack('card::taxonomies.after')
    @endif
    @if($type->supportsAny('author', 'status', 'password', 'comments', 'templates', 'show-in-menu'))
    @stack('card::attributes.before')
    <div class="card" id="attributes-card">
        <div class="card-header">
            <div class="card-title">
                <i class="fa fa-cogs mr-2"></i>
                @lang('Attributes')
            </div>
        </div>
        <div class="card-body">
            @stack('card::attributes.start')
            @if($type->supports('status'))
            <div class="form-group">
                <label for="status">@lang('Status')</label>
                <select name="status" id="status" class="selectr">
                    @foreach (\Cascade\Site\Enums\PostStatus::cases() as $case)
                    <option value="{{ $case->value }}" @selected(isset($post) && $post->status == $case)>{{ $case->translate() }}</option>
                    @endforeach
                </select>
            </div>
            @endif
            @if($type->supports('show-in-menu'))
            <div class="form-group">
                <label for="show_in_menu">@lang('Show in Menu')</label>
                <select name="show_in_menu" id="show_in_menu" class="selectr">
                    <option value="0" @selected(isset($post) && $post->attribute('show_in_menu') == '0')>@lang('No')</option>
                    <option value="1" @selected(isset($post) && $post->attribute('show_in_menu') == '1')>@lang('Yes')</option>
                </select>
            </div>
            @endif
            @if($type->supports('templates') && count($type->templates()) > 0)
            <div class="form-group">
                <label for="template">@lang('Template')</label>
                <select name="template" id="template" class="selectr">
                    <option value="">@lang('Default')</option>
                    @foreach ($type->templates() as $templateView => $templateName)
                    <option value="{{ $templateView }}" @selected(isset($post) && $post->template == $templateView)>{{ $templateName }}</option>
                    @endforeach
                </select>
            </div>
            @endif
            @if($type->supports('password'))
            <div class="form-group" id="password-group">
                <label for="password">@lang('Password')</label>
                <input type="password" name="password" id="password" class="form-control">
            </div>
            @endif
            @if($type->supports('author'))
            <div class="form-group">
                <label for="author_id">@lang('Author')</label>
                <select name="author_id" id="author_id" class="selectr">
                    <option value="{{ auth()->id() }}">@lang('Assign to Me')</option>
                    @foreach (\Cascade\Users\Entities\User::all() as $user)
                    <option value="{{ $user->id }}" @selected(isset($post) && $post->author_id == $user->id)>{{ $user->name }}</option>
                    @endforeach
                </select>
            </div>
            @endif
            @if($type->supports('comments'))
            <div class="form-group">
                <label for="comments-status">@lang('Comments Status')</label>
                <select name="comment_status" id="comments-status" class="selectr">
                    @foreach (\Cascade\Site\Enums\PostCommentStatus::cases() as $case)
                    <option value="{{ $case->value }}" @selected(isset($post) && $post->comment_status == $case)>{{ $case->translate() }}</option>
                    @endforeach
                </select>
            </div>
            @endif
            @stack('card::attributes.end')
        </div>
    </div>
    @stack('card::attributes.after')
    @endif
    <x-entity-cards :type="$type->type()" action="upsert" :placement="['secondary-end']" :params="['post' => $post ?? null, 'type' => $type]" />
    @stack('secondary::end')
</div>
</div>
<div class="row mt-4">
<x-entity-cards :type="$type->type()" action="upsert" :placement="['end']" :params="['post' => $post ?? null, 'type' => $type]" />
</div>
