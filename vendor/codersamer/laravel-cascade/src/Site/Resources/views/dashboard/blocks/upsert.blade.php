@extends('admin::layouts.master')
@section('page-title', isset($block) ? __('Update Block') .' - ' . $type->GetName() : __('Create Block') .' - ' . $type->GetName())
@section('page-description', __('Manage and Review Site Content Blocks') .' - ' . $type->GetName())
@section('content')
@stack('content::before')
<form action="{{ route('dashboard.site.content_blocks.store', ['type' => $type->GetType()]) }}" method="POST" enctype="multipart/form-data">
    @csrf
    <input type="hidden" name="type" value="{{ $type->GetType() }}">
    @isset($block)
    <input type="hidden" name="entity_id" value="{{ $block->id }}">
    @endisset
    @stack('form::start')
    <x-dashboard-tabs :tabs="$tabs" :data="$__data"/>
    @stack('form::end')
    <div class="row bg-white p-4">
        <div class="col-auto">
            <input type="submit" value="@lang('Save Block')" class="btn btn-block btn-primary">
        </div>
        @isset($block)
        <div class="col-auto">
            <a href="{{ route('dashboard.site.content_blocks.destroy', ['type' => $block->block_type,'block' => $block]) }}" class="btn btn-danger btn-block">@lang('Delete Block')</a>
        </div>
        @endisset
    </div>
</form>
@stack('content::after')
@endsection
