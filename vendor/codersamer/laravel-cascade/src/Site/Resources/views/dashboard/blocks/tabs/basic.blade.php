<div class="row">
    <div class="col-md-8">
        @stack('primary::start')
        @stack('card::basic.before')
        @if($type->SupportsAny('title', 'subtitle', 'content'))
        <div class="card mb-2" id="card-basic">
            <div class="card-header">@lang('Block Content')</div>
            <div class="card-body">
                @stack('card::basic.start')
                <ul class="nav nav-tabs mb-4">
                    @foreach(get_active_languages() as $language)
                    <li class="nav-item">
                        <a data-bs-target="#content-tab-{{ $language->locale }}" data-bs-toggle="tab" class="nav-link @if($loop->iteration == 1) active @endif" id="content-tab-button-{{ $language->locale }}" >
                            {{ $language->name }}
                        </a>
                    </li>
                    @endforeach
                </ul>
                <div class="tab-content">
                    @foreach (get_active_languages() as $language)
                    <div class="tab-pane fade @if($loop->iteration == 1) active show @endif" id="content-tab-{{ $language->locale }}">
                        @if($type->Supports('title'))
                        <div class="form-group">
                            <label for="title-{{ $language->locale }}">@lang('Title')</label>
                            <input type="text" name="title[{{ $language->locale }}]" value="{{ isset($block) ? $block->translate('title', $language->locale) : '' }}" id="title-{{ $language->locale }}" class="form-control">
                        </div>
                        @endif
                        @if($type->Supports('subtitle'))
                        <div class="form-group">
                            <label for="subtitle-{{ $language->locale }}">@lang('Subtitle')</label>
                            <input type="text" name="subtitle[{{ $language->locale }}]" value="{{ isset($block) ? $block->translate('subtitle', $language->locale) : '' }}" id="subtitle-{{ $language->locale }}" class="form-control">
                        </div>
                        @endif
                        @if($type->Supports('content'))
                        <div class="form-group">
                            <label for="content-title-{{ $language->locale }}">@lang('Content')</label>
                            <textarea name="content[{{ $language->locale }}]" id="content-{{ $language->locale }}" class="form-control editor">{!! isset($block) ? $block->translate('content', $language->locale) : '' !!}</textarea>
                        </div>
                        @endif
                    </div>
                    @endforeach
                </div>
                @stack('card::basic.end')
            </div>
        </div>
        @endif
        @stack('card::basic.after')
        @stack('card::design.before')
        @if($type->SupportsAny('background_color', 'foreground_color', 'theme', 'layout', 'css_class', 'css_id'))
        <div class="card mb-2" id="card-design">
            <div class="card-header">@lang('Appearance')</div>
            <div class="card-body">
                @stack('card::design.start')
                <div class="row">
                    @if($type->Supports('background_color'))
                    <div class="col-6">
                        <div class="form-group">
                            <label for="background_color">@lang('Background Color')</label>
                            <input type="color" name="background_color" id="background_color" class="form-control">
                        </div>
                    </div>
                    @endif
                    @if($type->Supports('foreground_color'))
                    <div class="col-6">
                        <div class="form-group">
                            <label for="foreground_color">@lang('Foreground Color')</label>
                            <input type="color" name="foreground_color" id="foreground_color" class="form-control">
                        </div>
                    </div>
                    @endif
                    @if($type->Supports('theme'))
                    @php $themeField = $type->GetField('theme'); @endphp
                    <div class="col-6">
                        <div class="form-group">
                            <label for="theme">@lang('Theme')</label>
                            <select name="theme" id="theme" class="selectr">
                                <option value="">@lang('Default')</option>
                                @foreach ($themeField->GetValues() as $key => $val)
                                <option value="{{ $key }}">{{ $val }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    @endif
                    @if($type->Supports('layout'))
                    <div class="col-6">
                        <div class="form-group">
                            <label for="layout">@lang('Layout')</label>
                            <select name="layout" id="layout" class="select2">
                                <option value="layout">@lang('Default')</option>
                            </select>
                        </div>
                    </div>
                    @endif
                    @if($type->Supports('css_class'))
                    <div class="col-6">
                        <div class="form-group">
                            <label for="css_class">@lang('CSS Class')</label>
                            <input type="text" name="css_class" id="css_class" class="form-control">
                        </div>
                    </div>
                    @endif
                    @if($type->Supports('css_id'))
                    <div class="col-6">
                        <div class="form-group">
                            <label for="css_id">@lang('CSS ID')</label>
                            <input type="text" name="css_id" id="css_id" class="form-control">
                        </div>
                    </div>
                    @endif
                </div>
                @stack('card::design.end')
            </div>
        </div>
        @endif
        @stack('card::design.after')
        @stack('primary::end')
    </div>
    <div class="col-md-4">
        @stack('secondary::start')
        
        @stack('card::media.before')
        @if($type->SupportsAny('image', 'icon'))
        <div class="card mb-2" id="card-media">
            <div class="card-header">@lang('Block Media')</div>
            <div class="card-body">
                @stack('card::media.start')
                @if($type->Supports('image'))
                <div class="form-group">
                    <label for="image">@lang('Image')</label>
                    <input type="file" name="image" id="image" class="dropify-image" data-default-file="{{ isset($block) && $block->image_url != null ? url('uploads/'.$block->image_url) : '' }}">
                </div>
                @endif
                @if($type->Supports('icon'))
                <div class="form-group">
                    <!--<label for="icon">@lang('Icon')</label>-->
                    <x-icon-select name="icon" value="{{isset($block) ? $block->icon : ''}}" />
                </div>
                @endif
                @stack('card::media.end')
            </div>
        </div>
        @endif
        @stack('card::media.after')
        @stack('card::meta.before')
        <div class="card" id="card-meta">
            <div class="card-header">@lang('Block Meta')</div>
            <div class="card-body">
                @stack('card::meta.start')
                @if($type->Supports('link'))
                <ul class="nav nav-tabs mb-4">
                    @foreach(get_active_languages() as $language)
                    <li class="nav-item">
                        <a data-bs-target="#link-tab-{{ $language->locale }}" data-bs-toggle="tab" class="nav-link @if($loop->iteration == 1) active @endif" id="link-tab-button-{{ $language->locale }}" >
                            {{ $language->name }}
                        </a>
                    </li>
                    @endforeach
                </ul>
                <div class="tab-content">
                    @foreach(get_active_languages() as $language)
                    <div class="tab-pane fade @if($loop->iteration == 1) active show @endif" id="link-tab-{{ $language->locale }}">
                        <div class="form-group">
                            <label for="link-text-{{ $language->locale }}">@lang('Link Text')</label>
                            <input type="text" name="link_text[{{ $language->locale }}]" value="{{ isset($block) ? $block->translate('link_text', $language->locale) : '' }}" id="link-text-{{ $language->locale }}" class="form-control">
                        </div>
                    </div>
                    @endforeach
                </div>
                <div class="form-group">
                    <label for="link-target">@lang('Link Target')</label>
                    <select name="link_target" id="link-target" class="selectr">
                        <option value="_self" @selected(isset($block) && $block->link_target == '_self')>@lang('Self')</option>
                        <option value="_blank" @selected(isset($block) && $block->link_target == '_blank')>@lang('Blank')</option>
                        <option value="_top" @selected(isset($block) && $block->link_target == '_top')>@lang('Top')</option>
                        <option value="_parent" @selected(isset($block) && $block->link_target == '_parent')>@lang('Parent')</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="link">@lang('Link')</label>
                    <input type="text" name="link" id="link" class="form-control" value="{{ isset($block) ? $block->link : '' }}">
                </div>
                @endif
                @if($type->Supports('order'))
                <div class="form-group">
                    <label for="order">@lang('Order')</label>
                    <input type="number" min="0" name="order" id="order" value="{{ isset($block) ? $block->order : '' }}" class="form-control">
                </div>
                @endif
                <div class="form-group">
                    <label for="status">@lang('Status')</label>
                    <select name="status" id="status" class="select2 form-control">
                        <option value="published" @if(isset($page) && $page->status == 'published') selected @endif>@lang('Published')</option>
                        <option value="draft" @if(isset($page) && $page->status == 'draft') selected @endif>@lang('Draft')</option>
                    </select>
                </div>
                @stack('card::meta.end')
            </div>
        </div>
        @stack('card::meta.after')
        @stack('secondary::end')
    </div>
</div>