@extends('admin::layouts.master')
@section('page-title', __('Themes Management'))
@section('page-description', __('Manage, Review, Create and Install Themes'))
@section('page-actions')
@can('install themes')
<a data-bs-target="#new-theme" data-bs-toggle="collapse" class="btn btn-success">@lang('New Theme')</a>
@endcan
@endsection
@section('content')
@can('install themes')
<div class="row collapse" id="new-theme">
    <div class="col-12">
        <form action="{{ route('dashboard.site.themes.install') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="card">
                <div class="card-header">@lang('Install New Theme')</div>
                <div class="card-body">
                    <input type="file" name="theme" id="plugin-file" class="dropify-archive">
                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">@lang('Install Theme')</button>
                </div>
            </div>
        </form>
    </div>

    <div class="col-12">
        <hr>
        <h3>@lang('Installed Themes')</h3>
    </div>
</div>
@endcan
<div class="row">
    @foreach (get_site_themes() as $theme)
        <div class="col-md-3 col-sm-6 col-12">
            <div class="card">
                <div class="card-header">
                    <div class="row justify-content-between">
                        <div class="col-auto">
                            <span>{{ $theme->GetName() }}</span>
                        </div>
                        <div class="col-auto">
                            <button type="button" class="btn btn-light" data-bs-toggle="collapse" title="Collapse" data-bs-target="#theme-{{$theme->GetSlug()}}">
                                <i class="fas fa-minus"></i>
                            </button>
                        </div>
                    </div>
                </div>
                <div class="card-body p-0">
                     <img src="{{ $theme->GetScreenshotURL() }}" style="width: 100%" />
                    @if($theme->GetDescription() != '')
                    <p class="p-2">{{ $theme->GetDescription() }}</p>
                    @endif
                    <table class="table table-striped mb-0 collapse" id="theme-{{$theme->GetSlug()}}">
                        <tr>
                            <th>@lang('Name')</th>
                            <td>{{ $theme->GetName() }}</td>
                        </tr>
                        <tr>
                            <th>@lang('Author')</th>
                            <td>{{ $theme->GetAuthor() }}</td>
                        </tr>
                        <tr>
                            <th>@lang('Status')</th>
                            <td>{{ $theme->IsEnabled() ? __('Enabled') : __('Disabled') }}</td>
                        </tr>
                        <tr>
                            <th>@lang('Requirements')</th>
                            <td>{{ count($theme->GetRequirements()) == 0 ? __('None') : implode(', ', $theme->GetRequirements()) }}</td>
                        </tr>
                    </table>
                </div>
                <div>

                </div>
                <div class="card-footer">
                @if(!$theme->IsEnabled())
                    @can('enable themes')
                    <a href="{{ route('dashboard.site.themes.enable', ['theme' => $theme->GetName()]) }}" class="btn btn-success">@lang('Activate')</a>
                    @endcan
                @else
                    @can('customize themes')
                    <a href="{{ route('dashboard.site.themes.customizer') }}" class="btn btn-primary">@lang('Customize')</a>
                    @endcan
                @endif
                </div>
            </div>
        </div>
    @endforeach
</div>
@endsection
