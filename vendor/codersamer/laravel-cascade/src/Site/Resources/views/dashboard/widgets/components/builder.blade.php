<div>
    <div class="row">
        <div class="col-md-4">
            <div class="row" id="available-widgets">
                @foreach ($widgets as $widget)
                    <div class="col-12" data-widget-id="{{ $widget->id }}">
                        <div class="card">
                            <div class="card-header">
                                <div class="card-title">
                                    {{ $widget->name }}
                                </div>
                                <div class="card-tools">
                                    <button class="btn btn-light" onclick="alert('{{$widget->description}}');"><i class="fa fa-info"></i></button>
                                    <button class="btn btn-light" wire:click="addWidget('{{$widget->id}}')"><i class="fa fa-plus"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>


        <div class="col-md-8">
            <div class="row">
                <div class="col-md-12">
                    <nav>
                        <ul class="nav nav-tabs" id="widgets-tab" role="tablist">
                            @foreach ($areas as $area)
                                <li class="nav-item">
                                    <button
                                        class="nav-link {{ $this->current_area == $area->id() ? 'active' : '' }} widge-area-tab-button"
                                        data-area-id="{{ $area->id() }}" id="nav-{{ $area->id() }}-tab"
                                        data-bs-toggle="tab" data-target="#widgets-area-{{ $area->id() }}" type="button"
                                        role="tab" aria-controls="nav-contact"
                                        aria-selected="false">{{ $area->name() }}</button>
                                </li>
                            @endforeach
                </div>
                </nav>
                <div class="tab-content" id="nav-tabContent">
                    @foreach ($areas as $area)
                        <div class="tab-pane fade {{ $this->current_area == $area->id() ? 'show active' : '' }} bg-white p-4 widget-area-body"
                            id="widgets-area-{{ $area->id() }}" role="tabpanel"
                            aria-labelledby="nav-{{ $area->id() }}-tab">
                            <p class="text-muted">@lang('Manage and Sort Widgets for Selected Area') : {{ $area->name() }}</p>
                            <ol class="sortable-widgets">
                            @foreach ($area->widgets() as $widget)
                                <li data-id="{{$widget->id}}">
                                    <div class="card collapsed-card" id="widget-{{ $widget->id }}"
                                        data-widget-id="{{ $widget->widget_id }}" data-instance-id="{{ $widget->id }}">
                                        <div class="card-header">
                                            <div class="card-title">
                                                {{ $widget->data->name }}
                                            </div>
                                            <div class="card-tools">
                                                <button type="button" class="btn btn-tool" wire:click="deleteWidget({{$widget->id}})"><i class="fas fa-times"></i></button>
                                                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-plus"></i></button>
                                            </div>
                                        </div>
                                        <div class="card-body widget-body">
                                            {!! $widget->renderDesigner()->render() !!}
                                        </div>
                                    </div>
                                </li>
                            @endforeach
                            </ol>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>

    <!-- Handlers -->
    <script type="text/javascript">
        document.addEventListener('DOMContentLoaded', function(){
            function StoreWidgetsOrder(data)
            {
                Livewire.emit('WidgetsOrderChanged', data);
            }
            function InitializeSorting()
            {
                document.querySelectorAll('.sortable-widgets').forEach(function (list) {
                    new NestedSort(
                    {
                        el: list,
                        nestingLevels: 0,
                        actions : { onDrop : function(data) {StoreWidgetsOrder(data);} }
                    });
                });
            }
            //Add Sortable to Widgets
            InitializeSorting();

            Livewire.on('WidgetsChanged', function(){
                InitializeSorting();
            });

            document.addEventListener('click', function(e){
                //Switch Tab
                if(e.target.classList.contains('widge-area-tab-button'))
                {
                    Livewire.emit('AreaSwitched', e.target.dataset.areaId);
                }
                if(e.target.classList.contains('widget-save-btn'))
                {
                    e.preventDefault();
                    e.stopPropagation();
                    var widgetOptionsForm = e.target.closest('form');
                    if(widgetOptionsForm)
                    {
                        var widgetData = new FormData(widgetOptionsForm);
                        e.target.disabled = true;
                        fetch(widgetOptionsForm.action, {
                            method: 'POST',
                            body : widgetData,
                        }).then((response) => response.text())
                        .then(html => console.log(html))
                        .catch(error => console.log(error))
                        .finally(() => {
                            e.target.disabled = false;
                        });
                        Livewire.emit('WidgetUpdated', widgetOptionsForm.querySelector('input[name=options_id]').value);
                    }
                }
            });
        });
    </script>
</div>

