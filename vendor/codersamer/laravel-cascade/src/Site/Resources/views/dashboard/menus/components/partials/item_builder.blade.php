<div class="card mb-0" data-builder-id="{{$item->builder->id}}">
    <div class="card-header">
        <div class="row justify-content-between align-items-center">
            <div class="col-auto">
                <div class="card-title">{{$item->builder->title}} : {{$item->builder->instance->Renderer($item->data)->GetTitle()}}</div>
            </div>
            <div class="col-auto d-flex-inline">
                <button type="button" class="btn btn-light" wire:click="DeleteItem({{$item->id}})"><i class="fas fa-times"></i>
                </button>
                <button type="button" class="btn btn-light" data-bs-target="#item-{{$item->id}}" data-bs-toggle="collapse"><i class="fas fa-plus"></i>
                </button>
            </div>
        </div>
    </div>
    <div id="item-{{$item->id}}" class="collapse">
        <div class="card-body">
            <form action="" onsubmit="return false;">
            {!! $item->builder->instance->Editor($item->data)->build() !!}
            </form>
        </div>
        <div class="card-footer">
            <button class="btn btn-primary update-menu-item-button" data-item-id="{{$item->id}}">@lang('Save Item')</button>
        </div>
    </div>
</div>
