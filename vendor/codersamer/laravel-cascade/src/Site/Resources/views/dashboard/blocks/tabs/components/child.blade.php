<div class="row">
    @php $child = $this->child; @endphp
    <div class="col-md--12">
        <div class="card" x-data="">
            <div class="card-body">
                <div class="row justify-content-end mb-3">
                    <div class="col-auto">
                        <button type="button" wire:click="addRow" class="btn btn-dark">
                            <i class="fa fa-plus me-2"></i>
                            @lang('Add New')
                        </button>
                    </div>
                </div>
                <table class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            @if($child->Supports('title'))
                            <th>@lang('Title')</th>
                            @endif
                            @if($child->Supports('subtitle'))
                            <th>@lang('Subtitle')</th>
                            @endif
                            @if($child->Supports('icon'))
                            <th>@lang('Icon')</th>
                            @endif
                            @if($child->Supports('order'))
                            <th>@lang('Order')</th>
                            @endif
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($this->records as $record)
                        <tr>
                        <input type="hidden" name="children[{{$child->GetType()}}][{{$loop->index}}][id]" value="{{$record->id}}">
                        @if($child->Supports('title'))
                            <td>
                                <input type="text" name="children[{{$child->GetType()}}][{{$loop->index}}][title]" id="" class="form-control" value="{{$record->title}}">
                            </td>
                        @endif
                        @if($child->Supports('subtitle'))
                            <td>
                                <input type="text" name="children[{{$child->GetType()}}][{{$loop->index}}][subtitle]" id="" class="form-control" value="{{$record->subtitle}}">
                            </td>
                        @endif
                        @if($child->Supports('icon'))
                            <td wire:ignore>
                                <x-icon-select name="children[{{$child->GetType()}}][{{$loop->index}}][icon]" value="{{$record->icon}}" />
                            </td>
                        @endif
                        @if($child->Supports('order'))
                            <td>
                                <input type="text" name="children[{{$child->GetType()}}][{{$loop->index}}][order]" id="" class="form-control" value="{{$record->order}}">
                            </td>
                        @endif
                        </tr>
                        @endforeach
                        @foreach($rows as $index => $row)
                        <tr wire:key="{{$index}}">
                        <input type="hidden" name="children[{{$child->GetType()}}][{{$index}}][id]" value="0">
                        @if($child->Supports('title'))
                            <td>
                                <input type="text" name="children[{{$child->GetType()}}][{{$index}}][title]" id="" class="form-control">
                            </td>
                        @endif
                        @if($child->Supports('subtitle'))
                            <td>
                                <input type="text" name="children[{{$child->GetType()}}][{{$index}}][subtitle]" id="" class="form-control">
                            </td>
                        @endif
                        @if($child->Supports('icon'))
                            <td wire:ignore>
                                <x-icon-select name="children[{{$child->GetType()}}][{{$index}}][icon]" />
                            </td>
                        @endif
                        @if($child->Supports('order'))
                            <td>
                                <input type="text" name="children[{{$child->GetType()}}][{{$index}}][order]" id="" class="form-control" >
                            </td>
                        @endif
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>