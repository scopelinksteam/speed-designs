<div>
    <div class="row">
        <div class="col-md-3">
            @foreach ($builders as $builder)
            <div class="card" id="menu-builder-{{$builder->id}}" data-builder-id="{{$builder->id}}" wire:key="{{$builder->id}}">
                <div class="card-header">
                    <div class="row justify-content-between align-items-center">
                        <div class="col-auto">
                            <div class="card-title">{{ $builder->title }}</div>
                        </div>
                        <div class="col-auto">
                            <button type="button" class="btn btn-light" data-bs-target="#builder-{{$builder->id}}" data-bs-toggle="collapse"><i class="fas fa-plus"></i>
                            </button>
                        </div>
                    </div>
                </div>
                <div class="collapse" id="builder-{{$builder->id}}">
                    <div class="card-body">
                        <form onsubmit="return false">
                            {!! $builder->instance->Designer()->build() !!}
                        </form>
                    </div>
                    <div class="card-footer">
                        <button class="btn btn-primary add-menu-item-button" >@lang('Add to Menu')</button>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
        <div class="col-md-9">
            <div class="card">
                <div class="card-header">
                    <div class="card-title">
                        <i class="fa fa-info me-2"></i>
                        @lang('Menu Items')
                    </div>
                </div>
                <div class="card-body">
                    <p class="text-muted">
                        @lang('Manage and Order your Menu Items Below, Allowed up to third Level')
                        <ol id="menu-builder-items">
                            @foreach ($menu->children()->get() as $item)
                            <li data-id="{{$item->id}}">
                                @include('site::dashboard.menus.components.partials.item_builder', ['item' => $item])
                                @if ($item->children()->count() > 0)
                                <ol data-id="{{$item->id}}">
                                    @foreach ($item->children as $child)
                                    <li data-id="{{$child->id}}">
                                        @include('site::dashboard.menus.components.partials.item_builder', ['item' => $child])
                                        @if ($child->children()->count() > 0)
                                        <ol data-id="{{$child->id}}">
                                            @foreach ($child->children as $subchild)
                                            <li data-id="{{$subchild->id}}">
                                                @include('site::dashboard.menus.components.partials.item_builder', ['item' => $subchild])
                                            </li>
                                            @endforeach
                                        </ol>
                                        @endif
                                    </li>
                                    @endforeach
                                </ol>
                                @endif
                            </li>
                            @endforeach
                        </ol>
                    </p>
                </div>
                <div class="card-footer">

                </div>
            </div>

        </div>
    </div>
</div>

