@extends('admin::layouts.master')
@section('page-title', __('View Message'))
@section('page-description', __('Review and View Message'))
@section('page-actions')
@can('view contact messages')
<a href="{{ route('dashboard.site.messages.show', ['message' => $message]) }}" class="btn btn-dark mx-2">@lang('Back to Messages')</a>
@endcan
@can('delete contact messages')
<a href="{{ route('dashboard.site.messages.destroy', ['message' => $message]) }}" class="btn btn-danger">@lang('Delete Message')</a>
@endcan
@endsection
@section('content')
<div class="row">
    <div class="col-8">
        <div class="card">
            <div class="card-header">@lang('Message Content')</div>
            <div class="card-body">
                <div class="form-group">
                    <label for="subject">@lang('Subject')</label>
                    <input type="text" name="subject" id="subject" class="form-control" value="{{ $message->subject }}" disabled>
                </div>
                <div class="form-group">
                    <label for="message">@lang('Message')</label>
                    <textarea name="message" id="message" class="form-control" rows="10" disabled>{{ $message->message }}</textarea>
                </div>
            </div>
        </div>
    </div>
    <div class="col-4">
        <div class="card">
            <div class="card-header">@lang('Sender Details')</div>
            <div class="card-body">
                <div class="form-group">
                    <label for="name">@lang('Name')</label>
                    <input type="text" name="name" id="name" value="{{ $message->name }}" class="form-control" disabled="">
                </div>
                <div class="form-group">
                    <label for="email">@lang('Email')</label>
                    <input type="text" name="email" id="email" class="form-control" value="{{ $message->email }}" disabled="disabled">
                </div>
                <div class="form-group">
                    <label for="website">@lang('Website')</label>
                    <input type="text" name="website" id="website" value="{{ $message->website }}" class="form-control" disabled="disabled">
                </div>
                <div class="form-group">
                    <label for="date">@lang('Sent At')</label>
                    <input type="text" name="date" id="date" value="{{ $message->created_at }}" class="form-control" disabled="disabled">
                </div>
            </div>
        </div>
        @if($message->attributes()->count() > 0)
        <div class="card">
            <div class="card-header">@lang('Additional Details')</div>
            <div class="card-body">
                @foreach($message->attributes as $attribute)
                <div class="form-group">
                    <label for="attribute-{{$attribute->key}}">{{ucwords(strtolower(str_replace('_', ' ', $attribute->key)))}}</label>
                    <input type="text" name="attribute[{{$attribute->key}}]" id="attribute-{{$attribute->key}}" value="{{ $attribute->value }}" class="form-control" disabled="">
                </div>
                @endforeach
            </div>
        </div>
        @endif
    </div>

</div>
@endsection
