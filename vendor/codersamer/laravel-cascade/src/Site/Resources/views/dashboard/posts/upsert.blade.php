@extends('admin::layouts.master')
@section('page-title')
<i class="fa fa-{{ $type->icon() }} mr-2"></i>
{{ $type->title() }}
@endsection
@section('page-description', $type->description())
@section('page-actions')
@if($type->supports('category'))
<a class="btn btn-warning" href="{{ route('dashboard.site.'.$type->slug().'.categories.index') }}">@lang('Categories')</a>
@endif
@if($type->supports('comments') && isset($post))
<a class="btn btn-primary" href="{{ route('dashboard.site.'.$type->slug().'.categories.index') }}">@lang('Comments') ( {{ $post->comments()->count() }} )</a>
@endif
<a href="{{ route('dashboard.site.'.$type->slug().'.index') }}" class="btn btn-dark">@lang('Back to List')</a>
@endsection
@section('content')
@stack('content::start')
<form action="{{ $type->route('store') }}" method="POST" enctype="multipart/form-data">
    @stack('form::start')
    @csrf
    @isset($post)
    <input type="hidden" name="entity_id" value="{{ $post->id }}">
    @endisset
    <x-dashboard-tabs :tabs="$tabs" :data="$__data" />
    <div class="p-3 rounded-3 bg-white">
        <input  type="submit" class="btn btn-primary " value="@lang('Save')" />
        <a href="#" class="ms-2 btn btn-dark">@lang('Back to List')</a>
    </div>
@stack('form::end')
</form>
@stack('content::end')
@endsection
@push('scripts')
<script>
    let PasswordGroup = document.getElementById('password-group');
    if(PasswordGroup != null && document.getElementById('status') != null)
    {
        document.getElementById('status').addEventListener('change', function(e){
            if(e.target.value == {{ \Cascade\Site\Enums\PostStatus::Protected->value }})
            {
                if(PasswordGroup)
                {
                    PasswordGroup.querySelector('input[type=password]').disabled = false;
                }
            }
            else
            {
                PasswordGroup.querySelector('input[type=password]').disabled = true;
            }
        });
    }
</script>
@endpush
