@extends('admin::layouts.master')
@section('page-title', __('Site Widgets'))
@section('page-description', __('Manage and Adjust Site Widgets'))
@section('content')
@livewire('widgets-builder')
@endsection
@push('styles')
<style>
    .nested-sort {
      padding: 0;
    }

    .nested-sort--enabled li {
      cursor: move;
    }

    .nested-sort li {
      list-style: none;
      margin: 0 0 5px;
      padding: 5px 25px;

      background: #fff;
      border: 1px solid transparent;
    }

    .nested-sort li ol {
      padding: 0;
      margin-top: 10px;
      margin-bottom: -5px;
    }

    /* ns-dragged is the class name of the item which is being dragged */
    .nested-sort .ns-dragged {
      border: 1px solid red;
    }

    /* ns-targeted is the class name of the item on which the dragged item is hovering */
    .nested-sort .ns-targeted {
      border: 1px solid green;
    }
    </style>
@endpush
