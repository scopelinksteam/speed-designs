@extends('admin::layouts.master')
@section('page-title', $type->title())
@section('page-description', $type->description())
@section('page-actions')
@if($type->supports('comments'))
<a class="btn btn-warning" href="{{ route('dashboard.site.'.$type->slug().'.categories.index') }}">@lang('Categories') ( {{ get_categories($type->type())->count() }} )</a>
@endif
@if($type->supports('tags'))
<a class="btn btn-danger" href="{{ route('dashboard.site.'.$type->slug().'.tags.index') }}">@lang('Tags') ( {{ get_tags($type->type())->count() }} )</a>
@endif
<a href="{{ $type->route('create') }}" class="btn btn-success">@lang('Create New')</a>
@endsection
@section('content')

@foreach($builder->cards()->for(\Cascade\System\Enums\EntityAction::List, \Cascade\System\Enums\EntityCardPlacement::Start) as $card)

{{ $card->render() }}
@endforeach
@stack('page::start')
<div class="row">
    <div class="col-12">
        @stack('card::table.before')
        <div class="card" id="table">
            <div class="card-header">
                <div class="card-title">
                    <i class="fa fa-{{ $type->icon() }} mr-2"></i>
                    {{ $type->title() }} @lang('List')
                </div>
            </div>
            <div class="card-body">
                @stack('card::table.start')
                @livewire('datatable', ['table' => $table, 'params' => $type->type()])
                @stack('card::table.end')
            </div>
        </div>
        @stack('card::table.after')
    </div>
</div>
@stack('page::end')
@endsection
