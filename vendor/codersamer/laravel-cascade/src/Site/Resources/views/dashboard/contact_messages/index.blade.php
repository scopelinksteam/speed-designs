@extends('admin::layouts.master')
@section('page-title', __('Contact Messages'))
@section('page-description', __('Manage and Review Messages') )
@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">@lang('Messages')</div>
            <div class="card-body">
                <livewire:datatable :table="$table" />
            </div>
        </div>
    </div>
</div>
@endsection
