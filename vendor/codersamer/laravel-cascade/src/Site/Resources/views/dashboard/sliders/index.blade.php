@extends('admin::layouts.master')
@section('page-title', __('Sliders Management'))
@section('page-description', __('Manage and Review Site Sliders'))
@section('content')
<div class="row">
    <div class="col-4">
        <form action="{{ route('dashboard.site.sliders.store') }}" method="POST">
            <div class="card">
                <div class="card-header">@lang('Slide Details')</div>
                <div class="card-body">
                    <div class="form-group">
                        <label for="name">@lang('Name')</label>
                        <input type="text" name="name" id="name" value="{{ $slider->name ?? '' }}" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="status">@lang('Status')</label>
                        <select name="status" id="status" class="form-control select2">
                            <option value="draft" {{ isset($slider) && $slider->status == 'draft' ? 'selected' : '' }}>@lang('Draft')</option>
                            <option value="published" {{ isset($slider) && $slider->status == 'published' ? 'selected' : '' }}>@lang('Published')</option>
                        </select>
                    </div>
                </div>
                <div class="card-footer">
                    @isset($slider)
                    <input type="hidden" name="entity_id" value="{{ $slider->id }}">
                    @endisset
                    @csrf
                    <button type="submit" class="btn btn-primary">@lang('Save Slider')</button>
                </div>
            </div>
        </form>
    </div>
    <div class="col-8">
        <div class="card">
            <div class="card-header">@lang('Available Sliders')</div>
            <div class="card-body">
                <livewire:datatable :table="$table" />
            </div>
        </div>
    </div>
</div>
@endsection
