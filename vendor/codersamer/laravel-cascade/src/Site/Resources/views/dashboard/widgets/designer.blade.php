<form enctype="multipart/form-data" action="{{ route('dashboard.site.widgets.designer.store') }}" method="POST" class="widget-designer-form widget-form-{{ $data->id }}" data-instance-id="{{ $options->id }}">
    <input type="hidden" name="widget" value="{{ $data->id }}">
    <input type="hidden" name="options_id" value="{{ $options->id }}">
    @csrf
    {!! $widget->builder()->render() !!}
    <div class="form-group">
        <input type="submit" value="@lang('Save')" class="btn btn-primary widget-save-btn">
    </div>
</form>
