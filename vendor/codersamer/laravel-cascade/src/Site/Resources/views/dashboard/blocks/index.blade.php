@extends('admin::layouts.master')
@section('page-title', __('Content Blocks') .' - '.$type->GetName())
@section('page-description', __('Manage and Review Site Content Blocks') .' - '.$type->GetName())
@section('page-actions')
@if(!$type->IsSingle())
<a href="{{ route('dashboard.site.content_blocks.create', ['type' => $type->GetType()]) }}" class="btn btn-success mr-2">@lang('Create') {{ $type->GetName() }}</a>
@endif
@endsection
@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">@lang('Blocks List') - {{ $type->GetName() }}</div>
            <div class="card-body">
                <livewire:datatable :table="$table" :params="$type->GetType()"/>
            </div>
        </div>
    </div>
</div>
@endsection
