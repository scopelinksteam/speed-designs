<?php

namespace Cascade\Site\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;
use Cascade\Dashboard\Entities\MenuItem;
use Cascade\Dashboard\Entities\MenuSection;
use Cascade\Dashboard\Services\DashboardBuilder;
use Cascade\Dashboard\Services\DashboardMenu;
use Cascade\Settings\Services\Settings;
use Cascade\Site\Components\ContactFormComponent;
use Cascade\Site\Components\ContentBlockChildrenComponent;
use Cascade\Site\Components\GoogleMapComponent;
use Cascade\Site\Components\IconComponent;
use Cascade\Site\Components\MenuBuilderComponent;
use Cascade\Site\Components\ScriptsComponent;
use Cascade\Site\Components\StylesComponent;
use Cascade\Site\Components\TitleComponent;
use Cascade\Site\Components\WidgetsBuilderComponent;
use Cascade\Site\Components\WidgetsComponent;
use Cascade\Site\Console\MakeThemeCommand;
use Cascade\Site\Console\MakeThemeComposerCommand;
use Cascade\Site\Console\TranslateThemeCommand;
use Cascade\Site\Directives\ScriptsDirective;
use Cascade\Site\Directives\StylesDirective;
use Cascade\Site\Directives\ThemeDirective;
use Cascade\Site\Directives\TitleDirective;
use Cascade\Site\Directives\WidgetsDirective;
use Cascade\Site\DTO\WidgetArea;
use Cascade\Site\Entities\ContactMessage;
use Cascade\Site\Entities\ContentBlock;
use Cascade\Site\Enums\PostFeature;
use Cascade\Site\Menus\Builders\LinkMenuBuilder;
use Cascade\Site\Pages\ContentBlockEntryPage;
use Cascade\Site\Services\MenuBuilder;
use Cascade\Site\Services\PostType;
use Cascade\Site\Services\Shortcodes;
use Cascade\Site\Services\Site;
use Cascade\Site\Services\ThemeCustomizer;
use Cascade\Site\Services\Themes;
use Cascade\Site\Services\Widgets;
use Cascade\Site\Shortcodes\BoldShortcode;
use Cascade\Site\Tabs\ContentBlocks\BasicTab;
use Cascade\Site\Tabs\Posts\PostBasicTab;
use Cascade\Site\Widgets\HelloWorldWidget;
use Cascade\Site\Widgets\HTMLWidget;
use Cascade\Site\Widgets\TextWidget;
use Cascade\System\Entities\Currency;
use Cascade\System\Services\BladeDirective;
use Cascade\System\Services\Icons;
use Cascade\System\Services\System;
use Cascade\Translations\Entities\Language;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\View;
use Livewire\Livewire;

class SiteServiceProvider extends ServiceProvider
{
    /**
     * @var string $moduleName
     */
    protected $moduleName = 'Site';

    /**
     * @var string $moduleNameLower
     */
    protected $moduleNameLower = 'site';

    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerTranslations();
        $this->registerConfig();
        $this->registerViews();
        $this->loadMigrationsFrom(module_path($this->moduleName, 'Database/Migrations'));
        $this->registerCommands();
        if(!$this->app->runningInConsole())
        {
            

            $this->registerMenu();
            $this->registerSettings();
            
            
            Blade::component('widgets', WidgetsComponent::class);
            Blade::component('title', TitleComponent::class);
            Blade::component('styles', StylesComponent::class);
            Blade::component('scripts', ScriptsComponent::class);
            Blade::component('gmap', GoogleMapComponent::class);
            Blade::component('contact-form', ContactFormComponent::class);
            Blade::component('icon', IconComponent::class);
            Livewire::component('menu-builder', MenuBuilderComponent::class);
            Livewire::component('widgets-builder', WidgetsBuilderComponent::class);
            Livewire::component('content-blocks-child', ContentBlockChildrenComponent::class);
            
            //ShortCodes
            Shortcodes::Register('b', BoldShortcode::class);
            
            System::BladeDirective('theme', ThemeDirective::class);
            Widgets::register(TextWidget::class, 'arbitrary-text', __('Arbitrary Text'), __('Enable you to Add Regular Text as a Widget'));
            Widgets::register(HTMLWidget::class, 'html-widget', __('HTML Widget'), __('Insert Plain HTML Code as a Widget'));
            
            MenuBuilder::AddBuilder('link', __('Custom Link'), new LinkMenuBuilder());
            
            DashboardBuilder::InjectScript(cascade_asset('dashboard/js/menu-builder.js'));
            
            foreach(Icons::GetActiveIcons() as $key => $icon)
            {
                Site::AddStyle($icon->library->style());
            }
            
            $this->booted(function(){
                foreach(PostType::types() as $post)
                {
                    call_user_func([$post->type(), 'AddTab'], PostBasicTab::class);
                }
                
            });

            $this->booted(function(){
                //Register Tabs
                ContentBlockEntryPage::AddTab(BasicTab::class);
                foreach(PostType::types() as $post)
                {
                    call_user_func([$post->type(), 'AddTab'], PostBasicTab::class);
                }
                
            });
        }

    }

    /**
     * Register Console Commands
     *
     * @return void
     */
    protected function registerCommands()
    {
        $this->commands([
            MakeThemeCommand::class,
            TranslateThemeCommand::class,
            MakeThemeComposerCommand::class,
        ]);
    }

    /**
     * Register Module Settings
     *
     * @return void
     */
    public function registerSettings()
    {
        Settings::module('site', __('Website'), function(){
            Settings::page(__('General'), 'general', function(){
                Settings::group(__('Site Info'), function(){
                    Settings::text(__('Site Name'), 'name', true);
                    Settings::text(__('Site Description'), 'description', true);
                    Settings::textarea(__('About us'), 'about_us', true);
                    if(Language::count() > 0)
                    {
                        Settings::group(__('Languages'), function(){
                            Settings::select(__('Default Language'), 'languages.default', fn() => Language::all()->pluck('name', 'locale'));
                        });
                    }
                    if(Currency::count() > 0)
                    {
                        Settings::group(__('Currency'), function(){
                            Settings::select(__('Default Currency'), 'currencies.default', fn() => Currency::all()->pluck('name', 'id'));
                        });
                    }
                });

            });
            Settings::page(__('Appearance'), 'appearance', function(){
                Settings::group(__('Header'), function(){
                    Settings::image(__('Site Logo (Dark)'), 'logo_dark');
                    Settings::image(__('Site Logo (Light)'), 'logo_light');
                    Settings::image(__('Favicon'), 'favicon');
                });
            });
            Settings::page(__('Contact Details'), 'contact', function(){
                Settings::group('Contacts', function(){
                    Settings::text(__('Phone Number'), 'phone');
                    Settings::text(__('Address'), 'address', true);
                    Settings::text(__('Google Map Address'), 'gmap_address');
                        Settings::text(__('Working Schedule'), 'working_schedule', true);
                    Settings::text(__('Email Address'), 'email');
                });
            });
            Settings::page(__('Social Media'), 'social_media', function(){
                Settings::group('Social Media', function(){
                    Settings::text(__('Facebook'), 'facebook');
                    Settings::text(__('Twitter'), 'twitter');
                    Settings::text(__('LinkedIn'), 'linkedin');
                    Settings::text(__('Instagram'), 'instagram');
                    Settings::text(__('Youtube'), 'youtube');
                    Settings::text(__('Whatsapp'), 'whatsapp');
                });
            });
        });

    }

    /**
     * Register Menu Items
     *
     * @return void
     */
    public function registerMenu()
    {
        DashboardBuilder::MainMenu()->Call(function($menu){

            DashboardBuilder::MainMenu()->AddSection(MenuSection::make('site', __('Site Management')), 5);

            //Post Types
            foreach(PostType::types() as $postType)
            {
                if(!$postType->showInMenu() || !$postType->hasCrud())
                { continue; }
                $item = MenuItem::make($postType->title(), 'site')->icon($postType->icon())->route('dashboard.site.'.$postType->slug().'.index');
                if($postType->supports(PostFeature::Category))
                {
                    $item->route(null)->link('#');
                    $item->children([
                        MenuItem::make($postType->title())->icon($postType->icon())->route('dashboard.site.'.$postType->slug().'.index'),
                        MenuItem::make(__('Categories'))->icon('folder-open')->route('dashboard.site.'.$postType->slug().'.categories.index')
                    ]);
                }
                DashboardBuilder::MainMenu()->AddItem($item);
            }

            $blocksMenu = [];
            $singleBlocks = [];
            foreach(ContentBlock::GetTypes() as $typeKey => $typeObject)
            {
                if(!$typeObject->IsMenuVisible()) { continue; }
                if($typeObject->IsSingle()) { $singleBlocks[$typeKey] = $typeObject; continue; }
                $blocksMenu[] = MenuItem::make($typeObject->GetName())->route('dashboard.site.content_blocks.index', ['type' => $typeKey]);

            }
            if(count($blocksMenu) > 0)
            {
                DashboardBuilder::MainMenu()->AddItems([
                    MenuItem::make( __('Site Contents'), 'site')->link('#')->icon('th-large')->children(
                        $blocksMenu
                    )->permission('view content blocks')
                ]);
            }
            if(site_enabled())
            {
                DashboardBuilder::MainMenu()->AddItem(
                    MenuItem::make( __('Messages'), 'site')->id('messages-menu')->icon('inbox')->route('dashboard.site.messages.index')->permission('view contact messages')
                    ->badge(fn() => ContactMessage::count())
                );
            }

            if(MenuBuilder::Enabled() && site_enabled())
            {
                DashboardBuilder::MainMenu()->AddItem(
                    MenuItem::make(__('Menus'), 'site')->icon('list')->route('dashboard.site.menus.index')->permission('build menus')->id('menus-menu')
                );
            }

            if(sliders_manager_enabled())
            {
                DashboardBuilder::MainMenu()->AddItem(
                    MenuItem::make(__('Sliders'), 'site')->icon('images')->route('dashboard.site.sliders.index')->permission('view sliders')->id('sliders-menu')
                );
            }
        });

        if(themes_manager_enabled())
        {
            DashboardBuilder::MainMenu()->Call(function(DashboardMenu $menu){
                $appearanceChildren = [];

                if(Gate::allows('view themes'))
                {
                    $appearanceChildren[] = MenuItem::make( __('Themes'))->description(__('Review Installed Themes'))->icon('paint-roller')->route('dashboard.site.themes.index')->permission('view themes');
                }
                if(Gate::allows('customize themes'))
                {
                    $appearanceChildren[] = MenuItem::make( __('Customize Site'))->description(__('Customize Site Appearance'))->icon('paint-roller')->route('dashboard.site.themes.customizer')->permission('customize themes');
                    $appearanceChildren[] = MenuItem::make( __('Widgets'))->description(__('Customize Site Widgets'))->icon('th-large')->route('dashboard.site.widgets.index')->permission('customize themes');
                }

                $menu->AddItem(
                    MenuItem::make(__('Appearance'), 'site')->icon('paint-roller')->link('#')->children($appearanceChildren)->order(20)
                );

            });
        }

        DashboardBuilder::Menu('navbar')->Call(function(DashboardMenu $menu){
            if(themes_manager_enabled())
            {
                if(Gate::allows('customize themes'))
                {
                    $menu->AddItem(MenuItem::make( __('Customize Site'))->description(__('Customize Site Appearance'))->icon('paint-roller')->route('dashboard.site.themes.customizer')->permission('customize themes'));
                }
                $menu->AddItem(MenuItem::make(__('View Site'))->link(url('/')));
            }
        });
    }
    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->booting(function(){
            dd('rEGISTERing');
            
        });
        $this->app->register(RouteServiceProvider::class);
        DashboardBuilder::InjectScript('https://cdn.jsdelivr.net/npm/nested-sort@5/dist/nested-sort.umd.min.js');

        $this->LoadActiveTheme();
    }

    protected function LoadActiveTheme()
    {
        if($this->app->runningInConsole()) { return; }
        $theme = Themes::GetActiveTheme();
        if($theme != null)
        {
            foreach($theme->GetProviders() as $provider)
            {
                $this->app->register($provider);
            }
            foreach($theme->GetFiles() as $helper)
            {
                if(file_exists($theme->GetPath($helper)))
                {
                    require_once $theme->GetPath($helper);
                }
            }
        }
    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->publishes([
            module_path($this->moduleName, 'Config/config.php') => config_path($this->moduleNameLower . '.php'),
        ], 'config');
        $this->mergeConfigFrom(
            module_path($this->moduleName, 'Config/config.php'), $this->moduleNameLower
        );
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $viewPath = resource_path('views/modules/' . $this->moduleNameLower);

        $sourcePath = module_path($this->moduleName, 'Resources/views');

        $this->publishes([
            $sourcePath => $viewPath
        ], ['views', $this->moduleNameLower . '-module-views']);

        $this->loadViewsFrom(array_merge($this->getPublishableViewPaths(), [$sourcePath]), $this->moduleNameLower);
    }

    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $langPath = resource_path('lang/modules/' . $this->moduleNameLower);

        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, $this->moduleNameLower);
        } else {
            $this->loadTranslationsFrom(module_path($this->moduleName, 'Resources/lang'), $this->moduleNameLower);
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }

    private function getPublishableViewPaths(): array
    {
        $paths = [];
        foreach (\Config::get('view.paths') as $path) {
            if (is_dir($path . '/modules/' . $this->moduleNameLower)) {
                $paths[] = $path . '/modules/' . $this->moduleNameLower;
            }
        }
        return $paths;
    }
}
