<?php

namespace Cascade\Site\Providers;

use Cascade\Site\Services\Themes;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Config;
use Cascade\System\Services\System;
use Modules\Blog\Services\BlogModule;

abstract class ThemeProvider extends ServiceProvider
{
    /**
     * @var string $themeName
     */
    protected $themeName = 'DummyTheme';

    /**
     * @var string $moduleNameLower
     */
    protected $themeNameLower = 'dummytheme';

    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerTranslations();
        $this->registerConfig();
        $this->registerViews();
        $this->loadMigrationsFrom(Themes::GetPath($this->themeName, 'Database/Migrations'));
        $this->PostTypes();
        $this->Components();
        $this->OnBoot();
        $this->Views();
        $this->Customizer();
        $this->ContentBlocks();
        $this->Widgets();
        $this->Assets();

    }
    abstract function OnRegister();
    abstract function OnBoot();
    abstract function OnActivate();
    abstract function OnDeactivate();
    public function Components() {}
    public function Assets(){}
    public function Widgets(){}
    public function ContentBlocks() {}
    public function PostTypes() {}
    public function Customizer() {}
    public function Views() { }
    public function Providers(){ return []; }



    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
        foreach($this->provides() as $provider)
        {
            $this->app->register($provider);
        }
        $this->OnRegister();
    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        if(file_exists(Themes::GetPath($this->themeName, 'Config/config.php')))
        {
            $this->publishes([
                Themes::GetPath($this->themeName, 'Config/config.php') => config_path($this->themeNameLower . '.php'),
            ], 'config');
            $this->mergeConfigFrom(
                Themes::GetPath($this->themeName, 'Config/config.php'), $this->themeNameLower
            );
        }
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $viewPath = resource_path('views/themes/' . $this->themeNameLower);

        $sourcePath = Themes::GetPath($this->themeName, 'Resources/views');

        $this->publishes([
            $sourcePath => $viewPath
        ], ['views', $this->themeNameLower . '-themes-views']);

        $this->loadViewsFrom(array_merge($this->getPublishableViewPaths(), [$sourcePath]), 'themes.'.$this->themeNameLower);
        if(Themes::GetActiveTheme()->GetName() == $this->themeName)
        {
            $this->loadViewsFrom(array_merge($this->getPublishableViewPaths(), [$sourcePath]), 'theme');
        }
    }

    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $langPath = resource_path('lang/themes/' . $this->themeNameLower);

        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, $this->themeNameLower);
        } else {
            $this->loadTranslationsFrom(Themes::GetPath($this->themeName, 'Resources/lang'), $this->themeNameLower);
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        $providers = $this->Providers();
        if($providers == null) { $providers = []; }
        $providers = is_array($providers) ? $providers : [$providers];
        return array_merge([

        ], $providers);
    }

    private function getPublishableViewPaths(): array
    {
        $paths = [];
        foreach (Config::get('view.paths') as $path) {
            if (is_dir($path . '/themes/' . $this->themeNameLower)) {
                $paths[] = $path . '/themes/' . $this->themeNameLower;
            }
        }
        return $paths;
    }
}
