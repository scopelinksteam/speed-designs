<?php

use Cascade\Site\Entities\ContentBlock;
use Cascade\Site\Entities\Slider;
use Cascade\Site\Entities\Theme;
use Cascade\Site\Services\PricingTables;
use Cascade\Site\Services\Shortcodes;
use Cascade\Site\Services\Site;
use Cascade\Site\Services\Themes;
use Cascade\System\Enums\EntityAction;
use Cascade\System\Enums\EntityCardPlacement;
use Cascade\System\Services\EntityBuilder;
use Illuminate\Support\Facades\Storage;

function get_content_blocks($type)
{
    return ContentBlock::published()->where('block_type', $type)->get();
}

if(!function_exists('content_block'))
{
    function content_block($type)
    { return ContentBlock::published()->type($type)->first(); }
}

if(!function_exists('has_pricing_tables'))
{
    function has_pricing_tables()
    { return PricingTables::HasBuilders(); }
}

if(!function_exists('pricing_table_builder'))
{
    function pricing_table_builder($slug)
    { return PricingTables::FindBuilder($slug); }
}

function get_site_themes($customOnly = null)
{
    return Themes::GetAvailableThemes();
}

function get_themes($customOnly = null)
{
    return Themes::GetThemes($customOnly);
}

function theme_asset($asset = '')
{
    $currentTheme = Themes::GetActiveTheme();
    if($currentTheme == null ) { return url($asset); }
    return $currentTheme->GetURL($asset);
}

function themes_manager_enabled() { return (config('cascade.themes_manager', true) && config('cascade.themes_manager', true)) || config('cascade.developer_mode', false); }
function site_enabled() { return config('cascade.site_manager', true) || config('cascade.developer_mode', false); }
function sliders_manager_enabled() { return (config('cascade.sliders_manager', true) && site_enabled()) || config('cascade.developer_mode', false); }


if(!function_exists('get_slider'))
{ function get_slider($id) { return Slider::find($id); } }

if(!function_exists('theme_option'))
{
    function theme_option($name, $defaultValue = null, $theme = null)
    {
        $theme = strtolower($theme == null ? Themes::GetActiveThemeName() : $theme);
        return get_setting($theme.'-theme::'.$name, $defaultValue);
    }
}

if(!function_exists('set_theme_option'))
{
    function set_theme_option($name, $value = null,  $translatable = false,  $theme = null )
    {
        $theme = strtolower($theme == null ? Themes::GetActiveThemeName() : $theme);
        return set_setting($theme.'-theme::'.$name, $value, $translatable);
    }
}


if(!function_exists('site_logo'))
{
    function site_logo($isDark = true, $defaultValue = null)
    { 
        $customLogo = get_setting('site::appearance.logo_'.($isDark ? 'dark' : 'light'), null);
        if($customLogo != null && Storage::disk('uploads')->exists($customLogo))
        {
            return uploads_url($customLogo);
        }
        return $defaultValue;
    }
}

if(!function_exists('site_name'))
{
    function site_name($defaultValue = null)
    { return get_setting('site::general.name', $defaultValue); }
}

if(!function_exists('site_title'))
{
    function site_title()
    {
        $pageTitle = Site::Title();
        return site_name().($pageTitle != null ? ' | '.$pageTitle : '');
    }
}

if(!function_exists('site_description'))
{
    function site_description($defaultValue = null)
    { return get_setting('site::general.description', $defaultValue); }
}

if(!function_exists('social_media'))
{
    function social_media($name, $defaultValue = '#')
    { return get_setting('site::social_media.'.$name, $defaultValue); }
}



if(!function_exists('apply_shortcodes'))
{
    function apply_shortcodes($content)
    { return Shortcodes::Compile($content); }
}
