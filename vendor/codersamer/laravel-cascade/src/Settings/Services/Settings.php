<?php

namespace Cascade\Settings\Services;

use Closure;
use Cascade\Settings\Components\Dashboard\SettingInputComponent;
use Cascade\Settings\Contracts\ISettingsPage;
use Cascade\Settings\Enums\SettingInputType;
use Cascade\System\Services\Help;
use Str;

class Settings
{
    protected static $registeredSettings = [];

    public static $moduleContext = '';

    protected static $pageContext = '';

    protected static $groupContext = '';

    protected static $context = [];

    protected static $callbacks = [];

    protected static $injectableCallbacks = [];


    public static function on($eventIdentifier, Closure $callback)
    {
        $eventIdentifier = strtolower($eventIdentifier);
        if(!isset(static::$injectableCallbacks[$eventIdentifier])) { self::$injectableCallbacks[$eventIdentifier] = []; }
        self::$injectableCallbacks[$eventIdentifier][] = $callback;
    }

    protected static function ProcessInjections($eventIdentifier)
    {
        $eventIdentifier = strtolower($eventIdentifier);
        if(isset(static::$injectableCallbacks[$eventIdentifier]))
        {
            foreach(static::$injectableCallbacks[$eventIdentifier] as $callback)
            {
                $callback();
            }
        }
    }

    public static function RegisterSetting($moduleName, $pageName, $options)
    {
        if(!isset(static::$registeredSettings[$moduleName]))
        { self::$registeredSettings[$moduleName] = []; }
        if(!isset(static::$registeredSettings[$moduleName][$pageName]))
        { static::$registeredSettings[$moduleName][$pageName] = []; }
        static::$registeredSettings[$moduleName][$pageName] =
            array_merge(static::$registeredSettings[$moduleName][$pageName], $options);

    }

    public static function GetContexts()
    {
        $tempModules = [];
        foreach(static::$registeredSettings as $moduleName => $data)
        {
            $pages = $data['pages'] ?? null;
            if(empty($pages) || count($pages) == 0) { continue; }
            $tempModules[$moduleName] = $data;
        }
        ksort($tempModules);
        return $tempModules;
    }

    public static function GetPage($module, $page)
    {
        if( isset(static::$registeredSettings[$module])
            && isset(static::$registeredSettings[$module]['pages'][$page]))
        {
            return static::$registeredSettings[$module]['pages'][$page];
        }
        return false;
    }

    public static function module($moduleName, $moduleDisplay, Closure $callback)
    {
        static::$moduleContext = $moduleName;
        static::$context = [
            'module' => $moduleName,
            'title' => $moduleDisplay,
            'pages' => []
        ];
        $callback();
        static::ProcessInjections($moduleName);
        static::$registeredSettings[$moduleName] = static::$context;
        static::$context = [];
    }

    public static function page($pageName, $slug, Closure $callback)
    {
        static::$pageContext = $slug;
        static::$context['pages'][$slug] = [
            'title' => $pageName,
            'slug' => $slug,
            'groups' => [],
            'module' => static::$context['module']
        ];
        $callback();
        static::ProcessInjections(static::$context['module'].'.'.$slug);
        static::$pageContext = '';
    }

    public static function view($pageName, $slug, $handlerClass, $params = [])
    {
        static::$pageContext = $slug;
        $handler = app()->make($handlerClass);
        $params['slug'] = $slug;
        $params['module'] = static::$context['module'];
        static::$context['pages'][$slug] = [
            'title' => $pageName,
            'slug' => $slug,
            'handler' => $handler,
            'params' => $params,
            'groups' => [],
            'module' => static::$context['module']
        ];
        static::$pageContext = '';
    }

    public static function group($groupName, Closure $callback)
    {
        static::$groupContext = $groupName;
        static::$context['pages'][static::$pageContext]['groups'][$groupName] = [ 'items' => ['translatable' => [], 'not_translatable' => []] ];
        $callback();
        static::$groupContext = '';
    }

    public static function item($title, $name, $inputType, $valuesCallback = null, $translatable = false, ?Help $help = null)
    {
        $item =
        [
            'title' => $title,
            'name' => $name,
            'type' => $inputType,
            'values_callback' => $valuesCallback,
            'translatable' => $translatable,
            'help' => $help,
            'key' => Str::slug(static::$moduleContext) . '::' . Str::lower(static::$pageContext) . '.' . $name,
            'identifier' => Str::slug(static::$moduleContext) . '-' . Str::slug(static::$pageContext) . '-' . Str::slug($name),
        ];
        $itemTranslationKey = $translatable ? 'translatable' : 'not_translatable';
        static::$context['pages'][static::$pageContext]['groups'][static::$groupContext]['items'][$itemTranslationKey][] = $item;
        static::ProcessInjections(static::$context['module'].'::'.static::$pageContext.'.'.$name);
    }

    public static function GetItem($key)
    {

        $segments = explode('::', $key);
        $formattedKey = strtolower($key);
        $module = $segments[0] ?? null;
        $segments = explode('.', $segments[1] ?? '');
        $page = $segments[0] ?? null;
        if(empty($module) || empty($page)) { return null; }
        $page = static::GetPage($module, $page);
        foreach($page['groups'] as $groupName => $groupBlocks)
        {
            foreach($groupBlocks['items'] as $mode => $items)
            {
                foreach($items as $item)
                {
                    if(strtolower($item['key']) == $formattedKey)
                    {
                        $item['group_name'] = $groupName;
                        return $item;
                    }
                }
            }
        }
    }

    public static function note($text, ?Help $help = null)
    { static::item($text, '', SettingInputType::NOTE, null, false, $help);}

    public static function text($title, $name, $translatable = false, ?Help $help = null)
    { static::item($title, $name, SettingInputType::TEXT, null, $translatable, $help);}

    public static function textarea($title, $name, $translatable = false, ?Help $help = null)
    { static::item($title, $name, SettingInputType::TEXTAREA, null, $translatable, $help);}

    public static function editor($title, $name, $translatable = false, ?Help $help = null)
    { static::item($title, $name, SettingInputType::EDITOR, null, $translatable, $help);}

    public static function number($title, $name, $translatable = false,?Help $help = null)
    { static::item($title, $name, SettingInputType::NUMBER, null, $translatable, $help);}

    public static function color($title, $name, $translatable = false, ?Help $help = null)
    { static::item($title, $name, SettingInputType::COLOR, null, $translatable, $help);}

    public static function password($title, $name, $translatable = false, ?Help $help = null)
    { static::item($title, $name, SettingInputType::PASSWORD, null, $translatable, $help);}

    public static function email($title, $name, $translatable = false, ?Help $help = null)
    { static::item($title, $name, SettingInputType::EMAIL, null, $translatable, $help);}

    public static function checkbox($title, $name, $translatable = false, ?Help $help = null)
    { static::item($title, $name, SettingInputType::CHECKBOX, null, $translatable, $help);}

    public static function choices($title, $name, $valuesCallback = null, $translatable = false, ?Help $help = null)
    { static::item($title, $name, SettingInputType::CHOICES, $valuesCallback, $translatable, $help);}

    public static function select($title, $name, $valuesCallback = null, $translatable = false, ?Help $help = null)
    { static::item($title, $name, SettingInputType::SELECT, $valuesCallback, $translatable, $help);}

    public static function multiple($title, $name, $valuesCallback = null, $translatable = false, ?Help $help = null)
    { static::item($title, $name, SettingInputType::MULTIPLE_SELECT, $valuesCallback, $translatable, $help);}

    public static function radio($title, $name, $valuesCallback = null, $translatable = false, ?Help $help = null)
    { static::item($title, $name, SettingInputType::RADIO, $valuesCallback, $translatable, $help);}

    public static function file($title, $name, $valuesCallback = null, $translatable = false, ?Help $help = null)
    { static::item($title, $name, SettingInputType::FILE, $valuesCallback, $translatable, $help);}

    public static function image($title, $name, $valuesCallback = null, $translatable = false, ?Help $help = null)
    { static::item($title, $name, SettingInputType::IMAGE, $valuesCallback, $translatable, $help);}
}
