<?php

namespace Cascade\Settings\Entities;

use Cascade\System\Traits\HasActions;
use Cascade\System\Traits\HasFilters;
use Cascade\System\Traits\Hookable;
use Illuminate\Database\Eloquent\Model;
use Cascade\Users\Traits\HasUserTrack;
use Exception;
use Illuminate\Http\UploadedFile;

class Setting extends Model
{
    use HasUserTrack, HasActions, HasFilters;

    protected $casts = [
        'translatable' => 'boolean'
    ];

    public function getValueAttribute()
    {
        $decodedValue = null;
        try { $decodedValue = json_decode($this->attributes['value'], true); } catch(Exception $ex) { }
        return $decodedValue === null ? $this->attributes['value'] : $decodedValue;
    }

    public static function Get($key, $default = false, $locale = null /*, Tenant $tenant = null */)
    {
        $object = Setting::where('key', $key)->first();
        if($object == null) { return $default; }

        if($locale === false)
        {
            $value = empty($object->value) ? $default : $object->value;
            return $value;
        }

        $locale = $locale === null ? app()->getLocale() : $locale;

        if($object->translatable)
        {
            $value = $object->value[$locale] ?? $default;
            return self::DispatchFilter('setting::'.$key, $value);
        }
        return self::DispatchFilter('setting::'.$key, $object->value);
    }

    public static function Set($key, $value, $translatable = false /*, Tenant $tenant = null*/)
    {
        $object = Setting::where('key', $key)->first();
        if ($object == null) {
            $object = new Setting();
            $object->key = $key;
            $object->translatable = $translatable;
        }
        
        if($value instanceof UploadedFile)
        {
            $value = $value->store('/settings', ['disk' => 'uploads']);
        }
        if(is_array($value))
        {
            $tempValue = [];
            foreach($value as $singleKey => $singleValue)
            {
                if($singleValue instanceof UploadedFile)
                {
                    $tempValue[$singleKey] = $singleValue->store('/settings', ['disk' => 'uploads']);
                }
                else
                {
                    $tempValue[$singleKey] = $singleValue;
                }
            }
            $value = $tempValue;
        }

        $value = self::DispatchFilter('set_setting::'.$key, $value);
        $object->translatable = $translatable;
        $object->value = (is_array($value))? json_encode($value) : $value;
        if($object->value == null)
        {
            $object->value = $translatable ? json_encode([]) : '';
        }
        $object->save();
        return $object;
    }

    public static function remove($key)
    {
        $object = Setting::where('key', $key)->first();
        if($object != null) { $object->delete(); }
    }
}
