<?php

namespace Cascade\Settings\Traits;

trait ScopedSettings
{
    protected static $registeredSettings = [];

    public static $moduleContext = '';

    protected static $pageContext = '';

    protected static $groupContext = '';

    protected static $context = [];

    protected static $callbacks = [];

    protected static $injectableCallbacks = [];
}
