@if($item['translatable'])
<div class="form-group">
    <label for="{{$item['identifier']}}">{{__($item['title'])}}</label>
    <input type="number" min="0" step="1" name="setting[{{$item['name']}}][locale]" id="{{$item['identifier']}}" class="form-control" value="{{$item['current_value'] ?? ''}}">
</div>
@else
<div class="form-group">
    <label for="{{$item['identifier']}}">{{__($item['title'])}}</label>
    <input type="number" min="0" step="1" name="setting[{{$item['name']}}]" id="{{$item['identifier']}}" class="form-control" value="{{$item['current_value'] ?? ''}}">
</div>
@endif
