@if($item['translatable'])
@foreach (get_active_languages() as $language)
<div class="form-group">
    <label for="{{$item['identifier']}}_{{$language->locale}}" class="form-label"><span class="badge bg-dark">{{$language->name}}</span> {{__($item['title'])}}</label>
    <textarea class="form-control" name="setting[{{$item['name']}}][{{$language->locale}}]" id="{{$item['identifier']}}_{{$language->locale}}" rows="2">{{$item['current_value'][$language->locale] ?? ''}}</textarea>
</div>
@endforeach

@else
<div class="form-group">
    <label for="{{$item['identifier']}}">{{__($item['title'])}}</label>
    <textarea class="form-control" name="setting[{{$item['name']}}]" id="{{$item['identifier']}}" rows="2">{{$item['current_value'] ?? ''}}</textarea>
</div>
@endif
