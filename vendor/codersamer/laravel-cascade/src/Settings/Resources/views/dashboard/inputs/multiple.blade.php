@if($item['translatable'])
@foreach (get_active_languages() as $language)
<div class="form-group">
    <label for="{{$item['identifier']}}_{{$language->locale}}" class="form-label"><span class="badge badge-dark">{{$language->name}}</span> {{__($item['title'])}}</label>
    <select name="setting[{{$item['name']}}][{{$language->locale}}][]" data-toggle="" class="form-control selectr" id="{{$item['identifier']}}_{{$language->locale}}" multiple>
        @foreach ($item['values'] as $key => $value)
        <option value="{{$key}}" @if(is_array($item['current_value'][$language->locale]) && in_array($key,$item['current_value'][$language->locale])) selected @endif>{{__($value)}}</option>
        @endforeach
    </select>
</div>
@endforeach
@else
<div class="form-group">
    <label for="{{$item['identifier']}}" class="form-label">{{__($item['title'])}}</label>
    <select name="setting[{{$item['name']}}][]" data-toggle="" class="form-control selectr" id="{{$item['identifier']}}" multiple>
        @foreach ($item['values'] as $key => $value)
        <option value="{{$key}}" @if(is_array($item['current_value']) && in_array($key,$item['current_value'])) selected @endif>{{__($value)}}</option>
        @endforeach
    </select>
</div>
@endif
