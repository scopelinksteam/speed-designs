@if($item['translatable'])
<div class="form-group">
    <div class="custom-control custom-checkbox">
        <input type="hidden" name="setting[{{$item['name']}}][locale]" value="0">
        <input type="checkbox" name="setting[{{$item['name']}}][locale]" id="{{$item['identifier']}}" class="custom-control-input" value="1" @if((bool)$item['current_value']) checked @endif>
        <label class="custom-control-label" for="{{$item['identifier']}}">{{__($item['title'])}}</label>
    </div>
</div>
@else
<div class="form-group">
    <div class="custom-control custom-checkbox">
        <input type="hidden" name="setting[{{$item['name']}}]" value="0">
        <input type="checkbox" name="setting[{{$item['name']}}]" id="{{$item['identifier']}}" class="custom-control-input" value="1" @if((bool)$item['current_value']) checked @endif>
        <label class="custom-control-label" for="{{$item['identifier']}}">{{__($item['title'])}}</label>
    </div>
</div>
@endif
