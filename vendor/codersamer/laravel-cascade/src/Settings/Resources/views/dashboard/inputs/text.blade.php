@if($item['translatable'])
@foreach (get_active_languages() as $language)
<div class="mb-3">
    <label class="form-label" for="{{$item['identifier']}}">
        @if($item['help'])
        <x-help :help="$item['help']" />
        @endif
        <span class="badge bg-dark">{{$language->name}}</span>
         {{__($item['title'])}}
    </label>
    <input type="text" name="setting[{{$item['name']}}][{{$language->locale}}]" id="{{$item['identifier']}}" class="form-control" value="{{$item['current_value'][$language->locale] ?? ''}}">
</div>
@endforeach
@else
<div class="mb-3">
    <label for="{{$item['identifier']}}">
        @if($item['help'])
        <x-help :help="$item['help']" />
        @endif
        {{__($item['title'])}}
    </label>
    <input type="text" name="setting[{{$item['name']}}]" id="{{$item['identifier']}}" class="form-control" value="{{$item['current_value'] ?? ''}}">
</div>
@endif
