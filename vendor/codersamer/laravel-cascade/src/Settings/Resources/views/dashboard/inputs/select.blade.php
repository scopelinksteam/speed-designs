@if($item['translatable'])
@foreach (get_active_languages() as $language)
<div class="form-group">
    <label class="form-label" for="{{$item['identifier']}}_{{$language->locale}}"><span class="badge bg-dark">{{$language->name}}</span> {{__($item['title'])}}</label>
    <select name="setting[{{$item['name']}}][{{$language->locale}}]" data-toggle="" class="form-control selectr" id="{{$item['identifier']}}_{{$language->locale}}">
        @foreach ($item['values'] as $key => $value)
        <option value="{{$key}}" @if(($item['current_value'][$language->locale] ?? '') == $key) selected @endif>{{__($value)}}</option>
        @endforeach
    </select>
</div>
@endforeach
@else
<div class="form-group">
    <label class="form-label" for="{{$item['identifier']}}">{{__($item['title'])}}</label>
    <select name="setting[{{$item['name']}}]" data-toggle="" class="form-control selectr" id="{{$item['identifier']}}">
        @foreach ($item['values'] as $key => $value)
        <option value="{{$key}}" @if($item['current_value'] == $key) selected @endif>{{__($value)}}</option>
        @endforeach
    </select>
</div>
@endif
