@if($item['translatable'])
@foreach (get_active_languages() as $language)
<div class="mb-3">
    <label class="form-label" for="{{$item['identifier']}}-{{$language->locale}}"><span class="badge bg-dark">{{$language->name}}</span> {{__($item['title'])}}</label>
    <input type="file" name="setting[{{$item['name']}}][{{$language->locale}}]" id="{{$item['identifier']}}-{{$language->locale}}" class=" dropify-image" value="{{$item['current_value'][$language->locale] ?? ''}}">

    @if($item['current_value'][$language->locale])
    <img class="thumbnail" height="100px" src="{{ uploads_url($item['current_value'][$language->locale]) }}" />
    @endif
</div>
@endforeach
@else
<div class="mb-3">
    <label class="form-label" for="{{$item['identifier']}}">{{__($item['title'])}}</label>
    <input type="file" name="setting[{{$item['name']}}]" id="{{$item['identifier']}}" class=" dropify-image" @if($item['current_value']) data-default-file="{{ uploads_url($item['current_value']) }}" @endif value="{{$item['current_value'] ?? ''}}">
</div>
@endif
