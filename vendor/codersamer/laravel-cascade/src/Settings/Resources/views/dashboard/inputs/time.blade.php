@if($item['translatable'])
<div class="form-group">
    <label for="{{$item['identifier']}}">{{__($item['title'])}}</label>
    <input type="time" name="setting[{{$item['name']}}][locale]" id="{{$item['identifier']}}" class="form-control" value="{{$entity->title ?? ''}}">
</div>
@else
<div class="form-group">
    <label for="{{$item['identifier']}}">{{__($item['title'])}}</label>
    <input type="time" name="setting[{{$item['name']}}]" id="{{$item['identifier']}}" class="form-control" value="{{$entity->title ?? ''}}">
</div>
@endif
