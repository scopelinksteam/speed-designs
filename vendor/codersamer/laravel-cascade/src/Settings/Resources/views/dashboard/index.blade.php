@extends('admin::layouts.master')
@section('page-title', $title ?? __('Settings'))
@section('page-description', $description ?? __('Manage System Settings and Configurations'))
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="setting-sidebar">
            <ul class="nav nav-tabs mb-4" id="myTab" role="tablist">
                @foreach ($contexts as $context)
                    <li class="nav-item">
                        <a class="nav-link @if ($loop->iteration == 1) active @endif"
                            id="{{ $context['module'] }}-tab-button" data-bs-toggle="tab"
                            data-bs-target="#{{ $context['module'] }}-tab" role="tab">{{ $context['title'] }}</a>
                    </li>
                @endforeach
            </ul>
            </nav>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 tab-content">
        @foreach ($contexts as $context)
            <div class=" tab-pane fade @if ($loop->iteration == 1) show active @endif"
                id="{{ $context['module'] }}-tab">
                <div class="row">
                    <div class="col-md-3">
                        <ul class="nav flex-column nav-pills w-100">
                            @foreach ($context['pages'] as $page)
                                <li><a class="nav-link @if ($loop->iteration == 1) show active @endif"
                                        id="{{ $context['module'] . '-' . $page['slug'] }}-tab-button" data-bs-toggle="tab"
                                        data-bs-target="#{{ $context['module'] . '-' . $page['slug'] }}-tab"
                                        role="tab">{{ $page['title'] }}</a></li>
                            @endforeach
                        </ul>
                    </div>
                    <div class="col-md-9">

                        @foreach ($context['pages'] as $page)
                            <div class="tab-pane fade @if ($loop->iteration == 1) show active @endif"
                                id="{{ $context['module'] . '-' . $page['slug'] }}-tab">
                                @include('settings::dashboard.page', [
                                    'page' => $page,
                                    'context' => $context,
                                ])
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        @endforeach
    </div>
</div>

@endsection
@push('scripts')
    <script>
        document.addEventListener('DOMContentLoaded', () => {
            if (window.location.hash) {
                var hash = window.location.hash.substring(1);
                $('a[href="#' + hash + '"]').tab('show');
            }
        });
    </script>
@endpush
