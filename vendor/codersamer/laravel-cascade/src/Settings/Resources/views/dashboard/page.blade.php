@if(isset($page['handler']))
    @if(method_exists($page['handler'], 'register'))
        @php $page['handler']->register($page['params']) @endphp
    @endif
<div class="card">
    <div class="card-header">
        <div class="card-title">
            @if(method_exists($page['handler'], 'header'))
            {!! $page['handler']->header() !!}
            @else
            {{ $page['title'] }}
            @endif
        </div>
    </div>
    <div class="card-body">
        {!! $page['handler']->view()->render();  !!}
    </div>
</div>
@else

    <form action="{{ route('dashboard.settings.store', ['module' => $page['module'], 'page' => $page['slug']]) }}" method="POST" enctype="multipart/form-data">
        @php $itemsKeys = []; @endphp
        @foreach ($page['groups'] as $groupName => $group)
            <div class="card">
                <div class="card-header">
                    <div class="card-title">
                        <i class="fa fa-cogs mr-2"></i>
                        {{$groupName}}
                    </div>
                </div>
                <div class="card-body">
                    @foreach ($group['items']['not_translatable'] ?? [] as $item)
                        @if(!in_array($item['type'], [2,6,7]))
                        @php $itemsKeys[] = $item['name']; @endphp
                        @endif
                        <x-setting-input :item="$item" />
                    @endforeach
                    @foreach ($group['items']['translatable'] ?? [] as $item)
                        @if(!in_array($item['type'], [2,6,7]))
                        @php $itemsKeys[] = $item['name']; @endphp
                        @endif
                        <x-setting-input :item="$item" />
                    @endforeach
                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">
                        {{__('Save Settings')}}
                    </button>
                </div>
            </div>

            @endforeach
            <div class="row">
                <div class="col-12">
                    @csrf
                    <input type="hidden" name="items" value="{{ json_encode($itemsKeys) }}" />
            </div>
        </div>
    </form>
@endif
