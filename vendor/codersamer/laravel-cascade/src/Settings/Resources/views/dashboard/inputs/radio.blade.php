@if($item['translatable'])
<div class="form-group">
    <label class="mr-2" for="{{$item['identifier']}}">{{__($item['title'])}} : </label>
    @foreach ($item['values'] as $key => $value)
        <div class="radio form-check-inline">
            <input type="radio" id="{{$item['identifier']}}-{{$key}}" value="{{$key}}" name="setting[{{$item['name']}}][locale]" @if($item['current_value'] == $key) checked @endif>
            <label for="{{$item['identifier']}}-{{$key}}">{{$value}}</label>
        </div>
    @endforeach
</div>
@else
<div class="form-group">
    <label for="{{$item['identifier']}}">{{__($item['title'])}}</label>
    @foreach ($item['values'] as $key => $value)
        <div class="radio form-check-inline">
            <input type="radio" id="{{$item['identifier']}}-{{$key}}" value="{{$value}}" name="setting[{{$item['name']}}]" @if($item['current_value'] == $value) checked @endif>
            <label for="{{$item['identifier']}}-{{$key}}"> Inline Two </label>
        </div>
    @endforeach
</div>
@endif
