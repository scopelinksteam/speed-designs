<?php

namespace Cascade\Settings\Abstractions;

use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;

abstract class SettingPage
{
    public function register(mixed $params) : void {  }
    public function header() : String { return ''; }
    public abstract function view() : View|Factory;
}
