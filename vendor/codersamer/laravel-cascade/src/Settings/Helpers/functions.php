<?php

use Cascade\Settings\Entities\Setting;

if(!function_exists('get_setting')){
    function get_setting($key, $default = false, $locale = null)
    { return Setting::Get($key, $default, $locale); }
}

if(!function_exists('set_setting')){
    function set_setting($key, $value, $translatable)
    { return Setting::Set($key, $value, $translatable); }
}
if(!function_exists('delete_setting')){
    function delete_setting($key)
    { return Setting::remove($key); }
}
