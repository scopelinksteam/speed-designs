<?php

namespace Cascade\Settings\Components\Dashboard;

use Cascade\Settings\Entities\Setting;
use Illuminate\View\Component;
use Cascade\Settings\Enums\SettingInputType;
use Cascade\Settings\Services\Settings;
use Exception;

class SettingInputComponent extends Component
{

    protected $item = [];
    protected $type = SettingInputType::TEXT;
    public $locale = null;

    public function __construct($item, $locale = null, $name = null)
    {
        if(is_string($item))
        { $item = Settings::GetItem($item); }
        $this->item = $item;
        $this->locale = $locale;
        if($item)
        {
            $this->type = $item['type'];
            if($name)
            { $this->item['name'] = $name; }

        }
    }

    public function render()
    {
        if(empty($this->item)) { return ''; }
        $viewName = 'text';
        switch($this->type)
        {
            case SettingInputType::TEXT : $viewName = 'text'; break;
            case SettingInputType::TEXTAREA : $viewName = 'textarea'; break;
            case SettingInputType::PASSWORD : $viewName = 'password'; break;
            case SettingInputType::CHECKBOX : $viewName = 'checkbox'; break;
            case SettingInputType::RADIO : $viewName = 'radio'; break;
            case SettingInputType::SELECT : $viewName = 'select'; break;
            case SettingInputType::IMAGE : $viewName = 'image'; break;
            case SettingInputType::FILE : $viewName = 'file'; break;
            case SettingInputType::EDITOR : $viewName = 'editor'; break;
            case SettingInputType::EMAIL : $viewName = 'email'; break;
            case SettingInputType::COLOR : $viewName = 'color'; break;
            case SettingInputType::TIME : $viewName = 'time'; break;
            case SettingInputType::DATE : $viewName = 'date'; break;
            case SettingInputType::MONTH : $viewName = 'month'; break;
            case SettingInputType::WEEK : $viewName = 'week'; break;
            case SettingInputType::NUMBER : $viewName = 'number'; break;
            case SettingInputType::CHOICES : $viewName = 'choices'; break;
            case SettingInputType::MULTIPLE_SELECT : $viewName = 'multiple'; break;
            case SettingInputType::NOTE : $viewName = 'note'; break;
            default: $viewName = $this->type; break;
        }
        if($this->item['values_callback'] != null)
        {
            $this->item['values'] = $this->item['values_callback']();
        }
        //dd(get_setting('settings::app_preferences.name'));
        $this->item['current_value'] = get_setting($this->item['key'], false, false);
        if($this->item['translatable'] && !is_array($this->item['current_value']))
        { $this->item['current_value'] = fill_locales([], false); }
        return view('settings::dashboard.inputs.'.$viewName)->with('item', $this->item);
    }
}
