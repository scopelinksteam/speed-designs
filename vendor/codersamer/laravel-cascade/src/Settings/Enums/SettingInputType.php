<?php

namespace Cascade\Settings\Enums;


class SettingInputType {

    const TEXT      = 0;
    const TEXTAREA  = 1;
    const PASSWORD  = 2;
    const CHECKBOX  = 3;
    const RADIO     = 4;
    const SELECT    = 5;
    const IMAGE     = 6;
    const FILE      = 7;
    const EDITOR    = 8;
    const EMAIL     = 9;
    const COLOR     = 10;
    const TIME      = 11;
    const DATE      = 12;
    const MONTH     = 13;
    const WEEK      = 14;
    const NUMBER    = 15;
    const CHOICES   = 16;
    const MULTIPLE_SELECT   = 17;
    const NOTE = 18;

}
