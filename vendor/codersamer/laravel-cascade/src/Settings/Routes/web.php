<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;
use Cascade\Settings\Http\Controllers\Dashboard\SettingsController;

Route::hybrid(function(){
    Route::get('/', [SettingsController::class, 'index'])->name('index')->middleware('can:manage settings');
    Route::post('/{module}/{page}', [SettingsController::class, 'store'])->name('store')->middleware('can:manage settings');
}, 'settings');
