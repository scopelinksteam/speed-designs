<?php

namespace Cascade\Settings\Http\Controllers\Dashboard;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Gate;
use Cascade\System\Services\Feedback;
use Cascade\Settings\Services\Settings;
use Illuminate\Support\Facades\Storage;

class SettingsController extends Controller
{

    /**
     * Display Settings Index
     */
    public function index()
    {
        Gate::authorize('manage settings');
        return view('settings::dashboard.index', [
            'contexts' => Settings::GetContexts()
        ]);
    }
    /**
     * Display a Setting Page
     * @return Renderable
     */
    public function page($module, $page)
    {
        Gate::authorize('manage settings');
        $page = Settings::GetPage($module, $page);
        if(!$page) { abort(404, __("Request Setting Page is not Exist or Empty")); }
        return view('settings::dashboard.page', compact('page'));
    }

    /**
     * Store Setting Page Item
     *
     */
    public function store(Request $request, $module, $page)
    {
        Gate::authorize('manage settings');
        $dashboardURL = get_setting('system::dashboard.url_prefix', 'dashboard');
        $savedItems = [];
        $itemsKeys = array_unique(json_decode($request->items, true));
        if($request->setting)
        {
            foreach($request->setting as $key => $value)
            {
                if($value == null) { continue; }
                $savedItems[] = $key;
                set_setting($module.'::'.$page.'.'.$key, $value, is_array($value));
            }
        }
        foreach($request->allFiles() as $settings => $files)
        {
            foreach($files as $key => $input)
            {
                $savedItems[] = $key;
                if ($input == null) {
                    continue;
                }
                //if(!is_allowed_extension($input->extension()))
                if(false)
                {
                    Feedback::getInstance()->addAlert(__('File Type not Allowed, File Skipped')." : ".ucfirst(str_replace('_', ' ', $key)));
                    continue;
                }
                if(is_array($input))
                {
                    $values = [];
                    foreach($input as $inputKey => $entry)
                    {
                        $values[$inputKey] = $entry->store('/settings', ['disk' => 'uploads']);
                    }
                    set_setting($module . '::' . $page . '.' . $key, $values, is_array($values));
                }
                else
                {

                    $value = $input->store('/settings', ['disk' => 'uploads']);
                    set_setting($module . '::' . $page . '.' . $key, $value, is_array($value));
                }
            }
        }
        $unsavedItems = array_diff($itemsKeys, $savedItems);
        foreach($unsavedItems as $key)
        {
            delete_setting($module.'::'.$page.'.'.$key);
        }
        Feedback::getInstance()->addSuccess(__('Settings Saved Successfully'));
        Feedback::flash();
        if(get_setting('system::dashboard.url_prefix', 'dashboard') == $dashboardURL)
        { return back(); }
        $path = '/'.get_setting('system::dashboard.url_prefix', 'dashboard');
        return redirect($path);
    }

}
