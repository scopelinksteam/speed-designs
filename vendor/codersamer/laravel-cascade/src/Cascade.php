<?php

namespace Cascade;

use Cascade\System\Entities\Module as EntitiesModule;
use Exception;
use Illuminate\Support\Facades\Artisan;
use Nwidart\Modules\Module;
use Symfony\Component\Finder\Finder;

class Cascade {

    public static function PostAutoloadDump()
    {
        
    }

    public static function ScanModules()
    {
        $modules = [];
        $scanningPathes = [__DIR__];
        if(is_dir(base_path('Modules')))
        {
            $scanningPathes[] = base_path('Modules');
        }

        $finder = new Finder();
        $modulesFiles = $finder->in($scanningPathes)->depth(1)->name('module.json')->files();
        foreach($modulesFiles as $file)
        {
            $object = json_decode($file->getContents());
            $modules[] = $object->name;
        }
        
        $json = [];
        if(file_exists(base_path('modules_statuses.json')))
        {
            $json = json_decode(file_get_contents(base_path('modules_statuses.json')), true);
        }
        $systemModules = ['Admin', 'Dashboard', 'Users', 'System', 'Settings', 'Translations', 'Site', 'API', 'People', 'Tenancy', 'Payments'];
        foreach($modules as $module)
        {
            if(in_array($module, $systemModules))
            {
                $json[$module] = true;
            }
            else
            {
                if(!isset($json[$module]))
                {
                    $json[$module] = false;
                }
            }
        }
        try
        {
            file_put_contents(base_path('modules_statuses.json'), json_encode($json, JSON_PRETTY_PRINT));
        }
        catch(Exception $ex) { }
    }

    public static function ActivateModules()
    {
        //Activate Modules
        if(is_dir(base_path('Modules'))) { return; }
        $finder = new Finder();
        $modulesFiles = $finder->in([base_path('Modules')])->depth(1)->name('module.json')->files();
        $modules = [];
        foreach($modulesFiles as $file)
        {
            $modules[] = EntitiesModule::LoadFrom($file) ;
        }
        foreach($modules as $module)
        {
            if(!$module->IsEnabled()) { continue; }
            foreach($module->GetProviders() as $provider)
            {
                if(class_exists($provider)) { app()->register($provider); }
                foreach($module->GetFiles() as $file)
                {
                    require_once $module->GetPath($file);
                }
            }
        }
    }
    
}