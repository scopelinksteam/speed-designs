<?php

namespace Cascade\Users\Listeners;

use Cascade\Users\Notifications\NewUserRegisteration;
use Illuminate\Auth\Events\Registered;

class AccountRegisterationListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(Registered $event)
    {
        NewUserRegisteration::Send($event->user);
    }
}
