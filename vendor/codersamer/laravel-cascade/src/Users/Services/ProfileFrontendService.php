<?php

namespace Cascade\Users\Services;

use Cascade\System\Abstraction\Service;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Support\Facades\Route;

class ProfileFrontendService extends Service
{
    public function name(): string
    {
        return __('Frontend Users Profiles');
    }

    public function description(): string
    {
        return __('Adds Profile and Account Management Functionality to Frontend');
    }

    public function identifier(): string
    {
        return 'users-frontend-profile';
    }

    public function register(Application $app)
    {
        Route::middleware(['web', 'auth'])->group(module_path('Users', 'Routes/frontend/profile.php'));
    }
}
