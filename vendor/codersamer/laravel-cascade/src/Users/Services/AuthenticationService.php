<?php

namespace Cascade\Users\Services;

class AuthenticationService
{
    protected static $registerRecaptcha = true;

    protected static $loginRecaptcha = true;

    public static function SetRegisterRecaptcha(bool $enabled)
    {
        static::$registerRecaptcha = $enabled;
    }

    public static function SetLoginRecaptcha(bool $enabled)
    {
        static::$loginRecaptcha = $enabled;
    }

    public static function RegisterHasRecaptcha() : bool
    {
        return static::$registerRecaptcha;
    }

    public static function LoginHasRecaptcha() : bool
    {
        return static::$loginRecaptcha;
    }
}