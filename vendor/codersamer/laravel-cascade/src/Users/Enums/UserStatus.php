<?php

namespace Cascade\Users\Enums;

enum UserStatus : int
{
    case Pending = 0;
    case Verifying = 1;
    case Active = 2;
    case Disabled = 3;
    case Banned = 4;
    case Locked = 5;

    public function color() : String
    {
        return match($this) {
            UserStatus::Pending => 'warning',
            UserStatus::Verifying => 'primary',
            UserStatus::Active => 'success',
            UserStatus::Disabled => 'dark',
            UserStatus::Banned => 'danger',
            UserStatus::Locked => 'light'
        };
    }
}
