<?php

namespace Cascade\Users\Enums;


enum UsersProviders : String
{
    case System = 'system';
    case Facebook = 'facebook';
    case Twitter = 'twitter';
    case Google = 'google';
    case Github = 'github';
}
