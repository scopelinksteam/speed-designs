<?php

namespace Cascade\Users\Datatables;

use Cascade\Dashboard\Services\Datatables\Action;
use Cascade\Dashboard\Services\Datatables\Column;
use Cascade\Dashboard\Services\Datatables\DataTable;
use Cascade\Users\Entities\Role;

class RolesDatatable extends DataTable
{
    function query()
    {
        return Role::query();
    }
    function columns()
    {
        return [
            Column::make('id')->text(__('ID')),
            Column::make('name')->text(__('Name'))
        ];
    }

    function actions()
    {
        return [
            Action::make('permissions', __('Permissions'))->route('dashboard.users.roles.permissions', ['role' => 'id']),
            Action::make('edit', __('Edit'))->route('dashboard.users.roles.edit', ['role' => 'id']),
            Action::make('delete', __('Delete'))->route('dashboard.users.roles.delete', ['role' => 'id'])
        ];
    }

}
