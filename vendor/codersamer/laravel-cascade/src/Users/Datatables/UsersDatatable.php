<?php

namespace Cascade\Users\Datatables;

use Illuminate\Support\Facades\Gate;
use Cascade\Dashboard\Services\Datatables\Action;
use Cascade\Dashboard\Services\Datatables\Column;
use Cascade\Dashboard\Services\Datatables\DataTable;
use Cascade\Dashboard\Services\Datatables\Filter;
use Cascade\People\Services\PeopleService;
use Cascade\Users\Entities\Role;
use Cascade\Users\Entities\User;

class UsersDatatable extends DataTable
{

    public function __construct($params = null)
    {
        $this->editColumn('email', function($user){
            return $this->email($user->email);
        });
        $this->editColumn('username', function($user){
            return $this->badge($user->username, 'dark');
        });
        $this->editColumn('status', function($user){
            return $this->badge($user->status->name, $user->status->color());
        });
        $this->editColumn('roles', function($user){
            $roles = [];
            if($user->is_super_admin)
            {$roles[] = $this->badge(__('Super Admin'), 'danger');}
            foreach($user->roles as $role)
            { $roles[] = $this->badge($role->name, 'dark'); }
            return implode('', $roles);
        });
        $this->editColumn('name', function(User $user){
            $object = $user->impersonate();
            $type = $object->type == null ? null : PeopleService::for($object->type);
            return $object->display_name.'<br />'.$this->badge($type == null ? __('User') : $type->singular(), 'primary');
        });
    }

    public function columns()
    {
        return [
            //Column::make('id')->text(__('ID')),
            Column::make('name')->text(__('Name')),
            Column::make('username')->text(__('Username')),
            Column::make('email')->text(__('Email')),
            Column::make('status')->text(__('Status')),
            Column::make('roles')->text(__('Roles')),
        ];
    }

    public function query($search = null, $filters = [])
    {
        $query = User::query();

        if(!empty($search))
        {
            $query = $query->where('name', 'LIKE', '%'.$search.'%');
        }
        if(isset($filters['email']) && !empty($filters['email']))
        {
            $query = $query->where('email', 'LIKE', '%'.$filters['email'].'%');
        }

        if(isset($filters['role']) && !empty($filters['role']))
        {
            $roleName = $filters['role'];
            $query = $query->whereHas('roles', function($rolesQuery) use($roleName){
                $rolesQuery->where('id', $roleName);
            });
        }


        return $query;
    }

    public function actions()
    {
        $actions = [];
        if(Gate::allows('edit users'))
        { $actions[] = Action::make('edit', __('Edit'))->route('dashboard.users.edit', ['user' => 'id']); }
        if(Gate::allows('delete users'))
        { $actions[] = Action::make('delete', __('Delete'))->route('dashboard.users.delete', ['user' => 'id']); }
        return $actions;
    }

    public function filters()
    {
        return [
            Filter::make(Filter::FILTER_TYPE_TEXT, 'email')->title(__('Filter By Email'))->placeholder(__('Start Searching')),
            Filter::make(Filter::FILTER_TYPE_SELECT, 'role')->title(__('Has Role'))->values(function(){
                return Role::all()->pluck('name', 'id');
            }),
        ];
    }
}
