<?php

namespace Cascade\Users\Datatables;

use Cascade\Dashboard\Services\Datatables\Action;
use Cascade\Dashboard\Services\Datatables\Column;
use Cascade\Dashboard\Services\Datatables\DataTable;
use Cascade\Users\Entities\PermissionRequest;

class PermissionsRequestsTable extends DataTable
{

    public function __construct($params = null)
    {
        $this->editColumn('permission', function(PermissionRequest $request){
            return '<strong>'.ucwords($request->permission->name).'</strong><br/><small>'.$request->permission->description.'</small>';
        })->editColumn('user', function(PermissionRequest $request){
            return $request->user->name;
        })
        ->editColumn('department', function(PermissionRequest $request){
            $department = $request->permission->owner_type;
            $department = ucwords(str_replace('cascade', __('System'),strtolower($department)));
            return $department;
        })
        ->editColumn('scope', function(PermissionRequest $request){
            return $request->permission->owner_name;
        });
    }

    public function query($search = null, $filters = [])
    {
        $query = PermissionRequest::query();
        $query = $query->pending();

        return $query;
    }

    public function columns()
    {
        return [
            Column::make('user')->text(__('Username')),
            Column::make('permission')->text(__('Permission')),
            Column::make('department')->text(__('Department')),
            Column::make('scope')->text(__('Scope')),
            Column::make('created_at')->text(__('Datetime'))
        ];
    }

    public function actions()
    {
        return [
            Action::make('approve', __('Approve'))->route('dashboard.users.permissions.requests.approve', ['request' => 'id'])->permission('approve permissions requests'),
            Action::make('reject', __('Reject'))->route('dashboard.users.permissions.requests.reject', ['request' => 'id'])->permission('reject permissions requests'),
        ];
    }

}
