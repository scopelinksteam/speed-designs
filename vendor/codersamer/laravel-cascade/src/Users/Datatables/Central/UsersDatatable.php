<?php

namespace Cascade\Users\Datatables\Central;

use Illuminate\Support\Facades\Gate;
use Cascade\Dashboard\Services\Datatables\Action;
use Cascade\Dashboard\Services\Datatables\Column;
use Cascade\Dashboard\Services\Datatables\DataTable;
use Cascade\Dashboard\Services\Datatables\Filter;
use Cascade\Tenancy\Entities\Tenant;
use Cascade\Tenancy\Services\TenantManager;
use Cascade\Users\Entities\Role;
use Cascade\Users\Entities\User;

class UsersDatatable extends DataTable
{

    protected $tenantId  = 0;
    protected Tenant $tenant;
    public function __construct($tenant_id)
    {
        $this->tenantId = intval($tenant_id);
        $this->tenant = Tenant::find($this->tenantId);
        $this->editColumn('email', function($user){
            return $this->email($user->email);
        });
    }

    public function columns()
    {
        return [
            Column::make('id')->text(__('ID')),
            Column::make('name')->text(__('Name')),
            Column::make('email')->text(__('Email')),
        ];
    }

    public function query($search = null, $filters = [])
    {
        $this->tenant->manager->SetDatabase();

        $query = User::query();

        if(!empty($search))
        {
            $query = $query->where('name', 'LIKE', '%'.$search.'%');
        }
        if(isset($filters['email']) && !empty($filters['email']))
        {
            $query = $query->where('email', 'LIKE', '%'.$filters['email'].'%');
        }

        if(isset($filters['role']) && !empty($filters['role']))
        {
            $roleName = $filters['role'];
            $query = $query->whereHas('roles', function($rolesQuery) use($roleName){
                $rolesQuery->where('id', $roleName);
            });
        }




        return $query;
    }

    public function actions()
    {
        $actions = [];
        if(Gate::allows('edit users'))
        { $actions[] = Action::make('edit', __('Edit'))->route('central.users.tenant.edit', ['user' => 'id', 'tenant' => $this->tenantId]); }
        if(Gate::allows('delete users'))
        { $actions[] = Action::make('delete', __('Delete'))->route('central.users.tenant.delete', ['user' => 'id', 'tenant' => $this->tenantId]); }
        return $actions;
    }

    public function filters()
    {
        $tenant = $this->tenant;
        return [
            Filter::make(Filter::FILTER_TYPE_TEXT, 'email')->title(__('Filter By Email'))->placeholder(__('Start Searching')),
            Filter::make(Filter::FILTER_TYPE_SELECT, 'role')->title(__('Has Role'))->values(function() use($tenant){
                $tenant->manager->SetDatabase();
                return Role::all()->pluck('name', 'id');
            }),
        ];
    }
}
