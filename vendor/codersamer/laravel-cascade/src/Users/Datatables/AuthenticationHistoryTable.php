<?php

namespace Cascade\Users\Datatables;

use Cascade\Dashboard\Services\Datatables\Column;
use Cascade\Dashboard\Services\Datatables\DataTable;
use Cascade\Users\Entities\AuthenticationHistory;

class AuthenticationHistoryTable extends DataTable
{

    public function __construct($params = null)
    {
        $this->editColumn('status', function(AuthenticationHistory $log){
            return $log->is_valid ? $this->success() : $this->error();
        });
    }

    public function query($search = null, $filters = [])
    {
        $query = AuthenticationHistory::query();

        return $query;
    }

    public function columns()
    {
        return [
            Column::make('status')->text(__('Status')),
            Column::make('username')->text(__('Username')),
            Column::make('ip_address')->text(__('IP Address')),
            Column::make('client')->text(__('User Agent')),
            Column::make('created_at')->text(__('Issued At'))
        ];
    }
}
