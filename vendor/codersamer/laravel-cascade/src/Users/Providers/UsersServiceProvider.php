<?php

namespace Cascade\Users\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;
use Illuminate\Support\Facades\Gate;
use Cascade\Dashboard\Entities\MenuItem;
use Cascade\Dashboard\Entities\MenuSection;
use Cascade\Dashboard\Entities\Statistic;
use Cascade\Dashboard\Services\DashboardBuilder;
use Cascade\Dashboard\Services\DashboardMenu;
use Cascade\Settings\Services\Settings;
use Cascade\System\Enums\BootstrapColors;
use Cascade\System\Services\NotificationService;
use Cascade\System\Services\System;
use Cascade\Tenancy\Enums\TenantMode;
use Cascade\Tenancy\Services\TenancyService;
use Cascade\Users\Components\Auth\ForgotPasswordComponent;
use Cascade\Users\Components\Auth\LoginFormComponent;
use Cascade\Users\Components\Auth\RegisterFormComponent;
use Cascade\Users\Components\Auth\ResetPasswordFormComponent;
use Cascade\Users\Components\Central\UserNavbarAction as CentralUserNavbarAction;
use Cascade\Users\Components\UserNavbarAction;
use Cascade\Users\Console\ScanPermissionsCommand;
use Cascade\Users\Entities\Role;
use Cascade\Users\Entities\User;
use Cascade\Users\Handlers\ScanPermissionsHandler;
use Cascade\Users\Http\Controllers\Auth\RegisteredUserController;
use Cascade\Users\Notifications\NewUserRegisteration;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\Event;
use Cascade\Users\Listeners\AccountRegisterationListener;
use Cascade\Users\Tabs\User\UserBasicTab;
use Illuminate\Support\Facades\Artisan;
use SebastianBergmann\CodeCoverage\Report\Html\Dashboard;

class UsersServiceProvider extends ServiceProvider
{
    /**
     * @var string $moduleName
     */
    protected $moduleName = 'Users';

    /**
     * @var string $moduleNameLower
     */
    protected $moduleNameLower = 'users';

    protected $commands = [
        ScanPermissionsCommand::class
    ];


    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerTranslations();
        $this->registerConfig();
        $this->registerViews();
        $this->loadMigrationsFrom(module_path($this->moduleName, 'Database/Migrations'));
        
        if(!$this->app->runningInConsole())
        {
            $this->registerMenu();
            $this->registerStatistics();
            $this->registerSettings();
            $this->registerComponents();
            User::AddTab(UserBasicTab::class);
            $this->registerCommands();
        }
        System::OnAction('rebuild', ScanPermissionsHandler::class);
    }

    protected function registerComponents()
    {
        Blade::component(LoginFormComponent::class, 'login-form', 'users');
        Blade::component(RegisterFormComponent::class, 'register-form', 'users');
        Blade::component(ForgotPasswordComponent::class, 'forgot-password-form', 'users');
        Blade::component(ResetPasswordFormComponent::class, 'reset-password-form', 'users');
    }

    protected function registerSettings()
    {
        //Publish Configs
        config([
            //Facebook
            'services.facebook.client_id' => get_setting('users::authentication_drivers.facebook.client_id', ''),
            'services.facebook.client_secret' => get_setting('users::authentication_drivers.facebook.client_secret', ''),
            'services.facebook.redirect' => url('auth/checkpoint/facebook'),

            //Twitter
            'services.twitter.client_id' => get_setting('users::authentication_drivers.twitter.client_id', ''),
            'services.twitter.client_secret' => get_setting('users::authentication_drivers.twitter.client_secret', ''),
            'services.twitter.redirect' => url('auth/checkpoint/twitter'),
            //Google
            'services.google.client_id' => get_setting('users::authentication_drivers.google.client_id', ''),
            'services.google.client_secret' => get_setting('users::authentication_drivers.google.client_secret', ''),
            'services.google.redirect' => url('auth/checkpoint/google'),
            //Github
            'services.github.client_id' => get_setting('users::authentication_drivers.github.client_id', ''),
            'services.github.client_secret' => get_setting('users::authentication_drivers.github.client_secret', ''),
            'services.github.redirect' => url('auth/checkpoint/github'),
        ]);

        Settings::module('users', __('Users'), function(){
            Settings::page(__('General'), 'general', function(){
                Settings::group(__('Registeration'), function(){
                    Settings::checkbox(__('Enable Register'), 'register.enabled');
                    Settings::select(__('Default Status'), 'register.status', function(){
                        $data = [];
                        foreach(get_users_statuses() as $case)
                        {
                            $data[$case->value] = $case->name;
                        }
                        return $data;
                    });
                    Settings::select(__('Default Role'), 'register.role', function(){
                        return Role::all()->pluck('name', 'id');
                    });
                    Settings::multiple(__('Available Roles'), 'register.available_roles', function(){
                        return Role::all()->pluck('name', 'id');
                    });
                    Settings::checkbox(__('Requires Verification'), 'register.verification');
                    Settings::checkbox(__('Requires Review'), 'register.review');
                });
            });
            Settings::page('Authentication Drivers', 'authentication_drivers', function(){
                Settings::group('Enabled Drivers', function(){
                    Settings::checkbox(__('Facebook'), 'facebook');
                    Settings::checkbox(__('Twitter'), 'twitter');
                    Settings::checkbox(__('Google'), 'google');
                    Settings::checkbox(__('Github'), 'github');
                });

                Settings::group('Facebook Integration', function(){
                    Settings::note(__('OAuth Callback URL'). ' : <code>'.url('auth/checkpoint/facebook').'</code>');
                    Settings::text(__('Client ID'), 'facebook.client_id');
                    Settings::text(__('Client Secret'), 'facebook.client_secret');
                });

                Settings::group('Twitter Integration', function(){
                    Settings::note(__('OAuth Callback URL'). ' : <code>'.url('auth/checkpoint/twitter').'</code>');
                    Settings::text(__('Client ID'), 'twitter.client_id');
                    Settings::text(__('Client Secret'), 'twitter.client_secret');
                });

                Settings::group('Google Integration', function(){
                    Settings::note(__('OAuth Callback URL'). ' : <code>'.url('auth/checkpoint/google').'</code>');
                    Settings::text(__('Client ID'), 'google.client_id');
                    Settings::text(__('Client Secret'), 'google.client_secret');
                });

                Settings::group('Github Integration', function(){
                    Settings::note(__('OAuth Callback URL'). ' : <code>'.url('auth/checkpoint/github').'</code>');
                    Settings::text(__('Client ID'), 'github.client_id');
                    Settings::text(__('Client Secret'), 'github.client_secret');
                });
            });
        });
    }

    protected function registerStatistics()
    {
        DashboardBuilder::AddStatistic(
            Statistic::make(__('Users'), User::where('type', User::class)->orWhere('type', null)->count())->icon('user-group')->color(BootstrapColors::Red)
        );
    }

    protected function registerCommands()
    {
        $this->commands([ScanPermissionsCommand::class]);
    }

    /**
     * Register Module Menu
     *
     * @return void
     */
    public function registerMenu()
    {

        DashboardBuilder::Menu('system-overview')->Call(function(DashboardMenu $menu){

            $menu->AddItems([
                    MenuItem::make(__('Users Accounts'))->description(__('Registered Users Accounts'))->icon('users')->route('dashboard.users.index')->permission('view users'),
                    MenuItem::make(__('Roles Management'))->icon('user-shield')->description(__('Users Roles & Permissions'))->route('dashboard.users.roles.index')->permission('view roles'),
                    MenuItem::make(__('Permissions Requests'))->icon('clipboard-check')->description(__('Grant Requested Permissions'))->route('dashboard.users.permissions.requests.index')->permission('view permissions requests'),
                    MenuItem::make(__('Auth Log'))->icon('history')->description(__('Login History Review'))->route('dashboard.users.authentication.history')->permission('view roles'),
            ]);
        });

        DashboardBuilder::Central(function(){
            DashboardBuilder::MainMenu()->AddItems([
                MenuItem::make(__('Accounts'))->description(__('Registered Users Accounts'))->icon('users')->permission('view users')->children([
                    MenuItem::make(__('Users Accounts'))->description(__('Registered Users Accounts'))->icon('users')->route('dashboard.users.index')->permission('view users'),
                    MenuItem::make(__('Roles Management'))->icon('user-shield')->description(__('Users Roles & Permissions'))->route('dashboard.users.roles.index')->permission('view roles'),
                    MenuItem::make(__('Permissions Requests'))->icon('clipboard-check')->description(__('Grant Requested Permissions'))->route('dashboard.users.permissions.requests.index')->permission('view permissions requests'),
                    MenuItem::make(__('Auth Log'))->icon('history')->description(__('Login History Review'))->route('dashboard.users.authentication.history')->permission('view roles'),
                ]),
            ]);
        });


         DashboardBuilder::RegisterNavbarAction(new UserNavbarAction);
    }

    /**
     * Register Macros found in ../Macros Directory
     *
     * @return void
     */
    public function registerMacros()
    {
        $macros = array_diff(scandir(__DIR__ . '/../Macros/'), ['.', '..']);
        foreach ($macros as $macro) {
            require_once(__DIR__ . '/../Macros/' . $macro);
        }
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->registerMacros();
        $this->registerCommands();
        NotificationService::Register(__('New Registeration'), 'new_registeration', NewUserRegisteration::class);
        $this->app->register(RouteServiceProvider::class);
        Event::listen(Registered::class, [AccountRegisterationListener::class, 'handle']);
        Gate::before(function($user, $ability){
            return $user->is_super_admin ? true : null;
        });
    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->publishes([
            module_path($this->moduleName, 'Config/config.php') => config_path($this->moduleNameLower . '.php'),
        ], 'config');
        $this->mergeConfigFrom(
            module_path($this->moduleName, 'Config/config.php'), $this->moduleNameLower
        );
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $viewPath = resource_path('views/modules/' . $this->moduleNameLower);

        $sourcePath = module_path($this->moduleName, 'Resources/views');

        $this->publishes([
            $sourcePath => $viewPath
        ], ['views', $this->moduleNameLower . '-module-views']);

        $this->loadViewsFrom(array_merge($this->getPublishableViewPaths(), [$sourcePath]), $this->moduleNameLower);
    }

    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $langPath = resource_path('lang/modules/' . $this->moduleNameLower);

        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, $this->moduleNameLower);
        } else {
            $this->loadTranslationsFrom(module_path($this->moduleName, 'Resources/lang'), $this->moduleNameLower);
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }

    private function getPublishableViewPaths(): array
    {
        $paths = [];
        foreach (\Config::get('view.paths') as $path) {
            if (is_dir($path . '/modules/' . $this->moduleNameLower)) {
                $paths[] = $path . '/modules/' . $this->moduleNameLower;
            }
        }
        return $paths;
    }
}
