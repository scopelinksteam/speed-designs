<?php

return [
    'create users' => 'Allow user to create new accounts',
    'view users' => 'View Registered Users Accounts',
    'edit users' => 'Edit and Manage Users Details',
    'delete users' => 'Ability to Delete Users Accounts',
    'set super admin' => 'Set Users Accounts to be Super Admins',
    'view roles' => 'Show and List Roles',
    'create roles' => 'Allow user to create new Roles',
    'edit roles' => 'Edit and Manage Roles Details',
    'delete roles' => 'Allow users to delete Roles',
    'manage roles permissions' => 'Allow Users to set roles associated permissions',
    'set users status' => 'Allow User to change users statuses',
    'scan permissions' => 'Allow users to make a permissions scan',
    'approve permissions requests' => 'Approve Incoming Permissions Requests',
    'reject permissions requests' => 'Reject Incoming Permissions Requests',
    'view permissions requests' => 'View Incoming Permissions Requests',
];
