<?php

namespace Cascade\Users\Handlers;

use Cascade\Users\Entities\Permission;
use Nwidart\Modules\Facades\Module;
use Symfony\Component\Finder\Finder;

class ScanPermissionsHandler
{
    public function Handle()
    {
        Permission::unguard();
        $permissions = [];
        $scanPathes = ['Cascade', 'Modules', 'Plugins', 'Themes'];

        foreach($scanPathes as $path)
        {
            $absolutePath = base_path($path);
            $finder = new Finder();
            $files = $finder->in($absolutePath)->depth(2)->name('permissions.php')->files();
            foreach($files as $file)
            {
                $ownerName = dirname($file->getRelativePath(), 1);
                $permissions[\Str::singular($path).':'.$ownerName] = require_once $file;
            }
        }


        $dbPermissions = Permission::all();
        $guards = ['web'];
        foreach($permissions as $ownerName => $items)
        {
            foreach($items as $permission => $description)
            {
                foreach($guards as $guard)
                {
                    $exist = $dbPermissions->where('name', $permission)->where('owner', $ownerName)->where('guard_name', $guard)->first();
                    if($exist != null)
                    {
                        if($exist->description != $description) { $exist->description = $description; $exist->save(); }
                    }
                    else
                    {
                        $newPermission = new Permission([
                            'name' => $permission,
                            'guard_name' => $guard,
                            'description' => $description,
                            'owner' => $ownerName
                        ]);
                        $newPermission->save();
                    }
                }

            }
        }
    }
}
