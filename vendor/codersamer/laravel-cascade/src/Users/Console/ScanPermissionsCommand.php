<?php

namespace Cascade\Users\Console;

use Cascade\Users\Entities\Permission;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Finder\Finder;

class ScanPermissionsCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'permissions:scan';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Permission::unguard();
        $permissions = [];
        $scanPathes = [ 'Cascade', 'Modules', 'Plugins', 'Themes'];

        foreach($scanPathes as $path)
        {
            $absolutePath = $path == 'Cascade' ? __DIR__.'/../../' : base_path($path);
            $finder = new Finder();
            $files = $finder->in($absolutePath)->depth(2)->name('permissions.php')->files();
            foreach($files as $file)
            {
                $ownerName = dirname($file->getRelativePath(), 1);
                $permissions[\Str::singular($path).':'.$ownerName] = require_once $file;
            }
        }


        $dbPermissions = Permission::all();
        $guards = ['web'];
        foreach($permissions as $ownerName => $items)
        {
            foreach($items as $permission => $description)
            {
                foreach($guards as $guard)
                {
                    $exist = $dbPermissions->where('name', $permission)->where('owner', $ownerName)->where('guard_name', $guard)->first();
                    if($exist != null)
                    {
                        if($exist->description != $description) { $exist->description = $description; $exist->save(); }
                    }
                    else
                    {
                        $newPermission = new Permission([
                            'name' => $permission,
                            'guard_name' => $guard,
                            'description' => $description,
                            'owner' => $ownerName
                        ]);
                        $newPermission->save();
                    }
                }

            }
        }

    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['example', InputArgument::REQUIRED, 'An example argument.'],
        ];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null],
        ];
    }
}
