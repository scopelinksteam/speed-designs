<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\Auth\ConfirmablePasswordController;
use App\Http\Controllers\Auth\EmailVerificationNotificationController;
use App\Http\Controllers\Auth\EmailVerificationPromptController;
use App\Http\Controllers\Auth\NewPasswordController;
use App\Http\Controllers\Auth\PasswordResetLinkController;
use App\Http\Controllers\Auth\VerifyEmailController;
use Cascade\Users\Http\Controllers\Auth\AuthenticatedSessionController;
use Cascade\Users\Http\Controllers\Auth\LoginController;
use Cascade\Users\Http\Controllers\Auth\RegisteredUserController;
use Cascade\Users\Http\Controllers\Central\UsersController as CentralUsersController;
use Cascade\Users\Http\Controllers\Dashboard\AuthenticationHistoryController;
use Cascade\Users\Http\Controllers\Dashboard\PermissionsController;
use Illuminate\Support\Facades\Route;
use Cascade\Users\Http\Controllers\Dashboard\RolesController;
use Cascade\Users\Http\Controllers\Dashboard\UsersController;
use Illuminate\Http\Request;


Route::hybrid(function(){
    Route::get('/', [UsersController::class, 'index'])->name('index');
    Route::get('/profile', [UsersController::class, 'profile'])->name('profile');
    Route::post('/profile', [UsersController::class, 'storeProfile'])->name('profile.store');
    Route::get('/create', [UsersController::class, 'upsert'])->name('create');
    Route::get('/edit/{user}', [UsersController::class, 'upsert'])->name('edit');
    Route::get('/delete/{user}', [UsersController::class, 'destroy'])->name('delete');
    Route::post('/', [UsersController::class, 'store'])->name('store');
    Route::prefix('authentication')->as('authentication.')->group(function(){
        Route::get('/history', [AuthenticationHistoryController::class, 'history'])->name('history');
        Route::get('/history/clear', [AuthenticationHistoryController::class, 'clear'])->name('history.clear');
    });
    Route::prefix('roles')->as('roles.')->group(function(){
        Route::get('/', [RolesController::class, 'index'])->name('index');
        Route::post('/', [RolesController::class, 'store'])->name('store');
        Route::get('/edit/{role}', [RolesController::class, 'index'])->name('edit');
        Route::get('/{role}/permissions', [RolesController::class, 'permissions'])->name('permissions');
        Route::post('/{role}/permissions', [RolesController::class, 'storePermissions'])->name('permissions.store');
        Route::get('/delete/{role}', [RolesController::class, 'destroy'])->name('delete');
        Route::get('/scan', [RolesController::class, 'scanPermissions'])->name('scan')->middleware('can:scan permissions');
    });
    Route::prefix('permissions')->as('permissions.')->group(function(){
        Route::get('/request/{permission}', [PermissionsController::class, 'request'])->name('request');
        Route::prefix('requests')->as('requests.')->group(function(){
            Route::get('/', [PermissionsController::class, 'requests'])->name('index');
            Route::get('/approve/{request}', [PermissionsController::class, 'approveRequest'])->name('approve');
            Route::get('/reject/{request}', [PermissionsController::class, 'rejectRequest'])->name('reject');
        });
    });
}, 'users');

Route::central(function(){

    //Tenant Users
    Route::prefix('tenant/{tenant}')->as('tenant.')->group(function(){
        Route::get('/', [CentralUsersController::class, 'index'])->name('index');
        Route::get('/profile', [CentralUsersController::class, 'profile'])->name('profile');
        Route::get('/create', [CentralUsersController::class, 'upsert'])->name('create');
        Route::get('/edit/{user}', [CentralUsersController::class, 'upsert'])->name('edit');
        Route::get('/delete/{user}', [CentralUsersController::class, 'destroy'])->name('delete');
        Route::post('/', [CentralUsersController::class, 'store'])->name('store');
    });

}, 'users');

Route::get('/request', function(Request $request){
    dd($request->getClientIp());
});

Route::prefix('auth')->middleware('guest')->as('auth.')->group(function(){


    Route::prefix('login')->as('login.')->group(function(){

        $facebookDriverEnabled = (bool)get_setting('users::authentication_drivers.facebook', false);
        $twitterDriverEnabled = (bool)get_setting('users::authentication_drivers.twitter', false);
        $googleDriverEnabled = (bool)get_setting('users::authentication_drivers.google', false);
        $githubDriverEnabled = (bool)get_setting('users::authentication_drivers.github', false);

        if($facebookDriverEnabled)
        { Route::get('facebook', [LoginController::class, 'facebook'])->name('facebook'); }

        if($twitterDriverEnabled)
        { Route::get('twitter', [LoginController::class, 'twitter'])->name('twitter');}

        if($googleDriverEnabled)
        { Route::get('google', [LoginController::class, 'google'])->name('google'); }

        if($githubDriverEnabled)
        { Route::get('github', [LoginController::class, 'github'])->name('github');}
    });
    Route::prefix('checkpoint')->as('checkpoint.')->group(function(){

        $facebookDriverEnabled = (bool)get_setting('users::authentication_drivers.facebook', false);
        $twitterDriverEnabled = (bool)get_setting('users::authentication_drivers.twitter', false);
        $googleDriverEnabled = (bool)get_setting('users::authentication_drivers.google', false);
        $githubDriverEnabled = (bool)get_setting('users::authentication_drivers.github', false);

        if($facebookDriverEnabled)
        {Route::get('facebook', [LoginController::class, 'facebookCallback'])->name('facebook');}

        if($twitterDriverEnabled)
        {Route::get('twitter', [LoginController::class, 'twitterCallback'])->name('twitter');}

        if($googleDriverEnabled)
        {Route::get('google', [LoginController::class, 'googleCallback'])->name('google');}

        if($githubDriverEnabled)
        {Route::get('github', [LoginController::class, 'githubCallback'])->name('github');}
    });
});



Route::get('/register', [RegisteredUserController::class, 'create'])->middleware('guest')->name('register');
Route::post('/register', [RegisteredUserController::class, 'store'])->middleware('guest');
Route::get('/login', [AuthenticatedSessionController::class, 'create'])
                ->middleware('guest')
                ->name('login');

Route::post('/login', [AuthenticatedSessionController::class, 'store'])
                ->middleware('guest');

Route::get('/forgot-password', [PasswordResetLinkController::class, 'create'])
                ->middleware('guest')
                ->name('password.request');

Route::post('/forgot-password', [PasswordResetLinkController::class, 'store'])
                ->middleware('guest')
                ->name('password.email');

Route::get('/reset-password/{token}', [NewPasswordController::class, 'create'])
                ->middleware('guest')
                ->name('password.reset');

Route::post('/reset-password', [NewPasswordController::class, 'store'])
                ->middleware('guest')
                ->name('password.update');

Route::get('/verify-email', [EmailVerificationPromptController::class, '__invoke'])
                ->middleware('auth')
                ->name('verification.notice');

Route::get('/verify-email/{id}/{hash}', [VerifyEmailController::class, '__invoke'])
                ->middleware(['auth', 'signed', 'throttle:6,1'])
                ->name('verification.verify');

Route::post('/email/verification-notification', [EmailVerificationNotificationController::class, 'store'])
                ->middleware(['auth', 'throttle:6,1'])
                ->name('verification.send');

Route::get('/confirm-password', [ConfirmablePasswordController::class, 'show'])
                ->middleware('auth')
                ->name('password.confirm');

Route::post('/confirm-password', [ConfirmablePasswordController::class, 'store'])
                ->middleware('auth');

Route::get('/logout', [AuthenticatedSessionController::class, 'destroy'])
                ->middleware('auth')
                ->name('logout');
