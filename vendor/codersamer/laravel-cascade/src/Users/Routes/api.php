<?php

use Cascade\Users\Http\Controllers\API\AuthenticationController;
use Cascade\Users\Http\Controllers\API\ProfileController;
use Cascade\Users\Http\Controllers\API\RegisterController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
/*
Route::get('users/debug', function(){
    return ['Hello', 'World'];
});
*/

Route::ApiGuest(function(){
    Route::prefix('auth')->as('auth.')->group(function(){
        Route::post('login', [AuthenticationController::class, 'login'])->name('login');
        Route::post('register', [RegisterController::class, 'register'])->name('register');
    });
}, 'users');

Route::Api(function(){
    Route::prefix('auth')->as('auth.')->group(function(){
        Route::get('/me', [AuthenticationController::class, 'me'])->name('me');
        Route::post('/profile', [ProfileController::class, 'update'])->name('update');
    });
    Route::get('debug', function(){
        return auth()->user();
    });
}, 'users');

