<?php

use Cascade\Users\Http\Controllers\Frontend\ProfileController;
use Illuminate\Support\Facades\Route;

Route::prefix('profile')->name('profile.')->group(function(){
    Route::get('/', [ProfileController::class, 'index'])->name('index');
    Route::get('/show/{user}', [ProfileController::class, 'show'])->name('show');
    Route::get('/edit', [ProfileController::class, 'edit'])->name('edit');
    Route::post('/', [ProfileController::class, 'store'])->name('store');
});
