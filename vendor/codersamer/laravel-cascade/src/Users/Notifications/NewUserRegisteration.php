<?php

namespace Cascade\Users\Notifications;

use Cascade\System\Abstraction\NotificationBase;
use Cascade\Users\Entities\User;
use Illuminate\Notifications\Messages\MailMessage;

class NewUserRegisteration extends NotificationBase
{

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(public User $user) { }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
        ->view('users::mail.new_user_registered', [
            'user' => $this->user
        ]);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [];
    }

    public function notifiables()
    {
        return User::where('is_super_admin', 1)->get();
    }

    public function reference()
    {
        return $this->user;
    }

    public function title()
    {
        return __('New Account Registeration');
    }

    public function icon()
    {
        return '<i class="fas fa-user-plus"></i>';
    }

    public function link()
    {
        return route('dashboard.users.edit', ['user' => $this->user]);
    }
}
