<?php

namespace Cascade\Users\Http\Controllers\Auth;

use Cascade\Users\Entities\User;
use Cascade\Users\Enums\UsersProviders;
use Illuminate\Contracts\Auth\UserProvider;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Hash;
use Laravel\Socialite\Facades\Socialite;
use Laravel\Socialite\Two\User as SocialUser;


class LoginController extends Controller
{

    //Facebook Redirect
    public function facebook() { return Socialite::driver('facebook')->redirect(); }
    //Facebook Authentication
    public function facebookCallback() { return $this->AuthenticateSocialUser(UsersProviders::Facebook); }
    //Twitter Redirect
    public function twitter() { return Socialite::driver('twitter')->redirect(); }
    //Twitter Authentication
    public function twitterCallback() { return $this->AuthenticateSocialUser(UsersProviders::Twitter); }
    //Google Redirect
    public function google() { return Socialite::driver('google')->redirect(); }
    //Google Authentication
    public function googleCallback() { return $this->AuthenticateSocialUser(UsersProviders::Google); }
    //Github Redirect
    public function github() { return Socialite::driver('github')->redirect(); }
    //Github Authentication
    public function githubCallback() { return $this->AuthenticateSocialUser(UsersProviders::Github); }

    protected function AuthenticateSocialUser(UsersProviders $provider)
    {
        $user = Socialite::driver($provider->value)->user();

        if($user == null) { return ; }

        $dbUser = User::updateOrCreate([
            'email' => $user->email
        ], [
            'name' => $user->name,
            'password' => Hash::make(\Str::random(20)),
            'provider' => $provider->value,
            'provider_id' => $user->id,
            'locale' => 'en'
        ]);

        auth()->login($dbUser);
        return redirect('/');
    }

}
