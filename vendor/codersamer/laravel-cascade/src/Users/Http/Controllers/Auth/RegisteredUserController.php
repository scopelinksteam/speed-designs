<?php

namespace Cascade\Users\Http\Controllers\Auth;

use Cascade\System\Components\ReCaptchaComponent;
use Cascade\System\Http\Rules\RecaptchaRule;
use Cascade\System\Services\Action;
use Cascade\System\Services\Filter;
use Cascade\Users\Actions\RegisteredAction;
use Cascade\Users\Actions\RegisteringAction;
use Cascade\Users\Entities\User;
use Cascade\Users\Filters\RegisterValidationRulesFilter;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rules\Password;

class RegisteredUserController extends \App\Http\Controllers\Auth\RegisteredUserController
{
    /**
     * Handle an incoming registration request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request) : \Illuminate\Http\RedirectResponse
    {
        $validationRules = [
            'name' => ['required_without_all:first_name,last_name', 'string', 'max:255'],
            'first_name' => ['required_without_all:name', 'string', 'max:255'],
            'last_name' => ['required_without_all:name', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users,email'],
            'phone' => ['nullable', 'string', 'numeric', 'unique:users,phone'],
            'password' => ['required', 'confirmed', Password::defaults()],
        ];

        $recaptchaSiteKey = get_setting('integrations::recaptcha.credentials.site_key', '', false);
        if(!empty($recaptchaSiteKey)) 
        { 
            $validationRules['recaptcha'] = ['required', new RecaptchaRule];
        }

        $validationRules = Filter::Apply(RegisterValidationRulesFilter::class, $validationRules, $request);

        $request->validate($validationRules);
        $shouldContinue = Action::Apply(RegisteringAction::class, $request);
        if(!$shouldContinue)
        {
            return back();
        }
        $user = User::create([
            'name' => $request->name ?? ($request->first_name .' '.$request->last_name),
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'phone' => $request->phone ?? '',
            'status' => intval(get_setting('users::general.register.status', 0))
        ]);

        foreach($request->input('attributes', []) as $key => $val)
        {
            $user->storeAttribute($key, $val);
        }

        foreach($request->file('attributes', []) as $key => $val)
        {
            $user->storeAttribute($key, $val->store('users/attributes', ['disk' => 'uploads']));
        }

        Action::Apply(RegisteredAction::class, $user, $request);
        event(new Registered($user));

        Auth::login($user);

        return response()->noContent();
    }
}