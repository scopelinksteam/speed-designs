<?php

namespace Cascade\Users\Http\Controllers\Central;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Hash;
use Cascade\System\Services\Feedback;
use Cascade\Tenancy\Entities\Tenant;
use Cascade\Users\Datatables\Central\UsersDatatable as CentralUsersDatatable;
use Cascade\Users\Entities\Role;
use Cascade\Users\Entities\User;
use Cascade\Users\Http\Requests\Dashboard\CreateUserRequest;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index(Tenant $tenant)
    {
        Gate::authorize('view users');
        return view('users::central.users.list', ['table' => CentralUsersDatatable::class, 'tenant' => $tenant]);
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function upsert(Tenant $tenant, User $user = null)
    {
        $tenant->manager->SetDatabase();
        if($user == null) { Gate::authorize('create users'); }
        else { Gate::authorize('edit users'); }

        return view('users::central.users.upsert', [
            'user' => $user,
            'roles' => Role::all(),
            'tenant' => $tenant
        ]);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Tenant $tenant, Request $request)
    {
        $tenant->manager->SetDatabase();

        $request->validate([
            'name' => ['required', 'min:3'],
            'email' => ['required', 'email', 'unique:users,email,'.$request->get('entity_id')],
            'password' => ['required_without:entity_id'],
            'confirm_password' => ['same:password', 'required_without:entity_id']
        ]);

        if($request->has('entity_id')) { Gate::authorize('edit users'); }
        else { Gate::authorize('create users'); }
        $user = $request->has('entity_id') ? User::find(intval($request->entity_id)) : new User();
        if($request->filled('password')) { $user->password = bcrypt($request->password); }
        $user->name = $request->name;
        $user->email = $request->email;
        $user->locale = $request->locale ?? 'en';
        //Set Super Admin
        if(Gate::allows('set super admin')) { $user->is_super_admin = $request->has('is_super_admin'); }
        if(Gate::allows('set users status')) { $user->status = intval($request->input('status', 0)); }
        $user->save();

        if($request->file('avatar'))
        {
            $user->avatar_url = $request->avatar->store('avatars', ['disk' => 'uploads']);
            $user->save();
        }

        if($request->has('roles'))
        {
            $user->syncRoles($request->roles);
        }


        Feedback::getInstance()->addSuccess(__('User Saved Successfully'));
        Feedback::flash();
        return redirect()->route('central.users.tenant.index', ['tenant' => $tenant]);
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy(Tenant $tenant, User $user)
    {
        Gate::authorize('delete users');
        $user->delete();
        Feedback::getInstance()->addSuccess(__('User Deleted Successfully'));
        Feedback::flash();
        return redirect()->back();
    }

    public function profile(Tenant $tenant)
    {

        return view('users::central.users.tenant.profile', [
            'user' => auth()->user()
        ]);
    }
}
