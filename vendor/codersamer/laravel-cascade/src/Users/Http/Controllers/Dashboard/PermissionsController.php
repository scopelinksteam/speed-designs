<?php

namespace Cascade\Users\Http\Controllers\Dashboard;

use Cascade\System\Services\Feedback;
use Cascade\Users\Datatables\PermissionsRequestsTable;
use Cascade\Users\Entities\Permission;
use Cascade\Users\Entities\PermissionRequest;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class PermissionsController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function request(Permission $permission)
    {
        PermissionRequest::Request($permission);
        Feedback::getInstance()->addSuccess(__('Permission Request Submitted Successfully and Waiting for Apporval'));
        Feedback::flash();
        return back();
    }

    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function requests(PermissionsRequestsTable $table)
    {
        return view('users::dashboard.permissions.requests', [
            'table' => $table
        ]);
    }

    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function approveRequest(PermissionRequest $request)
    {
        $request->Approve();
        $request->save();
        Feedback::getInstance()->addSuccess(__('Permission Request Approved Successfully'));
        Feedback::flash();
        return back();
    }

    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function rejectRequest(PermissionRequest $request)
    {
        $request->Reject();
        $request->save();
        Feedback::getInstance()->addSuccess(__('Permission Request Rejected Successfully'));
        Feedback::flash();
        return back();
    }

}
