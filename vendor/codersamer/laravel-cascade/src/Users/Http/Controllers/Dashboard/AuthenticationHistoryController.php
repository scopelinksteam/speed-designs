<?php

namespace Cascade\Users\Http\Controllers\Dashboard;

use Cascade\System\Services\Feedback;
use Cascade\Users\Datatables\AuthenticationHistoryTable;
use Cascade\Users\Entities\AuthenticationHistory;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class AuthenticationHistoryController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function history(AuthenticationHistoryTable $table)
    {
        return view('users::dashboard.authentication.history', [
            'table' => $table
        ]);
    }

    public function clear()
    {
        AuthenticationHistory::truncate();
        Feedback::alert(__('Authentication History Cleared Successfully'));
        Feedback::flash();
        return back();
    }


}
