<?php

namespace Cascade\Users\Http\Controllers\Dashboard;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Cascade\System\Services\Feedback;
use Cascade\System\Services\Modules;
use Cascade\System\Services\Plugins;
use Cascade\Users\Datatables\RolesDatatable;
use Cascade\Users\Entities\Permission;
use Cascade\Users\Entities\Role;
use Cascade\Users\Http\Requests\Dashboard\CreateRoleRequest;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Gate;
use Nwidart\Modules\Facades\Module;

class RolesController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index(Role $role = null)
    {
        $rolesTable = new RolesDatatable();
        return view('users::roles.index', [
            'table' => $rolesTable,
            'role' => $role
        ]);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(CreateRoleRequest $request)
    {
        $entity = $request->has('entity_id') ? Role::find($request->entity_id) : new Role();
        $entity->name = $request->name;
        $entity->save();
        Feedback::getInstance()->addSuccess(__('Role Saved Successfully'));
        Feedback::flash();
        return redirect()->route('dashboard.users.roles.index');
    }


    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy(Role $role)
    {
        if($role->users()->count() > 0)
        {
            Feedback::getInstance()->addError(__('Selected Role has Users Associated, which means it cannot be deleted'));
            Feedback::flash();
            return redirect()->back();
        }
        $role->delete();
        Feedback::getInstance()->addSuccess(__('Role Deleted Successfully'));
        Feedback::flash();
        return redirect()->back();
    }

    /**
     * Set Role Permissions
     *
     * @param Role $role
     */
    public function permissions(Role $role)
    {
        $permissions = Permission::all();

        $sections = [
            Permission::OWNER_TYPE_CASCADE => [
                'title' => __('System Permissions'),
                'permissions' => []
            ],
            Permission::OWNER_TYPE_MODULE => [
                'title' => __('Modules Permissions'),
                'permissions' => []
            ],
            Permission::OWNER_TYPE_PLUGIN => [
                'title' => __('Plugins Permissions'),
                'permissions' => []
            ],
            Permission::OWNER_TYPE_THEME => [
                'title' => __('Themes Permissions'),
                'permissions' => []
            ]
        ];

        foreach($permissions as $permission)
        {
            if(!isset($sections[$permission->owner_type]['permissions'][$permission->owner_name]))
            { $sections[$permission->owner_type]['permissions'][$permission->owner_name] = []; }

            if($permission->IsOwnedBy(Permission::OWNER_TYPE_CASCADE))
            {

            }
            if($permission->IsOwnedBy(Permission::OWNER_TYPE_MODULE) && !Modules::Enabled($permission->owner_name)) { continue; }
            if($permission->IsOwnedBy(Permission::OWNER_TYPE_PLUGIN) && !Plugins::IsEnabled($permission->owner_name)) { continue; }

            $sections[$permission->owner_type]['permissions'][$permission->owner_name][] = $permission;
        }
        return view('users::roles.permissions',[
            'role' => $role,
            'sections' => $sections,
        ]);
    }

    /**
     * Store Role Permissions
     *
     * @param Request $request
     * @param Role $role
     */
    public function storePermissions(Request $request, Role $role)
    {
        $owner = $request->owner;
        $segments = explode(':',$owner);
        $requestedPermissions = $request->has('permissions') ? $request->permissions : [];
        $modulePermissions = Permission::where('owner',$owner)->get();
        foreach($modulePermissions as $item)
        {
            if(in_array($item->id, $requestedPermissions)) { $role->givePermissionTo($item->name); }
            else { $role->revokePermissionTo($item->name); }
        }
        Feedback::getInstance()->addSuccess(__('Permissions Applied Successfully'.' : '.$segments[1]));
        Feedback::flash();
        return redirect()->back();
    }

    public function scanPermissions()
    {
        Gate::authorize('scan permissions');
        Artisan::call('permissions:scan');
        Feedback::getInstance()->addSuccess(__('Permissions Scanned Successfully'));
        Feedback::flash();
        return redirect()->back();
    }
}
