<?php

namespace Cascade\Users\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Cascade\System\Services\Action;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Hash;
use Cascade\System\Services\Feedback;
use Cascade\Users\Actions\UserStoreAction;
use Cascade\Users\Datatables\UsersDatatable;
use Cascade\Users\Entities\Role;
use Cascade\Users\Entities\User;
use Cascade\Users\Http\Requests\Dashboard\CreateUserRequest;
use Illuminate\Support\Arr;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index(UsersDatatable $table)
    {
        Gate::authorize('view users');
        return view('users::users.list', ['table' => $table]);
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function upsert(User $user = null)
    {
        if($user == null) { Gate::authorize('create users'); }
        else { Gate::authorize('edit users'); }

        return view('users::users.upsert', [
            'user' => $user,
            'roles' => Role::all(),
            'tabs' => User::GetTabs(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(CreateUserRequest $request)
    {
        if($request->has('entity_id')) { Gate::authorize('edit users'); }
        else { Gate::authorize('create users'); }
        $user = $request->has('entity_id') ? User::find(intval($request->entity_id)) : new User();
        if($request->filled('password')) { $user->password = bcrypt($request->password); }
        $user->name = $request->name;
        $user->email = $request->email;
        $user->locale = $request->locale ?? 'en';
        //Set Super Admin
        if(Gate::allows('set super admin')) { $user->is_super_admin = $request->has('is_super_admin'); }
        if(Gate::allows('set users status')) { $user->status = intval($request->input('status', 0)); }
        $user->save();

        if($request->file('avatar'))
        {
            $user->avatar_url = $request->avatar->store('avatars', ['disk' => 'uploads']);
            $user->save();
        }

        if($request->has('roles'))
        {
            $user->syncRoles(Arr::wrap($request->roles));
        }

        foreach($request->input('attributes.*', []) as $attributeKey => $attributeValue)
        {
            $user->storeAttribute($attributeKey, $attributeValue);
        }
        foreach($request->file('attributes.*', []) as $key => $file)
        {
            $path = $file->store('users', ['disk' => 'uploads']);
            $user->storeAttribute($key, $path);
        }
        Action::Apply(UserStoreAction::class, $user);


        Feedback::getInstance()->addSuccess(__('User Saved Successfully'));
        Feedback::flash();
        return redirect()->route('dashboard.users.index');
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy(User $user)
    {
        Gate::authorize('delete users');
        $user->delete();
        Feedback::getInstance()->addSuccess(__('User Deleted Successfully'));
        Feedback::flash();
        return redirect()->back();
    }

    public function profile()
    {

        return view('users::users.profile', [
            'user' => auth()->user()
        ]);
    }

    public function storeProfile(Request $request)
    {

        $user = auth()->user();
        if($request->filled('password'))
        {
            if($request->password != $request->input('confirm-password'))
            {
                Feedback::error(__('Password and Confirm Password must match'));
                Feedback::flash();
                return back();
            }
            $user->password = bcrypt($request->password);
        }
        $user->name = $request->name;
        $user->email = $request->email;
        $user->save();


        if($request->file('avatar'))
        {
            $user->avatar_url = $request->avatar->store('avatars', ['disk' => 'uploads']);
            $user->save();
        }

        //Attributes
        foreach($request->input(['attributes']) as $key => $value)
        {
            $user->storeAttribute($key, $value);
        }
        foreach($request->file(['attributes']) as $key => $file)
        {
            $path = $file->store('users', ['disk' => 'uploads']);
            $user->storeAttribute($key, $path);
        }

        static::DispatchAction('storeProfile', $user);
        Feedback::getInstance()->addSuccess(__('Profile Saved Successfully'));
        Feedback::flash();
        return back();
    }
}
