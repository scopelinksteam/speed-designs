<?php

namespace Cascade\Users\Http\Controllers\API;

use Cascade\API\Enums\ResponseMessageType;
use Cascade\API\Http\Controllers\ApiController;
use Cascade\API\Services\ApiResponse;
use Cascade\Users\Entities\User;
use Cascade\Users\Http\Requests\API\LoginRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class AuthenticationController extends ApiController
{
    public function login(LoginRequest $request)
    {
        $user = User::where('username', $request->username)->orWhere('email', $request->username)->orWhere('phone', $request->username)->first();

        if($user == null)
        { return ApiResponse::SendError(__('Supplied Information doesn\'t match any records')); }

        if(!Hash::check($request->password, $user->password))
        { return ApiResponse::SendError(__('Invalid Username or Password')); }

        $token = $user->createToken('generic');
        $tokenText = $token->plainTextToken;

        return ApiResponse::SendSuccess(__('Account Logged Successfully'), [
            'token' => $tokenText,
            'user' => $user
        ]);
    }

    public function me(Request $request)
    {
        return ApiResponse::SendSuccess(__('Valid'), ['user' => auth()->user()]);
    }
}
