<?php

namespace Cascade\Users\Http\Controllers\API;

use Cascade\API\Enums\ResponseMessageType;
use Cascade\API\Http\Controllers\ApiController;
use Cascade\API\Services\ApiResponse;
use Cascade\System\Services\Action;
use Cascade\Users\Actions\RegisteredAction;
use Cascade\Users\Entities\User;
use Cascade\Users\Http\Requests\API\LoginRequest;
use Cascade\Users\Http\Requests\API\RegisterRequest;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class RegisterController extends ApiController
{

    public function register(RegisterRequest $request)
    {
        $user = new User;
        $user->name = $request->name;
        $user->phone = $request->phone;
        $user->username = $request->username;
        $user->email = $request->email;
        $user->password = \bcrypt($request->password);
        $user->locale = $request->locale ?? 'en';
        $user->save();

        foreach($request->input('attributes', []) as $key => $val)
        {
            $user->storeAttribute($key, $val);
        }

        foreach($request->file('attributes', []) as $key => $val)
        {
            $user->storeAttribute($key, $val->store('users/attributes', ['disk' => 'uploads']));
        }

        Action::Apply(RegisteredAction::class, $user, $request);
        event(new Registered($user));
        
        return ApiResponse::SendSuccess(__('Account Created Successfully'), [
            'user' => $user
        ]);
    }
    
}
