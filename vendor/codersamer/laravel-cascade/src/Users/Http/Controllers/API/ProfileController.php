<?php

namespace Cascade\Users\Http\Controllers\API;

use Cascade\API\Http\Controllers\ApiController;
use Cascade\API\Services\ApiResponse;
use Cascade\Users\Entities\User;
use Illuminate\Http\Request;

class ProfileController extends ApiController
{

    public function update(Request $request)
    {
        $user = auth()->user();

        $user->name = $request->name ?? $user->name;
        $user->phone = $request->phone ?? $user->phone;
        $user->email = $request->email ?? $user->email;
        $user->locale = $request->locale ?? $user->locale ?? 'en';
        $user->username = $request->username ?? $user->username;
        if($request->filled('password'))
        {
            $user->password = \bcrypt($request->password);
        }
        $user->save();
        return ApiResponse::SendSuccess(__('Account Updated Successfully'), [
            'user' => $user
        ]);
    }
    
}
