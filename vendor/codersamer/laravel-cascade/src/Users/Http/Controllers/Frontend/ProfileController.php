<?php

namespace Cascade\Users\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Cascade\System\Services\Action;
use Cascade\System\Services\Feedback;
use Cascade\Users\Actions\UserStoreAction;
use Cascade\Users\Entities\User;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        return view('users::frontend.profile.index', [
            'user' => auth()->user()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        $user = User::find(auth()->user()->id);
        if($request->filled('password')) { $user->password = bcrypt($request->password); }
        $user->name = $request->name ?? $user->name;
        $user->email = $request->email ?? $user->email;
        $user->locale = $request->locale ?? $user->locale;
        $user->phone = $request->phone ?? $user->phone;
        $user->save();

        if($request->file('avatar'))
        {
            $user->avatar_url = $request->avatar->store('avatars', ['disk' => 'uploads']);
            $user->save();
        }

        foreach($request->input('attributes.*', []) as $attributeKey => $attributeValue)
        {
            $user->storeAttribute($attributeKey, $attributeValue);
        }
        foreach($request->file('attributes.*', []) as $key => $file)
        {
            $path = $file->store('users', ['disk' => 'uploads']);
            $user->storeAttribute($key, $path);
        }

        foreach($request->file('attachments.*', []) as $key => $file)
        {
            $file = Arr::wrap($file);
            foreach($file as $singleKey => $singleFile)
            {
                $user->attach($singleFile, is_string($singleKey) ? $singleKey : 'profile');
            }
        }

        Action::Apply(UserStoreAction::class, $user);


        Feedback::success(__('Profile Saved Successfully'));
        Feedback::flash();
        return back();
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show(User $user)
    {
        return view('users::frontend.profile.show', [
            'user' => $user,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit()
    {
        return view('users::frontend.profile.edit', [
            'user' => auth()->user()
        ]);
    }

}
