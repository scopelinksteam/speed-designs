<?php

namespace Cascade\Users\Http\Requests\API;

use Cascade\API\Http\Requests\ApiRequest;

class LoginRequest extends ApiRequest
{
    public function rules() : array
    {
        return [
            'username' => 'required',
            'password' => 'required'
        ];
    }
}
