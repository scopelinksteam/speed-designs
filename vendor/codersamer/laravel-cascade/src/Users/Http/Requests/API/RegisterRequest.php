<?php

namespace Cascade\Users\Http\Requests\API;

use Cascade\API\Http\Requests\ApiRequest;

class RegisterRequest extends ApiRequest
{
    public function rules()
    {
        return [
            'email' => 'required|email|unique:users,email',
            'username' => 'unique:users,username',
            'password' => 'required',
            'phone' => 'unique:users,phone',
            'name' => 'required'
        ];
    }
}