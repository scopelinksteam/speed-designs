<?php

namespace Cascade\Users\Http\Requests\Dashboard;

use Illuminate\Foundation\Http\FormRequest;

class CreateUserRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required', 'min:3'],
            'email' => ['required', 'email', 'unique:users,email,'.$this->get('entity_id')],
            'password' => ['required_without:entity_id'],
            'confirm_password' => ['same:password', 'required_without:entity_id']
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
