<?php

namespace Cascade\Users\Entities;

use Cascade\Users\Entities\Permission;
use Cascade\Users\Entities\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class PermissionRequest extends Model
{

    const STATUS_PENDING = 'Pending';
    const STATUS_APPROVED = 'Approved';
    const STATUS_REJECTED = 'Rejected';

    protected $table = 'permissions_requests';

    public function review_user()
    {
        return $this->belongsTo(User::class, 'review_user_id');
    }

    public function permission()
    {
        return $this->belongsTo(Permission::class, 'permission_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function getIsPendingAttribute()
    {
        return $this->status == static::STATUS_PENDING;
    }

    public function getIsApprovedAttribute()
    {
        return $this->status == static::STATUS_APPROVED;
    }

    public function getIsRejectedAttribute()
    {
        return $this->status == static::STATUS_REJECTED;
    }

    public function Approve()
    {
        $this->status = static::STATUS_APPROVED;
        $this->review_user_id = auth()->id();
        if($this->user != null && $this->permission != null)
        {
            $this->user->givePermissionTo($this->permission);
        }
    }

    public function SetPending()
    {
        $this->status = static::STATUS_PENDING;
    }

    public function Reject()
    {
        $this->status = static::STATUS_REJECTED;
        $this->review_user_id = auth()->id();
    }

    public function scopePending(Builder $query)
    {
        return $query->where('status', static::STATUS_PENDING);
    }

    public function scopeApproved(Builder $query)
    {
        return $query->where('status', static::STATUS_APPROVED);
    }

    public function scopeRejected(Builder $query)
    {
        return $query->where('status', static::STATUS_REJECTED);
    }

    public static function Request(Permission $permission) : PermissionRequest
    {
        $request = new static;
        $request->permission_id = $permission->id;
        $request->user_id = auth()->id();
        $request->status = static::STATUS_PENDING;
        $request->save();
        return $request;
    }
}
