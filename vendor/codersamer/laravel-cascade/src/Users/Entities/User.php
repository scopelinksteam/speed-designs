<?php

namespace Cascade\Users\Entities;

use Cascade\Dashboard\Traits\HasTabs;
use Cascade\System\Entities\Address;
use Cascade\System\Entities\Currency;
use Cascade\System\Entities\Member;
use Cascade\System\Enums\MemberStatus;
use Cascade\System\Traits\HasAddresses;
use Cascade\System\Traits\HasAttachments;
use Cascade\System\Traits\HasAttributes;
use Cascade\System\Traits\Notifiable;
use Cascade\Users\Enums\UsersProviders;
use Cascade\Users\Enums\UserStatus;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Laravel\Sanctum\HasApiTokens;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable, HasRoles, HasAttributes, HasTabs, HasAttachments, HasAddresses;

    protected $table = 'users';

    const STATUS_PENDING = 0;

    const STATUS_ACTIVE = 1;

    const STATUS_DISABLED = 2;

    const STATUS_BANNED = 3;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'provider',
        'provider_id',
        'locale',
        'status',
        'username',
        'phone'
    ];



    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'is_super_admin'    => 'boolean',
        'provider' => UsersProviders::class,
        'status' => UserStatus::class,
    ];

    public function currency()
    {
        return $this->belongsTo(Currency::class, 'currency_id');
    }

    public function switch(User $user) : bool
    {
        if(!$user->canBeSwitched()) { return false; }
        if(!$this->canSwitch()) { return false; }
        request()->session()->put('original_user' , $this->id);
        Auth::login($user);
        return true;
    }

    public function isSwitched()
    {
        return request()->session()->has('original_user');
    }

    public function leaveSwitch()
    {
        if($this->isSwitched())
        {
            $original = User::find(request()->session()->get('original_user', 0));
            if($original)
            {
                Auth::login($original);
                request()->session()->forget('original_user');
                return true;
            }
        }
        return false;
    }

    public function canSwitch()
    {
        return $this->can('switch account');
    }

    public function canBeSwitched()
    {
        return !$this->is_super_admin;
    }
    public function impersonate()
    {
        if($this->type == static::class || !class_exists($this->type)) { return $this; }
        $class = $this->type;
        return (new $class())->newInstance([], true)->setRawAttributes($this->getAttributes());
    }

    public function getBillingAddressAttribute()
    {
        $address = $this->addresses()->where('identifier', 'billing')->first();
        if($address == null)
        {
            $impersonated = $this->impersonate();
            $address = new Address();
            $address->addressable_type = get_class($impersonated);
            $address->addressable_id = $impersonated->id;
            $address->identifier = 'billing';
            $address->save();
        }
        return $address;
    }

    public function getFirstNameAttribute()
    {
        $segments = explode(' ', $this->name);
        return count($segments) > 0 ? $segments[0] : null;
    }

    public function getLastNameAttribute()
    {
        $segments = explode(' ', $this->name);
        return count($segments) > 0 ? end($segments) : $this->first_name;
    }

    public function join($joinable, $group = '', $status = MemberStatus::Active)
    {
        $group = str_replace(' ', '_',strtoupper($group));
        $exist = $joinable->members()->where('member_type', $this->type)->where('member_id', $this->id)->where('group', $group)->first();
        if($exist) { return; }
        $member = new Member();
        $member->status = $status;
        $member->member_id = $this->id;
        $member->member_type = $this->type;
        $member->joinable_type = get_class($joinable);
        $member->joinable_id = $joinable->id ?? 0;
        $member->group = $group;
        $member->save();
        return $member;
    }

    public function leave($joinable)
    {
        $exist = $joinable->members()->where('member_type', $this->type)->where('member_id', $this->id)->first();
        if($exist)
        {
            $exist->delete();
            return true;
        }
        return false;
    }

    public function joined($joinable)
    {
        $exist = $joinable->members()->where('member_type', $this->type)->where('member_id', $this->id)->first();
        return $exist != null;
    }

    public static function GetStatuses()
    {
        return UserStatus::cases();
        
    }

    public function getAvatarAttribute()
    {
        return empty($this->avatar_url) || !Storage::disk('uploads')->exists($this->avatar_url) ? cascade_asset('images/avatar.jpg') : uploads_url($this->avatar_url);
    }

    public function displayName() : Attribute
    {
        return new Attribute(
            get: fn($value) => $this->name,
        );
    }


    public function getCanLoginAttribute() : bool
    {
        if($this->is_super_admin) { return true; }
        if($this->status != UserStatus::Active) { return false; }
        if($this->email_verified_at == null && get_setting('users::general.register.verification', 0) != '0')
        {return false;}
        return true;
    }
}
