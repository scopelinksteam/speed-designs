<?php

namespace Cascade\Users\Entities;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class AuthenticationHistory extends Model
{

    protected $table = 'authentication_history';

    protected $casts = [
        'is_valid' => 'boolean'
    ];

    protected static function CreateFromRequest()
    {
        $object = new static;
        $object->client = request()->userAgent();
        $object->ip_address = request()->getClientIp();

        return $object;
    }

    public static function LogSuccess($user = null) : AuthenticationHistory
    {
        $object = static::CreateFromRequest();
        $object->user_id = $user == null ? 0 : $user->id;
        $object->is_valid = true;
        $object->save();
        return $object;
    }

    public static function LogFailure($user = null) : AuthenticationHistory
    {
        $object = static::CreateFromRequest();
        $object->user_id = $user == null ? 0 : $user->id;
        $object->is_valid = false;
        $object->save();
        return $object;
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function getUsernameAttribute()
    {
        return $this->user != null ? $this->user->name : __('Unknown');
    }

    public function scopeSucceed(Builder $query)
    {
        return $query->where('is_valid', '1');
    }

    public function scopeFailed(Builder $query)
    {
        return $query->where('is_valid', '0');
    }

    public function scopeLinked(Builder $query)
    {
        return $query->whereHas('user');
    }
}
