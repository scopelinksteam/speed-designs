<?php

namespace Cascade\Users\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Spatie\Permission\Models\Permission as SpatiePermission;

class Permission extends SpatiePermission
{

    const OWNER_TYPE_CASCADE = 'cascade';

    const OWNER_TYPE_MODULE = 'module';

    CONST OWNER_TYPE_PLUGIN = 'plugin';

    const OWNER_TYPE_THEME = 'theme';

    public function getDisplayNameAttribute()
    {
        return ucwords(str_replace('_', ' ', $this->name));
    }

    public function getOwnerTypeAttribute()
    {
        $segments = explode(':', $this->owner);
        return strtolower($segments[0]);
    }

    public function IsOwnedBy($type, $owner = null)
    {
        $valid = true;
        $valid = strtolower($this->owner_type) == strtolower($type);
        if($owner != null)
        {

        }
        return $valid;
    }

    public function getOwnerNameAttribute()
    {
        $segments = explode(':', $this->owner);
        return $segments[1];
    }

    public function RequestBy($user = null)
    {
        $user = $user == null ? auth()->user() : $user;
        return PermissionRequest::where('user_id', $user->id)->where('permission_id', $this->id)->first();
    }
}
