<?php

namespace Cascade\Users\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Cascade\Users\Entities\Permission;
use Nwidart\Modules\Facades\Module;

class PermissionsSeederTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Permission::unguard();
        $modules = Module::allEnabled();
        $permissions = [];
        foreach($modules as $module)
        {
            if(file_exists($module->getPath().'/Config/permissions.php'))
            {
                $permissions[$module->getName()] = require_once $module->getPath().'/Config/permissions.php';
            }
        }
        $dbPermissions = Permission::all();
        foreach($permissions as $moduleName => $items)
        {
            foreach($items as $permission => $description)
            {
                $exist = $dbPermissions->where('name', $permission)->where('module', $moduleName)->first();
                if($exist != null)
                {
                    if($exist->description != $description) { $exist->description = $description; $exist->save(); }
                }
                else
                {
                    $newPermission = new Permission([
                        'name' => $permission,
                        'description' => $description,
                        'module' => $moduleName
                    ]);
                    $newPermission->save();
                }
            }
        }

    }
}
