<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePermissionsRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('permissions_requests', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('permission_id')->default(0);
            $table->unsignedBigInteger('user_id')->default(0);
            $table->string('status')->default('Pending');
            $table->unsignedBigInteger('review_user_id')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('permissions_requests');
    }
}
