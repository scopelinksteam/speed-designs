<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddModuleTypeToPermissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('permissions', function (Blueprint $table) {
            $table->dropColumn('module');
            $table->string('owner')->nullable();
            $table->dropIndex('permissions_name_guard_name_unique');
            $table->unique(['name', 'guard_name', 'owner'], 'permission_name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('permissions', function (Blueprint $table) {
            $table->dropColumn('owner');
            $table->string('module')->nullable();
            $table->dropIndex('permission_name');
            $table->unique(['name', 'guard_name']);
        });
    }
}
