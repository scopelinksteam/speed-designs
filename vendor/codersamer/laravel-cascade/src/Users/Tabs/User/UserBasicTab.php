<?php

namespace Cascade\Users\Tabs\User;

use Cascade\Dashboard\Abstraction\DashboardTab;
use Cascade\System\Services\System;
use Cascade\Users\Entities\User;
use Illuminate\View\View;

class UserBasicTab extends DashboardTab
{
    public function __construct()
    {

    }
    public function name(): string
    {
        return __('Basic Details');
    }

    public function slug(): string
    {
        return 'basic';
    }

    public function view(): View|string
    {
        return System::FindView('users::dashboard.tabs.users.basic');
    }
}
