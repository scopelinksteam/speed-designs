@extends('admin::layouts.master')
@section('page-title', __('Profile'))
@section('page-description', __('Manage, Review and Edit your Profile'))
@section('content')
<div class="row">
    <div class="col-3">
        <div class="card card-primary card-outline">
            <div class="card-body box-profile">
                <div class="text-center">
                  <img class="profile-user-img img-fluid img-circle" src="{{url($user->avatar) }}" style="object-fit: cover;height:120px;width:120px;" alt="User profile picture">
                </div>

                <h3 class="profile-username text-center">{{ $user->name }}</h3>

                <p class="text-muted text-center">Software Engineer</p>

                <ul class="list-group list-group-unbordered mb-3">
                  <li class="list-group-item">
                    <b>Roles</b> <a class="float-right">{{ implode(', ',$user->roles->pluck('name')->toArray()) }}</a>
                  </li>
                  <li class="list-group-item">
                    <b>Email</b> <a class="float-right">{{ $user->email }}</a>
                  </li>
                  <li class="list-group-item">
                    <b>Friends</b> <a class="float-right">13,287</a>
                  </li>
                </ul>

                <a href="#" class="btn btn-primary btn-block"><b>Follow</b></a>
            </div>
        </div>
    </div>
    <div class="col-9">
        <div class="card">
            <div class="card-header p-2">
                <ul class="nav nav-pills d-flex">
                    <li class="nav-item">
                        <a href="#" data-bs-toggle="tab" class="nav-link active">Activities</a>
                    </li>
                    <li class="nav-item">
                        <a href="#" data-bs-toggle="tab" class="nav-link ">Account</a>
                    </li>
                    <li class="nav-item">
                        <a href="#capabilities-tab" data-bs-toggle="tab" class="nav-link ">Capabilities</a>
                    </li>
                    <li class="nav-item">
                        <a href="#" data-bs-toggle="tab" class="nav-link ">Settings</a>
                    </li>
                </ul>
            </div>
            <div class="card-body p-0">
                <div class="tab-content">
                    <div class="tab-pane fade" id="capabilities-tab">
                        @php
                            $permissions = \Cascade\Users\Entities\Permission::all();
                            $userPermissions = $user->is_super_admin ? $permissions : $user->getAllPermissions();
                            $userPermissions = $userPermissions->pluck('id')->toArray();
                        @endphp
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>@lang('Permission')</th>
                                    <th>@lang('Capability')</th>
                                    <th>@lang('Status')</th>
                                    <th>@lang('Actions')</th>
                                </tr>
                            </thead>
                            @foreach($permissions as $permission)
                            <tr class="table-">
                                <td><p class="">{{  ucwords($permission->name)  }}</p></td>
                                <td><p class="">{{  $permission->description  }}</p></td>
                                <td><p class="text-{{ in_array($permission->id, $userPermissions) ? 'success' : 'danger' }} font-bold">{{  in_array($permission->id, $userPermissions) ? __('Granted') : __('Denied')  }}</p></td>
                                <td>
                                    @php $request = $permission->RequestBy($user); @endphp
                                    @if(in_array($permission->id, $userPermissions))
                                    @lang('No Action Needed')
                                    @elseif ($request != null)
                                        @if($request->is_pending)
                                        <span class="text-warning">@lang('Pending Approval')</span>
                                        @elseif ($request->is_rejected)
                                        <span class="text-danger">@lang('Request Rejected')</span>
                                        @endif
                                    @elseif($user->is_super_admin)
                                    <span class="text-success">@lang('Full Access')</span>
                                    @else
                                    <a href="{{ route('dashboard.users.permissions.request', ['permission' => $permission]) }}" class="btn btn-info">@lang('Request Permission')</a>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
