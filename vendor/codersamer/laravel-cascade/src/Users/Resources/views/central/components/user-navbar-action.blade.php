
<a class="nav-link" data-toggle="dropdown" href="#">
    <i class="far fa-user"></i>
    {{ auth()->user()->name }}
</a>
<div class="dropdown-menu dropdown-menu-right">
    <span class="dropdown-header">@lang('Account')</span>
    <div class="dropdown-divider"></div>
    <a href="{{ route('central.users.profile') }}" class="dropdown-item">
        <i class="fas fa-user mr-2"></i> @lang('Profile')
    </a>
    <div class="dropdown-divider"></div>
    <a href="#" class="dropdown-item">
        <i class="fas fa-cogs mr-2"></i> @lang('Preferences')
    </a>
    <div class="dropdown-divider"></div>
    <a href="#" class="dropdown-item">
        <i class="fas fa-envelope mr-2"></i> @lang('Messages')
    </a>
    <div class="dropdown-divider"></div>
    <a href="#" class="dropdown-item dropdown">@lang('Logout')</a>
</div>
