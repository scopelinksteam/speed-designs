@extends('admin::layouts.master')
@section('title', __('Users Management'))
@section('page-title', (isset($user)) ? __('Update User') : __('Create New User'))
@section('page-description', __('Manage User Entry Details and Attributes'))
@section('page-actions')
<a href="{{ route('central.users.tenant.index', ['tenant' => $tenant]) }}" class="btn btn-secondary">{{ __('Back to List') }}</a>
@endsection
@section('content')
<form method="POST" action="{{ route('central.users.tenant.store', ['tenant' => $tenant]) }}" enctype="multipart/form-data">
    <div class="row">
        <div class="col-9">
            <div class="card">
                @csrf
                <div class="card-header">
                    @if(isset($user))
                    {{ __('Update User Details') }}
                    @else
                    {{ __('Create new User') }}
                    @endif
                </div>
                <div class="card-body">
                    <h4>{{ __('User Details') }}</h4>
                    <div class="row">
                        <div class="form-group col-6">
                            <label for="name">{{ __('Name') }}</label>
                            <input type="text" name="name" id="name" value="{{ isset($user) ? $user->name : old('name') }}" class="form-control">
                        </div>
                        <div class="form-group col-6">
                            <label for="email">{{ __('Email') }}</label>
                            <input type="email" name="email" id="email" value="{{ isset($user) ? $user->email : old('email') }}" class="form-control">
                        </div>
                        <div class="form-group col-6">
                            <label for="password">{{ __('Password') }}</label>
                            <input type="password" name="password" id="password" class="form-control">
                        </div>
                        <div class="form-group col-6">
                            <label for="confirm_password">{{ __('Confirm Password') }}</label>
                            <input type="password" name="confirm_password" id="confirm_password" class="form-control">
                        </div>
                        <div class="form-group col-6">
                            <label for="locale">{{ __('Prefered Language') }}</label>
                            <select name="locale" id="locale" class="select2">
                                @foreach (get_active_languages() as $language)
                                <option value="{{ $language->locale }}" @if(isset($user) && $user->locale == $language->locale) selected @endif>{{ $language->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <hr>
                    <h4>{{ __('Roles and Permissions') }}</h4>
                    <div class="row">
                        <div class="form-group col-4">
                            <label for="roles">{{ __('Assigned Roles') }}</label>
                            <select class="form-control select2-multi" multiple data-placeholder="{{ __('Select Roles') }}" name="roles[]" id="roles">
                                @foreach($roles as $role)
                                <option value="{{ $role->id }}" @if(isset($user) && $user->hasRole($role->id)) selected @endif>{{ $role->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        @can('set super admin')
                        <div class="form-group col-4">
                            <label for="roles">{{ __('Super Admin') }}</label>
                            <div class="custom-control custom-switch">
                                <input type="checkbox" name="is_super_admin" value="1" class="custom-control-input" id="super_admin_toggle" @if(isset($user) && $user->is_super_admin) checked @endif>
                                <label class="custom-control-label" for="super_admin_toggle">{{ __('Activate Super Admin Mode') }}</label>
                                <br />
                                <small class="text-muted">{{ __('Super Admins Can Bypass All Permissions Including Exclusive Permissions') }}</small>
                            </div>
                        </div>
                        @endcan
                        @can('set users status')
                        <div class="form-group col-4">
                            <label for="roles">{{ __('Status') }}</label>
                            <select class="form-control select2" data-placeholder="{{ __('Select Status') }}" name="status" id="status">
                                @foreach(get_users_statuses() as $id => $display)
                                <option value="{{ $id }}" @if(isset($user) && $user->status == $id) selected @endif>{{ $display }}</option>
                                @endforeach
                            </select>
                        </div>
                        @endcan
                    </div>
                </div>
                <div class="card-footer">
                    @isset($user)
                    <input type="hidden" name="entity_id" value="{{ $user->id }}">
                    @endisset
                    <input type="submit" value="{{ __('Save User') }}" class="btn btn-primary">
                </div>
            </div>
        </div>
        <div class="col-3">
            <div class="card">
                <div class="card-header">
                    {{ __('Profile Image') }}
                </div>
                <div class="card-body">
                    <div class="form-group">
                        <input type="file" name="avatar" class="dropify-image" id="">
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
@endsection
