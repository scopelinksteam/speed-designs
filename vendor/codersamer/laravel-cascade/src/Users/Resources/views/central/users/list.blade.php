@extends('admin::layouts.master')
@section('title', __('Users Management'))
@section('page-title', __('Users Management').': '.$tenant->name)
@section('page-description', __('Manage and List Tenant Registered Users Accounts'))
@section('page-actions')
@can('create users')
<a href="{{ route('central.users.tenant.create', ['tenant' => $tenant]) }}" class="btn btn-primary">{{ __('Create User') }}</a>
@endcan
@endsection
@section('content')
<div class="card">
    <div class="card-body">
        @livewire('datatable', ['table' => $table, 'params' => $tenant->id])
    </div>
  </div>
@endsection
