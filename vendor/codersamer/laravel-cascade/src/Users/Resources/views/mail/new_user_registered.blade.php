<x-mail.message>
    <x-mail.heading>
        New Registeration Has been Occureed
    </x-mail.heading>
    <x-mail.line>Email Address : {{$user->email}}</x-mail.line>
    <x-mail.line>Hello Admin, we would like to notify you about the new registeration process occurred on our system</x-mail.line>
    <x-mail.button link="{{url('/')}}">Preview Account</x-mail.button>
    <x-mail.line>Email Address : {{$user->email}}</x-mail.line>
    <x-slot:header>
        <small>{{$user->name}}</small>
    </x-slot:header>
</x-mail.message>
