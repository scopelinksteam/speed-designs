@extends('admin::layouts.master')
@section('title', __('Role Permissions'))
@section('page-title', __('Role Permissions'). ' : '.$role->name)
@section('page-description', __('Manage and Associate Permissions with the Selected Role'))
@section('page-actions')
<a href="{{ route('dashboard.users.roles.index') }}" class="btn btn-primary">{{ __('Back to Roles') }}</a>
@can('scan permissions')
<a href="{{ route('dashboard.users.roles.scan') }}" class="btn btn-secondary">@lang('Scan Permissions')</a>
@endcan
@endsection
@section('content')
<div class="row">
    <div class="col-12">
        <ul class="nav nav-tabs mb-4">
            @foreach($sections as $sectionType => $sectionInfo)
            <li class="nav-item">
                <a href="#permissions-{{ $sectionType }}" data-bs-toggle="tab" class="nav-link @if($loop->iteration == 1) active @endif">{{ $sectionInfo['title'] }}</a>
            </li>
            @endforeach
        </ul>
    </div>
    <div class="col-12">
        <div class="tab-content">
            @foreach ($sections as $sectionType => $sectionInfo)
            <div class="tab-pane fade @if($loop->iteration == 1) show active @endif" id="permissions-{{ $sectionType }}">
                <div class="row">
                    @if(!empty($sectionInfo['permissions']))
                    <div class="col-3">
                        <ul class="nav flex-column nav-pills w-100">
                            @foreach ($sectionInfo['permissions'] as $ownerName => $permissions)
                            <li class="">
                                <a href="#permissions-{{ $sectionType }}-{{ $ownerName }}" data-bs-toggle="tab" class="nav-link @if($loop->iteration == 1) active @endif">{{ $ownerName }}</a>
                            </li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                    <div class="col-{{ empty($sectionInfo['permissions']) ? 12 : 9 }}">
                        <div class="tab-content">
                            @forelse ($sectionInfo['permissions'] as $ownerName => $permissions)
                            <div class="tab-pane fade @if($loop->iteration == 1) show active @endif" id="permissions-{{ $sectionType }}-{{ $ownerName }}">
                                <form action="{{ route('dashboard.users.roles.permissions.store', ['role' => $role]) }}" method="POST">
                                    @csrf
                                    <input type="hidden" name="owner" value="{{ ucfirst($sectionType) }}:{{ $ownerName }}">
                                    <div class="card">
                                        <div class="card-header">
                                            <div class="card-title">{{ $ownerName }}</div>
                                        </div>
                                        <div class="card-body p-0">
                                            <table class="table table-striped table-response">
                                                <thead>
                                                    <tr>
                                                        <th>@lang('Status')</th>
                                                        <th>@lang('Permissions')</th>
                                                        <th>@lang('Description')</th>
                                                    </tr>
                                                </thead>
                                                @foreach ($permissions as $permission)
                                                <tr>
                                                    <td>
                                                        <div class="custom-control custom-switch">
                                                            <input type="checkbox" name="permissions[]" value="{{ $permission->id }}" class="custom-control-input" id="permission-{{ $permission->id }}" @if($role->hasPermissionTo($permission->name)) checked @endif>
                                                            <label class="custom-control-label" for="permission-{{ $permission->id }}"></label>
                                                        </div>
                                                    </td>
                                                    <td><label class="" for="permission-{{ $permission->id }}">{{ $permission->display_name }}</label></td>
                                                    <td>{{ __($permission->description) }}</td>
                                                </tr>
                                                @endforeach
                                            </table>
                                        </div>
                                        <div class="card-footer">
                                            <input type="submit" value="@lang('Save Permissions')" class="btn btn-primary">
                                        </div>
                                    </div>
                                </form>
                            </div>
                            @empty
                            <div class="tab-pane fade active show">
                                <div class="alert alert-warning" role="alert">
                                    <div>
                                        <h4 class="alert-heading"><i class="icon fas fa-exclamation-triangle me-3"></i> @lang('Alert')!</h4>
                                        @lang('Cannot Find any Permission') : {{ $sectionInfo['title'] }}
                                        @can('scan permissions')
                                        <hr />
                                        <small class="d-block mb-1">@lang('You can try doing a Permissions Scan as it may help find missing permissions')</small>
                                        <a href="{{ route('dashboard.users.roles.scan') }}" class="btn btn-dark text-white mt-2" style="text-decoration: none">@lang('Scan Permissions')</a>
                                    </div>
                                    @endcan
                                  </div>
                            </div>
                            @endforelse
                        </div>

                    </div>

                </div>
            </div>
            @endforeach
        </div>
    </div>

    {{--
@foreach($modules as $module)
    @if(!isset($permissions[$module->getName()])) @continue @endif
    <div class="col-6">
        <div class="card">
            <form action="{{ route('dashboard.users.roles.permissions.store', ['role' => $role]) }}" method="POST">
                @csrf
                <input type="hidden" name="module" value="{{ $module->getName() }}" />
                <div class="card-header">{{ $module->getName() }}</div>
                <div class="card-body">
                    <div class="row">
                        @foreach ($permissions[$module->getName()] as $permission)
                        <div class="col-4 mb-2">
                            <div class="custom-control custom-switch">
                                <input type="checkbox" name="permissions[]" value="{{ $permission->id }}" class="custom-control-input" id="permission-{{ $permission->id }}" @if($role->hasPermissionTo($permission->name)) checked @endif>
                                <label class="custom-control-label" for="permission-{{ $permission->id }}">{{ $permission->display_name }}</label>
                                <br />
                                <small class="text-muted">{{ $permission->description }}</small>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
                <div class="card-footer">
                    <input type="submit" class="btn btn-primary" value="{{ __('Save Module Permissions') }}" />
                </div>
            </form>
        </div>
    </div>
@endforeach
 --}}
</div>
@endsection
