@extends('admin::layouts.master')
@section('title', __('Roles Management'))
@section('page-title', __('Roles Management'))
@section('page-description', __('Create, Edit and Manage Roles and Permissions'))
@section('content')
<div class="row">
    <div class="col-4">
        <div class="card">
            <form method="POST" action="{{ route('dashboard.users.roles.store') }}">
                @csrf
                @isset($role)
                <input type="hidden" name="entity_id" value="{{ $role->id }}">
                @endisset
                <div class="card-header">
                    <strong>{{ ($role == null) ?  __('Create New Role') : __('Edit Role') }}</strong>
                </div>
                <div class="card-body">
                    <div class="form-group">
                        <label for="name">{{ __('Name') }}</label>
                        <input type="text" name="name" value="{{ $role == null ? '' : $role->name }}" id="name" class="form-control">
                    </div>
                </div>
                <div class="card-footer">
                    <input type="submit" value="{{ __('Save Role') }}" class="btn btn-primary">
                </div>
            </form>
        </div>
    </div>
    <div class="col-8">
        <div class="card">
            <div class="card-header">
                <strong>{{ __('Roles List') }}</strong>
            </div>
            <div class="card-body">
                <livewire:datatable :table="$table" />
            </div>
        </div>
    </div>
</div>
@endsection
