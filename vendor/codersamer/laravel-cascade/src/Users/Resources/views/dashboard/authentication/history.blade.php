@extends("admin::layouts.master")
@section('page-title', __('Authentication History'))
@section('page-description', __('Review Authentications Attemps Log and History'))
@section('page-actions')
<a href="{{route('dashboard.users.authentication.history.clear')}}" class="btn btn-danger">@lang('Clear History')</a>
@endsection
@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <div class="card-title">
                    <i class="fa fa-history"></i>
                    @lang('Authentication History')
                </div>
            </div>
            <div class="card-body">
                <livewire:datatable :table="$table" />
            </div>
        </div>
    </div>
</div>
@endsection
