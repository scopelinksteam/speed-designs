@extends("admin::layouts.master")
@section('page-title', __('Permissions Requests'))
@section('page-description', __('Review and Manage Permissions Requests'))
@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <div class="card-title">
                    <i class="fa fa-clipboard-check mr-2"></i>
                    @lang('Permissions Requests')
                </div>
            </div>
            <div class="card-body">
                <livewire:datatable :table="$table" />
            </div>
        </div>
    </div>
</div>
@endsection
