<div class="row">
    <div class="col-md-8">
        @stack('primary::start')
        @stack('card::content.before')
        <div class="card" id="content">
            @csrf
            <div class="card-header">
                {{ __('Account Details') }}
            </div>
            <div class="card-body">
                @stack('card::content.start')
                <h4>{{ __('User Details') }}</h4>
                <div class="row">
                    <div class="form-group col-md-6">
                        <label for="name">{{ __('Name') }}</label>
                        <input type="text" name="name" id="name" value="{{ isset($user) ? $user->name : old('name') }}" class="form-control">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="email">{{ __('Email') }}</label>
                        <input type="email" name="email" id="email" value="{{ isset($user) ? $user->email : old('email') }}" class="form-control">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="password">{{ __('Password') }}</label>
                        <input type="password" name="password" id="password" class="form-control">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="confirm_password">{{ __('Confirm Password') }}</label>
                        <input type="password" name="confirm_password" id="confirm_password" class="form-control">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="locale">{{ __('Prefered Language') }}</label>
                        <select name="locale" id="locale" class="select2">
                            @foreach (get_active_languages() as $language)
                            <option value="{{ $language->locale }}" @if(isset($user) && $user->locale == $language->locale) selected @endif>{{ $language->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <hr>
                <h4>{{ __('Roles and Permissions') }}</h4>
                <div class="row">
                    <div class="form-group col-md-4">
                        <label for="roles">{{ __('Assigned Roles') }}</label>
                        <select class="form-control select2-multi" multiple data-placeholder="{{ __('Select Roles') }}" name="roles[]" id="roles">
                            @foreach($roles as $role)
                            <option value="{{ $role->id }}" @if(isset($user) && $user->hasRole($role->id)) selected @endif>{{ $role->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    @can('set super admin')
                    <div class="form-group col-md-4">
                        <label for="roles">{{ __('Super Admin') }}</label>
                        <div class="custom-control custom-switch">
                            <input type="checkbox" name="is_super_admin" value="1" class="custom-control-input" id="super_admin_toggle" @if(isset($user) && $user->is_super_admin) checked @endif>
                            <label class="custom-control-label" for="super_admin_toggle">{{ __('Activate Super Admin Mode') }}</label>
                            <br />
                            <small class="text-muted">{{ __('Super Admins Can Bypass All Permissions Including Exclusive Permissions') }}</small>
                        </div>
                    </div>
                    @endcan
                    @can('set users status')
                    <div class="form-group col-md-4">
                        <label for="roles">{{ __('Status') }}</label>
                        <select class="form-control select2" data-placeholder="{{ __('Select Status') }}" name="status" id="status">
                            @foreach(get_users_statuses() as $case)
                            <option value="{{ $case->value }}" @if(isset($user) && $user->status == $case) selected @endif>{{ $case->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    @endcan
                </div>
                @stack('card::content.end')
            </div>
            <div class="card-footer">
                @isset($user)
                <input type="hidden" name="entity_id" value="{{ $user->id }}">
                @endisset

            </div>
        </div>
        @stack('card::content.after')
        @stack('primary::end')
    </div>
    <div class="col-md-4">
        @stack('secondary::start')
        @stack('card::avatar.before')
        <div class="card" id="avatar">
            <div class="card-header">
                {{ __('Profile Image') }}
            </div>
            <div class="card-body">
                @stack('card::avatar.start')
                <div class="form-group">
                    <input type="file" name="avatar" class="dropify-image" id="">
                </div>
                @stack('card::avatar.end')
            </div>
        </div>
        @stack('card::avatar.after')
        @stack('secondary::end')
    </div>
</div>
