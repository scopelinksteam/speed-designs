@extends('admin::layouts.master')
@section('title', __('Users Management'))
@section('page-title', __('Users Management'))
@section('page-description', __('Manage and List Registered Users Accounts'))
@section('page-actions')
@can('create users')
<a href="{{ route('dashboard.users.create') }}" class="btn btn-primary">{{ __('Create User') }}</a>
@endcan
@endsection
@section('content')
<div class="card">
    <div class="card-body">
        <livewire:datatable :table="$table" />
    </div>
  </div>
@endsection
