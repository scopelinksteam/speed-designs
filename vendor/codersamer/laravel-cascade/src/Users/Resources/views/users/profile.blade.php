@extends('admin::layouts.master')
@section('page-title', __('Profile'))
@section('page-description', __('Manage, Review and Edit your Profile'))
@section('content')
<div class="row">
    <div class="col-3">
        <div class="card card-primary card-outline">
            <img class="card-img-top" src="{{url($user->avatar) }}" alt="{{ $user->name }}">
            <div class="card-body box-profile">
                <div class="text-center">
                </div>
                <div class="card-title">{{ $user->name }}</div>

                <p class="text-muted small mb-0">
                    @if($user->is_super_admin)
                    @lang('Super Admin')
                    @else
                    {{ implode(', ', $user->roles->pluck('name')->toArray()); }}
                    @endif
                </p>
            </div>
            <ul class="list-group list-group-flush">
                <li class="list-group-item">
                  <b>@lang('Joined At')</b> <a class="float-end">{{format_date($user->created_at) }}</a>
                </li>
                <li class="list-group-item">
                  <b>@lang('Email')</b> <a class="float-end">{{ $user->email }}</a>
                </li>
              </ul>

        </div>
    </div>
    <div class="col-9">
        <div class="card">
            <div class="card-header p-2">
                <ul class="nav nav-tabs">
                    <li class="nav-item">
                        <a data-bs-target="#" data-bs-toggle="tab" class="nav-link ">Activities</a>
                    </li>
                    <li class="nav-item">
                        <a data-bs-target="#profile-tab" data-bs-toggle="tab" class="nav-link active">Account</a>
                    </li>
                    <li class="nav-item">
                        <a data-bs-target="#capabilities-tab" data-bs-toggle="tab" class="nav-link ">Capabilities</a>
                    </li>
                    <li class="nav-item">
                        <a data-bs-target="#" data-bs-toggle="tab" class="nav-link ">Settings</a>
                    </li>
                </ul>
            </div>
            <div class="card-body">
                <div class="tab-content">
                    <div class="tab-pane fade active show" id="profile-tab">
                        <form action="{{route('dashboard.users.profile.store')}}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="container">
                                <div class="row mb-3">
                                    <div class="col-md-6">
                                        <label for="name" class="form-label">@lang('Name')</label>
                                        <input type="text" name="name" id="name" class="form-control" value="{{old('name') ?? $user->name}}">
                                    </div>
                                    <div class="col-md-6">
                                        <label for="email" class="form-label">@lang('Email')</label>
                                        <input type="email" name="email" id="email" class="form-control" value="{{old('email') ?? $user->email}}">
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <div class="col-md-6">
                                        <label for="password" class="form-label">@lang('Password')</label>
                                        <input type="password" name="password" id="password" class="form-control">
                                    </div>
                                    <div class="col-md-6">
                                        <label for="confirm-password" class="form-label">@lang('Confirm Password')</label>
                                        <input type="password" name="confirm-password" id="confirm-password" class="form-control">
                                    </div>
                                    <div class="col-12">
                                        <small class="text-muted">@lang('Keep Blank to not change your password')</small>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <div class="col-md-6">
                                        <label for="avatar" class="form-label">@lang('Avatar')</label>
                                        <input type="file" name="avatar" id="avatar" class="dropify-image" data-default-file="{{$user->avatar}}">
                                    </div>
                                    <div class="col-md-6">
                                        <label for="bio" class="form-label">@lang('Bio')</label>
                                        <textarea name="attributes[bio]" id="bio"  class="form-control" rows="9">{{ $user->Attribute('bio') }}</textarea>
                                    </div>
                                </div>
                                <div class="row justify-content-end">
                                    <div class="col-auto">
                                        <button class="btn btn-dark" type="submit">@lang('Save Account')</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="tab-pane fade" id="capabilities-tab">
                        @php
                            $permissions = \Cascade\Users\Entities\Permission::all();
                            $userPermissions = $user->is_super_admin ? $permissions : $user->getAllPermissions();
                            $userPermissions = $userPermissions->pluck('id')->toArray();
                        @endphp
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>@lang('Permission')</th>
                                    <th>@lang('Capability')</th>
                                    <th>@lang('Status')</th>
                                    <th>@lang('Actions')</th>
                                </tr>
                            </thead>
                            @foreach($permissions as $permission)
                            <tr class="table-">
                                <td><p class="">{{  ucwords($permission->name)  }}</p></td>
                                <td><p class="">{{  $permission->description  }}</p></td>
                                <td><p class="text-{{ in_array($permission->id, $userPermissions) ? 'success' : 'danger' }} font-bold">{{  in_array($permission->id, $userPermissions) ? __('Granted') : __('Denied')  }}</p></td>
                                <td>
                                    @php $request = $permission->RequestBy($user); @endphp
                                    @if(in_array($permission->id, $userPermissions))
                                    @lang('No Action Needed')
                                    @elseif ($request != null)
                                        @if($request->is_pending)
                                        <span class="text-warning">@lang('Pending Approval')</span>
                                        @elseif ($request->is_rejected)
                                        <span class="text-danger">@lang('Request Rejected')</span>
                                        @endif
                                    @elseif($user->is_super_admin)
                                    <span class="text-success">@lang('Full Access')</span>
                                    @else
                                    <a href="{{ route('dashboard.users.permissions.request', ['permission' => $permission]) }}" class="btn btn-info">@lang('Request Permission')</a>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
