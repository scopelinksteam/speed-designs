@extends('admin::layouts.master')
@section('title', __('Users Management'))
@section('page-title', (isset($user)) ? __('Update User') : __('Create New User'))
@section('page-description', __('Manage User Entry Details and Attributes'))
@section('page-actions')
<a href="{{ route('dashboard.users.index') }}" class="btn btn-secondary">{{ __('Back to List') }}</a>
@endsection
@section('content')
@stack('content::start')
<form method="POST" action="{{ route('dashboard.users.store') }}" enctype="multipart/form-data">
    @stack('form::start')
    <x-dashboard-tabs :tabs="$tabs" :data="$__data" />
    @stack('form::end')
    <div class="p-3 rounded-3 bg-white">
        <input type="submit" value="{{ __('Save User') }}" class="btn btn-primary">
    </div>
</form>
@stack('content::end')
@endsection
