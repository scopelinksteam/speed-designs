<form method="POST" action="{{ route('login') }}">
    @stack('form_start')
    @csrf
    <h1 class="h6 mb-3">@lang('Sign in')</h1>
     <!-- Session Status -->
     <x-auth-session-status class="mb-4" :status="session('status')" />

     <!-- Validation Errors -->
     <x-auth-validation-errors class="mb-4" :errors="$errors" />
     @stack('before_fields')
    <div class="form-group mb-4">
        <label for="inputEmail" class="sr-only">@lang('Email address')</label>
        <input type="email" id="inputEmail" class="form-control form-control-lg" placeholder="Email address" name="email" :value="old('email')" required autofocus>
    </div>
    <div class="form-group mb-4">
        <label for="inputPassword" class="sr-only">@lang('Password')</label>
        <input type="password" id="inputPassword" name="password" class="form-control form-control-lg" placeholder="Password" required autocomplete="current-password">
    </div>
    <div class="checkbox mb-3">
        <label>
        <input type="checkbox" value="remember-me" name="remember"> @lang('Stay logged in') </label>
    </div>
    @stack('after_fields')
    @stack('before_buttons')
    <div>
        <x-recaptcha />
    </div>
    <div class="btn-group btn-group-lg w-100 mt-4">
        
        <button class="btn  btn-primary btn-block" type="submit">@lang('Sign in')</button>
        @if (Route::has('password.request'))
            <a class="btn btn-dark btn-block" href="{{ route('password.request') }}">
                {{ __('Forgot your password?') }}
            </a>
        @endif
    </div>
    <div class="w-100 d-flex flex-column gap-2 mt-4">
        @if(facebook_auth_enabled())
        <a href="{{ route('auth.login.facebook') }}" class="btn btn-primary"><i class="fab fa-facebook me-3"></i>@lang('Login with Facebook')</a>
        @endif
        @if(twitter_auth_enabled())
        <a href="{{ route('auth.login.twitter') }}" class="btn btn-info"><i class="fab fa-twitter me-3"></i>@lang('Login with Twitter')</a>
        @endif
        @if(google_auth_enabled())
        <a href="{{ route('auth.login.google') }}" class="btn btn-danger"><i class="fab fa-google me-3"></i>@lang('Login with Google')</a>
        @endif
        @if(github_auth_enabled())
        <a href="{{ route('auth.login.github') }}" class="btn btn-dark"><i class="fab fa-github me-3"></i>@lang('Login with Github')</a>
        @endif
    </div>
    @stack('after_buttons')
    @stack('form_end')
  </form>
