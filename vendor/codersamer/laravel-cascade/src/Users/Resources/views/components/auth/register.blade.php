@stack('form::before')
<form method="POST" action="{{ route('register') }}">
    @csrf
    @stack('form::start')
    <h1 class="h6 mb-3">@lang('Sign up')</h1>
     <!-- Session Status -->
     <x-auth-session-status class="mb-4" :status="session('status')" />

     <!-- Validation Errors -->
     <x-auth-validation-errors class="mb-4" :errors="$errors" />
     @stack('inputs::start')
     <div class="form-group mb-3">
        <label for="inputEmail4">{{ __('Email') }}</label>
        <input type="email" class="form-control" name="email" :value="old('email')" required>
      </div>
      <div class="form-row mb-3">
        <div class="form-group col-md-12">
          <label for="firstname">{{ __('Name') }}</label>
          <input type="text" id="firstname" class="form-control" name="name" :value="old('name')" required autofocus>
        </div>
      </div>
      <hr class="my-4">
      <div class="row mb-4">
        <div class="col-md-12">
          <div class="form-group mb-3">
            <label for="inputPassword5">{{ __('New Password') }}</label>
            <input type="password" class="form-control" name="password" required autocomplete="new-password">
          </div>
          <div class="form-group">
            <label for="inputPassword6">{{ __('Confirm Password') }}</label>
            <input type="password" class="form-control" type="password" name="password_confirmation" required>
          </div>
        </div>
        {{-- 
        <div class="col-md-6">
          <p class="mb-2">{{ __('Password requirements') }}</p>
          <p class="small text-muted mb-2"> {{ __('To create a new password, you have to meet all of the following requirements') }}: </p>
          <ul class="small text-muted pl-4 mb-0">
            <li>{{ __('Minimum 8 character') }}</li>
            <li>{{ __('At least one special character') }}</li>
            <li>{{ __('At least one number') }}</li>
            <li>{{ __("Can’t be the same as a previous password") }} </li>
          </ul>
        </div>
        --}}
      </div>
      @stack('inputs::end')
      <div class="row">
          <div class="col">
              <div class="btn-group btn-group-lg w-100">
                  @if($recaptcha)
                  <x-recaptcha />
                  @endif
                  <button class="btn btn-lg btn-primary btn-block" type="submit">{{ __('Sign up') }}</button>
                  <a class="btn btn-lg btn-dark btn-block" href="{{ route('login') }}">
                      {{ __('Already registered?') }}
                  </a>
              </div>
          </div>
      </div>
      @stack('form::end')
</form>
@stack('form::end')