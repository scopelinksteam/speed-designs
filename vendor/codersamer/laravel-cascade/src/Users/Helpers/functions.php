<?php

use Cascade\Users\Entities\User;
use Illuminate\Support\Facades\Gate;

function can($permission) { return Gate::allows($permission); }

function cannot($permission) { return !can($permission); }

function canAny($permissions)
{
    foreach($permissions as $permission)
    {
        if(can($permission)) { return true; }
    }
    return false;
}

function get_users_statuses()
{
    return User::GetStatuses();
}

function facebook_auth_enabled() { return (bool)get_setting('users::authentication_drivers.facebook', false); }
function twitter_auth_enabled() { return (bool)get_setting('users::authentication_drivers.twitter', false); }
function google_auth_enabled() { return (bool)get_setting('users::authentication_drivers.google', false); }
function github_auth_enabled() { return (bool)get_setting('users::authentication_drivers.github', false); }
