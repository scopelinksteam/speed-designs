<?php

use Illuminate\Database\Schema\Blueprint;

Blueprint::macro('userTracks', function(){
    $this->integer('created_by')->nullable();
    $this->integer('updated_by')->nullable();
});
