<?php

namespace Cascade\Users\Components\Central;

use Cascade\Dashboard\Abstraction\NavbarAction;

class UserNavbarAction extends NavbarAction
{
    public function view()
    {
        return 'users::central.components.user-navbar-action';
    }
}
