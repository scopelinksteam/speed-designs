<?php

namespace Cascade\Users\Components;

use Cascade\Dashboard\Abstraction\NavbarAction;

class UserNavbarAction extends NavbarAction
{
    public function view()
    {
        return 'users::dashboard.components.user-navbar-action';
    }
}
