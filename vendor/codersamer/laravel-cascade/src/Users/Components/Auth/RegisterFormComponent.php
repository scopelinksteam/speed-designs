<?php

namespace Cascade\Users\Components\Auth;

use Cascade\System\Services\System;
use Cascade\Users\Services\AuthenticationService;
use Illuminate\View\Component;

class RegisterFormComponent extends Component
{

    protected static $recaptcha = true;

    public static function SetRecaptcha(bool $enabled)
    {
        static::$recaptcha = $enabled;
    }

    public static function HasRecaptcha() : bool
    {
        return static::$recaptcha;
    }

    public function render()
    {
        return System::FindView('users::components.auth.register', [
            'recaptcha' => AuthenticationService::RegisterHasRecaptcha()
        ]);
    }
}
