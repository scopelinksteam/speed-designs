<?php

namespace Cascade\Users\Components\Auth;

use Cascade\System\Services\System;
use Illuminate\View\Component;

class ForgotPasswordComponent extends Component
{
    public function render()
    {
        return System::FindView('users::components.auth.forgot-password');
    }
}
