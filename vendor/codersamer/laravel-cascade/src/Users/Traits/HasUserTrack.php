<?php

namespace Cascade\Users\Traits;

use Cascade\Users\Entities\User;

trait HasUserTrack
{

    public static function bootHasUserTrack()
    {
        static::creating(function($entity){
            if(auth()->check() && ($entity->created_by == null || $entity->created_by == 0)) {$entity->created_by = auth()->user()->id;}
        });
        static::created(function($entity){
            if (!auth()->check()){return;}
            //Activity::recordCreate($entity);
        });
        static::updating(function($entity){
            if (!auth()->check()){return;}
            $entity->updated_by = auth()->user()->id;
            //Activity::recordUpdate($entity);
        });
        static::deleting(function($entity){
            if (!auth()->check()){return;}
            //Activity::recordDelete($entity);
        });
    }


    public function create_user()
    {
        return $this->belongsTo(User::class, 'created_by', 'id');
    }

    public function update_user()
    {
        return $this->belongsTo(User::class, 'updated_by', 'id');
    }

    /*
    public function activities()
    {
        return $this->morphMany(Activity::class, 'activitable');
    }
    */

    public function scopeBy($query, $user)
    {
        $userId = is_object($user) ? $user->id : intval($user);
        return $query->where('created_by',$userId);
    }

    public function getModelNameAttribute()
    {
        return __((new \ReflectionClass($this))->getShortName());
    }

}
