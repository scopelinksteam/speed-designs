<?php

use Cascade\System\Entities\Label;
use Cascade\System\Http\Controllers\Dashboard\LabelsController;
use Illuminate\Routing\Router;
use Illuminate\Support\Facades\Route;

Router::macro('labels', function(String $typeClass){
    $slug = Label::findSlug($typeClass);
    $builder = Label::findBuilder($typeClass);
    if($slug == null) { return; }
    Route::get('/', [LabelsController::class, 'index'])->defaults('type', $builder)->name('index');
    Route::post('/', [LabelsController::class, 'store'])->defaults('type', $builder)->name('store');
    Route::get('/edit/{label}', [LabelsController::class, 'edit'])->defaults('type', $builder)->name('edit');
    Route::get('/delete/{label}', [LabelsController::class, 'destroy'])->defaults('type', $builder)->name('delete');
});
