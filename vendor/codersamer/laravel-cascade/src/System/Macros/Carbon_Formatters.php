<?php

use Carbon\Carbon;

Carbon::macro('date', function(){ return format_date($this); });
Carbon::macro('time', function(){ return format_time($this); });
Carbon::macro('datetime', function(){ return format_datetime($this); });
