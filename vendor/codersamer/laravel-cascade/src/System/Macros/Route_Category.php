<?php

use Cascade\System\Entities\Category;
use Illuminate\Routing\Router;
use Illuminate\Support\Facades\Route;
use Cascade\System\Http\Controllers\Dashboard\CategoriesController;

Router::macro('categories', function(String $typeClass){
    $slug = Category::findSlug($typeClass);
    if($slug == null) { return; }
    Route::get('/', [CategoriesController::class, 'index'])->defaults('type', $typeClass)->name('index');
    Route::post('/', [CategoriesController::class, 'store'])->defaults('type', $typeClass)->name('store');
    Route::get('/edit/{category}', [CategoriesController::class, 'edit'])->defaults('type', $typeClass)->name('edit');
    Route::get('/delete/{category}', [CategoriesController::class, 'destroy'])->defaults('type', $typeClass)->name('delete');
});
