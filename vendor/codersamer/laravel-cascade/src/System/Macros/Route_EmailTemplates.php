<?php

use Cascade\System\Contracts\Exportable;
use Cascade\System\Entities\Category;
use Cascade\System\Entities\EmailTemplate;
use Illuminate\Routing\Router;
use Illuminate\Support\Facades\Route;
use Cascade\System\Http\Controllers\Dashboard\CategoriesController;
use Cascade\System\Http\Controllers\Dashboard\EmailTemplatesController;

Router::macro('EmailTemplates', function(String|Exportable $typeClass){
    $composer = EmailTemplate::findComposer($typeClass);
    if($composer == null) { return; }
    Route::get('/', [EmailTemplatesController::class, 'index'])->defaults('composer', $composer)->name('index');
    Route::get('/create', [EmailTemplatesController::class, 'create'])->defaults('composer', $composer)->name('create');
    Route::post('/', [EmailTemplatesController::class, 'store'])->defaults('composer', $composer)->name('store');
    Route::get('/edit/{template}', [EmailTemplatesController::class, 'edit'])->defaults('composer', $composer)->name('edit');
    Route::get('/show/{template}', [EmailTemplatesController::class, 'show'])->defaults('composer', $composer)->name('show');
    Route::get('/json/{template}', [EmailTemplatesController::class, 'json'])->defaults('composer', $composer)->name('json');
    if($composer->destroyable())
    {
        Route::get('/delete/{template}', [EmailTemplatesController::class, 'destroy'])->defaults('composer', $composer)->name('delete');
    }
});
