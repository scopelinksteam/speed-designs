<?php

use Cascade\System\Entities\Category;
use Cascade\System\Entities\Tag;
use Illuminate\Routing\Router;
use Illuminate\Support\Facades\Route;
use Cascade\System\Http\Controllers\Dashboard\TagsController;

Router::macro('tags', function(String $typeClass){
    $slug = Tag::findSlug($typeClass);
    if($slug == null) { return; }
    Route::get('/', [TagsController::class, 'index'])->defaults('type', $typeClass)->name('index');
    Route::post('/', [TagsController::class, 'store'])->defaults('type', $typeClass)->name('store');
    Route::get('/edit/{tag}', [TagsController::class, 'edit'])->defaults('type', $typeClass)->name('edit');
    Route::get('/delete/{tag}', [TagsController::class, 'destroy'])->defaults('type', $typeClass)->name('delete');
});
