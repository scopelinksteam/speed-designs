<?php

use Cascade\System\Entities\Priority;
use Cascade\System\Http\Controllers\Dashboard\PrioritiesController;
use Illuminate\Routing\Router;
use Illuminate\Support\Facades\Route;

Router::macro('priorities', function(String $typeClass){
    $slug = Priority::findSlug($typeClass);
    if($slug == null) { return; }
    Route::get('/', [PrioritiesController::class, 'index'])->defaults('type', $typeClass)->name('index');
    Route::post('/', [PrioritiesController::class, 'store'])->defaults('type', $typeClass)->name('store');
    Route::get('/edit/{priority}', [PrioritiesController::class, 'edit'])->defaults('type', $typeClass)->name('edit');
    Route::get('/delete/{priority}', [PrioritiesController::class, 'destroy'])->defaults('type', $typeClass)->name('delete');
});
