<?php

use Illuminate\Database\Schema\Blueprint;

Blueprint::macro('category', function(){
    $this->unsignedBigInteger('category_id')->default(0);
});
