<?php

use Cascade\System\Entities\Status;
use Illuminate\Routing\Router;
use Illuminate\Support\Facades\Route;
use Cascade\System\Http\Controllers\Dashboard\StatusesController;

Router::macro('statuses', function(String $typeClass){
    $slug = Status::findSlug($typeClass);
    if($slug == null) { return; }
    Route::get('/', [StatusesController::class, 'index'])->defaults('type', $typeClass)->name('index');
    Route::post('/', [StatusesController::class, 'store'])->defaults('type', $typeClass)->name('store');
    Route::get('/edit/{status}', [StatusesController::class, 'edit'])->defaults('type', $typeClass)->name('edit');
    Route::get('/delete/{status}', [StatusesController::class, 'destroy'])->defaults('type', $typeClass)->name('delete');
});
