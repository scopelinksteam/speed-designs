<?php

namespace Cascade\System\Http\Middleware;

use Cascade\System\Actions\CurrencySetAction;
use Cascade\System\Entities\Currency;
use Cascade\System\Services\Action;
use Cascade\System\Services\System;
use Closure;
use Illuminate\Http\Request;

class ApplyCurrencyMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if(auth()->check() && auth()->user()->currency_id > 0)
        {
            session(['currency' => auth()->user()->currency_id]);
        }
        else
        {
            $firstCurrency = null;
            if(session('currency', 0) > 0)
            {
                $firstCurrency = Currency::find(session('currency', 0));
            }
            if($firstCurrency == null)
            {
                $firstCurrency = $firstCurrency ? $firstCurrency : Currency::first();
                $overrideCurrency = $firstCurrency == null ? 0 : $firstCurrency->id;
                $defaultCurrency = $request->is('dashboard/*')
                    ? get_setting('system::localization.currencies.default', $overrideCurrency)
                    : get_setting('site::general.currencies.default', $overrideCurrency);
                $defaultCurrency = Currency::find($defaultCurrency);
                if($defaultCurrency != null)
                {
                    session(['currency' => $defaultCurrency->id]);
                }
                else
                {
                    if($firstCurrency != null)
                    {
                        session(['currency' => $firstCurrency->id]);
                    }
                    else
                    {
                        session(['currency' => 0]);
                    }
                }
            }
        }
        Action::Apply(CurrencySetAction::class, get_active_currency());
        return $next($request);
    }
}