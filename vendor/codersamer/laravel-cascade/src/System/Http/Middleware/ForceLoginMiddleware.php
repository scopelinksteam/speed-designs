<?php

namespace Cascade\System\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class ForceLoginMiddleware
{

    protected static $exceptions = [
        'auth*',
        'login*',
        'register*',
        'forgot-password*',
        'verify-email*',
        'reset-password*',
        'email*',
        'confirm-password*'
    ];

    public static function Except(String $exception)
    {
        static::$exceptions[] = $exception;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        //Bypass for Logged Users
        if(auth()->check()) { return $next($request);  }

        //Bypass for UnGuarded Routes
        foreach(static::$exceptions as $unguardedRoute)
        {
            if($request->is($unguardedRoute))
            { return $next($request); }
        }
        return to_route('login');
    }
}
