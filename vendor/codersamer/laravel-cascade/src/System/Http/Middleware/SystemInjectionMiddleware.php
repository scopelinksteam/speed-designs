<?php

namespace Cascade\System\Http\Middleware;

use Cascade\System\Actions\SystemReadyAction;
use Cascade\System\Actions\SystemShutdownAction;
use Cascade\System\Services\Action;
use Closure;
use Illuminate\Http\Request;

class SystemInjectionMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        Action::Apply(SystemReadyAction::class);
        $response = $next($request);
        Action::Apply(SystemShutdownAction::class);
        return $response;
    }
}