<?php

namespace Cascade\System\Http\Controllers\API;

use Cascade\API\Http\Controllers\ApiController;
use Cascade\API\Services\ApiResponse;
use Cascade\System\Entities\Category;
use Cascade\System\Entities\City;
use Illuminate\Http\Request;

class CategoriesController extends ApiController
{
    /**
     * Display a listing of the resource.
     * @return ApiResponse
     */
    public function list(Request $request)
    {
        $categoriesQuery = Category::query();
        if($request->filled('type'))
        {
            $categoriesQuery->of($request->string('type'));
        }
        return ApiResponse::SendSuccess(__('Data Fetched Successfully'), $categoriesQuery->get());
    }
}