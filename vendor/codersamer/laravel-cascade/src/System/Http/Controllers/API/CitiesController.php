<?php

namespace Cascade\System\Http\Controllers\API;

use Cascade\API\Http\Controllers\ApiController;
use Cascade\API\Services\ApiResponse;
use Cascade\System\Entities\City;

class CitiesController extends ApiController
{
    /**
     * Display a listing of the resource.
     * @return ApiResponse
     */
    public function list()
    {
        return ApiResponse::SendSuccess(__('Data Fetched Successfully'), City::all());
    }
}