<?php

namespace Cascade\System\Http\Controllers\Dashboard;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Storage;
use Cascade\System\Services\Feedback;
use Cascade\System\Services\Plugins;

class PluginsController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        return view('system::dashboard.plugins.index', [
            'plugins' => Plugins::GetPlugins()
        ]);
    }

    /**
     * Create new Plugin
     *
     * @return Renderable
     */
    public function postCreate(Request $request)
    {
        if(!$request->filled('name'))
        {
            Feedback::getInstance()->addError(__('Plugin Name Cannot be Empty'));
            Feedback::flash();
            return back();
        }
        Artisan::call('plugin:make '.\Str::studly($request->name));
        Feedback::getInstance()->addSuccess(__('Plugin Created Successfully'));
        Feedback::flash();
        return back();
    }

    /**
     * Install a Plugin
     *
     * @return Renderable
     */
    public function postInstall(Request $request)
    {
        if(!$request->file('plugin'))
        {
            Feedback::getInstance()->addError(__('Plugin File Cannot be Empty'));
            Feedback::flash();
            return back();
        }
        if($request->file('plugin')->extension() != 'zip')
        {
            Feedback::getInstance()->addError(__('Invalid Plugin File'));
            Feedback::flash();
            return back();
        }
        $filename = $request->file('plugin')->store('temp', ['disk' => 'uploads']);
        $fullPath = Storage::disk('uploads')->path($filename);
        $installed = Plugins::InstallFromFile($fullPath);
        if($installed)
        {
            Feedback::getInstance()->addSuccess(__('Plugin Installed Successfully'));
        }
        else{
            Feedback::getInstance()->addError(__('Plugin Cannot be Installed, Invalid Archive'));
        }
        Feedback::flash();
        return back();
    }

    /**
     * Show Screenshot for Plugin
     *
     */
    public function screenshot($pluginName)
    {
        $plugin = Plugins::Get($pluginName);
        $screenshot = $plugin != null ? $plugin->GetScreenshotFile() : module_path('System', 'Resources/assets/images/default-plugin-screenshot.png');
        return response()->file($screenshot);
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function enable($plugin)
    {
        Gate::authorize('enable plugins');
        $valid = Plugins::Enable($plugin);
        if($valid)
        { Feedback::getInstance()->addSuccess(__('Plugin Enabled Successfully'));}
        else { Feedback::getInstance()->addError(__('Plugin Failed to Activate'));}
        Feedback::flash();
        return back();
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function disable($plugin)
    {
        Gate::authorize('disable plugins');
        Plugins::Disable($plugin);
        Feedback::getInstance()->addSuccess(__('Plugin Disabled Successfully'));
        Feedback::flash();
        return back();
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function delete($plugin)
    {
        Gate::authorize('delete plugins');
        $deleted = Plugins::Delete($plugin);
        if($deleted)
        {
            Feedback::getInstance()->addSuccess(__('Plugin Deleted Successfully'));
        }
        Feedback::flash();
        return back();
    }
}
