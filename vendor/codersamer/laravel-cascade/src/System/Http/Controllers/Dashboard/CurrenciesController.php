<?php

namespace Cascade\System\Http\Controllers\Dashboard;

use Cascade\System\Entities\Currency;
use Cascade\System\Services\Feedback;
use Cascade\System\Tables\CurrenciesTable;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Gate;

class CurrenciesController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index(CurrenciesTable $table)
    {
        Gate::authorize('view currencies');
        return view('system::dashboard.currencies.index', [
            'table' => $table
        ]);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        $entityId = intval($request->input('entity_id', 0));
        Gate::authorize($entityId > 0 ? 'edit currencies' : 'create currencies');

        $valid = true;
        $names = only_filled($request->name);
        if(empty($names))
        {
            Feedback::getInstance()->addError(__('Name is Required'));
            $valid = false;
        }
        if(empty($request->code))
        {
            Feedback::getInstance()->addError(__('Code is Required'));
            $valid = false;
        }
        if(empty($request->rate))
        {
            Feedback::getInstance()->addError(__('Exchange Rate is Required'));
            $valid = false;
        }

        if(!$valid)
        {
            Feedback::flash();
            return back();
        }

        $entity = $entityId > 0 ? Currency::find($entityId) : new Currency();
        $entity->name = fill_locales($names);
        $entity->code = $request->code;
        $entity->symbol = $request->symbol ?? '';
        $entity->rate = $request->rate ?? 1;
        $entity->save();
        Feedback::getInstance()->addSuccess(__('Currency Saved Successfully'));
        Feedback::flash();
        return redirect()->route('dashboard.system.currencies.index');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit(Currency $currency, CurrenciesTable $table)
    {
        Gate::authorize('edit currencies');
        return $this->index($table)->with([
            'currency' => $currency
        ]);
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy(Currency $currency)
    {
        Gate::authorize('delete currencies');
        $currency->delete();
        Feedback::getInstance()->addSuccess(__('Currency Deleted Successfully'));
        Feedback::flash();
        return redirect()->route('dashboard.system.currencies.index');
    }
}
