<?php

namespace Cascade\System\Http\Controllers\Dashboard;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Gate;
use Cascade\System\Services\Feedback;
use Cascade\System\Services\Modules;
use Cascade\System\Services\System;

class ModulesController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        return view('system::dashboard.modules.index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function enable($module)
    {
        Gate::authorize('enable modules');
        Modules::Enable($module);
        Feedback::getInstance()->addSuccess(__('Module Enabled Successfully'));
        Feedback::flash();
        return back();
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function disable($module)
    {
        Gate::authorize('disable modules');
        Modules::Disable($module);
        Feedback::getInstance()->addSuccess(__('Module Disabled Successfully'));
        Feedback::flash();
        return back();
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('system::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('system::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        //
    }
}
