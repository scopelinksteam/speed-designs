<?php

namespace Cascade\System\Http\Controllers\Dashboard;

use Cascade\System\Entities\Label;
use Cascade\System\Services\Feedback;
use Cascade\System\Services\Generators\LabelBuilder;
use Cascade\System\Tables\LabelsTable;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Gate;

class LabelsController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index(LabelBuilder $type)
    {

        return view('system::dashboard.labels.index', [
            'table' => LabelsTable::class,
            'type' => $type,
            'routes' => $this->CaptureRoutes()
        ]);
    }


    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request, LabelBuilder $builder)
    {
        $entityId = intval($request->input('entity_id', 0));

        $names = only_filled($request->name);

        $valid = true;

        if(empty($names))
        {
            Feedback::error(__('Name is Required'));
            $valid = false;
        }

        if(!$valid)
        {
            Feedback::flash();
            return back();
        }

        $type = $builder->type();
        $entity = $entityId > 0 ? call_user_func([$type, 'find'],$entityId) : new $type();

        $entity->type = $builder->type();
        $entity->name = fill_locales($names);
        $entity->color = $request->input('color', '#000000');

        $entity->save();

        if($request->has('attributes'))
        {
            foreach($request->input('attributes') as $key => $value)
            {
                $entity->storeAttribute($key, $value);
            }
        }
        Feedback::getInstance()->addSuccess(__('Label Saved Successfully'));
        Feedback::flash();
        return redirect()->to($this->CaptureRoutes()('index'));
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit(Label $label, LabelBuilder $builder)
    {
        $entity = call_user_func([$builder->type(), 'find'], $label->id);
        return $this->index($builder)->with('label', $entity);
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy(Label $label)
    {
        Gate::authorize('delete labels');

        $label->delete();
        Feedback::getInstance()->addSuccess(__('Label Deleted Successfully'));
        Feedback::flash();
        return redirect()->route($this->CaptureRoutes()('index',false));
    }


    protected function CaptureRoutes()
    {
        $currentRoute = request()->route()->getName();
        $routeSegments = explode('.', $currentRoute);
        array_pop($routeSegments);
        $routeBase = implode('.', $routeSegments).'.';
        return function($name, $full = true) use($routeBase){
            $route = $routeBase.$name;
            return $full ? route($route) : $route;
        };
    }
}
