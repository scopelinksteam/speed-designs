<?php

namespace Cascade\System\Http\Controllers\Dashboard;

use Cascade\System\Entities\Category;
use Cascade\System\Enums\CategoryStatus;
use Cascade\System\Services\Feedback;
use Cascade\System\Tables\CategoriesTable;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Gate;

class CategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index($type)
    {

        return view('system::dashboard.categories.index', [
            'table' => CategoriesTable::class,
            'type' => $type,

            'categories' => Category::of($type)->get(),

            'routes' => $this->CaptureRoutes()
        ]);
    }


    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request, $type)
    {
        $entityId = intval($request->input('entity_id', 0));

        $names = only_filled($request->name);

        $valid = true;

        if(empty($names))
        {
            Feedback::getInstance()->addError(__('Name is Required'));
            $valid = false;
        }

        if(!$valid)
        {
            Feedback::flash();
            return back();
        }

        $entity = $entityId > 0 ? Category::find($entityId) : new Category();

        $entity->type = $type;
        $entity->name = fill_locales($names);
        $entity->status = CategoryStatus::Active;
        $entity->parent_id = $request->input('parent_id', 0);

        if($request->file('thumbnail'))
        {
            $entity->thumbnail_file = $request->file('thumbnail')->store('categories', ['disk' => 'uploads']);
        }

        //Slug
        if($entity->slug == null || ($request->filled('slug') && $request->slug != $entity->slug))
        {
            $requestedSlug = $request->input('slug', null);
            if($requestedSlug == null)
            {
                $titles = fill_locales(only_filled($request->name));
                $requestedSlug = array_shift($titles);
            }
            $requestedSlug = str($requestedSlug)->slug();
            $queryBuilder = Category::of($type);
            $generatedSlug = $requestedSlug;
            $counter = 0;
            while($queryBuilder->where('slug', $generatedSlug)->count() > 0)
            {
                $counter++;
                $generatedSlug = $requestedSlug.'-'.$counter;
                $queryBuilder = Category::of($type);
            }

            $entity->slug = $generatedSlug;
        }

        $entity->save();
        Feedback::getInstance()->addSuccess(__('Category Saved Successfully'));
        Feedback::flash();
        return redirect()->to($this->CaptureRoutes()('index'));
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit(Category $category, $type)
    {
        return $this->index($type)->with('category', $category);
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy(Category $category)
    {
        Gate::authorize('delete categories');
        if($category->children()->count() > 0)
        {
            Feedback::alert(__('Selected Category Cannot be Deleted Since it has Child Categories'));
            Feedback::flash();
            return redirect()->route($this->CaptureRoutes()('index',false));
        }
        $category->delete();
        Feedback::getInstance()->addSuccess(__('Category Deleted Successfully'));
        Feedback::flash();
        return redirect()->route($this->CaptureRoutes()('index',false));
    }


    protected function CaptureRoutes()
    {
        $currentRoute = request()->route()->getName();
        $routeSegments = explode('.', $currentRoute);
        array_pop($routeSegments);
        $routeBase = implode('.', $routeSegments).'.';
        return function($name, $full = true) use($routeBase){
            $route = $routeBase.$name;
            return $full ? route($route) : $route;
        };
    }
}
