<?php

namespace Cascade\System\Http\Controllers\Dashboard;

use Cascade\System\Services\Feedback;
use Cascade\System\Services\System;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Gate;

class SystemManagerController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        $actions = [
            [
                'title' => __('Cron Jobs'),
                'icon' => 'chart-pie',
                'description' => __('Manage Installed Modules'),
                'link' => '#'
            ],
            [
                'title' => __('API Access'),
                'icon' => 'server',
                'description' => __('Manage Installed Modules'),
                'link' => '#'
            ],
            [
                'title' => __('Webhooks'),
                'icon' => 'project-diagram',
                'description' => __('Manage Installed Modules'),
                'link' => '#'
            ],
            [
                'title' => __('Developer Portal'),
                'icon' => 'laptop-code',
                'description' => __('Manage Installed Modules'),
                'link' => '#'
            ],
            [
                'title' => __('Web Terminal'),
                'icon' => 'terminal',
                'description' => __('Manage Installed Modules'),
                'link' => '#'
            ],
        ];
        return view('system::dashboard.manager.index', [
            'actions' => $actions
        ]);
    }


    public function clearCache()
    {
        Gate::authorize('clear cache');
        Artisan::call('cache:clear');
        Feedback::getInstance()->addSuccess(__('Cache Cleared Successfully'));
        Feedback::flash();
        return back();
    }

    public function rebuildSystem()
    {
        System::DispatchAction('rebuild');
        Feedback::success(__('System Rebuilt Successfully'));
        Feedback::flash();
        return back();
    }
}
