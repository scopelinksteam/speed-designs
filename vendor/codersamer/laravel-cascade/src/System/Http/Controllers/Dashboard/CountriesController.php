<?php

namespace Cascade\System\Http\Controllers\Dashboard;

use Cascade\System\Entities\Country;
use Cascade\System\Services\Feedback;
use Cascade\System\Services\System;
use Cascade\System\Tables\CountriesTable;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Gate;

class CountriesController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        return System::FindView('system::dashboard.geo.countries.index', [
            'table' => CountriesTable::class
        ]);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        $entityId = intval($request->input('entity_id', '0'));
        Gate::authorize($entityId > 0 ? 'edit country' : 'create country');
        $entity = $entityId > 0 ? Country::find($entityId) : new Country();
        if(!$request->filled('name'))
        { Feedback::error(__('Country name is Required')); }
        if(!$request->filled('code'))
        { Feedback::error(__('Country code is Required')); }
        if(Feedback::hasErrors())
        {
            Feedback::flash();
            return back();
        }

        $entity->name = $request->input('name');
        $entity->code = $request->input('code');
        $entity->phone_code = $request->integer('phone_code');
        if($request->file('thumbnail'))
        { $entity->thumbnail_file = $request->file('thumbnail')->store('countries', ['disk' => 'uploads']); }
        $entity->save();
        Feedback::success(__('Country Saved Successfully'));
        Feedback::flash();
        return to_route('dashboard.system.geo.countries.index');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit(Country $country)
    {
        return $this->index()->with([
            'entity' => $country
        ]);
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy(Country $country)
    {
        Gate::authorize('delete country');
        if($country->cities()->count() > 0)
        { Feedback::error(__('Country is associated with Cities, Delete Cities First')); }
        else
        {
            $country->delete();
            Feedback::success(__('Country Deleted Successfully'));
        }
        Feedback::flash();
        return back();
    }
}
