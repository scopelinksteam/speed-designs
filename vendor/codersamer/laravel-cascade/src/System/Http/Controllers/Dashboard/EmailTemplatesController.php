<?php

namespace Cascade\System\Http\Controllers\Dashboard;

use Cascade\System\Entities\EmailTemplate;
use Cascade\System\Services\EmailTemplateComposer;
use Cascade\System\Services\Feedback;
use Cascade\System\Tables\EmailTemplatesTable;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Gate;

class EmailTemplatesController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index(EmailTemplateComposer $composer)
    {
        return view('system::dashboard.email-templates.index', [
            'composer' => $composer,
            'table' => EmailTemplatesTable::class,
            'routes' => $this->CaptureRoutes()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create(EmailTemplateComposer $composer)
    {
        return view('system::dashboard.email-templates.upsert', [
            'composer' => $composer,
            'routes' => $this->CaptureRoutes()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request, EmailTemplateComposer $composer)
    {
        $entityId = intval($request->input('entity_id', 0));
        Gate::authorize($entityId > 0 ? 'edit email templates' : 'create email templates');

        $valid = true;

        if(!$request->filled('title'))
        {
            Feedback::getInstance()->addError(__('Title Cannot be Empty'));
            $valid = false;
        }

        if(!$request->filled('content'))
        {
            Feedback::getInstance()->addError(__('Title Cannot be Empty'));
            $valid = false;
        }

        if(!$valid)
        {
            Feedback::flash();
            return back()->withInput();
        }

        $entity = $entityId > 0 ? EmailTemplate::find($entityId) : new EmailTemplate();
        $entity->title = $request->title;
        $entity->content = $request->content;
        $entity->type = $composer->type();
        $entity->name = $request->name ?? '';
        $entity->save();
        Feedback::getInstance()->addSuccess(__('Email Template Saved Successfully'));
        Feedback::flash();
        return to_route($this->CaptureRoutes()('index', false));
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show(EmailTemplate $template, EmailTemplateComposer $composer)
    {
        return view('system::show');
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function json(EmailTemplate $template, EmailTemplateComposer $composer)
    {
        return $template;
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit(EmailTemplate $template, EmailTemplateComposer $composer)
    {
        return $this->create($composer)->with([
            'template' => $template
        ]);
    }


    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy(EmailTemplate $template, EmailTemplateComposer $composer)
    {
        //
    }

    protected function CaptureRoutes()
    {
        $currentRoute = request()->route()->getName();
        $routeSegments = explode('.', $currentRoute);
        array_pop($routeSegments);
        $routeBase = implode('.', $routeSegments).'.';
        return function($name, $full = true) use($routeBase){
            $route = $routeBase.$name;
            return $full ? route($route) : $route;
        };
    }
}
