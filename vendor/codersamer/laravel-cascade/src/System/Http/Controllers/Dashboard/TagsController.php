<?php

namespace Cascade\System\Http\Controllers\Dashboard;

use Cascade\System\Entities\Tag;
use Cascade\System\Services\Feedback;
use Cascade\System\Tables\TagsTable;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Gate;

class TagsController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index($type)
    {

        return view('system::dashboard.tags.index', [
            'table' => TagsTable::class,
            'type' => $type,
            'routes' => $this->CaptureRoutes()
        ]);
    }


    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request, $type)
    {
        $entityId = intval($request->input('entity_id', 0));

        $names = only_filled($request->name);

        $valid = true;

        if(empty($names))
        {
            Feedback::getInstance()->addError(__('Name is Required'));
            $valid = false;
        }

        if(!$valid)
        {
            Feedback::flash();
            return back();
        }

        $entity = $entityId > 0 ? Tag::find($entityId) : new Tag();

        $entity->type = $type;
        $entity->name = fill_locales($names);

        //Slug
        if($entity->slug == null || ($request->filled('slug') && $request->slug != $entity->slug))
        {
            $requestedSlug = $request->input('slug', null);
            if($requestedSlug == null)
            {
                $titles = fill_locales(only_filled($request->name));
                $requestedSlug = array_shift($titles);
            }
            $requestedSlug = str($requestedSlug)->slug();
            $queryBuilder = Tag::of($type);
            $generatedSlug = $requestedSlug;
            $counter = 0;
            while($queryBuilder->where('slug', $generatedSlug)->count() > 0)
            {
                $counter++;
                $generatedSlug = $requestedSlug.'-'.$counter;
                $queryBuilder = Tag::of($type);
            }

            $entity->slug = $generatedSlug;
        }

        $entity->save();
        Feedback::getInstance()->addSuccess(__('Tag Saved Successfully'));
        Feedback::flash();
        return redirect()->to($this->CaptureRoutes()('index'));
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit(Tag $tag, $type)
    {
        return $this->index($type)->with('tag', $tag);
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy(Tag $tag)
    {
        Gate::authorize('delete categories');
        $tag->delete();
        Feedback::getInstance()->addSuccess(__('Tag Deleted Successfully'));
        Feedback::flash();
        return redirect()->route($this->CaptureRoutes()('index',false));
    }


    protected function CaptureRoutes()
    {
        $currentRoute = request()->route()->getName();
        $routeSegments = explode('.', $currentRoute);
        array_pop($routeSegments);
        $routeBase = implode('.', $routeSegments).'.';
        return function($name, $full = true) use($routeBase){
            $route = $routeBase.$name;
            return $full ? route($route) : $route;
        };
    }
}
