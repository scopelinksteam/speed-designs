<?php

namespace Cascade\System\Http\Controllers\Dashboard;

use Cascade\System\Entities\Tax;
use Cascade\System\Enums\TaxRateType;
use Cascade\System\Services\Feedback;
use Cascade\System\Tables\TaxesTable;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class TaxesController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index(TaxesTable $table)
    {
        return view('system::dashboard.taxes.index', [
            'table' => $table,
            'taxes' => Tax::all()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        $entityId = intval($request->input('entity_id', '0'));
        $entity = $entityId > 0 ? Tax::find($entityId) : new Tax();
        if($entity == null) { return back(); }

        if(!$request->filled('name'))
        {
            Feedback::error(__('Please fill tax name'));
            Feedback::flash();
            return back();
        }

        $entity->name = $request->input('name');
        $entity->rate = doubleval($request->input('rate', 0));
        $entity->rate_type = TaxRateType::from(intval($request->input('rate_type', 0)));
        $entity->code = $request->input('code', '');
        $entity->parent_id = intval($request->input('parent_id', 0));
        $entity->save();

        Feedback::success(__('Tax Saved Successfully'));
        Feedback::flash();
        return to_route('dashboard.system.taxes.index');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit(Tax $tax)
    {
        return $this->index(new TaxesTable)->with([
            'entity' => $tax
        ]);
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy(Tax $tax)
    {
        $tax->delete();
        Feedback::success(__('Tax Deleted Successfully'));
        Feedback::flash();
        return to_route('dashboard.system.taxes.index');
    }
}
