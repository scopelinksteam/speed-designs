<?php

namespace Cascade\System\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Cascade\System\Entities\Member;
use Cascade\System\Services\Feedback;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;

class RemoveMemberController extends Controller
{

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy(Member $member)
    {
        $member->delete();
        Feedback::alert(__('Member Removed Successfully'));
        Feedback::flash();
        return back();
    }
}
