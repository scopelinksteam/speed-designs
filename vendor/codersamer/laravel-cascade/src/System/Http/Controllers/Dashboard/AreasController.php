<?php

namespace Cascade\System\Http\Controllers\Dashboard;

use Cascade\System\Entities\Area;
use Cascade\System\Entities\City;
use Cascade\System\Entities\Country;
use Cascade\System\Services\Feedback;
use Cascade\System\Services\System;
use Cascade\System\Tables\AreasTable;
use Cascade\System\Tables\CitiesTable;
use Cascade\System\Tables\CountriesTable;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Gate;

class AreasController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        return System::FindView('system::dashboard.geo.areas.index', [
            'cities' => City::all(),
            'table' => AreasTable::class
        ]);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        $entityId = intval($request->input('entity_id', '0'));
        Gate::authorize($entityId > 0 ? 'edit area' : 'create area');
        $entity = $entityId > 0 ? Area::find($entityId) : new Area();
        if(!$request->filled('name'))
        { Feedback::error(__('Area name is Required')); }
        if(!$request->filled('city_id'))
        { Feedback::error(__('City is Required')); }
        if(Feedback::hasErrors())
        {
            Feedback::flash();
            return back();
        }

        $entity->name = $request->input('name');
        $entity->city_id = intval($request->input('city_id'));
        if($request->file('thumbnail'))
        { $entity->thumbnail_file = $request->file('thumbnail')->store('areas', ['disk' => 'uploads']); }
        $entity->save();
        Feedback::success(__('Area Saved Successfully'));
        Feedback::flash();
        return to_route('dashboard.system.geo.areas.index');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit(Area $area)
    {
        return $this->index()->with([
            'entity' => $area
        ]);
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy(Area $area)
    {
        Gate::authorize('delete area');
        $area->delete();
        Feedback::success(__('Area Deleted Successfully'));
        Feedback::flash();
        return back();
    }
}
