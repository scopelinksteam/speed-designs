<?php

namespace Cascade\System\Http\Controllers\Dashboard;

use Cascade\System\Entities\Status;
use Cascade\System\Services\Feedback;
use Cascade\System\Tables\StatusesTable;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Gate;

class StatusesController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index($type)
    {

        return view('system::dashboard.statuses.index', [
            'table' => StatusesTable::class,
            'type' => $type,

            'statuses' => Status::of($type)->get(),

            'routes' => $this->CaptureRoutes()
        ]);
    }


    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request, $type)
    {
        $entityId = intval($request->input('entity_id', 0));

        $names = only_filled($request->name);

        $valid = true;

        if(empty($names))
        {
            Feedback::error(__('Name is Required'));
            $valid = false;
        }

        if(!$valid) { Feedback::flash(); return back(); }

        $entity = $entityId > 0 ? Status::find($entityId) : new Status();

        $entity->type = $type;
        $entity->name = fill_locales($names);
        $entity->color = $request->input('color', '#000000');

        $entity->save();
        Feedback::getInstance()->addSuccess(__('Status Saved Successfully'));
        Feedback::flash();
        return redirect()->to($this->CaptureRoutes()('index'));
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit(Status $status, $type)
    {
        return $this->index($type)->with('status', $status);
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy(Status $status)
    {
        Gate::authorize('delete statuses');

        $status->delete();
        Feedback::success(__('Status Deleted Successfully'));
        Feedback::flash();
        return redirect()->route($this->CaptureRoutes()('index',false));
    }


    protected function CaptureRoutes()
    {
        $currentRoute = request()->route()->getName();
        $routeSegments = explode('.', $currentRoute);
        array_pop($routeSegments);
        $routeBase = implode('.', $routeSegments).'.';
        return function($name, $full = true) use($routeBase){
            $route = $routeBase.$name;
            return $full ? route($route) : $route;
        };
    }
}
