<?php

namespace Cascade\System\Http\Controllers\Dashboard;

use Cascade\System\Entities\City;
use Cascade\System\Entities\Country;
use Cascade\System\Services\Feedback;
use Cascade\System\Services\System;
use Cascade\System\Tables\CitiesTable;
use Cascade\System\Tables\CountriesTable;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Gate;

class CitiesController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        return System::FindView('system::dashboard.geo.cities.index', [
            'countries' => Country::all(),
            'table' => CitiesTable::class
        ]);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        $entityId = intval($request->input('entity_id', '0'));
        Gate::authorize($entityId > 0 ? 'edit city' : 'create city');
        $entity = $entityId > 0 ? City::find($entityId) : new City();
        if(!$request->filled('name'))
        { Feedback::error(__('City name is Required')); }
        if(!$request->filled('country_id'))
        { Feedback::error(__('Country is Required')); }
        if(Feedback::hasErrors())
        {
            Feedback::flash();
            return back();
        }

        $entity->name = $request->input('name');
        $entity->country_id = intval($request->input('country_id'));

        if($request->file('thumbnail'))
        { $entity->thumbnail_file = $request->file('thumbnail')->store('cities', ['disk' => 'uploads']); }

        $entity->save();
        Feedback::success(__('City Saved Successfully'));
        Feedback::flash();
        return to_route('dashboard.system.geo.cities.index');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit(City $city)
    {
        return $this->index()->with([
            'entity' => $city
        ]);
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy(City $city)
    {
        Gate::authorize('delete city');
        $city->delete();
        Feedback::success(__('City Deleted Successfully'));
        Feedback::flash();
        return back();
    }
}
