<?php

namespace Cascade\System\Http\Controllers\Frontend;

use Cascade\System\Entities\Comment;
use Cascade\System\Enums\CommentStatus;
use Cascade\System\Services\Feedback;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class CommentsController extends Controller
{
    public function StoreComment(Request $request)
    {
        $valid = true;
        if(!$request->filled('entity_id') || !$request->filled('entity_type'))
        { Feedback::error('Invalid Comment'); $valid = false; }
        if(!$request->filled('comment'))
        { Feedback::error(__('Please fill Comment')); $valid = false; }
        if(!auth()->check() && !$request->filled('name'))
        { Feedback::error(__('Please fill Your Name')); $valid = false; }
        if(!auth()->check() && !$request->filled('email'))
        { Feedback::error(__('Please fill Your Email')); $valid = false; }
        if(!$valid)
        {
            Feedback::flash();
            return back()->withFragment('#comment-form');
        }
        $comment = new Comment();
        if(auth()->check()) { $comment->author_id = auth()->id(); }
        else
        {
            $comment->name = $request->name;
            $comment->email = $request->email;
            $comment->website = $request->website ?? null;
            $comment->phone = $request->phone ?? '';

        }
        $comment->commentable_id = $request->entity_id;
        $comment->commentable_type = $request->entity_type;
        $comment->status = CommentStatus::Approved;
        $comment->content = $request->comment;
        $comment->subject = $request->subject ?? '';
        $comment->save();
        Feedback::success(__('Comment Saved Successfully'));
        Feedback::flash();
        return back()->withFragment('comment-'.$comment->id);
    }
}
