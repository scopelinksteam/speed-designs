<?php

namespace Cascade\System\Http\Rules;

use GuzzleHttp\Client;
use Illuminate\Contracts\Validation\InvokableRule;

class RecaptchaRule implements InvokableRule
{
    public function __invoke($attribute, $value, $fail)
    {
        $client = new Client();
        $response = $client->post('https://www.google.com/recaptcha/api/siteverify', [
            'form_params' => [
                'secret' => get_setting('integrations::recaptcha.credentials.secret_key', '', false),
                'response' => $value
            ]
        ]);
        $answer = json_decode($response->getBody()->getContents(), true);
        if(!boolval($answer['success']))
        {
            $fail(__('Failed Recaptcha verification'). '. '.implode(', ', $answer['error-codes']));
        }
    }
}