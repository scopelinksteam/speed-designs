<?php

namespace Cascade\System\Tables;

use Cascade\Dashboard\Services\Datatables\Action;
use Cascade\Dashboard\Services\Datatables\Column;
use Cascade\Dashboard\Services\Datatables\DataTable;
use Cascade\System\Entities\Priority;
use Cascade\System\Entities\Status;

class PrioritiesTable extends DataTable
{
    public function __construct(public $type = null, public $routes = null)
    {
        $this->editColumn('name', function(Priority $priority){
            return '<div class="d-flex align-items-center">'.
                    $this->color($priority->color).
                    '<span class="ms-2">'.$priority->name.'</span>'.
                    '</div>';
        });
    }

    public function query()
    {
        return Priority::query()->where('type', $this->type);
    }

    public function columns()
    {
        return [
            Column::make('name')->text(__('Name')),
        ];
    }

    public function actions()
    {

        return [
            Action::make('edit', __('Edit'))->route(call_user_func($this->routes, 'edit', false), ['priority' => 'id']),
            Action::make('delete', __('Delete'))->route(call_user_func($this->routes, 'delete', false), ['priority' => 'id'])
        ];
    }
}
