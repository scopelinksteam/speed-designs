<?php

namespace Cascade\System\Tables;

use Cascade\Dashboard\Services\Datatables\Action;
use Cascade\Dashboard\Services\Datatables\Column;
use Cascade\Dashboard\Services\Datatables\DataTable;
use Cascade\System\Entities\Category;

class CategoriesTable extends DataTable
{

    public function __construct(public $type = null, public $routes = null)
    {
        $this->editColumn('name', function(Category $category){
            $contents = $category->name;
            $parent = $category->parent;
            if($parent) { $contents .= '<br />--<small class="text-muted">'.$parent->name.'</small>'; }
            return $contents;
        });
        $this->editColumn('slug', function(Category $category){
            return $this->badge($category->slug, 'dark');
        });
        $this->editColumn('thumbnail_file', function(Category $category){
            if($category->thumbnail_file)
            { return $this->thumbnail(uploads_url($category->thumbnail_file)); }
        });
    }

    public function query()
    {
        $query = Category::of($this->type);

        return $query;
    }

    public function columns()
    {
        return [
            //Column::make('id')->text(__('ID')),
            Column::make('thumbnail_file')->text(__('Thumbnail')),
            Column::make('name')->text(__('Name')),
            Column::make('slug')->text(__('Slug')),
        ];
    }

    public function actions()
    {

        return [
            Action::make('edit', __('Edit'))->route(call_user_func($this->routes, 'edit', false), ['category' => 'id']),
            Action::make('delete', __('Delete'))->route(call_user_func($this->routes, 'delete', false), ['category' => 'id'])
        ];
    }
}
