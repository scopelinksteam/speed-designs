<?php

namespace Cascade\System\Tables;

use Cascade\Dashboard\Services\Datatables\Action;
use Cascade\Dashboard\Services\Datatables\Column;
use Cascade\Dashboard\Services\Datatables\DataTable;
use Cascade\System\Entities\Area;

class AreasTable extends DataTable
{
    public function __construct()
    {
        $this->editColumn('city', function(Area $area){
            return $this->badge(($area->city->country ? $area->city->country->name.' - ' : '' ).$area->city->name);
        });
    }

    public function query($searchTerm = '', $filters = [])
    {
        return Area::query();
    }

    public function columns()
    {
        return [
            Column::make('name')->text(__('Name')),
            Column::make('city')->text(__('City')),
        ];
    }

    public function actions()
    {
        return [
            Action::make('edit', __('Edit Area'))->route('dashboard.system.geo.areas.edit', ['area' => 'id']),
            Action::make('delete', __('Delete'))->route('dashboard.system.geo.areas.delete', ['area' => 'id']),
        ];
    }
}
