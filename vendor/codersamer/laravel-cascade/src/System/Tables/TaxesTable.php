<?php

namespace Cascade\System\Tables;

use Cascade\Dashboard\Services\Datatables\Action;
use Cascade\Dashboard\Services\Datatables\Column;
use Cascade\Dashboard\Services\Datatables\DataTable;
use Cascade\System\Entities\Tax;
use Cascade\System\Enums\TaxRateType;

class TaxesTable extends DataTable
{

    public function __construct()
    {
        $this->editColumn('rate', function(Tax $tax){
            return format_money($tax->rate).($tax->rate_type == TaxRateType::Percentage ? ' %' : '');
        });
    }

    public function query()
    {
        return Tax::query();
    }

    public function columns()
    {
        return [
            Column::make('name')->text(__('Name')),
            Column::make('rate')->text(__('Rate')),
            Column::make('code')->text(__('Code'))
        ];
    }

    public function actions()
    {
        return [
            Action::make('edit', __('Edit Tax'))->route('dashboard.system.taxes.edit', ['tax' => 'id']),
            Action::make('delete', __('Delete'))->route('dashboard.system.taxes.destroy', ['tax' => 'id'])
        ];
    }
}
