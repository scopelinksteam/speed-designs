<?php

namespace Cascade\System\Tables;

use Cascade\Dashboard\Services\Datatables\Action;
use Cascade\Dashboard\Services\Datatables\Column;
use Cascade\Dashboard\Services\Datatables\DataTable;
use Cascade\System\Entities\Currency;

class CurrenciesTable extends DataTable
{

    public function query($search = null, $filters = [])
    {
        $query = Currency::query();


        return $query;
    }

    public function columns()
    {
        return [
            Column::make('name')->text(__('Name')),
            Column::make('code')->text(__('Code')),
            Column::make('symbol')->text(__('Symbol')),
            Column::make('rate')->text(__('Exchange Rate'))
        ];
    }

    public function actions()
    {
        return [
            Action::make('edit', __('Edit'))->route('dashboard.system.currencies.edit', ['currency' => 'id'])->permission('edit currencies'),
            Action::make('delete', __('Delete'))->route('dashboard.system.currencies.delete', ['currency' => 'id'])->permission('delete currencies'),
        ];
    }
}
