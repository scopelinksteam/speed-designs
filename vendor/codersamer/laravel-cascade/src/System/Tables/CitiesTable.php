<?php

namespace Cascade\System\Tables;

use Cascade\Dashboard\Services\Datatables\Action;
use Cascade\Dashboard\Services\Datatables\Column;
use Cascade\Dashboard\Services\Datatables\DataTable;
use Cascade\System\Entities\City;

class CitiesTable extends DataTable
{
    public function __construct()
    {
        $this->editColumn('country', function(City $city){
            return $this->badge($city->country->name);
        });
        $this->editColumn('areas', function(City $city){
            return $this->badge($city->areas()->count());
        });
    }

    public function query($searchTerm = '', $filters = [])
    {
        return City::query();
    }

    public function columns()
    {
        return [
            Column::make('name')->text(__('Name')),
            Column::make('country')->text(__('Country')),
            Column::make('areas')->text(__('Areas')),
        ];
    }

    public function actions()
    {
        return [
            Action::make('edit', __('Edit City'))->route('dashboard.system.geo.cities.edit', ['city' => 'id']),
            Action::make('delete', __('Delete'))->route('dashboard.system.geo.cities.delete', ['city' => 'id']),
        ];
    }
}
