<?php

namespace Cascade\System\Tables;

use Cascade\Dashboard\Services\Datatables\Action;
use Cascade\Dashboard\Services\Datatables\Column;
use Cascade\Dashboard\Services\Datatables\DataTable;
use Cascade\System\Entities\Status;

class StatusesTable extends DataTable
{
    public function __construct(public $type = null, public $routes = null)
    {
        $this->editColumn('name', function(Status $status){
            return '<div class="d-flex align-items-center">'.
                    $this->color($status->color).
                    '<span class="ms-2">'.$status->name.'</span>'.
                    '</div>';
        });
    }

    public function query()
    {
        return Status::query()->where('type', $this->type);
    }

    public function columns()
    {
        return [
            Column::make('name')->text(__('Name')),
        ];
    }

    public function actions()
    {

        return [
            Action::make('edit', __('Edit'))->route(call_user_func($this->routes, 'edit', false), ['status' => 'id']),
            Action::make('delete', __('Delete'))->route(call_user_func($this->routes, 'delete', false), ['status' => 'id'])
        ];
    }
}
