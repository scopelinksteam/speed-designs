<?php

namespace Cascade\System\Tables;

use Cascade\Dashboard\Services\Datatables\Action;
use Cascade\Dashboard\Services\Datatables\Column;
use Cascade\Dashboard\Services\Datatables\DataTable;
use Cascade\System\Entities\Country;

class CountriesTable extends DataTable
{
    public function __construct()
    {
        $this->editColumn('cities', function(Country $country){
            return $this->badge($country->cities()->count());
        });
    }

    public function query($searchTerm = '', $filters = [])
    {
        return Country::query();
    }

    public function columns()
    {
        return [
            Column::make('name')->text(__('Name')),
            Column::make('code')->text(__('Code')),
            Column::make('cities')->text(__('Cities'))
        ];
    }

    public function actions()
    {
        return [
            Action::make('edit', __('Edit Country'))->route('dashboard.system.geo.countries.edit', ['country' => 'id']),
            Action::make('delete', __('Delete'))->route('dashboard.system.geo.countries.delete', ['country' => 'id']),
        ];
    }
}
