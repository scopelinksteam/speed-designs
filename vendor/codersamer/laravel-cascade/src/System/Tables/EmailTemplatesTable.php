<?php

namespace Cascade\System\Tables;

use Cascade\Dashboard\Services\Datatables\Action;
use Cascade\Dashboard\Services\Datatables\Column;
use Cascade\Dashboard\Services\Datatables\DataTable;
use Cascade\System\Entities\EmailTemplate;
use Cascade\System\Services\EmailTemplateComposer;

class EmailTemplatesTable extends DataTable
{

    protected EmailTemplateComposer $composer;

    public function __construct(public $params)
    {
        $this->composer = EmailTemplate::findComposer($params['type']);
    }

    public function query()
    {
        $query = $this->composer->templates();

        return $query;
    }

    public function columns()
    {
        return [
            Column::make('id')->text(__('ID')),
            Column::make('name')->text(__('Name')),
            Column::make('title')->text(__('Title'))
        ];
    }

    public function actions()
    {
        $actions = [
            Action::make('edit', __('Edit Template'))->route($this->params['routes']('edit', false), ['template' => 'id'])->permission('edit email templates'),
        ];
        if($this->composer->destroyable())
        {
            $actions[] = Action::make('delete', __('Delete Template'))->route($this->params['routes']('delete', false), ['template' => 'id'])->permission('delete email templates');
        }
        return $actions;
    }
}
