<?php

namespace Cascade\System\Tables;

use Cascade\Dashboard\Services\Datatables\Action;
use Cascade\Dashboard\Services\Datatables\Column;
use Cascade\Dashboard\Services\Datatables\DataTable;
use Cascade\System\Entities\Label;
use Cascade\System\Entities\Priority;
use Cascade\System\Entities\Status;
use Cascade\System\Enums\LabelFeatures;
use Cascade\System\Services\Generators\LabelBuilder;

class LabelsTable extends DataTable
{
    protected ?LabelBuilder $type = null;
    public function __construct(String $type, public $routes = null)
    {
        $builder = Label::findBuilder($type);
        $this->type = $builder;

        $this->editColumn('name', function(Label $label) use($builder){
            return '<div class="d-flex align-items-center">'.
                    ($builder->supports(LabelFeatures::Color) ? $this->color($label->color) : '').
                    '<span class="ms-2">'.$label->name.'</span>'.
                    '</div>';
        });
    }

    public function query()
    {
        return call_user_func([$this->type->type(), 'query']);
    }

    public function columns()
    {
        $columns = [];
        if($this->type->supports(LabelFeatures::Name))
        {
            $columns[] = Column::make('name')->text(__('Name'));
        }
        return $columns;
    }

    public function actions()
    {

        return [
            Action::make('edit', __('Edit'))->route(call_user_func($this->routes, 'edit', false), ['label' => 'id']),
            Action::make('delete', __('Delete'))->route(call_user_func($this->routes, 'delete', false), ['label' => 'id'])
        ];
    }
}
