<?php

namespace Cascade\System\Tables;

use Cascade\Dashboard\Services\Datatables\Action;
use Cascade\Dashboard\Services\Datatables\Column;
use Cascade\Dashboard\Services\Datatables\DataTable;
use Cascade\System\Entities\Tag;

class TagsTable extends DataTable
{

    public function __construct(public $params = null) {}

    public function query()
    {
        $query = Tag::of($this->params['type']);

        return $query;
    }

    public function columns()
    {
        return [
            Column::make('id')->text(__('ID')),
            Column::make('name')->text(__('Name'))
        ];
    }

    public function actions()
    {
        return [
            Action::make('edit', __('Edit'))->route($this->params['routes']('edit', false), ['tag' => 'id']),
            Action::make('delete', __('Delete'))->route($this->params['routes']('delete', false), ['tag' => 'id'])
        ];
    }
}
