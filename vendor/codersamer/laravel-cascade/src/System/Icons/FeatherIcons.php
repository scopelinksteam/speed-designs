<?php

namespace Cascade\System\Icons;

use Cascade\System\Abstraction\IconLibrary;

class FeatherIcons extends IconLibrary
{
    public function json(): string
    {
        return $this->localJson('feather-icons');
    }

    public function style(): string
    {
        return $this->localStyle('feather-icons');
    }
}
