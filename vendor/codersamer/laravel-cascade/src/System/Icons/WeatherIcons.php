<?php

namespace Cascade\System\Icons;

use Cascade\System\Abstraction\IconLibrary;

class WeatherIcons extends IconLibrary
{
    public function json(): string
    {
        return $this->localJson('weather-icons');
    }

    public function style(): string
    {
        return $this->localStyle('weather-icons');
    }
}
