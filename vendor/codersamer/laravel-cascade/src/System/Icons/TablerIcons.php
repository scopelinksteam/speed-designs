<?php

namespace Cascade\System\Icons;

use Cascade\System\Abstraction\IconLibrary;

class TablerIcons extends IconLibrary
{
    public function json(): string
    {
        return $this->localJson('tabler-icons');
    }

    public function style(): string
    {
        return $this->localStyle('tabler-icons');
    }
}
