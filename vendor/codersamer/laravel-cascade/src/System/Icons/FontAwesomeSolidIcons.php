<?php

namespace Cascade\System\Icons;

use Cascade\System\Abstraction\IconLibrary;

class FontAwesomeSolidIcons extends IconLibrary
{
    public function json(): string
    {
        return $this->localJson('font-awesome-solid');
    }

    public function style(): string
    {

        return $this->localStyle('font-awesome-all');
    }
}
