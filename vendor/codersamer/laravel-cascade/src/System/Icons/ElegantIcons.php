<?php

namespace Cascade\System\Icons;

use Cascade\System\Abstraction\IconLibrary;

class ElegantIcons extends IconLibrary
{
    public function json(): string
    {
        return $this->localJson('elegant-icons');
    }

    public function style(): string
    {
        return $this->localStyle('elegant-icons');
    }
}
