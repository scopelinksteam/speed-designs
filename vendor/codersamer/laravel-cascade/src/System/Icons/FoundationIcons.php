<?php

namespace Cascade\System\Icons;

use Cascade\System\Abstraction\IconLibrary;

class FoundationIcons extends IconLibrary
{
    public function json(): string
    {
        return $this->localJson('foundation-icons');
    }

    public function style(): string
    {
        return $this->localStyle('foundation-icons');
    }
}
