<?php

namespace Cascade\System\Icons;

use Cascade\System\Abstraction\IconLibrary;

class IcoMoonIcons extends IconLibrary
{
    public function json(): string
    {
        return $this->localJson('icomoon');
    }

    public function style(): string
    {
        return $this->localStyle('icomoon');
    }
}
