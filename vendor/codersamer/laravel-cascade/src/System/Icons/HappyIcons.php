<?php

namespace Cascade\System\Icons;

use Cascade\System\Abstraction\IconLibrary;

class HappyIcons extends IconLibrary
{
    public function json(): string
    {
        return $this->localJson('happy-icons');
    }

    public function style(): string
    {
        return $this->localStyle('happy-icons');
    }
}
