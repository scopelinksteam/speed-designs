<?php

namespace Cascade\System\Icons;

use Cascade\System\Abstraction\IconLibrary;

class FontAwesomeBrandIcons extends IconLibrary
{
    public function json(): string
    {
        return $this->localJson('font-awesome-brands');
    }

    public function style(): string
    {
        return $this->localStyle('font-awesome-all');
    }
}
