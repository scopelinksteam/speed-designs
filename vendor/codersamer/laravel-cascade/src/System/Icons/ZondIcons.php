<?php

namespace Cascade\System\Icons;

use Cascade\System\Abstraction\IconLibrary;

class ZondIcons extends IconLibrary
{
    public function json(): string
    {
        return $this->localJson('zondicons');
    }

    public function style(): string
    {
        return $this->localStyle('zondicons');
    }
}
