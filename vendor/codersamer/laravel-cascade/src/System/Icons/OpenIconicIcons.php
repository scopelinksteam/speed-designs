<?php

namespace Cascade\System\Icons;

use Cascade\System\Abstraction\IconLibrary;

class OpenIconicIcons extends IconLibrary
{
    public function json(): string
    {
        return $this->localJson('open-iconic');
    }

    public function style(): string
    {
        return $this->localStyle('open-iconic');
    }
}
