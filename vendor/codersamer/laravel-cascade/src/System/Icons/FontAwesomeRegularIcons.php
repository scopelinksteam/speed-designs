<?php

namespace Cascade\System\Icons;

use Cascade\System\Abstraction\IconLibrary;

class FontAwesomeRegularIcons extends IconLibrary
{
    public function json(): string
    {
        return $this->localJson('font-awesome-regular');
    }

    public function style(): string
    {
        return $this->localStyle('font-awesome-all');
    }
}
