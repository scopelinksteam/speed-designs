<?php

namespace Cascade\System\Icons;

use Cascade\System\Abstraction\IconLibrary;

class BootstrapIcons extends IconLibrary
{
    public function json(): string
    {
        return $this->localJson('bootstrap-icons');
    }

    public function style(): string
    {
        return $this->localStyle('bootstrap-icons');
    }
}
