<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddExtraDataToAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('addresses', function (Blueprint $table) {
           $table->string('apartment')->after('address')->nullable();
           $table->string('floor')->after('address')->nullable();
           $table->string('street')->after('address')->nullable();
           $table->string('building')->after('address')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('addresses', function (Blueprint $table) {
            $table->dropColumn('apartment');
            $table->dropColumn('floor');
            $table->dropColumn('street');
            $table->dropColumn('building');
        });
    }
}
