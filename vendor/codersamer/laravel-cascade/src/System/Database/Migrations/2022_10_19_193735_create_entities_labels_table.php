<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEntitiesLabelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('entities_labels', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('label_id');
            $table->nullableMorphs('labelizable');
            $table->timestamps();
            $table->unique(['label_id', 'labelizable_type', 'labelizable_id'], 'entity_category');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('entities_labels');
    }
}
