<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('countries', function (Blueprint $table) {
            $table->dropColumn('name');
        });
        Schema::table('countries', function (Blueprint $table) {
            $table->text('name')->nullable()->after('id');
        });
        Schema::table('cities', function (Blueprint $table) {
            $table->dropColumn('name');
        });
        Schema::table('cities', function (Blueprint $table) {
            $table->text('name')->nullable()->after('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('countries', function (Blueprint $table) {
            $table->dropColumn('name');
        });
        Schema::table('countries', function (Blueprint $table) {
            $table->string('name')->nullable();
        });
        Schema::table('cities', function (Blueprint $table) {
            $table->dropColumn('name');
        });
        Schema::table('cities', function (Blueprint $table) {
            $table->string('name')->nullable();
        });
    }
};
