<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('countries', function (Blueprint $table) {
            $table->string('thumbnail_file')->after('name')->nullable();
            $table->string('phone_code')->after('name')->nullable();
        });
        Schema::table('cities', function (Blueprint $table) {
            $table->string('thumbnail_file')->after('name')->nullable();
            $table->string('phone_code')->after('name')->nullable();
            $table->string('code')->after('name')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('countries', function (Blueprint $table) {
            $table->dropColumn('thumbnail_file');
            $table->dropColumn('phone_code');
        });
        Schema::table('cities', function (Blueprint $table) {
            $table->dropColumn('thumbnail_file');
            $table->dropColumn('phone_code');
            $table->dropColumn('code');
        });
    }
};
