<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('categories', function (Blueprint $table) {
            $table->text('meta_keywords')->after('name')->nullable();
            $table->text('meta_description')->after('name')->nullable();
            $table->text('meta_title')->after('name')->nullable();
            $table->text('excerpt')->after('name')->nullable();
            $table->text('description')->after('name')->nullable();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('categories', function (Blueprint $table) {
            $table->dropColumn('meta_keywords');
            $table->dropColumn('meta_description');
            $table->dropColumn('meta_title');
            $table->dropColumn('excerpt');
            $table->dropColumn('description');
        });

    }
};
