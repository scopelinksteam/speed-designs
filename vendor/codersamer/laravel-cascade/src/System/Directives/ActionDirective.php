<?php

namespace Cascade\System\Directives;


class ActionDirective
{
    public function handle($expression)
    {
        return "<?php do_action({$expression}); ?>";
    }
}
