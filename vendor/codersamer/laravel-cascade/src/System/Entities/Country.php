<?php

namespace Cascade\System\Entities;

use Cascade\System\Entities\Currency;
use Cascade\System\Traits\HasAttributes;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Spatie\Translatable\HasTranslations;

class Country extends Model
{

    use HasAttributes, HasTranslations;

    protected $table = 'countries';

    public $translatable = [
        'name'
    ];

    public function currency()
    { return $this->belongsTo(Currency::class, 'currency_id'); }

    public function cities()
    { return $this->hasMany(City::class, 'country_id'); }
}
