<?php

namespace Cascade\System\Entities;

use Cascade\System\Traits\HasAttributes;
use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class Area extends Model
{

    use HasAttributes, HasTranslations;

    protected $table = 'areas';

    public $translatable = [
        'name'
    ];

    public function city()
    {
        return $this->belongsTo(City::class, 'city_id');
    }
}
