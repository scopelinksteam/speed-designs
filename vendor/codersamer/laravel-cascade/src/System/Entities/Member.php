<?php

namespace Cascade\System\Entities;

use Cascade\System\Enums\MemberStatus;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Member extends Model
{
    protected $table = 'members';

    protected $casts = [
        'status' => MemberStatus::class
    ];

    public function joinable()
    {
        return $this->morphTo('joinable');
    }

    public function member()
    {
        return $this->morphTo('member');
    }

    public function scopeGroup(Builder $query, $group)
    {
        $group = str_replace(' ', '_',strtoupper($group));
        return $query->where('group', $group);
    }
}
