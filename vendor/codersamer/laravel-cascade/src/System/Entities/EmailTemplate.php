<?php

namespace Cascade\System\Entities;

use Cascade\System\Contracts\Exportable;
use Cascade\System\Emails\EmailTemplateMail;
use Cascade\System\Services\EmailTemplateComposer;
use Exception;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Notification;
use Cascade\System\Notifications\EmailTemplateNotification;

class EmailTemplate extends Model
{
    protected $table = 'email_templates';

    protected static $composers = [];

    /**
     * Register Email Template Types
     *
     * @param String|Exportable|array $type
     *
     * @param String|null $slug
     *
     * @return boolean
     */
    public static function register(EmailTemplateComposer $composer) : bool
    {
        static::$composers[$composer->type()] = $composer;
        return true;
    }



    /**
     * Fetch Registered Email Templates Types
     *
     * @return array
     */
    public static function composers() : array
    {
        return static::$composers;
    }

    /**
     * Find Slug for Registered Type
     *
     * @param String|Exportable $typeClass
     *
     * @return EmailTemplateComposer|null
     */
    public static function findComposer(String|Exportable $typeClass, ?String $identifier = null) : ?EmailTemplateComposer
    {
        $typeClass = $typeClass instanceof Exportable ? $typeClass->export() : $typeClass;
        foreach(static::$composers as $type => $composer)
        {
            if($type == $typeClass)
            {
                if($identifier == null) { return $composer; }
                if(in_array($identifier, $composer->identifiers())) { return $composer; }
            }
        }
        return null;
    }

    /**
     * Query Scope: Limits Query to Templates of Specific Type
     *
     * @param Builder $builder
     *
     * @param String|Exportable $typeClass
     *
     * @return void
     */
    public function scopeOf(Builder $builder, String|Exportable $typeClass)
    {
        $typeClass = $typeClass instanceof Exportable ? $typeClass->export() : $typeClass;
        return $builder->where('type', $typeClass);
    }

    /**
     * Query Scope: Limits Query to Single Template of Specific Type and Identifier
     *
     * @param Builder $builder
     *
     * @param String|Exportable $typeClass
     *
     * @param String|null $identifierSlug
     *
     * @return void
     */
    public function scopeFor(Builder $builder, String|Exportable $typeClass, ?String $identifierSlug = null)
    {
        $typeClass = $typeClass instanceof Exportable ? $typeClass->export() : $typeClass;
        return $builder->where('type', $typeClass)->where('identifier', $identifierSlug)->limit(1);
    }

    /**
     * Sends Email Template to Specific Receivers
     *
     * @param String|Collection|array $receivers
     *
     * @param array $data
     *
     * @return bool
     */
    public function SendTo(String|Collection|array $receivers, array $data = [])
    {
        if(!isset(static::$composers[$this->type])) { return false; }

        $composer = static::$composers[$this->type];
        $composer = $composer;
        $composer->template($this);
        $composer->data($data);

        $receivers = is_string($receivers) ? [$receivers] : $receivers;
        $receivers = is_array($receivers) ? collect($receivers) : $receivers;

        $notification = new EmailTemplateNotification($composer);

        foreach($receivers as $key => $receiver)
        {
            if(is_string($key))
            {
                $receiver = [$key => $receiver];
            }
            if(is_object($receiver) && method_exists($receiver, 'notify'))
            {
                $receiver->notify($notification);
            }
            else  { Notification::route('mail', $receiver)->notify($notification); }
        }
        return true;
    }
}
