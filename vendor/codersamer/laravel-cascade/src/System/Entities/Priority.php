<?php

namespace Cascade\System\Entities;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class Priority extends Model
{
    use HasTranslations;

    protected $table = 'priorities';

    public $translatable = ['name'];

    protected static $types = [];

    public static function register(String $typeClass, ?String $slug = null) : bool
    {
        if(!class_exists($typeClass)) { return false; }
        if($slug == null) { $slug = class_basename($typeClass); }
        $slug = (String)str($slug)->slug();
        static::$types[$slug] = $typeClass;
        return true;
    }

    public static function findSlug(String $typeClass)
    {
        foreach(static::$types as $slug => $class)
        {
            if($class == $typeClass) { return $slug; }
        }
        return null;
    }

    public function scopeOf(Builder $builder, String $typeClass)
    {
        return $builder->where('type', $typeClass);
    }
}
