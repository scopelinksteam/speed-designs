<?php

namespace Cascade\System\Entities;

use Cascade\System\Abstraction\NotificationBase;
use Cascade\System\Services\NotificationService;
use Exception;
use Illuminate\Notifications\DatabaseNotification as NotificationsDatabaseNotification;

class DatabaseNotification extends NotificationsDatabaseNotification
{
    public function reference()
    {
        return $this->morphTo('reference');
    }

    public function getBuilderAttribute()
    {
        $object = null;
        try { $object = unserialize($this->attributes['payload']); }
        catch(Exception $ex) { }
        if($object != null && $object instanceof NotificationBase)
        {
            $object->SetEntity($this);
        }
        if($object == null)
        {
            $this->delete();
        }
        return $object;
    }
    public function getHandlerAttribute()
    {
        return NotificationService::GetDefinition($this->type);
    }

    public function getTitleAttribute()
    {
        $handler = $this->handler;
        return $handler ? $handler->title() : null;
    }

    public function getIconAttribute()
    {
        $handler = $this->handler;
        $icon = $handler ? $handler->icon() : '';
        if(empty($icon)){return '';}
        if(filter_var($icon, FILTER_VALIDATE_URL) === false)
        {
            return sprintf('<img src="%s"/>', $icon);
        }
        else
        {
            //Icon
            return sprintf('<i class="%s"></i>', $icon);
        }
        return '';
    }


    public function render()
    {
        $handler = $this->handler;
        return $handler ? $handler->render($this) : null;
    }
}
