<?php

namespace Cascade\System\Entities;

use Cascade\System\Enums\DiscountType;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Discount extends Model
{
    protected $table = 'discountables';

    protected $casts = [
        'type' => DiscountType::class,
        'amount' => 'double'
    ];

    public function discountable()
    {
        return $this->morphTo('discountable');
    }

    public function reference()
    {
        return $this->morphTo('reference');
    }

    public function CalculateFor($referenceAmount)
    {
        if($this->type == DiscountType::None) { return 0; }
        if($this->type == DiscountType::Fixed) { return $this->amount; }
        if($this->type == DiscountType::Percentage)
        {
            $referenceAmount = doubleval($referenceAmount);
            if($referenceAmount == 0) { return 0; }
            return ($referenceAmount / 100) * $this->amount;
        }
        return 0;
    }
}
