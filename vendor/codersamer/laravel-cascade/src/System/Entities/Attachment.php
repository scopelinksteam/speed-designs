<?php

namespace Cascade\System\Entities;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Attachment extends Model
{
    protected $table = 'attachments';

    public function attachmentable()
    {
        return $this->morphTo('attachmentable');
    }

    public function scopeOf(Builder $builder, $identifier)
    {
        return $builder->where('identifier', $identifier);
    }

    public static function Store($file, $attachmentable = null, $identifier = null)
    {

    }
}
