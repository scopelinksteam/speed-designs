<?php

namespace Cascade\System\Entities;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class Tag extends Model
{
    use HasTranslations;

    protected $table = 'tags';

    protected $casts = [
        'status' => CategoryStatus::class
    ];

    public $translatable = ['name'];

    protected static $types = [];

    public static function register(String $typeClass, ?String $slug = null) : bool
    {
        if(!class_exists($typeClass)) { return false; }
        if($slug == null) { $slug = class_basename($typeClass); }
        $slug = (String)str($slug)->slug();
        static::$types[$slug] = $typeClass;
        return true;
    }

    public static function findSlug(String $typeClass)
    {
        foreach(static::$types as $slug => $class)
        {
            if($class == $typeClass) { return $slug; }
        }
        return null;
    }

    public function scopeOf(Builder $builder, String $typeClass)
    {
        return $builder->where('type', $typeClass);
    }

    public static function SyncRequired(array|String $tags, $typeClass)
    {
        $tags = is_array($tags) ? $tags : [$tags];
        $objects = [];
        foreach($tags as $tag)
        {
            if(intval($tag) == $tag) { $objects[] = Tag::find($tag); continue; }
            $tagSlug = str($tag)->slug();
            $exist = static::of($typeClass)->where('slug', $tagSlug)->first();
            if($exist) { $objects[] = $exist; continue; }
            $exist = new Tag();
            $exist->name = fill_locales([], $tag);
            $exist->type = $typeClass;
            $exist->slug = $tagSlug;
            $exist->save();
            $objects[] = $exist;
        }
        return collect($objects);
    }
}
