<?php

namespace Cascade\System\Entities;

use Cascade\System\Traits\HasAttributes;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Spatie\Translatable\HasTranslations;

class City extends Model
{

    use HasAttributes, HasTranslations;

    protected $table = 'cities';

    public $translatable = [
        'name'
    ];

    public function country()
    {
        return $this->belongsTo(Country::class, 'country_id');
    }

    public function areas()
    { return $this->hasMany(Area::class, 'city_id'); }
}
