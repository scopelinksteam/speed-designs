<?php

namespace Cascade\System\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\MorphTo;
use Spatie\Translatable\HasTranslations;

class EntityAttribute extends Model
{
    use HasTranslations;

    protected $table = 'attributes';

    protected $casts = [
        'is_translatable' => 'boolean'
    ];

    protected $translatable = [];

    public static function boot()
    {
        parent::boot();
        static::retrieved(function(EntityAttribute $attr){
            if(!$attr->is_translatable) {
                $attr->DisableTranslations();
            }
        });
        static::saving(function(EntityAttribute $attr){
            if(is_array($attr->value) || is_object($attr->value)) { $attr->value = json_encode($attr->value); }
            return $attr;
        });
    }

    public function DisableTranslations()
    {
        $this->translatable = [];
    }
    public function bindable() : MorphTo
    {
        return $this->morphTo('bindable');
    }
}
