<?php

namespace Cascade\System\Entities;

use Cascade\System\Enums\LabelFeatures;
use Cascade\System\Services\Generators\LabelBuilder;
use Cascade\System\Traits\HasAttributes;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class Label extends Model
{
    use HasTranslations, HasAttributes;

    protected $table = 'labels';

    public $translatable = ['name'];

    protected static $types = [];

    public static function register(String|LabelBuilder $typeClass, ?String $slug = null) : bool
    {

        if(is_object($typeClass))
        {
            static::$types[$typeClass->slug()] = $typeClass;
            return true;
        }

        if(!class_exists($typeClass)) { return false; }

        if($slug == null) { $slug = class_basename($typeClass); }
        $slug = (String)str($slug)->slug();
        $name = class_basename($typeClass);

        static::$types[$slug] = LabelBuilder::make($name, $typeClass)->slug($slug)->features(LabelFeatures::All);
        return true;
    }

    public static function findSlug(String $typeClass)
    {
        foreach(static::$types as $slug => $object)
        {
            if($object->type() == $typeClass) { return $slug; }
        }
        return null;
    }

    public static function findBuilder(String $typeClass)
    {
        foreach(static::$types as $slug => $object)
        {
            if($object->type() == $typeClass) { return $object; }
        }
        return null;
    }

    public function scopeOf(Builder $builder, String|LabelBuilder $typeClass)
    {
        if($typeClass instanceof LabelBuilder)
        { $typeClass = $typeClass->type(); }
        return $builder->where('type', $typeClass);
    }

    public function items()
    {
        return $this->morphedByMany($this->type, 'labelizable', 'entities_labels');
    }
}
