<?php

namespace Cascade\System\Entities;

use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class Currency extends Model
{

    use HasTranslations;

    protected $table = 'currencies';

    protected $casts = [
        'rate' => 'double'
    ];

    public $translatable = ['name'];

    public function getIsActiveAttribute()
    {
        $activeCurrency = currency();
        return $activeCurrency && $activeCurrency->id == $this->id;
    }

    public function getSwitchLinkAttribute()
    {
        return route('system.set_currency', ['currency' => $this]);
    }

    public function exchange($amount)
    {
        return $amount * $this->rate;
    }


}
