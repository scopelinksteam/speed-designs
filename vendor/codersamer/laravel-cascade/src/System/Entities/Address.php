<?php

namespace Cascade\System\Entities;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Address extends Model
{
    protected $table = 'addresses';

    public function addressable()
    {
        return $this->morphTo('addressable');
    }

    public function country()
    {
        return $this->belongsTo(Country::class);
    }

    public function getCityNameAttribute()
    {
        $city = City::find(intval($this->city));
        return $city != null ? $city->name : $this->city;
    }

    public function scopeOf(Builder $builder, $identifier)
    {
        return $builder->where('identifier', $identifier);
    }
}
