<?php

namespace Cascade\System\Entities;

use BackedEnum;
use Illuminate\Database\Eloquent\Model;
use UnitEnum;

class Option extends Model
{
    protected $table = 'options';

    protected static function FormatKey(BackedEnum|String $key) : String
    {
        if($key instanceof BackedEnum) { $key = $key->value; }
        return strtoupper(str_replace(' ', '_', $key));
    }

    public static function Set(BackedEnum|String $key, $value)
    {
        $key = static::FormatKey($key);
        $entity = static::where('key', $key)->first();
        $entity = $entity == null ? new static : $entity;
        $entity->key = $key;
        $entity->value = serialize($value);
        $entity->save();
    }

    public static function Get(BackedEnum|String $key, $default = null)
    {
        $entity = static::where('key', static::FormatKey($key))->first();
        $value = $default;
        if($entity)
        {
            $unserialized = unserialize($entity->value);
            if($unserialized !== null)
            { $value = $unserialized; }
        }
        return $value;
    }
}