<?php

namespace Cascade\System\Entities;

use Cascade\System\Services\Activators\ModulesActivator;
use Cascade\System\Services\System;
use Nwidart\Modules\Facades\Module as FacadesModule;
use Symfony\Component\Finder\Finder;

class Module
{

    protected $name = '';

    protected $alias = '';

    protected $description = '';

    protected $file = '';

    protected $slug = '';

    protected $protected = false;

    protected $keywords = [];

    protected $priority = 0;

    protected $providers = [];

    protected $aliases = [];

    protected $files = [];

    protected $requires = [];

    protected $enabled = true;

    protected $author = '';

    protected $icon = '';

    public static function LoadFrom($file)
    {
        return (new Module())->Load($file);
    }

    protected function Load($file)
    {
        $this->file = (string)$file;
        $this->path = dirname($this->file);
        $moduleData = json_decode(file_get_contents($this->file));
        $this->name = $moduleData->name;
        $this->slug = \str($this->name)->slug();
        $this->protected = (bool)(isset($moduleData->protected) ? $moduleData->protected : false);
        $this->description = $moduleData->description;
        $this->keywords = $moduleData->keywords;
        $this->alias = $moduleData->alias;
        $this->priority = $moduleData->priority;
        $this->providers = $moduleData->providers;
        $this->aliases = $moduleData->aliases ?? [];
        $this->files = $moduleData->files;
        $this->requires = $moduleData->requires ?? [];

        $this->enabled = (new ModulesActivator)->hasStatus(FacadesModule::find($this->name), true);
        $this->author = isset($moduleData->author) ? $moduleData->author : __('Cascade');
        $this->icon = isset($moduleData->icon) ? $moduleData->icon : 'cubes';
        return $this;
    }

    public function GetModuleFile() { return $this->file; }
    public function GetPath($appends = '') { return rtrim($this->path.'/'.$appends, '/'); }
    public function GetName() { return $this->name; }
    public function GetSlug() { return $this->slug; }
    public function GetDescription() { return $this->description; }
    public function IsProtected() { return $this->protected; }
    public function GetKeywords() { return $this->keywords; }
    public function GetAlias() { return $this->alias; }
    public function GetPriority() { return $this->priority; }
    public function GetProviders() { return $this->providers; }
    public function GetAliases() { return $this->aliases; }
    public function GetFiles() { return $this->files; }
    public function GetRequirements() { return $this->requires; }
    public function IsEnabled() { return $this->enabled; }
    public function GetAuthor() { return $this->author; }
    public function GetIcon() { return $this->icon; }
    public function Activate() { if(!$this->IsEnabled()) { System::EnableModule($this->GetName()); }}
    public function Deactivate() { if($this->IsEnabled()) { System::DisableModule($this->GetName()); }}


    public static function InstalledModules()
    {

    }

    public static function All()
    {
        $finder = new Finder();
        $modulesFiles = $finder->in([base_path('/Modules')])->depth(1)->name('module.json')->files();
        $modules = [];

        foreach($modulesFiles as $file) { $modules[] = Module::LoadFrom($file); }
        return collect($modules);
    }

}
