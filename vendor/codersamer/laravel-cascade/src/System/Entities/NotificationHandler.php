<?php

namespace Cascade\System\Entities;

use Closure;
use Illuminate\Notifications\DatabaseNotification;
use Illuminate\Support\Facades\View as FacadesView;
use Illuminate\View\View;
use Str;

class NotificationHandler
{
    protected String $name = '';
    protected String $title = '';
    protected String $class = '';
    protected String $slug = '';
    protected String $icon = '';
    protected String|View|Closure|null $view = null;

    public static function make(String $class) : static
    {
        return new static($class);
    }

    protected function __construct(String $class)
    {
        $this->class = $class;
    }

    public function class(String $class = null) : String|static
    {
        if($class !== null) { $this->class = $class; return $this; }
        return $this->class;
    }

    public function name(String $name = null) : String|static
    {
        if($name !== null)
        { $this->name = $name; return $this; }
        return $this->name ?? $this->class;
    }

    public function icon(String $icon = null) : String|static
    {
        if($icon !== null)
        { $this->icon = $icon; return $this; }
        return $this->icon ?? '';
    }

    public function title(String $title = null) : String|static
    {
        if($title !== null)
        { $this->title = $title; return $this; }
        return empty($this->title) ? $this->name() : $this->title;
    }

    public function slug(String $slug = null) : String|static
    {
        if($slug !== null) { $this->slug = $slug; return $this; }
        return $this->slug ?? \Str::slug($this->class);
    }

    public function view(String|View|Closure|null $view = null)  : String|View|Closure|null|static
    {
        if($view !== null) { $this->view = $view; return $this; }
        return $this->view;
    }

    public function render(DatabaseNotification $notification) : String
    {
        $view = $this->view();
        if(is_callable($view))
        {
            $view = call_user_func($view, $notification, $this);
        }
        //View name or Text
        if(is_string($view))
        {
            if(FacadesView::exists($view))
            {
                $view = view($view, [
                    'notification' => $notification,
                    'handler' => $this
                ]);
            }
            else
            {
                return $view;
            }
        }

        if($view instanceof View)
        {
            return $view->render();
        }
        
        return $this->title() ?? '';
    }


}
