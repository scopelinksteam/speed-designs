<?php

namespace Cascade\System\Entities;

use Cascade\System\Enums\CategoryStatus;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Support\Traits\Macroable;
use Spatie\Translatable\HasTranslations;
use stdClass;

class Category extends Model
{

    use HasTranslations;

    protected $table = 'categories';

    protected $casts = [
        'status' => CategoryStatus::class,
        'parent_id' => 'int'
    ];

    public $translatable = [
        'name', 'description', 'excerpt', 'meta_title', 'meta_description', 'meta_keywords'
    ];

    protected static $types = [];

    public static function register(String $typeClass, ?String $slug = null) : bool
    {
        if(!class_exists($typeClass)) { return false; }
        if($slug == null) { $slug = class_basename($typeClass); }
        $slug = (String)str($slug)->slug();
        static::$types[$slug] = $typeClass;
        return true;
    }

    public static function findSlug(String $typeClass)
    {
        foreach(static::$types as $slug => $class)
        {
            if($class == $typeClass) { return $slug; }
        }
        return null;
    }

    public function scopeOf(Builder $builder, String $typeClass)
    {
        return $builder->where('type', $typeClass);
    }

    public function parent()
    {
        return $this->belongsTo(Category::class, 'parent_id', 'id');
    }

    public function children()
    {
        return $this->hasMany(Category::class, 'parent_id', 'id');
    }

    public function items()
    {
        return $this->morphedByMany($this->type, 'categorizable', 'entities_categories');
    }

}
