<?php

namespace Cascade\System\Entities;

use Cascade\System\Services\Plugins;
use stdClass;
use Symfony\Component\Finder\Finder;

class Plugin
{

    protected $name = '';
    protected $slug = '';

    protected $alias = '';

    protected $description = '';

    protected $file = '';

    protected $path = '';

    protected $url = '#';

    protected $protected = false;

    protected $keywords = [];

    protected $priority = 0;

    protected $providers = [];

    protected $aliases = [];

    protected $files = [];

    protected $requires = [];

    protected $enabled = true;

    protected $author = '';

    protected $author_url = '#';

    protected $icon = '';

    public static function LoadFrom($file)
    {
        return (new Plugin())->Load($file);
    }

    protected function Load($file)
    {
        $this->file = (string)$file;
        $this->path = dirname($this->file);
        $moduleData = json_decode(file_get_contents($this->file));
        $this->name = $moduleData->name;
        $this->slug = \str($this->name)->slug();
        $this->protected = (bool)(isset($moduleData->protected) ? $moduleData->protected : false);
        $this->description = $moduleData->description;
        $this->url = isset($moduleData->url) ? $moduleData->url : '#';
        $this->keywords = $moduleData->keywords;
        $this->alias = $moduleData->alias;
        $this->priority = $moduleData->priority;
        $this->providers = $moduleData->providers;
        $this->aliases = $moduleData->aliases;
        $this->files = $moduleData->files;
        $this->requires = $moduleData->requires;

        $this->enabled = Plugins::IsEnabled($this->name);
        $this->author = isset($moduleData->author) ? $moduleData->author : __('Cascade');
        $this->author_url = isset($moduleData->author_url) ? $moduleData->author_url : '#';
        $this->icon = isset($moduleData->icon) ? $moduleData->icon : 'cubes';
        $this->screenshot = file_exists($this->path.'/screenshot.png') ? $this->path.'/screenshot.png' : module_path('Site', 'Resources/assets/images/default-theme-screenshot.png');
        return $this;
    }

    public function GetPath($appends = '') { return rtrim($this->path.'/'.$appends, '/'); }
    public function GetModuleFile() { return $this->file; }
    public function GetName() { return $this->name; }
    public function GetSlug() { return $this->slug; }
    public function GetDescription() { return $this->description; }
    public function GetURL() { return $this->url; }
    public function GetScreenshotFile() { return $this->screenshot; }
    public function GetScreenshotURL() { return route('dashboard.system.plugins.screenshot', ['plugin' => $this->name]); }
    public function IsProtected() { return $this->protected; }
    public function GetKeywords() { return $this->keywords; }
    public function GetAlias() { return $this->alias; }
    public function GetPriority() { return $this->priority; }
    public function GetProviders() { return $this->providers; }
    public function GetAliases() { return $this->aliases; }
    public function GetFiles() { return $this->files; }
    public function GetRequirements() { return $this->requires; }
    public function IsEnabled() { return $this->enabled; }
    public function GetAuthor() { return $this->author; }
    public function GetAuthorURL() { return $this->author_url; }
    public function GetIcon() { return $this->icon; }
    public function GetComposerJson() {
        if(!file_exists($this->GetPath('composer.json')))
        { return new stdClass; }
        $contents = file_get_contents($this->GetPath('composer.json'));
        return json_decode($contents);
     }
     public function GetComposerAttribute($attr, $default = null)
     {
        $json = $this->GetComposerJson();
        return property_exists($json, $attr) ? $json->{$attr} : $default;
    }

    public function GetComposerRequire() { return $this->GetComposerAttribute('require', []); }
    public function GetComposerRequireDev() { return $this->GetComposerAttribute('require-dev', []); }


    public static function All()
    {
        $finder = new Finder();
        $modulesFiles = $finder->in([base_path('/Plugins')])->depth(1)->name('plugin.json')->files();
        $modules = [];

        foreach($modulesFiles as $file) { $modules[] = Plugin::LoadFrom($file); }
        return collect($modules);
    }


}
