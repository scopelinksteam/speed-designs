<?php

namespace Cascade\System\Entities;

use Cascade\System\Enums\CommentStatus;
use Cascade\System\Traits\HasAttachments;
use Cascade\Users\Entities\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\MorphTo;

class Comment extends Model
{
    use HasAttachments;

    protected $table = 'comments';

    protected $casts = [
        'status' => CommentStatus::class
    ];

    public function author() : BelongsTo
    {
        return $this->belongsTo(User::class, 'author_id');
    }

    public function getNameAttribute()
    {
        return $this->author ? $this->author->name : $this->name;
    }

    public function getEmailAttribute()
    {
        return $this->author ? $this->author->email : $this->email;
    }

    public function commentable()
    {
        //return $this->mor('commentable');
    }
}
