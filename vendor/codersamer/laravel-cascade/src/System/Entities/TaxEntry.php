<?php

namespace Cascade\System\Entities;

use Cascade\System\Entities\Tax;
use Cascade\System\Enums\TaxRateType;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class TaxEntry extends Model
{
    protected $table = 'taxables';

    protected $casts = [
        'rate_type' => TaxRateType::class,
        'rate' => 'double'
    ];

    public function taxable()
    { return $this->morphTo('taxable'); }

    public function tax()
    { return $this->belongsTo(Tax::class, 'tax_id'); }

    public function reference()
    { return $this->morphTo('reference'); }

    public function CalculateFor($referenceAmount)
    {
        if($this->rate_type == TaxRateType::None) { return 0; }
        if($this->rate_type == TaxRateType::Fixed) { return $this->rate; }
        if($this->rate_type == TaxRateType::Percentage)
        {
            $referenceAmount = doubleval($referenceAmount);
            if($referenceAmount == 0) { return 0; }
            return ($referenceAmount / 100) * $this->rate;
        }
        return 0;
    }
}
