<?php

namespace Cascade\System\Entities;

use Cascade\System\Enums\TaxRateType;
use Illuminate\Database\Eloquent\Model;

class Tax extends Model
{
    protected $table = 'taxes';

    protected $casts = [
        'rate_type' => TaxRateType::class,
        'rate' => 'double'
    ];

    public function parent()
    { return $this->belongsTo(Tax::class, 'parent_id'); }
}
