<?php

namespace Cascade\System\Components;

use Cascade\System\Services\GoogleMaps;
use Cascade\System\Services\System;
use Illuminate\View\Component;

class LocationPickerComponent extends Component
{

    protected static $index = 0;

    protected static bool $scriptInjected = false;

    public function __construct(public String $name, public String $height = '480px', public float $latitude = 0, public float $longitude = 0, public int $zoom = 0) 
    {
        static::$index++;
    }

    public function renderScript()
    {
        if(static::$scriptInjected) { return ''; }
        static::$scriptInjected = true;
        return '<script src="https://unpkg.com/location-picker/dist/location-picker.min.js"></script>';
    }

    public function render()
    {
        if(!GoogleMaps::Initialized()) { return ''; }
        return System::FindView('system::shared.components.location_picker', [
            'index' => static::$index
        ]);
    }
}