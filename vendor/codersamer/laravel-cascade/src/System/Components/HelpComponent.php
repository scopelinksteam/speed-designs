<?php

namespace Cascade\System\Components;

use Cascade\System\Services\Help;
use Cascade\System\Services\System;
use Illuminate\View\Component;

class HelpComponent extends Component
{
    public function __construct(public Help $help)
    {
        
    }

    public function render()
    {
        return empty($this->help->view()) ? System::FindView('system::shared.components.help', [
            'help' => $this->help
        ]) : (is_string($this->help->view()) ? System::FindView($this->help->view(), [
            'help' => $this->help
        ]) : $this->help->view()->with([
            'help' => $this->help
        ]));
    }
}