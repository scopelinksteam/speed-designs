<?php

namespace Cascade\System\Components;

use Illuminate\View\Component;

class ReCaptchaComponent extends Component
{
    protected static $activeIndex = 0;

    public function __construct(public $action = 'submit') { }

    public function render()
    {
        $recaptchaSiteKey = get_setting('integrations::recaptcha.credentials.site_key', '', false);
        if(empty($recaptchaSiteKey)) { return ''; }
        static::$activeIndex++;
        $index = static::$activeIndex;
        $action = $this->action;
        return <<<BLADE
        <style>.grecaptcha-badge { visibility: hidden; }</style>
        <span class="recaptcha-notice">This site is protected by reCAPTCHA and the Google <a href="https://policies.google.com/privacy">Privacy Policy</a> and <a href="https://policies.google.com/terms">Terms of Service</a> apply.</span>
        <input type="hidden" name="recaptcha" class="recaptcha-input" id="recaptcha-input-$index" data-action="$action" />
        <script type="text/javascript">
        document.addEventListener('DOMContentLoaded', function(){
            var recaptchaInput = document.getElementById('recaptcha-input-$index');
            if(recaptchaInput)
            {
                if(recaptchaInput.form)
                {
                    recaptchaInput.form.addEventListener('submit', function(ev){
                        if(!recaptchaInput.value)
                        {
                            ev.preventDefault();
                            grecaptcha.ready(function() {
                                grecaptcha.execute('$recaptchaSiteKey', {action: '$action'}).then(function(token) {
                                    recaptchaInput.value = token;
                                    recaptchaInput.form.submit();
                                });
                            });
                        }
                    });
                }
            }
        });
        </script>
        BLADE;
    }
}