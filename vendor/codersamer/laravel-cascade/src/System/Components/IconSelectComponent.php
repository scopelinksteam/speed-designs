<?php

namespace Cascade\System\Components;

use Cascade\System\Services\System;
use Illuminate\View\Component;

class IconSelectComponent extends Component
{

    public $name;

    public $value;

    public $id;

    protected static $iteration = 0;

    public $class = '';

    public function __construct($name, $value = '', $class = '')
    {
        static::$iteration += 1;
        $this->name = $name;
        $this->value = $value;
        $this->class = $class;
        $this->id = 'icon-selector-'.(static::$iteration);
    }

    protected function getLibraries() : array
    {
        return [

        ];
    }

    public function render()
    {
        return System::FindView('system::dashboard.components.icon-select', [
            'libraries' => $this->getLibraries(),
            'value' => $this->value,
            'name' => $this->name,
            'id' => $this->id,
            'class' => $this->class,
        ]);
    }

}
