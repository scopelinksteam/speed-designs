<?php

namespace Cascade\System\Components\Mail;

use Illuminate\View\Component;

class LineMailComponent extends Component
{

    public function __construct()
    {

    }

    public function render()
    {
        return view('system::mail.components.line', [

        ]);
    }
}
