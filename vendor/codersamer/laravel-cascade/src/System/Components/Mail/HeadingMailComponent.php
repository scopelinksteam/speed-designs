<?php

namespace Cascade\System\Components\Mail;

use Illuminate\View\Component;

class HeadingMailComponent extends Component
{

    public function __construct(public $level = 3)
    {

    }

    public function render()
    {
        return view('system::mail.components.heading', [
            'level' => $this->level
        ]);
    }
}
