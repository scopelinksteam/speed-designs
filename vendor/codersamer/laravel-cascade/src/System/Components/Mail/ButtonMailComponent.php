<?php

namespace Cascade\System\Components\Mail;

use Illuminate\View\Component;

class ButtonMailComponent extends Component
{

    public function __construct(public $link = '#')
    {

    }

    public function render()
    {
        return view('system::mail.components.button', [
            'link' => $this->link
        ]);
    }
}
