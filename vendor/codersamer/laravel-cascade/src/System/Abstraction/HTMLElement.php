<?php

namespace Cascade\System\Abstraction;

use Cascade\System\DTO\HTML\Text;
use Closure;

abstract class HTMLElement
{
    protected $id = '';
    protected $class = '';
    protected array $data = [];
    protected String $name = '';
    protected $valueClosure = null;
    protected $for = '';
    protected $value = '';
    protected bool $checked = false;
    protected $bindable = null;

    protected array $children = [];


    public static function make(...$args) : static
    {
        return new static(...$args);
    }

    /**
     * Get or Set Element ID
     *
     * @param String|null $id
     * @return String|static
     */
    public function id(?String $id = null) : String|static
    {
        if($id != null) { $this->id = $id; return $this; }
        return $this->id;
    }

    /**
     * Get or Set Element Name
     *
     * @param String|null $name
     * @return String|static
     */
    public function name(?String $name = null) : String|static
    {
        if($name != null) { $this->name = $name; return $this; }
        return $this->name;
    }

    /**
     * Get or Set Element Name
     *
     * @param String|null $for
     * @return String|static
     */
    public function for(?String $for = null) : String|static
    {
        if($for != null) { $this->for = $for; return $this; }
        return $this->for;
    }

    /**
     * Get or Set Element Name
     *
     * @param String|null $name
     * @return mixed
     */
    public function value(null|Closure|String $value = null) : mixed
    {
        if($value !== null)
        {
            if($value instanceof Closure) { $this->valueClosure = $value; }
            else { $this->value = $value; }
            return $this;
        }
        if($this->valueClosure != null)
        {
            $this->value = ($this->valueClosure)($this->bindable);
        }
        return $this->value;
    }

    /**
     * Get or Set Element Name
     *
     * @param bool|null $name
     * @return bool|static
     */
    public function checked(?bool $checked = null) : bool|static
    {
        if($checked !== null) { $this->checked = $checked; return $this; }
        return $this->checked;
    }

    public function bind(mixed $bindable)
    {
        $this->bindable = $bindable;
    }

    public function bindable()
    {
        return $this->bindable;
    }

    /**
     * Get or Set Element Class
     *
     * @param String|null $id
     * @return String|static
     */
    public function class(?String $class = null) : String|static
    {
        if($class != null) { $this->class = $class; return $this; }
        return $this->class;
    }

    /**
     * Get or Set Element Class
     *
     * @param String|null $id
     * @return String
     */
    public function data(String $key, $value = null) : String|static
    {
        if($value != null) { $this->data[$key] = $value; return $this; }
        return isset($this->data[$key]) ? $this->data[$key] : null;
    }

    /**
     * Append HTML Element
     *
     * @param HTMLElement $element
     *
     * @return self
     */
    public function append(HTMLElement ...$elements) : static
    {
        foreach($elements as $element)
        { $this->children[] = $element;}
        return $this;
    }

    public function renderChildren()
    {
        foreach($this->children as $child)
        {
            $child->bind($this->bindable);
            echo $child->render()->with(['element' => $child])->render();
        }
    }

    public function text(String $text) : static
    {
        return $this->append(Text::make($text));
    }

    public function clear() : static { $this->children = []; return $this; }
    public abstract function render();
}
