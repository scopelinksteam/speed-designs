<?php

namespace Cascade\System\Abstraction;

use Cascade\System\Channels\WebChannel;
use Cascade\System\Entities\DatabaseNotification;
use Cascade\System\Services\NotificationService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\Notification as NotificationFacade;
use Illuminate\View\View;

abstract class NotificationBase extends Notification implements ShouldQueue
{

    use Queueable;

    protected ?DatabaseNotification $entity;

    public static function Send(...$params)
    {
        $notification = new static(...$params);
        NotificationFacade::Send($notification->notifiables(), $notification);
    }

    public function SetEntity(DatabaseNotification $notification)
    {
        $this->entity = $notification;
    }

    public function via($notifiable)
    {
        $drivers = $this->drivers();
        return $drivers;
    }


    public abstract function notifiables();
    public abstract function toMail($notifiable);
    public abstract function toArray($notifiable);

    public function definition() { return NotificationService::GetDefinition(get_class($this)); }
    public function name() { return $this->definition() != null ? $this->definition()->name() : null; }
    public function slug() { return $this->definition() != null ? $this->definition()->slug() : null; }

    public function reference() { return null; }
    public function drivers()
    {
        $slug = $this->slug() ?? '';
        return get_setting('system::notifications.'.$slug.'.channels', [WebChannel::class], false);
    }



    public function icon() {  return null;  }
    public function image() { return null; }
    public function title() { return ''; }
    public function content() { return ''; }
    public function link() { return null; }

    public function render()
    {
        $content = $this->content();
        if(empty($content)) { return '' ;}
        if(is_string($content)) { return $content; }
        if($content instanceof View)
        { return $content->with(['builder' => $this, 'notification' => $this->entity])->render() ;}
    }
}
