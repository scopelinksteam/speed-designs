<?php

namespace Cascade\System\Abstraction;

abstract class IconLibrary
{
    public abstract function json() : String;
    public abstract function style() : String;

    protected function localJson($libraryName) : String
    {
        return $libraryName.'.min.json';
    }

    protected function localStyle($libraryName) : String
    {
        return cascade_asset('plugins/icon-picker/stylesheets/'.$libraryName.'.min.css');
    }
}
