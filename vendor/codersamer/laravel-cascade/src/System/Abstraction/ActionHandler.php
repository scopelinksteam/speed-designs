<?php

namespace Cascade\System\Abstraction;

abstract class ActionHandler
{

    protected $action = '';

    protected $request;

    public function __construct($actionName)
    {
        $this->action = $actionName;
        $this->request = request();
    }
}
