<?php

namespace Cascade\System\Abstraction;

use Cascade\System\Services\System;
use Illuminate\Contracts\Foundation\Application;

abstract class Service
{
    public function register(Application $app) {}
    public function boot(Application $app) {}
    public function services(Application $app) { return []; }

    abstract function name() : String;
    abstract function description() : String;
    abstract function identifier() : String;

    public static function Enabled() : bool
    {
        return System::ServiceEnabled(static::class);
    }
}
