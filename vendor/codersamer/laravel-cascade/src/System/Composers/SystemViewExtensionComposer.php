<?php

namespace Cascade\System\Composers;

use Cascade\System\Services\System;
use Illuminate\Support\Facades\View as FacadesView;
use Illuminate\View\View;

class SystemViewExtensionComposer
{

    protected static $parsedViews = [];

    public function compose(View $view)
    {

        $name = $view->getName();
        if(in_array($name, static::$parsedViews)) { return; }
        $extensions = System::GetViewExtensions($name);
        foreach($extensions as $extension)
        {
            if(is_callable($extension->content)) { $extension->content = call_user_func($extension->content, $view->getData()); }
            if(is_string($extension->content) && FacadesView::exists($extension->content))
            { $extension->content = view($extension->content); }
            if($extension->content instanceof View) { $extension->content->with($view->getData()); }
            $view->getFactory()->startPush($extension->section, $extension->content);
        }
        static::$parsedViews[] = $name;
    }
}
