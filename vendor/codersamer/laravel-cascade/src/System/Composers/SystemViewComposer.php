<?php

namespace Cascade\System\Composers;

use Cascade\System\Services\System;
use Illuminate\View\View;

class SystemViewComposer
{

    protected static $counters = [];

    public function __construct()
    {

    }

    public function compose(View $view)
    {
        if(!isset(static::$counters[$view->getName()])) { static::$counters[$view->getName()] = 0; }
        static::$counters[$view->getName()]++;
        if(static::$counters[$view->getName()] > 1) { return; }
        $replacement = System::FindViewReplacement($view->getName());
        if($replacement != $view->getName())
        {
            $replacementData = System::GetViewData($view->getName());
            $view->setPath(view($replacement)->getPath());
            $view->with($replacementData);
        }

    }
}
