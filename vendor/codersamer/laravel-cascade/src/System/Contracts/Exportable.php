<?php

namespace Cascade\System\Contracts;

interface Exportable
{
    public function export() : String;
}
