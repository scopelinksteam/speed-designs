<?php

namespace Cascade\System\Contracts;

use Cascade\System\Services\HTMLBuilder;

interface IHTMLBuilder
{
    public function build(HTMLBuilder $builder) : HTMLBuilder;
}
