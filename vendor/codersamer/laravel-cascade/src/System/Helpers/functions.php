<?php

use Carbon\Carbon;
use Cascade\System\Entities\Category;
use Cascade\System\Entities\Currency;
use Cascade\System\Entities\Tag;
use Cascade\System\Enums\EntityAction;
use Cascade\System\Enums\EntityCardPlacement;
use Cascade\System\Services\Activators\ModulesActivator;
use Cascade\System\Services\AssetsManager;
use Cascade\System\Services\EntityBuilder;
use Cascade\System\Services\Feedback;
use Cascade\System\Services\Modules;
use Cascade\System\Services\Plugins;
use Illuminate\Support\Arr;
use Cascade\System\Services\System;
use Cascade\Tenancy\Services\TenancyService;
use Illuminate\Support\Collection;
use Nwidart\Modules\Facades\Module;
use PhpOffice\PhpSpreadsheet\Calculation\DateTimeExcel\Current;

//Load Helpers
require_once __DIR__.'/geo.php';

if(!function_exists('only_filled')){
    function only_filled($arr)
    {
        if($arr == null) { return []; }
        return Arr::where($arr, function($val, $key){
            if($val == null) { return false; }
            if(is_array($val)) { return count($val) > 0; }
            $val = strip_tags(trim($val));
            return $val != null && $val != '';
        });
    }
}

function get_logo_url()
{ return get_setting('system::general.logo', null) == null ?  url('cascade/images/logo_small.png') : uploads_url(get_setting('system::general.logo', null)); }

function on_action($action, $callback, $order = 10) { return System::OnAction($action, $callback, $order); }
function do_action($action, ...$param) { return System::DispatchAction($action, ...$param ); }
function on_filter($filter, $callback, $priority = 20, $paramsCount = 1) { return System::OnFilter($filter, $callback, $priority, $paramsCount); }
function do_filter($filter, $param, $owner = null) { return System::DispatchFilter($filter, $param, $owner); }

function get_modules($variant = null) { return Modules::GetModules($variant); }

function module_enabled($name) { return (new ModulesActivator)->hasStatus(Module::find($name), true); }
function module_disabled($name) { return !\module_enabled($name);}

function get_plugins() { return Plugins::GetPlugins(); }

function modules_manager_enabled() { return config('cascade.modules_manager', true) || config('cascade.developer_mode', false); }

if(!function_exists('faker'))
{
    function faker()
    {
        return app(Faker\Generator::class) ;//new Faker\Generator(app());
    }
}

if(!function_exists('lorem'))
{
    function lorem($words = 30)
    {
        return faker()->words($words, true);
    }
}

function plugins_manager_enabled() { return config('cascade.plugins_manager', true) || config('cascade.developer_mode', false); }
function files_manager_enabled() { return env('FILES_MANAGER', true) || config('cascade.developer_mode', false); }
function uploads_url($appends = '')
{
    return AssetsManager::UploadsUrl($appends);
}

function module_url($module, $appends = '')
{ return AssetsManager::ModuleUrl($module, $appends); }

function plugin_url($plugin, $appends = '')
{ return AssetsManager::PluginUrl($plugin, $appends); }

function theme_url($theme, $appends = '')
{ return AssetsManager::ThemeUrl($theme, $appends); }

function cascade_asset($name)
{
    return url('cascade/'.$name);
}

if(!function_exists('entity_builder'))
{
    function entity_builder($class) : ?EntityBuilder
    {
        return call_user_func([$class, 'builder']);
    }
}

if(!function_exists('entity_cards'))
{
    function entity_cards($class, String $action, ...$placements)
    {
        $builder = entity_builder($class);
        if($builder == null) { return []; }
        $actionEnum = EntityAction::from($action);
        $placementsEnums = [];
        foreach($placements as $placement) { $placementsEnums[] = EntityCardPlacement::from($placement); }
        return $builder->cards()->for($actionEnum, ...$placementsEnums);
    }
}

if(!function_exists('get_categories'))
{
    function get_categories(String|object $parent) : Collection
    {
        $parent = is_object($parent) ? get_class($parent) : $parent;
        return Category::of($parent)->get();
    }
}

if(!function_exists('get_tags'))
{
    function get_tags(String|object $parent) : Collection
    {
        $parent = is_object($parent) ? get_class($parent) : $parent;
        return Tag::of($parent)->get();
    }
}

if(!function_exists('feedback'))
{
    function feedback()
    {
        $content = Feedback::display();
        Feedback::flush();
        return $content;
    }
}

if(!function_exists('timezones'))
{
    function timezones()
    {
        $zones = [];
        $currentZone = date_default_timezone_get();
        $timestamp = time();
        foreach(timezone_identifiers_list() as $key => $zone)
        {
            date_default_timezone_set($zone);
            $zones[$zone] = $zone. ' - UTC/GMT '. date('P',$timestamp);
        }
        date_default_timezone_set($currentZone);
        return $zones;
    }
}

if(!function_exists('format_date'))
{
    function format_date(Carbon|String $date)
    {
        if(is_string($date)) { $date = Carbon::parse($date); }
        $dateFormat = get_setting('system::localization.date_format', 'Y-m-d');
        return $date->format($dateFormat);
    }
}

if(!function_exists('format_time'))
{
    function format_time(Carbon|String $time)
    {
        if(is_string($time)) { $time = Carbon::parse($time); }
        $timeFormat = get_setting('system::localization.time_format', 'h:i');
        return $time->format($timeFormat);
    }
}

if(!function_exists('format_datetime'))
{
    function format_datetime(Carbon|String $time)
    {
        return format_date($time) .' '. format_time($time);
    }
}

if(!function_exists('timestamp_to_date_input'))
{
    function timestamp_to_date_input(Carbon|String|null $date)
    {
        if($date == null || empty($date)) { return ''; }
        if(is_string($date)) { Carbon::parse($date); }
        return $date->format('Y-m-d');
    }
}

if(!function_exists('format_money'))
{
    function format_money($quantity, ?Currency $currency = null)
    {
        //Add System Default
        if($currency == null) { $currency = Currency::first(); }
        $format = number_format($quantity, 2);
        if($currency != null) { $format .= ' '.$currency->code; }
        return $format;
    }
}

if(!function_exists('multi_tenancy_enabled'))
{
    function multi_tenancy_enabled()
    { return config('cascade.multi_tenancy', false); }
}