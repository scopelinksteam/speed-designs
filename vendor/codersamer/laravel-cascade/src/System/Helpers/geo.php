<?php

use Cascade\System\Entities\City;
use Cascade\System\Entities\Country;

if(!function_exists('get_countries'))
{ function get_countries() { return Country::all(); } }

if(!function_exists('get_cities'))
{ function get_cities() { return City::all(); } }
