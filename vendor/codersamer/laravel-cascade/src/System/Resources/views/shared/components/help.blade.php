@if($help->mode()->name == 'Tooltip')
<button data-bs-toggle="popover" data-bs-title="{{$help->title()}}" data-bs-content="{{$help->description()}}" class="text-muted btn p-0 border-0 me-2">
    <i class="fa fa-circle-question"></i>
</button>
@endif
@if($help->mode()->name == 'Link')
<a href="{{$help->target()}}" class="text-muted text-decoration-none me-2">
    <i class="fa fa-circle-question"></i>
</a>
@endif
@if($help->mode()->name == 'Scroll')
<a href="{{$help->target()}}" class="text-muted text-decoration-none me-2">
    <i class="fa fa-circle-question"></i>
</a>
@endif
@if($help->mode()->name == 'Modal')
<a href="#" data-bs-toggle="modal" data-bs-target="#help-modal-{{$help->id()}}" class="text-muted text-decoration-none me-2">
    <i class="fa fa-circle-question"></i>
</a>
<div class="modal fade help-modal" id="help-modal-{{$help->id()}}">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h1 class="modal-title fs-5" id="exampleModalLabel">{{$help->title()}}</h1>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <p>
                    {!! $help->description() !!}
                </p>
            </div>
        </div>
    </div>
</div>
@endif
@if($help->mode()->name == 'Youtube')
<a href="#" data-bs-toggle="modal" data-bs-target="#help-modal-{{$help->id()}}" class="text-muted text-decoration-none me-2">
    <i class="fa fa-circle-question"></i>
</a>
<div class="modal fade help-modal" id="help-modal-{{$help->id()}}">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h1 class="modal-title fs-5" id="exampleModalLabel">{{$help->title()}}</h1>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body p-0">
                <iframe class="embed-responsive-item" width="560" height="315" src="{{$help->target()}}" allowfullscreen></iframe>
            </div>
        </div>
    </div>
</div>
@endif