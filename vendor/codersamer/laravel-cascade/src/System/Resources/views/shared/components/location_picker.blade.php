{!! $renderScript() !!}
<style> #map-{{$index}} { width:100%; height : {{$height}}; } </style>
<div id="map-{{$index}}" data-map-index="{{$index}}"></div>
<input type="hidden" name="{{$name}}[latitude]" id="map-lat-input-{{$index}}">
<input type="hidden" name="{{$name}}[longitude]" id="map-lng-input-{{$index}}">
<script type="text/javascript">
document.addEventListener('DOMContentLoaded', function(){
    var mapOptions = {
    }
    var locationPicker{{$index}} = new locationPicker('map-{{$index}}', {
        setCurrentPosition: @if($latitude > 0 || $longitude > 0) false @else true @endif,
    },  { 
            zoom: {{$zoom < 1 ? 15 : $zoom }},
            @if($latitude > 0 || $longitude > 0)
            center: new google.maps.LatLng({{$latitude}}, {{$longitude}}),
            @endif
        } 
    );
    var latInput = document.getElementById('map-lat-input-{{$index}}');
    var lngInput = document.getElementById('map-lng-input-{{$index}}');
    google.maps.event.addListener(locationPicker{{$index}}.map, 'idle', function (event) {
        // Get current location and show it in HTML
        var location = locationPicker{{$index}}.getMarkerPosition();
        latInput.value = location.lat;
        lngInput.value = location.lng;
    });
});
</script>
