<!doctype html>
<html>
  <head>
    <meta name="viewport" content="width=device-width" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  </head>
  <body style="">
    <table role="presentation" border="0" cellpadding="0" cellspacing="0" style="max-width:600px;background-color:#edf2f7; min-width:100%;font-family:Arial;color:#">
      <tr>
        <td style="padding:20px;text-align:center">
          <h2>{{get_setting('system::general.app_name', config('app.name'))}}</h1>
            {!! $header ?? '' !!}
        </td>
      </tr>
      <tr>
        <td style="padding:0 20px 20px 20px;">
          <div style="border-radius:5px;background-color:#ffffff; padding:20px; color:#8491A4;line-height:1.5rem;">
            {!! $slot ?? '' !!}
          </div>
        </td>
      </tr>
      <tr>
        <td style="padding:0 20px 20px 20px;">
          <div style="background-color:#ffffff; padding:20px; color:#8491A4; text-align:center;">
            {!! $footer ?? '' !!}
            <small>© {{date('Y')}} {{get_setting('system::general.app_name', config('app.name'))}}. @lang('All rights reserved').</small>
          </div>
        </td>
      </tr>
    </table>
  </body>
</html>
