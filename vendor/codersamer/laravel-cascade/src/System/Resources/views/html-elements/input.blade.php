<input
    type="{{ $element->type()->value }}"
    class="{{ $element->class() }}"
    id="{{ $element->id() }}"
    name="{{ $element->name() }}"
    value="{{ $element->value() != '' ? $element->value() : ($element->type()->value == 'checkbox' ? '1' : '') }}"
    @if($element->checked()) checked="true" @endif
>
 {!! $element->renderChildren() !!}
