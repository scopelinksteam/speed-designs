<div class="col{{ $element->size() > 0 ? '-'.$element->size() : '' }} {{ $element->class() }}">
    {!! $element->renderChildren() !!}
</div>
