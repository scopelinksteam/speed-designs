<div class="row {{ $element->class() }}">
    {!! $element->renderChildren() !!}
</div>
