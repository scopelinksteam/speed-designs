<div class="{{ $element->class() }}" id="{{ $element->id() }}">
    {!! $element->renderChildren() !!}
</div>
