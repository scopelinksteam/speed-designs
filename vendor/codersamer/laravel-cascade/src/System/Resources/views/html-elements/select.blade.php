<select class="{{ $element->class() }}" id="{{ $element->id() }}" name="{{ $element->name() }}">
    @foreach ($element->options() as $value => $text)
    <option value="{{$value}}" @selected($value == $element->value())>{{$text}}</option>
    @endforeach
</select>
