<textarea class="{{ $element->class() }}" cols="{{ $element->cols() }}" rows="{{ $element->rows() }}" id="{{ $element->id() }}" name="{{ $element->name() }}">{!! $element->value() !!}</textarea>
