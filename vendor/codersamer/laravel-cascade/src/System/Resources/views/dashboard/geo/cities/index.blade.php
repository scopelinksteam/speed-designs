@extends('admin::layouts.master')
@section('page-title', __('Geo : Cities Management'))
@section('page-description', __('Manage Cities Entries within your system'))
@section('page-actions')
<a href="{{route('dashboard.system.geo.countries.index')}}" class="btn btn-dark">@lang('Manage Countries')</a>
<a href="{{route('dashboard.system.geo.areas.index')}}" class="btn btn-dark">@lang('Manage Areas')</a>
@endsection
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-4">
            <div class="card">
                <form action="{{route('dashboard.system.geo.cities.store')}}" method="POST" enctype="multipart/form-data">
                    <div class="card-header">
                        <div class="card-title">
                            <i class="fa fa-info"></i>
                            @lang('City Details')
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            <label for="name">@lang('Name')</label>
                            <input type="text" name="name" id="name" class="form-control" value="{{$entity->name ?? ''}}" required>
                        </div>
                        <div class="form-group">
                            <label for="country">@lang('Country')</label>
                            <select name="country_id" id="country" class="form-control selectr">
                                @foreach ($countries as $country)
                                <option value="{{$country->id}}" @selected(isset($entity) && $entity->country_id == $country->id)>{{$country->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="thumbnail">@lang('Image')</label>
                            <input type="file" name="thumbnail" id="thumbnail" class="dropify-image">
                        </div>
                    </div>
                    <div class="card-footer">
                        @csrf
                        @isset($entity)
                        <input type="hidden" name="entity_id" value="{{$entity->id}}">
                        @endisset
                        <input type="submit" value="@lang('Save City')" class="btn btn-primary" />
                    </div>
                </form>
            </div>
        </div>
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <div class="card-title">
                        <i class="fa fa-list"></i>
                        @lang('Cities List')
                    </div>
                </div>
                <div class="card-body">
                    @livewire('datatable', ['table' => $table])
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
