<div class="row mt-4">
    @foreach ($menu->GetRenderItems() as $section)
    @foreach ($section->GetItems() as $item)

    <div class="col-lg-4 col-md-6 col-6">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-auto">
                        <div class="icon">
                            <i class="display-5 fas fa-{{ $item->GetIcon() }}"></i>
                          </div>
                    </div>
                    <div class="col">
                        <h5 class="card-title">{{ $item->GetTitle() }}</h5>
                        <p>{{ $item->GetDescription() }}</p>
                    </div>
                </div>
            </div>
            <div class="card-footer bg-light">
                <a href="{{ $item->GetLink() }}" class="btn btn-dark">
                    @lang('View Section') <i class="fas fa-arrow-circle-right ms-3"></i>
                </a>
            </div>
        </div>
    </div>
    @endforeach
    @endforeach
</div>
