@extends('admin::layouts.master')
@section('page-title', __('Categories'))
@section('page-description', __('Manage and Overview Categories'))
@section('content')
<div class="row">
    <div class="col-4">
        <div class="card">
            <form action="{{ $routes('store') }}" method="POST" enctype="multipart/form-data">
                <div class="card-header">
                    <div class="card-title">
                        <i class="fa fa-info mr-2"></i>
                        @lang('Category Details')
                    </div>
                </div>
                <div class="card-body">
                    @foreach (get_active_languages() as $language)
                    <div class="form-group">
                        <label for="name-{{ $language->locale }}">@lang('Name') ( {{ $language->name }} )</label>
                        <input type="text" name="name[{{ $language->locale }}]" id="name-{{ $language->locale }}" value="@isset($category) {{ $category->translate('name', $language->locale) }} @endisset" class="form-control">
                    </div>
                    @endforeach
                    <div class="form-group">
                        <label for="slug">@lang('Slug')</label>
                        <input type="text" name="slug" id="slug" class="form-control" value="@isset($category){{ $category->slug }}@endisset">
                    </div>
                    <div class="form-group">
                        <label for="parent_id">@lang('Parent')</label>
                        <select name="parent_id" id="parent_id" class="selectr">
                            <option value="0">@lang('None')</option>
                            @foreach ($categories as $singleCategory)
                                @if(isset($category) && $singleCategory->id == $category->id) @continue @endif
                                <option value="{{$singleCategory->id}}" @if(isset($category) && $category->parent_id == $singleCategory->id) selected @endif>{{$singleCategory->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="thumbnail">@lang('Thumbnail')</label>
                        <input type="file" name="thumbnail" id="thumbnail" class="dropify-image" @if(isset($category) && $category->thumbnail_file) data-default-file="{{uploads_url($category->thumbnail_file)}}" @endif>
                    </div>
                </div>
                <div class="card-footer">
                    @csrf
                    @isset($category)
                    <input type="hidden" name="entity_id" value="{{ $category->id }}">
                    @endisset
                    <input type="hidden" name="type" value="{{ $type }}">
                    <input type="submit" value="@lang('Save Category')" class="btn btn-primary">
                </div>
            </form>
        </div>
    </div>
    <div class="col-8">
        <div class="card">
            <div class="card-header">
                <div class="card-title">
                    <i class="fa fa-folder-open mr-2"></i>
                    @lang('Categories')
                </div>
            </div>
            <div class="card-body">
                @livewire('datatable', ['table' => $table, 'params' => ['type' => $type, 'routes' => $routes]])
            </div>
        </div>
    </div>
</div>
@endsection
