@extends('admin::layouts.master')
@section('page-title', __('Statuses'))
@section('page-description', __('Manage and Overview Statuses'))
@section('content')
<div class="row">
    <div class="col-4">
        <div class="card">
            <form action="{{ $routes('store') }}" method="POST" enctype="multipart/form-data">
                <div class="card-header">
                    <div class="card-title">
                        <i class="fa fa-info mr-2"></i>
                        @lang('Status Details')
                    </div>
                </div>
                <div class="card-body">
                    @foreach (get_active_languages() as $language)
                    <div class="form-group">
                        <label for="name-{{ $language->locale }}">@lang('Name') ( {{ $language->name }} )</label>
                        <input type="text" name="name[{{ $language->locale }}]" id="name-{{ $language->locale }}" value="@isset($status) {{ $status->translate('name', $language->locale) }} @endisset" class="form-control">
                    </div>
                    @endforeach
                    <div class="form-group">
                        <label for="color">@lang('Color')</label>
                        <input type="color" name="color" id="color" class="form-control" value="@isset($status){{ $status->color }}@endisset">
                    </div>
                </div>
                <div class="card-footer">
                    @csrf
                    @isset($status)
                    <input type="hidden" name="entity_id" value="{{ $status->id }}">
                    @endisset
                    <input type="hidden" name="type" value="{{ $type }}">
                    <input type="submit" value="@lang('Save Status')" class="btn btn-primary">
                </div>
            </form>
        </div>
    </div>
    <div class="col-8">
        <div class="card">
            <div class="card-header">
                <div class="card-title">
                    <i class="fa fa-folder-open mr-2"></i>
                    @lang('Statuses')
                </div>
            </div>
            <div class="card-body">
                @livewire('datatable', ['table' => $table, 'params' => ['type' => $type, 'routes' => $routes]])
            </div>
        </div>
    </div>
</div>
@endsection
