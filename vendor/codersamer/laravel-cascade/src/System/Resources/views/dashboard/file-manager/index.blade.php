@extends('admin::layouts.master')
@section('page-title', __('Files Manager'))
@section('page-description', __('Browse and Manage Files Uploaded to your System'))
@section('content')
<div class="row">
    <div class="col-12" id="file-manager-container">
        <div id="fm"></div>
    </div>
</div>
@endsection
@push('styles')
<link href="{{ asset('vendor/file-manager/css/file-manager.css') }}" rel="stylesheet">
@endpush
@push('scripts')
<script src="{{ asset('vendor/file-manager/js/file-manager.js') }}"></script>
<script>
  document.addEventListener('DOMContentLoaded', function() {
    document.getElementById('file-manager-container').setAttribute('style', 'height:' + 700 + 'px');
    document.getElementById('file-manager-container').classList.add('mb-4');
    fm.$store.commit('fm/setFileCallBack', function(fileUrl) {
      window.opener.fmSetLink(fileUrl);
      window.close();
    });
  });
</script>
@endpush
