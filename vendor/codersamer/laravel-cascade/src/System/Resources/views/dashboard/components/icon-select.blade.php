<div class="icon-select" id="{{$id}}">
    <button type="button" class="btn btn-lg btn-dark icon-select-toggle d-flex align-items-center {{$class ?? ''}}" id="{{$id}}-toggle">
        <span class="icon-display me-2"><i class="{{$value}}"></i></span>
        <span>@lang('Select Icon')</span>
    </button>
    <input name="{{$name}}" type="hidden" value="{{$value}}" class="icon-select-value d-none">
</div>

