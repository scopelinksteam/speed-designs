@extends('admin::layouts.master')
@section('page-title', isset($template) ? __('Edit Template') .' : '.$template->name : __('Create Template'))
@section('page-description', $composer->description())
@section('content')
<form action="{{ $routes('store') }}" method="POST">
    <div class="row">
        <div class="col-8">
            <div class="card">
                <div class="card-header">
                    <div class="card-title">
                        <i class="fa fa-info mr-2"></i>
                        @lang('Template Content')
                    </div>
                </div>
                <div class="card-body">
                    <div class="form-group">
                        <label for="title">@lang('Name')</label>
                        <input type="text" name="name" id="name" value="{{ isset($template) ? $template->name : old('name') }}" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="title">@lang('Title / Subject')</label>
                        <input type="text" name="title" id="title" value="{{ isset($template) ? $template->title : old('title') }}" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="content">@lang('Body Content')</label>
                        <textarea name="content" id="content" class="editor">{!! isset($template) ? $template->content : old('content') !!}</textarea>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-4">
            <div class="card">
                <div class="card-header">
                    <div class="card-title">
                        <i class="fa fa-life-ring mr-2"></i>
                        @lang('Available Placeholders')
                    </div>
                </div>
                <div class="card-body">
                    @foreach ($composer->placeholders() as $placeholder)
                    <div class="form-group">
                        <label for="placeholder-{{ $placeholder->callable_key }}">{{ $placeholder->name }}</label>
                        <div class="input-group mb-3">
                            <input type="text" name="" id="placeholder-{{ $placeholder->callable_key }}" value="{{ $placeholder->callable_key }}" class="form-control" readonly>
                            <div class="input-group-append">
                              <button class="btn btn-outline-secondary" onclick="addShortCode('content', '{{ $placeholder->callable_key }}')" type="button">Button</button>
                            </div>
                        </div>

                    </div>
                    @endforeach
                </div>
                <div class="card-footer">
                    @csrf
                    @isset($template)
                    <input type="hidden" name="entity_id" value="{{ $template->id }}">
                    @endisset
                    <button type="submit" class="btn btn-primary">@lang('Save Template')</button>
                </div>
            </div>
        </div>
    </div>
</form>
@endsection
@push('scripts')
<script type="text/javascript">
    function addShortCode(editorId, shortCode)
    {
        tinymce.get(editorId).execCommand('mceInsertContent', false, shortCode);
    }
</script>
@endpush
