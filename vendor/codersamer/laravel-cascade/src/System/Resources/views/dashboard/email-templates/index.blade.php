@extends('admin::layouts.master')
@section('page-title', $composer->title())
@section('page-description', $composer->description())
@section('page-actions')
@if($composer->creatable() && can('create email templates'))
<a href="{{ $routes('create') }}" class="btn btn-primary">@lang('Create Template')</a>
@endif
@endsection
@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <div class="card-title">
                    <i class="fa fa-email mr-2"></i>
                    @lang('Email Templates')
                </div>
            </div>
            <div class="card-body">
                @livewire('datatable', ['table' => $table, 'params' => ['routes' => $routes, 'type' => $composer->type()]])
            </div>
        </div>
    </div>
</div>
@endsection
