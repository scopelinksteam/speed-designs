@extends('admin::layouts.master')
@section('page-title', __('Taxes Management'))
@section('page-description', __('Manage, Add and Edit Taxes in your System'))
@section('content')
<div class="row">
    <div class="col-md-4">
        <div class="card">
            <form action="{{route('dashboard.system.taxes.store')}}" method="POST">
                <div class="card-header">
                    <div class="card-title">
                        <i class="fa fa-info mr-2"></i>
                        @lang('Tax Details')
                    </div>
                </div>
                <div class="card-body">
                    <div class="form-group">
                        <label for="name">@lang('Name')</label>
                        <input type="text" name="name" id="name" class="form-control" @isset($entity) value="{{$entity->name}}" @endisset required>
                    </div>
                    <div class="form-group">
                        <label for="rate">@lang('Rate')</label>
                        <input type="number" name="rate" id="rate" class="form-control" step="0.01" @isset($entity) value="{{$entity->rate}}" @endisset>
                    </div>
                    <div class="form-group">
                        <label for="rate_type">@lang('Rate Type')</label>
                        <select name="rate_type" id="rate_type" class="selectr">
                            <option value="0" @selected(isset($entity) && $entity->rate_type->value == 0)>@lang('None')</option>
                            <option value="1" @selected(isset($entity) && $entity->rate_type->value == 1)>@lang('Percentage')</option>
                            <option value="2" @selected(isset($entity) && $entity->rate_type->value == 2)>@lang('Fixed')</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="code">@lang('Code')</label>
                        <input type="text" name="code" id="code" class="form-control" @isset($entity) value="{{$entity->code}}" @endisset>
                    </div>
                    <div class="form-group">
                        <label for="parent_id">@lang('Parent Tax')</label>
                        <select name="parent_id" id="parent_id" class="selectr">
                            <option value="0" @selected(isset($entity) && $entity->parent_id == 0)>@lang('None')</option>
                            @foreach($taxes as $tax)
                            @if(isset($entity) && $tax->id == $entity->id) @continue @endif
                            <option value="{{$tax->id}}" @selected(isset($entity) && $entity->parent_id == $tax->id)>{{$tax->name}} @if(!empty($tax->code)) [{{$tax->code}}] @endif</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="card-footer">
                    @csrf
                    @isset($entity)
                    <input type="hidden" name="entity_id" value="{{$entity->id}}">
                    @endisset
                    <input type="submit" value="@lang('Save Tax')" class="btn btn-primary">
                </div>
            </form>
        </div>
    </div>
    <div class="col-md-8">
        <div class="card">
            <div class="card-header">
                <div class="card-title">
                    <i class="fa fa-tag mr-2"></i>
                    @lang('Saved Taxes')
                </div>
            </div>
            <div class="card-body">
                @livewire('datatable', ['table' => $table])
            </div>
        </div>
    </div>
</div>
@endsection
