@extends('admin::layouts.master')
@section('page-title', __('Geo : Areas Management'))
@section('page-description', __('Manage Areas Entries within your system'))
@section('page-actions')
<a href="{{route('dashboard.system.geo.cities.index')}}" class="btn btn-dark">@lang('Manage Cities')</a>
<a href="{{route('dashboard.system.geo.countries.index')}}" class="btn btn-dark">@lang('Manage Countries')</a>
@endsection
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-4">
            <div class="card">
                <form action="{{route('dashboard.system.geo.areas.store')}}" method="POST" enctype="multipart/form-data">
                    <div class="card-header">
                        <div class="card-title">
                            <i class="fa fa-info"></i>
                            @lang('Area Details')
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            <label for="name">@lang('Name')</label>
                            <input type="text" name="name" id="name" class="form-control" value="{{$entity->name ?? ''}}" required>
                        </div>
                        <div class="form-group">
                            <label for="city">@lang('City')</label>
                            <select name="city_id" id="city" class="form-control selectr">
                                @foreach ($cities as $city)
                                <option value="{{$city->id}}" @selected(isset($entity) && $entity->city_id == $city->id)>{{(($city->country) ? $city->country->name.' - ' : '').$city->name}} </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="thumbnail">@lang('Image')</label>
                            <input type="file" name="thumbnail" id="thumbnail" class="dropify-image">
                        </div>
                    </div>
                    <div class="card-footer">
                        @csrf
                        @isset($entity)
                        <input type="hidden" name="entity_id" value="{{$entity->id}}">
                        @endisset
                        <input type="submit" value="@lang('Save Area')" class="btn btn-primary" />
                    </div>
                </form>
            </div>
        </div>
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <div class="card-title">
                        <i class="fa fa-list"></i>
                        @lang('Areas List')
                    </div>
                </div>
                <div class="card-body">
                    @livewire('datatable', ['table' => $table])
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
