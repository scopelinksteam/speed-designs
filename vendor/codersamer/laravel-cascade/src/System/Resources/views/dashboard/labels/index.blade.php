@extends('admin::layouts.master')
@section('page-title', $type->title())
@section('page-description', $type->description())
@section('content')
<div class="row">
    <div class="col-4">
        @stack('card::entity.before')
        <div class="card" id="entity">
            <form action="{{ $routes('store') }}" method="POST" enctype="multipart/form-data">
                <div class="card-header">
                    <div class="card-title">
                        <i class="fa fa-info mr-2"></i>
                        @lang('Record Details')
                    </div>
                </div>
                <div class="card-body">
                    @stack('card::entity.start')
                    @if($type->supports('name'))
                    @foreach (get_active_languages() as $language)
                    <div class="form-group">
                        <label for="name-{{ $language->locale }}">@lang('Name') ( {{ $language->name }} )</label>
                        <input type="text" name="name[{{ $language->locale }}]" id="name-{{ $language->locale }}" value="@isset($label) {{ $label->translate('name', $language->locale) }} @endisset" class="form-control">
                    </div>
                    @endforeach
                    @endif
                    @if($type->supports('color'))
                    <div class="form-group">
                        <label for="color">@lang('Color')</label>
                        <input type="color" name="color" id="color" class="form-control" value="@isset($label){{ $label->color }}@endisset">
                    </div>
                    @endif
                    @stack('card::entity.end')
                </div>
                <div class="card-footer">
                    @csrf
                    @isset($label)
                    <input type="hidden" name="entity_id" value="{{ $label->id }}">
                    @endisset
                    <input type="hidden" name="type" value="{{ $type->type() }}">
                    <input type="submit" value="@lang('Save Record')" class="btn btn-primary">
                </div>
            </form>
        </div>
        @stack('card::entity.after')
    </div>
    <div class="col-8">
        <div class="card">
            <div class="card-header">
                <div class="card-title">
                    <i class="fa fa-folder-open mr-2"></i>
                    @lang('Records List')
                </div>
            </div>
            <div class="card-body">
                @livewire('datatable', ['table' => $table, 'params' => ['type' => $type->type(), 'routes' => $routes]])
            </div>
        </div>
    </div>
</div>
@endsection
