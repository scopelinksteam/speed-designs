@extends('admin::layouts.clean')
@section('content')
<div class="row mt-4">
    <div class="col-12 mb-4">
        <h2><center>System Overview</center></h2>
        <p><center>Review System Configurations and Status and Customize System Workflow</center></p>
    </div>
</div>
{{--
<div class="row mt-4">

    <div class="col-4">
        <div class="card">
            <div class="card-header">
                <div class="card-title">
                    <i class="fa fa-heart mr-2"></i>
                    @lang('System Monitor')
                </div>
            </div>
            <div class="card-body p-0">
                <div class="p-3">
                    {{get_setting('system::general.app_name')}}
                    @if(current_tenant() != null)
                    <br />
                    @foreach (current_tenant()->domains as $domain)
                    <small class="badge bg-primary">{{$domain->domain}}</small>
                    @endforeach
                    @endif
                </div>
                <table class="table table-striped">
                    <tr>
                        <th>@lang('Storage Limit')</th>
                        <td>{{current_tenant()->GetOption('storage_limit', 1024) / 1024}} GB</td>
                    </tr>
                    <tr>
                        <th>@lang('Email Accounts')</th>
                        <td>{{current_tenant()->GetOption('email_accounts', 1)}} @lang('Account')</td>
                    </tr>
                    <tr>
                        <th>Available Storage</th>
                        <td>120 GB</td>
                    </tr>
                    <tr>
                        <th>Available Storage</th>
                        <td>120 GB</td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <div class="col-4">
        <div class="card">
            <div class="card-header">
                <div class="card-title">
                    <i class="fa fa-heart mr-2"></i>
                    @lang('Package Information')
                </div>
            </div>
            <div class="card-body p-0">
                <table class="table table-striped">
                    <tr>
                        <th>Laravel Version</th>
                        <td>{{ app()->version() }}</td>
                        <td><strong class="text-success">Passed</strong></td>
                    </tr>
                    <tr>
                        <th>PHP Version</th>
                        <td>{{ phpversion() }}</td>
                        <td><strong class="text-success">Passed</strong></td>
                    </tr>

                    <tr>
                        <th>Laravel Version</th>
                        <td>9.0</td>
                        <td><strong class="text-success">Passed</strong></td>
                    </tr>
                    <tr>
                        <th>MySQL Version</th>
                        <td>8.5</td>
                        <td><strong class="text-success">Passed</strong></td>
                    </tr>
                    <tr>
                        <th>PHP Version</th>
                        <td>8.1</td>
                        <td><strong class="text-success">Passed</strong></td>
                    </tr>
                </table>
            </div>
        </div>
    </div>

    <div class="col-4">
        <div class="card">
            <div class="card-header">
                <div class="card-title">
                    <i class="fa fa-heart mr-2"></i>
                    @lang('System Requirements')
                </div>
            </div>
            <div class="card-body p-0">
                <table class="table table-striped">
                    <tr>
                        <th>PHP Version</th>
                        <td>8.1</td>
                        <td><strong class="text-success">Passed</strong></td>
                    </tr>
                    <tr>
                        <th>IO Permissions</th>
                        <td></td>
                        <td><strong class="text-success">Passed</strong></td>
                    </tr>
                    <tr>
                        <th>Laravel Version</th>
                        <td>{{app()->version()}}</td>
                        <td><strong class="text-success">Passed</strong></td>
                    </tr>
                    <tr>
                        <th>MySQL Version</th>
                        <td>{{\current(DB::select("SELECT VERSION() as mysql_version"))->mysql_version}}</td>
                        <td><strong class="text-success">Passed</strong></td>
                    </tr>
                    <tr>
                        <th>PHP Version</th>
                        <td>{{phpversion()}}</td>
                        <td><strong class="text-success">Passed</strong></td>
                    </tr>
                </table>
            </div>
        </div>
    </div>

</div>
<hr>
--}}
<x-dashboard-menu name="system-overview" />

@endsection
