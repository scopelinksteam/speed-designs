@extends('admin::layouts.master')
@section('page-title', __('Geo : Countries Management'))
@section('page-description', __('Manage Countries Entries within your system'))
@section('page-actions')
<a href="{{route('dashboard.system.geo.cities.index')}}" class="btn btn-dark">@lang('Manage Cities')</a>
<a href="{{route('dashboard.system.geo.areas.index')}}" class="btn btn-dark">@lang('Manage Areas')</a>
@endsection
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-4">
            <div class="card">
                <form action="{{route('dashboard.system.geo.countries.store')}}" method="POST" enctype="multipart/form-data">
                    <div class="card-header">
                        <div class="card-title">
                            <i class="fa fa-info"></i>
                            @lang('Country Details')
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            <label for="name">@lang('Name')</label>
                            <input type="text" name="name" id="name" class="form-control" value="{{$entity->name ?? ''}}" required>
                        </div>
                        <div class="form-group">
                            <label for="code">@lang('Code')</label>
                            <input type="text" name="code" id="code" class="form-control" value="{{$entity->code ?? ''}}" required>
                        </div>
                        <div class="form-group">
                            <label for="code">@lang('Phone Code')</label>
                            <div class="input-group">
                                <span class="input-group-text">+</span>
                                <input type="number" name="phone_code" id="phone_code" class="form-control" value="{{$entity->phone_code ?? ''}}" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="thumbnail">@lang('Image')</label>
                            <input type="file" name="thumbnail" id="thumbnail" class="dropify-image">
                        </div>
                    </div>
                    <div class="card-footer">
                        @csrf
                        @isset($entity)
                        <input type="hidden" name="entity_id" value="{{$entity->id}}">
                        @endisset
                        <input type="submit" value="@lang('Save Country')" class="btn btn-primary" />
                    </div>
                </form>
            </div>
        </div>
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <div class="card-title">
                        <i class="fa fa-list"></i>
                        @lang('Countries List')
                    </div>
                </div>
                <div class="card-body">
                    @livewire('datatable', ['table' => $table])
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
