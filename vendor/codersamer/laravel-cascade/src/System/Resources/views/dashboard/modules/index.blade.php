@extends('admin::layouts.master')
@section('page-title', __('Modules Management'))
@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <div class="card-title">
                    <i class="fa fa-cubes mr-2"></i>
                    @lang('Installed Modules')
                </div>
            </div>
            <div class="card-body">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>@lang('Name')</th>
                            <th>@lang('Screenshot')</th>
                            <th>@lang('Description')</th>
                            <th>@lang('Actions')</th>
                        </tr>
                    </thead>
                    @foreach (get_modules(true) as $module)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $module->GetName() }}</td>
                        <td>
                            <span class="h1"><i class="fas fa-{{ $module->GetIcon() }}"></i></span>
                        </td>
                        <td>
                            <p>{{ $module->GetDescription() }}</p>
                            <table class="table table-striped mb-0">
                                <tr>
                                    <th>@lang('Name')</th>
                                    <td class="bg-light">{{ $module->GetName() }}</td>
                                    <th>@lang('Author')</th>
                                    <td class="bg-light">{{ $module->GetAuthor() }}</td>
                                    <th>@lang('Status')</th>
                                    <td class="bg-light">{{ $module->IsEnabled() ? __('Enabled') : __('Disabled') }}</td>
                                </tr>
                            </table>
                        </td>
                        <td>
                            @if($module->IsEnabled())
                            <a href="{{ route('dashboard.system.modules.disable', ['module' => $module->GetName()]) }}" class="btn btn-dark">@lang('Disable')</a>
                            @else
                            <a href="{{ route('dashboard.system.modules.enable', ['module' => $module->GetName()]) }}" class="btn btn-success">@lang('Enable')</a>
                            @endif
                            <a href="#" class="btn btn-danger">@lang('Delete')</a>
                        </td>
                    </tr>
                    @endforeach
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
