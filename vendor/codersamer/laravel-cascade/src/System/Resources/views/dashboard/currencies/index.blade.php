@extends('admin::layouts.master')
@section('page-title', __('Currencies'))
@section('page-description', __('View and Manage System Available Currencies'))
@section('content')
<div class="row">
    <div class="col-4">
        <div class="card">
            <form action="{{ route('dashboard.system.currencies.store') }}" method="POST">
                <div class="card-header">
                    <div class="card-title">
                        <i class="fa fa-info mr-2"></i>
                        @lang('Currency Details')
                    </div>
                </div>
                <div class="card-body">
                    @stack('before.form')
                    @foreach (get_active_languages() as $language)
                    <div class="form-group">
                        <label for="name-{{ $language->locale }}">@lang('Name') ( {{ $language->name }} )</label>
                        <input type="text" name="name[{{ $language->locale }}]" id="name-{{ $language->locale }}" class="form-control" value="{{ isset($currency) ? $currency->translate('name', $language->locale) : '' }}" />
                    </div>
                    @endforeach
                    <div class="form-group">
                        <label for="code">@lang('Code')</label>
                        <input type="text" name="code" id="code" class="form-control" value="{{ isset($currency) ? $currency->code : '' }}">
                    </div>
                    <div class="form-group">
                        <label for="symbol">@lang('Symbol')</label>
                        <input type="text" name="symbol" id="symbo" class="form-control" value="{{ isset($currency) ? $currency->symbol : '' }}">
                    </div>
                    <div class="form-group">
                        <label for="rate">@lang('Exchange Rate')</label>
                        <input type="number" name="rate" id="rate" step="0.01" class="form-control" value="{{ isset($currency) ? $currency->rate : '1' }}">
                    </div>
                    @stack('after.form')
                </div>
                <div class="card-footer">
                    @csrf
                    @isset($currency)
                    <input type="hidden" name="entity_id" value="{{ $currency->id }}">
                    @endisset
                    <input type="submit" value="@lang('Save Currency')" class="btn btn-primary">
                </div>
            </form>
        </div>
    </div>
    <div class="col-8">
        <div class="card">
            <div class="card-header">
                <div class="card-title">
                    <i class="fa fa-dollar-sign mr-2"></i>
                    @lang('Currencies')
                </div>
            </div>
            <div class="card-body">
                @livewire('datatable', ['table' => $table])
            </div>
        </div>
    </div>
</div>
@endsection
