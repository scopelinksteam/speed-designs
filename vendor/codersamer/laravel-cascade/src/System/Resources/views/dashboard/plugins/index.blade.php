@extends('admin::layouts.master')
@section('page-title', __('Plugins Management'))
@section('page-description', __('Manage, Review, Create and Install Plugins'))
@section('page-actions')
<a href="#new-plugin" data-toggle="collapse" class="btn btn-success">@lang('New Plugin')</a>
@endsection
@section('content')
<div class="row collapse" id="new-plugin">
    <div class="col-4">
        <form action="{{ route('dashboard.system.plugins.create') }}" method="POST">
            @csrf
            <div class="card">
                <div class="card-header">@lang('Create Plugin')</div>
                <div class="card-body">
                    <div class="form-group">
                        <label for="name">@lang('Plugin Name')</label>
                        <input type="text" name="name" id="name" class="form-control">
                    </div>
                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">@lang('Create Plugin')</button>
                </div>
            </div>
        </form>
    </div>
    <div class="col-8">
        <form action="{{ route('dashboard.system.plugins.install') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="card">
                <div class="card-header">@lang('Install New Plugin')</div>
                <div class="card-body">
                    <input type="file" name="plugin" id="plugin-file" class="dropify-archive">
                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">@lang('Install Plugin')</button>
                </div>
            </div>
        </form>
    </div>

    <div class="col-12">
        <hr>
        <h3>@lang('Installed Plugins')</h3>
    </div>
</div>
<div class="row">
    @foreach ($plugins as $plugin)
        <div class="col-md-3 col-sm-6 col-12">
            <div class="card">
                <div class="card-header">
                    <span>{{ $plugin->GetName() }}</span>
                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                            <i class="fas fa-minus"></i>
                        </button>
                    </div>
                </div>
                <div class="card-body p-0">
                    <img src="{{ $plugin->GetScreenshotURL() }}" style="width: 100%" />
                    @if($plugin->GetDescription() != '')
                    <p class="p-2">{{ $plugin->GetDescription() }}</p>
                    @endif
                </div>
                <div>
                    <table class="table table-striped mb-0">
                        <tr>
                            <th>@lang('Name')</th>
                            <td><a href="{{ $plugin->GetURL() }}">{{ $plugin->GetName() }}</a></td>
                        </tr>
                        <tr>
                            <th>@lang('Author')</th>
                            <td><a href="{{ $plugin->GetAuthorURL() }}">{{ $plugin->GetAuthor() }}</a></td>
                        </tr>
                        <tr>
                            <th>@lang('Status')</th>
                            <td>{{ $plugin->IsEnabled() ? __('Enabled') : __('Disabled') }}</td>
                        </tr>
                    </table>
                </div>
                @if(!$plugin->IsProtected())
                <div class="card-footer">
                    @if($plugin->IsEnabled())
                    <a href="{{ route('dashboard.system.plugins.disable', ['plugin' => $plugin->GetName()]) }}" class="btn btn-dark">@lang('Disable')</a>
                    @else
                    <a href="{{ route('dashboard.system.plugins.enable', ['plugin' => $plugin->GetName()]) }}" class="btn btn-success">@lang('Enable')</a>
                    @endif
                    @can('delete plugins')
                    <a href="{{ route('dashboard.system.plugins.delete', ['plugin' => $plugin->GetName()]) }}" class="btn btn-danger">@lang('Delete')</a>
                    @endcan
                </div>
                @endif
            </div>
        </div>
    @endforeach
</div>
@endsection
