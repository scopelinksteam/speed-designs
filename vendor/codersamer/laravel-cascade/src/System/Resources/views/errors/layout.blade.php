<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{cascade_asset('plugins/bootstrap/css/bootstrap.min.css')}}">
    <title>{{$code ?? ''}} | {{get_setting('system::general.app_name')}}</title>
    <style>
        #main {
            height: 100vh;
        }
    </style>
</head>
<body>
    <div class="d-flex justify-content-center align-items-center" id="main">
        <h1 class="me-3 pe-3 align-top border-end inline-block align-content-center">@yield('code')</h1>
        <div class="inline-block align-middle">
            <h2 class="font-weight-normal lead" id="desc">@yield('message')</h2>
        </div>
    </div>
</body>
</html>
