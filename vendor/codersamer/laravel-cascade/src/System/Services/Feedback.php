<?php

namespace Cascade\System\Services;

use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\Request;
use Illuminate\Validation\Validator;

class Feedback
{

    protected static $instance = null;

    protected static $messages = array();

    public function __construct()
    {

        self::$messages = array(
            'danger' => array(),
            'warning' => array(),
            'info'  => array(),
            'success' => array()
        );
        if (Request::session()->has('feedback')) {
            self::$messages = Request::session()->get('feedback');
        }
        if(Request::session()->get('errors') != null)
        {
            foreach(Request::session()->get('errors')->getMessages() as $error)
            {
                if(is_array($error))
                {
                    foreach($error as $message) { $this->addError($message); }
                }
                else { $this->addError($error); }
            }
        }

    }

    /**
     * Singleton Pattern Implementation
     *
     * @return Feedback
     */
    public static function getInstance(): Feedback
    {
        if (self::$instance == null) {
            self::$instance = new Feedback();
        }
        return self::$instance;
    }

    /**
     * Register an Generic Message
     *
     * @return void
     */
    public function addMessage($message)
    {
        self::$messages['info'][] = $message;
    }

    public static function message(String $message) : void
    {
        static::getInstance()->addMessage($message);
    }

    /**
     * Register Error Message
     *
     * @return void
     */
    public function addError($message)
    {
        self::$messages['danger'][] = $message;
    }

    public static function error(String $message) : void
    {
        static::getInstance()->addError($message);
    }

    /**
     * Register Error Message
     *
     * @return void
     */
    public function addAlert($message)
    {
        self::$messages['warning'][] = $message;
    }

    public static function alert(String $message) : void
    {
        static::getInstance()->addAlert($message);
    }

    /**
     * Register an Info Message
     *
     * @return void
     */
    public function addInfo($message)
    {
        self::$messages['info'][] = $message;
    }

    public static function info(String $message) : void
    {
        static::getInstance()->addInfo($message);
    }

    /**
     * Register Success Message
     *
     * @return void
     */
    public function addSuccess($message): void
    {
        self::$messages['success'][] = $message;
    }

    public static function success(String $message) : void
    {
        static::getInstance()->addSuccess($message);
    }

    /**
     * Release memory and forget Messages
     *
     * @return void
     */
    public static function flush(): void
    {
        Request::session()->forget('feedback');
    }

    /**
     * Flash Registered Messages to next HTTP Request
     *
     * @return void
     */
    public static function flash()
    {
        Request::session()->put('feedback', self::$messages);
    }

    public function withValidator(Validator $validator)
    {
        if ($validator->fails())
        {
            dd($validator->getMessageBag());
            foreach ($validator->getMessageBag() as $field_name => $messages) {
                foreach ($messages as $message) {
                    dd($messages);
                    $this->addError(__($message));
                }
            }
            self::flash();
            abort(redirect()->back());
        }
    }

    public static function hasErrors()
    {
        return isset(self::$messages['danger']) && count(self::$messages['danger']) > 0;
    }

    public static function GetToDisplay()
    {
        if (Request::session()->has('feedback')) {
            self::$messages = Request::session()->get('feedback');
        }
        $messages = self::$messages;
        self::flush();
        return $messages;
    }
    /**
     * Display Registered Messages
     *
     * @return void
     */
    public static function displayBoostrap()
    {
        if (Request::session()->has('feedback')) {
            self::$messages = Request::session()->get('feedback');
        }
        $markup = "";
        $element = '<div class="alert alert-%s" role="alert">%s</div>';
        foreach (self::$messages as $messageType => $messages) {
            foreach ($messages as $message) {
                $markup .= sprintf($element, $messageType, $message);
            }
        }
        return $markup;
        self::flush();
    }
    public static function display()
    {
        self::getInstance();
        $element = '<div class="alert alert-%s" role="alert">
        <!--<h4 class="alert-heading">%s!</h4>-->
        <span>%s</span>
        </div>';
        $markup = "";
        $interval = 4000;
        /*
        $element = '<div wire:ignore class="sl-toast toast-%s show" data-out="%s"><!-- Add Class show for display Toast -->
          <span class="toast-loader toast-loaded"></span><!-- Add Class toast-loaded for Animation Loader -->
          <span class="sl-toast-close">×</span>
          <h4 class="sl-toast-heading">%s</h4>
          <p>%s</p>
        </div>';
        */
        foreach (self::$messages as $messageType => $messages) {
            foreach ($messages as $message) {
                if($messageType == 'danger') { $messageType = 'danger';}
                //Temporary
                if($messageType == 'info') { $messageType = 'info';}
                $header = '';
                switch($messageType)
                {
                    case 'danger' : $header = __('Oh snap!'); break;
                    case 'warning' : $header = __('Heads up!'); break;
                    case 'success' : $header = __('Well done!'); break;
                    case 'info' : $header = __('Oh snap!'); break;
                }
                if(is_array($message)) { $message = implode(', ', $message); }
                //$markup .= sprintf($element, $messageType, $interval, $header, $message);
                $markup .= sprintf($element, $messageType, $header, $message);
                $interval += 500;
            }
        }
        return '<div class="sl-toast-wrap top-right">'.$markup.'</div>';
        self::flush();
    }
}
