<?php

namespace Cascade\System\Services;

use Cascade\System\Abstraction\IconLibrary;
use stdClass;

class Icons
{
    protected static $icons = [];

    public static function Register(String $name, String $slug, IconLibrary $library)
    {
        $icon = new stdClass;
        $icon->name = $name;
        $icon->slug = $slug;
        $icon->library = $library;
        static::$icons[$slug] = $icon;
    }

    public static function GetIcons()
    {
        return static::$icons;
    }

    public static function GetActiveIcons()
    {
        $active = [];
        $activeSetting = get_setting('system::services.icons-libraries', [], false);
        if(is_string($activeSetting)) { $activeSetting = json_decode($activeSetting); }
        foreach(static::$icons as $key => $icon)
        {
            if(in_array($key, $activeSetting))
            {
                $active[$key] = $icon;
            }
        }
        return $active;
    }

}
