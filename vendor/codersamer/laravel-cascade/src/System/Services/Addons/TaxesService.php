<?php

namespace Cascade\System\Services\Addons;

use Cascade\Dashboard\Entities\MenuItem;
use Cascade\Dashboard\Services\DashboardBuilder;
use Cascade\Dashboard\Services\DashboardMenu;
use Cascade\Settings\Services\Settings;
use Cascade\System\Abstraction\Service;
use Illuminate\Contracts\Foundation\Application;

class TaxesService extends Service
{
    public function boot(Application $app)
    {
        DashboardBuilder::Menu('system-overview')->Call(function(DashboardMenu $menu){
            $menu->AddItem(
                MenuItem::make(__('Taxes'))->icon('tag')
                ->route('dashboard.system.taxes.index')
                ->description(__('Manage Taxes Types and Rates'))
            );
        });
    }

    public function name(): string { return __('Taxes Service'); }
    public function description(): string { return __('Manage, Add and Edit Taxes in your System'); }
    public function identifier(): string { return 'taxes'; }
}
