<?php

namespace Cascade\System\Services\Addons;

use Cascade\System\Abstraction\Service;
use Cascade\System\Http\Middleware\ForceLoginMiddleware;
use Illuminate\Contracts\Foundation\Application;

class ForceLoginService extends Service
{
    public function name(): string
    {
        return __('Force Login');
    }

    public function description(): string
    {
        return __('Force Login to non guarded routes to override middlewares');
    }

    public function identifier(): string
    {
        return 'force-login';
    }

    public function register(Application $app)
    {
        $router = $app['router'];
        $router->pushMiddlewareToGroup('web', ForceLoginMiddleware::class);
    }
}
