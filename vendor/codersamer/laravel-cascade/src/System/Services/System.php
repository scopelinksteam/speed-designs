<?php

namespace Cascade\System\Services;

use Cascade\Site\Services\Themes;
use Cascade\System\Abstraction\Service;
use Cascade\System\Enums\OSFamily;
use Illuminate\Contracts\Support\Renderable;
use Cascade\System\Traits\HasActions;
use Cascade\System\Traits\HasFilters;
use Closure;
use Exception;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\View as FacadesView;
use Illuminate\View\View;
use stdClass;

class System
{

    use HasActions, HasFilters;

    protected static $viewsOverride = [];

    protected static $services = [];

    protected static $viewExtensions = [];

    protected static String $currentRenderingView = '';


    public static function IsMultiTenant() : bool
    {
        return config('cascade.multi_tenancy', false) == true;
    }

    public static function OverrideView($view, $replacement, $valuesCallback = null, $priority = 10)
    {
        if(!isset(self::$viewsOverride[$view])) { self::$viewsOverride[$view] = []; }
        $override = new stdClass;
        $override->view = $replacement;
        $override->priority = $priority;
        $override->values_callback = $valuesCallback;
        self::$viewsOverride[$view][] = $override;
    }

    public static function Link(String $source, String $target)
    {
        $currentLinks = config('filesystems.links');
        $currentLinks[$target] = $source;
        config(['filesystems.links' => $currentLinks]);
        Artisan::call('storage:link');
    }

    public static function Unlink($link)
    {
        if(OSFamily::CurrentOS() == OSFamily::Windows) { return rmdir($link); }
        else { if(is_link($link)) { return unlink($link); } }
        return false;
    }

    public static function Schedule(?Closure $callback = null) : \Illuminate\Console\Scheduling\CallbackEvent|Schedule
    {
        $schedule = app()->make(Schedule::class);
        return $callback == null ? $schedule : $schedule->call($callback);
    }
    /**
     * Try to Find a View or Replacement
     *
     * @param String $view
     * @param array $params
     * @return Renderable
     */
    public static function FindView($view, $params = [])
    {
        if(!FacadesView::exists($view))
        {
            $replacement = static::FindViewReplacement($view);
            if($replacement != $view)
            {
                $replacementData = System::GetViewData($view);
                return view($replacement, $params, $replacementData);
            }
            return view($replacement);
        }
        return view($view, $params);
    }

    /**
     * Alias FindView
     *
     * @param String $view
     * @param array $params
     * @return Renderable
     */
    public static function view($view, $params = [])
    {
        return static::FindView($view, $params);
    }

    /**
     * Check if Module Exists
     *
     */

    /**
     * Get Stubs Path
     *
     * @return string
     */
    public static function StubsPath($stubName = '')
    {
        return module_path('System', 'Stubs/'.$stubName);
    }

    public static function ModuleActive($moduleName)
    {
        return Modules::Enabled($moduleName);
    }

    public static function PluginActive($pluginName)
    {
        return Plugins::IsEnabled($pluginName);
    }

    public static function ThemeActive($themeName)
    {
        return Themes::IsEnabled($themeName);
    }

    public static function BladeDirective($name, $class)
    {
        Blade::directive($name, function($expression) use($class) { return (new $class)->handle($expression);} );
    }

    public static function EnableService(String|Service $service, $forceOverride = false)
    {
        $service = is_string($service) ? new $service : $service;
        if(!($service instanceof Service)) { return; }
        if(isset(static::$services[get_class($service)]) && !$forceOverride) { return; }
        static::$services[get_class($service)] = $service;
    }

    public static function ServiceEnabled(String|Service $service) : bool
    {
        $service = is_string($service) ? new $service : $service;
        return isset(static::$services[get_class($service)]);
    }

    public static function DisableService(String|Service $service)
    {
        $service = is_string($service) ? new $service : $service;
        if(!($service instanceof Service)) { return; }
        if(!isset(static::$services[get_class($service)])) { return; }
        unset(static::$services[get_class($service)]);
    }

    /**
     * Return List of Enabled Services
     *
     * @return Service[]
     */
    public static function GetServices()
    {
        return static::$services;
    }

    public static function SetRenderingView($viewName)
    {
        static::$currentRenderingView = $viewName;
    }

    public static function FindViewReplacement($viewName)
    {
        if(!isset(self::$viewsOverride[$viewName]) || count(self::$viewsOverride[$viewName]) == 0)
        { return $viewName; }
        return self::$viewsOverride[$viewName][0]->view;
    }

    public static function ExtendView(String $viewName, String $section, Closure|String|View $content)
    {
        if(!isset(static::$viewExtensions[$viewName])) { static::$viewExtensions[$viewName] = []; }
        $extension = new stdClass;
        $extension->view = $viewName;
        $extension->section = $section;
        $extension->content = $content;
        static::$viewExtensions[$viewName][] = $extension;
    }

    public static function GetViewExtensions(String $viewName)
    {
        return isset(static::$viewExtensions[$viewName]) ? static::$viewExtensions[$viewName] : [];
    }

    public static function GetViewData(String $viewName, $params = [])
    {
        if(!isset(self::$viewsOverride[$viewName]) || count(self::$viewsOverride[$viewName]) == 0)
        { return $params; }

        $viewsAvailable = self::$viewsOverride[$viewName];
        $extraValues = [];
        if($viewsAvailable[0]->values_callback != null)
        {

            $callback = $viewsAvailable[0]->values_callback;
            if(is_array($callback)) {
                $extraValues = $callback;
            }
            else if(is_callable($callback))
            {
                $extraValues = call_user_func($callback);
            }
        }
        return array_merge_recursive($params, $extraValues);
    }
}
