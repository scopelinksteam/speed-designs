<?php

namespace Cascade\System\Services;

use Cascade\System\Entities\NotificationHandler;
use Closure;
use Illuminate\View\View;

class NotificationService
{

    protected static $notifications = [];

    public static function Register(String $name, String $slug, String $notificationClass) : NotificationHandler
    {
        $object = NotificationHandler::make($notificationClass);
        $object->name($name);
        $object->slug($slug);
        static::$notifications[$notificationClass] = $object;
        return $object;
    }

    public static function SetView(String $notificationClass, View|Closure|String $view)
    {
        if(isset(static::$notifications[$notificationClass]))
        { static::$notifications[$notificationClass]->view($view); }
    }

    public static function GetDefinition($notificationClass)
    {
        foreach(static::$notifications as $notification)
        {
            if($notification->class() == $notificationClass) { return $notification; }
        }
        return null;
    }

    public static function Notifications() { return collect(static::$notifications); }
}
