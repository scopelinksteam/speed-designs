<?php

namespace Cascade\System\Services;

use Cascade\System\Entities\Module;
use Cascade\Tenancy\Enums\TenantMode;
use Cascade\Tenancy\Services\TenancyService;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\File;
use Symfony\Component\Finder\Finder;

class Modules
{

    protected static $modules = null;


    /**
     * Get Modules Base Path
     *
     * @return string
     */
    public static function GetBasePath($appends = '')
    {
        return ltrim(base_path('/Modules/'.trim($appends, '/')),'/');
    }

    /**
     * Get Modules
     *
     * @return Collection
     */
    public static function GetModules($customOnly = null)
    {
        $tenantModules = System::IsMultiTenant() && TenancyService::CurrentTenant() != null ? TenancyService::CurrentTenant()->modules : [];
        if(self::$modules !== null)
        {
            $modules = [];
            if($customOnly === null)
            {
                return (TenancyService::ActiveMode() != TenantMode::Tenant)
                ? collect(self::$modules)
                : collect(self::$modules)->filter(function($mod) use($tenantModules) { return isset($tenantModules[$mod->GetName()]) || !config('cascade.multi_tenancy'); });
            }
            if($customOnly === false)
            {
                return (TenancyService::ActiveMode() != TenantMode::Tenant)
                ? collect(self::$modules)->filter(function($mod){ return $mod->IsProtected(); })
                : collect(self::$modules)->filter(function($mod) use($tenantModules) { return $mod->IsProtected() && (isset($tenantModules[$mod->GetName()]) || !config('cascade.multi_tenancy')) ; });
            }
            if($customOnly === true)
            {
                return (TenancyService::ActiveMode() != TenantMode::Tenant)
                ? collect(self::$modules)->filter(function($mod){ return !$mod->IsProtected(); })
                : collect(self::$modules)->filter(function($mod) use($tenantModules) { return !$mod->IsProtected()  && (isset($tenantModules[$mod->GetName()]) || !config('cascade.multi_tenancy')); }) ;
            }
        }
        $finder = new Finder();
        $modulesFiles = $finder->in(base_path('/Modules'))->depth(1)->name('module.json')->files();
        self::$modules = [];
        $modules = [];

        foreach($modulesFiles as $file)
        {
            $object = json_decode($file->getContents());
            self::$modules[] = Module::LoadFrom($file);
            $isProtected = isset($object->protected) ? $object->protected : false;
            //Skip not allowed modules
            if(TenancyService::ActiveMode() == TenantMode::Tenant && !isset($tenantModules[$object->name]) && config('cascade.multi_tenancy'))
            { continue; }
            if($customOnly === null)
            { $modules[] = Module::LoadFrom($file);}
            if($customOnly === false && $isProtected)
            { $modules[] = Module::LoadFrom($file);}
            if($customOnly === true && !$isProtected)
            { $modules[] = Module::LoadFrom($file);}
        }

        return collect($modules);
    }

    /**
     * Enable a Module
     */
    public static function Enabled($moduleName)
    {
        $module = static::Get($moduleName);
        return $module != null && $module->IsEnabled();
    }

    /**
     * Enable a Module
     */
    public static function Enable($moduleName)
    {
        System::IsMultiTenant() && TenancyService::CurrentTenant() != null ? TenancyService::CurrentTenant()->SetModuleStatus($moduleName, true) : Artisan::call('module:enable '.$moduleName);
    }

    /**
     * Enable a Module
     */
    public static function Disable($moduleName)
    {
        System::IsMultiTenant() ?  TenancyService::CurrentTenant()->SetModuleStatus($moduleName, false) : Artisan::call('module:disable '.$moduleName);
    }

    public static function Path($moduleName = null, $combine = '')
    {
        return  $moduleName == null ? self::GetBasePath($combine) : self::GetBasePath(\Str::studly($moduleName).'/'.$combine);
    }

    public static function FileExists($moduleName, $filename)
    {
        return File::exists(self::Path($moduleName, $filename));
    }

    public static function DirectoryExists($moduleName, $filename)
    {
        return File::isDirectory(self::Path($moduleName, $filename));
    }

    /**
     * Get Module
     *
     * @return Module
     */
    public static function Get($name)
    {
        return self::GetModules()->filter(function($module) use($name){
            return strtolower($module->GetName()) == strtolower($name);
        })->first();
    }

    /**
     * Create New Module
     *
     * @return Module
     */
    public static function CreateModule($moduleName)
    {

    }


    public static function Delete($pluginName)
    {
        self::Disable($pluginName);
        File::deleteDirectory(self::Path($pluginName));
        return true;
    }

    public static function InstallFromFile($file, $deleteAfter = true)
    {
        if(!File::exists($file)) { return false;}
        if(File::extension($file) != 'zip') { return false; }
        $tempName = \Str::uuid();
        $tempDirectory = File::ensureDirectoryExists(storage_path($tempName));
        $files = (new Unzip())->extract($file, storage_path($tempName));
        if(count($files) == 0 || !\Str::endsWith($files[0],'/'))
        {
            File::deleteDirectory(storage_path($tempName));
            return false;
        }
        $moduleName = trim($files[0], '/');
        if(!in_array($moduleName.'/module.json', $files))
        {
            File::deleteDirectory(storage_path($tempName));
            return false;
        }


        File::copyDirectory(storage_path($tempName), self::GetBasePath());
        File::deleteDirectory(storage_path($tempName));
        return true;
    }

    protected static function EnsurePluginsListExists()
    {
        if(!File::exists(module_path('System', 'Resources/plugins.json')))
        {
            $contents = new stdClass;
            File::put(module_path('System', 'Resources/plugins.json'), json_encode($contents, JSON_PRETTY_PRINT));
        }
    }

    protected static function PutPlugin($pluginName, $status = false)
    {
        self::EnsurePluginsListExists();
        $contents = json_decode(file_get_contents(module_path('System', 'Resources/plugins.json')));
        $contents->{$pluginName} = $status === null ? self::IsEnabled($pluginName) : $status;
        File::put(module_path('System', 'Resources/plugins.json'), json_encode($contents, JSON_PRETTY_PRINT));
    }

    public static function PutFile($moduleName, $filename, $contents)
    {
        File::ensureDirectoryExists(self::Path($moduleName, dirname($filename)));
        $filePath = self::Path($moduleName, $filename);
        File::put($filePath, $contents);
        return $filePath;
    }

    public static function PutDirectory($moduleName, $path, $keep = false)
    {
        File::ensureDirectoryExists(self::Path($moduleName, $path));
        if($keep)
        {
            self::PutFile($moduleName, rtrim($path,'/').'/'.'.gitkeep', '');
        }
    }


    public static function GetCurrentModulesStatus()
    {

    }

}
