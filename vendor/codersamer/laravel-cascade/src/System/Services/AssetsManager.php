<?php

namespace Cascade\System\Services;

use Cascade\Site\Entities\Theme;
use Cascade\System\Actions\InitializeDisksAction;
use Cascade\System\Entities\Module;
use Cascade\System\Entities\Plugin;
use Cascade\Tenancy\Enums\TenantMode;
use Cascade\Tenancy\Services\TenancyService;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\File;

class AssetsManager
{

    public static function InitializeDisks()
    {
        Action::Apply(InitializeDisksAction::class);
        $links = [];
        //Modules
        if(!File::exists(public_path('modules'))) File::makeDirectory(public_path('modules'));
        $modules = Module::All();
        foreach($modules as $module)
        {
            if(File::isDirectory($module->GetPath('public')))
            { $links['modules/'.$module->GetSlug()] = $module->GetPath('public'); }
        }

        //Plugins
        if(!File::exists(public_path('plugins'))) File::makeDirectory(public_path('plugins'));
        $plugins = Plugin::All();
        foreach($plugins as $plugin)
        {
            if(File::isDirectory($plugin->GetPath('public')))
            { $links['plugins/'.$plugin->GetSlug()] = $plugin->GetPath('public'); }
        }

        //Plugins
        if(!File::exists(public_path('themes'))) File::makeDirectory(public_path('themes'));
        $themes = Theme::All();
        foreach($themes as $theme)
        {
            if(File::isDirectory($theme->GetPath('public')))
            { $links['themes/'.$theme->GetSlug()] = $theme->GetPath('public'); }
        }

        $originalLinks = config('filesystems.links');
        foreach($links as $target => $source)
        {
            $originalLinks[public_path($target)] = $source;
        }
        config(['filesystems.links' => $originalLinks]);
        Artisan::call('storage:link');

    }



    public static function GetTenantAssetsName()
    {
        $currentTenant = TenancyService::CurrentTenant();
        $tenantSlug = TenancyService::ActiveMode() == TenantMode::Central
                        ? 'central'
                        : $currentTenant->slug;
        $tenantSlug = empty($tenantSlug) ? str($currentTenant->name)->slug() : $tenantSlug;
        return $tenantSlug;
    }

    public static function UploadsUrl($appends = '')
    {
        $hasTenancy = config('cascade.multi_tenancy', false);
        return url('uploads/'.($hasTenancy ? static::GetTenantAssetsName().'/' : '').$appends);
    }

    protected static function GenerateSlug($name)
    {
        return str($name)->slug();
    }

    public static function PluginUrl($plugin, $appends = '')
    {
        return url('plugins/'.static::GenerateSlug($plugin).'/'.$appends);
    }

    public static function ModuleUrl($module, $appends = '')
    {
        return url('modules/'.static::GenerateSlug($module).'/'.$appends);
    }

    public static function ThemeUrl($theme, $appends = '')
    {
        return url('themes/'.static::GenerateSlug($theme).'/'.$appends);
    }
}
