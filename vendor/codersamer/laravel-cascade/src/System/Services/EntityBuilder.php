<?php

namespace Cascade\System\Services;

use Cascade\System\DTO\EntityCard;
use Cascade\System\DTO\MetaBox;
use Cascade\System\Enums\EntityAction;
use Cascade\System\Enums\EntityCardPlacement;
use Illuminate\Support\Collection;

class EntityBuilder
{
    protected array $cards = [];

    protected ?object $cardsManager = null;

    protected static array $buildersInstances = [];


    public function __construct(protected String $entityClass)
    {

        static::$buildersInstances[$entityClass] = $this;
    }

    public static function of(String|object $entityClass) : ?EntityBuilder
    {
        $entityClass = is_object($entityClass) ? get_class($entityClass) : $entityClass;
        return isset(static::$buildersInstances[$entityClass]) ? static::$buildersInstances[$entityClass] : null;
    }

    public function cards()
    {
        if($this->cardsManager != null) { return $this->cardsManager; }

        return $this->cardsManager = new class($this) {

            protected array $cards = [];

            public function __construct(protected EntityBuilder $builder) { }

            public function add(EntityCard $card, EntityAction $action) : self
            {
                if(!isset($this->cards[$action->value])) { $this->cards[$action->value] = []; }
                $this->cards[$action->value][$card->id()] = $card;
                return $this;
            }

            public function new(String $id, EntityAction $action) : self
            {
                if(!isset($this->cards[$action->value])) { $this->cards[$action->value] = []; }
                $card = EntityCard::make($id);
                $this->cards[$action->value][$card->id()] = $card;
                return $this;
            }

            public function find(String $id) : ?EntityCard
            {
                foreach($this->cards as $entityAction => $cards)
                {
                    if(isset($cards[$id])) { return $cards[$id]; }
                }
                return null;
            }

            public function all() : Collection
            { return collect($this->cards); }

            public function for(EntityAction $action, EntityCardPlacement ...$placements) : Collection
            {
                $filteredCards = [];
                $actions = [$action];
                if($action == EntityAction::Upsert)
                {
                    $actions = [$action, EntityAction::Create, EntityAction::Update];
                }
                if($action == EntityAction::Update || $action == EntityAction::Create)
                {
                    $actions = [$action, EntityAction::Upsert];
                }
                foreach($this->all() as $entityAction => $cards)
                {
                    //Filter By Actions, Skip Any Enumeration
                    $entityAction = EntityAction::from((string)$entityAction);
                    if($action != EntityAction::Any && !in_array($entityAction, $actions))
                    { continue; }
                    foreach($cards as $cardId => $card)
                    {
                        if(!in_array($card->placement(), $placements)) { continue; }
                        $filteredCards[] = $card;
                    }
                }
                return collect($filteredCards);
            }
        };

    }
}
