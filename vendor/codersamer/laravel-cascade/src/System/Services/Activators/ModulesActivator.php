<?php

namespace Cascade\System\Services\Activators;

use Cascade\System\Actions\ModuleDisableAction;
use Cascade\System\Actions\ModuleEnableAction;
use Cascade\System\Filters\AvailableModulesFilter;
use Cascade\System\Filters\ModuleStatusFilter;
use Cascade\System\Services\Action;
use Cascade\System\Services\Filter;
use Illuminate\Support\Facades\Artisan;
use Nwidart\Modules\Contracts\ActivatorInterface;
use Nwidart\Modules\Facades\Module as FacadesModule;
use Nwidart\Modules\Module;

class ModulesActivator implements ActivatorInterface
{

    public function isSystemModule(Module $module)
    {
        return $module->get('protected', false) !== false;
    }

    /**
     * Enables a module
     *
     * @param Module $module
     */
    public function enable(Module $module): void
    {
        if($this->isSystemModule($module)){return;}

        if(Action::Apply(ModuleEnableAction::class, $module))
        { Artisan::call("module:enable ".$module->getName()); }
    }

    /**
     * Disables a module
     *
     * @param Module $module
     */
    public function disable(Module $module): void
    {
        if($this->isSystemModule($module)){return;}
        if(Action::Apply(ModuleDisableAction::class, $module))
        { Artisan::call("module:disable ".$module->getName()); }
    }

    /**
     * Determine whether the given status same with a module status.
     *
     * @param Module $module
     * @param bool $status
     *
     * @return bool
     */
    public function hasStatus(?Module $module, bool $status): bool
    {
        if($module == null) { return false == $status; }
        if(app()->runningInConsole() || $this->isSystemModule($module)){ return $status; }
        $currentStatus = false;
        if(!file_exists(base_path('modules_statuses.json')))
        { $currentStatus = false; }
        else
        {
            $data = json_decode(file_get_contents(base_path('modules_statuses.json')), true);
            $currentStatus = isset($data[$module->getName()]) && $data[$module->getName()];
        }
        
        $currentStatus = Filter::Apply(ModuleStatusFilter::class, $currentStatus, $module);

        return $currentStatus == $status;

    }

    /**
     * Set active state for a module.
     *
     * @param Module $module
     * @param bool $active
     */
    public function setActive(Module $module, bool $active): void
    {
        if($this->isSystemModule($module)){return;}
        $active ? $this->enable($module) : $this->disable($module);
    }

    /**
     * Sets a module status by its name
     *
     * @param  string $name
     * @param  bool $active
     */
    public function setActiveByName(string $name, bool $active): void
    {
        $module = FacadesModule::find($name);
        if($module == null) { return; }        
        $active  ? $this->enable($module) : $this->disable($module);
    }

    /**
     * Deletes a module activation status
     *
     * @param  Module $module
     */
    public function delete(Module $module): void
    {
        if($this->isSystemModule($module)){return;}
        if($this->activeTenant == null) { return; }
        $this->disable($module);
    }

    public function GetAvailableModules()
    {
        $modules = FacadesModule::all();
        return Filter::Apply(AvailableModulesFilter::class, $modules);
    }

    /**
     * Deletes any module activation statuses created by this class.
     */
    public function reset(): void
    {
        foreach($this->GetAvailableModules() as $module)
        {
            $this->disable($module);
        }
    }
}
