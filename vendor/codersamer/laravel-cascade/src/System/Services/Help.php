<?php

namespace Cascade\System\Services;

use Cascade\System\Enums\HelpModes;
use Illuminate\View\View;

class Help
{
    protected static $counter = 0;

    protected int|String $id = 0;

    protected HelpModes $mode = HelpModes::Link;

    protected String $title = '';

    protected String $description = '';

    protected String $target = '';

    protected String|View|null $view = null;

    protected function __construct(HelpModes $mode)
    {
        $this->mode = $mode;
        static::$counter++;
        $this->id = static::$counter;
    }

    public static function make(HelpModes $mode = HelpModes::Link) : static
    {
        return new static($mode);
    }

    public function id(int|String $id = -1) : static|int|String
    {
        if($id !== -1) { $this->id = $id; return $this;}
        return $this->id;
    }

    public function title(?String $title = null) : static|String
    {
        if($title === null) { return $this->title; }
        $this->title = $title;
        return $this;
    }

    public function description(?String $description = null) : static|String
    {
        if($description === null) { return $this->description; }
        $this->description = $description;
        return $this;
    }

    public function target(?String $target = null) : static|String
    {
        if($target === null) { return $this->target; }
        $this->target = $target;
        return $this;
    }

    public function view(?String $view = null) : static|String|View|null
    {
        if($view === null) { return $this->view; }
        $this->view = $view;
        return $this;
    }

    public function mode(?HelpModes $mode = null) : static|HelpModes
    {
        if($mode === null) { return $this->mode; }
        $this->mode = $mode;
        return $this;
    }
}