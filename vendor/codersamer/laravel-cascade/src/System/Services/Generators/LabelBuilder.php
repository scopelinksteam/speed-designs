<?php

namespace Cascade\System\Services\Generators;

use Cascade\Site\Services\PostType;
use Cascade\System\Enums\LabelFeatures;

class LabelBuilder
{

    protected ?String $name = null;

    protected ?String $slug = null;

    protected ?String $type = null;

    protected ?String $title = null;

    protected ?String $description = null;

    protected array $features = [LabelFeatures::All];

    protected function __construct() { }

    public static function make(String $name, String $class) : static
    {
        $builder = new static;
        $builder->name($name);
        $builder->type($class);
        return $builder;
    }

    public function name(?String $name = null) : String|null|static
    {
        if($name === null) { return $this->name ?? __('Label'); }
        $this->name = $name;
        if($this->slug == null)
        {
            $this->slug = str($this->name)->slug();
        }
        return $this;
    }

    public function slug(?String $slug = null) : String|null|static
    {
        if($slug === null) { return $this->slug; }
        $this->slug = $slug;
        return $this;
    }

    public function title(?String $title = null) : String|null|static
    {
        if($title === null) { return $this->title ?? $this->name(); }
        $this->title = $title;
        return $this;
    }

    public function description(?String $description = null) : String|null|static
    {
        if($description === null) { return $this->description ?? __('Manage and Overview Records'); }
        $this->description = $description;
        return $this;
    }

    public function type(?String $type = null) : String|null|static
    {
        if($type === null) { return $this->type; }
        $this->type = $type;
        return $this;
    }

    public function features(LabelFeatures ...$features) : static
    {
        $this->features = $features;
        return $this;
    }

    public function supports(LabelFeatures|String $feature)
    {
        $searchFeature = is_string($feature) ? LabelFeatures::tryFrom($feature) : $feature;
        return in_array(LabelFeatures::All, $this->features) || in_array($searchFeature, $this->features);
    }
}