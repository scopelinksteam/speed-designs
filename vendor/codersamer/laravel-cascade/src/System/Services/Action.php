<?php

namespace Cascade\System\Services;

use Closure;
use Illuminate\Http\RedirectResponse;
use stdClass;

class Action
{
    protected static array $actionsHandlers = [];

    public static function Subscribe(String $subscriber)
    {
        if(class_exists($subscriber))
        {
            $obj = new $subscriber;
            if(method_exists($obj, 'actions'))
            {
                call_user_func(Closure::fromCallable([$obj, 'actions']));
            }
        }
    }

    public static function On(String $action, array|Closure|String $callback, int $order = 10)
    {
        if(!isset(static::$actionsHandlers[$action])) { static::$actionsHandlers[$action] = []; }
        $handler = new stdClass;
        $handler->target    = $action;
        $handler->order     = $order;
        if(is_string($callback) && class_exists($callback))
        {
            $callback = [new $callback($action), 'Handle'];
        }
        $handler->callback  = Closure::fromCallable($callback);
        static::$actionsHandlers[$action][] = $handler;
    }

    public static function Apply(String $action, ...$args)
    {
        if(!isset(static::$actionsHandlers[$action])) { return true; }
        $orderedHandlers = collect(static::$actionsHandlers[$action])->sort(function($a, $b){
            if($a->order == $b->order) { return 0; }
            return $a->order < $b->order ? -1 : 1;
        });
        foreach($orderedHandlers as $handler)
        {
            $shouldContinue = call_user_func($handler->callback, ...$args);
            if($shouldContinue instanceof RedirectResponse)
            {
                $shouldContinue->send();
                exit;
            }
            if($shouldContinue === false) { return false; }
        }
        return true;
    }

}