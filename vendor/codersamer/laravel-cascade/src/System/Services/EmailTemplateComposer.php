<?php

namespace Cascade\System\Services;

use Cascade\System\Contracts\Exportable;
use Cascade\System\Entities\EmailTemplate;
use Closure;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Notifications\AnonymousNotifiable;
use stdClass;

class EmailTemplateComposer
{


    protected $placeholders = [];

    protected $data = [];

    protected $for = null;

    protected String $title = '';

    protected String $description = '';

    protected static $globalPlaceholders = [];

    protected static $placeholderTemplate = '__PLACEHOLDER__';

    protected EmailTemplate $template;

    protected function __construct(public String|Exportable $type, public array|null $identifiers = [])
    {

    }

    public static function make(String|Exportable $type, array|null $identifiers = null, String|null|array $content = null) : self
    {
        $composer =  new static($type, $identifiers);
        if($identifiers != null)
        {
            foreach($identifiers as $identifierKey => $identifierText)
            {
                $existTemplate = EmailTemplate::for($composer->type(), $identifierKey)->first();
                $templateContent = $content == null ? '' : $content;
                $templateContent = is_array($templateContent) ? (isset($templateContent[$identifierKey]) ? $templateContent[$identifierKey] : current($templateContent)) : $templateContent;
                if($existTemplate == null)
                {
                    $template = new EmailTemplate();
                    $template->type = $composer->type();
                    $template->identifier = $identifierKey;
                    $template->content = $templateContent ?? '';
                    $template->title = $identifierText;
                    $template->name = $identifierText;
                    $template->save();
                }
            }

        }
        return $composer;
    }

    public function type() : String
    {
        return $this->type instanceof Exportable ? $this->type->export() : $this->type;
    }

    public function title(?String $title = null) : String|self
    {
        if($title != null)
        {
            $this->title = $title;
            return $this;
        }
        return $this->title;
    }

    public function description(?String $description = null) : String|self
    {
        if($description != null)
        {
            $this->description = $description;
            return $this;
        }
        return $this->description;
    }

    public function identifiers() : array|null { return $this->identifiers; }
    public function data(array|null $data = null) : array|self
    {
        if($data == null) { return $this->data; }
        $this->data = $data;
        return $this;
    }

    public function creatable() : bool
    {
        return empty($this->identifiers());
    }

    public function destroyable() : bool
    {
        return empty($this->identifiers());
    }

    public function template(EmailTemplate|null $template = null) : EmailTemplate|self
    {
        if($template == null) { return $this->template; }
        $this->template = $template;
        return $this;
    }

    public function hash() : String
    {
        return md5($this->type());
    }
    public function placeholders() : array
    {
        return array_merge(static::$globalPlaceholders, $this->placeholders);
    }

    public function placeholder(String $key, String $name, String|Closure $value) : self
    {
        $placeholder =  new stdClass;
        $placeholder->key = $key;
        $placeholder->callable_key = str_replace('PLACEHOLDER', str_replace(' ', '_',str_replace('-', '_',strtoupper($key))), static::$placeholderTemplate);
        $placeholder->name = $name;
        $placeholder->value = $value;
        $this->placeholders[] = $placeholder;
        return $this;
    }

    public static function globalPlaceholder(String $key, String $name, String|Closure $value)
    {
        $placeholder =  new stdClass;
        $placeholder->key = $key;
        $placeholder->callable_key = str_replace('PLACEHOLDER', str_replace(' ', '_',str_replace('-', '_',strtoupper($key))), static::$placeholderTemplate);
        $placeholder->name = $name;
        $placeholder->value = $value;
        static::$globalPlaceholders[] = $placeholder;
    }

    public function for($for = null) : mixed
    {
        if($for == null) { return $this->for; }
        $this->for = $for;
        return $this;
    }

    public function email() : mixed
    {
        if($this->for instanceof AnonymousNotifiable)
        {
            $route = $this->for->routeNotificationFor('mail');
            if(is_array($route))
            {
                return array_key_first($route);
            }
            return $route;
        }
        if(is_object($this->for))
        {
            return $this->for->email ?? '';
        }
        return '';
    }

    public function name() : String
    {
        if($this->for instanceof AnonymousNotifiable)
        {
            $route = $this->for->routeNotificationFor('mail');
            if(is_array($route))
            {
                return array_shift($route);
            }
            return $route;
        }
        if(is_object($this->for))
        {
            return $this->for->name ?? '';
        }
        return '';
    }

    public function templates() : Builder
    {
        return EmailTemplate::of($this->type());
    }

    public function render()
    {
        $content = $this->data['content'] ?? $this->template->content;
        $placeholders = array_merge(static::$globalPlaceholders, $this->placeholders);
        foreach($placeholders as $placeholder)
        {
            $value = is_callable($placeholder->value) ? call_user_func($placeholder->value, $this) : $placeholder->value;
            $content = str_replace($placeholder->callable_key, $value, $content);
        }
        //Fix Images
        return $content;
    }
}
