<?php

namespace Cascade\System\Services;

use Exception;
use ReflectionEnum;
use UnitEnum;

class Enums
{
    public static function OptionsFor(String $enumName)
    {
        try
        {
            $reflection = new ReflectionEnum($enumName);
            $cases = $reflection->getCases();
            $values = [];
            foreach($cases as $case)
            {
                $case = $case->getValue();
                $name = $case->name;
                $name = preg_split('/(?=[A-Z])/', $name);
                $name = ucwords(implode(' ', $name));
                $values[$case->value] = $name;
            }
            return $values;
        }
        catch(Exception $x) { return []; }
    }
}
