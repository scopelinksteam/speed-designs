<?php

namespace Cascade\System\Services;

use Cascade\System\DTO\Database\Table;
use Illuminate\Support\Facades\DB;

class DatabaseManager
{
    public static function tables()
    {
        $data = DB::connection()->select("SHOW TABLES");
        $tables = [];
        foreach($data as $row)
        {
            $rowArray = (array)$row;
            $tables[] = Table::make(array_shift($rowArray));
        }
        return collect($tables);
    }

}
