<?php

namespace Cascade\System\Services;

use Cascade\System\Abstraction\HTMLElement;

class HTMLBuilder extends HTMLElement
{

    public function render()
    {
        return view('system::html-elements.builder', ['element' => $this]);
    }

    public function build($bindable = null)
    {
        $this->bind($bindable);
        echo $this->render()->toHtml();
    }
}
