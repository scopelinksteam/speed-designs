<?php

namespace Cascade\System\Services;

use Closure;
use stdClass;

class Filter
{
    protected static array $filtersHandlers = [];

    public static function Subscribe(String $subscriber)
    {
        if(class_exists($subscriber))
        {
            $obj = new $subscriber;
            if(method_exists($obj, 'filters'))
            {
                call_user_func(Closure::fromCallable([$obj, 'filters']));
            }
        }
    }

    public static function On(String $filter, array|Closure|String $callback, int $order = 10)
    {
        if(!isset(static::$filtersHandlers[$filter])) { static::$filtersHandlers[$filter] = []; }
        $handler = new stdClass;
        $handler->target    = $filter;
        $handler->order     = $order;
        $handler->callback  = is_string($callback) ? $callback : Closure::fromCallable($callback);
        static::$filtersHandlers[$filter][] = $handler;
    }

    public static function Apply(String $filter, mixed $object, $owner = null)
    {
        if(!isset(self::$filtersHandlers[$filter])) { return $object; }
        $orderedHandlers = collect(static::$filtersHandlers[$filter])->sort(function($a, $b){
            if($a->order == $b->order) { return 0; }
            return $a->order < $b->order ? -1 : 1;
        });
        foreach($orderedHandlers as $handler)
        {
            if(is_string($handler->callback))
            {
                if(class_exists($handler->callback))
                {
                    $class = $handler->callback;
                    $handlerObj = new $class;

                    if(method_exists($handlerObj, 'filter')) 
                    { $object = call_user_func(Closure::fromCallable([$handler, 'filter']), ...[$object, $owner]); }
                    else if(method_exists($handlerObj, 'handle'))
                    { $object = call_user_func(Closure::fromCallable([$handler, 'handle']), ...[$object, $owner]); }
                }
            }
            else { $object =  call_user_func($handler->callback, ...[$object, $owner]); }
            
        }
        return $object;
    }
}