<?php

namespace Cascade\System\Services;

use Cascade\Dashboard\Enums\ScriptLocation;
use Cascade\Dashboard\Services\DashboardBuilder;
use Cascade\Site\Services\Site;

class GoogleMaps 
{

    protected static bool $initialized = false;

    protected static String $siteCallback = 'gmapCallback';

    public static function RegisterFrontend()
    {
        if(get_setting('integrations::gmap.credentials.api_key', null) != null)
        {
            $dashboardCallback = 'gmapCallback';
            $apiKey = get_setting('integrations::gmap.credentials.api_key', null);
            $script = 'https://maps.googleapis.com/maps/api/js?key='.$apiKey.'&libraries=places&callback=__CALLBACK__&language=ar&region=EG';
            Site::AddScript(str_replace('__CALLBACK__', static::$siteCallback, $script));
            DashboardBuilder::AddJavascriptFunction($dashboardCallback, 'return;');
            DashboardBuilder::InjectScript(str_replace('__CALLBACK__', $dashboardCallback, $script), ScriptLocation::BeforeCustom);
            static::$initialized = true;
        }
    }

    public static function Initialized() { return static::$initialized; }

    public static function SetSiteCallback(String $name)
    {
        static::$siteCallback = $name;
    }
}