<?php

namespace Cascade\System\Services;

use Cascade\System\Entities\Module;
use Illuminate\Support\Facades\File;
use Cascade\System\Entities\Plugin;
use Cascade\System\Filters\PluginStatusFilter;
use Cascade\Tenancy\Enums\TenantMode;
use Cascade\Tenancy\Services\TenancyService;
use stdClass;
use Symfony\Component\Finder\Finder;

class Plugins
{

    protected static $plugins = null;

    public static function ScanPlugins($force = false)
    {
        if(!is_dir(self::GetBasePath())) { return collect(); }
        if(self::$plugins !== null && !$force) { return collect(self::$plugins); }
        $finder = new Finder();
        $files = $finder->in(self::GetBasePath())->name('plugin.json')->depth(1)->files();
        self::$plugins = [];
        foreach($files as $file)
        {
            $plugin = Plugin::LoadFrom($file);
            self::PutPlugin($plugin->GetName(), null);
            self::$plugins[] = $plugin;
        }
        return collect(self::$plugins);
    }



    public static function GetPlugins()
    {
        $plugins = self::ScanPlugins();
        if(TenancyService::ActiveMode() == TenantMode::Tenant && !app()->runningInConsole())
        {
            $tenantPlugins = TenancyService::CurrentTenant()->plugins;
            return $plugins->filter(function($plugin) use ($tenantPlugins){
                return isset($tenantPlugins[$plugin->GetName()]);
            });
        }
        return $plugins;
    }

    /**
     * Get a Plugin Instance By its Name
     *
     * @return Plugin
     */
    public static function Get($name) : Plugin
    {
        return self::GetPlugins()->filter(function($item) use ($name){
            return strtolower($item->GetName()) == strtolower($name);
        })->first();
    }

    public static function GetEnabledPlugins()
    {
        $plugins = self::GetPlugins();
        return $plugins->filter(function($item){
            return $item->IsEnabled();
        });
    }


    public static function GetDisabledPlugins()
    {
        $plugins = self::GetPlugins();
        return $plugins->filter(function($item){
            return !$item->IsEnabled();
        });
    }

    public static function GetBasePath($path = '')
    {
        return $path == '' ? base_path('Plugins') : base_path('Plugins/'.$path);
    }

    public static function Path($pluginName = null, $combine = '')
    {
        return  $pluginName == null ? self::GetBasePath($combine) : self::GetBasePath(\Str::studly($pluginName).'/'.$combine);
    }

    public static function FileExists($pluginName, $filename)
    {
        return File::exists(self::Path($pluginName, $filename));
    }

    public static function DirectoryExists($pluginName, $filename)
    {
        return File::isDirectory(self::Path($pluginName, $filename));
    }

    public static function GetPluginStatus($pluginName)
    {
        $status = false;
        if(static::IsExists($pluginName))
        {
            if(file_exists(base_path('plugins.json'))) 
            { 
                $contents = file_get_contents(base_path('plugins.json'));
                $data = json_decode($contents, true);
                if(isset($data[$pluginName]))
                { $status = $data[$pluginName]; }
                else { $status = false; }    
            }
            else { $status = false; }
        }
        else { $status = false; }
        
        return Filter::Apply(PluginStatusFilter::class, $status, $pluginName);

    }

    public static function IsEnabled($pluginName)
    {
        return static::GetPluginStatus($pluginName);
    }

    public static function IsExists($pluginName)
    {
        return File::exists(self::Path($pluginName, 'plugin.json'));
    }

    public static function Enable($pluginName)
    {
        $pluginInstance = Plugins::Get($pluginName);

        $missingModules = [];
        $availableModules = [];
        foreach($pluginInstance->GetRequirements() as $requirement)
        {
            $module = System::GetModule($requirement);
            if($module == null) { $missingModules[] = $requirement; }
            else { $availableModules[] = $module; }
        }
        if(count($missingModules) > 0)
        {
            Feedback::getInstance()->addAlert(__('Required Modules Cannot be found').' : '.implode(', ',$missingModules));
            return false;
        }
        if(count($availableModules))
        {
            foreach($availableModules as $module)
            {
                $module->Activate();
            }
        }
        if(is_dir($pluginInstance->GetPath().'/public'))
        {
            File::copyDirectory($pluginInstance->GetPath().'/public', public_path('plugins/'.strtolower($pluginName)));
        }
        self::PutPlugin($pluginName, true);
        return true;
    }

    public static function Disable($pluginName)
    {
        if(is_dir(public_path('plugins/'.strtolower($pluginName))))
        {
            File::deleteDirectory(public_path('plugins/'.strtolower($pluginName)));
        }
        self::PutPlugin($pluginName, false);
    }

    public static function Delete($pluginName)
    {
        self::Disable($pluginName);
        File::deleteDirectory(self::Path($pluginName));
        return true;
    }

    public static function InstallFromFile($file, $deleteAfter = true)
    {
        if(!File::exists($file)) { return false;}
        if(File::extension($file) != 'zip') { return false; }
        $tempName = \Str::uuid();
        $tempDirectory = File::ensureDirectoryExists(storage_path($tempName));
        $files = (new Unzip())->extract($file, storage_path($tempName));
        if(count($files) == 0 || !\Str::endsWith($files[0],'/'))
        {
            File::deleteDirectory(storage_path($tempName));
            return false;
        }
        $pluginName = trim($files[0], '/');
        if(!in_array($pluginName.'/plugin.json', $files))
        {
            File::deleteDirectory(storage_path($tempName));
            return false;
        }


        File::copyDirectory(storage_path($tempName), self::GetBasePath());
        File::deleteDirectory(storage_path($tempName));
        return true;
    }

    protected static function EnsurePluginsListExists()
    {
        if(!File::exists(base_path('plugins.json')))
        {
            $contents = new stdClass;
            File::put(base_path('plugins.json'), json_encode($contents, JSON_PRETTY_PRINT));
        }
    }

    protected static function PutPlugin($pluginName, $status = false)
    {
        self::EnsurePluginsListExists();
        $contents = json_decode(file_get_contents(base_path('plugins.json')));
        if($contents == null) { $contents = new stdClass; }
        $contents->{$pluginName} = $status === null ? self::IsEnabled($pluginName) : $status;
        File::put(base_path('plugins.json'), json_encode($contents, JSON_PRETTY_PRINT));
    }

    public static function PutFile($pluginName, $filename, $contents)
    {
        File::ensureDirectoryExists(self::Path($pluginName, dirname($filename)));
        $filePath = self::Path($pluginName, $filename);
        File::put($filePath, $contents);
        return $filePath;
    }

    public static function PutDirectory($pluginName, $path, $keep = false)
    {
        File::ensureDirectoryExists(self::Path($pluginName, $path));
        if($keep)
        {
            self::PutFile($pluginName, rtrim($path,'/').'/'.'.gitkeep', '');
        }
    }
}
