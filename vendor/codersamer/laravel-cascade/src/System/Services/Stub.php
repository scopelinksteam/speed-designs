<?php

namespace Cascade\System\Services;

use Nwidart\Modules\Support\Stub as SupportStub;

class Stub extends SupportStub
{

    /**
     * Get stub path.
     *
     * @return string
     */
    public function getPath()
    {
        $path = static::getBasePath() . $this->path;

        return file_exists($path) ? $path : __DIR__ . '/../Commands/stubs' . $this->path;
    }

    /**
     * Get base path.
     *
     * @return string|null
     */
    public static function getBasePath()
    {
        return module_path('System', 'Stubs').'/';
    }
}
