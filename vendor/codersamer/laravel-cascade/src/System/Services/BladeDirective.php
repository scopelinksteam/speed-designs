<?php

namespace Cascade\System\Services;

use Closure;

class BladeDirective
{

    public static function of(String $class)
    {
        if(class_exists($class))
        {
            return fn($expression) => (new $class)->handle($expression);
        }
        return fn($expression) => '';
    }

    public function output(\Illuminate\Contracts\View\View|\Illuminate\Contracts\View\Factory|String $output) : String
    {
        $output = is_string($output) ? $output : $output->toHtml();
        $finalOutput = '<?php '."\n";
        $finalOutput .= "echo '".addcslashes($output, "'")."';\n";
        $finalOutput .= '?>';
        return $finalOutput;
    }

    public function call($code)
    {
        $finalOutput = '<?php '."\n";
        $finalOutput .= "echo eval($code);"."\n";
        $finalOutput .= '?>\n';
        return $finalOutput;
    }

    public function handle(String $expression) { return ''; }

}
