<?php

namespace Cascade\System\DTO;

use Cascade\System\Contracts\IHTMLBuilder;
use Cascade\System\Enums\EntityCardPlacement;
use Cascade\System\Traits\HasHTMLBuilder;

class EntityCard
{

    use HasHTMLBuilder;

    protected String $id = '';

    protected EntityCardPlacement $placement = EntityCardPlacement::Default;

    protected ?String $title = null;

    protected ?String $view = null;

    protected ?String $icon = null;

    protected ?IHTMLBuilder $builder = null;

    //protected array

    protected function __construct(String $id)
    {
        $this->id = $id;
    }

    public static function make(String $id)
    {
        return new static($id);
    }

    public function builder(?IHTMLBuilder $builder = null) : null|IHTMLBuilder|static
    {
        if($builder != null) { $this->builder = $builder; return $this; }
        return $this->builder;
    }

    /**
     * Get or Set Card ID
     *
     * @param String|null $id
     * @return String
     */
    public function id(?String $id = null) : String
    {
        if($id != null) { $this->id = $id; return $this; }
        return $this->id;
    }

    /**
     * Get or Set Card ID
     *
     * @param String|null $id
     * @return EntityCardPlacement|static
     */
    public function placement(?EntityCardPlacement $placement = null) : EntityCardPlacement|static
    {
        if($placement != null) { $this->placement = $placement; return $this; }
        return $this->placement;
    }

    /**
     * Get or Set Card Title
     *
     * @param String|null $title
     * @return String
     */
    public function title(?String $title = null) : String
    {
        if($title != null) { $this->title = $title; return $this; }
        return $this->title;
    }

    /**
     * Get or Set Card View
     *
     * @param String|null $view
     * @return String
     */
    public function view(?String $view = null) : String|static
    {
        if($view != null) { $this->view = $view; return $this; }
        return $this->view;
    }

    /**
     * Get or Set Card Icon
     *
     * @param String|null $icon
     * @return String
     */
    public function icon(?String $icon = null) : String
    {
        if($icon != null) { $this->icon = $icon; return $this; }
        return $this->icon;
    }

    public function render(array $params = [])
    {
        if($this->view != null)
        {
            return view($this->view, $params)->toHtml();
        }
        $html = $this->html();
        if($this->builder != null)
        {
            $html = $this->builder->build($html);
        }
        return $html->build();
    }

}
