<?php

namespace Cascade\System\DTO\HTML;

use Cascade\System\Abstraction\HTMLElement;

class Row extends HTMLElement
{

    function render()
    {
        return view('system::html-elements.row');
    }
}
