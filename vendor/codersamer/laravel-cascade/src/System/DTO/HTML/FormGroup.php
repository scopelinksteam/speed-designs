<?php

namespace Cascade\System\DTO\HTML;

class FormGroup extends Div
{
    function __construct()
    {
        $this->class('form-group');
    }
}
