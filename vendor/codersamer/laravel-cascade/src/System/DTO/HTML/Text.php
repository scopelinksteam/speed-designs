<?php

namespace Cascade\System\DTO\HTML;

use Cascade\System\Abstraction\HTMLElement;

class Text extends HTMLElement
{

    protected String $text = '';

    public function __construct(String $text)
    {
        $this->text = $text;
    }

    public function innerText(?String $text = null) : String|static
    {
        if($text != null) { $this->text = $text; return $this; }
        return $this->text;
    }
    function render()
    {
        return view('system::html-elements.text');
    }
}
