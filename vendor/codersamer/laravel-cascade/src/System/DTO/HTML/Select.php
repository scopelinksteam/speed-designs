<?php

namespace Cascade\System\DTO\HTML;

use Cascade\System\Abstraction\HTMLElement;

class Select extends HTMLElement
{
    protected $options = [];

    public function options(array|null $options = null) : array|self
    {
        if($options === null) { return $this->options; }
        $this->options = $options;
        return $this;
    }

    public function render()
    {
        return view('system::html-elements.select');
    }
}
