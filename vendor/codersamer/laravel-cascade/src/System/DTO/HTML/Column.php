<?php

namespace Cascade\System\DTO\HTML;

use Cascade\System\Abstraction\HTMLElement;

class Column extends HTMLElement
{

    protected int $size = 0;

    /**
     * Get or Set Column Size
     *
     * @param int|null $id
     * @return int|self
     */
    public function size(?int $size = null) : int|self
    {
        if($size != null) { $this->size = $size; return $this; }
        return $this->size;
    }

    function render()
    {
        return view('system::html-elements.column');
    }
}
