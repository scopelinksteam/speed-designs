<?php

namespace Cascade\System\DTO\HTML;

use Cascade\System\Abstraction\HTMLElement;

class Image extends HTMLElement
{
    protected ?String $source = null;

    public function source(?String $source = null) : String|self
    {
        if($source != null) { $this->source = $source; return $this; }
        return $this->source;
    }
    function render()
    {
        return view('system::html-elements.image');
    }
}
