<?php

namespace Cascade\System\DTO\HTML;

use Cascade\System\Abstraction\HTMLElement;

class Textarea extends HTMLElement
{

    protected int $cols = 20;
    protected int $rows = 5;

    public function __construct(String $name)
    {
        $this->name($name);
    }

    public function cols(?int $cols = null)
    {
        if($cols !== null) { $this->cols = $cols; return $this; }
        return $this->cols;
    }

    public function rows(?int $rows = null)
    {
        if($rows !== null) { $this->rows = $rows; return $this; }
        return $this->rows;
    }

    public function render()
    {
        return view('system::html-elements.textarea');
    }
}
