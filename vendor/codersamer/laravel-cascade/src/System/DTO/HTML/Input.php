<?php

namespace Cascade\System\DTO\HTML;

use Cascade\System\Abstraction\HTMLElement;
use Cascade\System\Enums\InputType;

class Input extends HTMLElement
{

    public function __construct(protected InputType $type, String $name)
    {
        $this->name($name);
    }

    public function type()
    {
        return $this->type;
    }

    public function render()
    {
        return view('system::html-elements.input');
    }
}
