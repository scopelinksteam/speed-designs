<?php

namespace Cascade\System\DTO\HTML;

use Cascade\System\Abstraction\HTMLElement;

class Label extends HTMLElement
{
    function render()
    {
        return view('system::html-elements.label');
    }
}
