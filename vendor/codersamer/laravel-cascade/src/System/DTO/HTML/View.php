<?php

namespace Cascade\System\DTO\HTML;

use Cascade\System\Abstraction\HTMLElement;

class View extends HTMLElement
{

    protected String $view = '';

    protected array $data = [];

    public function __construct(String $view)
    {
        $this->view = $view;
    }

    public function view(?String $view = null) : String|static
    {
        if($view != null) { $this->text = $view; return $this; }
        return $this->view;
    }

    function render()
    {
        return view($this->view, [
            $this->data
        ]);
    }
}
