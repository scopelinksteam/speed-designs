<?php

namespace Cascade\System\DTO\Database;

use Cascade\System\Services\DatabaseManager;
use Illuminate\Support\Facades\DB;

class Table
{

    protected String $name = '';

    public function __construct(String $tableName)
    {
        $this->name = $tableName;
    }

    public static function make(String $tableName) : static
    {
        return new static($tableName);
    }

    public function describe()
    {
        $data = DB::connection()->select("DESCRIBE ".$this->name);
        return $data;
    }

    public function columns() : array
    {
        $data = static::describe($this->name);
        $columns = [];
        foreach($data as $row)
        {
            $columns[] = $row->Field;
        }
        return $columns;
    }

    public function create()
    {
        $data = DB::connection()->select("SHOW CREATE TABLE ".$this->name);
        return $data;
    }
}
