<?php

namespace Cascade\System\Providers;

use Carbon\Carbon;
use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Blade;
use Cascade\Dashboard\Entities\MenuItem;
use Cascade\Dashboard\Entities\MenuSection;
use Cascade\Dashboard\Enums\ScriptLocation;
use Cascade\Dashboard\Services\DashboardBuilder;
use Cascade\Dashboard\Services\DashboardMenu;
use Cascade\Settings\Entities\Setting;
use Cascade\Settings\Services\Settings;
use Cascade\Site\Services\Site;
use Cascade\System\Channels\WebChannel;
use Cascade\System\Components\EntityCardsComponent;
use Cascade\System\Components\FontAwesomeSelectComponent;
use Cascade\System\Components\HelpComponent;
use Cascade\System\Components\IconSelectComponent;
use Cascade\System\Components\LocationPickerComponent;
use Cascade\System\Components\Mail\ButtonMailComponent;
use Cascade\System\Components\Mail\HeadingMailComponent;
use Cascade\System\Components\Mail\LineMailComponent;
use Cascade\System\Components\Mail\MessageMailComponent;
use Cascade\System\Components\ReCaptchaComponent;
use Cascade\System\Composers\SystemViewComposer;
use Cascade\System\Composers\SystemViewExtensionComposer;
use Cascade\System\Console\CascadeFixPublicCommand;
use Cascade\System\Console\CascadeInstallCommand;
use Cascade\System\Console\CascadeRegisterModulesCommand;
use Cascade\System\Console\MakePluginCommand;
use Cascade\System\Console\MakePluginControllerCommand;
use Cascade\System\Console\MakePluginMigrationCommand;
use Cascade\System\Console\MakePluginModelCommand;
use Cascade\System\Console\TranslateModuleCommand;
use Cascade\System\Console\TranslatePluginCommand;
use Cascade\System\Console\UpdatePluginCommand;
use Cascade\System\Directives\ActionDirective;
use Cascade\System\Directives\CardsDirective;
use Cascade\System\Entities\Currency;
use Cascade\System\Entities\EmailTemplate;
use Cascade\System\Enums\HelpModes;
use Cascade\System\Http\Middleware\ApplyCurrencyMiddleware;
use Cascade\System\Http\Middleware\SystemInjectionMiddleware;
use Cascade\System\Icons\BootstrapIcons;
use Cascade\System\Icons\ElegantIcons;
use Cascade\System\Icons\FeatherIcons;
use Cascade\System\Icons\FontAwesomeBrandIcons;
use Cascade\System\Icons\FontAwesomeRegularIcons;
use Cascade\System\Icons\FontAwesomeSolidIcons;
use Cascade\System\Icons\FoundationIcons;
use Cascade\System\Icons\HappyIcons;
use Cascade\System\Icons\IcoMoonIcons;
use Cascade\System\Icons\OpenIconicIcons;
use Cascade\System\Icons\TablerIcons;
use Cascade\System\Icons\WeatherIcons;
use Cascade\System\Icons\ZondIcons;
use Cascade\System\Services\AssetsManager;
use Cascade\System\Services\EmailTemplateComposer;
use Cascade\System\Services\EntityBuilder;
use Cascade\System\Services\GoogleMaps;
use Cascade\System\Services\Help;
use Cascade\System\Services\Icons;
use Cascade\System\Services\NotificationService;
use Cascade\System\Services\PersonType;
use Cascade\System\Services\Plugins;
use Cascade\System\Services\System;
use Cascade\Tenancy\Enums\TenantMode;
use Cascade\Tenancy\Services\TenancyService;
use Cascade\Translations\Entities\Language;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Container\Container;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;

class SystemServiceProvider extends ServiceProvider
{
    /**
     * @var string $moduleName
     */
    protected $moduleName = 'System';

    /**
     * @var string $moduleNameLower
     */
    protected $moduleNameLower = 'system';

    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        if(!$this->app->runningInConsole())
        {

            $this->registerTranslations();
            $this->registerSettings();
            $this->registerComponents();
            $this->registerMenu();
            $this->applySettings();
            $this->registerAssets();
            $this->registerMiddlewares();
            AssetsManager::InitializeDisks();
            View::composer('*', SystemViewExtensionComposer::class);
            View::composer('*', SystemViewComposer::class);
            //Add Google Maps
            GoogleMaps::RegisterFrontend();
        }
        $this->registerConfig();
        $this->registerViews();
        $this->loadMigrationsFrom(module_path($this->moduleName, 'Database/Migrations'));
        $this->registerCommands();
        $this->applyMigrations();
    }

    /**
     * Register Module Middlewares
     *
     * @return void
     */
    public function registerMiddlewares()
    {
        $this->app['router']->pushMiddlewareToGroup('web', ApplyCurrencyMiddleware::class);
    }

    protected function RegisterIcons()
    {
        Icons::Register(__('Bootstrap Icons'), 'bootstrap-icons', new BootstrapIcons);
        Icons::Register(__('Elegate Icons'), 'elegant-icons', new ElegantIcons);
        Icons::Register(__('Feather Icons'), 'feather-icons', new FeatherIcons);
        Icons::Register(__('Font Awesome Brands Icons'), 'fontawesome-brands-icons', new FontAwesomeBrandIcons);
        Icons::Register(__('Font Awesome Regular Icons'), 'fontawesome-regular-icons', new FontAwesomeRegularIcons);
        Icons::Register(__('Font Awesome Solid Icons'), 'fontawesome-solid-icons', new FontAwesomeSolidIcons);
        Icons::Register(__('Foundation Icons'), 'foundation-icons', new FoundationIcons);
        Icons::Register(__('Happy Icons'), 'happy-icons', new HappyIcons);
        Icons::Register(__('IcoMoon Icons'), 'icomoon-icons', new IcoMoonIcons);
        Icons::Register(__('Open Iconic Icons'), 'openiconic-icons', new OpenIconicIcons);
        Icons::Register(__('Tabler Icons'), 'tabler-icons', new TablerIcons);
        Icons::Register(__('Weather Icons'), 'weather-icons', new WeatherIcons);
        Icons::Register(__('Zond Icons'), 'zond-icons', new ZondIcons);
    }

    protected function registerAssets()
    {


        $iconsLibraries = [];
        foreach(Icons::GetActiveIcons() as $key => $icon)
        {
            DashboardBuilder::RegisterStyle($icon->library->style());
            //Site::AddStyle($icon->library->style());
            $iconsLibraries[] = $icon->library->json();
        }
        DashboardBuilder::AddJavascriptConstant('icon_libraries', implode(',', $iconsLibraries));
        //DashboardBuilder::RegisterStyle(cascade_asset('plugins/icon-picker/themes/bootstrap-5.min.css'));
        DashboardBuilder::InjectScript(cascade_asset('plugins/icon-picker/js/universal-icon-picker.min.js'), ScriptLocation::BeforeLibraries);
        $recaptchaSiteKey = get_setting('integrations::recaptcha.credentials.site_key', '', false);
        if(!empty($recaptchaSiteKey))
        {
            DashboardBuilder::InjectScript('https://www.google.com/recaptcha/api.js?render='.$recaptchaSiteKey, ScriptLocation::BeforeLibraries);
            Site::AddScript('https://www.google.com/recaptcha/api.js?render='.$recaptchaSiteKey, 'recaptcha-script');
        }
    }

    public function applyMigrations()
    {
        if(TenancyService::ActiveMode() == TenantMode::Tenant)
        {
            Artisan::call('module:migrate');
        }
    }

    protected function registerEmailTemplates()
    {
        if($this->app->runningInConsole()) { return; }
        EmailTemplateComposer::globalPlaceholder('recipient_name', __('Recipient Name'), function(EmailTemplateComposer $composer){
            return $composer->name();
        });
        EmailTemplateComposer::globalPlaceholder('recipient_email', __('Recipient Email'), function(EmailTemplateComposer $composer){
            return $composer->email();
        });
        EmailTemplate::register(
            EmailTemplateComposer::make(System::class, [
                'system-maintenance' => __('System Maintenance Template'),
                'system-running' => __('System Running Template'),
            ], [
                'system-maintenance' => __('System is on Scheduled Maintenance'),
                'system-running' => __('System is currently up and Running')
            ])
            ->title(__('System Email Templates'))
            ->description(__('Manage and Edit System Email Templates'))
        );

    }


    protected function registerCommands()
    {
        $this->commands([
            CascadeInstallCommand::class,
            CascadeFixPublicCommand::class,
            CascadeRegisterModulesCommand::class,
            MakePluginCommand::class,
            MakePluginControllerCommand::class,
            MakePluginModelCommand::class,
            MakePluginMigrationCommand::class,
            TranslatePluginCommand::class,
            TranslateModuleCommand::class,
            UpdatePluginCommand::class
        ]);
    }

    /**
     * Register Macros found in ../Macros Directory
     *
     * @return void
     */
    public function registerMacros()
    {
        $macros = array_diff(scandir(__DIR__ . '/../Macros/'), ['.', '..']);
        foreach ($macros as $macro) {
            require_once(__DIR__ . '/../Macros/' . $macro);
        }
    }

    /**
     * Register Module Menu
     *
     * @return void
     */
    protected function registerMenu()
    {
        $items = [];

        if(modules_manager_enabled())
        {
            $items[] = MenuItem::make(__('Modules'))
                        ->route('dashboard.system.modules.index')
                        ->description(__('Manage Installed Modules'))
                        ->permission('view modules')
                        ->icon('cubes');
        }
        if(plugins_manager_enabled())
        {
            $items[] = MenuItem::make(__('Plugins'))
                        ->route('dashboard.system.plugins.index')
                        ->description(__('Manage Installed Plugins'))
                        ->permission('view plugins')
                        ->icon('plug');
        }
        if(files_manager_enabled())
        {
            $items[] = MenuItem::make(__('Files Manager'))
            ->route('dashboard.system.files.manager')
            ->description(__('Files Uploaded to System'))
            ->permission('manage files')
            ->icon('folder');
        }

        $items[] = MenuItem::make(__('Currencies'))
        ->route('dashboard.system.currencies.index')
        ->description(__('System Available Currencies'))
        ->permission('view currencies')
        ->icon('dollar-sign');

        $items[] = MenuItem::make(__('Cache Cleaner'))
        ->route('dashboard.system.cache.clear')
        ->description(__('Clear Cached Files'))
        ->permission('clear cache')
        ->icon('hdd');

        $items[] = MenuItem::make(__('Email Templates'))
        ->route('dashboard.system.email.templates.index')
        ->description(__('Customize System Emails'))
        ->permission('view email templates')
        ->icon('envelope-open-text');

        $items[] = MenuItem::make(__('Geo Management'))
        ->route('dashboard.system.geo.countries.index')
        ->description('Manage Countries and Cities')
        ->permission('manage geo')
        ->icon('globe');
        $items[] = MenuItem::make(__('Rebuild System'))
        ->route('dashboard.system.rebuild')
        ->description('Rebuild Configurations and Files')
        ->permission('rebuild system')
        ->icon('rotate');

        if(count($items) > 0)
        {
            DashboardBuilder::Tenant(function() use ($items){
                DashboardBuilder::Menu('system-overview')->Call(function(DashboardMenu $menu) use($items){
                    $menu->AddItems($items);
                });
            });
            DashboardBuilder::Central(function() use ($items){
                DashboardBuilder::Menu('system-overview')->Call(function(DashboardMenu $menu) use($items){
                    $menu->AddItems($items);
                });
            });
        }
        DashboardBuilder::Tenant(function(){
            DashboardBuilder::MainMenu()->AddSection(MenuSection::make('system', __('System'),  1000));
            DashboardBuilder::MainMenu()->Call(function(DashboardMenu $menu){

                $menu->AddItem(
                    MenuItem::make(__('Control Panel'),'system')->route('dashboard.system.manager.index')->icon('cogs')
                );
            });
        });
        DashboardBuilder::Central(function(){
            DashboardBuilder::MainMenu()->AddSection(MenuSection::make('system', __('System'),  1000));
            DashboardBuilder::MainMenu()->Call(function(DashboardMenu $menu){
                $menu->AddItem(
                    MenuItem::make(__('Control Panel'),'system')->route('dashboard.system.manager.index')->icon('cogs')
                );
            });
        });




    }

    public function registerComponents()
    {
        Blade::component(IconSelectComponent::class, 'icon-select');
        Blade::component(FontAwesomeSelectComponent::class, 'fontawesome-select');
        Blade::component(ReCaptchaComponent::class, 'recaptcha');
        Blade::component(HelpComponent::class, 'help');

        //Mail Components
        Blade::component(MessageMailComponent::class, 'mail.message');
        Blade::component(HeadingMailComponent::class, 'mail.heading');
        Blade::component(LineMailComponent::class, 'mail.line');
        Blade::component(ButtonMailComponent::class, 'mail.button');
        Blade::component(LocationPickerComponent::class, 'location-picker');
    }

    public function registerSettings()
    {
        //Register Settings Placeholders
        $this->app->booted(function(){
            Settings::module('modules', __('Modules'), fn() => null);
            Settings::module('app', __('Application'), fn() => null);
            Settings::module('plugins', __('Plugins'), fn() => null);
            Settings::module('themes', __('Themes'), fn() => null);
            Settings::module('integrations', __('Integrations'), fn() => null);
        });

        Settings::on('integrations', function(){
            Settings::page(__('ReCaptcha V3'), 'recaptcha', function(){
                Settings::group(__('Credentials'), function(){
                    Settings::note(__('To Get Google Map API Keys Follow the following Link').' : '.'<a href="https://www.google.com/recaptcha/admin/create">'.__('Click Here').'</a>');
                    Settings::text(__('API Key'), 'credentials.site_key');
                    Settings::text(__('Secret Key'), 'credentials.secret_key');
                });
            });
            Settings::page(__('Google Maps'), 'gmap', function(){
                Settings::group(__('Credentials'), function(){
                    Settings::note(__('To Get Recaptcha Keys Follow the following Link').' : '.'<a href="https://console.cloud.google.com/project/_/google/maps-apis/credentials">'.__('Click Here').'</a>');
                    Settings::text(__('API Key'), 'credentials.api_key');
                    Settings::text(__('Secret Key'), 'credentials.secret_key');
                });
            });
        });

        Settings::module('system', __('System'), function(){
            Settings::page(__('General Settings'), 'general', function(){
                Settings::group(__('App Settings'), function(){
                    Settings::text(__('App Name'), 'app_name', true, 
                        Help::make(HelpModes::Modal)->title(__('Application Name'))->description(__('Name that displays only on your dashboard.'))
                    );
                    Settings::text(__('App Description'), 'app_description', true);
                    Settings::image(__('Logo'), 'logo');
                    Settings::image(__('Favicon'), 'favicon');
                });
            });

            Settings::page(__('Services'), 'services', function(){
                Settings::group(__('Icons Libraries'), function(){
                    Settings::choices(__('Libraries'), 'icons-libraries', function(){
                        $data = [];
                        foreach(Icons::GetIcons() as $key => $icon)
                        {
                            $data[$key] = $icon->name;
                        }
                        return $data;
                    });
                });
            });
            Settings::page(__('Notifications'), 'notifications', function(){
                Settings::group(__('Channels'), function(){
                    foreach(NotificationService::Notifications() as $notification)
                    {
                        Settings::multiple($notification->name(), $notification->slug().'.channels', function(){
                            return [WebChannel::class => __('Web'), 'mail' => 'Mail'];
                        });
                    }
                });
            });
            Settings::page(__('Mail Settings'), 'mail', function(){
                Settings::group(__('SMTP Server'), function(){
                    Settings::text(__('Primary Email'), 'primary');
                    Settings::text(__('Host'), 'host');
                    Settings::text(__('Username'), 'username');
                    Settings::password(__('Password'), 'password');
                    Settings::text(__('Port'), 'port');
                    Settings::select(__('Encryption'), 'encryption', function(){
                        return [
                            'tls' => __('TLS'), 'ssl' => __('SSL'), 'startlts' => __('StartTLS')
                        ];
                    });
                    Settings::email(__('From Email'), 'from_address');
                    Settings::text(__('From Name'), 'from_name');
                });
            });
            Settings::page(__('Localization'), 'localization', function(){
                if(Language::count() > 0)
                {
                    Settings::group(__('Languages'), function(){
                        Settings::select(__('Default Language'), 'languages.default', fn() => Language::all()->pluck('name', 'locale'));
                    });
                }
                if(Currency::count() > 0)
                {
                    Settings::group(__('Currency'), function(){
                        Settings::select(__('Default Currency'), 'currencies.default', fn() => Currency::all()->pluck('name', 'id'));
                    });
                }
                Settings::group(__('Date & Time'), function(){
                    Settings::select(__('Week Start'), 'week_start', fn() =>[
                        0 => __('Sunday'),
                        1 => __('Monday'),
                        2 => __('Tuesday'),
                        3 => __('Wednesday'),
                        4 => __('Thursday'),
                        5 => __('Friday'),
                        6 => __('Saturday'),
                    ]);
                    Settings::select(__('Timezone'), 'timezone', fn() => timezones());
                    Settings::select(__('Date Format'), 'date_format', fn() => [
                        'Y-m-d' => '( Y-m-d ) '. now()->format('Y-m-d'),
                        'd-m-Y' => '( d-m-Y ) '. now()->format('d-m-Y'),
                        'D d, M Y' => '( D d, M Y ) '. now()->format('D d, M Y'),
                        'Y/m/d' => '( Y/m/d ) '. now()->format('Y/m/d'),
                        'd/m/Y' => '( d/m/Y ) '. now()->format('d/m/Y'),
                        'd, M Y' => '( d, M Y ) '. now()->format('d, M Y'),
                        'M, d Y' => '( M, d Y ) '. now()->format('M, d Y'),
                        'Y, M, d' => '( Y, M, d ) '. now()->format('Y, M, d'),
                    ]);
                    Settings::select(__('Time Format'), 'time_format', fn() => [
                        'h:i:s' => '( h:i:s ) '. now()->format('h:i:s'),
                        'h:i:s a' => '( h:i:s a ) '. now()->format('h:i:s a'),
                        'h:i:s A' => '( h:i:s A ) '. now()->format('h:i:s A'),
                        'h:i' => '( h:i ) '. now()->format('h:i'),
                        'h:i a' => '( h:i a ) '. now()->format('h:i a'),
                        'h:i A' => '( h:i A ) '. now()->format('h:i A'),
                        'H:i:s' => '( H:i:s ) '. now()->format('H:i:s'),
                        'H:i' => '( H:i ) '. now()->format('H:i'),
                    ]);
                });
            });
        });

    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
        $router = $this->app['router'];
        $router->pushMiddlewareToGroup('web', SystemInjectionMiddleware::class);
        $router->pushMiddlewareToGroup('api', SystemInjectionMiddleware::class);
        System::BladeDirective('doAction', ActionDirective::class);
        Blade::component(EntityCardsComponent::class, 'entity-cards');
        $this->RegisterIcons();
        $this->registerEmailTemplates();
        $this->registerMacros();
        DashboardBuilder::AddMenu(DashboardMenu::make('system-overview'));
        DashboardBuilder::Tenant(function(){
            DashboardBuilder::Menu('system-overview')->view('system::dashboard.components.system-overview-menu');
        });
        DashboardBuilder::Central(function(){
            DashboardBuilder::Menu('system-overview')->view('system::dashboard.components.system-overview-menu');
        });
         
        //Inject Plugins
        $plugins = Plugins::GetPlugins();
        foreach($plugins as $plugin)
        {
            if(!$plugin->IsEnabled()) { continue; }
            foreach($plugin->GetProviders() as $provider)
            {
                if(class_exists($provider)) { $this->app->register($provider); }
            }
        }
        $this->booting(function(){
            $services = System::GetServices();
            foreach($services as $service)
            {
                foreach($service->services($this->app) as $childService) { System::EnableService($childService); }
            }
            $services = System::GetServices();
            foreach($services as $service) { $service->register($this->app); }
        });

        $this->booted(function(){
            $services = System::GetServices();
            foreach($services as $service) { $service->boot($this->app); }
        });
    }

    protected function applySettings()
    {


        $timezone = get_setting('system::localization.timezone', null);
        if($timezone !== null)
        {
            config(['app.timezone' => $timezone]);
            $this->app->bind(Carbon::class, fn(Container $container) => new Carbon('now', $timezone));
            date_default_timezone_set($timezone);
        }

        //Mail Settings
        config(['mail.mailers.smtp.host' => get_setting('system::mail.host')]);
        config(['mail.mailers.smtp.username' => get_setting('system::mail.username')]);
        config(['mail.mailers.smtp.password' => get_setting('system::mail.password')]);
        config(['mail.mailers.smtp.port' => get_setting('system::mail.port')]);
        config(['mail.mailers.smtp.encryption' => get_setting('system::mail.encryption')]);
        config(['mail.from.address' => get_setting('system::mail.from_address')]);
        config(['mail.from.name' => get_setting('system::mail.from_name')]);
        config(['app.name' => get_setting('system::general.app_name')]);

    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->publishes([
            module_path($this->moduleName, 'Config/config.php') => config_path($this->moduleNameLower . '.php'),
        ], 'config');
        $this->mergeConfigFrom(
            module_path($this->moduleName, 'Config/config.php'), $this->moduleNameLower
        );
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $viewPath = resource_path('views/modules/' . $this->moduleNameLower);

        $sourcePath = module_path($this->moduleName, 'Resources/views');

        $this->publishes([
            $sourcePath => $viewPath
        ], ['views', $this->moduleNameLower . '-module-views']);

        $this->loadViewsFrom(array_merge($this->getPublishableViewPaths(), [$sourcePath]), $this->moduleNameLower);
    }

    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $langPath = resource_path('lang/modules/' . $this->moduleNameLower);

        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, $this->moduleNameLower);
        } else {
            $this->loadTranslationsFrom(module_path($this->moduleName, 'Resources/lang'), $this->moduleNameLower);
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }

    private function getPublishableViewPaths(): array
    {
        $paths = [];
        foreach (\Config::get('view.paths') as $path) {
            if (is_dir($path . '/modules/' . $this->moduleNameLower)) {
                $paths[] = $path . '/modules/' . $this->moduleNameLower;
            }
        }
        return $paths;
    }
}
