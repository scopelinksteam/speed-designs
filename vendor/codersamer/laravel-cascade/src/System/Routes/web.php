<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Cascade\System\Entities\Currency;
use Cascade\System\Http\Controllers\Dashboard\AreasController;
use Cascade\System\Http\Controllers\Dashboard\CitiesController;
use Cascade\System\Http\Controllers\Dashboard\CountriesController;
use Cascade\System\Http\Controllers\Dashboard\CurrenciesController;
use Cascade\System\Http\Controllers\Dashboard\FilesManagerController;
use Illuminate\Support\Facades\Route;
use Cascade\System\Http\Controllers\Dashboard\ModulesController;
use Cascade\System\Http\Controllers\Dashboard\PluginsController;
use Cascade\System\Http\Controllers\Dashboard\RemoveMemberController;
use Cascade\System\Http\Controllers\Dashboard\SystemManagerController;
use Cascade\System\Http\Controllers\Frontend\CommentsController;
use Cascade\System\Services\System;
use Cascade\System\Http\Controllers\Dashboard\TaxesController;
use Cascade\Tenancy\Services\TenancyService;
use Illuminate\Support\Facades\Artisan;

Route::get('system/cron', function(){
    set_setting('system::cron.last_run', time(), false);
    Artisan::call('schedule:run');
    Artisan::call('queue:work --max-time=25 --stop-when-empty');
});

Route::get('system/migrate', function(){
    Artisan::call('module:migrate');
});

Route::get('/currency/{currency}', function(Currency $currency){
    session(['currency' => $currency->id]);
    if(auth()->check())
    {
        $user = auth()->user();
        $user->currency_id = $currency->id;
        $user->save();
    }
    return back();
})->name('system.set_currency');

Route::hybrid(function(){
    if(modules_manager_enabled())
    {
        Route::prefix('modules')->as('modules.')->group(function(){
            Route::get('/', [ModulesController::class, 'index'])->name('index')->middleware('can:view modules');
            Route::get('/enable/{module}', [ModulesController::class, 'enable'])->name('enable')->middleware('can:enable modules');
            Route::get('/disable/{module}', [ModulesController::class, 'disable'])->name('disable')->middleware('can:disable modules');
        });
    }
    if(plugins_manager_enabled())
    {
        Route::prefix('plugins')->as('plugins.')->group(function(){
            Route::get('/', [PluginsController::class, 'index'])->name('index')->middleware('can:view plugins');
            Route::get('/enable/{plugin}', [PluginsController::class, 'enable'])->name('enable')->middleware('can:enable plugins');
            Route::get('/disable/{plugin}', [PluginsController::class, 'disable'])->name('disable')->middleware('can:disable plugins');
            Route::get('/delete/{plugin}', [PluginsController::class, 'delete'])->name('delete')->middleware('can:delete plugins');
            Route::get('/screenshot/{plugin}', [PluginsController::class, 'screenshot'])->name('screenshot');
            Route::post('/create', [PluginsController::class, 'postCreate'])->name('create')->middleware('can:create plugins');
            Route::post('/install', [PluginsController::class, 'postInstall'])->name('install')->middleware('can:install plugins');
        });
    }

    Route::prefix('currencies')->as('currencies.')->group(function(){
        Route::get('/', [CurrenciesController::class, 'index'])->name('index');
        Route::post('/', [CurrenciesController::class, 'store'])->name('store');
        Route::get('/edit/{currency}', [CurrenciesController::class, 'edit'])->name('edit');
        Route::get('/delete/{currency}', [CurrenciesController::class, 'destroy'])->name('delete');
    });

    Route::prefix('files')->as('files.')->group(function(){
        Route::get('/', [FilesManagerController::class, 'index'])->name('manager');
    });

    Route::prefix('manager')->as('manager.')->group(function(){
        Route::get('/', [SystemManagerController::class, 'index'])->name('index');

    });

    Route::prefix('cache')->as('cache.')->group(function(){
        Route::get('/clear', [SystemManagerController::class, 'clearCache'])->name('clear');
    });

    Route::get('/rebuild', [SystemManagerController::class, 'rebuildSystem'])->name('rebuild');

    Route::prefix('email/templates')->as('email.templates.')->group(function(){
        Route::EmailTemplates(System::class);
    });
    Route::get('debug', function(){
        dd(get_featured_blog_posts());
    });

    Route::prefix('taxes')->as('taxes.')->group(function(){
        Route::get('/', [TaxesController::class, 'index'])->name('index');
        Route::post('/', [TaxesController::class, 'store'])->name('store');
        Route::get('/edit/{tax}', [TaxesController::class, 'edit'])->name('edit');
        Route::get('/delete/{tax}', [TaxesController::class, 'destroy'])->name('destroy');
    });
}, 'system');

Route::dashboard(function(){
    Route::prefix('geo')->name('geo.')->group(function(){
        Route::prefix('countries')->name('countries.')->group(function(){
            Route::get('/', [CountriesController::class, 'index'])->name('index');
            Route::get('/edit/{country}', [CountriesController::class, 'edit'])->name('edit');
            Route::get('/delete/{country}', [CountriesController::class, 'destroy'])->name('delete');
            Route::post('/', [CountriesController::class, 'store'])->name('store');
        });
        Route::prefix('cities')->name('cities.')->group(function(){
            Route::get('/', [CitiesController::class, 'index'])->name('index');
            Route::get('/edit/{city}', [CitiesController::class, 'edit'])->name('edit');
            Route::get('/delete/{city}', [CitiesController::class, 'destroy'])->name('delete');
            Route::post('/', [CitiesController::class, 'store'])->name('store');
        });
        Route::prefix('areas')->name('areas.')->group(function(){
            Route::get('/', [AreasController::class, 'index'])->name('index');
            Route::get('/edit/{area}', [AreasController::class, 'edit'])->name('edit');
            Route::get('/delete/{area}', [AreasController::class, 'destroy'])->name('delete');
            Route::post('/', [AreasController::class, 'store'])->name('store');
        });
    });

    Route::prefix('members')->name('members.')->group(function(){
        Route::get('/remove/{member}', [RemoveMemberController::class, 'destroy'])->name('remove');
    });
}, 'system');

Route::prefix('system/comments')->name('system.comments.')->group(function(){
    Route::post('/', [CommentsController::class, 'StoreComment'])->name('comment');
});

