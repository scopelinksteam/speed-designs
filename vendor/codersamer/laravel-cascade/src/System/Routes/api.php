<?php

use Cascade\System\Http\Controllers\API\AreasController;
use Cascade\System\Http\Controllers\API\CategoriesController;
use Cascade\System\Http\Controllers\API\CitiesController;
use Cascade\System\Http\Controllers\API\CountriesController;
use Cascade\System\Http\Controllers\API\CurrenciesController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->group(function() {

});

Route::ApiGuest(function(){
    //Geo - Guest
    Route::prefix('geo')->name('geo.')->group(function(){
        Route::prefix('countries')->name('countries.')->group(function(){
            Route::get('/', [CountriesController::class, 'list'])->name('list');
        });

        Route::prefix('cities')->name('cities.')->group(function(){
            Route::get('/', [CitiesController::class, 'list'])->name('list');
        });
        Route::prefix('areas')->name('areas.')->group(function(){
            Route::get('/', [AreasController::class, 'list'])->name('list');
        });
    });

    Route::prefix('currencies')->name('currencies.')->group(function(){
        Route::get('/', [CurrenciesController::class, 'list'])->name('list');
    });

    //Geo - Guest
    Route::prefix('categories')->name('categories.')->group(function(){
        Route::get('/', [CategoriesController::class, 'list'])->name('list');
    });
}, 'system');
