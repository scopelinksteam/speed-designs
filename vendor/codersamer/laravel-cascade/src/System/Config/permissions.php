<?php

return [
    'view modules' => 'View installed modules',
    'enable modules' => 'Enable installed modules',
    'disable modules' => 'Disable Enabled Modules',
    'view plugins' => 'View Installed Plugins',
    'enable plugins' => 'Enable Installed Plugins',
    'disable plugins' => 'Disable Enabled Plugins',
    'delete plugins' => 'Delete Existing Plugins',
    'create plugins' => 'Create new Plugins',
    'install plugins' => 'Install new Plugins',

    //Currencies
    'view currencies' => 'View Available Currencies',
    'edit currencies' => 'Edit Existing Currencies',
    'create currencies' => 'Create new Currencies',
    'delete currencies' => 'Delete Existing Currencies',

    //Cache
    'clear cache' => 'Clear Cached Files',

    //Geo Management
    'view area' => 'Allow user to View Areas',
    'create area' => 'Allow user to Create new Areas',
    'edit area' => 'Allow user to Edit Areas',
    'delete area' => 'Allow user to Delete Areas',
    'view city' => 'Allow user to View Cities',
    'create city' => 'Allow user to Create new Cities',
    'edit city' => 'Allow user to Edit Cities',
    'delete city' => 'Allow user to Delete Cities',
    'view country' => 'Allow user to View Countries',
    'create country' => 'Allow user to Create new Countries',
    'edit country' => 'Allow user to Edit Countries',
    'delete country' => 'Allow user to Delete Countries',
];
