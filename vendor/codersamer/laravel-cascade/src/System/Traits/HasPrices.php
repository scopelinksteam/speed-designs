<?php

namespace Cascade\System\Traits;

trait HasPrices
{
    public function GetPriceValue($priceName = 'price')
    {
        $amount = floatval($this->attributes[$priceName] ?? 0);
        return currency()->exchange($amount);
    }
}