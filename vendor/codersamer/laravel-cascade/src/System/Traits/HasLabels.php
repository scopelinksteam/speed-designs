<?php

namespace Cascade\System\Traits;

use Cascade\System\Entities\Label;

trait HasLabels
{
    public function getLabelAttribute()
    {
        return $this->labels()->first();
    }

    public function labels()
    {
        return $this->morphToMany(Label::class, 'labelizable', 'entities_labels');
    }
}
