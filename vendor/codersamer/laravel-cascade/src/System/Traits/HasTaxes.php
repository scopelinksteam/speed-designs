<?php

namespace Cascade\System\Traits;

use Cascade\System\Entities\Tax;
use Cascade\System\Enums\TaxRateType;
use Cascade\System\Entities\TaxEntry;

trait HasTaxes
{
    public function taxes()
    {
        return $this->morphMany(TaxEntry::class, 'taxable');
    }

    public function AddTax(int|Tax $tax, $rate = null, TaxRateType $type = TaxRateType::None, $reference = null)
    {
        $tax = is_object($tax) ? $tax : Tax::find($tax);
        $taxEntry = new TaxEntry();
        $taxEntry->taxable_type = static::class;
        $taxEntry->taxable_id = $this->id;
        $taxEntry->rate_type = $type == TaxRateType::None ? $tax->rate_type : $type;
        $taxEntry->rate = $rate == null ? $tax->rate : doubleval($rate);
        $taxEntry->tax_id = $tax->id;
        if($reference != null)
        {
            $taxEntry->reference_id = $reference->id ?? 0;
            $taxEntry->reference_type = get_class($reference);
        }
        $taxEntry->save();
        return $taxEntry;
    }

    public function ClearTaxes()
    {
        $this->taxes()->delete();
    }
}
