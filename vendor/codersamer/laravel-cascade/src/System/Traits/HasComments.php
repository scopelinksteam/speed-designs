<?php

namespace Cascade\System\Traits;

use Cascade\System\Entities\Attachment;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Cascade\System\Entities\Comment;

trait HasComments
{
    public function comments() : MorphMany
    {
        return $this->morphMany(Comment::class, 'commentable');
    }

    public function getHasCommentsAttribute()
    {
        return $this->comments()->count() > 0;
    }

    public function comment(Comment $comment) : Comment|bool
    {
        return $this->comments()->save($comment);
    }
}
