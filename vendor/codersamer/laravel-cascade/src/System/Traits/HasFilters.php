<?php

namespace Cascade\System\Traits;

use Closure;
use stdClass;

trait HasFilters
{

    protected static array $filtersHandlers = [];



    public static function OnFilter(String $filter, array|Closure $callback, int $order = 10)
    {
        if(!isset(static::$filtersHandlers[$filter])) { static::$filtersHandlers[$filter] = []; }
        $handler = new stdClass;
        $handler->target    = $filter;
        $handler->order     = $order;
        $handler->callback  = Closure::fromCallable($callback);
        static::$filtersHandlers[$filter][] = $handler;
    }

    public function filter($key)
    {
        if(property_exists($this, 'filterable') && is_array($this->filterable) && in_array($key, $this->filterable))
        {
            return $this->DispatchFilter($key, $this->{$key}, $this);
        }
        return $this->{$key};
    }

    public static function DispatchFilter(String $filter, mixed $object, $owner = null)
    {
        if(!isset(self::$filtersHandlers[$filter])) { return $object; }
        $orderedHandlers = collect(static::$filtersHandlers[$filter])->sort(function($a, $b){
            if($a->order == $b->order) { return 0; }
            return $a->order < $b->order ? -1 : 1;
        });
        foreach($orderedHandlers as $handler)
        {
            $object = call_user_func($handler->callback, ...[$object, $owner]);
        }
        return $object;
    }

}
