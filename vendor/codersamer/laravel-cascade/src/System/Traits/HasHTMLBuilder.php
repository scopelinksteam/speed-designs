<?php

namespace Cascade\System\Traits;

use Cascade\System\Services\HTMLBuilder;

trait HasHTMLBuilder
{

    protected ?HTMLBuilder $htmlBuilder = null;

    public function html() : HTMLBuilder
    {
        if($this->htmlBuilder != null) { return $this->htmlBuilder; }
        return $this->htmlBuilder = new HTMLBuilder;
    }

}
