<?php

namespace Cascade\System\Traits;

use Cascade\System\Entities\EntityAttribute;
use Exception;

trait HasAttributes
{
    public function attributes()
    {
        return $this->morphMany(EntityAttribute::class, 'bindable');
    }

    protected function formatAttributeKey($key) : String
    {
        return (String)(\str($key)->upper()->replace(' ', '_'));
    }

    public function storeAttribute($key, $value)
    {
        $key = $this->formatAttributeKey($key);
        $exist = $this->attributes()->where('key', $key)->first();
        if($exist == null)
        {
            $exist = new EntityAttribute();
            $exist->bindable_type = get_class($this);
            $exist->bindable_id = $this->id;
            $exist->key = $key;
        }
        $exist->value = $value;
        $exist->save();
    }

    public function Attribute($key, $default = null)
    {
        $key = $this->formatAttributeKey($key);
        $exist = $this->attributes()->where('key', $key)->first();
        $value = $exist->value ?? $default;
        try 
        {
            if(is_string($value)) 
            {
                $decodedValue = json_decode($value, true); 
                if(is_array($decodedValue)){ $value = $decodedValue; } 
            }
        }
        catch(Exception $ex) {}
        if(is_array($value) && in_array(app()->getLocale(), array_keys($value)))
        { return $value[app()->getLocale()]; }
        if(!empty($default) && !is_array($default) && is_array($value))
        { return current($value); }
        return $value;
    }
}
