<?php

namespace Cascade\System\Traits;

use Cascade\System\Entities\Address;
use Cascade\System\Entities\Attachment;

trait HasAddresses
{
    public function addresses()
    {
        return $this->morphMany(Address::class, 'addressable');
    }

    public function getHasAddressesAttribute()
    {
        return $this->addresses()->count() > 0;
    }
}
