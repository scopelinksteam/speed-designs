<?php

namespace Cascade\System\Traits;

use Cascade\System\Entities\Member;

trait Joinable
{

    public function members()
    {
        return $this->morphMany(Member::class, 'joinable');
    }

}
