<?php

namespace Cascade\System\Traits;

use Cascade\System\Entities\Category;
use Cascade\System\Entities\Tag;

trait HasTags
{
    public function tags()
    {
        return $this->morphToMany(Tag::class, 'taggable', 'entities_tags');
    }
}
