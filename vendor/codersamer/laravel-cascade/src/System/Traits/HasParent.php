<?php

namespace Cascade\System\Traits;

trait HasParent
{
    public function parent()
    {
        return $this->belongsTo(static::class, 'parent_id');
    }
}
