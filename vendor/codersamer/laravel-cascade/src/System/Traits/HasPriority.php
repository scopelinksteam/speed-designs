<?php

namespace Cascade\System\Traits;

use Cascade\System\Entities\Priority;

trait HasPriority
{
    public function priority()
    {
        return $this->belongsTo(Priority::class, 'priority_id');
    }
}
