<?php

namespace Cascade\System\Traits;

use Cascade\Users\Entities\User;
use Closure;
use Illuminate\Http\RedirectResponse;
use stdClass;

trait HasActions
{

    protected static array $actionsHandlers = [];



    public static function OnAction(String $action, array|Closure|String $callback, int $order = 10)
    {
        if(!isset(static::$actionsHandlers[static::class])){ static::$actionsHandlers[static::class] = []; }
        if(!isset(static::$actionsHandlers[static::class][$action])) { static::$actionsHandlers[static::class][$action] = []; }
        $handler = new stdClass;
        $handler->target    = $action;
        $handler->order     = $order;
        if(is_string($callback) && class_exists($callback))
        {
            $callback = [new $callback($action), 'Handle'];
        }
        $handler->callback  = Closure::fromCallable($callback);
        static::$actionsHandlers[static::class][$action][] = $handler;
    }

    public static function DispatchAction(String $action, ...$args)
    {
        if(!isset(static::$actionsHandlers[static::class][$action])) { return true; }
        $orderedHandlers = collect(static::$actionsHandlers[static::class][$action])->sort(function($a, $b){
            if($a->order == $b->order) { return 0; }
            return $a->order < $b->order ? -1 : 1;
        });
        foreach($orderedHandlers as $handler)
        {
            $shouldContinue = call_user_func($handler->callback, ...$args);
            if($shouldContinue instanceof RedirectResponse)
            {
                $shouldContinue->send();
                exit;
            }
            if($shouldContinue === false) { return false; }
        }
        return true;
    }


}
