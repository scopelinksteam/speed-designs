<?php

namespace Cascade\System\Traits;

use Cascade\System\Entities\Discount;
use Cascade\System\Enums\DiscountType;

trait HasDiscounts
{
    public function discounts()
    {
        return $this->morphMany(Discount::class, 'discountable');
    }

    public function AddDiscount(DiscountType $type, $amount, $reference = null)
    {
        $discount = new Discount();
        $discount->type = $type;
        $discount->amount = doubleval($amount);
        $discount->discountable_type = static::class;
        $discount->discountable_id = $this->id ?? 0;
        if($reference != null)
        {
            $discount->reference_id = $reference->id ?? 0;
            $discount->reference_type = get_class($reference);
        }
        $discount->save();
        return $discount;
    }

    public function ClearDiscounts()
    {
        $this->discounts()->delete();
    }
}
