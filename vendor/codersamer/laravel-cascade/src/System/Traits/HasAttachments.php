<?php

namespace Cascade\System\Traits;

use Cascade\System\Entities\Attachment;

trait HasAttachments
{
    public function attachments()
    {
        return $this->morphMany(Attachment::class, 'attachmentable');
    }

    public function getHasAttachmentsAttribute()
    {
        return $this->attachments()->count() > 0;
    }

    public function attach($file, $identifier = '')
    {
        $attachment = new Attachment();
        $attachment->file = $file->store('/attachments', ['disk' => 'uploads']);;
        $attachment->name = $file->getClientOriginalName();
        $attachment->identifier = $identifier;
        $this->attachments()->save($attachment);
        return $attachment;
    }
}
