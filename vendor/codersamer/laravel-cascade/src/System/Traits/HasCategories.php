<?php

namespace Cascade\System\Traits;

use Cascade\System\Entities\Category;

trait HasCategories
{

    public function getCategoryAttribute()
    {
        return $this->categories()->first();
    }

    public function categories()
    {
        return $this->morphToMany(Category::class, 'categorizable', 'entities_categories');
    }
}
