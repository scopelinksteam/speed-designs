<?php

namespace Cascade\System\Traits;

use Cascade\System\Entities\DatabaseNotification;
use Illuminate\Notifications\Notifiable as NotificationsNotifiable;

trait Notifiable
{
    use NotificationsNotifiable;

    /**
     * Get the entity's notifications.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function notifications()
    {
        return $this->morphMany(DatabaseNotification::class, 'notifiable')->orderBy('created_at', 'desc');
    }
}
