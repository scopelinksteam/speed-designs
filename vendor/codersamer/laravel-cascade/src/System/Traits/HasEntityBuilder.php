<?php

namespace Cascade\System\Traits;

use Cascade\System\Services\EntityBuilder;

trait HasEntityBuilder
{
    /**
     * Get Entity Builder for Called Entity
     *
     * @return EntityBuilder
     */
    public static function  builder() : EntityBuilder
    {
        $recoveredInstance = EntityBuilder::of(static::class);
        return $recoveredInstance == null ? new EntityBuilder(static::class) : $recoveredInstance;
    }
}
