<?php

namespace Cascade\System\Enums;

enum LabelFeatures : String 
{
    case All = 'all';
    case Name = 'name';
    case Color = 'color';
    //TODO: To Be Implemented
    case Icon = 'icon';
    case Status = 'status';
}