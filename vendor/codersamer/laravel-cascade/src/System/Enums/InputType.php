<?php

namespace Cascade\System\Enums;

enum InputType : String
{
    case Text = 'text';
    case Number = 'number';
    case Password = 'password';
    case Email = 'email';
    case Url = 'url';
    case File = 'file';
    case Tel = 'tel';
    case Submit = 'submit';
    case Checkbox = 'checkbox';
}
