<?php

namespace Cascade\System\Enums;

enum EntityAction : String
{
    case Any = 'any';
    case List = 'list';
    case Show = 'show';
    case Create = 'create';
    case Update = 'update';
    case Upsert = 'upsert';
    case Delete = 'delete';
}
