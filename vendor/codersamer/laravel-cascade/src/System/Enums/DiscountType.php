<?php

namespace Cascade\System\Enums;

enum DiscountType : int
{
    case None = 0;
    case Fixed = 1;
    case Percentage = 2;
}
