<?php

namespace Cascade\System\Enums;

enum CategoryStatus : String
{
    case Active = 'active';
    case Inactive = 'inactive';
}
