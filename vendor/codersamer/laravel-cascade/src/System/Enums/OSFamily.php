<?php

namespace Cascade\System\Enums;

enum OSFamily : String
{
    case Unknown    = 'unknown';
    case Windows    = 'Windows';
    case Linux      = 'Linux';
    case MacOS      = 'Darwin';

    public static function CurrentOS() : OSFamily
    {
        if (stripos(PHP_OS, 'win') === 0) { return OSFamily::Windows; }
        elseif (stripos(PHP_OS, 'darwin') === 0) { return OSFamily::MacOS; }
        elseif (stripos(PHP_OS, 'linux') === 0) { return OSFamily::Linux; }
        return OSFamily::Unknown;
    }
}
