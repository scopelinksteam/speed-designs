<?php

namespace Cascade\System\Enums;

enum EntityCardPlacement : String
{
    case Default = 'default';
    case Start = 'start';
    case PrimaryStart = 'primary-start';
    case Primary = 'primary';
    case PrimaryEnd = 'primary-end';
    case SecondaryStart = 'secondary-start';
    case Secondary = 'secondary';
    case SecondaryEnd = 'secondary-end';
    case End = 'end';
}
