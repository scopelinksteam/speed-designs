<?php

namespace Cascade\System\Enums;

enum CommentStatus : int
{
    case Pending = 0;
    case Approved = 1;
    case Rejected = 2;
    case Hidden = 3;
    case Protected = 4;
}
