<?php

namespace Cascade\System\Enums;

enum MemberStatus : int
{
    case Pending = 0;
    case Active = 1;
    case Blocked = 2;
}
