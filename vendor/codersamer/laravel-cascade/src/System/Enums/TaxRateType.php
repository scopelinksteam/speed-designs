<?php

namespace Cascade\System\Enums;

enum TaxRateType : int
{
    case None = 0;
    case Percentage = 1;
    case Fixed = 2;
}
