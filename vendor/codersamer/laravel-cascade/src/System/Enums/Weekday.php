<?php

namespace Cascade\System\Enums;

enum Weekday : int
{
    case Sunday = 0;
    case Monday = 1;
    case Tuesday = 2;
    case Wednesday = 3;
    case Thursday = 4;
    case Friday = 5;
    case Saturday = 6;

    public function translate()
    {
        $value = '';
        switch($this)
        {
            case Weekday::Sunday :      $value = __('Sunday');      break;
            case Weekday::Monday :      $value = __('Monday');      break;
            case Weekday::Tuesday :     $value = __('Tuesday');     break;
            case Weekday::Wednesday :   $value = __('Wednesday');   break;
            case Weekday::Thursday :    $value = __('Thursday');    break;
            case Weekday::Friday :      $value = __('Friday');      break;
            case Weekday::Saturday :    $value = __('Saturday');    break;
        }
        return $value;
    }
}