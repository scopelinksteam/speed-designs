<?php

namespace Cascade\System\Enums;

enum HelpModes : int
{
    case Link  = 0;
    case Tooltip = 1;
    case Youtube = 2;
    case Modal = 3;
    case Scroll = 4;
}