<?php

namespace Cascade\System\Enums;

enum BootstrapColors : String
{
    case Blue = '#0d6efd';
    case Indigo = '#6610f2';
    case Purple = '#6f42c1';
    case Pink = '#d63384';
    case Red = '#dc3545';
    case Orange = '#fd7e14';
    case Yellow = '#ffc107';
    case Green = '#198754';
    case Teal = '#20c997';
    case Cyan = '#0dcaf0';
    case Gray = '#adb5bd';
    case Black = '#000';
    case Dark = '#212529';
    case Light = '#f8f9fa';

}
