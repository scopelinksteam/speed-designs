<?php

namespace Cascade\System\Console;

use Illuminate\Console\Command;
use Cascade\System\Services\Plugins;
use Cascade\System\Services\Stub;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class UpdatePluginCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'plugin:update {plugin}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $pluginName = $this->argument('plugin');
        if(!Plugins::IsExists($pluginName))
        {
            $this->error('Plugin ['.$pluginName.'] Not Found');
            return;
        }
        $plugin = Plugins::Get($pluginName);
        foreach($plugin->GetComposerRequire() as $package => $version)
        {
            passthru("composer2 require {$package}:{$version}");
        }

        foreach($plugin->GetComposerRequireDev() as $package => $version)
        {
            passthru("composer2 require {$package}:{$version} --dev");
        }

        $this->info('Plugin ['.$pluginName.'] Updated Successfully');
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['plugin', InputArgument::REQUIRED, 'Plugin Name' ]
        ];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null],
        ];
    }
}
