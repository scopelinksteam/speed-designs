<?php

namespace Cascade\System\Console;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;
use Cascade\System\Services\Plugins;
use Cascade\System\Services\Stub;
use Cascade\System\Services\System;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class MakePluginCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'plugin:make {plugin}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description.';


    protected $pluginName = '';

    protected $pluginSlug = '';

    protected $pluginPath = '';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->pluginName = $this->argument('plugin');
        $this->pluginSlug = strtolower($this->pluginName);
        $pluginsPath = Plugins::GetBasePath();
        //Ensure Plugins Folder Exists
        File::ensureDirectoryExists($pluginsPath);
        //Generate Plugin Directory
        $this->pluginPath = $pluginsPath.'/'.$this->pluginName;
        File::ensureDirectoryExists($this->pluginPath);
        //Scaffold Plugin
        $this->scaffoldPlugin();

        $this->info('Plugin ['.$this->pluginName.'] Generated Successfully');
    }

    /**
     * Scaffold and Generate Plugin Contents
     *
     * @return void
     *
     */
    protected function scaffoldPlugin()
    {
        //Generate Base Provider
        $this->generateProviders();
        //Generate Commands
        $this->generateCommands();
        //Generate Database
        $this->generateDatabase();
        //Generate HTTP
        $this->generateHTTP();
        //Generate Entities
        $this->generateEntities();
        //Generate Configs
        $this->generateConfigs();
        //Generate Helpers
        $this->generateHelpers();
        //Generate Resources
        $this->generateResources();
        //Generate Routes
        $this->generateRoutes();
        //Generate Plugin.json
        $this->generateJson();
        //Generate Composer File
        $this->generateComposer();
        //Put Screenshot
        $this->generateScreenshot();
    }

    /**
     * Generate Base Plugin Providers
     *
     * @return void
     */
    protected function generateProviders()
    {
        //Base Provider
        $baseProviderContent = (new Stub('plugins/base_provider.stub', [
            'NAME' => $this->pluginName,
            'SLUG' => $this->pluginSlug
        ]))->render();
        Plugins::PutFile($this->pluginName, 'Providers/'.$this->pluginName.'PluginProvider.php', $baseProviderContent);
        //Route Provider
        $routeProviderContent = (new Stub('plugins/route_provider.stub', [
            'NAME' => $this->pluginName,
        ]))->render();
        Plugins::PutFile($this->pluginName, 'Providers/'.$this->pluginName.'PluginRouteProvider.php', $routeProviderContent);
    }

    protected function generateCommands()
    {
        Plugins::PutDirectory($this->pluginName, 'Commands');
    }

    protected function generateDatabase()
    {
        Plugins::PutDirectory($this->pluginName, 'Database/Migrations');
        Plugins::PutDirectory($this->pluginName, 'Database/Seeders');
        Plugins::PutDirectory($this->pluginName, 'Database/Factories');
    }

    protected function generateHTTP()
    {
        Plugins::PutDirectory($this->pluginName, 'Http/Controllers');
        Plugins::PutDirectory($this->pluginName, 'Http/Middlewares');
    }

    protected function generateEntities()
    {
        Plugins::PutDirectory($this->pluginName, 'Entities');
    }

    protected function generateConfigs()
    {
        $configContent = (new Stub('plugins/config.stub', [
            'NAME' => $this->pluginName
        ]))->render();
        Plugins::PutFile($this->pluginName, 'Config/config.php', $configContent);
    }

    protected function generateHelpers()
    {
        $helpersContent = (new Stub('plugins/helpers.stub', []))->render();
        Plugins::PutFile($this->pluginName, 'Helpers/functions.php', $helpersContent);
    }

    protected function generateResources()
    {
        Plugins::PutDirectory($this->pluginName, 'Resources/views');
        Plugins::PutDirectory($this->pluginName, 'Resources/lang');
        Plugins::PutDirectory($this->pluginName, 'Resources/assets');
    }

    protected function generateRoutes()
    {
        $webRoutesContent = (new Stub('plugins/web_routes.stub', []))->render();
        Plugins::PutFile($this->pluginName, 'Routes/web.php', $webRoutesContent);
        $apiRoutesContent = (new Stub('plugins/api_routes.stub', []))->render();
        Plugins::PutFile($this->pluginName, 'Routes/api.php', $apiRoutesContent);
    }

    protected function generateJson()
    {
        $jsonContent = (new Stub('plugins/plugin_json.stub', [
            'NAME' => $this->pluginName,
            'SLUG' => $this->pluginSlug
        ]))->render();
        Plugins::PutFile($this->pluginName, 'plugin.json', $jsonContent);
    }

    protected function generateComposer()
    {
        $composerContent = (new Stub('plugins/composer_json.stub', [
            'NAME' => $this->pluginName,
            'SLUG' => $this->pluginSlug
        ]))->render();
        Plugins::PutFile($this->pluginName, 'composer.json', $composerContent);
    }

    /**
     * Generate Plugin Screenshot
     */
    protected function generateScreenshot()
    {
        Plugins::PutFile($this->pluginName, 'screenshot.png', file_get_contents(module_path('System', 'Resources/assets/images/default-plugin-screenshot.png')));
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['plugin', InputArgument::REQUIRED, 'Plugin Name to Create Directories and Files Required.'],
        ];
    }


    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['force', null, InputOption::VALUE_OPTIONAL, 'Force Overwrite if Plugin Already Exists.', null],
        ];
    }
}
