<?php

namespace Cascade\System\Console;

use Illuminate\Console\Command;
use Cascade\System\Services\Plugins;
use Cascade\System\Services\Stub;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class MakePluginModelCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'plugin:make-model {name} {plugin}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $className = basename($this->argument('name'));
        $namespace = trim(dirname($this->argument('name')), '.');
        $pluginName = $this->argument('plugin');
        if(!Plugins::IsExists($pluginName))
        {
            $this->error('Plugin ['.$pluginName.'] Not Found');
            return;
        }
        $modelContent = (new Stub('plugins/model.stub', [
            'NAME' => $className,
            'PLUGIN_NAME' => $pluginName,
            'NAMESPACE' => ($namespace == '') ? '' : '\\'.$namespace
        ]))->render();
        $path = 'Entities/'.($namespace != '' ? $namespace.'/' : '').$className.'.php';
        Plugins::PutFile($pluginName, $path, $modelContent);
        $this->info('Model ['.$className.'] Created Successfully for Plugin ['.$pluginName.']');
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['name', InputArgument::REQUIRED, 'Model Name.'],
            ['plugin', InputArgument::REQUIRED, 'Plugin Name' ]
        ];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null],
        ];
    }
}
