<?php

namespace Cascade\System\Console;

use Cascade\Users\Entities\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
use stdClass;

class CascadeRegisterModulesCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cascade:register-modules';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Register Official Modules.';


    protected $modules = [];

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->modules = [
            'blog', 'faq', 'events', 'vendors', 'customers', 'staff', 'services',
            'testimonials', 'c-panel', 'portal', 'employees', 'galleries',  'newsletters',
            'sponsors', 'hotels', 'advertisements', 'projects', 'tasks', 'firebase',
            'licenser', 'charity', 'portfolio', 'clinic', 'pro-sales', 'twilio'
        ];
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Registering Modules Repositories into Composer');
        $composerJson = json_decode(file_get_contents(base_path('composer.json')));
        if(empty($composerJson->repositories))
        {
            $composerJson->repositories = [];
        }
        $existRepositories = [];
        foreach($composerJson->repositories as $repo)
        {
            if(!empty($repo->name)) { $existRepositories[] = $repo->name; }
        }
        foreach($this->modules as $module)
        {
            $moduleName = 'laravel-cascade/'.$module.'-module';
            if(in_array($moduleName, $existRepositories)) { continue; }
            $url = 'https://github.com/'.$moduleName.'.git';
            $moduleObject = new stdClass;
            $moduleObject->type = 'vcs';
            $moduleObject->name = $moduleName;
            $moduleObject->url = $url;
            $composerJson->repositories[] = $moduleObject;
        }
        $composerJson = json_encode($composerJson, JSON_PRETTY_PRINT|JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_LINE_TERMINATORS);
        file_put_contents(base_path('composer.json'), $composerJson);
        $this->info('Modules Repositories Registered Successfully, You can install them now');
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
        ];
    }


    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
        ];
    }
}
