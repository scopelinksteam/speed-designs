<?php

namespace Cascade\System\Console;

use Cascade\Users\Entities\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;

class CascadeFixPublicCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cascade:fix-public';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fix Public Folder for Shared Hosting.';



    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Artisan::call("vendor:publish --tag=cascade-htaccess --ansi --force");
        $this->info('Public Folder Fixed Successfully');
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
        ];
    }


    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
        ];
    }
}
