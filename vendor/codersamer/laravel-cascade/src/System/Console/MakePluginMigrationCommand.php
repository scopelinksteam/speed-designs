<?php

namespace Cascade\System\Console;

use Cascade\System\Services\Plugins;
use Illuminate\Console\Command;
use Nwidart\Modules\Commands\MigrationMakeCommand;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class MakePluginMigrationCommand extends MigrationMakeCommand
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'plugin:make-migration';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new migration for the specified plugin.';

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['name', InputArgument::REQUIRED, 'The migration name will be created.'],
            ['plugin', InputArgument::OPTIONAL, 'The name of plugin will be created.'],
        ];
    }

    /**
     * @return mixed
     */
    protected function getDestinationFilePath()
    {
        $path = Plugins::Path($this->argument('plugin'));

        return $path.'Database/Migrations/'. date('Y_m_d_His_') . $this->argument('name') . '.php';
    }
}
