<?php

namespace Cascade\System\Console;

use Cascade\System\Entities\Module;
use Cascade\System\Services\Modules;
use Cascade\System\Services\Plugins;
use Cascade\System\Services\System;
use Cascade\Translations\Services\TranslationScanner;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class TranslateModuleCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'module:translate {module}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $scanner = new TranslationScanner;
        $module = Modules::Get($this->argument('module'));
        if($module == null)
        {
            $this->error('Cannot Find Module with name : '.$this->argument('module'));
            return;
        }
        $this->info("Set up Scanner...");
        $scanner->in($module->GetPath());
        $this->info('Scanning Process Starting');
        $scanner->Scan();
        $this->info('Generate Default Language File');
        $scanner->SaveDefaultJson($module->GetPath('Resources/lang'));
        $this->info('Generate Languages JSON Files');
        $scanner->SaveLocalizedJson($module->GetPath('Resources/lang'));
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['module', InputArgument::REQUIRED, 'An example argument.'],
        ];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null],
        ];
    }
}
