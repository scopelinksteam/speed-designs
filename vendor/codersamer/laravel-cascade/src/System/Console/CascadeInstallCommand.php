<?php

namespace Cascade\System\Console;

use Cascade\Users\Entities\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;

class CascadeInstallCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cascade:install';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Install Cascade System and Scaffolding.';



    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Starting Cascade System Installation Sequence.');
        $this->info('Create Required Directories.');
        if(!is_dir(base_path('Modules'))) { mkdir(base_path('Modules')); }
        if(!is_dir(base_path('Plugins'))) { mkdir(base_path('Plugins')); }
        if(!is_dir(base_path('Themes'))) { mkdir(base_path('Themes')); }
        $this->info('Perform Database Migration');
        Artisan::call('module:migrate');
        $this->info('Scaffold Authentication System');
        Artisan::call('breeze:install blade');
        $this->info('Publishing Files and Assets.');
        Artisan::call("vendor:publish --tag=cascade-assets --ansi --force");
        $this->info('Publishing and Building API System.');
        Artisan::call("vendor:publish --provider=\"Laravel\Sanctum\SanctumServiceProvider\"");
        Artisan::call('migrate');

        $this->info('Publishing Config Files.');
        $configCommand = 'vendor:publish --tag=cascade-config --ansi --force';
        if(file_exists(config_path('cascade.php')))
        {
            $response = $this->ask('Configuration File Detected, Want Override ? ["yes"|"no"]', "no");
            if(strtolower($response) == 'yes')
            {
                Artisan::call($configCommand);
            }
        }
        else
        {
            Artisan::call($configCommand);
        }
        $this->info('Basic Setup Done, Starting System Configuration Sequence');
        if(User::count() == 0)
        {
            $name = $this->ask('Your name');
            $email = $this->ask('Super Admin Email');
            $password = $this->ask('Password');
            $admin = new User();
            $admin->name = $name;
            $admin->email = $email;
            $admin->password = bcrypt($password);
            $admin->is_super_admin = true;
            $admin->save();
            $this->info('Super Admin Account Created Successfully.');
        }
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
        ];
    }


    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
        ];
    }
}
