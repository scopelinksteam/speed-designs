<?php

namespace Cascade\System\Console;

use Cascade\System\Services\Plugins;
use Cascade\Translations\Services\TranslationScanner;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class TranslatePluginCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'plugin:translate {plugin}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $scanner = new TranslationScanner;
        $plugin = Plugins::Get($this->argument('plugin'));
        if($plugin == null)
        {
            $this->error('Cannot Find Plugin with name : '.$this->argument('plugin'));
            return;
        }
        $this->info("Set up Scanner...");
        $scanner->in($plugin->GetPath())->exclude(['storage','vendor','public','database']);
        $this->info('Scanning Process Starting');
        $scanner->Scan();
        $this->info('Generate Default Language File');
        $scanner->SaveDefaultJson($plugin->GetPath('Resources/lang'));
        $this->info('Generate Languages JSON Files');
        $scanner->SaveLocalizedJson($plugin->GetPath('Resources/lang'));

    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['plugin', InputArgument::REQUIRED, 'Plugin Name' ]
        ];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null],
        ];
    }
}
