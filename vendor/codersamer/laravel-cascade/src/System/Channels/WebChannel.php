<?php

namespace Cascade\System\Channels;

use Cascade\System\Abstraction\NotificationBase;
use Illuminate\Notifications\Channels\DatabaseChannel;
use Illuminate\Notifications\Notification;

class WebChannel extends DatabaseChannel
{

    /**
     * Build an array payload for the DatabaseNotification Model.
     *
     * @param  mixed  $notifiable
     * @param  \Illuminate\Notifications\Notification  $notification
     * @return array
     */
    protected function buildPayload($notifiable, Notification $notification)
    {
        $reference = $notification instanceof NotificationBase ? $notification->reference() : null;
        return [
            'id' => $notification->id,
            'type' => method_exists($notification, 'databaseType')
                        ? $notification->databaseType($notifiable)
                        : get_class($notification),
            'data' => $this->getData($notifiable, $notification),
            'reference_type' =>  $reference == null ? null : get_class($reference),
            'reference_id' =>  $reference == null ? null : $reference->id,
            'payload' => serialize($notification),
            'read_at' => null,
        ];
    }

}
