<?php

namespace Cascade;

use Cascade\System\Services\Action;
use Cascade\System\Services\Activators\ModulesActivator;
use Cascade\System\Services\Filter;
use Cascade\Tenancy\Services\TenancyService;
use Cascade\Tenancy\Subscribers\ModulesSubscriber;
use Cascade\Tenancy\Subscribers\PluginsSubscriber;
use Cascade\Tenancy\Subscribers\ThemesSubscriber;
use Cascade\Users\Entities\User;
use Illuminate\Support\ServiceProvider;
use Nwidart\Modules\Facades\Module;

class CascadeServiceProvider extends ServiceProvider
{
    public function register()
    {
        if(!$this->app->runningInConsole())
        {
            //Filters
            Filter::Subscribe(ThemesSubscriber::class);
            Filter::Subscribe(ModulesSubscriber::class);
            Action::Subscribe(ModulesSubscriber::class);
            Filter::Subscribe(PluginsSubscriber::class);
        }
        Cascade::ScanModules();
        if(config('database.connections.central', null) === null)
        {
            config(['database.connections.central' => [
                    'driver' => 'mysql',
                    'url' => env('DATABASE_URL'),
                    'host' => env('DB_HOST', '127.0.0.1'),
                    'port' => env('DB_PORT', '3306'),
                    'database' => env('DB_DATABASE', 'forge'),
                    'username' => env('DB_USERNAME', 'forge'),
                    'password' => env('DB_PASSWORD', ''),
                    'unix_socket' => env('DB_SOCKET', ''),
                    'charset' => 'utf8mb4',
                    'collation' => 'utf8mb4_unicode_ci',
                    'prefix' => '',
                    'prefix_indexes' => true,
                    'strict' => true,
                    'sticky' => true,
                    'engine' => null,
                    'options' => extension_loaded('pdo_mysql') ? array_filter([
                        \PDO::MYSQL_ATTR_SSL_CA => env('MYSQL_ATTR_SSL_CA'),
                    ]) : [],
                ]
            ]);
        }
        //Enable Modules Scan
        config(["modules.scan.enabled" => true]);
        config(['modules.scan.paths' => [
            __DIR__,
            base_path('vendor/*/*'),
            base_path('Themes'),
            base_path('Plugins')
        ]]);

        //Set Uploads Disk
        config(['filesystems.disks.uploads' =>  [
                'driver' => 'local',
                'root' => public_path('uploads'),
                'visibility' => 'public'
            ]
        ]);

        //Override Users
        config(['auth.providers.users.model' => User::class]);
        
        if(!file_exists(config_path('cascade.php')))
        {
            //Set Default Cascade Configurations
            config(['cascade.multi_tenancy' => env('MULTI_TENANCY', null) ?? false]);
            config(['cascade.developer_mode' => env('DEVELOPER_MODE', null) ?? false]);
            config(['cascade.modules_manager' => env('MODULES_MANAGER', null) ?? false]);
            config(['cascade.plugins_manager' => env('PLUGINS_MANAGER', null) ?? false]);
            config(['cascade.themes_manager' => env('THEMES_MANAGER', null) ?? false]);
            config(['cascade.translation_manager' => env('TRANSLATION_MANAGER', null) ?? false]);
            config(['cascade.site_manager' => env('SITE_MANAGER', null) ?? false]);
            config(['cascade.sliders_manager' => env('SLIDERS_MANAGER', null) ?? false]);
        }



        //Get System Modules
        if(TenancyService::Enabled())
        {
            config(['modules.activators.database.class' => ModulesActivator::class]);
            config(['modules.activator' => 'database']);
        }


    }

    public function boot()
    {
        $this->SetPublishes();
    }

    protected function SetPublishes()
    {
        //Htaccess
        $this->publishes([
            __DIR__.'/../dist/.htaccess' => base_path('.htaccess')
        ], 'cascade-htaccess');

        //Config
        $this->publishes([
            __DIR__.'/../dist/configs/cascade.php' => config_path('cascade.php')
        ], 'cascade-config');

        //Public
        $this->publishes([
            __DIR__.'/../dist/public' => public_path()
        ], 'cascade-assets');

        //Resources
        $this->publishes([
            __DIR__.'/../dist/resources' => resource_path()
        ], 'cascade-assets');

        //Resources
        $this->publishes([
            __DIR__.'/../dist/routes' => base_path('routes')
        ], 'cascade-assets');
        //App
        $this->publishes([
            __DIR__.'/../dist/app' => app_path()
        ], 'cascade-assets');

    }

    
}