document.addEventListener('DOMContentLoaded', function(){
    const list = [].slice.call(document.querySelectorAll('[data-bs-toggle="popover"]'));
    list.map((el) => {
        let opts = {
            animation: false,
        };
        if (el.hasAttribute('data-bs-content-id')) {
            opts.content = document.getElementById(el.getAttribute('data-bs-content-id')).innerHTML;
            opts.html = true;
        }
        new bootstrap.Popover(el, opts);
    });

    document.querySelectorAll('.popper').forEach(function(el, index){
        el.addEventListener('click', function(e){
            let target = document.querySelector(el.getAttribute('data-popper-target'));
            let placement = el.getAttribute('data-popper-placement');
            if(!placement) { placement = 'top'; }
            Popper.createPopper(el, target, {
                placement : placement
            });
            e.preventDefault();
            e.stopPropagation();
        });
    });
});
