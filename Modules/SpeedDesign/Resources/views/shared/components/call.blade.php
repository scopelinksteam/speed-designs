<button type="button" data-project-id="{{$projectId}}" class="call-button btn btn-primary">
    @lang('Call') {{sd_get_project_call_balance_formatted($projectId)}}

</button>
@push('scripts')
<script type="text/javascript">
    const AuthId = {{auth()->id()}};
    window.addEventListener('TwilioSetup', function(){
        //On ready
        Twilio.Device.ready(function(device){
            console.log('READY Device');
        });
        //Start Call

    });
</script>
@endpush
