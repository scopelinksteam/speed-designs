<div>
    <div class="bg-white p-4 " id="call-modal" style="position: fixed; z-index:9999; top:0; left:0; width:100%;display:none;" >
        <div class="row justify-content-between">
            <div class="col-auto d-flex align-items-center">
                <div class="col-auto pe-2">
                    <h3 class="fw-bold my-4">@lang('Call Manager')</h3>
                </div>
                <div class="col-auto pe-2">
                    <p class="fw-bold my-4 ">{{$statusText}}</p>
                </div>
                <div class="col-auto pe-2">
                    <p class="text-muted my-4">{{$isCaller ? $reciever->name ?? '' : $caller->name ?? ''}}</p>
                </div>
                <div class="col-auto">

                </div>
                <div id="call-counter" class="col-auto" style="display: none">
                    <span id="call-counter-minutes"></span>
                    <span id="call-counter-seconds"></span>
                </div>
            </div>
            <div class="col-auto">
                <div class="row gx-2">
                    @if($isRinging)
                        @if(!$isCaller)
                        <div class="col-auto ps-2">
                            <button class="btn btn-success" wire:click="acceptCall">@lang('Accept')</button>
                        </div>
                        <div class="col-auto ps-2">
                            <button class="btn btn-danger" wire:click="rejectCall">@lang('Reject')</button>
                        </div>
                        @else
                        <div class="col-auto">
                            <button class="btn btn-danger" wire:click="rejectCall">@lang('End Call')</button>
                        </div>
                        @endif
                    @else
                    <div class="col-auto">
                        <button class="btn btn-danger" wire:click="rejectCall">@lang('End Call')</button>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@push('scripts')
<script src="https://sdk.twilio.com/js/client/v1.13/twilio.js"></script>
<script type="text/javascript">
document.addEventListener('DOMContentLoaded', function(){
    //Setup Twilio
    const callModal = document.getElementById('call-modal');
    const twilioToken = '{{$token}}';
    Twilio.Device.setup(twilioToken);
    const event = new Event('TwilioSetup');
    window.dispatchEvent(event);
    function ShowModal() { callModal.style.display = 'block'; }
    function HideModal() { callModal.style.display = 'none'; }
    window.addEventListener('ShowCallModal', function(){
        ShowModal();
    });

    var minutesLabel = document.getElementById("call-counter-minutes");
    var secondsLabel = document.getElementById("call-counter-seconds");

    var totalSeconds = 0;
    var intervalSetter = setInterval(() => {
            ++totalSeconds;
            secondsLabel.innerHTML = pad(totalSeconds % 60);
            minutesLabel.innerHTML = pad(parseInt(totalSeconds / 60));
    }, 1000);


    Array.from(document.getElementsByClassName('call-button')).forEach(element => {
        element.addEventListener('click', function(ev){
            Livewire.emit('Call', element.dataset.projectId);
            ev.preventDefault();
            ev.stopPropagation();
        });
    });

    window.addEventListener('SetCallStarted', function(){
        totalSeconds = 0;
        document.getElementById('call-counter').style.display = 'block';
    });
    function pad(val) {
        var valString = val + "";
        if (valString.length < 2) {
        return "0" + valString;
        } else {
        return valString;
        }
    }

    window.addEventListener('StartCall', function(ev) {
        var connection = Twilio.Device.connect({ userId : ev.detail.userId, projectId: ev.detail.projectId });
        connection.on('open', function(connection){
            console.log('Call Started');
            Livewire.emit('CallStarted');
        });
        //connection.on('Answer', function)

        connection.on('ready', function(connection){
            console.log('Call Ready');
            Livewire.emit('CallStarted');
        });
    });

    //Handle Connection
    Twilio.Device.connect(function(connection){
        console.log('Recieved a Connection', connection);
        window.addEventListener('RejectCall', function(){
            HideModal();
            document.getElementById('call-counter').style.display = 'none';
            connection.disconnect();
        });
        Livewire.emit('CallConnected', connection);
        const userId = connection.message.userId;
    });

    //Handle Connection
    Twilio.Device.disconnect(function(){
        console.log('Disconnected');
        HideModal();
    });
    //Handle Error
    Twilio.Device.error(function(err){
        console.log('Error', err);
    });
    //Handle Incomming Calls
    Twilio.Device.incoming(function(connection) {
        const userId = connection.message.userId;
        // Set a callback to be executed when the connection is accepted
        console.log('Incomming Call');
        console.log(connection);
        Livewire.emit('IncomingCall', connection);
        connection.accept(function() {
            console.log('Call Accepted');
            Livewire.emit('CallAccepted');
        });
        window.addEventListener('AcceptCall', function(){
            connection.accept();
        });



        window.addEventListener('DisconnectCall', function(){
            HideModal();
            document.getElementById('call-counter').style.display = 'none';
            Twilio.Device.disconnectAll();
        });
    });
});
</script>
@endpush
