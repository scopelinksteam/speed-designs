<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    @livewire('datatable', ['table' => $table, 'params' => ['teamId' => $team->id]], key($team->id))
                </div>
            </div>
        </div>
    </div>
</div>
