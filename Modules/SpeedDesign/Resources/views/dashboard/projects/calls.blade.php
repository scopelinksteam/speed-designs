<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <table class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>@lang('Call Code')</th>
                            <th>@lang('Started At')</th>
                            <th>@lang('Duration')</th>
                            <th>@lang('Record')</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($calls as $call)
                        <tr>
                            <td>{{$call->sid}}</td>
                            <td>{{$call->created_at}}</td>
                            <td>{{$call->duration == 0 ? __('In Progress') : sprintf('%02d:%02d:%02d', ($call->duration / 3600),($call->duration/ 60 % 60), $call->duration % 60 - 1);}}</td>
                            <td>
                                @if(!empty($call->record_file))
                                <audio src="{{uploads_url($call->record_file)}}" type="audio/mpeg" controls></audio>
                                @endif
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
