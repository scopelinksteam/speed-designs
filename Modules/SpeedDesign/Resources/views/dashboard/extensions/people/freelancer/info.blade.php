<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <div class="card-title">
                    @lang('Freelancer Details')
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="" class="form-label">@lang('First Language')</label>
                            <input type="text" name="attributes[first_language]" id="" class="form-control" value="{{isset($person) ? $person->Attribute('first_language') : ''}}">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="" class="form-label">@lang('Second Language')</label>
                            <input type="text" name="attributes[second_language]" id="" class="form-control" value="{{isset($person) ? $person->Attribute('second_language') : ''}}">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="" class="form-label">@lang('Job')</label>
                            <input type="text" name="attributes[job]" id="" class="form-control" value="{{isset($person) ? $person->Attribute('job') : ''}}">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="" class="form-label">@lang('Design Sample')</label>
                            <input type="text" name="attributes[design_sample]" id="" class="form-control" value="{{isset($person) ? $person->Attribute('design_sample') : ''}}">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="" class="form-label">@lang('Birthdate')</label>
                            <input type="date" name="attributes[birthdate]" id="" class="form-control" value="{{isset($person) ? $person->Attribute('birthdate') : ''}}">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="" class="form-label">@lang('Nationality')</label>
                            <input type="text" name="attributes[nationality]" id="" class="form-control" value="{{isset($person) ? $person->Attribute('nationality') : ''}}">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="" class="form-label">@lang('Experience')</label>
                            <input type="text" name="attributes[experience]" id="" class="form-control" value="{{isset($person) ? $person->Attribute('experience') : ''}}">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
