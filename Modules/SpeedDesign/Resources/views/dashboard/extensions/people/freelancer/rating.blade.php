<div class="card">
    <div class="card-header">
        <div class="card-title">@lang('Rating')</div>
    </div>
    <div class="card-body">
        <div class="form-group">
            @php $currentRating = isset($person) ? $person->Attribute('performance_rating', 0) : 0; @endphp
            <label for="performance_rating" class="form-label">@lang('Performance Rating')</label>
            <select name="attributes[performance_rating]" id="performance_rating" class="form-control">
                @for ($i = 1; $i <= 10; $i++)
                <option value="{{$i}}" @selected($i == $currentRating)>{{$i}}</option>
                @endfor
            </select>
        </div>
    </div>
</div>
