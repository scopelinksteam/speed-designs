@php
$freelancers = [];
if($project->owner_type == \Modules\Teams\Entities\Team::class)
{
    $team = $project->owner;
    if($team)
    {
        $freelancers = \Cascade\Users\Entities\User::whereIn('id', $team->members->pluck('member_id')->toArray())->get();
    }
}
else {
    $freelancers = \Cascade\Users\Entities\User::all();
}

@endphp
<div class="card">
    <div class="card-header">
        <div class="card-title">@lang('Request Estimation')</div>
    </div>
    <div class="card-body">
        <form action="{{route('dashboard.app.projects.estimation_requests.store')}}" method="POST">
            <div class="form-group">
                <label for="users" class="form-label">@lang('Select User')</label>
                <select name="user_id" id="users" class="selectr">
                    <option value="0">@lang('Users')</option>
                    @foreach ($freelancers as $freelancer)
                    <option value="{{$freelancer->id}}">{{$freelancer->name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                @csrf
                <input type="hidden" name="project_id" value="{{$project->id}}">
                <button type="submit" class="btn btn-primary">@lang('Request Estimation')</button>
            </div>
        </form>
    </div>
</div>
