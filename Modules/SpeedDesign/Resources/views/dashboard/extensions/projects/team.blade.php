@php $selectedTeamId = $project->owner_type == \Modules\Teams\Entities\Team::class ? intval($project->owner_id) : 0; @endphp
<div class="card">
    <div class="card-header">
        <div class="card-title">@lang('Assign Team')</div>
    </div>
    <div class="card-body">
        <form action="{{route('dashboard.app.projects.team.store')}}" method="POST">
            <div class="form-group">
                <label for="team" class="form-label">@lang('Select Team')</label>
                <select name="team_id" id="team" class="selectr">
                    <option value="0">@lang('Teams')</option>
                    @foreach (\Modules\Teams\Entities\Team::all() as $team)
                    <option value="{{$team->id}}" @selected($selectedTeamId == $team->id)>{{$team->name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                @csrf
                <input type="hidden" name="project_id" value="{{$project->id}}">
                <button type="submit" class="btn btn-primary">@lang('Assign Team')</button>
            </div>
        </form>
    </div>
</div>
