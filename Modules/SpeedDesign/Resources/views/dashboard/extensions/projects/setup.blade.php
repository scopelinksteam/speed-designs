<div class="row">
    <div class="col-md-12">
        <div>
            <label for="space" class="form-label">@lang('Space')</label>
            <input class="form-control" type="number" name="attributes[space]" id="space" value="{{isset($entity) ? $entity->Attribute('space', 0) : old('attributes.space')}}">
        </div>
    </div>
    <div class="col-md-12">
        <div>
            <label for="designer_code" class="form-label">@lang('Designer Code')</label>
            <input class="form-control" type="text" name="attributes[designer_code]" id="designer_code" value="{{isset($entity) ? $entity->Attribute('designer_code', '') : old('attributes.designer_code')}}">
        </div>
    </div>
    <div class="col-md-12">
        <div>
            <label for="desired_time" class="form-label">@lang('Desired Time')</label>
            <input class="form-control" type="text" name="attributes[desired_time]" id="desired_time" value="{{isset($entity) ? $entity->Attribute('desired_time', '') : old('attributes.desired_time')}}">
        </div>
    </div>
</div>
