@if($type->type() == \Modules\Sponsors\Entities\Sponsor::class)
<div class="card" id="gallery-builder-card">
    <div class="card-header">
        <i class="fa fa-image mr-2"></i>
        @lang('Sponsor Gallery')
    </div>
    <div class="card-body">
        <x-gallery-builder :gallery="isset($post) ? $post->GetGallery() : null" :gridColumns="1" />
    </div>
</div>
@endif
