<?php

namespace Modules\SpeedDesign\Http\Controllers\Dashboard;

use Cascade\System\Services\Feedback;
use Cascade\Users\Entities\User;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Projects\Entities\Project;
use Modules\SpeedDesign\Notifications\ProjectEstimationRequestNotification;
use Modules\Teams\Entities\Team;

class ProjectsController extends Controller
{
    public function storeTeam(Request $request)
    {
        $project = Project::find($request->integer('project_id'));
        if($project == null)
        {
            Feedback::error(__('Cannot find Selected Project'));
            Feedback::flash();
            return back();
        }

        $team = Team::find($request->integer('team_id'));

        if($team == null)
        {
            Feedback::error(__('Cannot find Selected Team'));
            Feedback::flash();
            return back();
        }

        $project->owner_id = $team->id;
        $project->owner_type = Team::class;
        $project->save();

        Feedback::success(__('Team Associated Successfully'));
        Feedback::flash();
        return back();
    }

    public function requestEstimation(Request $request)
    {
        $project = Project::find($request->integer('project_id'));
        if($project == null)
        {
            Feedback::error(__('Cannot find Selected Project'));
            Feedback::flash();
            return back();
        }

        $user = User::find($request->integer('user_id'));

        if($user == null)
        {
            Feedback::error(__('Cannot find Selected User'));
            Feedback::flash();
            return back();
        }

        ProjectEstimationRequestNotification::Send($project, $user);
        Feedback::success(__('Request Sent Successfully'));
        Feedback::flash();
        return back();
    }
}
