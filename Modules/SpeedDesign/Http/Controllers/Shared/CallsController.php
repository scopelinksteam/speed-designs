<?php

namespace Modules\SpeedDesign\Http\Controllers\Shared;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Storage;
use Modules\Projects\Entities\Project;
use Modules\SpeedDesign\Entities\TwilioCallLog;
use Twilio\TwiML\VoiceResponse;

class CallsController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        return view('speeddesign::index');
    }

    public function request(Request $request)
    {

        $userId = $request->input('userId');
        $projectId = $request->input('projectId');
        $project = Project::find($projectId);

        $log = new TwilioCallLog();
        $log->sid = $request->input('CallSid');
        $log->reciever_id = $userId;
        if($project)
        {
            $log->caller_id = $project->customer_id;
            $log->reference_id = $project->id;
            $log->reference_type = get_class($project);
        }
        $log->save();
        $response = new VoiceResponse();
        $dial = $response->dial('');
        $dial->client('user'.$request->input('userId'));
        $dial->setRecord('true');
        $dial->setRecordingStatusCallback(route('calls.record'));
        $dial->setRecordingStatusCallbackMethod('GET');
        $dial->setRecordingStatusCallbackEvent('completed');
        $dial->setRecordingTrack('both');
        return $response;
    }

    public function status(Request $request)
    {
        if($request->input('CallStatus') == 'completed')
        {
            $sid = $request->input('CallSid');
            $caller = $request->input('Caller');
            $caller = intval(str_replace('client:user', '', $caller));

            $lastCall = TwilioCallLog::where('sid', $sid)->first();
            if($lastCall)
            {
                $lastCall->duration = intval($request->input('CallDuration'));
                $lastCall->save();
            }
        }
        return '';
    }

    public function record(Request $request)
    {
        if($request->input('RecordingStatus') == 'completed')
        {
            $sid = $request->input('CallSid');

            $lastCall = TwilioCallLog::where('sid', $sid)->first();
            if($lastCall)
            {
                $recordingFile = file_get_contents($request->input('RecordingUrl').'.mp3');
                $path = 'calls/'.$sid.'.mp3';
                Storage::disk('uploads')->put($path, $recordingFile);
                $lastCall->duration = intval($request->input('RecordingDuration'));
                $lastCall->record_file = $path;
                $lastCall->save();
            }
        }
        return '';
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('speeddesign::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('speeddesign::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('speeddesign::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        //
    }
}
