<?php

namespace Modules\SpeedDesign\Http\Controllers\Frontend;

use Cascade\System\Entities\DatabaseNotification;
use Cascade\System\Services\Feedback;
use Cascade\System\Services\System;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Projects\Entities\Project;
use Modules\Projects\Entities\ProjectEstimation;
use Modules\SpeedDesign\Notifications\ProjectEstimatedNotification;
use Modules\SpeedDesign\Notifications\ProjectEstimationRequestNotification;

class EstimationController extends Controller
{
    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create(Project $project)
    {
        return System::FindView('speeddesign::frontend.estimations.create', [
            'project' => $project
        ]);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        $project = Project::find(intval($request->input('project_id', 0)));
        if(!$request->filled('time') || !$request->filled('price') || !$request->filled('edits') || !$request->filled('stages'))
        {
            Feedback::error(__('Please Fill all Required Fields'));
            Feedback::flash();
            return back();
        }
        $estimation = new ProjectEstimation();
        $estimation->project_id = $project->id;
        $estimation->user_id = auth()->id();
        $estimation->price = doubleval($request->input('price', '0'));
        $estimation->stages = doubleval($request->input('stages', '0'));
        $estimation->duration = doubleval($request->input('time', '0'));
        $estimation->notes = $request->input('notes', '');
        $estimation->save();
        $estimation->storeAttribute('edits', doubleval($request->input('edits', '0')));
        $estimation->storeAttribute('edit_price', doubleval($request->input('edits_price', '0')));

        //Delete Notification
        DatabaseNotification::where('type', ProjectEstimationRequestNotification::class)->where('notifiable_id', auth()->id())->where('reference_id', $project->id)->delete();

        //Send Notification
        ProjectEstimatedNotification::Send($estimation);

        Feedback::success(__('Estimation Saved Successfully'));
        Feedback::flash();
        return to_route('orders.list');
    }

    public function approve(ProjectEstimation $estimation)
    {
        $estimation->approve();
        Feedback::success(__('Estimation Approved Successfully'));
        Feedback::flash();
        return redirect('/');
    }

    public function terms(ProjectEstimation $estimation)
    {
        return System::FindView('speeddesign::frontend.estimations.terms', [
            'estimation' => $estimation,
            'project' => $estimation->project
        ]);
    }

    public function approveTerms(ProjectEstimation $estimation)
    {
        $estimation->project->storeAttribute('approved_terms', true);
        return to_route('estimation.approve', ['estimation' => $estimation]);
    }


    public function contract(ProjectEstimation $estimation)
    {
        return System::FindView('speeddesign::frontend.estimations.contract', [
            'estimation' => $estimation,
            'project' => $estimation->project
        ]);
    }

    public function approveContract(ProjectEstimation $estimation)
    {
        $estimation->project->storeAttribute('approved_contract', true);
        return to_route('estimation.approve', ['estimation' => $estimation]);
    }
}
