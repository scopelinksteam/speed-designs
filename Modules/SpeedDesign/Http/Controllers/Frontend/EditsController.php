<?php

namespace Modules\SpeedDesign\Http\Controllers\Frontend;

use Cascade\System\Services\Feedback;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Projects\Entities\Project;
use Modules\Projects\Entities\ProjectEditRequest;
use Modules\Projects\Entities\ProjectPayment;
use Modules\Projects\Enums\EditRequestStatus;

class EditsController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        return view('speeddesign::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('speeddesign::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        $project = Project::find($request->integer('project_id'));
        if($project == null)
        {
            Feedback::error(__('Invalid Project'));
            Feedback::flash();
            return back();
        }

        $freeEdits = intval($project->Attribute('edits', 0));
        $editPrice = doubleval($project->Attribute('edits_price', 0));
        $edit = new ProjectEditRequest();
        $edit->project_id = $project->id;
        $edit->description = $request->description ?? '';
        $edit->cost = 0;
        $edit->price = $project->edits()->count() >= $freeEdits ? $editPrice : 0;
        $edit->status_id = $edit->price > 0 ? EditRequestStatus::PendingPayment : EditRequestStatus::Active;
        $edit->save();
        //Add Payment
        if($edit->price > 0)
        {
            $payment = new ProjectPayment();
            $payment->project_id = $project->id;
            $payment->amount = $edit->price;
            $payment->currency_id = 0;
            $payment->transaction_id = 0;
            $payment->due_date = now();
            $payment->is_paid = false;
            $payment->save();
            $payment->storeAttribute('edit_id', $edit->id);
        }

        Feedback::success(__('Edit Request Saved Successfully'));
        Feedback::flash();
        return back();
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('speeddesign::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('speeddesign::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        //
    }
}
