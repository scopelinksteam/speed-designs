<?php

namespace Modules\SpeedDesign\Http\Controllers\Frontend;

use Cascade\System\Services\System;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Projects\Entities\Project;
use Modules\SpeedDesign\Entities\Company;
use Modules\SpeedDesign\Entities\Freelancer;
use Modules\SpeedDesign\Entities\Member;

class ProjectsController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        $activeUser = auth()->user()->impersonate();
        if($activeUser instanceof Company || $activeUser instanceof Member)
        {
            return System::FindView('speeddesign::frontend.projects.customer', [
                'projects' => Project::where('customer_id', $activeUser->id)->get(),
            ]);
        }
        if($activeUser instanceof Freelancer)
        {
            return System::FindView('speeddesign::frontend.projects.freelancer', [
                'projects' => Project::whereHas('members', function($query) use($activeUser) {
                    $query->where('member_id', $activeUser->id);
                })->get(),
            ]);
        }
        return redirect('/');
    }

    public function show(Project $project)
    {
        return System::FindView('speeddesign::frontend.projects.show', [
            'project' => $project
        ]);
    }


}
