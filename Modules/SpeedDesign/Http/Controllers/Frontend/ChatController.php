<?php

namespace Modules\SpeedDesign\Http\Controllers\Frontend;

use Cascade\System\Services\System;
use Cascade\Users\Entities\User;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Twilio\Entities\ChatRoom;

class ChatController extends Controller
{

    public function index()
    {
        return System::FindView('speeddesign::frontend.chat.index', [
            'rooms' => ChatRoom::mine()->get(),
        ]);
    }

    public function room(User $user)
    {
        return $this->index()->with([
            'active_room' => ChatRoom::Of(auth()->user(), $user->impersonate())
        ]);
    }
}
