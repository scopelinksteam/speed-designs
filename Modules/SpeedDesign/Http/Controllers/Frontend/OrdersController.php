<?php

namespace Modules\SpeedDesign\Http\Controllers\Frontend;

use Cascade\System\Services\System;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Projects\Entities\Project;

class OrdersController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {

        return System::FindView('speeddesign::frontend.orders.index', [
            'orders' => \sd_get_requests()
        ]);
    }


}
