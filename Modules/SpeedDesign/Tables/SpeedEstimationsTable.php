<?php

namespace Modules\SpeedDesign\Tables;

use Cascade\Dashboard\Services\Datatables\Column;
use Modules\Projects\Entities\Project;
use Modules\Projects\Entities\ProjectEstimation;
use Modules\Projects\Tables\ProjectsEstimationsTable;

class SpeedEstimationsTable extends ProjectsEstimationsTable
{

    public function __construct(Project $project)
    {
        parent::__construct($project);
        $this->editColumn('edits', function(ProjectEstimation $estimation){
            return $estimation->Attribute('edits', 0);
        });
    }

    public function columns()
    {
        $columns = parent::columns();
        $columns[] = Column::make('edits')->text(__('Edits'));
        return $columns;
    }
}
