<?php

namespace Modules\SpeedDesign\Tables;

use Modules\Projects\Tables\ProjectsTable;
use Modules\Teams\Entities\Team;

class TeamsProjectsTable extends ProjectsTable
{
    protected $team;

    public function __construct($teamId = 0)
    {
        $this->team = Team::find($teamId);
        parent::__construct();
    }

    public function query($search = null, $filters = [])
    {
        return parent::query()->where('owner_type', Team::class)->where('owner_id', $this->team->id);
    }
}
