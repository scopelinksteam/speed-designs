<?php

namespace Modules\SpeedDesign\Entities;

use Cascade\Users\Entities\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class TwilioCallLog extends Model
{
    protected $table = 'twilio_call_log';

    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope('linked', function(Builder $query){
            return $query->whereNotNull('reference_type');
        });
    }

    public function caller()
    {
        return $this->belongsTo(User::class, 'caller_id');
    }

    public function reciever()
    {
        return $this->belongsTo(User::class, 'reciever_id');
    }

    public function reference()
    {
        return $this->morphTo();
    }
}
