<?php

namespace Modules\SpeedDesign\Components;

use Cascade\Users\Entities\User;
use Livewire\Component;
use Modules\Projects\Entities\Project;
use Modules\SpeedDesign\Entities\Freelancer;
use Twilio\Jwt\ClientToken;

class CallsComponent extends Component
{

    public $user;
    public $token = '';
    public $callerName = '';
    public $statusText = '';
    public $isRinging = true;
    public $isCaller = true;
    public $caller;
    public $reciever;
    public $project;
    protected $listeners = [
        'IncomingCall' => 'handleIncommingCall',
        'CallAccepted' => 'handleCallAccepted',
        'Call' => 'handleCall',
        'CallConnected' => 'handleCallConnected'
    ];

    public function mount()
    {
        $this->user = User::find(auth()->id());
        $this->generateToken();
    }
    public function handleCallAccepted()
    {
        $this->SetCallStarted();

    }

    public function SetCallStarted()
    {

        $this->statusText = __('In Call');
        $this->isRinging = false;
        $this->dispatchBrowserEvent('SetCallStarted');
    }

    public function handleIncommingCall($data)
    {
        $params = $data['parameters'];
        $from = intval(str_replace('client:user', '', $params['From']));
        $to =  intval(str_replace('client:user', '', $params['To']));
        $this->caller = User::find($from);
        $this->reciever = User::find($to);
        $user = User::find($from);
        $this->statusText = $from == auth()->id() ? __('Ringing') : __('Incoming Call');
        $this->isRinging = true;
        $this->isCaller = $from == auth()->id();
        $this->callerName = $user->name ?? __('System');
        $this->dispatchBrowserEvent('ShowCallModal');
    }

    public function handleCallConnected($data)
    {
        $params = $data['parameters'];
        $from = isset($params['From']) ? intval(str_replace('client:user', '', $params['From'])) :  auth()->id();
        $to =  isset($params['To']) ? intval(str_replace('client:user', '', $params['To'])) : intval(str_replace('client:user', '', $data['message']['userId']));
        $this->caller = User::find($from);
        $this->reciever = User::find($to);
        $user = User::find($from);
        $this->statusText = $from == auth()->id() ? __('Ringing') : __('Incoming Call');
        if($data['_isAnswered'])
        {
            $this->SetCallStarted();
        }
        else
        {
            $this->isRinging = true;
        }
        $this->isCaller = $from == auth()->id();
        $this->callerName = $user->name ?? __('System');
        $this->dispatchBrowserEvent('ShowCallModal');
    }

    public function handleCall($projectId)
    {
        $this->project = Project::find($projectId);
        if($this->project)
        {
            $members = $this->project->members;
            $user = null;
            foreach($members as $member)
            {
                if($member->member->type != $this->project->customer_id)
                {
                    $user = $member->member;
                    break;
                }
            }
            if($user != null)
            {
                $this->dispatchBrowserEvent('StartCall', ['userId' => $user->id, 'projectId' => $projectId]);
            }
        }
    }

    public function acceptCall()
    {
        $this->dispatchBrowserEvent('AcceptCall');
    }

    public function rejectCall()
    {
        $this->isRinging = false;

        $this->dispatchBrowserEvent('RejectCall');
    }

    protected function generateToken()
    {
        if(!empty($this->token)) { return; }
        $sid = get_setting('integrations::twilio.credentials.account_sid', '', false);
        $accountToken = get_setting('integrations::twilio.credentials.auth_token', '', false);
        $clientToken = new ClientToken($sid, $accountToken);
        $clientToken->allowClientOutgoing('AP5e8759445f4ce374ee06d65d4541be16');
        $clientToken->allowClientIncoming('user'.auth()->id());

        $this->token = $clientToken->generateToken();
    }


    public function render()
    {
        return view('speeddesign::shared.components.calls');
    }
}
