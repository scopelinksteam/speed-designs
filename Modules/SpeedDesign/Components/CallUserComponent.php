<?php

namespace Modules\SpeedDesign\Components;

use Cascade\Users\Entities\User;
use Illuminate\View\Component as ViewComponent;
use Livewire\Component;
use Modules\Projects\Entities\Project;
use Twilio\Jwt\ClientToken;

class CallUserComponent extends ViewComponent
{

    public $userId;
    public $user;

    public $projectId;
    public $project;

    public function __construct($projectId)
    {
        $this->projectId = $projectId;
        $this->project = Project::find(intval($this->projectId));
        /*
        $this->userId = $userId;
        $this->user = User::find($userId);
        */
    }

    public function render()
    {
        return view('speeddesign::shared.components.call');
    }
}
