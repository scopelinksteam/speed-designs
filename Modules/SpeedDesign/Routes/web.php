<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;
use Modules\Projects\Entities\Project;
use Modules\Projects\Entities\ProjectPayment;
use Modules\SpeedDesign\Http\Controllers\Dashboard\ProjectsController as DashboardProjectsController;
use Modules\SpeedDesign\Http\Controllers\Frontend\ChatController;
use Modules\SpeedDesign\Http\Controllers\Frontend\EditsController;
use Modules\SpeedDesign\Http\Controllers\Frontend\EstimationController;
use Modules\SpeedDesign\Http\Controllers\Frontend\OrdersController;
use Modules\SpeedDesign\Http\Controllers\Frontend\ProfileController;
use Modules\SpeedDesign\Http\Controllers\Frontend\ProjectsController;
use Modules\SpeedDesign\Http\Controllers\Shared\CallsController;
use Plugins\UPayment\Foundation\UPaymentClient;

Route::prefix('calls')->name('calls.')->group(function(){
    Route::get('request', [CallsController::class, 'request'])->name('request');
    Route::get('status', [CallsController::class, 'status'])->name('status');
    Route::get('record', [CallsController::class, 'record'])->name('record');
});

Route::get('/switch', function(){
    if(auth()->user()->isSwitched())
    {
        auth()->user()->leaveSwitch();
        if(auth()->user()->can('view dashboard'))
        {
            return redirect('/dashboard');
        }
        else
        {
            return redirect('/');
        }
    }
})->name('switch');
Route::middleware('auth')->group(function(){

    Route::get('/orders', [OrdersController::class, 'index'])->name('orders.list');
    Route::get('/estimate/{project}', [EstimationController::class, 'create'])->name('estimation.create');
    Route::post('/estimate', [EstimationController::class, 'store'])->name('estimation.store');
    Route::get('/estimate/terms/{estimation}', [EstimationController::class, 'terms'])->name('estimation.terms');
    Route::get('/estimate/terms/{estimation}/approve', [EstimationController::class, 'approveTerms'])->name('estimation.terms.approve');
    Route::get('/estimate/contract/{estimation}', [EstimationController::class, 'contract'])->name('estimation.contract');
    Route::get('/estimate/contract/{estimation}/approve', [EstimationController::class, 'approveContract'])->name('estimation.contract.approve');
    Route::get('/estimate/approve/{estimation}', [EstimationController::class, 'approve'])->name('estimation.approve');

    Route::get('/projects', [ProjectsController::class, 'index'])->name('projects.index');
    Route::get('/projects/{project}', [ProjectsController::class, 'show'])->name('projects.show');
    Route::post('/projects/edits', [EditsController::class, 'store'])->name('projects.edits.store');
    /*
    Route::prefix('chat')->name('chat.')->group(function(){
        Route::get('/', [ChatController::class, 'index'])->name('index');
        Route::get('/{user}', [ChatController::class, 'room'])->name('room');
    });
    */

    Route::get('/calls/buy/{project}', function(Project $project){
        $paymentClient = new UPaymentClient();
        $hourPrice = doubleval(get_setting('app::projects.calls.hour_price', 1));
        $url = $paymentClient->GetPaymentURL($project->id, $project->customer_id, $hourPrice, route('calls.pay.success', ['project' => $project]), route('calls.pay.failure', ['project' => $project]));
        if(!is_string($url))
        {
            dd($url);
        }
        return redirect()->away($url);
    })->name('calls.buy');

    Route::get('/calls/pay/{project}/success', function(Project $project){
        sd_add_project_call_balance($project->id, 3600);
        return redirect('/');
    })->name('calls.pay.success');


    Route::get('/calls/pay/{project}/failure', function(Project $project){
        return redirect('/');
    })->name('calls.pay.failure');

    Route::get('/projects/pay/{payment}', function(ProjectPayment $payment){
        $project = $payment->project;
        $paymentClient = new UPaymentClient();
        $url = $paymentClient->GetPaymentURL($payment->id, $project->customer_id, $payment->amount, route('pay.success', ['payment' => $payment]), route('pay.failure', ['payment' => $payment]));
        if(!is_string($url))
        {
            dd($url);
        }
        return redirect()->away($url);
    })->name('pay');

    Route::get('/projects/pay/{payment}/success', function(ProjectPayment $payment){
        $payment->is_paid = true;
        $payment->save();
        return redirect('/');
    })->name('pay.success');

    Route::get('/projects/pay/{payment}/failed', function(ProjectPayment $payment){
        return redirect('/');
    })->name('pay.failure');

    Route::prefix('profile')->name('profile.')->group(function(){
        Route::prefix('gallery')->name('gallery.')->group(function(){
            Route::get('/delete/{attachment}', [ProfileController::class, 'deleteImage'])->name('delete.image');
        });
    });
});

Route::dashboard(function(){
    Route::prefix('projects')->name('projects.')->group(function(){
        Route::prefix('team')->name('team.')->group(function(){
            Route::post('/assign', [DashboardProjectsController::class, 'storeTeam'])->name('store');
        });
        Route::prefix('estimation_requests')->name('estimation_requests.')->group(function(){
            Route::post('/store', [DashboardProjectsController::class, 'requestEstimation'])->name('store');
        });

    });
}, 'app');
