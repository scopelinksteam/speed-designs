<?php

namespace Modules\SpeedDesign\Providers;

use Cascade\Dashboard\Entities\Statistic;
use Cascade\Dashboard\Services\DashboardBuilder;
use Cascade\Dashboard\Services\Datatables\Action as DatatablesAction;
use Cascade\Dashboard\Services\Datatables\Actions\Generic;
use Cascade\Dashboard\Services\Datatables\DataTable;
use Cascade\Dashboard\Services\Datatables\DataTableExtender;
use Cascade\People\Enums\PersonFeature;
use Cascade\People\Services\PeopleService;
use Cascade\Settings\Services\Settings;
use Cascade\Site\Actions\PostAfterStoreAction;
use Cascade\Site\Http\Controllers\Dashboard\PostsController;
use Cascade\Site\Services\PagesService;
use Cascade\System\Actions\SystemReadyAction;
use Cascade\System\Entities\Category;
use Cascade\System\Entities\DatabaseNotification;
use Cascade\System\Entities\Label;
use Cascade\System\Http\Middleware\ForceLoginMiddleware;
use Cascade\System\Services\Action;
use Cascade\System\Services\Addons\ForceLoginService;
use Cascade\System\Services\Generators\LabelBuilder;
use Cascade\System\Services\NotificationService;
use Cascade\System\Services\System;
use Cascade\Users\Datatables\UsersDatatable;
use Cascade\Users\Entities\Role;
use Cascade\Users\Entities\User;
use Cascade\Users\Services\ProfileFrontendService;
use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Response;
use Livewire\Livewire;
use Modules\Galleries\Services\GalleryService;
use Modules\Portfolio\Services\PortfolioFrontendService;
use Modules\Projects\Entities\Project;
use Modules\Projects\Entities\ProjectEditRequest;
use Modules\Projects\Entities\ProjectEstimation;
use Modules\Projects\Entities\ProjectPayment;
use Modules\Projects\Enums\EditRequestStatus;
use Modules\Projects\Events\ProjectCreated;
use Modules\Projects\Events\ProjectPaymentUpdated;
use Modules\Projects\Pages\ProjectViewPage;
use Modules\Projects\Services\ProjectEstimationService;
use Modules\Projects\Tables\ProjectsEstimationsTable;
use Modules\SpeedDesign\Components\CallsComponent;
use Modules\SpeedDesign\Components\CallUserComponent;
use Modules\SpeedDesign\Entities\Company;
use Modules\SpeedDesign\Entities\Freelancer;
use Modules\SpeedDesign\Entities\Member;
use Modules\SpeedDesign\Handlers\ProjectCreatedHandler;
use Modules\SpeedDesign\Handlers\SponsorStore;
use Modules\SpeedDesign\Jobs\RemoveExpiredProjectsJob;
use Modules\SpeedDesign\Notifications\ProjectEstimatedNotification;
use Modules\SpeedDesign\Notifications\ProjectEstimationRequestNotification;
use Modules\SpeedDesign\Tables\SpeedEstimationsTable;
use Modules\SpeedDesign\Tabs\FreelancerInfoTab;
use Modules\SpeedDesign\Tabs\ProjectCallsTab;
use Modules\SpeedDesign\Tabs\TeamProjectsTab;
use Modules\Sponsors\Entities\Sponsor;
use Modules\Teams\Pages\TeamPage;
use Modules\Twilio\Services\TwilioChatService;

class SpeedDesignServiceProvider extends ServiceProvider
{
    /**
     * @var string $moduleName
     */
    protected $moduleName = 'SpeedDesign';

    /**
     * @var string $moduleNameLower
     */
    protected $moduleNameLower = 'speeddesign';

    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerTranslations();
        $this->registerConfig();
        $this->registerViews();
        $this->loadMigrationsFrom(module_path($this->moduleName, 'Database/Migrations'));
        $this->registerStatistics();
        $this->registerUsers();
        $this->registerExtensions();
        $this->registerSettings();

        Blade::component('call-user', CallUserComponent::class);
        Livewire::component('calls', CallsComponent::class);
        Event::listen(ProjectCreated::class, ProjectCreatedHandler::class);
        ProjectsEstimationsTable::Extender()->override(SpeedEstimationsTable::class);
        ProjectEstimation::OnAction('approved', function(ProjectEstimation $estimation){
            if($estimation->project == null) { return; }
            $project = $estimation->project;
            $project->storeAttribute('edits', $estimation->Attribute('edits', 0));
            $project->storeAttribute('edits_price', $estimation->Attribute('edits_price', 0));
            $project->storeAttribute('stages', $estimation->stages);
            $project->storeAttribute('duration', $estimation->duration);
            $project->start_date = now();
            $project->deadline = now()->addDays(intval($estimation->duration));
            $activeStatus = intval(get_setting('projects::general.setup.active_status', 0));

            $days = intval($estimation->duration);
            $stages = intval($estimation->stages);

            if($activeStatus > 0)
            {
                $project->status_id = $activeStatus;
            }
            $project->save();
            $user = $estimation->user;
            if($user)
            {
                $user->join($project, 'members');
            }
            ProjectEstimation::where('project_id', $estimation->project_id)->delete();
            DatabaseNotification::where('type', ProjectEstimatedNotification::class)->where('reference_id', $estimation->project_id)->delete();

            if($estimation->price > 0)
            {
                $paymentInterval = intval($days / $stages);
                $paymentAmount = doubleval($estimation->price / $stages);
                $nextDueDate = now();
                for($i = 0; $i < $stages; $i++)
                {
                    $payment = new ProjectPayment();
                    $payment->project_id = $project->id;
                    $payment->amount = $paymentAmount;
                    $payment->due_date = $nextDueDate;
                    $payment->save();
                    $nextDueDate = $nextDueDate->addDays($paymentInterval);
                }
                if($project->payments()->count() > 0)
                {
                    return to_route('pay', ['payment' => $project->payments()->first()]);
                }
            }
        });

        ProjectEstimation::OnAction('approving', function(ProjectEstimation $estimation){
            $project = $estimation->project;
            if($project->Attribute('approved_terms', null) === null)
            {
                return to_route('estimation.terms', ['estimation' => $estimation]);
            }
            if($project->Attribute('approved_contract', null) === null)
            {
                return to_route('estimation.contract', ['estimation' => $estimation]);
            }
        });
        System::Schedule()->job(RemoveExpiredProjectsJob::class)->everyThirtyMinutes();

        Event::listen(ProjectPaymentUpdated::class, function(ProjectPaymentUpdated $event){
            $payment = $event->payment;
            if($payment->is_paid)
            {
                if($payment->Attribute('edit_id', 0) > 0)
                {
                    $editId = intval($payment->Attribute('edit_id', 0));
                    $edit = ProjectEditRequest::find($editId);
                    if($edit)
                    {
                        $edit->status_id = EditRequestStatus::Active;
                        $edit->save();
                    }
                }
                else
                {
                    $project = $payment->project;
                    $activeStatus = intval(get_setting('projects::general.setup.active_status', 0));
                    $project->status_id = $activeStatus;
                    $project->save();
                }
            }
        });
        Freelancer::AddTab(FreelancerInfoTab::class);
    }


    protected function registerStatistics ()
    {
        DashboardBuilder::AddStatistic(Statistic::make(__('Members'), Member::count())->icon('users'));
        DashboardBuilder::AddStatistic(Statistic::make(__('Freelancers'), Freelancer::count())->icon('users'));
        DashboardBuilder::AddStatistic(Statistic::make(__('Companies'), Company::count())->icon('users'));
    }


    protected function registerSettings()
    {
        Settings::on('app', function(){
            Settings::page(__('Membership'), 'membership', function(){
                Settings::group(__('Default Roles'), function(){
                    Settings::multiple(__('Freelancer'), 'default_roles.freelancer', function(){
                        return Role::all()->pluck('name', 'id')->toArray();
                    });
                    Settings::multiple(__('Member'), 'default_roles.member', function(){
                        return Role::all()->pluck('name', 'id')->toArray();
                    });
                    Settings::multiple(__('Company'), 'default_roles.company', function(){
                        return Role::all()->pluck('name', 'id')->toArray();
                    });
                });
            });

            Settings::page(__('Projects'), 'projects', function(){
                Settings::group(__('Assignment'), function(){
                    Settings::select(__('Automatic Assignment'), 'assignment.automatic_mode', function(){
                        return [ '0' => __('Disabled'), '1' => __('Enabled') ];
                    });
                });
                Settings::group(__('Pricing'), function(){
                    Settings::number(__('Free Calls Minutes'), 'calls.free_minutes', false);
                    Settings::text(__('Call Hour Price'), 'calls.hour_price', false);
                });
            });

            Settings::page(__('Terms'), 'terms', function(){
                Settings::group(__('Terms Customization'), function(){
                    Settings::editor(__('Estimation Terms'), 'estimation');
                    Settings::editor(__('Contract Terms'), 'contract');
                    Settings::editor(__('Application Terms'), 'application');
                });
            });
            Settings::page(__('Contracts'), 'contracts', function(){
                Settings::group(__('Terms Customization'), function(){
                    Settings::editor(__('Content'), 'content');
                });
            });
        });

    }

    protected function registerExtensions()
    {
        System::ExtendView(
            'projects::dashboard.projects.tabs.details',
            'card::setup.end',
            'speeddesign::dashboard.extensions.projects.setup');
        System::ExtendView('people::dashboard.person.tabs.basic', 'secondary::end', function($data){
            if(isset($data['type']) && $data['type']->type() == Freelancer::class)
            {
                return view('speeddesign::dashboard.extensions.people.freelancer.rating');
            }
            return;
        });
        System::ExtendView('site::frontend.components.contact_form', 'before::inputs.message', 'speeddesign::frontend.extensions.contact_form');
        GalleryService::InjectInto(Sponsor::class);
        System::ExtendView('site::dashboard.posts.upsert', 'card::content.after', 'speeddesign::dashboard.extensions.sponsors.gallery');
        Action::On(PostAfterStoreAction::class, SponsorStore::class);

        System::ExtendView('projects::dashboard.projects.tabs.overview', 'card::members.after', function(){
            return System::FindView('speeddesign::dashboard.extensions.projects.team');
        });

        System::ExtendView('projects::dashboard.projects.tabs.overview', 'card::members.after', function(){
            return System::FindView('speeddesign::dashboard.extensions.projects.estimation_request');
        });
        TeamPage::AddTab(TeamProjectsTab::class);

        Action::On(SystemReadyAction::class, function(){
            if(auth()->check() && auth()->user()->canSwitch())
            {
                UsersDatatable::Extender()->addAction(function(User $user){
                    $actions = [];
                    if($user->canBeSwitched())
                    {
                        $actions[] = Generic::make('switch', __('Switch to'))->OnExecute(function(User $user){
                            $switched = auth()->user()->switch($user);
                            if($switched)
                            {
                                return Response::redirectTo('/');
                                die();
                            }
                        });
                    }
                    return $actions;
                });
            }
        });

        ProjectViewPage::AddTab(ProjectCallsTab::class);
    }

    protected function registerUsers()
    {
        PeopleService::new(Member::class, 'member')
        ->title(__('Members Management'))
        ->plural(__('Members'))
        ->singular(__('Member'))
        ->defaultRoles(get_setting('app::membership.default_roles.member', [], false))
        ->features(
            PersonFeature::Name, PersonFeature::Thumbnail, PersonFeature::Email,
            PersonFeature::Phone, PersonFeature::Password,
        )->showInMenu(true);

        PeopleService::new(Freelancer::class, 'freelancer')
        ->title(__('Freelancers Management'))
        ->plural(__('Freelancers'))
        ->singular(__('Freelancer'))
        ->defaultRoles(get_setting('app::membership.default_roles.freelancer', [], false))
        ->icon('user-tie')
        ->features(
            PersonFeature::Name, PersonFeature::Thumbnail, PersonFeature::Email,
            PersonFeature::Phone, PersonFeature::Password,
        )->showInMenu(true);

        PeopleService::new(Company::class, 'company')
        ->title(__('Companies Management'))
        ->plural(__('Companies'))
        ->singular(__('Company'))
        ->defaultRoles(get_setting('app::membership.default_roles.company', [], false))
        ->icon('house-user')
        ->features(
            PersonFeature::Name, PersonFeature::Thumbnail, PersonFeature::Email,
            PersonFeature::Phone, PersonFeature::Password,
        )->showInMenu(true);
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        System::EnableService(PortfolioFrontendService::class);
        System::EnableService(ProfileFrontendService::class);
        System::EnableService(ProjectEstimationService::class);
        System::EnableService(ForceLoginService::class);
        System::EnableService(PagesService::class);
        System::EnableService(TwilioChatService::class);
        $this->registerNotifications();
        ForceLoginMiddleware::Except('page/privacy-policy');
        ForceLoginMiddleware::Except('calls/request');
        ForceLoginMiddleware::Except('calls/status');
        ForceLoginMiddleware::Except('calls/record');
        ForceLoginMiddleware::Except('portfolio/*');
        ForceLoginMiddleware::Except('sponsors/*');
        $this->app->register(RouteServiceProvider::class);
    }

    protected function registerNotifications()
    {
        NotificationService::Register(
            __('Project Estimation Request'), 'project-estimation-request', ProjectEstimationRequestNotification::class
        )->title(__('Project Estimation Request'));
        NotificationService::Register(
            __('Project Estimation Recieved'), 'project-estimation-recieved', ProjectEstimatedNotification::class
        )->title(__('Project Estimation Recieved'));
    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->publishes([
            module_path($this->moduleName, 'Config/config.php') => config_path($this->moduleNameLower . '.php'),
        ], 'config');
        $this->mergeConfigFrom(
            module_path($this->moduleName, 'Config/config.php'), $this->moduleNameLower
        );
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $viewPath = resource_path('views/modules/' . $this->moduleNameLower);

        $sourcePath = module_path($this->moduleName, 'Resources/views');

        $this->publishes([
            $sourcePath => $viewPath
        ], ['views', $this->moduleNameLower . '-module-views']);

        $this->loadViewsFrom(array_merge($this->getPublishableViewPaths(), [$sourcePath]), $this->moduleNameLower);
    }

    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $langPath = resource_path('lang/modules/' . $this->moduleNameLower);

        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, $this->moduleNameLower);
        } else {
            $this->loadTranslationsFrom(module_path($this->moduleName, 'Resources/lang'), $this->moduleNameLower);
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }

    private function getPublishableViewPaths(): array
    {
        $paths = [];
        foreach (\Config::get('view.paths') as $path) {
            if (is_dir($path . '/modules/' . $this->moduleNameLower)) {
                $paths[] = $path . '/modules/' . $this->moduleNameLower;
            }
        }
        return $paths;
    }
}
