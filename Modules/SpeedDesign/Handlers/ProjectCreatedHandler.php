<?php

namespace Modules\SpeedDesign\Handlers;

use Modules\Projects\Events\ProjectCreated;
use Modules\SpeedDesign\Notifications\ProjectEstimationRequestNotification;

class ProjectCreatedHandler
{
    public function handle(ProjectCreated $event)
    {
        //Send Estimation
        ProjectEstimationRequestNotification::Send($event->project);
    }
}
