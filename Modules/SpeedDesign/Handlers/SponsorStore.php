<?php

namespace Modules\SpeedDesign\Handlers;

use Cascade\Site\Entities\Post;
use Cascade\System\Abstraction\ActionHandler;
use Modules\Sponsors\Entities\Sponsor;

class SponsorStore extends ActionHandler
{
    public function Handle(Post $item)
    {
        if(!($item instanceof Sponsor)) { return; }
        $item->StoreGalleryFromRequest();
    }
}
