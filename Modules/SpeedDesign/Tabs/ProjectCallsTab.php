<?php

namespace Modules\SpeedDesign\Tabs;

use Cascade\Dashboard\Abstraction\DashboardTab;
use Cascade\System\Services\System;
use Illuminate\View\View;
use Modules\SpeedDesign\Entities\TwilioCallLog;

class ProjectCallsTab extends DashboardTab
{
    public function name(): string
    {
        return __('Calls');
    }

    public function slug(): string
    {
        return 'calls';
    }

    public function show(): bool
    {
        return true;
    }

    public function view(): View|string
    {
        return System::FindView('speeddesign::dashboard.projects.calls', [
            'calls' => TwilioCallLog::where('reference_id', $this->data['project']->id)->get()
        ]);
    }
}
