<?php

namespace Modules\SpeedDesign\Tabs;

use Cascade\Dashboard\Abstraction\DashboardTab;
use Cascade\System\Services\System;
use Illuminate\View\View;

class FreelancerInfoTab extends DashboardTab
{
    public function name(): string
    {
        return __('Additional Info');
    }

    public function slug(): string
    {
        return 'additional-info';
    }

    public function view(): View|string
    {
        return System::FindView('speeddesign::dashboard.extensions.people.freelancer.info', $this->data);
    }

    public function order(): int
    {
        return 100;
    }
}
