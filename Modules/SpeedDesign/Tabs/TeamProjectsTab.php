<?php

namespace Modules\SpeedDesign\Tabs;

use Cascade\Dashboard\Abstraction\DashboardTab;
use Cascade\System\Services\System;
use Illuminate\View\View;
use Modules\SpeedDesign\Tables\TeamsProjectsTable;

class TeamProjectsTab extends DashboardTab
{
    public function name(): string
    {
        return __('Projects');
    }

    public function slug(): string
    {
        return 'team-projects';
    }

    public function view(): View|string
    {
        return System::FindView('speeddesign::dashboard.teams.tabs.projects', [
            'table' => TeamsProjectsTable::class
        ]);
    }
}
