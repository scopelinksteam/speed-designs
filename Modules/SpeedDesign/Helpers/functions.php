<?php

use Modules\Projects\Entities\Project;
use Modules\SpeedDesign\Entities\Company;
use Modules\SpeedDesign\Entities\Freelancer;
use Modules\SpeedDesign\Entities\Member;
use Modules\SpeedDesign\Entities\TwilioCallLog;

function sd_get_requests()
{
    $orders = [];
    $activeUser = auth()->user()->impersonate();
    $employeesRoles = get_setting('app::membership.default_roles.freelancer', [], false);
    if (count(array_intersect($activeUser->roles->pluck('id')->toArray(), $employeesRoles)) > 0)
    {
        $orders = Project::where('status_id', get_setting('projects::general.setup.default_status', 0))
            ->whereDoesntHave('estimations', function ($estimationQuery) {
                return $estimationQuery->where('user_id', auth()->id());
            })->whereDate('created_at', '>=', Carbon\Carbon::now()->subDays(1))->get();
    }
    return $orders;
}

function sd_get_project_call_balance($projectId)
{
    $project = Project::find($projectId);
    if(!$project) { return 0; }
    $balanceInSeconds = intval(get_setting('app::projects.calls.free_minutes', 60)) * 60;
    $balanceInSeconds +=  intval($project->Attribute('calls_balance', 0));
    foreach(TwilioCallLog::where('reference_id', $projectId)->get() as $log)
    {
        $balanceInSeconds -= intval($log->duration);
    }
    return $balanceInSeconds < 0 ? 0 : $balanceInSeconds;
}

function sd_add_project_call_balance($projectId, $balance = 3600)
{
    $project = Project::find($projectId);
    if(!$project) { return 0; }
    $currentActiveBalance = intval($project->Attribute('calls_balance', 0));
    $project->storeAttribute('calls_balance', $currentActiveBalance + $balance);
}

function sd_get_project_call_balance_formatted($projectId)
{
    $seconds = sd_get_project_call_balance($projectId);
    if($seconds == 0) { return '00:00:00'; }
    return sprintf('%02d:%02d:%02d', ($seconds/ 3600),($seconds/ 60 % 60), $seconds% 60);
}


function sd_is_customer()
{
    if(!auth()->check()) { return false; }
    $activeUser = auth()->user()->impersonate();
    return ($activeUser instanceof Company || $activeUser instanceof Member);
}

function sd_is_freelancer()
{
    if(!auth()->check()) { return false; }
    $activeUser = auth()->user()->impersonate();
    return ($activeUser instanceof Freelancer);
}
