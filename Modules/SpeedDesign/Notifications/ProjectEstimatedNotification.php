<?php

namespace Modules\SpeedDesign\Notifications;

use Cascade\System\Abstraction\NotificationBase;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Modules\Projects\Entities\ProjectEstimation;

class ProjectEstimatedNotification extends NotificationBase
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(public ProjectEstimation $estimation)
    {

    }

    public function notifiables()
    {
        return $this->estimation->project->customer->impersonate();
    }

    public function reference()
    {
        return $this->estimation;
    }


    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('New Estimation Recieved.')
                    ->action('View and Approve Estimation', route('estimation.approve', ['estimation' => $this->estimation]))
                    ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
