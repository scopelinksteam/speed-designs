<?php

namespace Modules\SpeedDesign\Notifications;

use Cascade\System\Abstraction\NotificationBase;
use Cascade\System\Services\System;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\Arr;
use Modules\Projects\Entities\Project;
use Modules\SpeedDesign\Entities\Freelancer;

class ProjectEstimationRequestNotification extends NotificationBase
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(public Project $project, public $notifiables = [])
    {

    }

    public function notifiables()
    {
        if(!empty($this->notifiables)) { return $this->notifiables; }
        $designerCode = $this->project->Attribute('designer_code', []);
        $designers = Freelancer::whereIn('id', Arr::wrap($designerCode))->get();
        //No Designers Assigned ?
        if($designers->count() == 0)
        {
            //Automatic Assignment to all Freelancers ?
            if(intval(get_setting('app::projects.assignment.automatic_mode', '1')) == 1)
            { $designers = Freelancer::all(); }
        }
        //Found Designers ?
        return $designers;
    }


    public function reference()
    {
        return $this->project;
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('New Project Requires Estimation.')
                    ->action('Write Estimation', route('estimation.create', ['project' => $this->project]))
                    ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [

        ];
    }
}
