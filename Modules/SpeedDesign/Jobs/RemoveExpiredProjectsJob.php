<?php

namespace Modules\SpeedDesign\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Modules\Projects\Entities\Project;

class RemoveExpiredProjectsJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $defaultStatus = get_setting('projects::general.setup.default_status', 0, false);
        $expired = Project::where('status_id', $defaultStatus)
            ->whereDate('created_at', '<=', \Carbon\Carbon::now()->subDays(1))->get();
        foreach($expired as $project)
        {
            $project->delete();
        }
    }
}
