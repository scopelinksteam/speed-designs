<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('twilio_call_log', function (Blueprint $table) {
            $table->id();
            $table->nullableMorphs('reference');
            $table->unsignedBigInteger('caller_id')->default(0);
            $table->unsignedBigInteger('reciever_id')->default(0);
            $table->unsignedBigInteger('duration')->default(0);
            $table->string('record_file')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('twilio_call_log');
    }
};
