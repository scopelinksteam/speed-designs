<?php

namespace Modules\Portfolio\Services;

use Cascade\System\Abstraction\Service;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Support\Facades\Route;

class PortfolioFrontendService extends Service
{
    public function name(): string
    {
        return __('Porftolio Frontend');
    }

    public function description(): string
    {
        return __('Register Portfolio Frontend and Site Setup');
    }

    public function identifier(): string
    {
        return 'portfolio-frontend';
    }

    public function register(Application $app)
    {
        Route::middleware('web')->group(module_path('Portfolio', 'Routes/frontend.php'));
    }
}
