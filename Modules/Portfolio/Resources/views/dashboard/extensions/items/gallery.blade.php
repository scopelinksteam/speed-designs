@if($type->type() == \Modules\Portfolio\Entities\PortfolioItem::class)
<div class="card" id="gallery-builder-card">
    <div class="card-header">
        <i class="fa fa-image mr-2"></i>
        @lang('Portfolio Gallery')
    </div>
    <div class="card-body">
        <x-gallery-builder :gallery="isset($post) ? $post->gallery : null" :gridColumns="1" />
    </div>
</div>
@endif
