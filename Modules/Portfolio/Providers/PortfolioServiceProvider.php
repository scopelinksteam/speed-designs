<?php

namespace Modules\Portfolio\Providers;

use Cascade\Site\Actions\PostAfterStoreAction;
use Cascade\Site\Enums\PostFeature;
use Cascade\Site\Http\Controllers\Dashboard\PostsController;
use Cascade\Site\Services\PostType;
use Cascade\System\Services\Action;
use Cascade\System\Services\System;
use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;
use Modules\Portfolio\Entities\PortfolioItem;
use Modules\Portfolio\Handlers\PortfolioItemStore;

class PortfolioServiceProvider extends ServiceProvider
{
    /**
     * @var string $moduleName
     */
    protected $moduleName = 'Portfolio';

    /**
     * @var string $moduleNameLower
     */
    protected $moduleNameLower = 'portfolio';

    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerTranslations();
        $this->registerConfig();
        $this->registerViews();
        $this->loadMigrationsFrom(module_path($this->moduleName, 'Database/Migrations'));
        $this->registerPostTypes();
        $this->registerExtensions();
    }

    protected function registerExtensions()
    {
        System::ExtendView('site::dashboard.posts.upsert', 'card::content.after', 'portfolio::dashboard.extensions.items.gallery');
        Action::On(PostAfterStoreAction::class, PortfolioItemStore::class);
    }

    protected function registerPostTypes()
    {
        PostType::new(PortfolioItem::class, 'portfolio_item')
            ->title(__('Portfolio Items'))
            ->description(__('Showcase of Portfolio Projects and Achievements'))
            ->icon('award')
            ->showInMenu(true)
            ->features(
                PostFeature::Title, PostFeature::Content, PostFeature::Thumbnail,
                PostFeature::Category, PostFeature::MetaTitle, PostFeature::MetaDescription,
                PostFeature::MetaDescription,
            )
            ->requires(PostFeature::Title, PostFeature::Thumbnail)
            ;
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->publishes([
            module_path($this->moduleName, 'Config/config.php') => config_path($this->moduleNameLower . '.php'),
        ], 'config');
        $this->mergeConfigFrom(
            module_path($this->moduleName, 'Config/config.php'), $this->moduleNameLower
        );
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $viewPath = resource_path('views/modules/' . $this->moduleNameLower);

        $sourcePath = module_path($this->moduleName, 'Resources/views');

        $this->publishes([
            $sourcePath => $viewPath
        ], ['views', $this->moduleNameLower . '-module-views']);

        $this->loadViewsFrom(array_merge($this->getPublishableViewPaths(), [$sourcePath]), $this->moduleNameLower);
    }

    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $langPath = resource_path('lang/modules/' . $this->moduleNameLower);

        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, $this->moduleNameLower);
        } else {
            $this->loadTranslationsFrom(module_path($this->moduleName, 'Resources/lang'), $this->moduleNameLower);
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }

    private function getPublishableViewPaths(): array
    {
        $paths = [];
        foreach (\Config::get('view.paths') as $path) {
            if (is_dir($path . '/modules/' . $this->moduleNameLower)) {
                $paths[] = $path . '/modules/' . $this->moduleNameLower;
            }
        }
        return $paths;
    }
}
