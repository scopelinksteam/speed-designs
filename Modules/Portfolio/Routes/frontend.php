<?php

use Illuminate\Support\Facades\Route;
use Modules\Portfolio\Http\Controllers\Frontend\PortfolioController;

Route::prefix('portfolio')->name('portfolio.')->group(function(){
    Route::get('show/{item:slug}', [PortfolioController::class, 'show'])->name('show');
});
