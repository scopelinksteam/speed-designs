<?php

namespace Modules\Portfolio\Handlers;

use Cascade\Site\Entities\Post;
use Cascade\System\Abstraction\ActionHandler;
use Modules\Portfolio\Entities\PortfolioItem;

class PortfolioItemStore extends ActionHandler
{
    public function Handle(Post $item)
    {
        if(!($item instanceof PortfolioItem)) { return; }
        $item->StoreGalleryFromRequest();
    }
}
