<?php

namespace Modules\Portfolio\Entities;

use Cascade\Site\Entities\Post;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Modules\Galleries\Traits\HasGallery;

class PortfolioItem extends Post
{
    use HasGallery;

}
