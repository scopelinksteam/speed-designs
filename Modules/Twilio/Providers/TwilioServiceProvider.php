<?php

namespace Modules\Twilio\Providers;

use Cascade\Settings\Services\Settings;
use Cascade\System\Services\System;
use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;
use Livewire\Livewire;
use Modules\Twilio\Components\Livewire\TwilioChatroomComponent;
use Modules\Twilio\Directives\TwilioConversationScripts;

class TwilioServiceProvider extends ServiceProvider
{
    /**
     * @var string $moduleName
     */
    protected $moduleName = 'Twilio';

    /**
     * @var string $moduleNameLower
     */
    protected $moduleNameLower = 'twilio';

    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerTranslations();
        $this->registerConfig();
        $this->registerViews();
        $this->loadMigrationsFrom(module_path($this->moduleName, 'Database/Migrations'));
        $this->registerSettings();
        System::BladeDirective('twilioConversationScripts', TwilioConversationScripts::class);
        Livewire::component('twilio-chatroom', TwilioChatroomComponent::class);
    }

    protected function registerSettings()
    {
        Settings::on('integrations', function(){
            Settings::page(__('Twilio'), 'twilio', function(){
                Settings::group(__('Credentials'), function(){
                    Settings::note(__('To Get your Account Credentials Please Visit Twilio Console'). ' : <a href="https://console.twilio.com/" target="_blank">'.__('Twilio Console').'</a>');
                    Settings::text(__('Account SID'), 'credentials.account_sid');
                    Settings::text(__('Auth Token'), 'credentials.auth_token');
                });
            });
        });
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->publishes([
            module_path($this->moduleName, 'Config/config.php') => config_path($this->moduleNameLower . '.php'),
        ], 'config');
        $this->mergeConfigFrom(
            module_path($this->moduleName, 'Config/config.php'), $this->moduleNameLower
        );
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $viewPath = resource_path('views/modules/' . $this->moduleNameLower);

        $sourcePath = module_path($this->moduleName, 'Resources/views');

        $this->publishes([
            $sourcePath => $viewPath
        ], ['views', $this->moduleNameLower . '-module-views']);

        $this->loadViewsFrom(array_merge($this->getPublishableViewPaths(), [$sourcePath]), $this->moduleNameLower);
    }

    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $langPath = resource_path('lang/modules/' . $this->moduleNameLower);

        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, $this->moduleNameLower);
        } else {
            $this->loadTranslationsFrom(module_path($this->moduleName, 'Resources/lang'), $this->moduleNameLower);
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }

    private function getPublishableViewPaths(): array
    {
        $paths = [];
        foreach (\Config::get('view.paths') as $path) {
            if (is_dir($path . '/modules/' . $this->moduleNameLower)) {
                $paths[] = $path . '/modules/' . $this->moduleNameLower;
            }
        }
        return $paths;
    }
}
