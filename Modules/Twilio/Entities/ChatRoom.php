<?php

namespace Modules\Twilio\Entities;

use Carbon\Carbon;
use Cascade\System\Traits\Joinable;
use Cascade\Users\Entities\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Modules\Twilio\Foundation\Conversations;
use stdClass;
use Twilio\Rest\Conversations\V1\Service\Conversation\MessageInstance;

class ChatRoom extends Model
{
    use Joinable;

    protected $table = 'twilio_chat_rooms';

    const ROOM_PREFIX = 'chatroom';

    public static function Of(User ...$users) : static
    {
        $room = static::with('members');

        foreach($users as $user)
        {
            $directUser = $user->impersonate();
            $room->whereHas('members', function($membersQuery) use($directUser){
                $membersQuery->where('member_type', get_class($directUser))->where('member_id', $directUser->id);
            });
        }
        $room = $room->first();

        if($room == null)
        {
            //Create New
            $room = new static;
            $room->name = static::ROOM_PREFIX.'_'.$room->id;
            $room->save();
            $room->slug = 'unique_name';
            $room->save();
            $room->name = $room->name . $room->id;
            $room->save();
            $conversation = (new Conversations())->create($room->name);
            $room->sid = $conversation->sid;
            $room->save();
            (new Conversations())->addParticipants($conversation->sid, ...$users);

            foreach($users as $user)
            {
                $user->join($room, 'participants');
            }
        }
        return $room;
    }

    public function scopeMine(Builder $builder)
    {
        $builder->with('members');

        $directUser = auth()->user()->impersonate();
        $builder->whereHas('members', function($membersQuery) use($directUser){
            $membersQuery->where('member_type', get_class($directUser))->where('member_id', $directUser->id);
        });
        return $builder;
    }

    public function getConversationAttribute()
    {
        if(empty($this->sid)) { return null; }
        return (new Conversations())->fetch($this->sid);
    }

    public function getMessagesAttribute()
    {
        if(empty($this->sid)) { return null; }
        $data = (new Conversations())->messages($this->sid);
        $messages = [];
        foreach($data as $record)
        {
            $obj = new stdClass;
            $obj->sid = $record->sid;
            $obj->account_sid = $record->accountSid;
            $obj->chat_service_sid = $record->chatServiceSid;
            $obj->conversation_sid = $record->conversationSid;
            $obj->body = $record->body;
            $obj->media = $record->media;
            $obj->author = $record->author;
            $obj->participant_sid = $record->participantSid;
            $obj->date_created = Carbon::parse($record->dateCreated);
            $obj->date_updated = Carbon::parse($record->dateUpdated);
            $obj->index = $record->index;
            $messages[] = $obj;
        }
        return $messages;
    }

    public function getRoomTitleAttribute()
    {
        $members = $this->members;
        if($members->count() == 0) { return __('Invalid Room'); }
        $participants = [];
        foreach($members as $member)
        {
            $user = $member->member;
            if(!$user) { continue; }
            if($user->id == auth()->user()->id) { continue; }
            $participants[] = $user->impersonate()->display_name;
        }
        return count($participants) == 0 ? __('Invalid Room') : implode(', ', $participants);
    }
}
