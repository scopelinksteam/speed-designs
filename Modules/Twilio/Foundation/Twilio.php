<?php

namespace Modules\Twilio\Foundation;

use Twilio\Rest\Client as TwilioClient;
use Twilio\Rest\Conversations\V1 as TwilioConversations;

class Twilio
{

    protected static ?TwilioClient $client = null;

    public static function Client($forceConnect = false) : TwilioClient
    {
        if(!$forceConnect && static::$client != null)
        { return static::$client; }
        $sid = get_setting('integrations::twilio.credentials.account_sid', '', false);
        $accountToken = get_setting('integrations::twilio.credentials.auth_token', '', false);
        $tempClient = new TwilioClient($sid, $accountToken);
        //Assign Default Client
        if(!$forceConnect){ static::$client = $tempClient; }
        return $tempClient;
    }

    public static function Conversations() : TwilioConversations
    {
        return static::Client()->conversations->v1;
    }
}
