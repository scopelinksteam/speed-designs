<?php

namespace Modules\Twilio\Foundation;

use Cascade\Users\Entities\User;
use Twilio\Rest\Client;
use Twilio\Rest\Conversations\V1 as ConversationsClient;
class Conversations
{
    protected Client $twilio;

    protected ConversationsClient $conversationsClient;

    public function __construct()
    {
        $this->twilio = Twilio::Client();
        $this->conversationsClient = Twilio::Conversations();
    }

    public function create($name)
    {
        $serviceId = get_setting('integrations::twilio.chat.service_id', '', false);
        $conversation = $this->conversationsClient->services($serviceId)->conversations->create([
            'friendlyName' => $name,
            'chatServiceSid' => $serviceId,
        ]);
        return $conversation;
    }

    public function fetch($sid)
    {
        $serviceId = get_setting('integrations::twilio.chat.service_id', '', false);
        return $this->conversationsClient->services($serviceId)->conversations($sid)->fetch();
    }

    public function messages($sid, $limit = 20)
    {
        $serviceId = get_setting('integrations::twilio.chat.service_id', '', false);
        return $this->conversationsClient->services($serviceId)->conversations($sid)->messages->read([], $limit);
    }

    public function addParticipants($sid, User ...$users)
    {
        $conversation = $this->fetch($sid);
        foreach($users as $user)
        {
            $conversation->participants->create([
                'identity' => $user->id
            ]);
        }
    }
}
