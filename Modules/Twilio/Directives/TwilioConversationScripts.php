<?php

namespace Modules\Twilio\Directives;

use Cascade\System\Directives\ActionDirective;

class TwilioConversationScripts
{
    public function handle($expression)
    {
        $scripts = [
            module_url('Twilio', 'js/twilio-conversations.min.js'),
            module_url('Twilio', 'js/twilio-chat.js'),
        ];

        $content = '';
        foreach($scripts as $script)
        {
            $content .= '<script src="'.$script.'"></script>'."\n";
        }
        return $content;
    }
}
