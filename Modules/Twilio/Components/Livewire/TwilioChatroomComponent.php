<?php

namespace Modules\Twilio\Components\Livewire;

use Cascade\System\Services\System;
use Livewire\Component;
use Modules\Twilio\Entities\ChatRoom;
use Modules\Twilio\Foundation\Conversations;
use Modules\Twilio\Foundation\Twilio;
use Twilio\Rest\Conversations\V1\Service\ConversationInstance;

class TwilioChatroomComponent extends Component
{

    public ChatRoom $room;

    public $newMessageContent = '';

    protected ConversationInstance  $conversation;

    public function mount(ChatRoom $room)
    {
        $this->room = $room;
        $this->conversation = $this->room->conversation;
    }

    protected function EnsureInitialized()
    {
        if(!isset($this->conversation))
        {
            $this->conversation = $this->room->conversation;
        }
    }

    public function GetMessages()
    {
        $this->EnsureInitialized();
        return $this->room->messages;
    }

    public function SendMessage()
    {
        if(empty($this->newMessageContent)){return;}
        $this->EnsureInitialized();
        $content = $this->newMessageContent;
        $this->newMessageContent = '';
        $this->conversation->messages->create([
            'author' => auth()->id(),
            'body' => $content
        ]);
    }

    public function render()
    {
        return System::FindView('twilio::shared.components.chatroom', [
            'messages' => $this->GetMessages(),
        ]);
    }
}
