<?php

namespace Modules\Twilio\Services;

use Cascade\Settings\Services\Settings;
use Cascade\System\Abstraction\Service;
use Cascade\System\Http\Middleware\ForceLoginMiddleware;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Modules\Twilio\Http\Controllers\Webhooks\ConversationsController;

class TwilioChatService extends Service
{
    public function name(): string
    {
        return __('Twilio Chat & Conversations');
    }

    public function identifier(): string
    {
        return 'twilio-conversations';
    }

    public function description(): string
    {
        return __('Enables Twilio Chat and Conversations Based Between System Users');
    }

    public function register(Application $app)
    {
        Settings::on('integrations.twilio', function(){
            Settings::group(__('Conversational Chat'), function(){
                Settings::note(__('Need a new Chat API Credentials ? Please Visit Twilio Console'). ' : <a href="https://www.twilio.com/console/chat/project/api-keys/create" target="_blank">'.__('Chat API Keys').'</a>');
                Settings::text(__('API Key'), 'chat.api_key');
                Settings::text(__('API Secret'), 'chat.api_secret');
                Settings::note(__('To Create a New Conversation Service Please Visit Twilio Console'). ' : <a href="https://console.twilio.com/?frameUrl=%2Fconsole%2Fconversations%2Fservices%3Fx-target-region%3Dus1" target="_blank">'.__('Conversation Services').'</a>');
                Settings::text(__('Service ID'), 'chat.service_id');
            });
        });
        //Routes
        Route::prefix('twilio')->name('twilio.')->group(function(){
            //Twilio Web Hooks
            Route::prefix('webhooks')->name('webhooks.')->group(function(){
                Route::prefix('conversations')->name('conversations.')->group(function(){
                    Route::post('/pre', [ConversationsController::class, 'PreAction'])->name('pre');
                    Route::post('/post', [ConversationsController::class, 'PostAction'])->name('post');
                });
            });
        });
        //Remove Webhooks from Authentication System
        ForceLoginMiddleware::Except('twilio/webhooks*');
    }
}
