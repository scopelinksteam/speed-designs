<?php

namespace Modules\Teams\Http\Controllers\Dashboard;

use Cascade\System\Entities\Member;
use Cascade\System\Services\Feedback;
use Cascade\Users\Entities\User;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Gate;
use Modules\Teams\Entities\Team;
use Modules\Teams\Entities\TeamLabel;
use Modules\Teams\Enums\MemberRole;
use Modules\Teams\Enums\TeamStatus;
use Modules\Teams\Http\Requests\InviteTeamMemberRequest;
use Modules\Teams\Http\Requests\TeamStoreRequest;
use Modules\Teams\Pages\TeamPage;
use Modules\Teams\Tables\TeamsTable;

class TeamsController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        return view('teams::dashboard.index', [
            'table' => TeamsTable::class,
            'owners' => User::where('id', '<>', auth()->id())->get(),
            'labels' => TeamLabel::all()
        ]);
    }

    /**
     * Display User Joined Teams
     *
     * @return Renderable
     */
    public function my()
    {
        return view('teams::dashboard.my-teams', [
            'teams' => Team::whereHas('members', function($membersQuery){
                return $membersQuery->where('member_id', auth()->id());
            })->get()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(TeamStoreRequest $request)
    {
        $entityId = $request->integer('entity_id');
        $entity = $entityId > 0 ? Team::find($entityId) : new Team();
        $entity->name = $request->name;
        $entity->description = $request->description ?? '';
        $entity->status = TeamStatus::from($request->integer('status'));
        $entity->owner_id = $request->integer('owner');
        $user = User::find($entity->owner_id) ?? auth()->user();
        $entity->owner_type = $user == null ? User::class : get_class($user->impersonate());
        if($request->file('image'))
        { $entity->image_file = $request->file('image')->store('teams/images', ['disk' => 'uploads']); }
        if($request->file('cover'))
        { $entity->cover_image = $request->file('cover')->store('teams/covers', ['disk' => 'uploads']); }
        $entity->save();
        $entity->labels()->sync($request->input('labels', []));
        auth()->user()->join($entity, MemberRole::Owner->value);
        Feedback::success(__('Team Saved Successfully'));
        Feedback::flash();
        return to_route('dashboard.teams.index');
    }

    /**
     * Current user Leave Selected Team
     *
     * @param Team $team
     * @return Renderable
     */
    public function leave(Team $team)
    {
        $left = auth()->user()->leave($team);
        if($left) { Feedback::success(__('You Left the Group Successfully')); }
        else { Feedback::alert(__('You are not a member of selected team'));}
        Feedback::flash();
        return to_route('dashboard.index');
    }

    /**
     * Current user Leave Selected Team
     *
     * @param Team $team
     * @return Renderable
     */
    public function remove(Team $team, User $user)
    {
        $left = $user->leave($team);
        if($left) { Feedback::success(__('Member Removed from the Group Successfully')); }
        else { Feedback::alert(__('Selected Member was not a member of selected team'));}
        Feedback::flash();
        if(auth()->user()->joined($team))
        {
            return to_route('dashboard.teams.show', ['team' => $team]);
        }
        return to_route('dashboard.index');
    }

    /**
     * Current user Leave Selected Team
     *
     * @param Team $team
     * @return Renderable
     */
    public function invite(InviteTeamMemberRequest $request)
    {
        $user = User::find($request->integer('member_id'));
        if($user == null)
        {
            Feedback::error(__('Cannot Find Selected Member'));
            Feedback::flash();
            return back();
        }
        $team = Team::find($request->integer('team_id'));
        if($team == null)
        {
            Feedback::error(__('Cannot Find Selected Team'));
            Feedback::flash();
            return back();
        }

        $user->join($team, $request->role);
        Feedback::success(__('Selected Member added successfully to the selected team'));
        Feedback::flash();
        return back();
    }

    /**
     * Current user Leave Selected Team
     *
     * @param Team $team
     * @return Renderable
     */
    public function promote(Request $request)
    {
        $membership = Member::find($request->integer('member_id'));
        if($membership == null)
        {
            Feedback::error(__('Cannot Find Selected Member'));
            Feedback::flash();
            return back();
        }
        $membership->group = $request->role;
        $membership->save();
        Feedback::success(__('Selected Member Promoted successfully to the selected role'));
        Feedback::flash();
        return back();
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show(Team $team)
    {
        return view('teams::dashboard.show', [
            'team' => $team,
            'tabs' => TeamPage::GetTabs(),
            'invitables' => User::whereNotIn('id', $team->members()->pluck('member_id')->toArray())->get()
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit(Team $team)
    {
        return $this->index()->with([
            'entity' => $team
        ]);
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy(Team $team)
    {
        Gate::authorize('delete teams');
        $team->delete();
        Feedback::alert(__('Team Deleted Successfully'));
        Feedback::flash();
        return to_route('dashboard.teams.index');
    }
}
