<?php

namespace Modules\Teams\Tables;

use Cascade\Dashboard\Services\Datatables\Action;
use Cascade\Dashboard\Services\Datatables\Column;
use Cascade\Dashboard\Services\Datatables\DataTable;
use Illuminate\Support\Facades\Gate;
use Modules\Teams\Entities\Team;

class TeamsTable extends DataTable
{

    public function __construct()
    {
        $this->editColumn('name', function(Team $team){
            $name = '';
            $name .= $this->badge($team->status->translated(), 'primary me-2'). $team->name;
            $name .= '<br />';
            $name .= '<small class="text-muted">'.$team->description.'</small>';
            if(!empty($name)) { $name .= '<br />'; }
            foreach($team->labels as $label)
            {
                $name .= sprintf('<span class="text-white py-1 px-4 rounded-4 d-inline-block mt-2 small" style="background-color:%s;">%s</span>', $label->color, $label->name);
            }
            return $name;
        });
        $this->editColumn('members', function(Team $team){
            return $this->badge($team->members()->count(), 'dark');
        });

    }

    public function query()
    {
        return Team::query();
    }

    public function columns()
    {
        return [
            Column::make('name')->text(__('Name')),
            Column::make('members')->text(__('Members'))
        ];
    }

    public function actions(Team $team)
    {
        $actions = [];
        if(Gate::allows('edit teams'))
        {
            $actions[] = Action::make('edit', __('Edit'))->route('dashboard.teams.edit', ['team' => $team])->icon('fas fa-pencil');
        }
        if(Gate::allows('delete teams'))
        {
            $actions[] = Action::make('delete', __('Delete'))->route('dashboard.teams.delete', ['team' => $team])->icon('fas fa-times');
        }

        return $actions;
    }
}
