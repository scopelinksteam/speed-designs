<?php

namespace Modules\Teams\Builders;

use Cascade\System\Entities\Label;
use Cascade\System\Enums\LabelFeatures;
use Cascade\System\Services\Generators\LabelBuilder;
use Modules\Teams\Entities\Team;
use Modules\Teams\Entities\TeamLabel;
use Modules\Teams\Pages\TeamPage;
use Modules\Teams\Tabs\TeamMembersTab;
use Modules\Teams\Tabs\TeamOverviewTab;

class TeamBuilder
{
    public static function register()
    {
        static::registerLabels();
        static::registerTabs();
    }

    protected static function registerTabs()
    {
        TeamPage::AddTab(TeamOverviewTab::class);
        TeamPage::AddTab(TeamMembersTab::class);
    }

    protected static function registerLabels()
    {
        Label::register(
            LabelBuilder::make(__('Teams Labels'), TeamLabel::class)
                ->features(LabelFeatures::Name, LabelFeatures::Color)
                ->slug('teams-labels')
                ->description(__('Labelize and categorize your teams for better organization'))
                ->title(__('Teams Labels')),
            'teams-labels'
        );
    }
}
