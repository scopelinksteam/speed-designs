<?php

namespace Modules\Teams\Entities;

use Cascade\System\Entities\Label;
use Illuminate\Database\Eloquent\Builder;

class TeamLabel extends Label
{
    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope('typeConstrain', function(Builder $query){
            return $query->where('type', static::class);
        });
    }
}
