<?php

namespace Modules\Teams\Entities;

use Cascade\System\Traits\HasAttributes;
use Cascade\System\Traits\HasLabels;
use Cascade\System\Traits\Joinable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Modules\Teams\Enums\TeamStatus;
use Spatie\Permission\Traits\HasPermissions;
use Spatie\Permission\Traits\HasRoles;

class Team extends Model
{
    use HasAttributes, Joinable, HasRoles, HasPermissions, HasLabels;

    protected $table = 'teams';

    protected $casts = [
        'status' => TeamStatus::class
    ];

    public function owner()
    { return $this->morphTo('owner'); }

    public function getImageAttribute()
    {
        if(empty($this->image_file)) { return module_url('Teams', 'images/team-image-default.jpg'); }
        return uploads_url($this->image_file);
    }

    public function getCoverAttribute()
    {
        if(empty($this->cover_file)) { return module_url('Teams', 'images/team-cover-default.png'); }
        return uploads_url($this->cover_file);
    }
}
