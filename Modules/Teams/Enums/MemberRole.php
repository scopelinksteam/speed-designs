<?php

namespace Modules\Teams\Enums;

enum MemberRole : string
{
    case Owner = 'OWNER';
    case Administrator = 'ADMINISTRATOR';
    case Editor = 'EDITOR';
    case Member = 'MEMBER';
    case Guest = 'GUEST';
}
