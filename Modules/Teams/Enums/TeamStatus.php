<?php

namespace Modules\Teams\Enums;


enum TeamStatus : int
{
    case Active = 0;
    case Inactive = 1;

    public function translated()
    {
        $value = '';
        switch($this)
        {
            case TeamStatus::Active : $value = __('Active'); break;
            case TeamStatus::Inactive : $value = __('Inactive'); break;
        }
        return $value;
    }
}
