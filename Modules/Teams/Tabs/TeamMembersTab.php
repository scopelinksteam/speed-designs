<?php

namespace Modules\Teams\Tabs;

use Cascade\Dashboard\Abstraction\DashboardTab;
use Cascade\System\Services\System;
use Illuminate\View\View;

class TeamMembersTab extends DashboardTab
{
    public function name(): string
    {
        return __('Members');
    }

    public function slug(): string
    {
        return 'team-members';
    }

    public function show(): bool
    {
        return true;
    }

    public function view(): View|string
    {
        return System::FindView('teams::dashboard.tabs.members');
    }
}
