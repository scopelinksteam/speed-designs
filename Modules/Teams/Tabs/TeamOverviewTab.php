<?php

namespace Modules\Teams\Tabs;

use Cascade\Dashboard\Abstraction\DashboardTab;
use Cascade\System\Services\System;
use Illuminate\View\View;

class TeamOverviewTab extends DashboardTab
{
    public function name(): string
    {
        return __('Overview');
    }

    public function slug(): string
    {
        return 'team-overview';
    }

    public function show(): bool
    {
        return true;
    }

    public function view(): View|string
    {
        return System::FindView('teams::dashboard.tabs.overview');
    }
}
