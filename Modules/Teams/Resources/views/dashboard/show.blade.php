@extends('admin::layouts.clean')
@section('page-title', __('Team') .' : '. $team->name)
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <x-dashboard-tabs :tabs="$tabs" :data="$__data"/>
        </div>
    </div>
</div>
@endsection
