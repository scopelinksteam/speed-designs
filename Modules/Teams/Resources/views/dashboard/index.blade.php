@extends('admin::layouts.master')
@section('page-title', __('Teams'))
@section('page-description', __('Manage teams of work and their members association'))
@section('page-actions')
<a href="{{route('dashboard.teams.labels.index')}}" class="btn btn-dark">
    <i class="fas fa-tag me-2"></i>
    @lang('Manage Labels')
</a>
@endsection
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-4">
            <div class="card">
                <form action="{{route('dashboard.teams.store')}}" method="POST" enctype="multipart/form-data">
                    <div class="card-header">
                        <div class="card-title">
                            <i class="fa fa-info me-2"></i>
                            @lang('Team Details')
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            <label for="name" class="form-label">@lang('Name')</label>
                            <input type="text" name="name" id="name" class="form-control" value="{{isset($entity) ? $entity->name : ''}}">
                        </div>
                        @if(isset($owners) && auth()->user()->can('assign teams owners'))
                        <div class="form-group">
                            <label for="owner" class="form-label">@lang('Owner')</label>
                            <select name="owner_id" id="owner" class="selectr">
                                @foreach ($owners as $owner)
                                <option value="{{$owner->id}}" @selected((isset($entity) && $entity->owner_id == $owner->id) || (!isset($entity) && auth()->id() == $owner->id))>{{$owner->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        @endisset
                        <div class="form-group">
                            <label for="status" class="form-label">@lang('Status')</label>
                            <select name="status" id="status" class="selectr">
                                @foreach (\Modules\Teams\Enums\TeamStatus::cases() as $case)
                                <option value="{{$case->value}}" @selected(isset($entity) && $entity->status == $case)>{{$case->translated()}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="labels" class="form-label">@lang('Assign Labels')</label>
                            <select name="labels[]" id="labels" class="selectr" multiple>
                                @foreach ($labels as $label)
                                <option value="{{$label->id}}" @selected(isset($entity) && $entity->labels->where('id', $label->id)->count() > 0)>{{$label->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="description" class="form-label">@lang('Description')</label>
                            <textarea name="description" id="description" rows="5" class="form-control">{{isset($entity) ? $entity->description : ''}}</textarea>
                        </div>
                        <div class="form-group">
                            <label for="image" class="form-label">@lang('Image')</label>
                            <input type="file" name="image" id="image" class="dropify-image" @isset($team) data-default-file="{{$team->image}}" @endisset>
                        </div>
                        <div class="form-group">
                            <label for="cover" class="form-label">@lang('Cover')</label>
                            <input type="file" name="cover" id="cover" class="dropify-image" @isset($team) data-default-file="{{$team->cover}}" @endisset>
                        </div>
                    </div>
                    <div class="card-footer">
                        @csrf
                        @isset($entity)
                        <input type="hidden" name="entity_id" value="{{$entity->id}}">
                        @endisset
                        <button class="btn btn-primary" type="submit">@lang('Save Team')</button>
                    </div>
                </form>
            </div>
        </div>
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <div class="card-title">
                        <i class="fa fa-users me-2"></i>
                        @lang('Teams')
                    </div>
                </div>
                <div class="card-body">
                    @livewire('datatable', ['table' => $table])
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
