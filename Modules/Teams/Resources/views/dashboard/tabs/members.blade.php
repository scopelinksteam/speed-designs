<div class="row align-items-center justify-content-between my-3">
    <div class="col-auto">
        <p class="text-muted m-0 small">@lang('Displaying') {{$team->members()->count()}} @lang('Team Members')</p>
    </div>
    <div class="col-auto">
        <a href="#" data-bs-toggle="offcanvas" data-bs-target="#new-member-canvas" class="btn btn-dark">
            <i class="fa fa-plus me-2"></i>
            @lang('New Member')
        </a>
    </div>
</div>
<div class="row">
    @foreach ($team->members as $member)
    <div class="col-md-4 mb-4">
        <div class="card h-100">
            <div class="card-body">
                <div class="row align-items-center">
                    <div class="col-md-4">
                        <img src="{{$member->member->avatar}}" alt="{{$member->member->name}}" class="img-fluid rounded-circle img-thumbnail">
                    </div>
                    <div class="col-md-8">
                        <h4 class="fw-bold d-flex align-items-center justify-content-between">
                            <span>{{$member->member->name}}</span>
                            <div class="dropdown">
                                <button class="btn btn-light" data-bs-toggle="dropdown"><i class="fas fa-ellipsis-h"></i></button>
                                <ul class="dropdown-menu dropdown-menu-dark">
                                    <li><a href="#" class="dropdown-item promote-button" data-id="{{$member->id}}" data-name="{{$member->member->name}}" data-role="{{$member->group}}">@lang('Promote')</a></li>
                                    <li><hr class="dropdown-divider"></li>
                                    <li><a href="{{route('dashboard.teams.remove', ['team' => $team, 'user' => $member->member])}}" class="dropdown-item">@lang('Remove')</a></li>
                                </ul>
                            </div>
                        </h4>
                        <p class="text-muted fw-bold mb-0">{{\Modules\Teams\Enums\MemberRole::from($member->group)->name}}</p>
                        <p class="text-muted mb-0 small">{{$member->created_at->diffForHumans()}}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endforeach
</div>

<div class="offcanvas offcanvas-end" id="new-member-canvas">
    <div class="offcanvas-header">
        <h5 class="offcanvas-title">@lang('New Team Member')</h5>
        <button class="btn-close" data-bs-dismiss="offcanvas"></button>
    </div>
    <div class="offcanvas-body">
        <form action="{{route('dashboard.teams.invite')}}" method="POST">
            <div class="form-group">
                <label for="member" class="form-label">@lang('Member')</label>
                <select name="member_id" id="member" class="selectr">
                    @foreach ($invitables as $invitable)
                    <option value="{{$invitable->id}}">{{$invitable->name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="role" class="form-label">@lang('Role')</label>
                <select name="role" id="role" class="selectr">
                    @foreach (\Modules\Teams\Enums\MemberRole::cases() as $role)
                    <option value="{{$role->value}}">{{$role->name}}</option>
                    @endforeach
                </select>
            </div>
            @csrf
            <input type="hidden" name="team_id" value="{{$team->id}}">
            <button type="submit" class="btn btn-dark mt-3">
                <i class="fa fa-plus me-2"></i>
                @lang('Add Selected Member')
            </button>
        </form>
    </div>
</div>


<div class="offcanvas offcanvas-end" id="edit-member-canvas">
    <div class="offcanvas-header">
        <h5 class="offcanvas-title">@lang('Promote Team Member')</h5>
        <button class="btn-close" data-bs-dismiss="offcanvas"></button>
    </div>
    <div class="offcanvas-body">
        <form action="{{route('dashboard.teams.promote')}}" method="POST">
            <div class="form-group">
                <h3 id="edit-member-name"></h3>
            </div>
            <div class="form-group">
                <label for="editRoleSelect" class="form-label">@lang('Role')</label>
                <select name="role" class="" id="editRoleSelect">
                    @foreach (\Modules\Teams\Enums\MemberRole::cases() as $role)
                    <option value="{{$role->value}}">{{$role->name}}</option>
                    @endforeach
                </select>
            </div>
            @csrf
            <input type="hidden" name="member_id" value="" id="editMemberInput">
            <input type="hidden" name="team_id" value="{{$team->id}}">
            <button type="submit" class="btn btn-dark mt-3">
                <i class="fa fa-plus me-2"></i>
                @lang('Promote Member')
            </button>
        </form>
    </div>
</div>

<script type="text/javascript">
    document.addEventListener('DOMContentLoaded', function(){
        const editCanvas = new bootstrap.Offcanvas('#edit-member-canvas');
        const editMemberName = document.getElementById('edit-member-name');
        const editMemberInput = document.getElementById('editMemberInput');
        const editRoleSelect = new Selectr(document.getElementById('editRoleSelect'));
        Array.from(document.querySelectorAll('.promote-button')).forEach(function(element){
            element.addEventListener('click', function(ev){
                editCanvas.show();
                editMemberName.innerText = ev.target.dataset.name;
                editRoleSelect.setValue(ev.target.dataset.role);
                editMemberInput.value = ev.target.dataset.id;
                ev.preventDefault();
                ev.stopPropagation();
            });
        });
    });
</script>
