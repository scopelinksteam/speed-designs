<div class="row">
    <div class="col-md-3">
        <div class="card">
            <img src="{{$team->image}}" alt="{{$team->name}}" class="card-img-top">
            <div class="card-header">
                <div class="card-title">
                    {{$team->name}}
                </div>
            </div>
            <ul class="list-group list-group-flush">
                <li class="list-group-item">
                    <b>@lang('Founded')</b>
                    <span class="ms-3 text-muted">{{$team->created_at}}</span>
                </li>
                <li class="list-group-item">
                    <b>@lang('Owner')</b>
                    <span class="ms-3 text-muted">{{$team->owner ? $team->owner->name : __('NA')}}</span>
                </li>
                <li class="list-group-item">
                    <b>@lang('Members')</b>
                    <span class="ms-3 text-muted">{{$team->members()->count()}}</span>
                </li>
            </ul>
        </div>
        <div class="card">
            <div class="card-header">
                <div class="card-title">@lang('Recent Members')</div>
            </div>
            <div class="card-body p-2">
                @foreach ($team->members()->orderBy('created_at', 'DESC')->limit(5)->get() as $recentMember)
                <div class="row align-items-center gx-2">
                    <div class="col-md-3">
                        <img src="{{$recentMember->member->avatar}}" alt="{{$recentMember->member->name}}" class="rounded-circle img-fluid img-thumbnail">
                    </div>
                    <div class="col-auto m-0">
                        <h4 class="fw-bold text-dark mb-0">{{$recentMember->member->name}}</h4>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
        @if(auth()->user()->joined($team))
        <a href="{{route('dashboard.teams.leave', ['team' => $team])}}" class="btn btn-danger btn-block w-100">@lang('Leave Team')</a>
        @endif
    </div>
    <div class="col-md-9">
        <div class="card">
            <img src="{{$team->cover}}" alt="{{$team->name}}" class="card-img-top">
            <div class="card-header">
                <div class="card-title">
                    @lang('Team Description')
                </div>
            </div>
            <div class="card-body">
                {{$team->description}}
            </div>
        </div>
        <div class="card">
            <div class="card-header">
                <div class="card-title">
                    @lang('Team Activities')
                </div>
            </div>
            <div class="card-body">
                <p class="text-center p-3 text-muted">
                    @lang('No Activities to show yet')
                </p>
            </div>
        </div>
    </div>
</div>
