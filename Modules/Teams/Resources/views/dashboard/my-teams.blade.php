@extends('admin::layouts.master')
@section('page-title', __('My Teams'))
@section('page-description', __('Browse and explore teams you recently joined'))
@section('content')
<div class="row">
@forelse ($teams as $team)
<div class="col-md-4">
    <div class="card">
        <img src="{{$team->image}}" alt="{{$team->name}}" class="card-img-top">
        <div class="card-header">
            <div class="card-title">
                <a href="{{route('dashboard.teams.show', ['team' => $team])}}">{{$team->name}}</a>
            </div>
        </div>
        <div class="card-body">
            {{$team->description}}
        </div>
    </div>
</div>
@empty
<div class="card">
    <div class="card-body">
        <p class="text-muted-text-center">
            @lang('You are not currenctly a member of any teams')
        </p>
    </div>
</div>
@endforelse
</div>
@endsection
