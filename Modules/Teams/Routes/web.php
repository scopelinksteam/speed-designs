<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;
use Modules\Teams\Entities\Team;
use Modules\Teams\Entities\TeamLabel;
use Modules\Teams\Http\Controllers\Dashboard\TeamsController;

Route::dashboard(function(){
    Route::get('/', [TeamsController::class, 'index'])->name('index');
    Route::get('/edit/{team}', [TeamsController::class, 'edit'])->name('edit');
    Route::get('/delete/{team}', [TeamsController::class, 'destroy'])->name('delete');
    Route::post('/', [TeamsController::class, 'store'])->name('store');
    Route::prefix('/labels')->name('labels.')->group(function(){ Route::labels(TeamLabel::class); });

    //Team Page
    Route::get('/show/{team}', [TeamsController::class, 'show'])->name('show');
    Route::get('/my', [TeamsController::class, 'my'])->name('my');
    Route::get('/leave/{team}', [TeamsController::class, 'leave'])->name('leave');
    Route::get('/remove/{team}/{user}', [TeamsController::class, 'remove'])->name('remove');
    Route::post('/invite', [TeamsController::class, 'invite'])->name('invite');
    Route::post('/promote', [TeamsController::class, 'promote'])->name('promote');

}, 'teams');
