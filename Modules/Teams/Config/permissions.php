<?php

return [
    'create teams' => __('Give the ability to create new teams'),
    'edit teams' => __('Can edit any team'),
    'delete teams' => __('Allows user to delete teams'),
    'edit own teams' => __('Ability to edit teams owned by himself only'),
    'assign teams owners' => __('When create or edit team user can assign team owner'),
    'add team members' => __('User can add new team members'),

];
