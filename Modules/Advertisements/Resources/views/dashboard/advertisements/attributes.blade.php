<div class="card">
    <div class="card-header">
        <div class="card-title">
            <i class="fa fa-chain"></i>
            @lang('Link Options')
        </div>
    </div>
    <div class="card-body">
        <div>
            <label for="attribute_link_text" class="form-label">@lang('Link Text')</label>
            <input type="text" name="attributes[link_text]" id="attribute_link_text" class="form-control" value="{{isset($post) ? $post->Attribute('link_text') : ''}}">
        </div>
        <div>
            <label for="attribute_link_url" class="form-label">@lang('Link URL')</label>
            <input type="url" name="attributes[link_url]" id="attribute_link_url" class="form-control" value="{{isset($post) ? $post->Attribute('link_url') : ''}}">
        </div>
    </div>
</div>
