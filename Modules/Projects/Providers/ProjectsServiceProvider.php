<?php

namespace Modules\Projects\Providers;

use Cascade\Dashboard\Entities\MenuItem;
use Cascade\Dashboard\Entities\MenuSection;
use Cascade\Dashboard\Services\DashboardBuilder;
use Cascade\Settings\Entities\Setting;
use Cascade\Settings\Services\Settings;
use Cascade\System\Entities\Category;
use Cascade\System\Entities\Status;
use Cascade\Users\Entities\Role;
use Cascade\Users\Entities\User;
use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;
use Livewire\Livewire;
use Modules\Projects\Entities\Project;
use Modules\Projects\Entities\ProjectStatus;
use Modules\Projects\Modals\AddProjectMemberModal;
use Modules\Projects\Pages\ProjectCreatePage;
use Modules\Projects\Pages\ProjectEditPage;
use Modules\Projects\Pages\ProjectViewPage;
use Modules\Projects\Tabs\Projects\ProjectDetailsTab;
use Modules\Projects\Tabs\Projects\ProjectOverviewTab;
use SebastianBergmann\CodeCoverage\Report\Html\Dashboard;

class ProjectsServiceProvider extends ServiceProvider
{
    /**
     * @var string $moduleName
     */
    protected $moduleName = 'Projects';

    /**
     * @var string $moduleNameLower
     */
    protected $moduleNameLower = 'projects';

    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerTranslations();
        $this->registerConfig();
        $this->registerViews();
        $this->loadMigrationsFrom(module_path($this->moduleName, 'Database/Migrations'));
        $this->registerMenu();
        $this->registerSettings();
        $this->registerModals();

        //Build Pages
        ProjectEditPage::AddTab(ProjectDetailsTab::class);
        ProjectCreatePage::AddTab(ProjectDetailsTab::class);
        ProjectViewPage::AddTab(ProjectOverviewTab::class);

    }

    public function registerModals()
    {
        Livewire::component('addProjectMember', AddProjectMemberModal::class);
    }
    protected function registerMenu()
    {
        DashboardBuilder::MainMenu()->AddSection(MenuSection::make('projects', __('Projects Management')));
        DashboardBuilder::MainMenu()->AddItem(
            MenuItem::make(__('Projects'), 'projects')->icon('cubes')->children([
                MenuItem::make(__('Projects List'))->icon('fire')->route('dashboard.projects.index'),
                MenuItem::make(__('Categories'))->icon('folder')->route('dashboard.projects.categories.index'),
                MenuItem::make(__('Types'))->icon('star')->route('dashboard.projects.types.index'),
                MenuItem::make(__('Statuses'))->icon('filter')->route('dashboard.projects.statuses.index'),
            ])
        );
    }

    protected function registerSettings()
    {
        Settings::module('projects', __('Projects'), function(){
            Settings::page(__('General'), 'general', function(){
                Settings::group(__('Setup'), function(){
                    Settings::select(__('Default Status'), 'setup.default_status', function(){
                        return ProjectStatus::all()->pluck('name','id')->toArray();
                    });
                    Settings::select(__('Active Status'), 'setup.active_status', function(){
                        return ProjectStatus::all()->pluck('name','id')->toArray();
                    });
                    Settings::multiple(__('Customers Roles'), 'setup.customers_roles', function(){
                        return Role::all()->pluck('name','id')->toArray();
                    });
                });
            });
        });
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        Category::register(Project::class, 'projects');
        Status::register(Project::class);
        $this->app->register(RouteServiceProvider::class);
    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->publishes([
            module_path($this->moduleName, 'Config/config.php') => config_path($this->moduleNameLower . '.php'),
        ], 'config');
        $this->mergeConfigFrom(
            module_path($this->moduleName, 'Config/config.php'), $this->moduleNameLower
        );
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $viewPath = resource_path('views/modules/' . $this->moduleNameLower);

        $sourcePath = module_path($this->moduleName, 'Resources/views');

        $this->publishes([
            $sourcePath => $viewPath
        ], ['views', $this->moduleNameLower . '-module-views']);

        $this->loadViewsFrom(array_merge($this->getPublishableViewPaths(), [$sourcePath]), $this->moduleNameLower);
    }

    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $langPath = resource_path('lang/modules/' . $this->moduleNameLower);

        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, $this->moduleNameLower);
        } else {
            $this->loadTranslationsFrom(module_path($this->moduleName, 'Resources/lang'), $this->moduleNameLower);
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }

    private function getPublishableViewPaths(): array
    {
        $paths = [];
        foreach (\Config::get('view.paths') as $path) {
            if (is_dir($path . '/modules/' . $this->moduleNameLower)) {
                $paths[] = $path . '/modules/' . $this->moduleNameLower;
            }
        }
        return $paths;
    }
}
