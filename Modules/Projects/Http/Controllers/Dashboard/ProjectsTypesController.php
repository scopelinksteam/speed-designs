<?php

namespace Modules\Projects\Http\Controllers\Dashboard;

use Cascade\System\Services\Feedback;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Projects\Entities\ProjectType;
use Modules\Projects\Tables\ProjectsTypesTable;

class ProjectsTypesController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        return view('projects::dashboard.types.index', [
            'table' => ProjectsTypesTable::class,
        ]);
    }


    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        $entityId = intval($request->input('entity_id', 0));
        $entity = $entityId > 0 ? ProjectType::find($entityId) : new ProjectType();

        if(!$request->filled('name'))
        {
            Feedback::error(__('Name is Required'));
            Feedback::flash();
            return back();
        }

        $entity->name = $request->input('name', '');
        $entity->color = $request->input('color', '#000000');
        $entity->save();

        Feedback::success(__('Project Type Saved Successfully'));
        Feedback::flash();

        return to_route('dashboard.projects.types.index');

    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit(ProjectType $type)
    {
        return $this->index()->with([
            'entity' => $type
        ]);
    }


    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy(ProjectType $type)
    {
        $type->delete();
        Feedback::success(__('Type Deleted Successfully'));
        Feedback::flash();
        return back();
    }
}
