<?php

namespace Modules\Projects\Http\Controllers\Dashboard;

use Cascade\System\Services\Feedback;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Projects\Entities\ProjectStatus;
use Modules\Projects\Tables\ProjectsStatusesTable;

class ProjectsStatusesController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        return view('projects::dashboard.statuses.index', [
            'table' => ProjectsStatusesTable::class,
        ]);
    }


    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        $entityId = intval($request->input('entity_id', 0));
        $entity = $entityId > 0 ? ProjectStatus::find($entityId) : new ProjectStatus();

        if(!$request->filled('name'))
        {
            Feedback::error(__('Name is Required'));
            Feedback::flash();
            return back();
        }

        $entity->name = $request->input('name', '');
        $entity->color = $request->input('color', '#000000');
        $entity->save();

        Feedback::success(__('Project Status Saved Successfully'));
        Feedback::flash();

        return to_route('dashboard.projects.statuses.index');

    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit(ProjectStatus $status)
    {
        return $this->index()->with([
            'entity' => $status
        ]);
    }


    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy(ProjectStatus $status)
    {

    }
}
