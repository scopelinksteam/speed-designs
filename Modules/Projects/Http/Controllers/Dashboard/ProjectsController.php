<?php

namespace Modules\Projects\Http\Controllers\Dashboard;

use Cascade\System\Services\Feedback;
use Cascade\System\Services\System;
use Cascade\Users\Entities\User;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Gate;
use Modules\Projects\Entities\Project;
use Modules\Projects\Entities\ProjectStatus;
use Modules\Projects\Entities\ProjectType;
use Modules\Projects\Pages\ProjectCreatePage;
use Modules\Projects\Pages\ProjectEditPage;
use Modules\Projects\Pages\ProjectViewPage;
use Modules\Projects\Tables\ProjectsTable;

class ProjectsController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        return view('projects::dashboard.projects.index', [
            'table' => ProjectsTable::class
        ]);
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        $customersRoles = array_map('intval', get_setting('projects::general.setup.customers_roles', [], false));
        return System::FindView('projects::dashboard.projects.upsert', [
            'tabs' => ProjectCreatePage::GetTabs(),
            'customers' => User::whereHas('roles', function($rolesQuery) use ($customersRoles){
                $rolesQuery->whereIn('id', $customersRoles);
            })->get()->map(function($userEntity){
                return $userEntity->impersonate();
            }),
            'statuses' => ProjectStatus::all(),
            'types' => ProjectType::all(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        $entityId = intval($request->input('entity_id', 0));
        Gate::authorize($entityId > 0 ? 'edit projects' : 'create projects');
        $entity = $entityId > 0 ? Project::find($entityId) : new Project();

        $entity->title = $request->input('title', '');
        $entity->description = $request->input('description', '');
        $entity->type_id = intval($request->input('type', 0));
        $entity->customer_id = intval($request->input('customer', 0));
        $entity->status_id = intval($request->input('status', 0));
        $entity->price = doubleval($request->input('price', 0));
        $entity->start_date = $request->date('start_date');
        $entity->deadline = $request->date('deadline');
        $entity->save();

        //Generate a Dynamic Title
        if(empty($entity->title))
        {
            $entity->title = __('Project') .' #'.$entity->id;
            $entity->save();
        }

        //Save Attributes

        foreach($request->input('attributes', []) as $key => $value)
        {
            $entity->storeAttribute($key, $value);
        }

        Feedback::success(__('Project Saved Successfully'));
        Feedback::flash();
        return $entityId > 0 ? to_route('dashboard.projects.edit', ['project' => $entity]) : to_route('dashboard.projects.index');
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show(Project $project)
    {
        return System::FindView('projects::dashboard.projects.view', [
            'tabs' => ProjectViewPage::GetTabs(),
            'project' => $project
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit(Project $project)
    {
        return $this->create()->with([
            'tabs' => ProjectEditPage::GetTabs(),
            'entity' => $project
        ]);
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy(Project $project)
    {
        Gate::authorize('delete projects');
        $project->delete();
        Feedback::alert(__('Project Deleted Successfully'));
        Feedback::flash();
        return to_route('dashboard.projects.index');
    }
}
