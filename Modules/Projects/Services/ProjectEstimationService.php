<?php

namespace Modules\Projects\Services;

use Cascade\System\Abstraction\Service;
use Illuminate\Contracts\Foundation\Application;
use Modules\Projects\Pages\ProjectOverviewPage;
use Modules\Projects\Pages\ProjectViewPage;
use Modules\Projects\Tabs\Projects\ProjectEstimationsTab;

class ProjectEstimationService extends Service
{
    public function name(): string
    {
        return __('Projects Estimation');
    }

    public function identifier(): string
    {
        return 'projects-estimation';
    }

    public function description(): string
    {
        return __('Add Estimation Capabilities to Projects');
    }

    public function register(Application $app)
    {
        ProjectViewPage::AddTab(ProjectEstimationsTab::class);
    }
}
