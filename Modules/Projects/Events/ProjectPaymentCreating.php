<?php

namespace Modules\Projects\Events;

use Illuminate\Queue\SerializesModels;
use Modules\Projects\Entities\ProjectPayment;

class ProjectPaymentCreating
{
    use SerializesModels;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(public ProjectPayment $payment)
    {
        //
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
