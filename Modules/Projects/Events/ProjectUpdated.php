<?php

namespace Modules\Projects\Events;

use Illuminate\Queue\SerializesModels;
use Modules\Projects\Entities\Project;

class ProjectUpdated
{
    use SerializesModels;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(public Project $project)
    {
        //
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
