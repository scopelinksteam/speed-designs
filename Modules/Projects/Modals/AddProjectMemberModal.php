<?php

namespace Modules\Projects\Modals;

use Cascade\Dashboard\Abstraction\DashboardModal;
use Cascade\System\Services\System;
use Cascade\Users\Entities\User;
use Modules\Projects\Entities\Project;

class AddProjectMemberModal extends DashboardModal
{

    public Project $project;
    public $users = [];

    public $member_id = 0;

    public function mount(int $projectId)
    {
        $this->project = Project::find($projectId);
        $this->users = User::all();
    }
    public function title(): string
    {
        return __('Add Project Member');
    }

    public function AddMember()
    {
        $user = User::find($this->member_id);
        if($user)
        {
            $user->join($this->project, 'members');
        }
    }

    public function render()
    {
        return System::FindView('projects::dashboard.projects.modals.add_member');
    }
}
