<?php

namespace Modules\Projects\Enums;

enum EditRequestStatus : int
{
    case PendingPayment = 0;
    case Active = 1;
    case Completed = 2;
    case InProgress = 3;

    public function translated()
    {
        $value = '';
        switch($this)
        {
            case EditRequestStatus::Active : $value = __('Active'); break;
            case EditRequestStatus::PendingPayment : $value = __('Pending Payment'); break;
            case EditRequestStatus::Completed : $value = __('Completed'); break;
            case EditRequestStatus::InProgress : $value = __('In Progress'); break;
        }
        return $value;
    }
}
