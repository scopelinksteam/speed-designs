@extends('admin::layouts.master')
@section('page-title', __('Projects Statuses'))
@section('page-description', __('Manage and Edit Avaialble Projects Statuses'))
@section('content')
<div class="row">
    <div class="col-md-4">
        <div class="card">
            <form action="{{ route('dashboard.projects.statuses.store')}}" method="POST">
                @csrf
                @isset($entity)
                <input type="hidden" name="entity_id" value="{{$entity->id}}">
                @endisset
                <div class="card-header">
                    <div class="card-title">@lang('Status Details')</div>
                </div>
                <div class="card-body">
                    <div>
                        <label for="name" class="form-label">@lang('Name')</label>
                        <input type="text" name="name" id="name" class="form-control" value="{{$entity->name ?? old('name')}}">
                    </div>
                    <div class="mt-3">
                        <label for="color" class="form-label">@lang('Color')</label>
                        <input type="color" name="color" id="color" class="form-control" value="{{$entity->color ?? old('color')}}">
                    </div>
                </div>
                <div class="card-footer">
                    <input type="submit" value="@lang('Save Project Status')" class="btn btn-primary">
                </div>
            </form>
        </div>
    </div>
    <div class="col-md-8">
        <div class="card">
            <div class="card-header">
                <div class="card-title">@lang('Available Statuses')</div>
            </div>
            <div class="card-body">
                @livewire('datatable', ['table' => $table])
            </div>
        </div>
    </div>
</div>
@endsection
