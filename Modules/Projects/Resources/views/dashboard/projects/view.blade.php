@extends('admin::layouts.master')
@section('page-title', $project->title)
@section('page-description', __('View Advanced Details and Overview Your Project'))
@section('content')
<x-dashboard-tabs :tabs="$tabs" :data="$__data" />
@endsection
