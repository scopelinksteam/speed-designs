<div class="modal-content">
    <div class="modal-header">
        <div class="modal-title">@lang('Add Project Member')</div>
        <button type="button" class="btn-close" aria-label="Close" wire:click="$emit('closeModal')"></button>
    </div>
    <div class="modal-body" wire:ignore>
        <select name="" id="" class="selectr" wire:model="member_id">
            @foreach ($users as $user)
            <option value="{{$user->id}}">{{$user->display_name}}</option>
            @endforeach
        </select>
    </div>
    <div class="modal-footer">
        <button class="btn btn-primary" wire:click="AddMember">@lang('Add Member')</button>
        <button type="button" class="btn btn-light" aria-label="Close" wire:click="$emit('closeModal')">@lang('Cancel')</button>
    </div>
</div>
