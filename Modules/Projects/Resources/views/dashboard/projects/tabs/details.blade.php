@stack('tab::start')
<div class="row">
    <div class="col-md-8">
        @stack('card::information.before')
        <div class="card" id="information-card">
            <div class="card-header">
                <div class="card-title">@lang('Project Information')</div>
            </div>
            <div class="card-body">
                @stack('card::information.start')
                <div class="row">
                    <div class="col-md-12">
                        <div>
                            <label for="title" class="form-label">@lang('Title')</label>
                            <input type="text" name="title" id="title" class="form-control" value="{{$entity->title ?? old('title')}}"/>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div>
                            <label for="description" class="form-label">@lang('Description')</label>
                            <textarea name="description" id="description" class="editor">{!! $entity->description ?? old('description') !!}</textarea>
                        </div>
                    </div>
                </div>
                @stack('card::information.end')
            </div>
        </div>
        @stack('card::information.after')
    </div>
    <div class="col-md-4">
        @stack('card::setup.before')
        <div class="card" id="setup-card">
            <div class="card-header">
                <div class="card-title">
                    @lang('Project Setup')
                </div>
            </div>
            <div class="card-body">
                @stack('card::setup.start')
                <div class="row">
                    <div class="col-md-12">
                        <label for="type" class="form-label">@lang('Project Type')</label>
                        <select name="type" id="type" class="selectr">
                            <option value="0">@lang('Select')</option>
                            @foreach ($types as $type)
                            <option value="{{$type->id}}" @selected(isset($entity) && $entity->type_id == $type->id)>{{$type->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-12">
                        <label for="customer" class="form-label">@lang('Customer')</label>
                        <select name="customer" id="customer" class="selectr">
                            <option value="0">@lang('Select')</option>
                            @foreach ($customers as $customer)
                            <option value="{{$customer->id}}" @selected(isset($entity) && $entity->customer_id == $customer->id)>{{ $customer->display_name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-12">
                        <label for="status" class="form-label">@lang('Status')</label>
                        <select name="status" id="status" class="selectr">
                            <option value="0">@lang('Select')</option>
                            @foreach ($statuses as $status)
                            <option value="{{$status->id}}" @selected(isset($entity) && $entity->status_id == $status->id)>{{$status->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-12">
                        <label for="status" class="form-label">@lang('Price')</label>
                        <input type="number" name="price" id="price" class="form-control" value="{{ $entity->price ?? old('price')}}">
                    </div>
                </div>
                @stack('card::setup.end')
            </div>
        </div>
        @stack('card::setup.after')
        @stack('card::time.before')
        <div class="card" id="time-card">
            <div class="card-header">
                <div class="card-title">@lang('Time Frame')</div>
            </div>
            <div class="card-body">
                @stack('card::time.start')
                <div class="row">
                    <div class="col-md-12">
                        <label for="start_date" class="form-label">@lang('Start Date')</label>
                        <input type="datetime-local" name="start_date" id="start_date" class="form-control" value="{{$entity->start_date ?? old('start_date')}}">
                    </div>
                    <div class="col-md-12">
                        <label for="deadline" class="form-label">@lang('Deadline')</label>
                        <input type="datetime-local" name="deadline" id="deadline" class="form-control" {{$entity->deadline ?? old('deadline')}}>
                    </div>
                </div>
                @stack('card::time.end')
            </div>
        </div>
        @stack('card::time.after')
    </div>
</div>
@stack('tab::end')
