<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <div class="card-title">@lang('Submitted Estimations')</div>
            </div>
            <div class="card-body">
                @livewire('datatable', ['table' => $table, 'params' => ['project' => $project]], key($project->id))
            </div>
        </div>
    </div>
</div>
