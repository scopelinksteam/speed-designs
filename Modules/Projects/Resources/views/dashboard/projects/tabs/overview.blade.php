<div class="container">
    <div class="row">
        @stack('primary::before')
        <div class="col-md-6">
            <div class="row">
                @stack('primary::start')
                <div class="col-md-12">
                    @stack('card::basic.before')
                    <div class="card">
                        <div class="card-header">
                            <div class="card-title">@lang('Basic Details')</div>
                        </div>
                        <div class="card-body">
                            @stack('card::basic.start')
                            <table class="table table-responsive table-striped">
                                @stack('table::basic.start')
                                <tr>
                                    <th>@lang('Start Date')</th>
                                    <td>{{$project->start_date ? format_date($project->start_date) : __('NA')}}</td>
                                    <th>@lang('Status')</th>
                                    <td>
                                        @if($project->status)
                                        <span class="badge text-dark" style="background-color:{{$project->status->color}}">{{$project->status->name}}</span>
                                        @endif
                                    </td>
                                </tr>
                                <tr>
                                    <th>@lang('Deadline')</th>
                                    <td>{{$project->deadline ? format_date($project->deadline) : __('NA')}}</td>
                                    <th>@lang('Type')</th>
                                    <td>
                                        @if($project->type)
                                        <span class="badge text-dark" style="background-color:{{$project->type->color}}">{{$project->type->name}}</span>
                                        @endif
                                    </td>
                                </tr>
                                <tr>
                                    <th>@lang('Customer')</th>
                                    <td>{{$project->customer ? $project->customer->display_name : __('NA')}}</td>
                                    <th>@lang('Price')</th>
                                    <td>{{$project->price}}</td>
                                </tr>
                                @stack('table::basic.end')
                            </table>
                            @stack('card::basic.end')
                        </div>
                    </div>
                    @stack('card::basic.after')
                </div>
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="card-title">@lang('Description')</div>
                        </div>
                        <div class="card-body">
                            {!! $project->description !!}
                        </div>
                    </div>
                </div>
                @stack('primary::end')
            </div>
        </div>
        @stack('primary::after')
        <div class="col-md-6">
            @stack('card::members.before')
            <div class="card">
                <div class="card-header">
                    <div class="card-title">@lang('Members')</div>
                </div>
                <div class="card-body">
                    <button type="button" onclick="Livewire.emit('openModal', 'addProjectMember', {{json_encode(['projectId' => $project->id])}} )" class="btn btn-success">@lang('Add Member')</button>
                    <table class="table table-striped table-responsive">
                        <tr>
                            <th>@lang('Member')</th>
                            <td>@lang('Joined At')</td>
                            <td>@lang('Actions')</td>
                        </tr>
                        @foreach ($project->members as $entry)
                        <tr>
                            <td>{{$entry->member->display_name}}</td>
                            <td>{{format_date($entry->created_at)}}</td>
                            <td>
                                <a href="{{route('dashboard.system.members.remove', ['member' => $entry])}}" class="btn btn-light">@lang('Remove')</a>
                            </td>
                        </tr>
                        @endforeach
                    </table>
                </div>
            </div>
            @stack('card::members.after')
        </div>
    </div>
</div>
