@extends('admin::layouts.master')
@section('page-title', __('Projects Management'))
@section('page-description', __('Manage and Overview Projects Created on your system'))
@section('page-actions')
<a href="{{route('dashboard.projects.create')}}" class="btn btn-success">@lang('Create Project')</a>
@endsection
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <div class="card-title">@lang('Projects')</div>
            </div>
            <div class="card-body">
                @livewire('datatable', ['table' => $table])
            </div>
        </div>
    </div>
</div>
@endsection
