@extends('admin::layouts.master')
@section('page-title', __('Project Management'))
@section('content')
<form action="{{route('dashboard.projects.store')}}" method="POST">
    @csrf
    @isset($entity)
    <input type="hidden" name="entity_id" value="{{$entity->id}}">
    @endisset
    <x-dashboard-tabs :tabs="$tabs" :data="$__data" />
    <div class="p-3 rounded-3 bg-white">
        <input type="submit" value="@lang('Save Project')" class="btn btn-primary">
    </div>
</form>
@endsection
