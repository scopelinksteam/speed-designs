<?php

namespace Modules\Projects\Entities;

use Cascade\System\Traits\HasAttachments;
use Cascade\System\Traits\HasAttributes;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Modules\Projects\Enums\EditRequestStatus;

class ProjectEditRequest extends Model
{
    use HasAttributes, HasAttachments;

    protected $table = 'projects_edits_requests';

    protected $casts = [
        'cost' => 'double',
        'price' => 'double',
        'status_id' => EditRequestStatus::class
    ];

    public function project()
    {
        return $this->belongsTo(Project::class, 'project_id');
    }

    public function getPaymentAttribute()
    {
        $editID = $this->id;
        return ProjectPayment::where('project_id', $this->project_id)->whereHas('attributes', function($attributesQuery) use($editID){
            return $attributesQuery->where('key', 'EDIT_ID')->where('value', $editID);
        })->first();
    }

}
