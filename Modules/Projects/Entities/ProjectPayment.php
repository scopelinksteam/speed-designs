<?php

namespace Modules\Projects\Entities;

use Cascade\System\Traits\HasAttributes;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Modules\Projects\Events\ProjectPaymentCreated;
use Modules\Projects\Events\ProjectPaymentCreating;
use Modules\Projects\Events\ProjectPaymentDeleted;
use Modules\Projects\Events\ProjectPaymentDeleting;
use Modules\Projects\Events\ProjectPaymentRestored;
use Modules\Projects\Events\ProjectPaymentRestoring;
use Modules\Projects\Events\ProjectPaymentRetrieved;
use Modules\Projects\Events\ProjectPaymentSaved;
use Modules\Projects\Events\ProjectPaymentSaving;
use Modules\Projects\Events\ProjectPaymentUpdated;
use Modules\Projects\Events\ProjectPaymentUpdating;

class ProjectPayment extends Model
{

    use HasAttributes;

    protected $table = 'projects_payments';

    protected $casts = [
        'due_date' => 'datetime',
        'is_paid' => 'bool'
    ];

    protected $dispatchesEvents =[
        'created'   => ProjectPaymentCreated::class,
        'creating'  => ProjectPaymentCreating::class,
        'updating'  => ProjectPaymentUpdating::class,
        'updated'   => ProjectPaymentUpdated::class,
        'deleting'  => ProjectPaymentDeleting::class,
        'deleted'   => ProjectPaymentDeleted::class,
        'saving'    => ProjectPaymentSaving::class,
        'saved'     => ProjectPaymentSaved::class,
        'retrieved' => ProjectPaymentRetrieved::class,
        'restoring' => ProjectPaymentRestoring::class,
        'restored'  => ProjectPaymentRestored::class,
    ];

    public function project()
    {
        return $this->belongsTo(Project::class, 'project_id');
    }


}
