<?php

namespace Modules\Projects\Entities;

use Cascade\System\Traits\HasActions;
use Cascade\System\Traits\HasAttributes;
use Cascade\System\Traits\HasFilters;
use Cascade\Users\Entities\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProjectEstimation extends Model
{

    use HasAttributes, HasActions, HasFilters, SoftDeletes;

    protected $table = 'projects_estimations';

    protected $casts = [
        'price' => 'double',
        'stages' => 'int',
        'duration' => 'double'
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function project()
    {
        return $this->belongsTo(Project::class, 'project_id');
    }

    public function approve()
    {
        if(!$this->DispatchAction('approving', $this)) { return; }
        $project = $this->project;
        if($project == null) { return; }
        $project->price = $this->price ?? $project->price;
        $project->save();
        //TODO: Add Members
        $this->user->join($project, 'assigned');
        $project->storeAttribute('stages', $this->stages);
        $project->storeAttribute('duration', $this->duration);
        $project->storeAttribute('stages', $this->stages);
        $project->storeAttribute('stages', $this->stages);
        $this->DispatchAction('approved', $this);
    }
}
