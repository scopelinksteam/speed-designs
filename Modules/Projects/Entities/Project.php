<?php

namespace Modules\Projects\Entities;

use Cascade\Dashboard\Traits\HasTabs;
use Cascade\System\Entities\Status;
use Cascade\System\Traits\HasActions;
use Cascade\System\Traits\HasAttachments;
use Cascade\System\Traits\HasAttributes;
use Cascade\System\Traits\HasCategories;
use Cascade\System\Traits\HasComments;
use Cascade\System\Traits\HasFilters;
use Cascade\System\Traits\Joinable;
use Cascade\Users\Entities\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Customers\Entities\Customer;
use Modules\Projects\Events\ProjectCreated;
use Modules\Projects\Events\ProjectCreating;
use Modules\Projects\Events\ProjectDeleted;
use Modules\Projects\Events\ProjectDeleting;
use Modules\Projects\Events\ProjectRestored;
use Modules\Projects\Events\ProjectRestoring;
use Modules\Projects\Events\ProjectRetrieved;
use Modules\Projects\Events\ProjectSaved;
use Modules\Projects\Events\ProjectSaving;
use Modules\Projects\Events\ProjectUpdated;
use Modules\Projects\Events\ProjectUpdating;

class Project extends Model
{
    use HasTabs, HasAttributes, SoftDeletes, HasCategories, HasActions, HasFilters, HasAttachments, HasComments, Joinable;

    protected $table = 'projects';

    protected $casts = [
        'start_date'    => 'datetime',
        'deadline'      => 'datetime',
        'price'       => 'double',
    ];

    protected $dispatchesEvents =[
        'created'   => ProjectCreated::class,
        'creating'  => ProjectCreating::class,
        'updating'  => ProjectUpdating::class,
        'updated'   => ProjectUpdated::class,
        'deleting'  => ProjectDeleting::class,
        'deleted'   => ProjectDeleted::class,
        'saving'    => ProjectSaving::class,
        'saved'     => ProjectSaved::class,
        'retrieved' => ProjectRetrieved::class,
        'restoring' => ProjectRestoring::class,
        'restored'  => ProjectRestored::class,
    ];

    protected static function boot()
    {
        parent::boot();
        static::created(function(Project $entity){
            if(empty($entity->title))
            { $entity->title = __('Project') .'# '.$entity->id; $entity->save(); }
        });
    }

    public function customer()
    {
        return $this->belongsTo(User::class, 'customer_id');
    }

    public function status()
    {
        return $this->belongsTo(Status::class, 'status_id');
    }

    public function type()
    {
        return $this->belongsTo(ProjectType::class, 'type_id');
    }

    public function estimations()
    {
        return $this->hasMany(ProjectEstimation::class, 'project_id');
    }

    public function payments()
    {
        return $this->hasMany(ProjectPayment::class, 'project_id');
    }

    public function edits()
    {
        return $this->hasMany(ProjectEditRequest::class, 'project_id');
    }

    public function owner()
    {
        return $this->morphTo('owner');
    }
}
