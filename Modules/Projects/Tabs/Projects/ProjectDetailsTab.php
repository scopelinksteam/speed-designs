<?php

namespace Modules\Projects\Tabs\Projects;

use Cascade\Dashboard\Abstraction\DashboardTab;
use Cascade\System\Services\System;
use Illuminate\View\View;

class ProjectDetailsTab extends DashboardTab
{
    public function name(): string
    {
        return __('Project Details');
    }

    public function slug(): string
    {
        return 'details';
    }

    public function view(): View|string
    {
        return System::FindView('projects::dashboard.projects.tabs.details', $this->data);
    }
}
