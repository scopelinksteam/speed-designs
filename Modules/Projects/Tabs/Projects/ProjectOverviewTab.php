<?php

namespace Modules\Projects\Tabs\Projects;

use Cascade\Dashboard\Abstraction\DashboardTab;
use Cascade\System\Services\System;
use Illuminate\View\View;

class ProjectOverviewTab extends DashboardTab
{
    public function name(): string
    {
        return __('Overview');
    }

    public function slug(): string
    {
        return 'overview';
    }

    public function view(): View|string
    {
        return System::FindView('projects::dashboard.projects.tabs.overview');
    }
}
