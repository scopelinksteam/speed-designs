<?php

namespace Modules\Projects\Tabs\Projects;

use Cascade\Dashboard\Abstraction\DashboardTab;
use Cascade\System\Services\System;
use Illuminate\View\View;
use Modules\Projects\Tables\ProjectsEstimationsTable;

class ProjectEstimationsTab extends DashboardTab
{
    public function name(): string
    {
        return __('Estimations');
    }

    public function slug(): string
    {
        return 'estimations';
    }

    public function view(): View|string
    {
        return System::FindView('projects::dashboard.projects.tabs.estimations', [
            'table' => ProjectsEstimationsTable::class
        ]);
    }
}
