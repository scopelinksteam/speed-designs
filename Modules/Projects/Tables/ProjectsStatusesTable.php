<?php

namespace Modules\Projects\Tables;

use Cascade\Dashboard\Services\Datatables\Action;
use Cascade\Dashboard\Services\Datatables\Column;
use Cascade\Dashboard\Services\Datatables\DataTable;
use Modules\Projects\Entities\ProjectStatus;

class ProjectsStatusesTable extends DataTable
{
    public function __construct()
    {
        $this->editColumn('name', function(ProjectStatus $type){
            return '<div class="d-flex align-items-center">'.
                    $this->color($type->color).
                    '<span class="ms-2">'.$type->name.'</span>'.
                    '</div>';
        });
    }

    public function query()
    {
        return ProjectStatus::query();
    }

    public function columns()
    {
        return [
            Column::make('name')->text(__('Name')),
        ];
    }

    public function actions()
    {
        return [
            Action::make('edit', __('Edit'))->route('dashboard.projects.statuses.edit', ['status' => 'id']),
            Action::make('delete', __('Delete'))->route('dashboard.projects.statuses.delete', ['status' => 'id']),
        ];
    }
}
