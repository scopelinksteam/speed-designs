<?php

namespace Modules\Projects\Tables;

use Carbon\CarbonInterval;
use Cascade\Dashboard\Services\Datatables\Action;
use Cascade\Dashboard\Services\Datatables\Column;
use Cascade\Dashboard\Services\Datatables\DataTable;
use Modules\Projects\Entities\Project;
use Modules\Projects\Entities\ProjectEstimation;

class ProjectsEstimationsTable extends DataTable
{

    public function __construct(protected Project $project)
    {
        $this->editColumn('user', function(ProjectEstimation $estimation){
            return $estimation->user->impersonate()->display_name;
        });
        $this->editColumn('duration', function(ProjectEstimation $estimation){
            return CarbonInterval::create(0,0,0,0,doubleval($estimation->duration),0)->forHumans();
        });
    }

    public function query()
    {
        return ProjectEstimation::where('project_id', $this->project->id);
    }

    public function columns()
    {
        return [
            Column::make('user')->text(__('User / Estimator')),
            Column::make('price')->text(__('Price')),
            Column::make('stages')->text(__('Milestones')),
            Column::make('duration')->text(__('Duration'))
        ];
    }

    public function actions()
    {
        return [
            Action::make('approve', __('Approve'))->callback(function(ProjectEstimation $estimation){
                $estimation->approve();
                redirect(route('dashboard.projects.show', ['project' => $estimation->project]));
            }),
        ];
    }
}
