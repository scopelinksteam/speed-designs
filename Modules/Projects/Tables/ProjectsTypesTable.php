<?php

namespace Modules\Projects\Tables;

use Cascade\Dashboard\Services\Datatables\Action;
use Cascade\Dashboard\Services\Datatables\Column;
use Cascade\Dashboard\Services\Datatables\DataTable;
use Modules\Projects\Entities\ProjectType;

class ProjectsTypesTable extends DataTable
{
    public function __construct()
    {
        $this->editColumn('name', function(ProjectType $type){
            return '<div class="d-flex align-items-center">'.
                    $this->color($type->color).
                    '<span class="ms-2">'.$type->name.'</span>'.
                    '</div>';
        });
    }

    public function query()
    {
        return ProjectType::query();
    }

    public function columns()
    {
        return [
            Column::make('name')->text(__('Name')),
        ];
    }

    public function actions()
    {
        return [
            Action::make('edit', __('Edit'))->route('dashboard.projects.types.edit', ['type' => 'id']),
            Action::make('delete', __('Delete'))->route('dashboard.projects.types.delete', ['type' => 'id']),
        ];
    }
}
