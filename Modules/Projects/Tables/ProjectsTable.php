<?php

namespace Modules\Projects\Tables;

use Cascade\Dashboard\Services\Datatables\Action;
use Cascade\Dashboard\Services\Datatables\Column;
use Cascade\Dashboard\Services\Datatables\DataTable;
use Cascade\Dashboard\Services\Datatables\Filter;
use Cascade\Users\Entities\User;
use Modules\Projects\Entities\Project;

class ProjectsTable extends DataTable
{

    public function __construct()
    {
        $this->editColumn('title', function(Project $project){
            $title =  $this->link($project->title, route('dashboard.projects.show', ['project' => $project])).'<br />';
            if($project->type)
            {
                $title .= '<span class="badge text-dark" style="background-color:'.$project->type->color.';">'.$project->type->name.'</span>';
            }
            return $title;
        });

        $this->editColumn('status', function(Project $project){
            if($project->status)
            {
                return '<span class="badge text-dark" style="background-color:'.$project->status->color.';">'.$project->status->name.'</span>';
            }
        });

        $this->editColumn('customer', function(Project $project){
            if($project->customer)
            {
                return $project->customer->impersonate()->display_name;
            }
        });
    }

    public function query($search = null, $filters = [])
    {
        $query = Project::query();
        $query->when(isset($filters['customer_id']) && $filters['customer_id'] > 0, function($query) use($filters){
            return $query->where('customer_id', intval($filters['customer_id']));
        });
        $query->when(isset($filters['member_id']) && $filters['member_id'] > 0, function($query) use($filters){
            return $query->whereHas('members', function($membersQuery) use($filters){
                return $membersQuery->where('member_id', intval($filters['member_id']));
            });
        });
        return $query;
    }

    public function filters()
    {
        $filters = [];
        $filters[] = Filter::make(Filter::FILTER_TYPE_SELECT, 'customer_id')
                        ->title(__('Customer'))
                        ->values(User::all()->pluck('name', 'id')->toArray());
        $filters[] = Filter::make(Filter::FILTER_TYPE_SELECT, 'customer_id')
                        ->title(__('Member'))
                        ->values(User::all()->pluck('name', 'id')->toArray());
        return $filters;
    }

    public function columns()
    {
        return [
            Column::make('title')->text(__('Name')),
            Column::make('customer')->text(__('Customer')),
            Column::make('price')->text(__('Price')),
            Column::make('start_date')->text(__('Start Date')),
            Column::make('deadline')->text(__('Deadline')),
            Column::make('status')->text(__('Status')),
        ];
    }

    public function actions()
    {
        return [
            Action::make('show', __('Show Project'))->route('dashboard.projects.show', ['project' => 'id']),
            Action::make('edit', __('Edit Project'))->route('dashboard.projects.edit', ['project' => 'id']),
            Action::make('delete', __('Delete Project'))->route('dashboard.projects.delete', ['project' => 'id']),
        ];
    }
}
