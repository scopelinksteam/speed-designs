<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;
use Modules\Projects\Entities\Project;
use Modules\Projects\Http\Controllers\Dashboard\ProjectsController;
use Modules\Projects\Http\Controllers\Dashboard\ProjectsStatusesController;
use Modules\Projects\Http\Controllers\Dashboard\ProjectsTypesController;

Route::dashboard(function(){

    Route::get('/', [ProjectsController::class, 'index'])->name('index');
    Route::get('/create', [ProjectsController::class, 'create'])->name('create');
    Route::get('/edit/{project}', [ProjectsController::class, 'edit'])->name('edit');
    Route::post('/', [ProjectsController::class, 'store'])->name('store');
    Route::get('/delete/{project}', [ProjectsController::class, 'destroy'])->name('delete');
    Route::get('/show/{project}', [ProjectsController::class, 'show'])->name('show');

    Route::prefix('types')->name('types.')->group(function(){
        Route::get('/', [ProjectsTypesController::class, 'index'])->name('index');
        Route::get('/edit/{type}', [ProjectsTypesController::class, 'edit'])->name('edit');
        Route::post('/', [ProjectsTypesController::class, 'store'])->name('store');
        Route::get('/delete/{type}', [ProjectsTypesController::class, 'destroy'])->name('delete');
    });

    Route::prefix('statuses')->name('statuses.')->group(function(){
        Route::statuses(Project::class);
    });

    Route::prefix('categories')->name('categories.')->group(function(){
        Route::categories(Project::class);
    });

}, 'projects');
