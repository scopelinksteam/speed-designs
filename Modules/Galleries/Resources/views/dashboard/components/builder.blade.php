<div class="gallery-builder">
    <div class="row" x-data="GalleyCustomizer()">
        <div class="col-12">
            <div class="row justify-content-between">
                <div class="col-auto">
                    <div class="btn-group" role="group">
                        @csrf
                        @isset($gallery)
                        <input type="hidden" name="gallery_id" value="{{ $gallery->id}}" />
                        @endisset
                        <button class="btn btn-secondary" type="button" x-on:click.prevent="ClearAll">@lang('Clear All')</button>
                    </div>
                </div>
                <div class="col-auto mb-4">
                    <div class="btn-group float-right" role="group">
                        <button x-on:click.prevent="AddItem('photo')" class="btn btn-warning" type="button">
                            <i class="fa fa-image me-2"></i>@lang('Add Photo')
                            <span class="badge bg-light text-dark ms-2" x-text="PhotosCount"></span>
                        </button>
                        <button x-on:click.prevent="AddItem('youtube')" class="btn btn-danger" type="button">
                            <i class="fab fa-youtube me-2"></i>@lang('Add Youtube')
                            <span class="badge bg-light text-dark ms-2" x-text="YoutubeCount"></span>
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <template x-for="item in items" :key="item.id">
            <div class="col-{{12/$gridColumns}}">
                <input type="hidden" x-bind:name="'gallery_items['+item.id+'][entity_id]'" x-bind:value="item.entity_id" />
                <input type="hidden" x-bind:name="'gallery_items['+item.id+'][type]'" x-bind:value="item.type">
                <div class="card">
                    <div class="card-header">
                        <div class="row justify-content-between align-items-center">
                            <div class="col-auto">
                                <template x-if="item.type == 'photo'">
                                    <div class="card-title">
                                        <i class="fa fa-image mr-2"></i>
                                        <span x-text="'#'+item.id"></span> @lang('Photo Item')
                                    </div>
                                </template>
                                <template x-if="item.type == 'youtube'">
                                    <div class="card-title">
                                        <i class="fab fa-youtube mr-2"></i>
                                        <span x-text="'#'+item.id"></span> @lang('Youtube Item')
                                    </div>
                                </template>
                            </div>
                            <div class="col-auto">
                                <div class="d-flex-inline">
                                    <button class="btn btn-light" type="button" x-bind:data-bs-target="'#slide-content-'+item.id" data-bs-toggle="collapse"><i class="fas fa-minus"></i></button>
                                    <button class="btn btn-light" type="button" x-on:click.prevent="RemoveItem(item.id)"><i class="fas fa-backspace"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-body collapse" x-bind:id="'slide-content-'+item.id">
                        <div class="row">
                            <div class="col-7">
                                <ul class="nav nav-tabs">
                                @foreach(get_active_languages() as $language)
                                    <li class="nav-item">
                                        <a x-bind:data-bs-target="'#item-'+item.id+'-content-tab-{{ $language->locale }}'" class="nav-link @if($loop->iteration == 1) active @endif" data-bs-toggle="tab">{{ $language->name }}</a>
                                    </li>
                                @endforeach
                                </ul>
                                <div class="tab-content mt-4">
                                    @foreach (get_active_languages() as $language)
                                    <div class="tab-pane fade @if($loop->iteration == 1) active show @endif" x-bind:id="'item-'+item.id+'-content-tab-{{ $language->locale }}'">
                                        <div class="form-group">
                                            <label x-bind:for="'title-'+item.id+'-{{ $language->locale }}'">@lang('Title')</label>
                                            <input type="text" x-bind:name="'gallery_items['+item.id+'][title][{{ $language->locale }}]'" x-bind:id="'title-'+item.id+'-{{ $language->locale }}'" x-bind:value="item.title_{{ $language->locale }}" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label x-bind:for="'description-'+item.id+'-{{ $language->locale }}'">@lang('Description')</label>
                                            <textarea x-bind:name="'gallery_items['+item.id+'][description][{{ $language->locale }}]'" rows="10" x-bind:id="'description-'+item.id+'-{{ $language->locale }}'" x-html="item.description_{{ $language->locale }}" class="form-control editor"></textarea>
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                            <div class="col-5">
                                <h5>@lang('Media Source')</h5>
                                <hr class="mb-4">
                                <template x-if="item.type == 'photo'">
                                    <div>
                                        <div class="callout callout-info">
                                            <h5>@lang('How it works')</h5>
                                            <p>@lang('Leaving Photo Empty will indicate that item it will not be saved')</p>
                                        </div>
                                        <input type="file" x-bind:name="'gallery_items['+item.id+'][photo]'" x-bind:data-default-file="item.media_source" class="dropify-image" id="">

                                    </div>
                                </template>
                                <template x-if="item.type == 'youtube'">
                                    <div class="form-group">

                                        <label x-bind:for="'youtube-link-'+item.id" >@lang('Youtube Link')</label>
                                        <input type="url" x-bind:name="'gallery_items['+item.id+'][youtube_link]'" x-bind:id="'youtube-link-'+item.id" x-bind:value="item.media_source" class="form-control" />
                                    </div>
                                </template>
                                {{--
                                <ul class="nav nav-tabs">
                                @foreach (get_active_languages() as $language)
                                    <li class="nav-item">
                                        <a href="#" class="nav-link">{{ $language->name }}</a>
                                    </li>
                                @endforeach
                                </ul>
                                 --}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </template>
    </div>
</div>
@push('scripts')
<script type="text/javascript">
const GalleyCustomizer = () => {

    //Variables
    let itemsCounter = 1;
    let existItems = [];
    class GalleryItem {
        constructor(type){
            //Set ID
            this.id = itemsCounter;
            itemsCounter++;
            this.type = type;
            @foreach(get_active_languages() as $language)
            this.title_{{ $language->locale }} = '';
            this.description_{{ $language->locale }} = '';
            this.media_source = '';
            this.entity_id = 0;
            @endforeach
        }
    }

    @if(isset($gallery) && $gallery)
    @foreach($gallery->items as $item)
    let item{{ $item->id }} = new GalleryItem('{{ $item->type->value }}');
    //Set Values ....
    item{{ $item->id }}.entity_id = {{ $item->id }};
    @foreach (get_active_languages() as $language)
    item{{ $item->id }}.title_{{ $language->locale }} = '{{ $item->translate('title', $language->locale) }}';
    item{{ $item->id }}.description_{{ $language->locale }} = '{{ $item->translate('description', $language->locale) }}';
    @endforeach
    item{{ $item->id }}.media_source = '{{ $item->type->value == 'photo' ? uploads_url(''.$item->media_source) : $item->media_source }}';
    existItems.push(item{{ $item->id }});
    @endforeach
    @endif

    return {
        items: existItems,
        currentId: 0,
        get PhotosCount() { return this.items.filter(item => item.type == 'photo').length; },
        get YoutubeCount() { return this.items.filter(item => item.type == 'youtube').length; },
        AddItem: function (type) {
            this.items.push(new GalleryItem(type));
            setTimeout(() => {
                $('.dropify-image').dropify();
            }, 500);
        },
        RemoveItem: function(itemId)
        {
            this.items = this.items.filter(item => item.id != itemId);
        },
        ClearAll: function () { this.items = []; itemsCounter = 1; }
    }
}

</script>
@endpush
