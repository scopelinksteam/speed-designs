@extends('admin::layouts.master')
@section('page-title', __('Galleries'))
@section('page-description', __('Manage and Customize Galleries'))
@section('content')
<div class="row">
    <div class="col-md-4">
        <div class="card">
            <form action="{{ route('dashboard.galleries.store') }}" method="POST">
                <div class="card-header">
                    <div class="card-title">
                        <i class="fa fa-info mr-2"></i>
                        @lang('Gallery Details')
                    </div>
                </div>
                <div class="card-body">
                    @foreach (get_active_languages() as $language)
                    <div class="form-group">
                        <label for="name-{{ $language->locale }}">@lang('Name') ( {{ $language->name }} )</label>
                        <input type="text" name="name[{{ $language->locale }}]" id="name-{{ $language->locale }}" value="@isset($gallery) {{ $gallery->translate('name', $language->locale)  }} @endisset" class="form-control">
                    </div>
                    @endforeach
                    <div class="form-group">
                        <label for="category_id">@lang('Category')</label>
                        <select name="category_id" id="category_id" class="select2 form-control">
                            @foreach ($categories as $category)
                            <option value="{{ $category->id }}" @if(isset($gallery) && $gallery->category_id == $category->id) selected @endif>{{ $category->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="card-footer">
                    @csrf
                    @isset($gallery)
                    <input type="hidden" name="entity_id" value="{{ $gallery->id }}">
                    @endisset
                    <input type="submit" value="@lang('Save Gallery')" class="btn btn-primary">
                </div>
            </form>
        </div>
    </div>
    <div class="col-md-8">
        <div class="card">
            <div class="card-header">
                <div class="card-title">
                    <i class="fa fa-images mr-2"></i>
                    @lang('Galleries List')
                </div>
            </div>
            <div class="card-body">
                @livewire('datatable', ['table' => $table])
            </div>
        </div>
    </div>
</div>
@endsection
