<div class="container-fluid">
    <div class="row">
        @foreach ($gallery->items as $item)
        <div class="col-md-{{floor(12/max($per_row, 1))}} p-3">
            <div class="card">
                <a href="{{ uploads_url($item->media_source) }}" target="_blank">
                    <img src="{{ uploads_url($item->media_source) }}" class="card-img-top" alt="{{$item->title}}" srcset="">
                </a>
                @if(!empty($item->description))
                <div class="card-body">
                    {!! $item->description !!}
                </div>
                @endif
                @if(!empty($item->title))
                <div class="card-footer">
                    <center>{{$item->title}}</center>
                </div>
                @endif
            </div>
        </div>
        @endforeach
    </div>
</div>
