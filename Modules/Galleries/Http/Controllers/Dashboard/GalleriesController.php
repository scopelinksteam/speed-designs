<?php

namespace Modules\Galleries\Http\Controllers\Dashboard;

use Cascade\System\Entities\Category;
use Cascade\System\Services\Feedback;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Gate;
use Modules\Galleries\Entities\Gallery;
use Modules\Galleries\Entities\GalleryItem;
use Modules\Galleries\Enums\GalleryItemType;
use Modules\Galleries\Enums\GalleryStatus;
use Modules\Galleries\Tables\GalleriesTable;

class GalleriesController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index(GalleriesTable $table)
    {
        return view('galleries::dashboard.index', [
            'table' => $table,
            'categories' => Category::of(Gallery::class)->get()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        $entityId = intval($request->input('entity_id', 0));
        Gate::authorize($entityId > 0 ? 'edit galleries' : 'create galleries');

        $names = only_filled($request->name);
        $categoryId = intval($request->input('category_id', 0));
        $valid = true;

        if(empty($names))
        {
            Feedback::getInstance()->addError(__('Name is Required'));
            $valid = false;
        }

        if($categoryId == 0)
        {
            Feedback::getInstance()->addError(__('Please Select Category'));
            $valid = false;
        }

        if(!$valid)
        {
            Feedback::flash();
            return back();
        }

        $entity = $entityId > 0 ? Gallery::find($entityId) : new Gallery();

        $entity->name = fill_locales($names);
        $entity->status = GalleryStatus::Published;
        $entity->category_id = $categoryId;
        $entity->save();

        Feedback::getInstance()->addSuccess(__('Gallery Saved Successfully'));
        Feedback::flash();
        return to_route('dashboard.galleries.index');
    }

    /**
     * Store Gallery Items and Customizations
     *
     * @param Request $request
     *
     * @return Renderable
     */
    public function storeItems(Request $request)
    {
        Gate::authorize('customize galleries');
        $gallery = Gallery::find($request->entity_id);
        if($gallery == null) { return to_route('dashboard.galleries.index'); }

        $validItems = [];

        foreach($request->input('items', []) as $itemKey => $itemInputs)
        {
            if(!is_array($itemInputs)) { continue; }
            $itemType = GalleryItemType::tryFrom($itemInputs['type'] ?? 'photo');
            if($itemType == GalleryItemType::Photo)
            {
                //Photo Processing
                $file = $request->file("items.{$itemKey}.photo");
                $entity_id = $request->input("items.{$itemKey}.entity_id", 0);
                $originalItem = GalleryItem::find($entity_id);
                if(!$file && $originalItem == null) { continue; }
                $titles = fill_locales(only_filled($itemInputs['title']), '');
                if(empty($titles)) { continue; }
                $descriptions = only_filled($itemInputs['description']);
                //File Path
                $filePath = $file != null ? $file->store('galleries', ['disk' => 'uploads']) : (($originalItem != null) ? $originalItem->media_source : null);
                $item = new GalleryItem();
                $item->title = fill_locales($titles);
                $item->description = fill_locales($descriptions);
                $item->media_source = $filePath;
                $item->type = GalleryItemType::Photo;
                $validItems[] = $item;
            }
            if($itemType == GalleryItemType::Youtube)
            {
                $link = $request->input("items.{$itemKey}.youtube_link", null);
                if(empty($link)) { continue; }
                $titles = only_filled($itemInputs['title']);
                if(empty($titles)) { continue; }
                $descriptions = only_filled($itemInputs['description']);
                $item = new GalleryItem();
                $item->title = fill_locales($titles);
                $item->description = fill_locales($descriptions);
                $item->media_source = $link;
                $item->type = GalleryItemType::Youtube;
                $validItems[] = $item;
            }
        }
        $gallery->items()->delete();
        foreach($validItems as $item)
        {
            $item->gallery_id = $gallery->id;
            $item->save();
        }
        Feedback::getInstance()->addSuccess(__('Gallery Saved Successfully'));
        Feedback::flash();
        return to_route('dashboard.galleries.index');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit(Gallery $gallery)
    {
        return $this->index(new GalleriesTable())->with([
            'gallery' => $gallery
        ]);
    }

    /**
     * Show the customization form for specified gallery.
     * @param int $id
     * @return Renderable
     */
    public function customize(Gallery $gallery)
    {
        return view('galleries::dashboard.customize', [
            'gallery' => $gallery
        ]);
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy(Gallery $gallery)
    {
        Gate::authorize('delete galleries');
        $gallery->delete();
        Feedback::getInstance()->addSuccess(__('Gallery Deleted Successfully'));
        Feedback::flash();
        return to_route('dashboard.galleries.index');
    }
}
