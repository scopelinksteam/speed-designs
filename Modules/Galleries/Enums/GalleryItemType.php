<?php

namespace Modules\Galleries\Enums;

enum GalleryItemType : String
{
    case Photo = 'photo';
    case Video = 'video';
    case Youtube = 'youtube';
}
