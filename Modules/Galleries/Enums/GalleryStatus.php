<?php

namespace Modules\Galleries\Enums;

enum GalleryStatus : String
{
    case Draft = 'draft';
    case Published = 'published';
    case Pending = 'pending';
}
