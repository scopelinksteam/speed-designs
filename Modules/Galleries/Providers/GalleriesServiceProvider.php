<?php

namespace Modules\Galleries\Providers;

use Cascade\Dashboard\Entities\MenuItem;
use Cascade\Dashboard\Services\DashboardBuilder;
use Cascade\Dashboard\Services\DashboardMenu;
use Cascade\Site\Services\Shortcodes;
use Cascade\System\Entities\Category;
use Cascade\System\Services\Modules;
use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;
use Illuminate\Support\Facades\Blade;
use Modules\Galleries\Components\GalleryBuilderComponent;
use Modules\Galleries\Entities\Gallery;
use Modules\Galleries\Shortcodes\GalleryShortcode;

class GalleriesServiceProvider extends ServiceProvider
{
    /**
     * @var string $moduleName
     */
    protected $moduleName = 'Galleries';

    /**
     * @var string $moduleNameLower
     */
    protected $moduleNameLower = 'galleries';

    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerTranslations();
        $this->registerConfig();
        $this->registerViews();
        $this->loadMigrationsFrom(module_path($this->moduleName, 'Database/Migrations'));
        $this->registerMenu();
        Shortcodes::Register('gallery', GalleryShortcode::class);
        Blade::component(GalleryBuilderComponent::class, 'gallery-builder');
    }

    /**
     * Register Galleries Module Dashboard Menu
     *
     * @return void
     */
    protected function registerMenu()
    {
        DashboardBuilder::MainMenu()->Call(function(DashboardMenu $menu){
            $menu->AddItem(
                MenuItem::make(__('Galleries'), 'site')->icon('images')->children([
                    MenuItem::make(__('Manage Galleries'))->route('dashboard.galleries.index')->icon('images')->permission('view galleries'),
                    MenuItem::make(__('Galleries Categories'))->route('dashboard.galleries.categories.index')->icon('folder-open')->permission('view galleries categories')
                ])
            );
        });
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        Category::register(Gallery::class);
        $this->app->register(RouteServiceProvider::class);
    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->publishes([
            module_path($this->moduleName, 'Config/config.php') => config_path($this->moduleNameLower . '.php'),
        ], 'config');
        $this->mergeConfigFrom(
            module_path($this->moduleName, 'Config/config.php'), $this->moduleNameLower
        );
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $viewPath = resource_path('views/modules/' . $this->moduleNameLower);

        $sourcePath = module_path($this->moduleName, 'Resources/views');

        $this->publishes([
            $sourcePath => $viewPath
        ], ['views', $this->moduleNameLower . '-module-views']);

        $this->loadViewsFrom(array_merge($this->getPublishableViewPaths(), [$sourcePath]), $this->moduleNameLower);
    }

    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $langPath = resource_path('lang/modules/' . $this->moduleNameLower);
        if (is_dir($langPath))
        {
            $this->loadTranslationsFrom($langPath, $this->moduleNameLower);
        }
        else
        {
            if(Modules::DirectoryExists($this->moduleName, 'Resources/lang'))
            {
                $this->app['translation.loader']->addJsonPath(Modules::Path($this->moduleName, 'Resources/lang'));
                $this->loadTranslationsFrom(Modules::Path($this->moduleName, 'Resources/lang'), $this->moduleNameLower);
            }
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }

    private function getPublishableViewPaths(): array
    {
        $paths = [];
        foreach (\Config::get('view.paths') as $path) {
            if (is_dir($path . '/modules/' . $this->moduleNameLower)) {
                $paths[] = $path . '/modules/' . $this->moduleNameLower;
            }
        }
        return $paths;
    }
}
