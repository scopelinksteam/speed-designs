<?php

namespace Modules\Galleries\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Modules\Galleries\Enums\GalleryItemType;
use Spatie\Translatable\HasTranslations;

class GalleryItem extends Model
{

    use HasTranslations;

    protected $table = 'galleries_items';

    protected $casts = [
        'type' => GalleryItemType::class
    ];

    public $translatable = ['title', 'description', 'link_text'];

    public function gallery() : BelongsTo
    {
        return $this->belongsTo(Gallery::class, 'gallery_id');
    }
}
