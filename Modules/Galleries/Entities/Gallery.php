<?php

namespace Modules\Galleries\Entities;

use Cascade\System\Traits\HasCategories;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\MorphTo;
use Modules\Galleries\Enums\GalleryStatus;
use Spatie\Translatable\HasTranslations;

class Gallery extends Model
{
    use HasTranslations, HasCategories;

    protected $table = 'galleries';

    protected $casts = [
        'status' => GalleryStatus::class
    ];

    public $translatable = ['name'];


    public function bindable() : MorphTo
    {
        return $this->morphTo('bindable');
    }

    public function items() : HasMany
    {
        return $this->hasMany(GalleryItem::class, 'gallery_id');
    }
}
