<?php

namespace Modules\Galleries\Traits;

use Modules\Galleries\Entities\Gallery;

trait HasGalleries
{
    public function galleries()
    {
        return $this->morphMany(Gallery::class, 'bindable');
    }
}
