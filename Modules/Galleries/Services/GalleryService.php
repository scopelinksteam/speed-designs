<?php

namespace Modules\Galleries\Services;

use Illuminate\Support\Facades\Gate;
use Javoscript\MacroableModels\Facades\MacroableModels;
use Modules\Galleries\Entities\Gallery;
use Modules\Galleries\Entities\GalleryItem;
use Modules\Galleries\Enums\GalleryItemType;
use Modules\Galleries\Enums\GalleryStatus;

class GalleryService
{
    public static function InjectInto(String $model)
    {
        MacroableModels::addMacro($model, 'GetGallery', function(){
            return Gallery::where('bindable_type', get_class($this))->where('bindable_id', $this->id)->first();
        });

        MacroableModels::addMacro($model, 'StoreGalleryFromRequest', function(){
            $request = request();
            $entityId = $this->gallery ? $this->gallery->id : 0;
            Gate::authorize($entityId > 0 ? 'edit galleries' : 'create galleries');

            $names = [];
            $categoryId = 0;
            $valid = true;


            $entity = $entityId > 0 ? $this->gallery : new Gallery();

            if($entityId == 0)
            {
                $entity->name = fill_locales($names, '');
                $entity->status = GalleryStatus::Published;
                $entity->bindable_type = get_class($this);
                $entity->bindable_id = $this->id;
                $entity->category_id = $categoryId;
                $entity->save();
            }

            $validItems = [];

            foreach($request->input('gallery_items', []) as $itemKey => $itemInputs)
            {
                if(!is_array($itemInputs)) { continue; }
                $itemType = GalleryItemType::tryFrom($itemInputs['type'] ?? 'photo');
                if($itemType == GalleryItemType::Photo)
                {
                    //Photo Processing
                    $file = $request->file("gallery_items.{$itemKey}.photo");
                    $entity_id = $request->input("gallery_items.{$itemKey}.entity_id", 0);
                    $originalItem = GalleryItem::find($entity_id);
                    if(!$file && $originalItem == null) { continue; }
                    $titles = fill_locales(only_filled($itemInputs['title']), '');
                    if(empty($titles)) { continue; }
                    $descriptions = only_filled($itemInputs['description']);
                    //File Path
                    $filePath = $file != null ? $file->store('galleries', ['disk' => 'uploads']) : (($originalItem != null) ? $originalItem->media_source : null);
                    $item = new GalleryItem();
                    $item->title = fill_locales($titles);
                    $item->description = fill_locales($descriptions);
                    $item->media_source = $filePath;
                    $item->type = GalleryItemType::Photo;
                    $validItems[] = $item;
                }
                if($itemType == GalleryItemType::Youtube)
                {
                    $link = $request->input("gallery_items.{$itemKey}.youtube_link", null);
                    if(empty($link)) { continue; }
                    $titles = only_filled($itemInputs['title']);
                    if(empty($titles)) { continue; }
                    $descriptions = only_filled($itemInputs['description']);
                    $item = new GalleryItem();
                    $item->title = fill_locales($titles);
                    $item->description = fill_locales($descriptions);
                    $item->media_source = $link;
                    $item->type = GalleryItemType::Youtube;
                    $validItems[] = $item;
                }
            }
            $entity->items()->delete();
            foreach($validItems as $item)
            {
                $item->gallery_id = $entity->id;
                $item->save();
            }
        });

    }

}
