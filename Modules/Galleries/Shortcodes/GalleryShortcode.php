<?php

namespace Modules\Galleries\Shortcodes;

use Cascade\Site\Abstraction\Shortcode;
use Cascade\System\Services\System;
use Modules\Galleries\Entities\Gallery;

class GalleryShortcode extends Shortcode
{
    public function handle(): String|\Illuminate\View\View
    {
        $attributes = $this->attributes();
        if(!isset($attributes['id'])) { return $this->content(); }
        $gallery = Gallery::find(intval($attributes['id']));
        if($gallery == null) { return $this->content(); }
        return System::FindView('galleries::frontend.shortcodes.gallery', [
            'gallery' => $gallery,
            'per_row' => intval($attributes['per_row'] ?? 3)
        ]);
    }
}
