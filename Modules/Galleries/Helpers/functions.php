<?php

use Modules\Galleries\Entities\Gallery;

if(!function_exists('get_gallery'))
{
    function get_gallery($galleryId)
    { return Gallery::find($galleryId); }
}
