<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;
use Modules\Galleries\Entities\Gallery;
use Modules\Galleries\Http\Controllers\Dashboard\GalleriesController;

Route::dashboard(function(){
    Route::get('/', [GalleriesController::class, 'index'])->name('index');
    Route::post('/', [GalleriesController::class, 'store'])->name('store');
    Route::post('/items', [GalleriesController::class, 'storeItems'])->name('items.store');
    Route::get('/edit/{gallery}', [GalleriesController::class, 'edit'])->name('edit');
    Route::get('/delete/{gallery}', [GalleriesController::class, 'destroy'])->name('delete');
    Route::get('/customize/{gallery}', [GalleriesController::class, 'customize'])->name('customize');
    Route::prefix('categories')->as('categories.')->group(function() {
        Route::categories(Gallery::class);
    });
}, 'galleries');
