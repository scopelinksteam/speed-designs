<?php

namespace Modules\Galleries\Components;

use Cascade\System\Services\System;
use Illuminate\View\Component;
use Modules\Galleries\Entities\Gallery;

class GalleryBuilderComponent extends Component
{

    public $gridColumns = 1;

    public ?Gallery $gallery = null;

    public function __construct($gridColumns = 1, $gallery = null)
    {
        $this->gridColumns = $gridColumns;
        $this->gallery = $gallery;
    }

    public function render()
    {
        return System::FindView('galleries::dashboard.components.builder');
    }
}
