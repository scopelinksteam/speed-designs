<?php

namespace Modules\Galleries\Tables;

use Cascade\Dashboard\Services\Datatables\Action;
use Cascade\Dashboard\Services\Datatables\Column;
use Cascade\Dashboard\Services\Datatables\DataTable;
use Modules\Galleries\Entities\Gallery;

class GalleriesTable extends DataTable
{
    public function __construct($params = null)
    {
        $this->editColumn('category', function(Gallery $gallery){
            return $gallery->category?->name;
        })
        ->editColumn('items', function(Gallery $gallery){
            return $this->badge($gallery->items()->count());
        })
        ->editColumn('shortcode', function(Gallery $gallery){
            return $this->badge("[gallery id={$gallery->id}]", 'dark');
        })
        ;
    }

    public function query($search = null, $filters = [])
    {
        $query = Gallery::where('bindable_type', null);

        return $query;
    }

    public function columns()
    {
        return [
            Column::make('id')->text(__('ID')),
            Column::make('name')->text(__('Name')),
            Column::make('category')->text(__('Category')),
            Column::make('status')->text(__('Status')),
            Column::make('shortcode')->text(__('Shortcode')),
            Column::make('items')->text(__('Items')),
        ];
    }

    public function actions()
    {
        return [
            Action::make('edit', __('Edit'))->route('dashboard.galleries.edit', ['gallery' => 'id'])->permission('edit galleries'),
            Action::make('customize', __('Customize'))->route('dashboard.galleries.customize', ['gallery' => 'id'])->permission('customize galleries'),
            Action::make('delete', __('Delete'))->route('dashboard.galleries.delete', ['gallery' => 'id'])->permission('delete galleries'),
        ];
    }
}
