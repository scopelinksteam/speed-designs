<?php

namespace Plugins\UPayment\Foundation;

use Plugins\UPayment\Enums\Environments;

class UPaymentClient
{
    const DEVELOPMENT_ENV = 'https://api.upayments.com/test-payment';
    const PRODUCTION_ENV  = 'https://api.upayments.com/payment-request';

    protected String $merchantId = '';
    protected String $username = '';
    protected String $password = '';
    protected String $apiKey = '';
    protected String $authKey = '';
    protected bool $isProduction = false;
    protected String $processPaymentURL = '';

    public function __construct()
    {
        $this->merchantId = get_setting('integrations::upayment.api.merchant_id', '', false);
        $this->username = get_setting('integrations::upayment.api.username', '', false);
        $this->password = get_setting('integrations::upayment.api.password', '', false);
        $this->apiKey = get_setting('integrations::upayment.api.key', '', false);
        $this->authKey = get_setting('integrations::upayment.api.auth_key', '', false);
        $this->method = get_setting('integrations::upayment.api.method', '', false);
        $this->isProduction = get_setting('integrations::upayment.api.environment', 'development', false) == Environments::Production->value;
        $this->processPaymentURL = $this->isProduction ? static::PRODUCTION_ENV : static::DEVELOPMENT_ENV;
    }


    public function GetPaymentURL(mixed $orderId, mixed $customerId, float $amount, $successURL, $failureURL)
    {
        $fields = array(
            'merchant_id'=> $this->merchantId,
            'username' => $this->username,
            'password'=>stripslashes($this->password),
            'api_key'=> $this->isProduction ? password_hash($this->apiKey,PASSWORD_BCRYPT) : $this->apiKey, // in sandbox request
            //'api_key' =>password_hash('API_KEY',PASSWORD_BCRYPT), //In production mode, please pass API_KEY with BCRYPTfunction
            'order_id'=> $orderId.'-'.time(), // MIN 30 characters with strong unique function (like hashing function with time)
            'total_price'=> $amount,
            'CurrencyCode'=>'USD',//only works in production mode
            'CstFName'=>'Test Name',
            'CstEmail'=>'test@test.com',
            'CstMobile'=>'12345678',
            'success_url'=> $successURL,
            'error_url'=> $failureURL,
            'test_mode'=>$this->isProduction ? 0 : 1, // test mode enabled
            'customer_unq_token'=>$customerId, //pass unique customer identifier (eg: mobile number)
            //'kfast_card_token'=>'5On9XaeXNM',//pass encrypted kfast card token received through user card token API
            //'credit_card_token'=>'dzk9Z1Lr7q',// pass encrypted credit card token received through user card token API
            'whitelabled'=>false, // only accept in live credentials (it will not work in test)
            'payment_gateway'=>'cc',// only works in production mode
            'ProductName'=>json_encode(['computer','television']),
            'ProductQty'=>json_encode([2,1]),
            'ProductPrice'=>json_encode([150,1500]),
            'reference'=>'Ref'.str_pad($orderId, 10, '0', STR_PAD_LEFT), // Reference that you want to show in invoice in ref column
            //'ExtraMerchantsData'=>json_encode($extraMerchantsData)
        );
        $fields_string = http_build_query($fields);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$this->processPaymentURL);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,$fields_string);
        // receive server response ...
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $server_output = curl_exec($ch);
        curl_close($ch);
        $server_output = json_decode($server_output,true);
        return isset($server_output['paymentURL']) ? $server_output['paymentURL'] : $server_output;
        //return redirect($server_output['paymentURL']);
        //header('Location:'.$server_output['paymentURL']); // PHP
    }
}
