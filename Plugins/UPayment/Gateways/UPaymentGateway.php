<?php

namespace Plugins\UPayment\Gateways;

use Cascade\Payments\Abstractions\Gateway;
use Cascade\Payments\Contracts\IPaymentGateway;
use Cascade\Payments\Entities\PaymentTransaction;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class UPaymentGateway extends Gateway implements IPaymentGateway
{
    public function GetName(): string
    {
        return __('UPayment');
    }

    public function GetAlias(): string
    {
        return 'upayment';
    }

    public function GetMethods(): array
    {
        return [__('Visa'), __('Mastercard')];
    }

    public function GetLogo(): string
    {
        return '';
    }

    public function RequiresCard(): bool
    {
        return true;
    }

    public function ProcessPayment(PaymentTransaction $transaction) : View|RedirectResponse
    {
        return view('');
    }

    public function HandleCallback(Request $request): PaymentTransaction
    {
        return new PaymentTransaction();
    }

    public function HandleReturn(Request $request): PaymentTransaction
    {
        return new PaymentTransaction();
    }
}
