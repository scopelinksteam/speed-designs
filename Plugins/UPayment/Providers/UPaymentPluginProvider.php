<?php

namespace Plugins\UPayment\Providers;

use Cascade\Payments\Services\PaymentService;
use Cascade\Settings\Services\Settings;
use Illuminate\Support\ServiceProvider;
use Cascade\System\Services\Plugins;
use Plugins\UPayment\Enums\Currency;
use Plugins\UPayment\Enums\Environments;
use Plugins\UPayment\Enums\PaymentMethods;
use Plugins\UPayment\Gateways\UPaymentGateway;

class UPaymentPluginProvider extends ServiceProvider
{
    protected $pluginName = 'UPayment';

    protected $pluginSlug = 'upayment';


    /**
     * Register Plugins Settings
     *
     * @return void
     */
    protected function registerSettings()
    {
        Settings::on('integrations', function(){
            Settings::page(__('UPayment'), 'upayment', function(){
                Settings::group(__('API Access'), function(){
                    Settings::text(__('Merchant ID'), 'api.merchant_id');
                    Settings::text(__('Username'), 'api.username');
                    Settings::text(__('Password'), 'api.password');
                    Settings::text(__('API Key'), 'api.key');
                    Settings::text(__('Authorization Key'), 'api.auth_key');
                    Settings::select(__('Environment'), 'api.environment', function(){
                        return [
                            Environments::Development->value => __('Development'),
                            Environments::Production->value => __('Production'),
                        ];
                    });
                });
                Settings::group(__('Payment Setup'), function(){
                    Settings::select(__('Currency'), 'payment.currency', function(){
                        return [
                            Currency::USD->value => __('USD'),
                            Currency::EUR->value => __('EUR'),
                            Currency::AED->value => __('AED'),
                            Currency::SAR->value => __('SAR'),
                            Currency::KWD->value => __('KWD'),
                            Currency::QAR->value => __('QAR'),
                            Currency::BHD->value => __('BHD'),
                            Currency::OMR->value => __('OMR'),
                        ];
                    });
                    Settings::select(__('Method'), 'payment.method', function(){
                        return [
                            PaymentMethods::CreditCard->value => __('Credit Card'),
                            PaymentMethods::Knet->value => __('Knet'),
                            PaymentMethods::Transaction->value => __('Transaction'),
                        ];
                    });
                    Settings::select(__('Whitelable'), 'payment.whitelable', function(){
                        return [
                            0 => __('No'),
                            1 => __('Yes'),
                        ];
                    });
                });
            });
        });
    }

    /**
     * Register Plugin Menus
     *
     * @return void
     */
    protected function registerMenu()
    {
        //....
    }

    /**
     * Register Plugin Components
     *
     * @return void
     */
    protected function registerComponents()
    {
        //....
    }

    /**
     * Register Plugin Commands
     *
     * @return void
     */
    protected function registerCommands()
    {
        //....
    }

    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerConfig();
        $this->registerViews();
        $this->loadMigrationsFrom(Plugins::Path($this->pluginName, 'Database/Migrations'));
        $this->registerSettings();
        $this->registerComponents();
        $this->registerMenu();
        $this->registerCommands();
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(UPaymentPluginRouteProvider::class);
        PaymentService::RegisterGateway(new UPaymentGateway);
        $this->registerTranslations();
    }

    /**************** Core Implementation ****************/

    /**
     * Register Plugin Configurations.
     *
     * @return void
     */
    protected function registerConfig()
    {
        if(Plugins::FileExists($this->pluginName, 'Config/config.php'))
        {
            $this->publishes([
                Plugins::Path($this->pluginName, 'Config/config.php') => config_path($this->pluginSlug . '.php'),
            ], 'config');
            $this->mergeConfigFrom(
                Plugins::Path($this->pluginName, 'Config/config.php'), $this->pluginSlug
            );
        }
    }

    /**
     * Register Plugin Views.
     *
     * @return void
     */
    public function registerViews()
    {
        $viewPath = resource_path('views/plugins/' . $this->pluginSlug);

        $sourcePath = Plugins::Path($this->pluginName, 'Resources/views');

        $this->publishes([
            $sourcePath => $viewPath
        ], ['views', $this->pluginName . '-plugin-views']);

        $this->loadViewsFrom(array_merge($this->getPublishableViewPaths(), [$sourcePath]), $this->pluginSlug);
    }

    private function getPublishableViewPaths(): array
    {
        $paths = [];
        foreach (\Config::get('view.paths') as $path) {
            if (is_dir($path . '/plugins/' . $this->pluginSlug)) {
                $paths[] = $path . '/plugins/' . $this->pluginSlug;
            }
        }
        return $paths;
    }

    /**
     * Register Plugin translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $langPath = resource_path('lang/plugins/' . $this->pluginSlug);

        if (is_dir($langPath))
        {
            $this->loadTranslationsFrom($langPath, $this->pluginSlug);
        }
        else
        {
            if(Plugins::DirectoryExists($this->pluginName, 'Resources/lang'))
            {
                $this->app['translation.loader']->addJsonPath(Plugins::Path($this->pluginName, 'Resources/lang'));
                $this->loadTranslationsFrom(Plugins::Path($this->pluginName, 'Resources/lang'), $this->pluginSlug);
            }
        }
    }
}
