<?php

namespace Plugins\UPayment\Enums;

enum Environments : String
{
    case Development = 'development';
    case Production  = 'production';
}
