<?php

namespace Plugins\UPayment\Enums;

enum Currency : String
{
    case KWD = 'KWD';
    case SAR = 'SAR';
    case USD = 'USD';
    case BHD = 'BHD';
    case EUR = 'EUR';
    case OMR = 'OMR';
    case QAR = 'QAR';
    case AED = 'AED';
}
