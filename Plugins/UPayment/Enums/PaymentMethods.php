<?php

namespace Plugins\UPayment\Enums;

enum PaymentMethods : String
{
    case Knet = 'Knet';
    case CreditCard = 'cc';
    case Transaction = 'TransactionTransaction';
}
