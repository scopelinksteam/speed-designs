@extends('theme::layouts.fullwidth')
@section('title', $page->title)
@section('content')
<div class="row about-page">
    <!-- Header -->
    <div class="col-md-12 bg-white" id="about-header" style="background-image: url('{{uploads_url($page->thumbnail_file)}}')">
        <div class="row" style="height: 200px;">
            <div class="col-md-3 bg-white h-100"></div>
            <div class="col-md-2 h-100 border-end"></div>
            <div class="col-md-4 h-100"></div>
            <div class="col-md-3 bg-white h-100">
                <!-- Social -->
            </div>
        </div>
        <div class="row h-100">
            <div class="col-md-5 bg-white h-100 p-4">
                <div class="p-4">
                    <h3 class="mt-4">LOREM IPSUM DOLOR SIT AMET CONSECTETUR ADIPISCING ELIT</h3>
                    <p>{{lorem(30)}}</p>
                </div>
            </div>
            <div class="col-md-4 h-100 border-end"></div>
            <div class="col-md-2"></div>
            <div class="col-md-1 bg-white h-100">

            </div>
        </div>
    </div>
    <!-- Steps -->
    <div class="col-md-12 bg-white" id="about-steps">
        <div class="row">
            <div class="col-md-2 offset-md-6 p-4">
                <h3 class="border-bottom mb-4">01</h3>
                <h5>Lorem Ipsum</h5>
            </div>
            <div class="col-md-2 p-4 ">
                <h3 class="border-bottom mb-4">02</h3>
                <h5>Lorem Ipsum</h5>
            </div>
            <div class="col-md-2 p-4 ">
                <h3 class="border-bottom mb-4">03</h3>
                <h5>Lorem Ipsum</h5>
            </div>
        </div>
    </div>
    <div class="col-md-12 bg-light">
        <div class="row">
            <div class="col-md-8 offset-md-2 text-center p-4">
                <h2 class="mt-4">{{$page->title}}</h2>
                <p class="mb-4">
                    {!! $page->content !!}
                </p>
            </div>
        </div>
    </div>
    @if(sd_is_customer())
    <div class="col-md-12 bg-white p-4">
        <div class="row p-4">
            <div class="col-md-8">
                <h3>@lang('Start your new Project right now')</h3>
                <p class="text-muted">@lang('start creating your projects and find an expert to help your ideas come true using our platform')</p>
            </div>
            <div class="col-md-4">
                <a href="#" class="btn btn-primary w-100" data-bs-toggle="offcanvas" data-bs-target="#request-canvas">
                    <i class="bi bi-plus-circle me-2"></i>
                    <span class="">@lang('New Request')</span>
                </a>
            </div>
        </div>
    </div>
    @endif
    <div class="col-md-12 bg-white">
        <div class="row p-4">
            <h3 class="text-center my-4">@lang('Our Clients')</h3>
        </div>
        <div class="row justify-content-center mb-4">
            @foreach ($clients as $client)
            <div class="col-md-2">
                <img src="{{uploads_url($client->image_url)}}" alt="{{$client->title}}" class="img-fluid rouned-circle">
            </div>
            @endforeach
        </div>
    </div>
</div>
@endsection
