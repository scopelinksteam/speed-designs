<div>
    @if(!$showSuccess)
    <h3>@lang('Send Request')</h3>
    @if (!empty($alerts))
    <div class="alert alert-danger">{{$alerts}}</div>
    @endif
    <div class="col-md-12">
        <label for="inputEmail4" class="form-label">@lang('Space') m</label>
        <input type="number" wire:model="space" class="form-control" id="">
    </div>
    <div class="col-md-12">
        <label for="inputEmail4" class="form-label">@lang('Time')</label>
        <input type="text" wire:model="time" class="form-control" id="">
    </div>
    <div class="col-md-12">
        <label for="inputEmail4" class="form-label">@lang('Design Style')</label>
        <select class="form-select" wire:model="category" aria-label="multiple select example">
            <option selected="">@lang('Design Style')</option>
            @foreach ($categories as $category)
            <option value="{{$category->id}}">{{$category->name}}</option>
            @endforeach
        </select>
    </div>
    <div class="col-md-12">
        <label for="inputEmail4" class="form-label">@lang('Notes')</label>
        <input type="text" wire:model="notes" class="form-control" id="">
    </div>

    <div class="mb-3">
        <label for="formFile" class="form-label">@lang('Upload File') (.CAD, .PDF, .3DS)</label>
        <input class="form-control" wire:model="attachments" type="file" id="formFile" accept=".cad,.pdf,.3ds" multiple>
    </div>

    <select class="form-select" wire:model="type" aria-label="multiple select example">
        <option selected="">@lang('Design Type')</option>
        @foreach ($types as $type)
        <option value="{{$type->id}}">{{$type->name}}</option>
        @endforeach
    </select>

    <div class="col-md-12 mt-2">
        <label for="inputEmail4" class="form-label">@lang('Designer Code') (@lang('If Any'))</label>
        <input type="text" class="form-control" id="inputEmail4" wire:model="designer_code">
    </div>
    <div class="col-12 mt-4">
        <button type="button" class="dbtn btn btn-primary" wire:click="Save()">@lang('Send')</button>
    </div>
    @else
    <div class="success-message">
        <i class="bi bi-check-circle"></i>
          <h4>@lang('Transmission Succeeded')</h4>
          <p>@lang('Your Request has been submitted for estimation and will be available for next 24 hours')</p>
    </div>
    @endif
</div>
