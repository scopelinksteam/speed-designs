<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}" dir="{{ app()->getLocale() == 'ar'  ? 'rtl' : 'ltr' }}">

<head>
    @if(!empty(theme_option('general.google_analytics.measurement_tag')))
    <!-- Google tag (gtag.js) -->
    <script async src="https://www.googletagmanager.com/gtag/js?id={{theme_option('general.google_analytics.measurement_tag')}}"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', '{{theme_option('general.google_analytics.measurement_tag')}}');
    </script>
    @endif
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">

    <title>{{site_name()}} - @yield('title')</title>
    <meta content="" name="description">
    <meta content="" name="keywords">

    <!-- Favicons -->
    <link href="@theme/img/favicon.png" rel="icon">
    <link href="@theme/img/apple-touch-icon.png" rel="apple-touch-icon">

    <!-- Google Fonts -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link
        href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,300;0,400;0,500;0,600;0,700;1,300;1,400;1,600;1,700&family=Amatic+SC:ital,wght@0,300;0,400;0,500;0,600;0,700;1,300;1,400;1,500;1,600;1,700&family=Inter:ital,wght@0,300;0,400;0,500;0,600;0,700;1,300;1,400;1,500;1,600;1,700&display=swap"
        rel="stylesheet">

    <!-- Vendor CSS Files -->
    <link href="@theme/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="@theme/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
    <link href="@theme/vendor/glightbox/css/glightbox.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/boxicons@latest/css/boxicons.min.css" rel="stylesheet">

    <!-- Template Main CSS File -->
    <link href="@theme/css/dropify.css" rel="stylesheet">
    <link href="@theme/css/main.css" rel="stylesheet">
    <link href="@theme/css/custom.css" rel="stylesheet">
    @livewireStyles()
    <x-styles />
    @stack('styles')
</head>

<body>


    <!-- ======= Hero Section ======= -->
    <main id="main" class="d-flex flex-nowrap main margin-left">
        <div class="container-fluid margin-left">
            <div class="row">
                @include('theme::partials.sidenav')
                <div class="container-fluid p-0">
                    <div class="row">
                        <div class="col-12">
                          {!! \Cascade\System\Services\Feedback::display() !!}
                          @php \Cascade\System\Services\Feedback::flush() @endphp
                        </div>
                    </div>
                </div>
                <div class="col-md-12 col-xl-12">
                        @yield('content')
                </div>
                <!-- ======= Footer ======= -->
                <div class="offcanvas offcanvas-end " style="width: 450px;max-width:100;" tabindex="-1" id="notifications-canvas" aria-labelledby="offcanvasExampleLabel">
                    <div class="offcanvas-header">
                      <h5 class="offcanvas-title" id="offcanvasExampleLabel">@lang('Notifications')</h5>
                      <button type="button" class="btn-close" data-bs-dismiss="offcanvas" aria-label="Close"></button>
                    </div>
                    <div class="offcanvas-body">
                        @include('theme::partials.sidebars.notifications')
                    </div>
                </div>
                <!-- ======= Footer ======= -->
                @auth
                <div class="offcanvas offcanvas-end " style="width: 450px;max-width:100%;" tabindex="-1" id="notifications-canvas" aria-labelledby="offcanvasExampleLabel">
                    <div class="offcanvas-header">
                      <h5 class="offcanvas-title" id="offcanvasExampleLabel">@lang('Notifications')</h5>
                      <button type="button" class="btn-close" data-bs-dismiss="offcanvas" aria-label="Close"></button>
                    </div>
                    <div class="offcanvas-body">
                        @include('theme::partials.sidebars.notifications')
                    </div>
                </div>
                @endauth
                @if(sd_is_customer())
                <div class="offcanvas offcanvas-end " style="width: 450px;max-width:100%;" tabindex="-1" id="request-canvas" aria-labelledby="offcanvasExampl">
                    <div class="offcanvas-header">
                      <h5 class="offcanvas-title" id="">@lang('New Request')</h5>
                      <button type="button" class="btn-close" data-bs-dismiss="offcanvas" aria-label="Close"></button>
                    </div>
                    <div class="offcanvas-body">
                        @livewire('project.request')
                    </div>
                </div>
                @endif
                <footer id="footer" class="footer">
                    <div class="Scontainer">
                        <div class="copyright">
                            &copy; @lang('Copyright') <strong><span>{{site_name()}}</span></strong>. @lang('All Rights Reserved')
                        </div>
                    </div>

                </footer><!-- End Footer -->
                <!-- End Footer -->
            </div><!-- row -->
        </div><!-- container -->
    </main><!-- End #main -->

    <a href="#" class="scroll-top d-flex align-items-center justify-content-center"><i
            class="bi bi-arrow-up-short"></i></a>

    <div id="preloader"></div>
    @livewire('calls')

    @livewireScripts()
    <!-- Vendor JS Files -->
    <script src="@theme/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="@theme/vendor/glightbox/js/glightbox.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>


    <!-- Template Main JS File -->
    <script src="@theme/js/dropify.js"></script>
    <script src="@theme/js/main.js"></script>
    <script src="https://unpkg.com/@popperjs/core@2"></script>
    <script src="@theme/js/custom.js"></script>
    <x-scripts />
    @stack('scripts')
    <script>
        document.addEventListener('DOMContentLoaded', function(){
            $('.dropify-image').dropify();
        });
        var lightbox = GLightbox();
        lightbox.on('open', (target) => {
            console.log('lightbox opened');
        });
        var lightboxDescription = GLightbox({
            selector: '.glightbox2'
        });
        var lightboxVideo = GLightbox({
            selector: '.glightbox3'
        });
        lightboxVideo.on('slide_changed', ({
            prev,
            current
        }) => {
            console.log('Prev slide', prev);
            console.log('Current slide', current);

            const {
                slideIndex,
                slideNode,
                slideConfig,
                player
            } = current;

            if (player) {
                if (!player.ready) {
                    // If player is not ready
                    player.on('ready', (event) => {
                        // Do something when video is ready
                    });
                }

                player.on('play', (event) => {
                    console.log('Started play');
                });

                player.on('volumechange', (event) => {
                    console.log('Volume change');
                });

                player.on('ended', (event) => {
                    console.log('Video ended');
                });
            }
        });

        var lightboxInlineIframe = GLightbox({
            selector: '.glightbox4'
        });

        /* var exampleApi = GLightbox({ selector: null });
        exampleApi.insertSlide({
            href: 'https://picsum.photos/1200/800',
        });
        exampleApi.insertSlide({
            width: '500px',
            content: '<p>Example</p>'
        });
        exampleApi.insertSlide({
            href: 'https://www.youtube.com/watch?v=WzqrwPhXmew',
        });
        exampleApi.insertSlide({
            width: '200vw',
            content: document.getElementById('inline-example')
        });
        exampleApi.open(); */
    </script>

</body>

</html>
