<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}" dir="{{ app()->getLocale() == 'ar'  ? 'rtl' : 'ltr' }}">

<head>
    @if(!empty(theme_option('general.google_analytics.measurement_tag')))
    <!-- Google tag (gtag.js) -->
    <script async src="https://www.googletagmanager.com/gtag/js?id={{theme_option('general.google_analytics.measurement_tag')}}"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', '{{theme_option('general.google_analytics.measurement_tag')}}');
    </script>
    @endif
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">

    <title>{{site_name()}} - @yield('title')</title>
    <meta content="" name="description">
    <meta content="" name="keywords">

    <!-- Favicons -->
    <link href="@theme/img/favicon.png" rel="icon">
    <link href="@theme/img/apple-touch-icon.png" rel="apple-touch-icon">

    <!-- Google Fonts -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link
        href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,300;0,400;0,500;0,600;0,700;1,300;1,400;1,600;1,700&family=Amatic+SC:ital,wght@0,300;0,400;0,500;0,600;0,700;1,300;1,400;1,500;1,600;1,700&family=Inter:ital,wght@0,300;0,400;0,500;0,600;0,700;1,300;1,400;1,500;1,600;1,700&display=swap"
        rel="stylesheet">

    <!-- Vendor CSS Files -->
    <link href="@theme/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="@theme/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
    <link href="@theme/vendor/glightbox/css/glightbox.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/boxicons@latest/css/boxicons.min.css" rel="stylesheet">

    <!-- Template Main CSS File -->
    <link href="@theme/css/dropify.css" rel="stylesheet">
    <link href="@theme/css/main.css" rel="stylesheet">
    <link href="@theme/css/custom.css" rel="stylesheet">
    <x-styles />
</head>

<body>


    <!-- ======= Hero Section ======= -->
    <main id="main" class="d-flex flex-nowrap main">
        <div class="container">
            <div class="row">
                <div class="container-fluid p-0">
                    <div class="row">
                        <div class="col-12">
                          {!! \Cascade\System\Services\Feedback::display() !!}
                          @php \Cascade\System\Services\Feedback::flush() @endphp
                        </div>
                    </div>
                </div>
                <div class="col-md-12 col-xl-12 my-4">
                        @yield('content')
                </div>
                <!-- ======= Footer ======= -->

                <footer id="footer" class="footer">
                    <div class="Scontainer">
                        <div class="copyright">
                            &copy; @lang('Copyright') <strong><span>{{site_name()}}</span></strong>. @lang('All Rights Reserved')
                        </div>
                    </div>

                </footer><!-- End Footer -->
                <!-- End Footer -->
            </div><!-- row -->
        </div><!-- container -->
    </main><!-- End #main -->

    <a href="#" class="scroll-top d-flex align-items-center justify-content-center"><i
            class="bi bi-arrow-up-short"></i></a>

    <div id="preloader"></div>

    <!-- Vendor JS Files -->
    <script src="@theme/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="@theme/vendor/glightbox/js/glightbox.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://unpkg.com/@popperjs/core@2"></script>

    <!-- Template Main JS File -->
    <script src="@theme/js/dropify.js"></script>
    <script src="@theme/js/main.js"></script>
    <script src="@theme/js/custom.js"></script>
</body>

</html>
