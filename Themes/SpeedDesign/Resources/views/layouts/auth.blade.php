<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}" dir="{{ app()->getLocale() == 'ar'  ? 'rtl' : 'ltr' }}">

<head>
    @if(!empty(theme_option('general.google_analytics.measurement_tag')))
    <!-- Google tag (gtag.js) -->
    <script async src="https://www.googletagmanager.com/gtag/js?id={{theme_option('general.google_analytics.measurement_tag')}}"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', '{{theme_option('general.google_analytics.measurement_tag')}}');
    </script>
    @endif
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>{{get_setting('site::general.name')}} | @lang('Authentication')</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="@theme/img/favicon.png" rel="icon">
  <link href="@theme/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,300;0,400;0,500;0,600;0,700;1,300;1,400;1,600;1,700&family=Amatic+SC:ital,wght@0,300;0,400;0,500;0,600;0,700;1,300;1,400;1,500;1,600;1,700&family=Inter:ital,wght@0,300;0,400;0,500;0,600;0,700;1,300;1,400;1,500;1,600;1,700&display=swap" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="@theme/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="@theme/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
  <link href="@theme/vendor/glightbox/css/glightbox.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="@theme/css/main.css" rel="stylesheet">
  <link href="@theme/css/custom.css" rel="stylesheet">
  <x-styles />
</head>
@php
    $loginBackground = get_setting('system::login_page.background', null);
    $loginBackground = empty($loginBackground) ? null : uploads_url($loginBackground);
@endphp
<body class="login" style="min-height:100vh;background-image: url('{{$loginBackground}}'); background-size:cover; background-repeat:no-repeat;">


    <!-- ======= Hero Section ======= -->
    <main id="main" class="">
        <div id="content" class="content" style="min-height: auto;">
            @yield('content')
        </div>

    </main><!-- End #main -->

  <!-- ======= Footer ======= -->
  <footer id="footer" class="footer">


    <div class="container">
      <div class="copyright text-start mt-3">
        &copy; @lang('Copyright') <strong><span>{{get_setting('site::general.name')}}</span></strong>. @lang('All Rights Reserved')
      </div>
    </div>

    </footer><!-- End Footer -->
    <!-- End Footer -->

    <a href="#" class="scroll-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

    <div id="preloader"></div>


    <!-- Vendor JS Files -->
    <script src="@theme/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="@theme/vendor/glightbox/js/glightbox.min.js"></script>

    <!-- Template Main JS File -->
    <script src="@theme/js/main.js"></script>
    <x-scripts />
    <script>
                var lightbox = GLightbox();
                lightbox.on('open', (target) => {
                    console.log('lightbox opened');
                });
                var lightboxDescription = GLightbox({
                    selector: '.glightbox2'
                });
                var lightboxVideo = GLightbox({
                    selector: '.glightbox3'
                });
                lightboxVideo.on('slide_changed', ({ prev, current }) => {
                    console.log('Prev slide', prev);
                    console.log('Current slide', current);

                    const { slideIndex, slideNode, slideConfig, player } = current;

                    if (player) {
                        if (!player.ready) {
                            // If player is not ready
                            player.on('ready', (event) => {
                                // Do something when video is ready
                            });
                        }

                        player.on('play', (event) => {
                            console.log('Started play');
                        });

                        player.on('volumechange', (event) => {
                            console.log('Volume change');
                        });

                        player.on('ended', (event) => {
                            console.log('Video ended');
                        });
                    }
                });

                var lightboxInlineIframe = GLightbox({
                    selector: '.glightbox4'
                });

                /* var exampleApi = GLightbox({ selector: null });
                exampleApi.insertSlide({
                    href: 'https://picsum.photos/1200/800',
                });
                exampleApi.insertSlide({
                    width: '500px',
                    content: '<p>Example</p>'
                });
                exampleApi.insertSlide({
                    href: 'https://www.youtube.com/watch?v=WzqrwPhXmew',
                });
                exampleApi.insertSlide({
                    width: '200vw',
                    content: document.getElementById('inline-example')
                });
                exampleApi.open(); */
            </script>

    </body>

    </html>
