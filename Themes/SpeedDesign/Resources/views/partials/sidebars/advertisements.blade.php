
<div id="carouselExampleCaptions" class="carousel slide" data-bs-ride="carousel" data-bs-interval="3000">
    <div class="carousel-indicators">
      @foreach($advertisements as $ad)
      <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="{{$loop->index}}" class="{{$loop->iteration == 1 ? 'active' : ''}}" aria-current="{{$loop->iteration == 1 ? 'true' : 'false'}}" aria-label="Slide 1"></button>
      @endforeach
    </div>
    <div class="carousel-inner">
        @foreach($advertisements as $ad)
      <div class="carousel-item {{$loop->index == 0 ? 'active' : ''}}">
        <a href="{{$ad->Attribute('link_url')}}">
            <img src="{{uploads_url($ad->thumbnail_file)}}" class="d-block w-100" alt="..." style="width: 100%!important;object-fit: cover;aspect-ratio: 2 / .7;">
        </a>
        <div class="carousel-caption d-none d-md-block">
          <p>{!! $ad->content !!}</p>
        </div>
      </div>
      @endforeach
    </div>
    <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="visually-hidden">Previous</span>
    </button>
    <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="visually-hidden">Next</span>
    </button>
  </div>
