<div id="notifications" class="notifications">
    @foreach ($notifications as $notification)
    <div class="col-12">
        <div class="card mb-3">
            {!! $notification->render() !!}
        </div>
    </div>
    @endforeach
</div>
