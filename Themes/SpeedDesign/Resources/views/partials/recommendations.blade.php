@if($sponsors)
<div class="col-12 recommendation mt-4">
    <h3>@lang('Recommendation')</h3>
    <div id="carouselExampleControls" class="carousel slide" data-bs-ride="carousel" data-bs-interval="3000">
        <div class="carousel-inner">
            @foreach ($sponsors as $sponsor)
            <div class="carousel-item {{$loop->iteration == 1 ? 'active' : ''}}">
                <a href="{{route('sponsors.show', ['sponsor' => $sponsor])}}">
                    <img src="{{uploads_url($sponsor->thumbnail_file)}}" class="d-block w-100" alt="...">
                </a>
            </div>
            @endforeach
        </div>
        <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="visually-hidden">Previous</span>
        </button>
        <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="visually-hidden">Next</span>
        </button>
    </div>
</div>
@endif
