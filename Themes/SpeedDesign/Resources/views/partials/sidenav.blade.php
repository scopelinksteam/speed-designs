<div class="s">

    <section id="body-pd">
        <header class="header" id="header">
            <div class="header_toggle"><i class="bx bx-menu" id="header-toggle"></i></div>
        </header>
        <div class="l-navbar" id="nav-bar">
            <nav class="sidenav">
                <div>

                    <a href="{{url('')}}" class="nav_logo"><img src="{{ site_logo() }}" alt="{{ site_name() }}"
                            class="w-100" srcset=""><span class="nav_logo-name">{{ site_name() }}</span> </a>
                    <div class="nav_list">
                        @if(auth()->check() && auth()->user()->isSwitched())
                        <a href="{{ route('switch') }}" class="nav_link"> <i class="bx bx-grid-alt nav_icon"></i>
                            <span class="nav_name">@lang('Leave')</span> </a>
                        @endif
                        <a href="{{ url('/') }}" class="nav_link {{trim(url()->current(),'/') == trim(url('/'), '/') ? 'active' : ''}}"> <i class="bx bx-grid-alt nav_icon"></i>
                            <span class="nav_name">@lang('Home')</span> </a>
                            
                        @auth
                        <a href="{{ route('profile.index') }}" class="nav_link {{trim(url()->current(),'/') == trim(route('profile.index'), '/') ? 'active' : ''}}"> <i class="bx bx-user nav_icon"></i>
                            <span class="nav_name">@lang('Profile')</span> </a>
                        <a href="#" class="nav_link" data-bs-toggle="offcanvas" data-bs-target="#notifications-canvas"><i class="bx bx-bell nav_icon"></i>
                            <span class="nav_name">
                                @lang('Notifications')
                                @if(auth()->user()->impersonate()->notifications()->count() > 0)
                                <span class="badge bg-danger">{{auth()->user()->impersonate()->notifications()->count()}}</span>
                                @endif
                            </span>
                        </a>
                        @if(sd_is_customer())
                        <a href="#" class="nav_link" data-bs-toggle="offcanvas" data-bs-target="#request-canvas"><i class="bi bi-plus-circle"></i> <span
                            class="nav_name">@lang('New Request')</span> </a>
                        @endif

                        <a href="{{ route('projects.index') }}" class="nav_link {{trim(url()->current(),'/') == trim(route('projects.index'), '/') ? 'active' : ''}}"> <i class="bx bx-bookmark nav_icon"></i>
                            <span class="nav_name">@lang('Projects')</span> </a>

                        <a href="{{ route('contact') }}" class="nav_link {{trim(url()->current(),'/') == trim(route('contact'), '/') ? 'active' : ''}}"> <i class="bi bi-telephone  nav_icon"></i>
                                <span class="nav_name">@lang('Contact')</span> </a>

                        <a href="{{ route('chat') }}" class="nav_link {{trim(url()->current(),'/') == trim(route('chat'), '/') ? 'active' : ''}}"> <i class="bi bi-chat  nav_icon"></i>
                            <span class="nav_name">@lang('Chat')
                                <?php
                                    $ChMessage = App\Models\ChMessage::where('to_id', auth()->user()->id)
                                                                        ->where('seen', 0)
                                                                        ->count()
                                ?>
                                @if($ChMessage > 0)
                                    <span class="badge bg-danger">{{$ChMessage}}</span>
                                @endif
                            </span>
                        </a>
                        {{--
                        <a href="#" class="nav_link"> <i class="bx bx-folder nav_icon"></i> <span
                                class="nav_name">Files</span> </a>
                        <a href="#" class="nav_link"> <i class="bx bx-bar-chart-alt-2 nav_icon"></i> <span
                                class="nav_name">Stats</span> </a>
                        --}}
                        @endauth
                    </div>
                </div>
                @foreach (get_active_languages() as $language)
                @if($language->locale == app()->getLocale()) @continue @endif
                <a href="{{ route('translations.set_locale', ['locale' => $language->locale]) }}" class="nav_link"> <i class="bi bi-globe nav_icon"></i> <span
                    class="nav_name">{{$language->name}}</span> </a>
                @endforeach
                <a href="{{ url('/page/about-us') }}" class="nav_link {{trim(url()->current(),'/') == trim(url('/page/about-us'), '/') ? 'active' : ''}}"> <i class="bi bi-info-circle nav_icon"></i>
                    <span class="nav_name">@lang('About')</span> </a>
                @auth
                <a href="{{ route('logout') }}" class="nav_link"> <i class="bx bx-log-out nav_icon"></i> <span
                    class="nav_name">@lang('SignOut')</span> </a>
                @endauth
                @guest
                <a href="{{ route('login') }}" class="nav_link"> <i class="bx bx-log-out nav_icon"></i>
                    <span class="nav_name">@lang('Login')</span>
                </a>
                @endguest
            </nav>
        </div>
    </section>
</div><!-- End #sidemenu -->
