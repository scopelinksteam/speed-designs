<div id="nominated" class="nominated">
    <div class="row">
        <h3>@lang('Top Engineers')</h3>
        @foreach ($freelancers as $freelancer)
            <div class="col-md-4 nominated-box">
                <div class="card mb-3">
                    <div class="row g-0">
                        <div class="col-md-2">
                            <img src="{{$freelancer->avatar}}" class="img-fluid" alt="{{$freelancer->name}}">
                        </div>
                        <div class="col-md-10">
                            <div class="card-body">
                                <h5 class="card-title">{{$freelancer->name}}</h5>
                                <p class="id">@lang('ID') : {{$freelancer->id}}</p>
                                <p class="stars">
                                    @php $rating = doubleval($freelancer->Attribute('performance_rating', 0)); @endphp
                                    @php $halfStars = $rating % 2; @endphp
                                    @php $fullStars = $rating == 0 ? 0 : floor($rating / 2); @endphp
                                    @php $emptyStars = $rating == 0 ? 5 : (5 - $fullStars - ceil($halfStars)); @endphp
                                    @for ($i = 1; $i <= $fullStars; $i++)
                                    <i class="bi bi-star-fill"></i>
                                    @endfor
                                    @for ($i = 1; $i <= $halfStars; $i++)
                                    <i class="bi bi-star-half"></i>
                                    @endfor
                                    @for ($i = 1; $i <= $emptyStars; $i++)
                                    <i class="bi bi-star"></i>
                                    @endfor
                                </p>
                                {{--<p class="card-text">This is a wider card with supporting text below as a natural lead-in</p>--}}
                            </div>
                        </div>
                    </div>
                    <a href="{{route('profile.show', ['user' => $freelancer])}}" class="btn btn-primary dbtn">@lang('View profile')</a>
                </div>
            </div><!-- End -->
        @endforeach
    </div>
</div>
