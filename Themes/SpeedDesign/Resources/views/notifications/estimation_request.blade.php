<div class="card-body bg-white">
    <div class="row align-items-center">
        @if($notification->reference->customer)
        <div class="col-auto">
            <img src="{{$notification->reference->customer->avatar}}" alt="{{$notification->reference->customer->name}}" class="img-fluid rounded-circle m-0">
        </div>
        @endif
        <div class="col-auto">
            <h3 class="fs-6">{{$notification->title}}</h3>
            <a href="{{route('estimation.create', ['project' => $notification->reference])}}" class="btn btn-primary mt-1">@lang('Write Estimation')</a>
        </div>
    </div>
</div>
