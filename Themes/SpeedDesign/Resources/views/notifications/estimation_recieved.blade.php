@php $estimation = $notification->reference; @endphp
@if($estimation)
<div class="row g-0 align-items-center">
    <div class="col-md-2">
        <img src="{{$estimation->user->avatar}}" class="img-fluid" alt="...">
    </div>
    <div class="col-md-10">
        <div class="card-body">
            <a href="{{url('profile/show/'. $estimation->user->impersonate()->id)}}"><h5 class="card-title">{{$estimation->user->impersonate()->display_name}}, Estimation</h5></a>
            <p class="card-text" style="display: inline;">{{$estimation->notes}}</p>
            <a href="{{url('chat/'. $estimation->user->impersonate()->id)}}" style="display: inline;"><i class="bi bi-chat  nav_icon"></i></a>
        </div>

    </div>

    <div class="accordion" id="accordionExample1">
        <div class="accordion-item">
            <h2 class="accordion-header d-flex justify-content-between align-items-center" id="headingOne">
                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                    data-bs-target="#collapseOne1" aria-expanded="false" aria-controls="collapseOne">
                    @lang('Details')
                </button>
                <a href="{{route('estimation.approve', ['estimation' => $estimation])}}" class="dbtn small btn btn-primary me-3">@lang('Acceptance')</a>
            </h2>
            <div id="collapseOne1" class="accordion-collapse collapse" aria-labelledby="headingOne"
                data-bs-parent="#accordionExample1">
                <div class="accordion-body">
                    <div class="row">
                        <div class="col-6">
                            <i class="bi bi-check-circle"></i>
                            <p>@lang('Time'):</p>
                            <p class="bold-orange">{{$estimation->duration}} @lang('Days')</p>
                        </div>
                        <!--
                        <div class="col-6">
                            <i class="bi bi-check-circle"></i>
                            <p>Payments:</p>
                            <p class="bold-orange">3Days</p>
                        </div>
                        -->
                        <div class="col-6">
                            <i class="bi bi-check-circle"></i>
                            <p>@lang('Edit'):</p>
                            <p class="bold-orange">{{$estimation->Attribute('edits', 0)}} @lang('Edits')</p>
                        </div>
                        <div class="col-6">
                            <i class="bi bi-check-circle"></i>
                            <p>@lang('Stages'):</p>
                            <p class="bold-orange">{{$estimation->stages}} @lang('Stages')</p>
                        </div>
                        <div class="col-6">
                            <i class="bi bi-check-circle"></i>
                            <p>@lang('Price'):</p>
                            <p class="bold-orange">{{$estimation->price}} {{currency()->code}}</p>
                        </div>
                        <!--
                        <div class="col-6">
                            <i class="bi bi-check-circle"></i>
                            <p>Folder:</p>
                            <p class="bold-orange">3Days</p>
                        </div>
                        -->
                        <!--
                        <div class="col-12">
                            <i class="bi bi-check-circle"></i>
                            <p>Notes:</p>
                            <p class="bold-orange"></p>
                        </div>
                        -->

                    </div>

                </div>
            </div>
        </div>

    </div>

</div>
@endif
