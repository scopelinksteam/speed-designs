<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">

    <title>{{ site_name() }} - {{ $sponsor->title }}</title>
    <meta content="" name="description">
    <meta content="" name="keywords">

    <!-- Favicons -->
    <link href="assets/img/favicon.png" rel="icon">
    <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

    <!-- Google Fonts -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link
        href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,300;0,400;0,500;0,600;0,700;1,300;1,400;1,600;1,700&family=Amatic+SC:ital,wght@0,300;0,400;0,500;0,600;0,700;1,300;1,400;1,500;1,600;1,700&family=Inter:ital,wght@0,300;0,400;0,500;0,600;0,700;1,300;1,400;1,500;1,600;1,700&display=swap"
        rel="stylesheet">

    <!-- Vendor CSS Files -->
    <link href="@theme/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="@theme/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
    <link href="@theme/vendor/glightbox/css/glightbox.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/boxicons@latest/css/boxicons.min.css" rel="stylesheet">

    <!-- Template Main CSS File -->
    <link href="@theme/css/main.css" rel="stylesheet">
    <link href="@theme/css/custom.css" rel="stylesheet">

</head>

<body>


    <!-- ======= Hero Section ======= -->
    <main id="main" class="d-flex flex-nowrap main">
        <div class="container-fluid">
            <div class="row">
                @include('theme::partials.sidenav')


                <div id="content" class="content recommended">
                    @php
                    $sponsorBG = theme_option('appearance.sponsor.background', null);
                    $sponsorBG = $sponsorBG == null ? 'https://media.geeksforgeeks.org/wp-content/uploads/Screen-Shot-2017-11-13-at-10.23.39-AM.png' : uploads_url($sponsorBG);
                    @endphp
                    <div class="col-md-12 col-xl-12">
                        <div class="full-gallery"
                            style="background-image: url({{$sponsorBG}}); width: 100%; height: 100%; position: absolute; background-repeat: no-repeat;background-position: center; background-size: cover;">
                            <div class="gallery-content">
                                <div class="gallery-left"><a href="#"></a>
                                    <h2>{{ $sponsor->title }}</h2>
                                    <p>{!! $sponsor->content !!}</p>
                                </div>

                                <div class="gallery-right">
                                    <section>
                                        <div class="container">
                                            <div class="row">


                                                <div class="col-12">
                                                    <div class="container text-center my-3">
                                                        <div class="row mx-auto my-auto justify-content-center">
                                                            @if ($sponsor->GetGallery())
                                                                <div id="recipeCarousel" class="carousel slide"
                                                                    data-bs-ride="carousel">

                                                                    <div class="carousel-indicators">
                                                                        @foreach ($sponsor->GetGallery()->items as $item)
                                                                            <button type="button"
                                                                                data-bs-target="#recipeCarousel"
                                                                                data-bs-slide-to="{{ $loop->iteration - 1 }}"
                                                                                class="@if ($loop->iteration == 1) active @endif"
                                                                                aria-label="{{ $item->title }}"></button>
                                                                        @endforeach
                                                                    </div>

                                                                    <div class="carousel-inner" role="listbox">
                                                                        @foreach ($sponsor->GetGallery()->items as $item)
                                                                            <div
                                                                                class="carousel-item @if ($loop->iteration == 1) active @endif">
                                                                                <div class="col-md-4">
                                                                                    <div class="card">
                                                                                        <div class="card-img">
                                                                                            <img src="{{ uploads_url($item->media_source) }}"
                                                                                                class="img-fluid">
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        @endforeach
                                                                    </div>
                                                                    <a class="carousel-control-prev bg-transparent w-aut"
                                                                        href="#recipeCarousel" role="button"
                                                                        data-bs-slide="prev">
                                                                        <span class="carousel-control-prev-icon"
                                                                            aria-hidden="true"></span>
                                                                    </a>
                                                                    <a class="carousel-control-next bg-transparent w-aut"
                                                                        href="#recipeCarousel" role="button"
                                                                        data-bs-slide="next">
                                                                        <span class="carousel-control-next-icon"
                                                                            aria-hidden="true"></span>
                                                                    </a>
                                                                </div>
                                                            @endif
                                                            {{--
                                                            <div id="recipeCarousel" class="carousel slide"
                                                                data-bs-ride="carousel">

                                                                <div class="carousel-indicators">
                                                                    <button type="button"
                                                                        data-bs-target="#recipeCarousel"
                                                                        data-bs-slide-to="0" class="active"
                                                                        aria-label="Slide 2"></button>
                                                                    <button type="button"
                                                                        data-bs-target="#recipeCarousel"
                                                                        data-bs-slide-to="1"
                                                                        aria-label="Slide 2"></button>
                                                                    <button type="button"
                                                                        data-bs-target="#recipeCarousel"
                                                                        data-bs-slide-to="2"
                                                                        aria-label="Slide 3"></button>
                                                                    <button type="button"
                                                                        data-bs-target="#recipeCarousel"
                                                                        data-bs-slide-to="3"
                                                                        aria-label="Slide 3"></button>
                                                                    <button type="button"
                                                                        data-bs-target="#recipeCarousel"
                                                                        data-bs-slide-to="4"
                                                                        aria-label="Slide 3"></button>
                                                                    <button type="button"
                                                                        data-bs-target="#recipeCarousel"
                                                                        data-bs-slide-to="5"
                                                                        aria-label="Slide 3"></button>
                                                                </div>

                                                                <div class="carousel-inner" role="listbox">
                                                                    <div class="carousel-item active">
                                                                        <div class="col-md-4">
                                                                            <div class="card">
                                                                                <div class="card-img">
                                                                                    <img src="https://dummyimage.com/700x400/000/fff"
                                                                                        class="img-fluid">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="carousel-item">
                                                                        <div class="col-md-4">
                                                                            <div class="card">
                                                                                <div class="card-img">
                                                                                    <img src="https://dummyimage.com/700x400/000/fff"
                                                                                        class="img-fluid">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="carousel-item">
                                                                        <div class="col-md-4">
                                                                            <div class="card">
                                                                                <div class="card-img">
                                                                                    <img src="https://dummyimage.com/700x400/000/fff"
                                                                                        class="img-fluid">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="carousel-item">
                                                                        <div class="col-md-4">
                                                                            <div class="card">
                                                                                <div class="card-img">
                                                                                    <img src="https://dummyimage.com/700x400/000/fff"
                                                                                        class="img-fluid">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="carousel-item">
                                                                        <div class="col-md-4">
                                                                            <div class="card">
                                                                                <div class="card-img">
                                                                                    <img src="https://dummyimage.com/700x400/000/fff"
                                                                                        class="img-fluid">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="carousel-item">
                                                                        <div class="col-md-4">
                                                                            <div class="card">
                                                                                <div class="card-img">
                                                                                    <img src="https://dummyimage.com/700x400/000/fff"
                                                                                        class="img-fluid">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <a class="carousel-control-prev bg-transparent w-aut"
                                                                    href="#recipeCarousel" role="button"
                                                                    data-bs-slide="prev">
                                                                    <span class="carousel-control-prev-icon"
                                                                        aria-hidden="true"></span>
                                                                </a>
                                                                <a class="carousel-control-next bg-transparent w-aut"
                                                                    href="#recipeCarousel" role="button"
                                                                    data-bs-slide="next">
                                                                    <span class="carousel-control-next-icon"
                                                                        aria-hidden="true"></span>
                                                                </a>
                                                            </div>
                                                            --}}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                </div>
                            </div>
                        </div>
                    </div>


                </div><!-- End #content -->






                <!-- ======= Footer ======= -->
                <footer id="footer" class="footer">


                    <div class="Scontainer">
                        <div class="copyright">
                            &copy; @lang('Copyright') <strong><span>{{ site_name() }}</span></strong>.
                            @lang('All Rights Reserved')
                        </div>
                    </div>

                </footer><!-- End Footer -->
                <!-- End Footer -->


            </div><!-- row -->

        </div><!-- container -->

    </main><!-- End #main -->




    <a href="#" class="scroll-top d-flex align-items-center justify-content-center"><i
            class="bi bi-arrow-up-short"></i></a>

    <div id="preloader"></div>

    <!-- Vendor JS Files -->
    <script src="@theme/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>


    <!-- Template Main JS File -->
    <script src="@theme/js/main.js"></script>
    <script>
        let items = document.querySelectorAll('.carousel .carousel-item')

        items.forEach((el) => {
            const minPerSlide = 3
            let next = el.nextElementSibling
            for (var i = 1; i < minPerSlide; i++) {
                if (!next) {
                    // wrap carousel by using first child
                    next = items[0]
                }
                let cloneChild = next.cloneNode(true)
                el.appendChild(cloneChild.children[0])
                next = next.nextElementSibling
            }
        })
    </script>

</body>

</html>
