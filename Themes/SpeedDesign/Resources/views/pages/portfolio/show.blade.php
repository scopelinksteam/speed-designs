@extends('theme::layouts.master')
@section('title', $item->title)
@section('content')
@include('theme::partials.recommendations')
<section id="gallery" class="gallery section mt-4">
    <div class="containers">
        <div class="row">
            <div class="col-md-12">
                <h3>
                    <i class="fa fa-camera-retro heading-icon" aria-hidden="true"></i>
                    @lang('Gallery')
                </h3>
                @if($item->gallery)
                <ul class="box-container three-cols">
                    @foreach ($item->gallery->items as $galleryItem)
                    <li class="box">
                        <div class="inner">
                            <a href="{{uploads_url($galleryItem->media_source)}}" class="glightbox">
                                <img src="{{uploads_url($galleryItem->media_source)}}" alt="image">
                            </a>
                        </div>
                    </li>
                    @endforeach
                </ul>
                @endif
            </div>
        </div>
    </div>
</section>
@endsection
@push('sidebar')
@auth
    @if(count(array_intersect(auth()->user()->impersonate()->roles->pluck('id')->toArray(), get_setting('projects::general.setup.customers_roles', [], false)))  > 0)
    @livewire('project.request')
    @endif
@endauth
@endpush
