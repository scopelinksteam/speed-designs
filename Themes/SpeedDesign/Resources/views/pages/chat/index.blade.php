@extends('theme::layouts.fullwidth')
@section('content')
<div class="container-fluid w-100">
    <div class="row clearfix">
        <div class="col-lg-12">
            <div class="card chat-app d-flex" style="min-height:600px;">
                <div id="plist" class="people-list">

                    <ul class="list-unstyled chat-list mt-2 mb-0">
                        @foreach ($rooms as $room)
                        <li class="clearfix @if((isset($active_room) && $active_room->id == $room->id) || (!isset($active_room) && $loop->first)) active @endif">
                            <a href="#" class="" data-bs-toggle="tab" data-bs-target="#room-tab-{{$room->id}}">
                                <img src="https://bootdey.com/img/Content/avatar/avatar1.png" alt="avatar">
                                <div class="about">
                                    <div class="name">{{$room->room_title}}</div>
                                    <div class="status"> <i class="fa fa-circle offline"></i></div>
                                </div>
                            </a>
                        </li>
                        @endforeach
                    </ul>
                </div>
                <div class="tab-content">
                    @foreach ($rooms as $room)
                    <div  id="room-tab-{{$room->id}}" class="tab-pane fade @if((isset($active_room) && $active_room->id == $room->id) || (!isset($active_room) && $loop->first)) active show @endif">
                        @livewire('twilio-chatroom', ['room' => $room], key($room->id))

                    </div>
                    @endforeach
                </div>

            </div>
        </div>
    </div>
    </div>
@endsection
@push('styles')
    <link rel="stylesheet" href="@theme/css/chat.css">
@endpush
@push('scripts')
    @twilioConversationScripts()
@endpush
