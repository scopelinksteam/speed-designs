<div class="chat">
    <div class="chat-header clearfix text-dark">
        <div class="row">
            <div class="col-lg-6">
                <a href="javascript:void(0);" data-toggle="modal" data-target="#view_info">
                    <img src="https://bootdey.com/img/Content/avatar/avatar2.png" alt="avatar">
                </a>
                <div class="chat-about">
                    <h6 class="m-b-0">{{$room->room_title}}</h6>
                    <small>Last seen: 2 hours ago</small>
                </div>
            </div>
            <div class="col-lg-6 hidden-sm d-flex justify-content-end align-items-center">
                <a href="javascript:void(0);" class="btn btn-outline-secondary"><i class="bi bi-telephone"></i></a>
            </div>
        </div>
    </div>
    <div class="chat-history" x-data="{ scroll: () => { $el.scrollTo(0, $el.scrollHeight); }}"
        x-intersect="scroll()" >
        <ul class="m-b-0" wire:poll>
            @foreach ($messages as $messageRecord)
            <li class="clearfix" wire:key="{{$messageRecord->sid}}">
                <div class="message-data">
                    <span class="message-data-time">{{$messageRecord->date_created}}</span>
                </div>
                <div class="message @if($messageRecord->author == auth()->id()) my-message @else other-message float-right  @endif">{{$messageRecord->body}}</div>
            </li>
            @endforeach
        </ul>
    </div>
    <div class="chat-message clearfix" wire:ignore wire:key="chat-input">
        <div class="input-group mb-0">
            <div class="input-group-prepend">
                <span class="input-group-text"><i class="fa fa-send"></i></span>
            </div>
            <input type="text" class="form-control" placeholder="Enter text here..." wire:model.lazy="newMessageContent" wire:keydown.enter="SendMessage" wire:ignore.self>
        </div>
    </div>
</div>
