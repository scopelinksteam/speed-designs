@extends('theme::layouts.fullwidth')
@section('title', __('Contract'))
@section('content')
<div class="scontainer">
    <div class="row">
      <div class="col-md-12 col-xl-9">
        <div class="agree-a4">
          <div class="row">
            <div class="col-6">
              <img class="logo" src="@theme/img/logo.png">
            </div>
            <div class="col-3">
              <p><b>{{$estimation->user->impersonate()->display_name}}</b></p>
              <p><b>@lang('Invoice #')</b> {{$estimation->id}}</p>
            </div>
            <div class="col-3">
                <p><b>{{$project->customer->impersonate()->display_name}}</b></p>
                <p><b>@lang('Project #')</b> {{$project->id}}</p>
            </div>
            <div class="col-12 p-0">
              <h2>@lang('Deal Contract')</h2>
            </div>
            <hr>
            <table class="table table-striped">
                <tr>
                    <th>@lang('Customer')</th>
                    <td>{{$project->customer->impersonate()->display_name}}</td>
                </tr>
                <tr>
                    <th>@lang('Freelancer')</th>
                    <td>{{$estimation->user->impersonate()->display_name}}</td>
                </tr>
            </table>

            <div class="col-md-12">
                <p class="text-muted">
                {!! get_setting('app::terms.contract', '') !!}
                </p>
            </div>

            <table class="table table-striped">
                <tr>
                    <th>@lang('Design Style')</th>
                    <td>{{$project->category ? $project->category->name : __('NA')}}</td>
                    <th>@lang('Design Type')</th>
                    <td>{{$project->type ? $project->type->name : __('NA')}}</td>
                </tr>
                <tr>
                    <th>@lang('Space')</th>
                    <td>{{$project->Attribute('space', __('NA'))}}</td>
                    <th>@lang('Time')</th>
                    <td>{{$estimation->duration}} @lang('Days')</td>
                </tr>
                <tr>
                    <th>@lang('Price')</th>
                    <td>{{$estimation->price}} {{currency()->code}}</td>
                    <th>@lang('Delivery Stages')</th>
                    <td>{{$estimation->stages}} @lang('Stage(s)')</td>
                </tr>
                <tr>
                    <th>@lang('Free Edits')</th>
                    <td>{{$estimation->Attribute('edits', 0)}}</td>
                    <th>@lang('Edit Price')</th>
                    <td>{{$estimation->Attribute('edits_price', 0)}} {{currency()->code}}</td>
                </tr>
                <tr>
                    <th>@lang('Customer Notes')</th>
                    <td colspan="3">{{$project->description}}</td>
                </tr>
                <tr>
                    <th>@lang('Designer Notes')</th>
                    <td colspan="3">{{$estimation->notes}}</td>
                </tr>
            </table>


            <div class="col-6">
              <p><a href="#">Back</a></p>
            </div>
            <div class="col-6">
              <p><a href="{{route('estimation.contract.approve', ['estimation' => $estimation])}}" class="dbtn btn btn-primary">@lang('Accept Contract')</a></p>
            </div>

          </div>
        </div><!-- End #agree-a4 -->
      </div>


    </div>
  </div>
@endsection
