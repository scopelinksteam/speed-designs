@extends('theme::layouts.fullwidth')
@section('title', __('Terms'))
@section('content')
    <div class="scontainer">
        <div class="row">
            <div class="col-md-12 col-xl-9">
                <div class="agree-a4">
                    <div class="row">
                        <div class="col-8">
                            <img class="logo" src="@theme/img/logo.png">
                        </div>
                        <div class="col-4">
                            <p>@lang('Please Read and Approve our usage Terms')</p>
                        </div>

                        <div class="col-12">
                            <h2>@lang('TERMS OF THE DEAL')</h2>
                            {!! get_setting('app::terms.application', '') !!}
                        </div>
                        <div class="col-6">
                            <p><a href="{{url('')}}">@lang('Back')</a></p>
                        </div>
                        <div class="col-6">
                            <p><a href="{{ route('estimation.terms.approve', ['estimation' => $estimation]) }}"
                                    class="dbtn btn btn-primary">@lang('Accept Terms')</a></p>
                        </div>

                    </div>
                </div><!-- End #agree-a4 -->
            </div>


        </div>
    </div>
@endsection
