@extends('theme::layouts.fullwidth')
@section('title', __('Estimation'))
@section('content')
    <div class="row">
        <div class="col-md-12 col-xl-12">
            <div class="agree-a4">
                <div class="row">
                    <div class="col-8">
                        <img class="logo" src="@theme/img/logo.png">
                    </div>
                    <div class="col-4">
                        <p>
                            #{{$project->id}}
                            <br/>
                            {{$project->customer != null ? $project->customer->name : __('NA')}}
                        </p>
                    </div>

                    <div class="col-12">
                        <h2>@lang('Write an estimation')</h2>
                        <p>{!! get_setting('app::terms.estimation', __('Edit in Dashboard')) !!}</p>
                        <h5 class="mb-3">@lang('Details')</h5>
                        <div class="row mb-4 gy-3">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="form-label">@lang('Customer')</label>
                                    <input type="text" name="" id="" value="{{$project->customer != null ? $project->customer->name : __('NA')}}" class="form-control" disabled>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="form-label">@lang('Requested Since')</label>
                                    <input type="text" name="" id="" value="{{$project->created_at->diffForHumans()}}" class="form-control" disabled>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="form-label">@lang('Space')</label>
                                    <input type="text" name="" id="" value="{{$project->Attribute('space', '0')}} @lang('Meter')" class="form-control" disabled>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="form-label">@lang('Time')</label>
                                    <input type="text" name="" id="" value="{{$project->Attribute('desired_time', '0')}} @lang('Days')" class="form-control" disabled>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="form-label">@lang('Design Style')</label>
                                    <input type="text" name="" id="" value="{{$project->category  != null ? $project->category->name : __('NA')}}" class="form-control" disabled>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="form-label">@lang('Design Type')</label>
                                    <input type="text" name="" id="" value="{{$project->type  != null ? $project->type->name : __('NA')}}" class="form-control" disabled>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="" class="form-label">@lang('Customer Notes')</label>
                                    <textarea name="" id="" rows="10" class="form-control" disabled>{{$project->description}}</textarea>
                                </div>
                            </div>
                        </div>
                        <h5 class="mb-3">@lang('Project Attachments')</h5>
                        <table class="table table-striped">
                            @forelse($project->attachments as $attachment)
                            <tr>
                                <td>{{$attachment->name}}</td>
                                <td>
                                    
                                    <a href="{{url('uploads/'. $attachment->file)}}" class="btn btn-primary" target="_blank">@lang('Download')</a>
                                </td>
                            </tr>
                            @empty
                            <tr>
                                <td colspan="2">@lang('No Attachments Added')</td>
                            </tr>
                            @endforelse

                        </table>
                        <form action="{{route('estimation.store')}}" method="POST">
                            @csrf
                            <input type="hidden" name="project_id" value="{{$project->id}}">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label" for="email"></label>
                                        <div class="input-group">
                                            <div class="input-group-text">
                                                <i class="bi bi-calendar-check me-2"></i>
                                                @lang('Time')
                                            </div>
                                            <input type="number" class="form-control" id="time" name="time" placeholder="Time">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label" for="email"></label>
                                        <div class="input-group">
                                            <div class="input-group-text">
                                                <i class="bi bi-coin me-2"></i>
                                                @lang('Price')
                                            </div>
                                            <input type="number" class="form-control" id="time" name="price" placeholder="Price">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label" for="email"></label>
                                        <div class="input-group">
                                            <div class="input-group-text">
                                                <i class="bi bi-pencil me-2"></i>
                                                @lang('Free Edits')
                                            </div>
                                            <input type="number" name="edits" class="form-control" id="time" placeholder="Edits">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label" for="email"></label>
                                        <div class="input-group">
                                            <div class="input-group-text">
                                                <i class="bi bi-coin me-2"></i>
                                                @lang('Edit Price')
                                            </div>
                                            <input type="number" name="edits_price" class="form-control" id="time" placeholder="Edits">
                                        </div>
                                    </div>
                                </div>


                                <div class="col-md-12 mt-4">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-text">
                                                <i class="bi bi-back me-2"></i>
                                                @lang('Delivery Stages')
                                            </div>
                                            <select name="stages" id="stages" class="form-control">
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>


                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label" for="email"></label>
                                        <div class="input-group">
                                            <div class="input-group-text">
                                                <i class="bi bi-card-text me-2"></i>
                                                @lang('Extra Notes')
                                            </div>
                                            <textarea name="notes" class="form-control" id="time" placeholder="Notes"></textarea>
                                        </div>
                                    </div>
                                </div>





                            </div>

                            <div class="col-md-12">
                                <p style="float: right;margin-top: 20px;;"><button type="submit"
                                        class="dbtn small btn btn-primary">Send</button></p>
                            </div>

                        </form>

                    </div>



                </div>
            </div><!-- End #agree-a4 -->
        </div>


    </div>
@endsection
