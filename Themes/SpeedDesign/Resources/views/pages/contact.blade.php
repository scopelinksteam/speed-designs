@extends('theme::layouts.master')
@section('title', __('Contact us'))
@section('content')
<div class="container mt-4">
    <div class="col-md-8">
        <x-contact-form />
    </div>
</div>
@endsection
