@extends('theme::layouts.master')
@section('title', __('Home'))
@section('content')
<div class="contentTabs">
    <ul class="nav nav-tabs" id="myTab" role="tablist">
        @foreach ($portfolio_categories as $category)
        <li class="nav-item" role="presentation">
            <button class="nav-link @if($loop->iteration == 1) active @endif" id="tab-button-{{$category->id}}" data-bs-toggle="tab"
                data-bs-target="#category-tab-{{$category->id}}" type="button" role="tab" aria-controls="home"
                aria-selected="true" style="margin-bottom: 14px;width: 188px;padding-left: 0px;padding-right: 0px;">
                @if($category->thumbnail_file)
                <img src="{{uploads_url($category->thumbnail_file)}}" class="me-3" style="height: 20px !important;"  alt="{{$category->name}}" srcset="">
                @endif
                {{$category->name}}
            </button>
        </li>
        @endforeach
    </ul>
    <div class="tab-content" id="myTabContent">

        @foreach ($portfolio_categories->sortBy('name') as $category)
        @php
            $items = $category->items()->where('title', 'LIKE', '%'.request()->input('search', '').'%')->get();
        @endphp
        <div class="design-card tab-pane fade @if($loop->iteration == 1) show active @endif" id="category-tab-{{$category->id}}" role="tabpanel" aria-labelledby="tab-button-{{$category->id}}">
            <div class="row">
                @forelse ($items as $item)
                <div class="col-md-3 cardbox">
                    <div class="card">
                        @if($item->thumbnail_file)
                        <a href="{{route('portfolio.show', ['item' => $item])}}">
                            <img class="card-img-top" src="{{uploads_url($item->thumbnail_file)}}"
                                alt="Card image cap">
                        </a>
                        @endif
                        <div class="card-body">
                            <h5 class="card-title"><a href="{{route('portfolio.show', ['item' => $item])}}">{{$item->title}}</a></h5>
                            <p class="card-text">{{\Str::words(strip_tags($item->content), 20)}}</p>
                            <a href="{{route('portfolio.show', ['item' => $item])}}" class="btn btn-primary dbtn">@lang('Explore More')</a>
                        </div>
                    </div>
                </div>
                @empty
                <div class="bg-white rounded-3 text-dark p-3 col-md-12 cardbox">
                    @lang('No Results can be Found')
                </div>
                @endforelse
            </div>


        </div>
        @endforeach
    </div>
</div>
@include('theme::partials.nominated')
@endsection
