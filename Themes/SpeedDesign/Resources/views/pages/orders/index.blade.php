@extends('theme::layouts.fullwidth')
@section('title', __('All Orders'))
@section('content')
    <div id="content" class="content">
        <div class="scontainer">
            <div class="row">
                <div class="col-md-8">
                    <h2>@lang('All Orders')</h2>
                    <p>@lang('sed do eiusmod tempor incididunt ut labore et dolore')</p>
                </div>
                <div class="col-md-4">

                </div>
            </div>
        </div>

        <div class="col-md-8 orders">
            @foreach ($orders as $order)
                <div class="card mb-3" style="max-width: 540px;">
                    <div class="row g-0">
                        <div class="col-md-12"><img src="@theme/img/image.png" class="img-fluid profile" alt="...">
                            <h5 class="card-title">{{$order->title}}</h5>
                            <p class="card-text"><small class="text-muted">{{$order->created_at->diffForHumans()}}</small></p>
                        </div>
                        <div class="col-md-4">
                            <img src="@theme/img/image2.png" class="img-fluid rounded-start img" alt="...">
                        </div>
                        <div class="col-md-8">
                            <div class="card-body">

                                <p class="card-text">{{strip_tags($order->description)}}.</p>
                                <ul>
                                    <li>
                                        <p>Space</p> <i class="bi bi-alarm-fill"></i> {{$order->Attribute('space', __('NA'))}}
                                    </li>
                                    <li>
                                        <p>Design Type</p> <i class="bi bi-alarm-fill"></i> {{$order->type ? $order->type->name : __('NA')}}
                                    </li>
                                    <li>
                                        <p>Time</p> <i class="bi bi-alarm-fill"></i> {{ $order->Attribute('duration', __('NA')) }}
                                    </li>

                                </ul>
                                <a href="{{route('estimation.create', ['project' => $order])}}" class="dbtn small btn btn-primary">@lang('Send')</a>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach











        </div>


    </div>
@endsection
