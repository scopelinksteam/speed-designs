@extends('theme::layouts.fullwidth')
@section('title', __('My Projects'))
@section('content')
<section class="page page-projects page-projects-freelancer py-4">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2><span class="text-muted">@lang('My Projects')</span> #{{$projects->count()}}</h2>
                <hr>
                @foreach ($projects as $project)
                <div class="card card-project border-0 shadow-sm rounded-3 p-3">
                    <div class="card-body">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="row justify-content-between align-items-center">
                                        <div class="col-auto d-flex align-items-center">
                                            <input type="checkbox" name="" class="form-check-input rounded-circle m-0 me-3"  style="height: 32px; width:32px;" id="">
                                            <a href="{{route('projects.show', ['project' => $project])}}">
                                            {{$project->title}}
                                            <small class="text-muted">{{$project->type->name}}</small>
                                            </a>
                                        </div>
                                        <div class="col-auto">
                                            <small class="text-muted">(@lang('started'))</small>
                                            <button class="btn btn-white border btn-sm me-2">{{ $project->start_date }}</span>
                                            <button class="btn btn-white border btn-sm"><i class="bi bi-three-dots"></i></button>
                                        </div>
                                        <div class="col-12 my-3">
                                            <p class="text-black-50">{{ strip_tags($project->description) }}</p>
                                        </div>
                                        <div class="col-12">
                                            <div class="row justify-content-between align-items-center">
                                                <div class="col-6">
                                                    <div class="row align-items-center">
                                                        <div class="col-md-2 col-4">
                                                            <img src="{{$project->customer->impersonate()->avatar}}" class="rounded-circle w-100" alt="{{ $project->customer->impersonate()->display_name }}" srcset="">
                                                        </div>
                                                        <div class="col-8 text-black-50">
                                                            <span class="">{{ $project->customer->impersonate()->display_name }}</span>
                                                            {{--
                                                            <br />
                                                            <i class="bi bi-clock"></i>
                                                            03/22/2022 <small>(@lang('last meeting'))</small>
                                                            --}}
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="row justify-content-end align-items-center">
                                                        <div class="col-auto">
                                                            <span class="bg-success bg-opacity-10 text-success rounded-2 py-1 px-3"><i class="bi bi-lock me-2"></i>{{$project->price}} {{currency()->code}}</span>
                                                        </div>
                                                        @php $progress = (100 / $project->payments()->count()) * $project->payments()->where('is_paid', '1')->count(); @endphp
                                                        <div class="col-md-1">
                                                            <div class="progress">
                                                                <div class="progress-bar bg-success" role="progressbar" aria-label="Success example" style="width: {{$progress}}%" aria-valuenow="{{$progress}}" aria-valuemin="0" aria-valuemax="100"></div>
                                                            </div>
                                                        </div>
                                                        <div class="col-auto">
                                                            <span class="text-primary fw-bold">{{$project->payments()->where('is_paid', '1')->sum('amount')}}</span>
                                                            <span class="text-black-50">/{{$project->price}} {{currency()->code}}</span>
                                                        </div>
                                                        <div class="col-auto">
                                                            <button data-bs-toggle="modal" data-bs-target="#terms-modal-{{$project->id}}" class="btn btn-white btn-sm"><i class="bi bi-blockquote-left me-2"></i>@lang('Conditions')</button>
                                                        </div>
                                                        <div class="col-auto">
                                                            <a href="{{ url('/chat/'.$project->customer_id)}}" class="btn btn-white btn-sm"><i class="bi bi-chat me-2"></i>@lang('Chat')</a>
                                                        </div>
                                                        <div class="modal fade" id="terms-modal-{{$project->id}}" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                                                            <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-lg rounded-0 ">
                                                              <div class="modal-content">

                                                                <div class="modal-body p-4">
                                                                    <div class="row">
                                                                        <div class="col-8">
                                                                            <img class="logo" src="@theme/img/logo.png">
                                                                        </div>
                                                                        <div class="col-4">
                                                                            <p></p>
                                                                        </div>

                                                                        <div class="col-12">
                                                                            <h2>@lang('TERMS OF THE DEAL')</h2>
                                                                            {!! get_setting('app::terms.contract', '') !!}
                                                                            <table class="table table-striped">
                                                                                <tr>
                                                                                    <th>@lang('Design Style')</th>
                                                                                    <td>{{$project->category ? $project->category->name : __('NA')}}</td>
                                                                                    <th>@lang('Design Type')</th>
                                                                                    <td>{{$project->type ? $project->type->name : __('NA')}}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <th>@lang('Space')</th>
                                                                                    <td>{{$project->Attribute('space', __('NA'))}}</td>
                                                                                    <th>@lang('Time')</th>
                                                                                    <td>{{$project->duration}} @lang('Days')</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <th>@lang('Price')</th>
                                                                                    <td>{{$project->price}} {{currency()->code}}</td>
                                                                                    <th>@lang('Delivery Stages')</th>
                                                                                    <td>{{$project->stages}} @lang('Stage(s)')</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <th>@lang('Free Edits')</th>
                                                                                    <td>{{$project->Attribute('edits', 0)}}</td>
                                                                                    <th>@lang('Edit Price')</th>
                                                                                    <td>{{$project->Attribute('edits_price', 0)}} {{currency()->code}}</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <th>@lang('Customer Notes')</th>
                                                                                    <td colspan="3">{{$project->description}}</td>
                                                                                </tr>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                              </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
            {{--
            <div class="col-md-3 ">
                <h5 class="mb-4">@lang('Recieving Money')</h5>
                <div class="card shadow-sm rounded-3 border-0">
                    <div class="card-body">
                            <div class="recieving-money-block">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="bg-white border p-1 text-center">may<br />20</div>
                                    </div>
                                    <div class="col-md-8">
                                        @lang('You\'ll recieve amount of 500$')
                                        <br />
                                        <small class="text-black-50">@lang('From') : Ahmed Mahmoud</small>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="progress" style="height: 10px;">
                                            <div class="progress-bar bg-success" role="progressbar" aria-label="Success example" style="width: 25%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="recieving-money-block mt-3">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="bg-white border p-1 text-center">may<br />20</div>
                                    </div>
                                    <div class="col-md-8">
                                        @lang('You\'ll recieve amount of 500$')
                                        <br />
                                        <small class="text-black-50">@lang('From') : Ahmed Mahmoud</small>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="progress" style="height: 10px;">
                                            <div class="progress-bar bg-success" role="progressbar" aria-label="Success example" style="width: 25%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </div>
                </div>
            </div>
            --}}
        </div>
    </div>
</section>


@endsection
