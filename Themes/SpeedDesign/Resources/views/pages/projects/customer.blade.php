@extends('theme::layouts.fullwidth')
@section('title', __('Projects'))
@section('content')
<div class="container my-4">
    <div class="page page-projects page-project-customer overflow-scroll">
        <h3>@lang('Projects')</h3>
        <hr>
        <table class="table table-responsive" border="0" style="border-collapse: separate; border-spacing:0 1rem;border:0;">
            <thead class="border-0">
                <th class="border-0 text-center">@lang('Number')</th>
                <th class="border-0 text-center">@lang('Name of Holes')</th>
                <th class="border-0 text-center">@lang('Type')</th>
                <th class="border-0 text-center">@lang('Remaining Edits')</th>
                <th class="border-0 text-center">@lang('Start Date')</th>
                <th class="border-0 text-center">@lang('Notes')</th>
                <th class="border-0 text-center"></th>
            </thead>
            <tbody>
                @foreach ($projects as $project)
                    <tr class="bg-white rounded-3">
                        <td style="vertical-align: middle;" class="border-0 py-3 text-center fs-2 text-black-50">{{ $loop->iteration }}</td>
                        <td style="vertical-align: middle;" class="border-0 py-3 text-center align-items-center"></td>
                        <td style="vertical-align: middle;" class="border-0 py-3 text-center align-items-center">{{ $project->type->name }}</td>
                        <td style="vertical-align: middle;" class="border-0 py-3 text-center align-items-center">{{ $project->Attribute('edits', 0) }}</td>
                        <td style="vertical-align: middle;" class="border-0 py-3 text-center align-items-center">{{ $project->start_date }}</td>
                        <td style="vertical-align: middle;" class="border-0 py-3 text-center align-items-center">{{ $project->description }}</td>
                        <td style="vertical-align: middle;" class="border-0 py-3 text-center align-items-center">
                            <a class="btn btn-link"  href="{{route('projects.show', ['project' => $project])}}">
                                <i class="bi bi-plus fs-1"></i>
                              </a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>

    </div>
</div>
@endsection
