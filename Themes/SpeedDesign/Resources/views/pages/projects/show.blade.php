@extends('theme::layouts.fullwidth')
@section('title', __('Project') . ' #' . $project->id)
@section('content')
    <div class="row my-4">
        <div class="col-md-12 my-4">
            <h2>@lang('Project') #{{ $project->id }}</h2>
            <div>
                @php
                $otherUser = null;
                if($project->customer_id == auth()->id())
                {
                    $otherSideUser = $project->members()->whereNot('member_id', auth()->id())->first();
                    $otherUser = $otherSideUser ? $otherSideUser->member : null;
                }
                else {
                    $otherUser = $project->customer;
                }
                @endphp
                @if($otherUser)
                <div class="row">
                    <div class="col-auto">
                        <a href="{{ url('/chat/'.$otherUser->id)}}" class="btn btn-primary">@lang('Chat')</a>
                    </div>
                    @if($project->customer_id == auth()->id())
                    <div class="col-auto">
                        @if(sd_get_project_call_balance($project->id) <= 0)
                        <a class="btn btn-primary" href="{{route('calls.buy', ['project' => $project])}}">@lang('Buy Additional Calls (1 Hour)')</a>
                        @else
                        <x-call-user :projectId="$project->id" />
                        @endif
                    </div>
                    @endif
                </div>
                @endif
            </div>
        </div>
        <div class="col-md-12">
            <ul class="nav nav-tabs">
                <li class="nav-item">
                    <a href="#" class="nav-link active" data-bs-toggle="tab"
                        data-bs-target="#overview-tab">@lang('Overview')</a>
                </li>
                <li class="nav-item">
                    <a href="#" class="nav-link" data-bs-toggle="tab"
                        data-bs-target="#edits-tab">@lang('Edits')</a>
                </li>
                <li class="nav-item">
                    <a href="#" class="nav-link" data-bs-toggle="tab"
                        data-bs-target="#payments-tab">@lang('Payments')</a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane fade show active text-dark bg-white p-4" id="overview-tab" role="tabpanel">
                    <div class="row">
                        <div class="col-md-12">
                            <h4 class="my-3">@lang('Platform Terms')</h4>
                            {!! get_setting('app::terms.contract', '') !!}
                            <table class="table table-striped">
                                <tr>
                                    <th>@lang('Design Style')</th>
                                    <td>{{ $project->category ? $project->category->name : __('NA') }}</td>
                                    <th>@lang('Design Type')</th>
                                    <td>{{ $project->type ? $project->type->name : __('NA') }}</td>
                                </tr>
                                <tr>
                                    <th>@lang('Space')</th>
                                    <td>{{ $project->Attribute('space', __('NA')) }}</td>
                                    <th>@lang('Time')</th>
                                    <td>{{ $project->duration }} @lang('Days')</td>
                                </tr>
                                <tr>
                                    <th>@lang('Price')</th>
                                    <td>{{ $project->price }} {{currency()->code}}</td>
                                    <th>@lang('Delivery Stages')</th>
                                    <td>{{ $project->Attribute('stages', 0) }} @lang('Stage(s)')</td>
                                </tr>
                                <tr>
                                    <th>@lang('Free Edits')</th>
                                    <td>{{ min($project->edits()->count(), $project->Attribute('edits', 0))}} / {{ $project->Attribute('edits', 0) }}</td>
                                    <th>@lang('Edit Price')</th>
                                    <td>{{ $project->Attribute('edits_price', 0) }} {{currency()->code}}</td>
                                </tr>
                                <tr>
                                    <th>@lang('Customer Notes')</th>
                                    <td colspan="3">{{ $project->description }}</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade text-dark bg-white p-4" id="edits-tab" role="tabpanel">
                    <div class="row">
                        <div class="col-md-12 mb-4">
                            @if($project->customer->id != auth()->id())
                            <div class="card">
                                <div class="card-header">
                                    <div class="card-title">
                                        @lang('New Edit Request')
                                    </div>
                                </div>
                                <div class="card-body">
                                    <form action="{{route('projects.edits.store')}}" method="POST">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <label for="description" class="form-label">@lang('Description')</label>
                                            </div>
                                            <div class="col-md-7">
                                                <input type="text" name="description" id="description" class="form-control">
                                            </div>
                                            <div class="col-md-2">
                                                @csrf
                                                <input type="hidden" name="project_id" value="{{$project->id}}">
                                                <button type="submit" class="btn btn-primary">@lang('Save Edit')</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            @endif
                        </div>
                        <div class="col-md-12">
                            <table class="table table-striped">
                                <tr>
                                    <th>#</th>
                                    <th>@lang('Description')</th>
                                    <th>@lang('Status')</th>
                                    <th>@lang('Price')</th>
                                    <th></th>
                                </tr>
                                @foreach ($project->edits as $edit)
                                <tr>
                                    <td>{{$loop->iteration}}</td>
                                    <td>{{$edit->description}}</td>
                                    <td>{{$edit->status_id->translated()}}</td>
                                    <td>{{$edit->price}} {{currency()->code}}</td>
                                    <td>
                                        @if($project->customer->id == auth()->id() && ( $edit->payment != null && !($edit->payment->is_paid)))
                                        <a href="{{ route('pay', ['payment' => $edit->payment])}}" class="btn btn-primary">@lang('Pay')</a>
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                            </table>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade text-dark bg-white p-4" id="payments-tab" role="tabpanel">
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-striped">
                                <tr>
                                    <th>@lang('Due Date')</th>
                                    <th>@lang('Status')</th>
                                    <th></th>
                                </tr>
                                @foreach ($project->payments as $payment)
                                <tr>
                                    <td>{{$payment->due_date}}</td>
                                    <td>{{$payment->is_paid ? __('Paid') : __('Pending Payment')}}</td>
                                    <td>
                                        @if($project->customer->id == auth()->id() && !($payment->is_paid))
                                            <a href="{{ route('pay', ['payment' => $payment])}}" class="btn btn-primary">@lang('Pay')</a>
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
