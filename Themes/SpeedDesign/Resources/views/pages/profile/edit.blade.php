@extends('theme::layouts.fullwidth')
@section('title', __('Profile'))
@section('content')
<div id="content" class="content profile">
    <div class="scontainer">
      <div class="row">
        <div class="col-md-12">
          <div class="header-banner">
            <img src="@theme/img/banner.png">
          </div>
          <div class="uploadbtn" style="display: inline-block; float: right;"><a href="{{route('profile.edit')}}" class="dbtn small btn btn-light">@lang('Edit Profile')</a></div>
          <div class="profile-img">
            <img src="{{$user->avatar}}">
          </div>
          <div class="profile-info">
            <h3>{{$user->name}}</h3>
            <p>@lang('ID'): {{ $user->id}}</p>
            <i class="bi bi-star-fill"></i><i class="bi bi-star-fill"></i><i class="bi bi-star-fill"></i><i class="bi bi-star"></i><i class="bi bi-star"></i>

          </div>
          <ul class="nav nav-tabs mt-4 ps-4">
            <li class="nav-item">
                <a href="#" class="nav-link active" data-bs-toggle="tab" data-bs-target="#info-tab">@lang('About')</a>
            </li>
            @if($showGallery)
            <li class="nav-item">
                <a href="#" class="nav-link" data-bs-toggle="tab" data-bs-target="#gallery-tab">@lang('Gallery')</a>
            </li>
            @endif
          </ul>
          <form action="{{route('profile.store')}}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="tab-content px-4">
                <div class="tab-pane fade active show" id="info-tab">
                    <div class="profile-edit text-dark">

                        <div class="row mb-3">
                            <div class="col-sm-3">
                                <label for="name" class="form-label">@lang('Name')</label>
                            </div>
                            <div class="col-sm-9">
                                <input type="text" name="name" id="name" class="form-control" value="{{$user->name ?? old('name')}}">
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-sm-3">
                                <label for="email" class="form-label">@lang('Email')</label>
                            </div>
                            <div class="col-sm-9">
                                <input type="email" name="email" id="email" class="form-control" value="{{$user->email ?? old('email')}}">
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-sm-3">
                                <label for="phone" class="form-label">@lang('Phone')</label>
                            </div>
                            <div class="col-sm-9">
                                <input type="tel" name="phone" id="phone" class="form-control" value="{{$user->phone ?? old('phone')}}">
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-sm-3">
                                <label for="avatar" class="form-label">@lang('Profile Picture')</label>
                            </div>
                            <div class="col-sm-9">
                                <input type="file" name="avatar" id="avatar" class="dropify-image" data-allowed-file-extensions="jpg png bmp" data-default-file="{{$user->avatar}}">
                            </div>
                        </div>

                        @if($user->type == Modules\SpeedDesign\Entities\Freelancer::class)

                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="" class="form-label">@lang('First Language')</label>
                                            <input type="text" name="attributes[first_language]" id="" class="form-control" value="{{isset($user) ? $user->Attribute('first_language') : ''}}">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="" class="form-label">@lang('Second Language')</label>
                                            <input type="text" name="attributes[second_language]" id="" class="form-control" value="{{isset($user) ? $user->Attribute('second_language') : ''}}">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="" class="form-label">@lang('Job')</label>
                                            <input type="text" name="attributes[job]" id="" class="form-control" value="{{isset($user) ? $user->Attribute('job') : ''}}">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="" class="form-label">@lang('Design Sample')</label>
                                            <input type="text" name="attributes[design_sample]" id="" class="form-control" value="{{isset($user) ? $user->Attribute('design_sample') : ''}}">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="" class="form-label">@lang('Birthdate')</label>
                                            <input type="date" name="attributes[birthdate]" id="" class="form-control" value="{{isset($user) ? $user->Attribute('birthdate') : ''}}">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="" class="form-label">@lang('Nationality')</label>
                                            <input type="text" name="attributes[nationality]" id="" class="form-control" value="{{isset($user) ? $user->Attribute('nationality') : ''}}">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="" class="form-label">@lang('Experience')</label>
                                            <input type="text" name="attributes[experience]" id="" class="form-control" value="{{isset($user) ? $user->Attribute('experience') : ''}}">
                                        </div>
                                    </div>
                                </div>

                            @endif



                  </div>
                </div>
                @if($showGallery)
                <div class="tab-pane fade " id="gallery-tab">
                    <div class="container">
                        <div class="row pt-4">
                            <div class="col-md-12">
                                <input type="file" name="attachments[gallery][]" id="" class="dropify-image" data-allowed-file-extensions="jpg png bmp" multiple>
                            </div>
                        </div>
                    </div>
                </div>
                @endif
            </div>
            <div class="row px-4">
                <div class="col-md-12">
                    <div class="row mb-3 mt-4">
                        <div class="col-sm-9 offset-sm-3">
                            <button type="submit" class="dbtn btn btn-light">@lang('Save Profile')</button>
                        </div>
                    </div>
                </div>
            </div>
          </form>


        </div>

      </div>
    </div>

  </div>
@endsection
