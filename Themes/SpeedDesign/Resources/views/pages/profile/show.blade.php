@extends('theme::layouts.fullwidth')
@section('title', __('Profile'))
@section('content')
<div id="content" class="content profile">
    <div class="scontainer">
      <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-6">
                    <div class="header-banner">
                        <img src="@theme/img/banner.png">
                      </div>
                </div>
                <div class="col-md-6">

                </div>
            </div>

          <div class="uploadbtn" style="display: inline-block; float: right;">
            <a href="{{ url('/chat/'.$user->id) }}" class="btn btn-primary"><i
                class="bi bi-chat text-white me-2"></i>@lang('Chat')</a>
        </div>
          <div class="profile-img">
            <img src="{{$user->avatar}}">
          </div>

          <div class="profile-info">
            <h3>{{$user->name}}</h3>
            <p>@lang('ID'): {{ $user->id}}</p>
            <i class="bi bi-star-fill"></i><i class="bi bi-star-fill"></i><i class="bi bi-star-fill"></i><i class="bi bi-star"></i><i class="bi bi-star"></i>

          </div>
          <ul class="nav nav-tabs mt-4 ps-4">
            <li class="nav-item">
                <a href="#" class="nav-link active" data-bs-toggle="tab" data-bs-target="#info-tab">@lang('About')</a>
            </li>
            @if($user->impersonate() instanceof \Modules\SpeedDesign\Entities\Freelancer)
            <li class="nav-item">
                <a href="#" class="nav-link" data-bs-toggle="tab" data-bs-target="#gallery-tab">@lang('Gallery')</a>
            </li>
            @endif
          </ul>
          <div class="tab-content px-4">
            <div class="tab-pane fade active show" id="info-tab">
                <div class="profile-edit text-dark">
                  <ul>
                    <li><span>@lang('Name'):</span> <strong>{{$user->name}}</strong> </li>
                    <li><span>@lang('Email'):</span> <strong>{{$user->email}}</strong></li>
                    <li><span>@lang('Phone'):</span> <strong>{{$user->phone}}</strong></li>
                    @if($user->type == Modules\SpeedDesign\Entities\Freelancer::class)
                    <li>
                        <span>@lang('First Language') : </span>
                        <strong>{{$user->impersonate()->Attribute('first_language', __('NA'))}}</strong>
                    </li>
                    <li>
                        <span>@lang('Second Language') : </span>
                        <strong>{{$user->impersonate()->Attribute('second_language', __('NA'))}}</strong>
                    </li>
                    <li>
                        <span>@lang('Job') : </span>
                        <strong>{{$user->impersonate()->Attribute('job', __('NA'))}}</strong>
                    </li>
                    <li>
                        <span>@lang('Design Sample') : </span>
                        <strong>{{$user->impersonate()->Attribute('design_sample', __('NA'))}}</strong>
                    </li>
                    <li>
                        <span>@lang('Birthdate') : </span>
                        <strong>{{$user->impersonate()->Attribute('birthdate', __('NA'))}}</strong>
                    </li>
                    <li>
                        <span>@lang('Nationality') : </span>
                        <strong>{{$user->impersonate()->Attribute('nationality', __('NA'))}}</strong>
                    </li>
                    <li>
                        <span>@lang('Experience') : </span>
                        <strong>{{$user->impersonate()->Attribute('experience', __('NA'))}}</strong>
                    </li>
                    @endif
                  </ul>
                </div>
            </div>
            @if($user->impersonate() instanceof \Modules\SpeedDesign\Entities\Freelancer)
            <div class="tab-pane fade text-dark" id="gallery-tab">
                <div class="container text-dark my-4">
                    <div class="row">
                        @foreach ($user->attachments()->where('identifier', 'profile')->get() as $file)
                        <div class="col-md-3">
                            <div class="card">
                                <img src="{{uploads_url($file->file)}}" alt="" srcset="">
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
            @endif
          </div>

        </div>

      </div>
    </div>

  </div>
@endsection
