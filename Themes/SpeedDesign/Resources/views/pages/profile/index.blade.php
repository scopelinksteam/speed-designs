@extends('theme::layouts.fullwidth')
@section('title', __('Profile'))
@section('content')
<div id="content" class="content profile">
    <div class="scontainer">
      <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-6">
                    <div class="header-banner">
                      <img src="@theme/img/banner.png">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-4 mb-2">
                            <div class="bg-white shadow-sm p-3 text-center">
                                <p>@lang('Deals Count')</p>
                                <h3>{{$projects->count()}}</h3>
                            </div>
                        </div>
                        <div class="col-md-4 mb-2">
                            <div class="bg-white shadow-sm p-3 text-center">
                                <p>@lang('Complete Deals')</p>
                                <h3>{{$projects->where('status_id', 5)->count()}}</h3>
                            </div>
                        </div>
                        <div class="col-md-4 mb-2">
                            <div class="bg-white shadow-sm p-3 text-center">
                                <p>@lang('InProgress Deals')</p>
                                <h3>{{$projects->where('status_id', '<>', 5)->count()}}</h3>
                            </div>
                        </div>
                        <div class="col-md-4 mb-2">
                            <div class="bg-white shadow-sm p-3 text-center">
                                <p>@lang('Total Balance')</p>
                                <h3>{{$projects->sum('price')}}</h3>
                            </div>
                        </div>
                        <div class="col-md-4  mb-2">
                            <div class="bg-white shadow-sm p-3 text-center">
                                <p>@lang('Locked Balance')</p>
                                @php
                                    $list = $projects;
                                    $lockedBalance = 0;
                                    foreach($list as $item)
                                    {
                                        $lockedBalance += $item->payments()->where('is_paid', 1)->sum('amount');
                                    }
                                @endphp
                                <h3>{{$lockedBalance}}</h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
          <div class="uploadbtn" style="display: inline-block; float: right;"><a href="{{route('profile.edit')}}" class="dbtn small btn btn-light">@lang('Edit Profile')</a></div>
          <div class="profile-img">
            <img src="{{$user->avatar}}">
          </div>

          <div class="profile-info">
            <h3>{{$user->name}}</h3>
            <p>@lang('ID'): {{ $user->id}}</p>
            <i class="bi bi-star-fill"></i><i class="bi bi-star-fill"></i><i class="bi bi-star-fill"></i><i class="bi bi-star"></i><i class="bi bi-star"></i>

          </div>
          <ul class="nav nav-tabs mt-4 ps-4">
            <li class="nav-item">
                <a href="#" class="nav-link active" data-bs-toggle="tab" data-bs-target="#info-tab">@lang('About')</a>
            </li>
            @if($showGallery)
            <li class="nav-item">
                <a href="#" class="nav-link" data-bs-toggle="tab" data-bs-target="#gallery-tab">@lang('Gallery')</a>
            </li>
            @endif
          </ul>
          <div class="tab-content px-4">
            <div class="tab-pane fade active show" id="info-tab">
                <div class="profile-edit text-dark">
                  <ul>
                    <li><span>@lang('Name'):</span> <strong>{{$user->name}}</strong> </li>
                    <li><span>@lang('Email'):</span> <strong>{{$user->email}}</strong></li>
                    {{-- <li><span>@lang('Phone'):</span> <strong>{{$user->phone}}</strong></li> --}}
                  </ul>
                </div>
            </div>
            @if($showGallery)
            <div class="tab-pane fade text-dark" id="gallery-tab">
                <div class="container text-dark my-4">
                    <div class="row">
                        @foreach ($user->attachments()->where('identifier', 'profile')->get() as $file)
                        <div class="col-md-3">
                            <div class="card" style="margin-bottom: 25px;">
                                <img src="{{uploads_url($file->file)}}" alt="" srcset="" style="width: 100%!important;object-fit: cover;aspect-ratio: 2 / 2;">
                                @if($user->id == auth()->id())
                                <div class="card-footer" style="text-align: center;">
                                    <a href="{{route('profile.gallery.delete.image', ['attachment' => $file])}}" class="btn btn-danger" >@lang('Delete Image')</a>
                                </div>
                                @endif
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
            @endif
          </div>

        </div>

      </div>
    </div>

  </div>
@endsection
