@extends('theme::layouts.master')
@section('title', $page->title)
@section('content')
<div class="card shadow-sm border-0 rounded-3">
    <div class="card-body p-4">
        <h2 class="mb-4">{{$page->title}}</h2>
        <div class="">
            {!! $page->content !!}
        </div>
    </div>
</div>
@endsection
