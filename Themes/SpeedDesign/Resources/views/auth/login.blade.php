@extends('theme::layouts.auth')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-auto">
            <div class="login-form rounded-3 mt-4">

                <h2>@lang('Login to') <span>{{site_name()}}</span></h2>
                <p>@lang('Login with mail and your password').</p>
                <form method="POST" action="{{route('login')}}">
                    @csrf
                    <!-- Session Status -->
                    <x-auth-session-status class="mb-4" :status="session('status')" />
                    <!-- Validation Errors -->
                    <x-auth-validation-errors class="mb-4" :errors="$errors" />
                    <div class="form-group">
                        <label for="exampleInputEmail1">@lang('Email')</label>
                        <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"
                            placeholder="Enter Email" name="email" value="{{ old('email') }}">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">@lang('Password')</label>
                        <input type="password" name="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
                    </div>

                    <p><a href="{{ route('password.request') }}">@lang('Forget password')</a></p>

                    <button type="submit" class="dbtn btn btn-primary">@lang('Login')</button>
                    @if(google_auth_enabled() || facebook_auth_enabled() || twitter_auth_enabled())
                    <p class="login-with">@lang('or login with')</p>
                    @endif
                    <ul>
                        @if(google_auth_enabled())
                        <li style="border:1px solid #d32c2c;background-color:#d32c2c;">
                            <a href="{{ route('auth.login.google') }}" class="" style="background-color:#d32c2c;color:#fff;">
                                <i class="bi bi-envelope"></i>
                            </a>
                        </li>
                        @endif
                        @if(facebook_auth_enabled())
                        <li style="border:1px solid #2778ce;background-color:#2778ce;">
                            <a href="{{ route('auth.login.facebook') }}" class="" style="background-color:#2778ce;color:#fff;">
                                <i class="bi bi-facebook"></i></a>
                        </li>
                        @endif
                        @if(twitter_auth_enabled())
                        <li style="border:1px solid #00b6ff;background-color:#00b6ff;">
                            <a href="{{ route('auth.login.twitter') }}" class="" style="background-color:#00b6ff;color:#fff;">
                            <i class="bi bi-twitter"></i>
                            </a>
                        </li>
                        @endif
                    </ul>

                    <p class="create-new mb-0"><a href="{{route('register')}}">@lang('Create new account')</a></p>
                </form>
            </div>
        </div>

    </div>
</div>
@endsection
