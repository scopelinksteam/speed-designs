@extends('theme::layouts.auth')
@section('content')
    <div id="content" class="content" style="min-height: auto;">
        <div class="container">
            <div class="row">
                <div class="col-md-10 offset-md-1">
                    <div class="register-form rounded-3 mt-4 p-4">
                        <div class="row align-items-center p-4 pb-0" style="">
                            <div class="col-auto">
                                <img src="{{ site_logo() }}" alt="{{ site_name() }}" class="img-fluid">
                            </div>
                            <div class="col-auto">
                            </div>
                        </div>
                        <!-- Session Status -->
                        <x-auth-session-status class="mb-4" :status="session('status')" />

                        @if (count($errors) > 0)
                        <div class="row p-4 pb-0">
                            <div class="col-md-12">
                                <div class="alert alert-danger">
                                    @foreach ($errors->all() as $error)
                                        <div>{{ $error }}</div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        @endif
                        @php $activeTab = old('tab') ?? 'member-register'; @endphp
                        <div class="contentTabs p-4 pt-1 pb-0">
                            <h3 class="text-center mb-3">@lang('Create New Account')</h3>
                            <ul class="nav nav-tabs mb-2 d-flex justify-content-center" id="myTab" role="tablist">
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link @if ($activeTab == 'member-register') active @endif" id="member-tab"
                                        data-bs-toggle="tab" data-bs-target="#member-register" type="button" role="tab"
                                        aria-controls="member-register" aria-selected="true">@lang('Register as member')</button>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link @if ($activeTab == 'company-register') active @endif"
                                        id="company-tab" data-bs-toggle="tab" data-bs-target="#company-register"
                                        type="button" role="tab" aria-controls="company-register"
                                        aria-selected="false">@lang('Register as a company')</button>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link @if ($activeTab == 'freelancer-register') active @endif"
                                        id="freelancer-tab" data-bs-toggle="tab" data-bs-target="#freelancer-register"
                                        type="button" role="tab" aria-controls="freelancer-register"
                                        aria-selected="false">@lang('Register as a Freelancer')</button>
                                </li>
                            </ul>
                            <div class="tab-content" id="registerTabContent">


                                <div class="design-card tab-pane fade @if ($activeTab == 'member-register') show active @endif"
                                    id="member-register" role="tabpanel" aria-labelledby="home-tab">
                                    <div class="row">

                                        <form method="POST" action="{{ route('register') }}">
                                            @csrf
                                            <input type="hidden" name="type" value="member">
                                            <input type="hidden" name="role" value="members">
                                            <input type="hidden" name="tab" value="register1">
                                            <div class="row gx-2 gy-0">

                                                <div class="col-md-6">
                                                    <label for="formFile" class="form-label"></label>
                                                    <input type="text" class="form-control" name="name"
                                                        id="register-name" placeholder="Name" value="{{ old('name') }}">
                                                </div>
                                                <div class="col-md-6">
                                                    <label for="formFile" class="form-label"></label>
                                                    <input type="text" class="form-control" name="username"
                                                        id="register-username" placeholder="username"
                                                        value="{{ old('username') }}">
                                                </div>
                                                <div class="col-md-6">
                                                    <label for="formFile" class="form-label"></label>
                                                    <input type="password" class="form-control" name="password"
                                                        id="register-password" placeholder="Password">
                                                </div>
                                                <div class="col-md-6">
                                                    <label for="formFile" class="form-label"></label>
                                                    <input type="password" class="form-control"
                                                        name="password_confirmation" id="register-password2"
                                                        placeholder="Password">
                                                </div>
                                                <div class="col-md-6">
                                                    <label for="formFile" class="form-label"></label>
                                                    <input type="email" class="form-control" name="email"
                                                        id="exampleInputPassword1" placeholder="Your mail"
                                                        value="{{ old('email') }}">
                                                </div>
                                                <div class="col-md-6">
                                                    <label for="formFile" class="form-label"></label>
                                                    <input type="text" class="form-control" id="exampleInputPassword1"
                                                        placeholder="Phone number" name="phone"
                                                        value="{{ old('phone') }}">
                                                </div>
                                                <div class="col-12">
                                                    <x-recaptcha />
                                                </div>
                                            </div>
                                            <button type="submit"
                                                class="create btn btn-primary px-4 mt-4 fw-bold text-black">@lang('Create')</button>

                                        </form>

                                    </div>
                                </div>

                                <div class="design-card tab-pane fade @if ($activeTab == 'company-register') show active @endif "
                                    id="company-register" role="tabpanel" aria-labelledby="home-tab">
                                    <form method="POST" action="{{ route('register') }}">
                                        @csrf
                                        <input type="hidden" name="type" value="company">
                                        <input type="hidden" name="role" value="company">
                                        <input type="hidden" name="tab" value="register2">
                                        <div class="row gx-2 gy-0">
                                            <div class="col-md-3">
                                                <label for="formFile" class="form-label"></label>
                                                <input type="text" class="form-control" name="name"
                                                    id="register-name" placeholder="Name" value="{{ old('name') }}">
                                            </div>
                                            <div class="col-md-3">
                                                <label for="formFile" class="form-label"></label>
                                                <input type="text" class="form-control" name="username"
                                                    id="register-username" placeholder="username"
                                                    value="{{ old('username') }}">
                                            </div>
                                            <div class="col-md-6">
                                                <label for="formFile" class="form-label"></label>
                                                <input type="text" class="form-control" name="attributes[address]"
                                                    id="register-address" placeholder="Address"
                                                    value="{{ old('attributes.address') }}">
                                            </div>
                                            <div class="col-md-3">
                                                <label for="formFile" class="form-label"></label>
                                                <input type="password" class="form-control" name="password"
                                                    id="register-password" placeholder="Password">
                                            </div>
                                            <div class="col-md-3">
                                                <label for="formFile" class="form-label"></label>
                                                <input type="password" class="form-control" name="password_confirmation"
                                                    id="register-password2" placeholder="Password">
                                            </div>
                                            <div class="col-md-6">
                                                <label for="formFile" class="form-label"></label>
                                                <input type="email" class="form-control" name="email"
                                                    id="exampleInputPassword1" placeholder="Your mail"
                                                    value="{{ old('email') }}">
                                            </div>
                                            <div class="col-md-6">
                                                <label for="formFile" class="form-label"></label>
                                                <input type="text" class="form-control" id="exampleInputPassword1"
                                                    placeholder="Phone number" name="phone"
                                                    value="{{ old('phone') }}">
                                            </div>
                                            <div class="col-md-6">
                                                <label for="formFile" class="form-label"></label>
                                                <input type="text" class="form-control" id="exampleInputPassword1"
                                                    placeholder="Phone number 2" name="attributes[mobile]"
                                                    value="{{ old('attributes.mobile') }}">
                                            </div>
                                            <div class="col-md-12">
                                                <label for="formFile" class="form-label"></label>
                                                <input type="text" class="form-control" id="exampleInputPassword1"
                                                    placeholder="Youtube" name="attributes[youtube]"
                                                    value="{{ old('attributes.youtube') }}">
                                            </div>
                                            <div class="col-md-12">
                                                <x-recaptcha />
                                            </div>
                                            <div class="col-md-12 d-flex justify-content-end">
                                                <button type="submit" class="create btn btn-primary fw-bold text-black px-4 mt-4">@lang('Create')</button>
                                            </div>
                                        </div>


                                    </form>

                                </div>

                                <div class="design-card tab-pane fade @if ($activeTab == 'freelancer-register') show active @endif "
                                    id="freelancer-register" role="tabpanel" aria-labelledby="home-tab">
                                    <form method="POST" action="{{ route('register') }}" enctype="multipart/form-data">
                                        @csrf
                                        <input type="hidden" name="type" value="freelancer">
                                        <input type="hidden" name="role" value="freelancers">
                                        <input type="hidden" name="tab" value="register3">
                                        <div class="tab-content">
                                            <div class="tab-pane fade show active" id="freelancer-register-1">
                                                <div class="row gx-2 gy-0">

                                                    <div class="col-md-6">
                                                        <label for="formFile" class="form-label"></label>
                                                        <input type="text" class="form-control" name="name"
                                                            id="register-name" placeholder="Name" value="{{ old('name') }}">
                                                    </div>
                                                    <div class="col-md-6">
                                                        <label for="formFile" class="form-label"></label>
                                                        <input type="email" class="form-control" name="email"
                                                            id="exampleInputPassword1" placeholder="Your mail"
                                                            value="{{ old('email') }}">
                                                    </div>
                                                    <div class="col-md-6">
                                                        <label for="formFile" class="form-label"></label>
                                                        <input type="text" class="form-control" name="username"
                                                            id="register-username" placeholder="username"
                                                            value="{{ old('username') }}">
                                                    </div>
                                                    <div class="col-md-6">
                                                        <label for="formFile" class="form-label"></label>
                                                        <input type="text" class="form-control" id="exampleInputPassword1"
                                                            placeholder="Phone number" name="phone"
                                                            value="{{ old('phone') }}">
                                                    </div>
                                                    <div class="col-md-6">
                                                        <label for="formFile" class="form-label"></label>
                                                        <input type="password" class="form-control" name="password"
                                                            id="register-password" placeholder="Password">
                                                    </div>
                                                    <div class="col-md-6">
                                                        <label for="formFile" class="form-label"></label>
                                                        <input type="password" class="form-control" name="password_confirmation"
                                                            id="register-password2" placeholder="Password">
                                                    </div>
                                                    <div class="col-md-6">
                                                        <label for="formFile" class="form-label"></label>
                                                        <input type="text" class="form-control" name="attributes[first_language]"
                                                            id="register-password2" placeholder="First Language">
                                                    </div>
                                                    <div class="col-md-6">
                                                        <label for="formFile" class="form-label"></label>
                                                        <input type="text" class="form-control" name="attributes[second_language]"
                                                            id="register-password2" placeholder="Second Language">
                                                    </div>


                                                    <div class="col-md-12 d-flex justify-content-end">
                                                        <button type="button" id="next-freelancer-button" class="create btn btn-primary fw-bold text-black px-4 mt-4">
                                                            @lang('Next')
                                                            <i class="bi bi-arrow-right me-2"></i>
                                                            <span style="width:25px; height:25px;padding-top:2px;" class="ms-4 bg-white d-inline-block rounded-circle">2</span>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tab-pane fade" id="freelancer-register-2">
                                                <div class="row gx-2 gy-0">
                                                    <div class="col-md-6">
                                                        <label for="formFile" class="form-label"></label>
                                                        <input type="text" class="form-control" name="attributes[job]"
                                                            id="register-password2" placeholder="Job">
                                                    </div>
                                                    <div class="col-md-6">
                                                        <label for="formFile" class="form-label"></label>
                                                        <input type="text" class="form-control" name="attributes[design_sample]"
                                                            id="register-password2" placeholder="Your Design">
                                                    </div>
                                                    <div class="col-md-6">
                                                        <label for="formFile" class="form-label"></label>
                                                        <input type="date" class="form-control" name="attributes[birthdate]"
                                                            id="register-password2" placeholder="Birthdate">
                                                    </div>
                                                    <div class="col-md-6">
                                                        <label for="formFile" class="form-label"></label>
                                                        <input type="text" class="form-control" name="attributes[nationality]"
                                                            id="register-password2" placeholder="Nationality">
                                                    </div>
                                                    <div class="col-md-6">
                                                        <label for="formFile" class="form-label"></label>
                                                        <input type="text" class="form-control" name="attributes[experience]"
                                                            id="register-password2" placeholder="Experience">
                                                    </div>
                                                    <div class="col-md-6">
                                                        <label for="formFile" class="form-label"></label>
                                                        <input type="file" class="form-control" name="attributes[cv]"
                                                            id="register-password2" placeholder="C.V" accept=".pdf, .doc, .docx">
                                                    </div>
                                                    <div class="col-md-12">
                                                        <x-recaptcha />
                                                    </div>
                                                    <div class="col-md-12 d-flex justify-content-end">
                                                        <button type="button" id="back-freelancer-button" class="create btn btn-light fw-bold text-black px-4 mt-4 bg-light me-2">
                                                            <i class="bi bi-arrow-left me-2"></i>
                                                            @lang('Back')
                                                        </button>
                                                        <button type="submit" class="create btn btn-primary fw-bold text-black px-4 mt-4">@lang('Create')</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>


                            </div>
                            <div id="login-area" class="row justify-content-between align-items-center mt-4">
                                <div class="col-auto">
                                    <p class="create-new"><a href="{{route('login')}}">Already have an account</a></p>
                                </div>
                                <div class="col-auto d-flex align-items-center">
                                    <p class="login-with m-0 me-2">or login with</p>
                                    <ul class="d-flex nav m-0">
                                        @if(google_auth_enabled())
                                        <li class="p-2 ms-2" style="border:1px solid #d32c2c;background-color:#d32c2c;">
                                            <a href="{{ route('auth.login.google') }}" class="" style="background-color:#d32c2c;color:#fff;">
                                                <i class="bi bi-envelope"></i>
                                            </a>
                                        </li>
                                        @endif
                                        @if(facebook_auth_enabled())
                                        <li class="p-2 ms-2" style="border:1px solid #2778ce;background-color:#2778ce;">
                                            <a href="{{ route('auth.login.facebook') }}" class="" style="background-color:#2778ce;color:#fff;">
                                                <i class="bi bi-facebook"></i></a>
                                        </li>
                                        @endif
                                        @if(twitter_auth_enabled())
                                        <li class="p-2 ms-2" style="border:1px solid #00b6ff;background-color:#00b6ff;">
                                            <a href="{{ route('auth.login.twitter') }}" class="" style="background-color:#00b6ff;color:#fff;">
                                            <i class="bi bi-twitter"></i>
                                            </a>
                                        </li>
                                        @endif
                                </div>
                                </ul>



                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div><!-- End #content -->
    <script type="text/javascript">
        document.addEventListener('DOMContentLoaded', function(){
            const nextFreelancerButton = document.getElementById('next-freelancer-button');
            const backFreelancerButton = document.getElementById('back-freelancer-button');
            const freelancerStep1 = document.getElementById('freelancer-register-1');
            const freelancerStep2 = document.getElementById('freelancer-register-2');
            nextFreelancerButton.addEventListener('click', function(){
                freelancerStep1.classList.remove('show');
                freelancerStep1.classList.remove('active');
                freelancerStep2.classList.add('show');
                freelancerStep2.classList.add('active');
            });
            backFreelancerButton.addEventListener('click', function(){
                freelancerStep2.classList.remove('show');
                freelancerStep2.classList.remove('active');
                freelancerStep1.classList.add('show');
                freelancerStep1.classList.add('active');
            });
        });
    </script>
@endsection
