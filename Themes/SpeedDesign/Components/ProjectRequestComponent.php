<?php

namespace Themes\SpeedDesign\Components;

use App\Models\User;
use Cascade\System\Entities\Category;
use Cascade\System\Services\System;
use Illuminate\Support\Arr;
use Livewire\Component;
use Livewire\WithFileUploads;
use Modules\Projects\Entities\Project;
use Modules\Projects\Entities\ProjectType;
use Modules\SpeedDesign\Entities\Freelancer;

class ProjectRequestComponent extends Component
{
    use WithFileUploads;

    public $space;
    public $time;
    public $notes;
    public $attachments = [];
    public $type;
    public $category;
    public $designer_code;
    public $alerts = '';

    public $showSuccess = false;

    public function mount()
    {

    }

    public function Save()
    {
        if(empty($this->space) || empty($this->time) || empty($this->type) || intval($this->type) == 0 || empty($this->category) || intval($this->category) == 0)
        {
            $this->alerts = __('Please Fill Required Fields');
            $this->emit('refreshComponent');
            return;
        }
        if(!empty($this->designer_code))
        {
            $designer = User::find(intval($this->designer_code));
            if($designer == null || $designer->type != Freelancer::class)
            {
                $this->alerts = __('Invalid Designer Code');
                $this->emit('refreshComponent');
                return;
            }
        }
        $this->alerts = '';

        $project = new Project();
        $project->description = $this->notes ?? '';
        $project->type_id = intval($this->type);
        $project->customer_id = auth()->id();
        $project->status_id = intval(get_setting('projects::general.setup.default_status', 0));
        $project->save();
        if(intval($this->category) > 0)
        {
            $project->categories()->sync([intval($this->category)]);
        }
        $project->storeAttribute('designer_code', empty($this->designer_code) ? [] : Arr::wrap($this->designer_code));
        $project->storeAttribute('space', $this->space ?? '');
        $project->storeAttribute('desired_time', $this->time ?? '');
        if($this->attachments && count($this->attachments) > 0)
        {
            foreach($this->attachments as $attachment)
            { $project->attach($attachment); }
        }
        $this->showSuccess = true;
    }

    public function render()
    {
        return System::FindView('theme::components.request', [
            'types' => ProjectType::all(),
            'categories' => Category::of(Project::class)->get(),
        ]);
    }
}
