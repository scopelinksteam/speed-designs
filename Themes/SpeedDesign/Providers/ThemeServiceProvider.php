<?php

namespace Themes\SpeedDesign\Providers;

use Cascade\Site\Entities\ContentBlock;
use Cascade\Site\Entities\ContentBlockField;
use Cascade\Site\Entities\ContentBlockType;
use Cascade\Site\Entities\Page;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Config;
use Cascade\System\Services\System;
use Cascade\Site\Providers\ThemeProvider;
use Cascade\Site\Services\PagesService;
use Cascade\Site\Services\PostType;
use Cascade\Site\Services\ThemeCustomizer;
use Cascade\Site\Services\Site;
use Cascade\System\Entities\Category;
use Cascade\System\Http\Middleware\ForceLoginMiddleware;
use Cascade\System\Services\NotificationService;
use Illuminate\Support\Facades\View;
use Livewire\Livewire;
use Modules\Advertisements\Entities\Advertisement;
use Modules\Portfolio\Entities\PortfolioItem;
use Modules\Projects\Entities\Project;
use Modules\Projects\Entities\ProjectEstimation;
use Modules\SpeedDesign\Entities\Freelancer;
use Modules\SpeedDesign\Notifications\ProjectEstimatedNotification;
use Modules\SpeedDesign\Notifications\ProjectEstimationRequestNotification;
use Modules\Sponsors\Entities\Sponsor;
use Themes\SpeedDesign\Components\ProjectRequestComponent;

class ThemeServiceProvider extends ThemeProvider
{
    /**
     * @var string $themeName
     */
    protected $themeName = 'SpeedDesign';

    /**
     * @var string $moduleNameLower
     */
    protected $themeNameLower = 'speeddesign';

    /*************************************************/
    /* Theme Registrations                           */
    /*************************************************/

    //Register Theme Assets
    public function Assets()
    {
        if(active_language()->is_rtl)
        {
            Site::AddStyle(theme_asset('/css/main-rtl.css'));
        }

        Site::AddScript(cascade_asset('plugins/alpinejs/alpinejs.min.js'));
    }

    //Register Theme Components
    public function Components()
    {
        Livewire::component('project.request', ProjectRequestComponent::class);
    }

    //Register Theme Widgets
    public function Widgets() {}

    //Register Theme Content Blocks
    public function ContentBlocks()
    {
        ContentBlock::RegisterType(
            ContentBlockType::make('client', __('Clients'))
            ->fields(ContentBlockField::make(ContentBlockField::TITLE_FIELD), ContentBlockField::make(ContentBlockField::IMAGE_FIELD))
        );
    }

    //Register Theme Post Types
    public function PostTypes() {}

    //Register Theme Custom Views
    public function Views()
    {

        PostType::of(Page::class)->addTemplate('theme::templates.pages.about', __('About Page'));
        PostType::of(Page::class)->addTemplate('theme::templates.pages.blank', __('Blank Template'));

        View::composer('theme::templates.pages.about', function($view){
            return $view->with([
                'clients' => ContentBlock::type('client')->get()
            ]);
        });

        NotificationService::SetView(ProjectEstimationRequestNotification::class, 'theme::notifications.estimation_request');
        NotificationService::SetView(ProjectEstimatedNotification::class, 'theme::notifications.estimation_recieved');

        System::OverrideView('auth.login', 'theme::auth.login');
        System::OverrideView('auth.register', 'theme::auth.register');
        System::OverrideView('welcome', 'theme::pages.home', function(){
            return [
                'portfolio_categories' => Category::of(PortfolioItem::class)->orderBy('name', 'asc')->get(),
                'portfolio_type' => PortfolioItem::class,
            ];
        });
        System::OverrideView('portfolio::frontend.items.show', 'theme::pages.portfolio.show');
        System::OverrideView('users::frontend.profile.index', 'theme::pages.profile.index', function(){
            $user = auth()->user()->impersonate();
            $showGallery = $user instanceof Freelancer;
            $projects = Project::whereHas('members', function($membersQuery) use($user){
                return $membersQuery->where('member_id', $user->id);
            })->get();
            return [
                'showGallery' => $showGallery,
                'projects' => $projects,
            ];
        });


        System::OverrideView('users::frontend.profile.edit', 'theme::pages.profile.edit',function(){
            $user = auth()->user()->impersonate();
            $showGallery = $user instanceof Freelancer;
            return [
                'showGallery' => $showGallery
            ];
        });
        System::OverrideView('users::frontend.profile.show', 'theme::pages.profile.show');
        System::OverrideView('contact', 'theme::pages.contact');
        System::OverrideView('speeddesign::frontend.orders.index', 'theme::pages.orders.index');
        System::OverrideView('speeddesign::frontend.estimations.create', 'theme::pages.estimations.create');
        System::OverrideView('speeddesign::frontend.estimations.terms', 'theme::pages.estimations.terms');
        System::OverrideView('speeddesign::frontend.estimations.contract', 'theme::pages.estimations.contract');
        System::OverrideView('speeddesign::frontend.projects.customer', 'theme::pages.projects.customer');
        System::OverrideView('speeddesign::frontend.projects.show', 'theme::pages.projects.show');
        System::OverrideView('speeddesign::frontend.projects.freelancer', 'theme::pages.projects.freelancer');
        System::OverrideView('speeddesign::frontend.chat.index', 'theme::pages.chat.index');
        System::OverrideView('twilio::shared.components.chatroom', 'theme::pages.chat.room');
        System::OverrideView('site::frontend.pages.single', 'theme::pages.page');
        System::OverrideView('sponsors::frontend.show', 'theme::pages.sponsor');
        View::composer('theme::partials.recommendations', function($view){
            return $view->with([
                'sponsors' => Sponsor::all(),
            ]);
        });
        View::composer('theme::partials.nominated', function($view){
            $freelancers = Freelancer::all();
            $map = [];
            foreach($freelancers as $freelancerObject)
            {
                $map[$freelancerObject->id] = doubleval($freelancerObject->Attribute('performance_rating', 0));
            }
            arsort($map);
            $map = array_slice($map, 0, 3, true);
            $freelancers = [];
            foreach($map as $id => $rating)
            {
                $freelancers[] = Freelancer::find($id);
            }
            return $view->with([
                'freelancers' => collect($freelancers)
            ]);

        });
        View::composer('theme::partials.sidebars.notifications', function($view){
            $notifications = [];
            if(auth()->check())
            {
                $user = auth()->user()->impersonate();
                $notifications = $user->notifications;
            }
            return $view->with('notifications', $notifications);
        });

        View::composer('theme::partials.sidebars.advertisements', function($view){
            return $view->with([
                'advertisements' => Advertisement::all(),
            ]);
        });
    }

    //Register Theme Options
    public function Customizer()
    {
        ThemeCustomizer::theme('speeddesign', __('SpeedDesign Theme Options'), function(){
            ThemeCustomizer::page(__('General'), 'general', function(){
                ThemeCustomizer::group(__('Google Analytics'), function(){
                    ThemeCustomizer::text(__('Measurement Tag'), 'google_analytics.measurement_tag');
                });
            });
            ThemeCustomizer::page(__('Appearance'), 'appearance', function(){
                ThemeCustomizer::group(__('Sponsor Page'), function(){
                    ThemeCustomizer::image(__('Background'), 'sponsor.background');
                });
            });
        });
    }

    /*************************************************/
    /* Events Handling                               */
    /*************************************************/
    /**
     * Fires on Application Register
     *
     * @return void
     */
    public function OnRegister()
    {
        ForceLoginMiddleware::Except('/');
        ForceLoginMiddleware::Except('page/*');
        ForceLoginMiddleware::Except('localize/*');
        System::EnableService(PagesService::class);
    }

    /**
     * Fires on Application Boot after All Services Registered
     *
     * @return void
     */
    public function OnBoot() {}

    /**
     * Fires When Theme Activated
     *
     * @return void
     */
    public function OnActivate() {}

    /**
     * Fires When Theme Deactivated
     *
     * @return void
     */
    public function OnDeactivate() {}
}
